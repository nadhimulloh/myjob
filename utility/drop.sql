drop user my_job cascade;

create user my_job identified by my_job;

grant dba to my_job;

commit;
exit;
