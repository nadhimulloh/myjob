unit BAST;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, URecord, UConst, UGeneral,
  UTransaksi, QRPrntr, UEngine, OracleConnection, USystemMenu, StdCtrls;

type
  TR_print = Record
    No, nopo, nilaipo, nilaitagih, ket : string;
  End;
  AR_print = array of TR_print;
  TfrmBAST = class(TForm)
    qrpprint: TQuickRep;
    PageHeaderBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    qrlDesk: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    qrlNilaiPO: TQRLabel;
    QRLabel14: TQRLabel;
    qrlQty: TQRLabel;
    QRLabel16: TQRLabel;
    qrlTot: TQRLabel;
    qrlNAmaPer: TQRLabel;
    QRShape1: TQRShape;
    QRShape4: TQRShape;
    qrlDeskripsiPer: TQRLabel;
    qrlAlamatPer: TQRLabel;
    qrlPhone: TQRLabel;
    QRLabel4: TQRLabel;
    qrParagrap1: TQRMemo;
    z: TQRBand;
    qrlFooterPer2: TQRLabel;
    qrlFooterNama2: TQRLabel;
    qrlFooterJab2: TQRLabel;
    mmTemp: TMemo;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRLabel2: TQRLabel;
    qrlPek: TQRLabel;
    qrlLok: TQRLabel;
    qrlProj: TQRLabel;
    qrlBast: TQRLabel;
    QRLabel8: TQRLabel;
    qrlNama1: TQRLabel;
    qrmJabatan1: TQRMemo;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    qrlNama2: TQRLabel;
    qrmJabatan2: TQRMemo;
    QRLabel1: TQRLabel;
    QRLabel23: TQRLabel;
    qrlPO: TQRLabel;
    QRLabel3: TQRLabel;
    qrlBaut2: TQRLabel;
    QRMemo1: TQRMemo;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRMemo2: TQRMemo;
    qrlFooterPer1: TQRLabel;
    qrlFooterNama1: TQRLabel;
    qrlFooterJab1: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    procedure qrpprintBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrpprintNeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
    i, y : integer;
    persen : real;
    datenow: tdate;
    arr_Print : AR_print;
    ardetJual : aR_data_jual_detail;
    dtMstJual : TR_data_jual_master;
    procedure preparedPrint;
  public
    { Public declarations }
    procedure Execute(dtMst : TR_invoice_master);
  end;

var
  frmBAST: TfrmBAST;

implementation

uses
  UFinance, StrUtils, DateUtils;

{$R *.dfm}

{ TfrmBAUT }

procedure TfrmBAST.Execute(dtMst: TR_invoice_master);
var dtDet :TR_invoice_detail;
    company : TR_company_info;
    mstJob  : TR_master_job;
    mstcard  : TR_master_card;
    thn, bln, tgl: integer;
    listBln,listHari : TStringList;
begin
  listBln  := TStringList.Create;
  listHari := TStringList.Create;

  Company:=Get_company_info;
  dtDet := Get_invoice_detail(dtMst.seq);
  dtMstJual:= Get_data_jual_master(dtDet.jual_seq);
  ardetJual  := Get_Arr_data_jual_detail(dtMstJual.seq);
  mstJob := Get_master_job(dtMstJual.job_seq);
  mstcard := Get_master_card(dtMstJual.card_seq);

  qrlPek.Caption := ': '+MixedCase(mstJob.deskripsi);
  qrlLok.Caption := ': '+MixedCase(mstJob.lokasi);
  qrlProj.Caption := ': '+MixedCase(mstJob.nama);
  qrlBast.Caption := ': '+dtMst.nomor_bast;

  datenow:= servernow;
  tgl := strtointdef(FormatDateTime('DD',dateNow),0);
  thn := strtointdef(FormatDateTime('YYYY',dateNow),0);
  qrParagrap1.Lines.Text := 'Pada hari ini '+ FormatDateTime('dddd',dateNow)+' tanggal '+
  MixedCase(MoneyToIndonesianText(inttostr(tgl),false))+' bulan '+FormatDateTime('MMMM', dateNow)+' tahun '+
  MixedCase(MoneyToIndonesianText(inttostr(thn),false))+' '+FormatDateTime('(DD/MM/YYYY).',dateNow)+#13+
  'Kami yang bertanda tangan dibawah ini :';

  qrlNama1.Caption := MixedCase(mstcard.png_jwb1);
  qrmJabatan1.Lines.Text := MixedCase(mstcard.jabatan1)+#13+'Dalam hal ini bertindak dan atas nama '+
    UpperCase(mstcard.nama)+'(Selanjutnya disebut "Kisel") dan, ';

  qrlNama2.Caption := MixedCase(company.png_jwb1);
  qrmJabatan2.Lines.Text := MixedCase(company.jabatan1)+#13+'Dalam hal ini bertindak dan atas nama '+
    UpperCase(company.png_jwb1)+'(Selanjutnya disebut "Vendor").';

  qrlPO.Caption := ': '+dtMstJual.nomor_po+' Tanggal : '+FormatDateTime('dd MMMM yyyy',
      dtMstJual.tanggal);
  qrlBaut2.Caption := ': '+dtMst.nomor_baut+' Tanggal : '+FormatDateTime('dd MMMM yyyy',
    dtMst.tanggal);


  qrlFooterPer1.Caption := MixedCase(mstcard.nama);
  qrlFooterNama1.Caption := MixedCase(mstcard.png_jwb1);
  qrlFooterJab1.Caption := MixedCase(mstcard.jabatan1);

  qrlFooterPer2.Caption := MixedCase(company.nama);
  qrlFooterNama2.Caption := MixedCase(company.png_jwb1);
  qrlFooterJab2.Caption := MixedCase(company.jabatan1);

//  qrlNAmaPer.Caption   := Company.nama;
//  qrlAlamatPer.Caption := Company.alamat;
//  qrlPhone.Caption  := ifthen(Company.no_telp<>'', 'Phone : '+Company.no_telp)+ ifthen(
//    Company.email<>'', ' Email: '+Company.email) ;
////   qrlNamaper3.Caption   := Company.nama;
//  qrlNamaPimpinan.Caption   := Company.png_jwb1;
//  qrlJabatan.Caption   := Company.jabatan1;
//
//  qrlTempatTanggal.Caption  := 'Bandung, '+FormatDateTime('dd MMMM yyyy', DtMst.tanggal);
//  qrmTerbilang.lines.text := 'Terbilang : ---'+ MoneyToIndonesianText(FloatToStr(DtMst.total))+'---';


  persen := (dtMst.total / dtMstJual.subtotal) * 100;
  SetLength(arr_print, 1);
  arr_print[0].nopo       := dtMstJual.nomor_po;
  arr_print[0].nilaipo    := FloatToStrFmt(dtMstJual.subtotal);
  arr_print[0].nilaitagih := FloatToStrFmt(dtMst.total);
  arr_Print[0].ket        := '('+FloatToStrFmt2(persen)+'%)';

//  qrlNoTrans.Caption        := dtMst.nomor;
//  qrlNoPO.Caption           := ': '+dtMstJual.nomor_po;
//  qrlCard.Caption           := EkstrakString(ListCardGlobal.Values[IntToStr(dtMstJual.card_seq)],
//    '#',2);
//  qrAlamat.Lines.Text       := EkstrakString(ListCardGlobal.Values[IntToStr(dtMstJual.card_seq)],
//    '#',7)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMstJual.card_seq)],
//    '#',8)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMstJual.card_seq)],
//    '#',9)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMstJual.card_seq)],
//    '#',11);


//  if dtMstJual.ppn <> 0 then begin
//    qrlgrandTotal.Caption     := FloatToStrFmt(dtMst.total);
//    qrlPpn.Caption            := FloatToStrFmt((dtMst.total*10)/100);
//    qrltotal.Caption          := FloatToStrFmt(dtMst.total-((dtMst.total*10)/100));
//  end else begin
//    qrlgrandTotal.Caption     := FloatToStrFmt(dtMst.total);
//    qrlPpn.Caption            := FloatToStrFmt(0);
//    qrltotal.Caption          := FloatToStrFmt(dtMst.total);
//  end;

  preparedPrint;
  qrpprint.PreviewModal;
end;

procedure TfrmBAST.preparedPrint;
var idx, idx1,j, z : integer;
    hrg : real;
begin
  idx:=0;



//  mmTemp.Lines.Text  := EkstrakString(ListJobGlobal.
//                    Values[IntToStr(dtMstJual.job_seq)],'#',2)
//                    +'-'+
//                    EkstrakString(ListJobGlobal.Values[IntToStr(
//                                  dtMstJual.job_seq)],'#',6);
//
//  for z := 0 to mmTemp.Lines.Count-1 do begin
//    if z = 0 then arr_print[idx].uraian := mmTemp.Lines[Z]
//    else begin
//      if mmTemp.Lines[Z] <> '' then BEGIN
//        inc(idx);
//        SetLength(arr_print, idx+1);
//        arr_print[idx].uraian := mmTemp.Lines[Z];
//      END;
//    end;
//  end;
//
//  arr_print[idx].nilaipo     := '';
//  arr_print[idx].qty         := '';
//  arr_print[idx].total       := '';
//  arr_Print[idx].isHeader    := 'T';
//  inc(idx);
//  for j := 0 to Length(ardetJual)-1 do begin
//    SetLength(arr_print, idx+1);
//    arr_print[idx].No          := IntToStr(J+1)+'.';
//
//    mmTemp.Lines.Text  := ardetJual[j].spesifikasi;
//
//    idx1 := idx;
//    for z := 0 to mmTemp.Lines.Count-1 do begin
//      if z = 0 then arr_print[idx].uraian := mmTemp.Lines[z]
//      else begin
//        SetLength(arr_print, idx+1);
//        arr_print[idx].uraian := mmTemp.Lines[z];
//        inc(idx);
//      end;
//    end;
//
//    hrg := (((ardetJual[j].harga+ardetJual[j].PPN)*ardetJual[j].qty)*persen)/100;
//    arr_print[idx1].nilaipo     := FloatToStrFmt((ardetJual[j].harga+ardetJual[j].PPN)*ardetJual[j].qty);
//    arr_print[idx1].qty         := FloatToStrFmt2(persen)+'%';
//    arr_print[idx1].total       := FloatToStrFmt(hrg);
//    arr_Print[idx1].isHeader    := '';
//    arr_Print[idx1].uraian      := ' - '+arr_Print[idx1].uraian;
//    inc(idx);
//  end;
end;

procedure TfrmBAST.qrpprintBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  i := 0; y := 0;
end;

procedure TfrmBAST.qrpprintNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := i < Length(arr_print);
  if MoreData then begin
//    qrlNo.Caption       := arr_print[i].No;
    qrlDesk.Caption     := arr_print[i].nopo;
    qrlNilaiPO.Caption  := arr_print[i].nilaipo;
    qrlQty.Caption      := arr_print[i].nilaitagih;
    qrlTot.Caption      := arr_print[i].ket;
    inc(i);
  end;

end;

end.

