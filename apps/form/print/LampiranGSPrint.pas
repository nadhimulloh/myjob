unit LampiranGSPrint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, URecord, UConst, UGeneral,
  UTransaksi, QRPrntr, UEngine, OracleConnection, USystemMenu, StdCtrls, AdvGrid;

type
  TR_print = Record
    No,
    uraian,
    Qty,
    unitt,
    HrgbELI,
    TotalbELI,
    Hrg         ,
    Total       ,
    keterangan  ,
    Seq ,seqVendor        : string;
  End;
  AR_print = array of TR_print;
  TfrmLampiranGSPrint = class(TForm)
    qrpprint: TQuickRep;
    PageHeaderBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel11: TQRLabel;
    z: TQRBand;
    QRShape13: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRShape11: TQRShape;
    QRShape14: TQRShape;
    qrNo: TQRLabel;
    qrUraian: TQRLabel;
    QRShape15: TQRShape;
    qrKet: TQRLabel;
    qrUnit: TQRLabel;
    qrQty: TQRLabel;
    qrHargaBeli: TQRLabel;
    qrTotBeli: TQRLabel;
    QRShape18: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    qrHargaJual: TQRLabel;
    qrTotJual: TQRLabel;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    qrlJudul: TQRLabel;
    qrlPT: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape36: TQRShape;
    QRLabel23: TQRLabel;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRShape42: TQRShape;
    QRShape43: TQRShape;
    QRLabel24: TQRLabel;
    qrSumHB: TQRLabel;
    qrSUMHJ: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel19: TQRLabel;
    qrTanggal: TQRLabel;
    QRLabel21: TQRLabel;
    qrTanggal2: TQRLabel;
    qrPT2: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape48: TQRShape;
    QRShape50: TQRShape;
    QRShape49: TQRShape;
    QRShape51: TQRShape;
    qrPersentase: TQRLabel;
    qrGross: TQRLabel;
    qrRekapHJ: TQRLabel;
    qrRekapHB: TQRLabel;
    qrlVendor: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRLabel22: TQRLabel;
    QRShape34: TQRShape;
    QRLabel25: TQRLabel;
    QRShape35: TQRShape;
    QRShape33: TQRShape;
    qrMulai: TQRLabel;
    qrSAmpai: TQRLabel;
    procedure qrpprintBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrpprintNeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
    i, y : integer;
    persen : real;
    arr_Print : AR_print;
    Grid : TAdvStringGrid;
    procedure preparedPrint;
  public
    { Public declarations }
    procedure Execute(var judul: string; agrid:TAdvStringGrid);
  end;

var
  frmLampiranGSPrint: TfrmLampiranGSPrint;

implementation

uses
  UFinance, StrUtils;

const
  colNo            = 0;
  colGSUraian      = 1;
  colGSQty         = 2;
  colGSunit        = 3;
  colGSHrgbELI     = 4;
  colGSTotalbELI   = 5;
  colGSHrg         = 6;
  colGSTotal       = 7;
  colGSketerangan  = 8;
  colGSSeq         = 9;

{$R *.dfm}

{ TfrmLampiranGSPrint }

procedure TfrmLampiranGSPrint.Execute(var judul: string; agrid:TAdvStringGrid);
var company : TR_company_info;
    grs  : real;
begin
  Company := Get_company_info;
  Grid := agrid;
  qrlJudul.Caption := judul;
  qrlPT.Caption    := Company.nama;
  qrPT2.Caption    := Company.nama;
//  qrlPt3.Caption   := Company.nama;

//  qrMulai.Caption   := FormatDateTime('dd MMMM yyyy', Grid.Dates[colGSTgl,2]);
//  qrSampai.Caption  := FormatDateTime('dd MMMM yyyy', Grid.Dates[colGSTgl,grid.rowcount-2]);
  qrlVendor.caption := '';

  qrSumHB.Caption := FloatToStrFmt2(Grid.Floats[colGSTotalbELI,grid.rowcount-1]);
  qrSumHj.Caption := FloatToStrFmt2(Grid.Floats[colGSTotal,grid.rowcount-1]);
  qrRekapHJ.caption := qrSumHj.Caption;
  qrRekapHb.caption := qrSumHb.Caption;
  grs := Grid.Floats[colGSTotal,grid.rowcount-1] - Grid.Floats[colGSTotalbELI,grid.rowcount-1];
  qrGross.Caption := FloatToStrFmt2( grs );
  qrPersentase.Caption := FloatToStrFmt2( (grs/Grid.Floats[colGSTotal,grid.rowcount-1])*100 )+' %';

  qrTanggal.Caption := 'Bandung, '+FormatDateTime('dd MMMM yyyy', ServerNow);
  qrTanggal2.Caption := 'Bandung, '+FormatDateTime('dd MMMM yyyy', ServerNow);

  preparedPrint;
  frmLampiranGSPrint.qrpprint.PreviewModal;
end;

procedure TfrmLampiranGSPrint.preparedPrint;
var idx, idx1,j, z : integer;
    hrg : real;
begin
  idx:=0;
  for j := 2 to Grid.RowCount-2 do begin
    SetLength(arr_print, idx+1);

    arr_Print[idx].No          := Grid.cells[colNo         ,j];
    arr_Print[idx].uraian      := Grid.cells[colGSuraian   ,j];
    arr_Print[idx].Qty         := floattostrfmt(Grid.floats[colGSQty ,j]);
    arr_Print[idx].unitt       := Grid.cells[colGSunit       ,j];
    arr_Print[idx].HrgbELI     := Grid.cells[colGSHrgbELI    ,j];
    arr_Print[idx].TotalbELI   := Grid.cells[colGSTotalbELI  ,j];
    arr_Print[idx].Hrg         := Grid.cells[colGSHrg        ,j];
    arr_Print[idx].Total       := Grid.cells[colGSTotal      ,j];
    arr_Print[idx].keterangan  := Grid.cells[colGSketerangan ,j];
    arr_Print[idx].Seq         := Grid.cells[colGSSeq        ,j];

    inc(Idx);
  end;

end;

procedure TfrmLampiranGSPrint.qrpprintBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  i := 0; y := 0;
end;

procedure TfrmLampiranGSPrint.qrpprintNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := i < Length(arr_print);
  if MoreData then begin
    qrNO.caption := arr_Print[i].No;
    qrUraian.caption := arr_Print[i].uraian;
    qrqty.caption := arr_Print[i].Qty;
    qrunit.caption := arr_Print[i].unitt;
    qrhargabeli.caption := arr_Print[i].HrgbELI;
    qrtotBeli.caption := arr_Print[i].TotalbELI;
    qrhargajual.caption := arr_Print[i].Hrg;
    qrtotJual.caption := arr_Print[i].Total;
    qrKet.caption := arr_Print[i].keterangan;

    if (TrimAllString(arr_Print[i].seqVendor) <> '0') and (TrimAllString(arr_Print[i].seqVendor) <> '') then
      qrlVendor.Caption := EkstrakString(ListCardGlobal.Values[arr_Print[i].seqVendor],'#',2);

    inc(i);
  end;

end;

end.



