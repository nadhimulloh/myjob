unit LampiranPrint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, URecord, UConst, UGeneral,
  UTransaksi, QRPrntr, UEngine, OracleConnection, USystemMenu, StdCtrls, AdvGrid;

type
  TR_print = Record
    No,
    Tgl,
    serial,
    Cust,
    Orig,
    Clus,
    Dest,
    BPid,
    Qty,
    unitt,
    HrgbELI,
    TotalbELI,
    Hrg         ,
    Total       ,
    keterangan  ,
    Seq         : string;
  End;
  AR_print = array of TR_print;
  TfrmLampiranPrint = class(TForm)
    qrpprint: TQuickRep;
    PageHeaderBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    z: TQRBand;
    QRShape13: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRShape11: TQRShape;
    QRShape14: TQRShape;
    qrNo: TQRLabel;
    qrTgl: TQRLabel;
    QRShape15: TQRShape;
    qrOrigin: TQRLabel;
    qrCluster: TQRLabel;
    qrKet: TQRLabel;
    qrBPid: TQRLabel;
    qrUnit: TQRLabel;
    qrQty: TQRLabel;
    qrHargaBeli: TQRLabel;
    qrTotBeli: TQRLabel;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    qrHargaJual: TQRLabel;
    qrTotJual: TQRLabel;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRLabel20: TQRLabel;
    qrNama: TQRLabel;
    qrDest: TQRLabel;
    qrlJudul: TQRLabel;
    qrlPT: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRLabel22: TQRLabel;
    QRShape34: TQRShape;
    QRLabel25: TQRLabel;
    QRShape35: TQRShape;
    QRShape33: TQRShape;
    QRShape36: TQRShape;
    QRLabel23: TQRLabel;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRShape42: TQRShape;
    QRShape43: TQRShape;
    QRLabel24: TQRLabel;
    qrSumHB: TQRLabel;
    qrSUMHJ: TQRLabel;
    qrMulai: TQRLabel;
    qrSAmpai: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel19: TQRLabel;
    qrTanggal: TQRLabel;
    QRLabel21: TQRLabel;
    qrTanggal2: TQRLabel;
    qrPT2: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape48: TQRShape;
    QRShape50: TQRShape;
    QRShape49: TQRShape;
    QRShape51: TQRShape;
    qrPersentase: TQRLabel;
    qrGross: TQRLabel;
    qrRekapHJ: TQRLabel;
    qrRekapHB: TQRLabel;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    qrlSerial: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    mmBPID: TMemo;
    mmSN: TMemo;
    procedure qrpprintBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrpprintNeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
    i, y : integer;
    persen : real;
    arr_Print : AR_print;
    Grid : TAdvStringGrid;
    procedure preparedPrint;
  public
    { Public declarations }
    procedure Execute(var judul: string; agrid:TAdvStringGrid);
  end;

var
  frmLampiranPrint: TfrmLampiranPrint;

implementation

uses
  UFinance, StrUtils, Math;

const
  colNo          = 0;
  colTgl         = 1;
  colCust        = 2;
  colOrig        = 3;
  colClus        = 4;
  colDest        = 5;
  colBPid        = 6;
  colserial      = 7;
  colQty         = 8;
  colunit        = 9;
  colHrgbELI     = 10;
  colTotalbELI   = 11;
  colHrg         = 12;
  colTotal       = 13;
  colketerangan  = 14;
  colSeq         = 15;

{$R *.dfm}

{ TfrmLampiranPrint }

procedure TfrmLampiranPrint.Execute(var judul: string; agrid:TAdvStringGrid);
var company : TR_company_info;
    grs  : real;
begin
  Company := Get_company_info;
  Grid := agrid;
  qrlJudul.Caption := judul;
  qrlPT.Caption   := Company.nama;
  qrPT2.Caption  := Company.nama;

  qrMulai.Caption   := FormatDateTime('dd MMMM yyyy', Grid.Dates[colTgl,2]);
  qrSampai.Caption  := FormatDateTime('dd MMMM yyyy', Grid.Dates[colTgl,grid.rowcount-2]);

  qrSumHB.Caption := FloatToStrFmt2(Grid.Floats[colTotalbELI,grid.rowcount-1]);
  qrSumHj.Caption := FloatToStrFmt2(Grid.Floats[colTotal,grid.rowcount-1]);
  qrRekapHJ.caption := qrSumHj.Caption;
  qrRekapHb.caption := qrSumHb.Caption;
  grs := Grid.Floats[colTotal,grid.rowcount-1] - Grid.Floats[colTotalbELI,grid.rowcount-1];
  qrGross.Caption := FloatToStrFmt2( grs );
  qrPersentase.Caption := FloatToStrFmt2( (grs/Grid.Floats[colTotal,grid.rowcount-1])*100 )+' %';

  qrTanggal.Caption := 'Bandung, '+FormatDateTime('dd MMMM yyyy', ServerNow);
  qrTanggal2.Caption := 'Bandung, '+FormatDateTime('dd MMMM yyyy', ServerNow);

  preparedPrint;
  frmLampiranPrint.qrpprint.PreviewModal;
end;

procedure TfrmLampiranPrint.preparedPrint;
var idx2, idx, idx1,j, z : integer;
    hrg : real;
begin
  idx:=0;
  for j := 2 to Grid.RowCount-2 do begin
    SetLength(arr_print, idx+1);

    arr_Print[idx].No          := Grid.cells[colNo         ,j];
    arr_Print[idx].Tgl         := FormatDateTime('dd MMMM yyyy', Grid.Dates[colTgl,j]);
    arr_Print[idx].Cust        := Grid.cells[colCust       ,j];
    arr_Print[idx].Orig        := Grid.cells[colOrig       ,j];
    arr_Print[idx].Clus        := Grid.cells[colClus       ,j];
    arr_Print[idx].Dest        := Grid.cells[colDest       ,j];
//    arr_Print[idx].BPid        := Grid.cells[colBPid       ,j];
    arr_Print[idx].Qty         := floattostrfmt(Grid.floats[colQty ,j]);
    arr_Print[idx].unitt       := Grid.cells[colunit       ,j];
    arr_Print[idx].HrgbELI     := Grid.cells[colHrgbELI    ,j];
    arr_Print[idx].TotalbELI   := Grid.cells[colTotalbELI  ,j];
    arr_Print[idx].Hrg         := Grid.cells[colHrg        ,j];
    arr_Print[idx].Total       := Grid.cells[colTotal      ,j];
    arr_Print[idx].keterangan  := Grid.cells[colketerangan ,j];
    arr_Print[idx].Seq         := Grid.cells[colSeq        ,j];
//    arr_Print[idx].serial      := Grid.cells[colserial       ,j];


    mmBPID.Lines.Text := trimallstring(Grid.cells[colBPid ,j]);
    if mmBPID.Lines.Count > 1 then begin
      for z := 0 to mmBPID.Lines.Count-1 do begin
        if z = 0 then arr_Print[idx].BPid   := mmBPID.Lines[z]
        else begin
          if trimallstring(mmBPID.Lines[z]) <> '' then begin
            idx1 := max(idx,idx1)+1;
            SetLength(arr_print, length(arr_print)+1);
            arr_Print[idx1].BPid := mmBPID.Lines[z]
          end;
        end;
      end;
    end else begin
      arr_Print[idx].BPid   := mmBPID.Lines.Text;
    end;

    idx2 := idx;
    mmSN.Lines.Text := trimallstring(Grid.cells[colserial ,j]);
    if mmSN.Lines.Count > 1 then begin
      for z := 0 to mmSN.Lines.Count-1 do begin
        if z = 0 then arr_Print[idx].serial   := mmSN.Lines[z]
        else begin
          if trimallstring(mmSN.Lines[z]) <> '' then begin
            try
              idx2 := idx2 + 1;
              if idx2 > idx1 then SetLength(arr_print, length(arr_print)+1);
              arr_Print[idx2].serial := mmSN.Lines[z]
            except
              SetLength(arr_print, length(arr_print)+1);
              arr_Print[idx2].serial := mmSN.Lines[z]
            end;
          end;
        end;
      end;
    end else begin
      arr_Print[idx].serial   := mmSN.Lines.Text;
    end;

//    arr_Print[idx].BPid    := Grid.cells[colBPid   ,j];
//    arr_Print[idx].serial  := Grid.cells[colserial ,j];

    idx := max(idx,idx1);
    idx := max(idx,idx2);
    inc(Idx);
  end;

end;

procedure TfrmLampiranPrint.qrpprintBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  i := 0; y := 0;
end;

procedure TfrmLampiranPrint.qrpprintNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := i < Length(arr_print);
  if MoreData then begin
    qrNO.caption := arr_Print[i].No          ;
    qrTgl.caption := arr_Print[i].Tgl         ;
    qrNama.caption := arr_Print[i].Cust        ;
    qrOrigin.caption := arr_Print[i].Orig        ;
    qrCluster.caption := arr_Print[i].Clus        ;
    qrdest.caption := arr_Print[i].Dest        ;
    qrBpid.caption := arr_Print[i].BPid        ;
    qrlserial.caption := arr_Print[i].serial        ;
    qrqty.caption := arr_Print[i].Qty         ;
    qrunit.caption := arr_Print[i].unitt       ;
    qrhargabeli.caption := arr_Print[i].HrgbELI     ;
    qrtotBeli.caption := arr_Print[i].TotalbELI   ;
    qrhargajual.caption := arr_Print[i].Hrg         ;
    qrtotJual.caption := arr_Print[i].Total       ;
    qrKet.caption := arr_Print[i].keterangan  ;
    //qrseq.caption := arr_Print[i].Seq         ;

    inc(i);
  end;

end;

end.



