unit Invoice2print;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, URecord, UConst, UGeneral,
  UTransaksi, QRPrntr, UEngine, OracleConnection, USystemMenu, StdCtrls;

type
  TR_print = Record
    No, uraian, nilaipo, qty, total, ket, isHeader : string;
  End;
  AR_print = array of TR_print;
  TfrmInvoice2print = class(TForm)
    qrpprint: TQuickRep;
    PageHeaderBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    qrlDesk: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    qrlTot: TQRLabel;
    QRLabel16: TQRLabel;
    qrlKet: TQRLabel;
    qrlNAmaPer: TQRLabel;
    QRShape1: TQRShape;
    QRShape4: TQRShape;
    qrlDeskripsiPer: TQRLabel;
    qrlAlamatPer: TQRLabel;
    qrlPhone: TQRLabel;
    QRLabel1: TQRLabel;
    qrlCard: TQRLabel;
    QRLabel3: TQRLabel;
    qrlNoTrans: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape5: TQRShape;
    imgLogo: TQRImage;
    QRShape6: TQRShape;
    qrlNoPO: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape7: TQRShape;
    QRLabel4: TQRLabel;
    qrAlamat: TQRMemo;
    z: TQRBand;
    QRShape8: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    qrlTotal: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    qrlPpn: TQRLabel;
    q: TQRLabel;
    QRLabel6: TQRLabel;
    qrlGrandTOtal: TQRLabel;
    qrlTempatTanggal: TQRLabel;
    qrlNamaper3: TQRLabel;
    qrlNamaPimpinan: TQRLabel;
    qrlJabatan: TQRLabel;
    QRShape9: TQRShape;
    QRLabel5: TQRLabel;
    qrBank: TQRLabel;
    qrCab: TQRLabel;
    qran: TQRLabel;
    qrNorek: TQRLabel;
    mmTemp: TMemo;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape18: TQRShape;
    qrmTerbilang: TQRMemo;
    QRShape19: TQRShape;
    Memo1: TMemo;
    QRShape2: TQRShape;
    procedure qrpprintBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrpprintNeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
    i, y : integer;
    persen : real;
    IsShowDetail : boolean;
    arDtMstJual : ar_data_jual_master;
    arr_Print : AR_print;
    ardetJual : aR_data_jual_detail;
    arDet :aR_invoice_detail;
    subtotal, ppn : real;
//    dtMstJual : TR_data_jual_master;
    procedure preparedPrint;
  public
    { Public declarations }
    procedure Execute(dtMst : TR_invoice_master; aIsShowDetail : boolean = true);
  end;

var
  frmInvoice2print: TfrmInvoice2print;

implementation

uses
  UFinance, StrUtils, Math;

{$R *.dfm}

{ TfrmInvoice2print }

procedure TfrmInvoice2print.Execute(dtMst: TR_invoice_master; aIsShowDetail : boolean = true);
var tmpNoPO, seqin, file_foto : string;
    company : TR_company_info;
    z : integer;
    uraian:string;
    dtJual : tr_data_jual_master;
begin
  Company:=Get_company_info;
  IsShowDetail := aIsShowDetail;

  imgLogo.Visible := True; imgLogo.Picture.Bitmap.FreeImage;
  file_foto := GetAppPath + 'Images\' + Company.logo;
  imgLogo.Visible := True;
  if FileExists(file_foto) then
    imgLogo.Picture.LoadFromFile(file_foto);

  qrlNAmaPer.Caption   := Company.nama;
  qrlAlamatPer.Caption := Company.alamat;
  qrlPhone.Caption  := ifthen(Company.no_telp<>'', 'Phone : '+Company.no_telp)+ ifthen(
    Company.email<>'', ' Email: '+Company.email) ;
   qrlNamaper3.Caption   := Company.nama;
  qrlNamaPimpinan.Caption   := Company.png_jwb1;
  qrlJabatan.Caption   := Company.jabatan1;

  qrlTempatTanggal.Caption  := 'Bandung, '+FormatDateTime('dd MMMM yyyy', DtMst.tanggal);

  if dtMst.bank_seq > 0 then begin
    qrBank.Caption  := EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 1);
    qrCab.Caption   := 'Cab. '+ EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 2);
    qrNorek.Caption := 'No. Rek : '+EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 3);
    qrAn.Caption    := 'a.n. '+EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 4);
  end;

  subtotal := 0;
  ppn      := 0;
  tmpNoPO := '';
  arDet := Get_Arr_invoice_detail(dtMst.seq);
  for z := 0 to length(arDet)-1 do begin
    seqin := seqin + ifthen(seqin<>'', ',') + inttostr(arDet[z].jual_seq);
    dtJual := Get_data_jual_master(ardet[z].jual_seq);
    tmpNoPO:= tmpNoPO + ifthen(tmpNoPO<>'', ',') + dtJual.nomor_po;

    if dtJual.total = ardet[z].dibayar then begin
      ppn := ppn + dtJual.ppn;
      subtotal := subtotal + dtJual.subtotal;
    end else begin
      if dtJual.ppn > 0 then begin
        ppn := ppn + ( (arDet[z].dibayar)*0.1);
        subtotal := subtotal + (arDet[z].dibayar);
        //ppn := ppn + ( (arDet[z].dibayar/1.1)*0.1);
        //subtotal := subtotal + (arDet[z].dibayar/1.1);
      end else subtotal := subtotal + (arDet[z].dibayar);
    end;
  end;

//  arDtMstJual := Get_arr_data_jual_master(0,0, seqin);
//  for z := 0 to Length(arDtMstJual)-1 do begin
//    tmpNoPO:= tmpNoPO + ifthen(tmpNoPO<>'', ',') + ardtmstJual[z].nomor_po;
////    uraian  := EkstrakString(ListJobGlobal.
////               Values[IntToStr(ardtmstJual[z].job_seq)],'#',2)+'-'+
////               EkstrakString(ListJobGlobal.Values[IntToStr(ardtmstJual[z].job_seq)],'#',6);
//
//  end;

//  ardetJual := Get_Arr_data_jual_master(0, seqIn);

//  dtMstJual := Get_data_jual_master(dtDet.jual_seq);
//  ardetJual := Get_Arr_data_jual_detail(dtMstJual.seq);
//  persen := (dtMst.total / dtMstJual.total) * 100;

  qrlNoTrans.Caption        := dtMst.nomor;
  if IsShowDetail then
    qrlNoPO.Caption           := ': '+tmpNoPO
  else qrlNoPO.Caption           := ': - ';
  qrlCard.Caption           := EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',2);
  qrAlamat.Lines.Text       := EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',7)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',8)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',9)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',11);


//  if dtMst.deskripsi <> '' then
    mmTemp.Lines.Text  := dtMst.deskripsi;
//  else mmTemp.Lines.Text  := uraian;
//  if dtMstJual.ppn <> 0 then begin
//    qrlgrandTotal.Caption     := FloatToStrFmt(dtMst.total);
//    qrlPpn.Caption            := FloatToStrFmt((dtMst.total*10)/100);
//    qrltotal.Caption          := FloatToStrFmt(dtMst.total-((dtMst.total*10)/100));
//  end else begin
    qrlgrandTotal.Caption     := FloatToStrFmt2(subtotal+ppn);
    qrlPpn.Caption            := FloatToStrFmt2(ppn);
    qrltotal.Caption          := FloatToStrFmt2(subtotal);
//  end;
  qrmTerbilang.lines.text := 'Terbilang : ---'+ MoneyToIndonesianText(FloatToStr(subtotal+ppn))+'---';
  preparedPrint;
  frmInvoice2print.qrpprint.PreviewModal;
end;

procedure TfrmInvoice2print.preparedPrint;
var idx, idx2, idx1,j, z : integer;
    hrg : real;
    dt_jual : tr_data_jual_master;
begin
  idx:=0;
  SetLength(arr_print, 1);

//  mmTemp.Lines.Text  := EkstrakString(ListJobGlobal.
//                    Values[IntToStr(dtMstJual.job_seq)],'#',2)
//                    +'-'+
//                    EkstrakString(ListJobGlobal.Values[IntToStr(
//                                  dtMstJual.job_seq)],'#',6);

  for z := 0 to mmTemp.Lines.Count-1 do begin
    if z = 0 then arr_print[idx].uraian := mmTemp.Lines[Z]
    else begin
      if mmTemp.Lines[Z] <> '' then BEGIN
        inc(idx);
        SetLength(arr_print, idx+1);
        arr_print[idx].uraian := mmTemp.Lines[Z];
      END;
    end;
  end;

  arr_print[idx].nilaipo     := '';
  arr_print[idx].qty         := '';
  arr_print[idx].total       := '';
  arr_Print[idx].isHeader    := 'T';

  if IsShowDetail then begin
    inc(idx);
    for j := 0 to Length(arDet)-1 do begin
      SetLength(arr_print, idx+1);
      arr_print[idx].No          := IntToStr(J+1)+'.';

      dt_jual := Get_data_jual_master(ardet[j].jual_seq);

      mmTemp.Lines.Text  := ' - '+EkstrakString(ListJobGlobal.Values[IntToStr(dt_jual.job_seq)],'#',2)+'-'+
      EkstrakString(ListJobGlobal.Values[IntToStr(dt_jual.job_seq)],'#',6);

      idx1 := idx;
      for z := 0 to mmTemp.Lines.Count-1 do begin
        if z = 0 then arr_print[idx].uraian := mmTemp.Lines[z]
        else begin
          inc(idx);
          SetLength(arr_print, idx+1);
           arr_print[idx].uraian := '   '+mmTemp.Lines[z];
        end;
      end;

      persen := (ardet[j].dibayar / dt_jual.subtotal) * 100;

      if persen <> 100 then
        memo1.Lines.text := arDet[j].keterangan + '('+FloatToStrFmt2(persen)+'%)'
      else memo1.Lines.text := arDet[j].keterangan ;

      idx2 := idx1;
      for z := 0 to memo1.Lines.Count-1 do begin
        if z = 0 then arr_print[idx1].ket := memo1.Lines[z]
        else begin
          inc(idx1);
          SetLength(arr_print, idx+1);
           arr_print[idx1].ket := '   '+memo1.Lines[z];
        end;
      end;

  //    hrg := ((arDet[j].harga+arDet[j].PPN)*arDet[j].qty);//*persen)/100;
  //    arr_print[idx1].nilaipo     := FloatToStrFmt((arDet[j].harga+arDet[j].PPN)*arDet[
  //j].qty);
  //    arr_print[idx1].nilaipo     := FloatToStrFmt(arDet[j].harga+arDet[j].PPN);
      arr_print[idx2].qty         := FloatToStrFmt(0);
  //    arr_print[idx1].total       := FloatToStrFmt(hrg);
      arr_print[idx2].total       := FloatToStrFmt2(arDet[j].dibayar);
      arr_Print[idx2].isHeader    := '';
  //    arr_Print[idx1].uraian      := ' - '+arr_Print[idx1].uraian;
  //    arr_Print[idx1].ket         := arDet[j].keterangan;
      idx := max(idx, idx1);
      idx := max(idx, idx2);
      inc(idx);
    end;
  end else begin
    arr_print[idx].nilaipo     := FloatToStrFmt2(subtotal);
    arr_print[idx].qty         := '-';
    arr_print[idx].total       := FloatToStrFmt2(subtotal);
  end;
end;

procedure TfrmInvoice2print.qrpprintBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  i := 0; y := 0;
end;

procedure TfrmInvoice2print.qrpprintNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := i < Length(arr_print);
  if MoreData then begin
//    qrlNo.Caption       := arr_print[i].No;
    qrlDesk.Caption     := arr_print[i].uraian;
//    qrlNilaiPO.Caption  := arr_print[i].nilaipo;
    qrlKet.Caption      := arr_print[i].ket;
    qrlTot.Caption      := arr_print[i].total;
    inc(i);
  end;

end;

end.



