unit Invoice2printFmt2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, URecord, UConst, UGeneral,
  UTransaksi, QRPrntr, UEngine, OracleConnection, USystemMenu, StdCtrls;

type
  TR_print = Record
    No, uraian, nilaipo, qty, total, isHeader : string;
  End;
  AR_print = array of TR_print;
  TfrmInvoice2printFmt2 = class(TForm)
    qrpprint: TQuickRep;
    PageHeaderBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel7: TQRLabel;
    DetailBand1: TQRBand;
    qrlDesk: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    qrlNilaiPO: TQRLabel;
    QRLabel14: TQRLabel;
    qrlQty: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    qrlTot: TQRLabel;
    qrlNAmaPer: TQRLabel;
    QRShape1: TQRShape;
    QRShape4: TQRShape;
    qrlDeskripsiPer: TQRLabel;
    qrlAlamatPer: TQRLabel;
    qrlPhone: TQRLabel;
    QRLabel1: TQRLabel;
    qrlCard: TQRLabel;
    QRLabel3: TQRLabel;
    qrlNoTrans: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape5: TQRShape;
    imgLogo: TQRImage;
    QRShape6: TQRShape;
    qrlNoPO: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape7: TQRShape;
    QRLabel4: TQRLabel;
    qrAlamat: TQRMemo;
    z: TQRBand;
    QRShape8: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    qrlTotal: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    qrlPpn: TQRLabel;
    q: TQRLabel;
    QRLabel6: TQRLabel;
    qrlGrandTOtal: TQRLabel;
    qrlTempatTanggal: TQRLabel;
    qrlNamaper3: TQRLabel;
    qrlNamaPimpinan: TQRLabel;
    qrlJabatan: TQRLabel;
    QRShape9: TQRShape;
    QRLabel5: TQRLabel;
    qrBank: TQRLabel;
    qrCab: TQRLabel;
    qrAn: TQRLabel;
    qrNorek: TQRLabel;
    mmTemp: TMemo;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    qrmTerbilang: TQRMemo;
    QRShape19: TQRShape;
    procedure qrpprintBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrpprintNeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
    i, y : integer;
    IsShowDetail : boolean;
    persen : real;
    arr_Print : AR_print;
    ardetJual : aR_data_jual_detail;
//    dtMstJual : TR_data_jual_master;
    procedure preparedPrint;
  public
    { Public declarations }
    procedure Execute(dtMst : TR_invoice_master; aIsShowDetail : boolean = true);
  end;

var
  frmInvoice2printFmt2: TfrmInvoice2printFmt2;

implementation

uses
  UFinance, StrUtils, Math;

{$R *.dfm}

{ TfrmInvoice2printFmt2 }

procedure TfrmInvoice2printFmt2.Execute(dtMst: TR_invoice_master; aIsShowDetail : boolean = true);
var arDet :aR_invoice_detail;
    tmpNoPO, seqin, file_foto : string;
    company : TR_company_info;
    idx2, idx1, no, idx, m, n, z : integer;
    arDtMstJual : ar_data_jual_master;
    uraian:string;
    subtotal , ppn : real;
    dtJual : tr_data_jual_master;
    pct, hrg : real;
begin
  no := 1; IsShowDetail := AIsShowDetail;
  Company:=Get_company_info;

  imgLogo.Visible := True; imgLogo.Picture.Bitmap.FreeImage;
  file_foto := GetAppPath + 'Images\' + Company.logo;
  imgLogo.Visible := True;
  if FileExists(file_foto) then
    imgLogo.Picture.LoadFromFile(file_foto);

  qrlNAmaPer.Caption   := Company.nama;
  qrlAlamatPer.Caption := Company.alamat;
  qrlPhone.Caption  := ifthen(Company.no_telp<>'', 'Phone : '+Company.no_telp)+ ifthen(
    Company.email<>'', ' Email: '+Company.email) ;
   qrlNamaper3.Caption   := Company.nama;
  qrlNamaPimpinan.Caption   := Company.png_jwb1;
  qrlJabatan.Caption   := Company.jabatan1;

  qrlTempatTanggal.Caption  := 'Bandung, '+FormatDateTime('dd MMMM yyyy', DtMst.tanggal);

  if dtMst.bank_seq > 0 then begin
    qrBank.Caption  := EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 1);
    qrCab.Caption   := 'Cab. '+ EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 2);
    qrNorek.Caption := 'No. Rek : '+EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 3);
    qrAn.Caption    := 'a.n. '+EkstrakString(ListBankGlobal.Values[inttostr(dtMst.bank_seq)], '#', 4);
  end;

  tmpNoPO := '';
  subtotal := 0;
  ppn      := 0;

  idx:=0;
  SetLength(arr_print, 1);
  mmTemp.Lines.Text  := dtMst.deskripsi;
  for z := 0 to mmTemp.Lines.Count-1 do begin
    if z = 0 then arr_print[idx].uraian := mmTemp.Lines[Z]
    else begin
      if mmTemp.Lines[Z] <> '' then BEGIN
        inc(idx);
        SetLength(arr_print, idx+1);
        arr_print[idx].uraian := mmTemp.Lines[Z];
      END;
    end;
  end;
  arr_print[idx].nilaipo     := '';
  arr_print[idx].qty         := '';
  arr_print[idx].total       := '';
  arr_Print[idx].isHeader    := 'T';
  inc(idx);

  arDet := Get_Arr_invoice_detail(dtMst.seq);
  if IsShowDetail then begin
    for z := 0 to length(arDet)-1 do begin
        seqin := seqin + ifthen(seqin<>'', ',') + inttostr(arDet[z].jual_seq);
      dtJual := Get_data_jual_master(ardet[z].jual_seq);
      tmpNoPO:= tmpNoPO + ifthen(tmpNoPO<>'', ',') + dtJual.nomor_po;

      if dtJual.total = ardet[z].dibayar then begin
        ppn := ppn + dtJual.ppn;
        subtotal := subtotal + dtJual.subtotal;
      end else begin
        if dtJual.ppn > 0 then begin
          ppn := ppn + ( arDet[z].dibayar*0.1);
          subtotal := subtotal + (arDet[z].dibayar);
          //ppn := ppn + ( (arDet[z].dibayar/1.1)*0.1);
          //subtotal := subtotal + (arDet[z].dibayar/1.1);
        end else subtotal := subtotal + (arDet[z].dibayar);
      end;

      pct := (ardet[z].dibayar/dtJual.total);
      ardetJual := Get_Arr_data_jual_detail(ardet[z].jual_seq);
      for n := 0 to length(ardetJual)-1 do begin
        SetLength(arr_print, idx+1);
        arr_print[idx].No          := IntToStr(no)+'.';
        inc(no);

        mmTemp.Lines.Text  := ardetJual[n].spesifikasi +' - '+ ardetJual[n].site_name;

        idx1 := idx;
        for m := 0 to mmTemp.Lines.Count-1 do begin
          if m = 0 then arr_print[idx].uraian := mmTemp.Lines[m]
          else begin
            inc(idx);
            SetLength(arr_print, idx+1);
            arr_print[idx].uraian := mmTemp.Lines[m];
          end;
        end;

        arr_print[idx1].nilaipo     := FloatToStrFmt2(ardetJual[n].harga{+ardetJual[n].ppn});
        arr_print[idx1].qty         := FloatToStrFmt2(ardetJual[n].qty);
        hrg := (ardetJual[n].harga{+ardetJual[n].PPN})*ardetJual[n].qty;
        arr_print[idx1].total       := FloatToStrFmt2(hrg);//*pct);
        arr_Print[idx1].isHeader    := '';
        arr_Print[idx1].uraian      := ' - '+arr_Print[idx1].uraian;
        idx := Max(idx, idx1);
        inc(idx);
      end;
    end;
  end else begin
    for z := 0 to length(arDet)-1 do begin
      seqin := seqin + ifthen(seqin<>'', ',') + inttostr(arDet[z].jual_seq);
      dtJual := Get_data_jual_master(ardet[z].jual_seq);
      tmpNoPO:= tmpNoPO + ifthen(tmpNoPO<>'', ',') + dtJual.nomor_po;

      if dtJual.total = ardet[z].dibayar then begin
        ppn := ppn + dtJual.ppn;
        subtotal := subtotal + dtJual.subtotal;
      end else begin
        if dtJual.ppn > 0 then begin
          ppn := ppn + ( arDet[z].dibayar*0.1);
          subtotal := subtotal + (arDet[z].dibayar);
        end else subtotal := subtotal + (arDet[z].dibayar);
      end;
    end;

    arr_print[idx-1].nilaipo     := FloatToStrFmt2(subtotal);
    arr_print[idx-1].qty         := '-';
    arr_print[idx-1].total       := FloatToStrFmt2(subtotal);
    arr_Print[idx-1].isHeader    := 'T';
  end;


//  arDtMstJual := Get_arr_data_jual_master(0,0, seqin);
//  for z := 0 to Length(arDtMstJual)-1 do begin
//    tmpNoPO:= tmpNoPO + ifthen(tmpNoPO<>'', ',') + ardtmstJual[z].nomor_po;
////    uraian  := EkstrakString(ListJobGlobal.
////               Values[IntToStr(ardtmstJual[z].job_seq)],'#',2)+'-'+
////               EkstrakString(ListJobGlobal.Values[IntToStr(ardtmstJual[z].job_seq)],'#',6);
//
//  end;

//  arDtMstJual := Get_Arr_data_jual_master(0, 0, seqIn);

//  dtMstJual := Get_data_jual_master(dtDet.jual_seq);
  ardetJual := Get_Arr_data_jual_detail(0, seqin);
//  persen := (dtMst.total / dtMstJual.total) * 100;

  qrlNoTrans.Caption        := dtMst.nomor;
  if IsShowDetail then
    qrlNoPO.Caption         := ': '+tmpNoPO//dtMstJual.nomor_po;
  else qrlNoPO.Caption      := ': - ';
  qrlCard.Caption           := EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',2);
  qrAlamat.Lines.Text       := EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',7)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',8)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',9)+' '+EkstrakString(ListCardGlobal.Values[IntToStr(dtMst.card_seq)],
    '#',11);


//  if dtMst.deskripsi <> '' then
    mmTemp.Lines.Text  := dtMst.deskripsi;
//  else mmTemp.Lines.Text  := uraian;
//  if dtMstJual.ppn <> 0 then begin
//    qrlgrandTotal.Caption     := FloatToStrFmt(dtMst.total);
//    qrlPpn.Caption            := FloatToStrFmt((dtMst.total*10)/100);
//    qrltotal.Caption          := FloatToStrFmt(dtMst.total-((dtMst.total*10)/100));
//  end else begin
    qrltotal.Caption      := FloatToStrFmt2(subtotal);
    qrlPpn.Caption        := FloatToStrFmt2(ppn);
    qrlgrandTotal.Caption := FloatToStrFmt2(subtotal+ppn);
//  end;

  qrmTerbilang.lines.text := 'Terbilang : ---'+ MoneyToIndonesianText(FloatToStr(subtotal+ppn))+'---';

//  preparedPrint;
  frmInvoice2printFmt2.qrpprint.PreviewModal;
end;

procedure TfrmInvoice2printFmt2.preparedPrint;
var idx, idx1,j, z : integer;
    hrg : real;
begin
  idx:=0;
  SetLength(arr_print, 1);

//  mmTemp.Lines.Text  := EkstrakString(ListJobGlobal.
//                    Values[IntToStr(dtMstJual.job_seq)],'#',2)
//                    +'-'+
//                    EkstrakString(ListJobGlobal.Values[IntToStr(
//                                  dtMstJual.job_seq)],'#',6);

  for z := 0 to mmTemp.Lines.Count-1 do begin
    if z = 0 then arr_print[idx].uraian := mmTemp.Lines[Z]
    else begin
      if mmTemp.Lines[Z] <> '' then BEGIN
        inc(idx);
        SetLength(arr_print, idx+1);
        arr_print[idx].uraian := mmTemp.Lines[Z];
      END;
    end;
  end;

  arr_print[idx].nilaipo     := '';
  arr_print[idx].qty         := '';
  arr_print[idx].total       := '';
  arr_Print[idx].isHeader    := 'T';
  inc(idx);
  for j := 0 to Length(ardetJual)-1 do begin
    SetLength(arr_print, idx+1);
    arr_print[idx].No          := IntToStr(J+1)+'.';

    mmTemp.Lines.Text  := ardetJual[j].spesifikasi;

    idx1 := idx;
    for z := 0 to mmTemp.Lines.Count-1 do begin
      if z = 0 then arr_print[idx].uraian := mmTemp.Lines[z]
      else begin
        SetLength(arr_print, idx+1);
        arr_print[idx].uraian := mmTemp.Lines[z];
        inc(idx);
      end;
    end;
//    hrg := ((ardetJual[j].harga+ardetJual[j].PPN)*ardetJual[j].qty);//*persen)/100;
//    arr_print[idx1].nilaipo     := FloatToStrFmt((ardetJual[j].harga+ardetJual[j].PPN)*ardetJual[
//j].qty);
    arr_print[idx1].nilaipo     := FloatToStrFmt2(ardetJual[j].harga);//+ardetJual[j].PPN);
    arr_print[idx1].qty         := FloatToStrFmt2(ardetJual[j].qty);
//    arr_print[idx1].total       := FloatToStrFmt(hrg);             //+ardetJual[j].PPN
    arr_print[idx1].total       := FloatToStrFmt2((ardetJual[j].harga)*ardetJual[j].qty);
    arr_Print[idx1].isHeader    := '';
    arr_Print[idx1].uraian      := ' - '+arr_Print[idx1].uraian;
    inc(idx);
  end;
end;

procedure TfrmInvoice2printFmt2.qrpprintBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  i := 0; y := 0;
end;

procedure TfrmInvoice2printFmt2.qrpprintNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := i < Length(arr_print);
  if MoreData then begin
//    qrlNo.Caption       := arr_print[i].No;
    qrlDesk.Caption     := arr_print[i].uraian;
    qrlNilaiPO.Caption  := arr_print[i].nilaipo;
    qrlQty.Caption      := arr_print[i].Qty;
    qrlTot.Caption      := arr_print[i].total;
    inc(i);
  end;

end;

end.


