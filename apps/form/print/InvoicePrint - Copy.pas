unit InvoicePrint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, URecord, UConst, UEngine, QRPrntr,
  UGeneral, USystemMenu;

type
  TR_AlatBayar = record
    tipe_bayar , KasBank , no_rek, no_dok, tgl_cair, jumlah, AIsCaption : string;
  end;
  AR_AlatBayar = array of TR_AlatBayar;
  TfrmPrintInvoice = class(TForm)
    qrpPrint: TQuickRep;
    PageHeaderBand1: TQRBand;
    qrlNAmaPer: TQRLabel;
    QRShape1: TQRShape;
    DetailBand1: TQRBand;
    qrlNoDok: TQRLabel;
    qrlTglCair: TQRLabel;
    qrlRp: TQRLabel;
    qrlDana: TQRLabel;
    qrlNmKasBank: TQRLabel;
    qrlTipe: TQRLabel;
    qrlNamaPer2: TQRBand;
    QRShape4: TQRShape;
    qrlDeskripsiPer: TQRLabel;
    qrlAlamatPer: TQRLabel;
    qrlPhone: TQRLabel;
    QRLabel1: TQRLabel;
    qrlCard: TQRLabel;
    QRLabel3: TQRLabel;
    qrlNoTrans: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    qrlTerbilang: TQRLabel;
    QRLabel5: TQRLabel;
    mmKet: TQRMemo;
    QRLabel2: TQRLabel;
    qrlJml: TQRLabel;
    qrlTempatTanggal: TQRLabel;
    qrlNamaper3: TQRLabel;
    qrlNamaPimpinan: TQRLabel;
    qrlJabatan: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    imgLogo: TQRImage;
  private
    { Private declarations }
    //arr_ALatBayar : AR_AlatBayar;
    Company :  TR_company_info;
    SeqMst, i, y : integer;
    Total : real;
    procedure preparedPrint;
  public
    { Public declarations }
    procedure Execute(DtMst : TR_invoice_master);
  end;

var
  frmPrintInvoice: TfrmPrintInvoice;

implementation

uses
  UTransaksi, UFinance, StrUtils, UCreateForm, UDummy, UReport, OracleConnection;

{$R *.dfm}

{ TfrmPrintSetoranModal }

procedure TfrmPrintInvoice.Execute(DtMst : TR_invoice_master);
var DtDet : TR_invoice_detail;
    dtJual : Tr_data_jual_master;
    file_foto : string;
begin
  Company:=Get_company_info;

  imgLogo.Visible := True; imgLogo.Picture.Bitmap.FreeImage;
  file_foto := GetAppPath + 'Images\' + Company.logo;
  imgLogo.Visible := True;
  if FileExists(file_foto) then
    imgLogo.Picture.LoadFromFile(file_foto);

  qrlNAmaPer.Caption   := Company.nama;
  qrlAlamatPer.Caption := Company.alamat;
  qrlPhone.Caption  := ifthen(Company.no_telp<>'', 'Phone : '+Company.no_telp)+ ifthen(
    Company.email<>'', ' Email: '+Company.email) ;
   qrlNamaper3.Caption   := Company.nama;
  qrlNamaPimpinan.Caption   := Company.png_jwb1;
  qrlJabatan.Caption   := Company.jabatan1;

  qrlTempatTanggal.Caption  := 'Bandung, '+FormatDateTime('dd MMMM yyyy', DtMst.tanggal);

  qrlNoTrans.Caption  := DtMst.nomor_kwt;
  qrlCard.Caption     := EkstrakString(ListCardGlobal.Values[IntToStr(DtMst.card_seq)],'#',2);
  qrlTerbilang.Caption    :=  MoneyToIndonesianText(FloatToStr(DtMst.total));
  qrlJml.Caption:= 'Rp. '+FloatToStrFmt(DtMst.total)+',-';

  DtDet := Get_invoice_detail(DtMst.seq);
  dtJual := Get_data_jual_master(DtDet.jual_seq);

//  if DtMst.deskripsi <> '' then begin
    mmket.lines.text := DtMst.deskripsi;
//  end else begin
//    mmket.lines.text := EkstrakString(ListJobGlobal.Values[IntToStr(dtJual.job_seq)],'#',2)+' - '+
//                        EkstrakString(ListJobGlobal.Values[IntToStr(dtJual.job_seq)],'#',6);
//  end;

//  preparedPrint;
  qrpPrint.Previewmodal;
end;

procedure TfrmPrintInvoice.preparedPrint;
begin

end;

end.

