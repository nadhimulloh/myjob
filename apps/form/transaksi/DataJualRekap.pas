unit DataJualRekap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls;

type
  TfrmRekapDataJual = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnHapus: TAdvGlowButton;
    btnTambah: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label19: TLabel;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    btnDetail: TAdvGlowButton;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    cmbStat: TComboBox;
    Label3: TLabel;
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapSelectionChanged(Sender: TObject; ALeft, ATop, ARight,
      ABottom: Integer);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnDetailClick(Sender: TObject);
  private
    seqCard,AMenuid : integer;
    datenow: TDate;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
    function isDelete(alasanhapus: string) :boolean;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmRekapDataJual: TfrmRekapDataJual;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, StrUtils, LOV, DataJualInput;

{$R *.dfm}

const
  ColNo         = 0;
  ColTanggal    = 1;
  ColNomor      = 2;
  colCUst       = 3;
  colUnit       = 4;
  colTempo      = 5;
  ColSubtotal   = 6;
  ColPpn        = 7;
  ColTotal      = 8;
  ColKet        = 9;
  colUserId     = 10;
  colTglInput   = 11;
  ColSeq        = 12;
  ColTerbayar   = 13;


procedure TfrmRekapDataJual.arrangecolsize;
begin
  asgRekap.AutoNumberCol(colno);
  asgrekap.cells[colTempo, asgrekap.rowcount-1]:= 'Total';
  asgrekap.floatingfooter.columncalc[colsubtotal] :=  acsum;
  asgrekap.floatingfooter.columncalc[colppn]      :=  acsum;
  asgrekap.floatingfooter.columncalc[coltotal]    :=  acsum;
  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
    asgRekap.ColWidths[ColTerbayar]:=0;
  end;
end;

procedure TfrmRekapDataJual.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  btnEdit.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
  btnHapus.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
  btnDetail.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
//  btnCetak.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
end;

procedure TfrmRekapDataJual.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow > 0) then btndetail.Click;
end;

procedure TfrmRekapDataJual.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [ColSubtotal,ColPpn,ColTotal,colSeq, colNo] then HAlign := taRightJustify;
end;

procedure TfrmRekapDataJual.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0) and (arow < asgrekap.rowcount - 1) then begin
//    if (asgRekap.floats[ColTerbayar, ARow] <> asgRekap.floats[ColTotal, ARow]) then
//      AFont.Color := clRed
//    else AFont.Color := clBlack;
  end;
end;

procedure TfrmRekapDataJual.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0) and (arow < asgrekap.rowcount - 1) then begin
//    if (asgRekap.floats[ColTerbayar, ARow] <> asgRekap.floats[ColTotal, ARow]) then
//      AFont.Color := clRed
//    else AFont.Color := clBlack;
  end;
end;

procedure TfrmRekapDataJual.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, ColSubtotal,ColPpn,ColTotal,colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol in [colseq] then FloatFormat    := '%.0f'
    else FloatFormat := '%.2n'
  end;
end;

procedure TfrmRekapDataJual.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapDataJual.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapDataJual.asgRekapSelectionChanged(Sender: TObject;
  ALeft, ATop, ARight, ABottom: Integer);
begin
  asgRekap.OnClickCell(Self,asgRekap.Row,asgRekap.Col);
end;

procedure TfrmRekapDataJual.btnHapusClick(Sender: TObject);
var Tanggal : tdate;
    alasanhapus : string;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;                       
  end;
  if asgRekap.Ints[ColSeq, asgRekap.Row] <> 0 then begin

//    if asgRekap.floats[Colterbayar, asgRekap.Row] = asgRekap.floats[Coltotal, asgRekap.Row] then begin
//      inform('Data tidak bisa dihapus, karena sudah close.');
//      exit;
//    end;
    if asgRekap.floats[ColTerbayar, asgRekap.Row] > 0 then begin
      inform('Data tidak bisa dihapus, karena sudah ada invoice.');
      exit;
    end;

    if Confirmed('No. Transaksi "'+asgRekap.Cells[ColNomor, asgRekap.Row]+'" akan dihapus ?') then begin
      alasanhapus := TrimAllString(UGeneral.InputNote);
      if alasanhapus = '' then begin
        Inform('Catatan hapus tidak boleh kosong.');
        Exit;
      end;
      if isDelete(alasanhapus) then begin
        inform(MSG_SUCCESS_DELETING);
        if AsgRekap.RowCount > 3  then AsgRekap.RemoveRows(AsgRekap.Row,1)
        else AsgRekap.ClearRows(AsgRekap.Row,1);
        Arrangecolsize;
        AsgRekap.Col := colNomor;
        AsgRekap.Row := 1;
      end else Inform(MSG_UNSUCCESS_DELETING);
    end;
  end;
end;

procedure TfrmRekapDataJual.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Rekap Data Jual');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 3;
  arrangecolsize;
end;

procedure TfrmRekapDataJual.btnCustClick(Sender: TObject);
begin
  seqCard := 0 ;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(
    TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmRekapDataJual.btnDetailClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    Application.CreateForm(TfrmInputDataJual, frmInputDataJual);
    frmInputDataJual.OnClose := frmMainMenu.ChildFormSingle;
    frmInputDataJual.execute(asgRekap.Ints[colseq, asgRekap.Row], false);
  end;
end;

procedure TfrmRekapDataJual.btnEditClick(Sender: TObject);
begin
  if not BisaEdit(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
//    if asgRekap.floats[Colterbayar, asgRekap.Row] = asgRekap.floats[Coltotal, asgRekap.Row] then begin
//      inform('Data tidak bisa diedit, karena sudah close.');
//      exit;
//    end;
//    if asgRekap.floats[ColTerbayar, asgRekap.Row] > 0 then begin
//      inform('Data tidak bisa diedit, karena sudah ada pembayaran.');
//      exit;
//    end;

    Application.CreateForm(TfrmInputDataJual, frmInputDataJual);
    frmInputDataJual.OnClose := frmMainMenu.ChildFormSingle;
    if frmInputDataJual.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then btnLoad.Click;
  end;
end;

procedure TfrmRekapDataJual.btnLoadClick(Sender: TObject);
begin
  loaddata;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmRekapDataJual.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapDataJual.btnTambahClick(Sender: TObject);
begin
  if not BisaNambah(AmenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  Application.CreateForm(TfrmInputDataJual, frmInputDataJual);
  frmInputDataJual.OnClose := frmMainMenu.ChildFormSingle;
  if (frmInputDataJual.execute(0,false)) or (frmInputDataJual.statSimpan = true)  then btnLoad.Click;
end;

procedure TfrmRekapDataJual.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapDataJual.Execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  datenow  :=ServerNow;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmRekapDataJual.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmRekapDataJual.initform;
begin
  dtpAwal.Date    := datenow;
  dtpAkhir.Date    := datenow;
  txtCust.Text    := '';
  seqCard         := 0;
  cmbDes.ItemIndex              := 0;
  cmbStat.ItemIndex             := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

function TfrmRekapDataJual.isDelete(alasanhapus: string): boolean;
var MstTmp  : TR_data_jual_master;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
    SeqMst, SeqDummy : Integer;
begin
  myConnection.BeginSQL;
  try
    SeqMst   := asgRekap.Ints[ColSeq, asgRekap.Row];
    MstTmp   := Get_data_jual_master(seqMst);
    SeqDummy := Save_Dummy_data_jual_master(MstTmp);

    Save_Dummy_data_jual_detail(SeqDummy,SeqMst);
    Delete_data_jual_detail(MstTmp.Seq);
    Delete_data_jual_master(MstTmp.Seq);

    Ket.IsEdit    := False;
    Ket.NamaTrans := 'Data Jual';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := EkstrakString(ListCardGlobal.Values[inttostr(MstTmp.card_seq)], '#', 2);
    Ket.NoTrans   := MstTmp.nomor_po;
    Ket.TglTrans  := MstTmp.Tanggal;

    Log.Nama_Tabel := 'data_jual_master';
    Log.Tanggal    := ServerNow;
    Log.Seq_Dummy  := SeqDummy;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := 0;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := alasanhapus;
    Save_Log_File(Log);

    Result := True;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

procedure TfrmRekapDataJual.loaddata;
var buffer      : _recordset;
    i,Row       : Integer;
    unitTmp,Filter,  Divisi : string;
    Periode : TR_FilterPeriode;
begin
  asgRekap.RowCount := 3;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  periode.FPeriodeAwal      := dtpAwal.Date;
  periode.FPeriodeAkhir     := dtpAkhir.Date;
  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;

  Divisi := '';
  case cmbDes.ItemIndex of
    1 : Divisi := TIPE_DIVISI_LOGISTIC;
    2 : Divisi := TIPE_DIVISI_TRADING;
    3 : Divisi := TIPE_DIVISI_CONSTRUCTION;
  end;

  case cmbStat.ItemIndex of
    1 : filter := filter + ' and terbayar <> total ';
    2 : filter := filter + ' and terbayar = total ';
  end;


//  filter := filter + ifthen(divisi<>'','and job_seq in (select seq from master_job where unit_bisnis = '+formatsqlstring(divisi)+')');

  //  (seq = 0)  (tanggal = 1)  (nomor_po = 2)  (job_seq = 3)  (card_seq = 4)  (subtotal = 5)  (ppn = 6)  (total = 7)  (keterangan = 8)
  //  (tempo_byr = 9)  (user_id = 10)  (tgl_input = 11)  (user_edit = 12)  (tgl_edit = 13)

  Load_data_jual_master(buffer,periode,0,seqCard, divisi, Filter);
  for i := 0 to buffer.RecordCount - 1 do begin
    AdvProgressBar1.Position := round((i+1)/buffer.RecordCount*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -2;
    Asgrekap.dates[ColTanggal  , row] := BufferToDateTime(buffer.Fields[1].Value)  ;
    Asgrekap.cells[ColNomor    , row] := BufferToString(buffer.Fields[2].Value)  ;
    Asgrekap.cells[colCUst     , row] := EkstrakString(ListCardGlobal.Values[buffertoString(buffer.Fields[4].Value)],'#',2);

    unitTmp := EkstrakString(ListJobGlobal.Values[buffertoString(buffer.Fields[3].Value)],'#',3);
    if unitTmp = TIPE_DIVISI_LOGISTIC then
      Asgrekap.cells[colUnit     , row] :=TIPE_DIVISI_LOGISTIC_text
    else if unitTmp = TIPE_DIVISI_TRADING then
      Asgrekap.cells[colUnit     , row] :=TIPE_DIVISI_TRADING_text
    else if unitTmp = TIPE_DIVISI_CONSTRUCTION then
      Asgrekap.cells[colUnit     , row] :=TIPE_DIVISI_CONSTRUCTION_text;


    Asgrekap.cells[colTempo     , row] := BufferTostring(buffer.Fields[9].Value)+' hari';
    Asgrekap.Floats[colSubtotal   , row] := BufferToFloat(buffer.Fields[5].Value);
    Asgrekap.Floats[colPpn        , row] := BufferToFloat(buffer.Fields[6].Value);
    Asgrekap.Floats[colTotal      , row] := BufferToFloat(buffer.Fields[7].Value);
    Asgrekap.cells[colKet      , row] := BufferToString(buffer.Fields[8].Value);
    Asgrekap.cells[colUserId   , row] := BufferToString(buffer.Fields[10].Value);
    Asgrekap.dates[colTglInput  , row] := BufferToDateTime(buffer.Fields[11].Value);
    Asgrekap.ints[colSeq        , row] := BufferToINteger(buffer.Fields[0].Value);
    Asgrekap.Floats[colterbayar, row] := BufferToFloat(buffer.Fields[14].Value);

    buffer.MoveNext;
  end;
  buffer.Close;
  AdvProgressBar1.Hide;
  arrangecolsize;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapDataJual.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 15;
  asgRekap.RowCount := 3;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;

  arrangecolsize;
end;

procedure TfrmRekapDataJual.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

end.
