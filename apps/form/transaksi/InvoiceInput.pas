unit InvoiceInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker, Grids, AdvObj, BaseGrid, AdvGrid;

type
  TfrmInputInvoice = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Label22: TLabel;
    Label23: TLabel;
    txtCust: TAdvEdit;
    btnCust: TAdvGlowButton;
    dtpTanggal: TAdvDateTimePicker;
    mmKet: TMemo;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    asgInput: TAdvStringGrid;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label11: TLabel;
    txtSubTotal: TAdvEdit;
    txtPPN: TAdvEdit;
    txtTotal: TAdvEdit;
    chbPPn: TAdvOfficeCheckBox;
    Label2: TLabel;
    txtJumlah: TAdvEdit;
    Label9: TLabel;
    sttNo: TStaticText;
    mmDesk: TMemo;
    Label3: TLabel;
    Label7: TLabel;
    txtNoInv: TAdvEdit;
    Label8: TLabel;
    lblNoInv: TLabel;
    txtNoKwitansi: TAdvEdit;
    lblNoKw: TLabel;
    GroupBox3: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    lblNoBast: TLabel;
    lblNoBaut: TLabel;
    txtNoBaut: TAdvEdit;
    txtNoBast: TAdvEdit;
    Label12: TLabel;
    txtBank: TAdvEdit;
    btnBank: TAdvGlowButton;
    procedure btnBatalClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure asgInputAutoDeleteRow(Sender: TObject; ARow: Integer);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgInputGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgInputGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgInputMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgInputMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chbPPnClick(Sender: TObject);
    procedure asgInputAutoAddRow(Sender: TObject; ARow: Integer);
    procedure asgInputCanAddRow(Sender: TObject; var CanAdd: Boolean);
    procedure asgInputCheckBoxChange(Sender: TObject; ACol, ARow: Integer;
      State: Boolean);
    procedure FormResize(Sender: TObject);
    procedure btnBankClick(Sender: TObject);
  private
    { Private declarations }
    normal_height, seqCard, seqBank, SeqMst : integer;
    browseMode, EditMode : Boolean;
    dateNow : TDateTime;
    procedure setgrid;
    procedure arrangecolsize;
    function isValid : Boolean;
    function isSaved : Boolean;
    function isSavedEdit : Boolean;
    procedure loaddata;
    procedure InitForm;
    procedure updateTotal;
    procedure loaddatajual;
    procedure setGrb;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(ASeqMst : integer; isEdit: boolean):boolean;
  end;

var
  frmInputInvoice: TfrmInputInvoice;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi, UDummy, ADOInt;

const
  colNo          = 0;
  colNomor       = 1;
  colInv         = 2;
  colTgl         = 3;
  colTotal       = 4;
  colTerbayar    = 5;
  colSisa        = 6;
  colCheck       = 7;
  colDibayar     = 8;
  colketerangan  = 9;
  colSeqJual     = 10;

{$R *.dfm}

procedure TfrmInputInvoice.btnCustClick(Sender: TObject);
var noBast, noBaut, noInv, nokw: string;

begin
//  GroupBox2.Top := GroupBox3.Top;
//  setGrb;
  setgrid;
  txtCust.Text :=''; txtJumlah.IntValue := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.ExecuteCard('', botrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    loaddatajual;
    noInv := GetNextNumber(dtpTanggal.Date, TRANS_INVOICE, seqCard);
    nokw  := GetNextNumber(dtpTanggal.Date, TRANS_KWITANSI, seqCard);
    noBast := GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
    noBaut := GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);

    txtNoInv.Text      := EkstrakString( noInv , '/', 1) ;
    txtNoKwitansi.Text := EkstrakString( noKw, '/', 1) ;
    txtNoBast.Text     := EkstrakString( noBast , '/', 1);
    txtNoBaut.Text     := EkstrakString( noBaut, '/', 1);

    lblNoInv.Caption   := Copy( noInv, 4, Length(noInv)-3);
    lblNoKw.Caption    := Copy( nokw, 4, Length(nokw)-3);
    lblNoBast.Caption  := Copy( noBast, 4, Length(noBast)-3);
    lblNoBaut.Caption  := Copy( noBaut, 4, Length(noBaut)-3);

//    if EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)], '#', 26) = TRUE_STRING then begin
//    end else begin
//      txtNoBast.Text     := '0';
//      txtNoBaut.Text     := '0';
//      lblNoBast.Caption  := '';
//      lblNoBaut.Caption  := '';
//    end;
//    sttNo.caption := GetNextNumber(dtpTanggal.Date, TRANS_INVOICE, seqCard);
  end;
  setGrb;
  updateTotal;
  arrangecolsize;
end;

procedure TfrmInputInvoice.btnSimpanClick(Sender: TObject);
begin
  btnSimpan.Enabled := False;
  if (browsemode) and (not editmode) then begin
//    data := Get_faktur_beli_master(seqMst);
//    Application.CreateForm(TfrmPrintPenerimaanSparepart, frmPrintPenerimaanSparepart);
//    frmPrintPenerimaanSparepart.OnClose := frmMainMenu.ChildFormSingle;
//    frmPrintPenerimaanSparepart.Executes(Data);
  end else if (not (browseMode xor editMode)) and (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if editmode then begin
      if isSavedEdit then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else Inform(MSG_UNSUCCESS_UPDATE);
    end else begin
      if isSaved then begin
//        if Confirmed(MSG_PRINT) then begin
//          data := Get_faktur_beli_master(SeqMst);
//          Application.CreateForm(TfrmPrintPenerimaanSparepart, frmPrintPenerimaanSparepart);
//          frmPrintPenerimaanSparepart.OnClose := frmMainMenu.ChildFormSingle;
//          frmPrintPenerimaanSparepart.Executes(Data);
//        end;
        if Confirmed(MSG_ADD_DATA) then begin
          initform; Setgrid; statSimpan := true;
//          txtNomor.SetFocus;
        end else ModalResult := mrOk;
      end else begin
        ActiveControl := btnBatal;
      end;
    end;
  end;
  btnSimpan.Enabled := True;
end;

procedure TfrmInputInvoice.chbPPnClick(Sender: TObject);
begin
  updateTotal;
end;

function TfrmInputInvoice.execute(ASeqMst: integer; isEdit: boolean): boolean;
begin
  normal_height := groupbox2.height;
  dateNow    := ServerNow(True);
  browseMode := (AseqMst <> 0); editMode := isEdit;
  initform;
  SeqMst      := ASeqMst;
  if editMode then begin
    Self.Caption := 'Edit Invoice';
    btnSimpan.Caption := '&Simpan';
  end else if browsemode then begin
    Self.Caption := 'Detail Invoice';
    btnSimpan.Caption := '&Simpan';
    btnSimpan.enabled := false;
  end else begin
    Self.Caption := 'Tambah Invoice';
    btnSimpan.Caption := '&Simpan';
  end;
  setgrid;
  if browseMode then loaddata;
  run(self);
  result := ModalResult = mrOk;
end;

procedure TfrmInputInvoice.FormResize(Sender: TObject);
begin
  if EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#', 26) = TRUE_STRING then begin
    normal_height := groupbox2.height
  end else begin
    normal_height := groupbox2.height - groupbox3.height
  end;
end;

procedure TfrmInputInvoice.InitForm;
var noInv, nokw, noBast, noBaut : string;
begin
  txtSetAllEmpty(self);
  dtpTanggal.Date := Datenow;
  seqCard   := 0; seqBank:= 0;
  chbPPn.Checked:= False;

  noInv := GetNextNumber(dtpTanggal.Date, TRANS_INVOICE, seqCard);
  nokw  := GetNextNumber(dtpTanggal.Date, TRANS_KWITANSI, seqCard);
  noBast := GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
  noBaut := GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);

  txtNoInv.Text      := EkstrakString( noInv , '/', 1);
  txtNoKwitansi.Text := EkstrakString( noKw, '/', 1);
  txtNoBast.Text     := EkstrakString( noBast , '/', 1);
  txtNoBaut.Text     := EkstrakString( noBaut, '/', 1);

  lblNoInv.Caption   := Copy( noInv, 4, Length(noInv)-3);
  lblNoKw.Caption    := Copy( nokw, 4, Length(nokw)-3);
  lblNoBast.Caption  := Copy( noBast, 4, Length(noBast)-3);
  lblNoBaut.Caption  := Copy( noBaut, 4, Length(noBaut)-3);

  txtNoInv.readonly       := (browseMode xor editMode);
  txtNoKwitansi.readonly  := (browseMode xor editMode);
  txtNoBast.readonly      := (browseMode xor editMode);
  txtNoBaut.readonly      := (browseMode xor editMode);

  setGrb;
  
  dtpTanggal.Enabled :=  not (browseMode xor editMode);
  btnCust.Enabled    :=  not (browseMode);
  btnBank.Enabled    :=  not (browseMode xor editMode);
  mmdesk.readonly    := (browseMode xor editMode);
  mmKet.readonly     := (browseMode xor editMode);
  txtJumlah.readonly := (browseMode xor editMode);
end;

function TfrmInputInvoice.isSaved: Boolean;
var dtMst : TR_invoice_master;
    trDet : TR_invoice_detail;
    row, i : integer;
    state : boolean;
begin
  //save detail faktur beli
  myConnection.BeginSQL;
  try
    dtMst.tanggal    := dtpTanggal.Date;
    dtMst.keterangan := TrimAllString(mmKet.Lines.Text);
    dtMst.card_seq   := seqCard;
    dtMst.total      := asgInput.floats[colDibayar, asgInput.RowCount-1];
    dtmst.bank_seq   := seqBank ;

    if EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#', 26) = TRUE_STRING then begin
//      dtMst.nomor_baut := GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);
//      dtMst.nomor_bast := GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
      dtMst.nomor_baut := TrimAllString(txtNoBaut.Text)+lblNoBaut.Caption;//GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);
      dtMst.nomor_bast := TrimAllString(txtNoBast.Text)+lblNoBast.Caption;//GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
    end else begin
      dtMst.nomor_baut := '';//GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);
      dtMst.nomor_bast := '';//GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
    end;

    dtMst.nomor      := TrimAllString(txtNoInv.Text)+lblNoInv.Caption;     //GetNextNumber(dtpTanggal.Date, TRANS_INVOICE, seqCard);//sttno.caption;
    dtMst.nomor_kwt  := TrimAllString(txtNoKwitansi.Text)+lblNoKw.Caption; //GetNextNumber(dtpTanggal.Date, TRANS_KWITANSI, seqCard);
    dtMst.deskripsi  := TrimAllString(mmDesk.Lines.Text);

    SeqMst := Save_invoice_master(dtMst);

    if Copy(ErrorSave, 1, 9) = ERR_LOCKED_TABLE then begin
      Result := False;
      Inform(MSG_SERVER_BUSY);
      Exit;
    end;
    if SeqMst = 0 then begin
      Result := False;
      Inform(MSG_UNSUCCESS_SAVING);
      Exit;
    end;
    for i := 1 to asgInput.RowCount-2 do begin
      asgInput.GetCheckBoxState (colCheck,i,state);
      if state then begin
        trDet.seq:= CreateNewSeq('seq', 'invoice_detail');
        trDet.master_seq  := SeqMst;
        trDet.jual_seq    := asgInput.Ints[colSeqJual, i];
        trDet.nomor       := '';//getNextNumber();
        trDet.dibayar     := asgInput.floats[colDibayar, i];
        trDet.keterangan  := TrimAllString(asgInput.cells[colKeterangan, i]);
        save_invoice_detail(trDet);

        update_terbayar(FALSE, trDet.jual_seq, trDet.dibayar);
      end;
    end;
    myConnection.EndSQL;
    Result:=True;
  except
    myConnection.undosql;
    myConnection.BeginSQL;
    delete_invoice_master(seqMst);
    myConnection.endsql;
    Result:=false;
  end;
end;

function TfrmInputInvoice.isSavedEdit : Boolean;
var dtMst,dtTemp : TR_invoice_master;
    trDet : TR_invoice_detail;
    arDet : AR_invoice_detail;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
    i, SeqDummy : Integer;
    state : boolean;
begin
  dtMst := get_invoice_master(seqMst);
  dtTemp := dtMst;

  dtMst.tanggal    := dtpTanggal.Date;
  dtMst.keterangan := TrimAllString(mmKet.Lines.Text);
  dtMst.card_seq   := seqCard;
  dtMst.total      := asgInput.floats[colDibayar, asgInput.RowCount-1];
  dtMst.deskripsi  := TrimAllString(mmDesk.Lines.Text);
  dtmst.bank_seq   := seqBank ;

  dtMst.nomor      := TrimAllString(txtNoInv.Text)+lblNoInv.Caption;     //GetNextNumber(dtpTanggal.Date, TRANS_INVOICE, seqCard);//sttno.caption;
  dtMst.nomor_kwt  := TrimAllString(txtNoKwitansi.Text)+lblNoKw.Caption; //GetNextNumber(dtpTanggal.Date, TRANS_KWITANSI, seqCard);
  dtMst.deskripsi  := TrimAllString(mmDesk.Lines.Text);

  if EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#', 26) = TRUE_STRING then begin
    dtMst.nomor_baut := TrimAllString(txtNoBaut.Text)+lblNoBaut.Caption;//GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);
    dtMst.nomor_bast := TrimAllString(txtNoBast.Text)+lblNoBast.Caption;//GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
  end else begin
    dtMst.nomor_baut := '';//GetNextNumber(dtpTanggal.Date, TRANS_BAUT, seqCard);
    dtMst.nomor_bast := '';//GetNextNumber(dtpTanggal.Date, TRANS_BAST, seqCard);
  end;

  try
    myConnection.BeginSQL;
    update_invoice_master(dtMst);
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result:=False;
    Exit;
  end;

  //save detail faktur beli
  myConnection.BeginSQL;
  try
    SeqDummy := save_dummy_invoice_master(dtTemp);
    save_dummy_invoice_detail(Seqdummy,seqMst);

    arDet := Get_Arr_invoice_detail(SeqMst);
    for i := 0 to length(ardet)-1 do begin
      update_terbayar(True, ardet[i].jual_seq, ardet[i].dibayar);
    end;
    delete_invoice_detail(SeqMst);

    // save ke log file
    Ket.IsEdit    := True;
    Ket.NamaTrans := 'Invoice';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := '';
    Ket.NoTrans   := '';
    Ket.TglTrans  := dtTemp.tanggal;

    Log.Nama_Tabel := 'invoice_master';
    Log.Tanggal    := dateNow;
    Log.Seq_Dummy  := SeqDummy;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := seqMst;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := '';
    Save_Log_File(Log);

    for i := 1 to asgInput.RowCount-2 do begin
      asgInput.GetCheckBoxState (colCheck,i,state);
      if state then begin
        trDet.seq:= CreateNewSeq('seq', 'invoice_detail');
        trDet.master_seq  := SeqMst;
        trDet.jual_seq    := asgInput.Ints[colSeqJual, i];
        trDet.nomor       := '';
        trDet.dibayar     := asgInput.floats[colDibayar, i];
        trDet.keterangan  := TrimAllString(asgInput.cells[colKeterangan, i]);
        save_invoice_detail(trDet);

        update_terbayar(false, trdet.jual_seq, trdet.dibayar);
      end;
    end;

    myConnection.EndSQL;
    Result:=True;
  except
    myConnection.undosql;
    myConnection.BeginSQL;
    update_invoice_master(dtTemp);
    myConnection.endsql;
    Result:=false;
  end;
end;


                                               
function TfrmInputInvoice.isValid: Boolean;
var i,jumData : integer;
    isAdaData, state :boolean;
begin
  Result := True;

  if length(txtNoInv.Text) < 3 then begin
    inform('Panjang nomor harus 3 karakter.');
    txtNoInv.SetFocus;
    Result := false;
    Exit;
  end;

  if StrToIntDef(txtNoInv.Text ,0) = 0 then begin
    inform('Nomor Invoice belum sesuai.');
    txtNoInv.SetFocus;
    Result := false;
    Exit;
  end;

  if Get_Count('nomor', 'invoice_master', TrimAllString((txtNoInv.Text)+lblNoInv.Caption),
               ifthen(SeqMst<>0,' and seq <> '+FormatSQLNumber(SeqMst)), 'string', true) > 0 then begin
    inform('Nomor Invoice sudah ada.');
    txtNoInv.SetFocus;
    Result := false;
    Exit;
  end;

  if length(txtNoKwitansi.Text) < 3 then begin
    inform('Panjang nomor harus 3 karakter.');
    txtNoKwitansi.SetFocus;
    Result := false;
    Exit;
  end;

  if StrToIntDef(txtNoKwitansi.Text ,0) = 0 then begin
    inform('Nomor Kwitanasi belum sesuai.');
    txtNoKwitansi.SetFocus;
    Result := false;
    Exit;
  end;

  if Get_Count('nomor_kwt', 'invoice_master', TrimAllString((txtNoKwitansi.Text)+lblNoKw.Caption),
               ifthen(SeqMst<>0,' and seq <> '+FormatSQLNumber(SeqMst)), 'string', true) > 0 then begin
    inform('Nomor Kwitansi sudah ada.');
    txtNoKwitansi.SetFocus;
    Result := false;
    Exit;
  end;

  if EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#', 26) = TRUE_STRING then begin
    if length(txtNoBaut.Text) < 3 then begin
      inform('Panjang nomor harus 3 karakter.');
      txtNoBaut.SetFocus;
      Result := false;
      Exit;
    end;

    if StrToIntDef(txtNoBaut.Text ,0) = 0 then begin
      inform('Nomor BAUT belum sesuai.');
      txtNoBaut.SetFocus;
      Result := false;
      Exit;
    end;

    if Get_Count('nomor_baut', 'invoice_master', TrimAllString((txtNoBaut.Text)+lblNoBaut.Caption),
                 ifthen(SeqMst<>0,' and seq <> '+FormatSQLNumber(SeqMst)), 'string', true) > 0 then begin
      inform('Nomor BAUT sudah ada.');
      txtNoBaut.SetFocus;
      Result := false;
      Exit;
    end;

    if length(txtNoBast.Text) < 3 then begin
      inform('Panjang nomor harus 3 karakter.');
      txtNoBast.SetFocus;
      Result := false;
      Exit;
    end;

    if StrToIntDef(txtNoBast.Text ,0) = 0 then begin
      inform('Nomor Bast belum sesuai.');
      txtNoBast.SetFocus;
      Result := false;
      Exit;
    end;

    if Get_Count('nomor_Bast', 'invoice_master', TrimAllString((txtNoBast.Text)+lblNoBast.Caption),
                 ifthen(SeqMst<>0,' and seq <> '+FormatSQLNumber(SeqMst)), 'string', true) > 0 then begin
      inform('Nomor Bast sudah ada.');
      txtNoBast.SetFocus;
      Result := false;
      Exit;
    end;
  end;

  if seqCard= 0 then begin
    inform('Customer harus dipilih.');
    btnCust.SetFocus;
    Result := false;
    Exit;
  end;

  if trimallstring(mmDesk.Lines.Text) = '' then begin
    inform('Deskripsi harus diisi');
    mmDesk.setfocus;
    Result := False;
    exit;
  end;

  if seqBank= 0 then begin
    inform('Bank harus dipilih.');
    btnBank.SetFocus;
    Result := false;
    Exit;
  end;


//  if txtJumlah.FloatValue = 0 then begin
//    inform('Jumlah Bayar harus diisi');
//    txtJumlah.setfocus;
//    Result := False;
//    exit;
//  end;

//  i := 0;
//  While (i <= asgInput.RowCount-2) do begin
//    i := i + 1;
//    if (i <> asgInput.RowCount) and ((asgInput.Floats[colTgl,i] = 0) and (asgInput.Cells[colNomor,i]
//      = '')) then begin
//      if (asgInput.RowCount > 3) then begin
//        asgInput.RemoveRows(i,1);
//        i:= i - 1;
//      end else asgInput.ClearRows(i,1);
//    end;
//  if (asgInput.RowCount = 3) and (asgInput.Floats[colDibayar, asgInput.RowCount-1] = 0) then begin
//    ArrangeColSize;
//    Inform('Tidak ada pembayaran yang diinput.');
//    Result := False;
//    Exit;
//  end;
//  end;
//  arrangecolsize;

  jumData := 0;
  isAdaData := false;
  for i := 1 to asgInput.rowcount-2 do begin
    asgInput.GetCheckBoxState (colCheck,i,state);
    if state then begin
      isAdaData := true;
      Inc(jumData);
      if (asgInput.floats[colDibayar, i] = 0) then begin
        Inform('Jumlah belum diinput.');
        asgInput.Col  := colDibayar;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
      if (asgInput.floats[colDibayar, i] > asgInput.floats[colsisa, i] ) then begin
        Inform('Jumlah melebihi sisa.');
        asgInput.Col  := colDibayar;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
      if ((asgInput.floats[colDibayar, i] <> asgInput.floats[colTotal, i] )) and (TrimAllString(
        asgInput.Cells[colketerangan,i]) = '') then begin
        Inform('Keterangan harus diisi karena nominal yang diinvoicekan tidak sama dengan total data jualnya.');
        asgInput.Col  := colketerangan;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;

    end;
  end;
  if isAdaData = false then begin
    Inform('Tidak ada data yang diinput.');
    Result := False;
    Exit;
  end;
  if (jumData > 1) and (trimallstring(mmDesk.lines.Text) = '') then begin
    Inform('Deskripsi harus diinput');
    mmDesk.SetFocus;
    Result := False;
    Exit;
  end;

//  if txtJumlah.FloatValue <> txtTOtal.floatvalue then begin
//    inform('Jumlah Bayar && Total tidak sama');
//    txtJumlah.setfocus;
//    Result := False;
//    exit;
//  end;

end;

procedure TfrmInputInvoice.loaddata;
var dtMst : TR_invoice_master;
    arDet : ar_invoice_detail;
    dtMstJual : TR_data_jual_master;
    buffer  :  _recordset;
    j, row, i : integer;
    Periode : TR_FilterPeriode;
    noInv, noKw, noBast, noBaut : string;
begin
  dtMst := get_invoice_master(seqMst);
  seqCard := dtMst.card_seq;
  seqBank := dtMst.bank_seq;

  txtBank.text := EkstrakString(ListBankGlobal.Values[IntToStr(seqBank)],'#',1);
  txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqcard)],'#',1) + ' - ' +
                  EkstrakString(ListCardGlobal.Values[IntToStr(seqcard)],'#',2);

  dtpTanggal.Date   := dtMst.tanggal;
  mmKet.Lines.Text  := dtMst.keterangan;
  sttno.caption     := dtMst.nomor;
  txtJumlah.FloatValue := dtMst.total;
  mmDesk.Lines.Text := dtMst.deskripsi;

  noInv := dtMst.nomor;
  nokw  := dtMst.nomor_kwt;
  noBast := dtMst.nomor_bast;
  noBaut := dtMst.nomor_baut;

  txtNoInv.Text      := EkstrakString( noInv , '/', 1) ;
  txtNoKwitansi.Text := EkstrakString( noKw, '/', 1) ;
  txtNoBast.Text     := EkstrakString( noBast , '/', 1) ;
  txtNoBaut.Text     := EkstrakString( noBaut, '/', 1) ;

  lblNoInv.Caption   := Copy( noInv, 4, Length(noInv)-3);
  lblNoKw.Caption    := Copy( nokw,  4, Length(nokw)-3);
  lblNoBast.Caption  := Copy( noBast, 4, Length(noBast)-3);
  lblNoBaut.Caption  := Copy( noBaut, 4, Length(noBaut)-3);

  setGrb;

  arDet := Get_Arr_invoice_detail(SeqMst);
  for i := 0 to length(arDet)-1 do begin
    if i > 0 then asgInput.AddRow;
    row := asgInput.RowCount-2;
    dtMstJual := Get_data_jual_master(arDet[i].jual_seq);

    asgInput.ints[colSeqJual      ,row] := arDet[i].jual_seq;
    asgInput.Dates[colTgl         ,row] := dtMstJual.tanggal;
    asgInput.Cells[colNomor ,      row] := dtMstJual.nomor_po;
    asgInput.Cells[colInv ,        row] := dtmst.nomor;
    asgInput.floats[colTotal      ,row] := dtMstJual.subtotal;
    asgInput.floats[colTerbayar   ,row] := dtMstJual.terbayar;
    asgInput.floats[colSisa       ,row] := asgInput.floats[colTotal,row] -
                                           asgInput.floats[colTerbayar,row] +
                                           arDet[i].dibayar;
    asgInput.floats[colDibayar    ,row] := arDet[i].dibayar;
    asgInput.Cells[colketerangan  ,row] := arDet[i].keterangan;
    asgInput.AddCheckBox(colCheck,row,true,false);
  end;

  arrangecolsize;
  updatetotal;
end;

procedure TfrmInputInvoice.loaddatajual;
var buffer  :  _recordset;
    row, i : integer;
    Periode : TR_FilterPeriode;
begin
  txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                  EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);

  periode.FPeriodeAwal      := 0;
  periode.FPeriodeAkhir     := dateNow;
  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;

  Load_data_jual_master(buffer,periode, 0, seqCard, '', ' and terbayar <> subtotal ');

//  (seq = 0)  (tanggal = 1)  (nomor_po = 2)  (job_seq = 3)  (card_seq = 4)  (subtotal = 5)  (ppn =
//6)  (total = 7)  (keterangan = 8)  (tempo_byr = 9)  (user_id = 10)  (tgl_input = 11)  (user_edit =
// 12)  (tgl_edit = 13) terbayar 14, sisa  15

  for i := 0 to buffer.RecordCount-1 do begin
    if i > 0 then asgInput.AddRow;
    row := asgInput.RowCount-2;

    asgInput.ints[colSeqJual      ,row] := BufferToInteger(buffer.Fields[0].Value);
    asgInput.Dates[colTgl         ,row] := BufferToDateTime(buffer.Fields[1].Value);
    asgInput.Cells[colNomor ,      row] := BufferToString(buffer.Fields[2].Value);
    asgInput.floats[colTotal      ,row] := BufferToFloat(buffer.Fields[5].Value);
    asgInput.floats[colTerbayar   ,row] := BufferToFloat(buffer.Fields[14].Value);
    asgInput.floats[colSisa       ,row] := asgInput.floats[colTotal,row] -
                                           asgInput.floats[colTerbayar,row];
    asgInput.floats[colDibayar    ,row] := 0;
    asgInput.Cells[colketerangan  ,row] := '';

    asgInput.AddCheckBox(colCheck,row,false,false);

    buffer.movenext;
  end;
  buffer.close;
  updatetotal;
  arrangecolsize;
end;

procedure TfrmInputInvoice.arrangecolsize;
begin
  asgInput.AutoNumberCol(colNo);
  asgInput.Cells[colNomor,asgInput.RowCount-1]:='Total';
  asgInput.FloatingFooter.ColumnCalc[colTotal]  :=acSUM;
  asgInput.FloatingFooter.ColumnCalc[colDibayar]  :=acSUM;
  asgInput.FloatingFooter.ColumnCalc[colSisa]  :=acSUM;
  asgInput.FloatingFooter.ColumnCalc[colTerbayar]  :=acSUM;
  asgInput.AutoSizeColumns(true);

  asgInput.ColWidths[colTerbayar] := 0;
//  asgInput.ColWidths[colSisa]     := 0;
//  asgInput.ColWidths[colDibayar]  := 0;
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgInput.ColWidths[colSeqJual]   := 0;
  end;
  if (browsemode) and (not editmode) then begin
    asginput.ColWidths[colsisa] := 0;
  end;

  if not browsemode then begin
    asginput.ColWidths[colInv] := 0;
  end ELSE BEGIN
    asginput.ColWidths[colcheck] := 0;
  END;
end;

procedure TfrmInputInvoice.setGrb;
begin
  if EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#', 26) = TRUE_STRING then begin
//    GroupBox2.Top := GroupBox2.Top + (GroupBox3.height);
//    GroupBox2.height := normal_height;// GroupBox2.height - (GroupBox3.height);
    GroupBox3.visible := true;
  end else begin
    GroupBox3.visible := false;
//    GroupBox2.Top := GroupBox3.Top ;
//    GroupBox2.height := GroupBox2.height + (GroupBox3.height);
  end;
end;

procedure TfrmInputInvoice.setgrid;
begin
  asgInput.ClearNormalCells;
  asgInput.ColCount  := 12;
  asgInput.RowCount  := 3;
  asgInput.FixedCols := 1;
  asgInput.FixedRows := 1;

  asgInput.Cells[colNo,0] :=  'No.';
  asgInput.Cells[colNomor,0] :=  'Nomor PO';
  asgInput.Cells[colInv,0] :=  'Nomor Invoice';
  asgInput.Cells[colTgl,0] :=  'Tanggal';
  asgInput.Cells[colSisa,0] :=  'Sisa';
  asgInput.Cells[colTerbayar,0] :=  'Tagihan';
  asgInput.Cells[colDibayar,0] :=  'Jumlah';
  asgInput.Cells[colSeqJual,0] :=  'Seq Jual';
  asgInput.Cells[coltotal,0] :=  'Total';
  asgInput.Cells[colketerangan,0] :=  'Keterangan';

  arrangecolsize;
end;

procedure TfrmInputInvoice.updateTotal;
begin
  txtTotal.FloatValue    := asgInput.ColumnSum(colDibayar, 1 , asgInput.RowCount-2);
//  txtTotal.FloatValue    := asgInput.Floats[colDibayar, asgInput.RowCount-1];
end;

procedure TfrmInputInvoice.asgInputAutoAddRow(Sender: TObject;
  ARow: Integer);
begin
  arrangecolsize;
end;

procedure TfrmInputInvoice.asgInputAutoDeleteRow(Sender: TObject;
  ARow: Integer);
begin
  arrangecolsize;
  updateTotal;
end;

procedure TfrmInputInvoice.asgInputCanAddRow(Sender: TObject;
  var CanAdd: Boolean);
begin
  CanAdd:= false;//not isAdaDataJual ;//not(BrowseMode xor EditMode);
end;

procedure TfrmInputInvoice.asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
  var CanDelete: Boolean);
begin
  CanDelete := false;//not isAdaDataJual ;//not(BrowseMode xor EditMode);
end;

procedure TfrmInputInvoice.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
  var state : boolean;
begin
  if Not (browseMode xor editMode) then  begin
//    if isAdaDataJual then
    asgInput.GetCheckBoxState(colCheck, ARow, state);
    if state then
      CanEdit := Acol in [colCheck, colDibayar, colKeterangan]
    else CanEdit := Acol in [colCheck];
//    else CanEdit := Acol in [colNomor, colTgl, colTerbayar, colSisa, colKeterangan];
  end
  else canEdit := false;
end;

procedure TfrmInputInvoice.asgInputCellValidate(Sender: TObject; ACol,
  ARow: Integer; var Value: string; var Valid: Boolean);
begin
  value := TrimAllString(Value);
//  case acol of
//    coldibayar: begin
//      if asgInput.Floats[colDibayar, arow] > txtJumlah.floatvalue then begin
//        asgInput.Floats[colDibayar, arow] := txtJumlah.floatvalue;
////        value := FloatToStr(txtJumlah.floatvalue);
//      end;

//      if  asgINput.cells[colSisa, arow]='Ada' then begin
//        asgInput.Floats[colDibayar, arow] := asgInput.Floats[colTerbayar, arow] * 0.1;
//      end else begin
//        asgInput.Floats[colDibayar, arow] := 0;
//      end;
//    end;
//  end;
  arrangecolsize;
  updatetotal;
end;

procedure TfrmInputInvoice.asgInputCheckBoxChange(Sender: TObject; ACol,
  ARow: Integer; State: Boolean);
var tmp : real;
  satatus : boolean;
  jumdat, i : integer;
begin
  if (arow > 0) and (arow < asgINput.rowcount-1) then begin
    if State then begin
//      if txtJumlah.FloatValue = 0 then begin
//        inform('Jumlah Bayar harus diisi');
//        state := false;
//        txtJumlah.setfocus;
//        asgInput.SetCheckBoxState(colCheck, ARow, False);
//        exit;
//      end;
//      tmp := txtJumlah.FloatValue - asgInput.floats[coldibayar, asginput.rowcount-1] ;
//      if tmp > asgInput.Floats[colSisa,arow] then
      asgInput.Floats[colDibayar,arow] := asgInput.Floats[colSisa,arow] ;
//      else asgInput.Floats[colDibayar,arow] := tmp;
    end else asgInput.Floats[colDibayar,arow] := 0;

//    for i := 1 to asginput.rowcount - 2 do begin
//      asginput.GetCheckBoxState(colCheck,i,satatus);
//      if satatus then inc(jumdat);
//    end;
//    if jumdat > 1 then begin
//      mmDesk.Lines.Clear
//    end els             
    arrangecolsize;
    updatetotal;
  end;
end;

procedure TfrmInputInvoice.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (arow = 0) or (acol in [colcheck]) then HAlign := taCenter
  else if Acol in [ColNo, colTerbayar, coltotal, colDibayar, colsisa, colSeqJual] then HAlign :=
    taRightJustify;

  VAlign := vtaCenter;
end;

procedure TfrmInputInvoice.asgInputGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case ACol of
    colNomor, colketerangan: AEditor := edMixedCase;
    colSeqJual: AEditor := edPositiveNumeric;
    colSisa, coltotal, colDibayar,colTerbayar : AEditor := edPositiveFloat;
    colCheck : aeditor := edCheckBox;
  end;
end;

procedure TfrmInputInvoice.asgInputGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := acol in [ColNo,colTerbayar,colDibayar,colSeqJual,coltotal];
  if isfloat then begin
    case ACol of
      colSeqJual,ColNo: FloatFormat := '%.0n';
      colTerbayar,colDibayar,coltotal : FloatFormat := '%.2n';
    end;
  end else FloatFormat := '';
end;

procedure TfrmInputInvoice.asgInputMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize;
end;

procedure TfrmInputInvoice.asgInputMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize;
end;

procedure TfrmInputInvoice.btnBankClick(Sender: TObject);
begin
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqBank := frmLov.ExecuteBank('');
  if seqBank > 0 then begin
    txtBank.text := EkstrakString(ListBankGlobal.Values[IntToStr(seqBank)],'#',1);
  end else txtBank.text := '';


end;

procedure TfrmInputInvoice.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.

