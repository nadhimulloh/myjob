unit DataJualInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker, Grids, AdvObj, BaseGrid, AdvGrid,
  AdvOfficePager;

type
  TfrmInputDataJual = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    txtNomor: TAdvEdit;
    Label1: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    txtCust: TAdvEdit;
    btnPek: TAdvGlowButton;
    dtpTanggal: TAdvDateTimePicker;
    mmKet: TMemo;
    Label6: TLabel;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label11: TLabel;
    txtSubTotal: TAdvEdit;
    txtPPN: TAdvEdit;
    txtTotal: TAdvEdit;
    chbPPn: TAdvOfficeCheckBox;
    txtPek: TAdvEdit;
    Label2: TLabel;
    txtTempo: TAdvEdit;
    Label7: TLabel;
    mmDesk: TMemo;
    Label8: TLabel;
    lblPPN: TLabel;
    txtUnitBisnis: TAdvEdit;
    Label10: TLabel;
    AopPager: TAdvOfficePager;
    pgLog: TAdvOfficePage;
    pgCons: TAdvOfficePage;
    pgGen: TAdvOfficePage;
    asgInput: TAdvStringGrid;
    procedure btnBatalClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure btnPekClick(Sender: TObject);
    procedure asgInputAutoDeleteRow(Sender: TObject; ARow: Integer);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgInputGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgInputGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgInputMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgInputMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chbPPnClick(Sender: TObject);
    procedure asgInputAutoAddRow(Sender: TObject; ARow: Integer);
    procedure asgInputComboChange(Sender: TObject; ACol, ARow,
      AItemIndex: Integer; ASelection: string);
    procedure asgInputKeyPress(Sender: TObject; var Key: Char);
    procedure dtpTanggalChange(Sender: TObject);
  private
    { Private declarations }
    seqCard, seqPek, SeqMst : integer;
    browseMode, EditMode : Boolean;
    dateNow : TDateTime;
    PPN_default : real;
    procedure setgrid(idx : integer);
    procedure arrangecolsize(idx : integer);
    function isValid : Boolean;
    function isSaved : Boolean;
    function isSavedEdit : Boolean;
    procedure loaddata;
    procedure InitForm;
    procedure updateTotal(idx : integer);
    procedure setPPN(idx : integer;row : integer = 0);
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(ASeqMst : integer; isEdit: boolean):boolean;
  end;

var
  frmInputDataJual: TfrmInputDataJual;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi, UDummy;

const
  colNo          = 0;
  colUraian      = 1;
  colSite        = 2;
  colOrigin      = 3;
  colCluster     = 4;
  colDest        = 5;
  colBpid        = 6;
  colserial      = 7;
  colqty         = 8;
  colSatuan      = 9;
  colharga       = 10;
  colPajak       = 11;
  colppn         = 12;
  colHrgAKhir    = 13;
  coltotal       = 14;
  colketerangan  = 15;
  colSeqDetail   = 16;

{$R *.dfm}

procedure TfrmInputDataJual.btnPekClick(Sender: TObject);
var dtPek : TR_master_job;
begin
  seqPek:=0;
  txtCust.Text :='';
  txtpek.Text :='';
  mmDesk.Clear;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqPek := frmLov.ExecuteJob('', botrue, '', boFalse);
  if seqPek <> 0 then begin
    dtPek:= Get_master_job(seqPek);
    txtnomor.text  := dtPek.no_po;
    txtPek.text := EkstrakString(ListJobGlobal.Values[IntToStr(seqPek)],'#',1) + ' - ' +
                    EkstrakString(ListJobGlobal.Values[IntToStr(seqPek)],'#',2);
    seqCard := dtPek.card_seq;
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);

    if dtpek.unit_bisnis = TIPE_DIVISI_LOGISTIC then
      txtUnitBIsnis.text := tiPE_DIVISI_LOGISTIC_text
    else if dtpek.unit_bisnis = TIPE_DIVISI_TRADING then
      txtUnitBIsnis.text := TIPE_DIVISI_TRADING_text
    else if dtpek.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
      txtUnitBIsnis.text := TIPE_DIVISI_CONSTRUCTION_text;

    mmDesk.Lines.Text := dtPek.deskripsi;

    pgCons.tabVisible := false;
    pgGen.tabVisible := false;
    pgLog.tabVisible := true;
    AopPager.ActivePage := pgLog;
    if dtPek.unit_bisnis = TIPE_DIVISI_LOGISTIC then begin
      pgLog.caption := TIPE_DIVISI_LOGISTIC_TEXT;
    end else
    if dtPek.unit_bisnis = TIPE_DIVISI_TRADING then begin
      pgLog.caption := TIPE_DIVISI_TRADING_TEXT;
//      pgLog.tabVisible := false;
//      pgCons.tabVisible := false;
//      pgGen.tabVisible := true;
//      AopPager.ActivePage := pggen;
    end else
    if dtPek.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then begin
      pgLog.caption := TIPE_DIVISI_CONSTRUCTION_TEXT;
//      pgLog.tabVisible := false;
//      pgGen.tabVisible := false;
//      pgCons.tabVisible := true;
//      AopPager.ActivePage := pgCons;
    end;
  end;
  arrangecolsize(1);
end;

procedure TfrmInputDataJual.btnSimpanClick(Sender: TObject);
begin
  btnSimpan.Enabled := False;
  if (browsemode) and (not editmode) then begin
//    data := Get_faktur_beli_master(seqMst);
//    Application.CreateForm(TfrmPrintPenerimaanSparepart, frmPrintPenerimaanSparepart);
//    frmPrintPenerimaanSparepart.OnClose := frmMainMenu.ChildFormSingle;
//    frmPrintPenerimaanSparepart.Executes(Data);
  end else if (not (browseMode xor editMode)) and (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if editmode then begin
      if isSavedEdit then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else Inform(MSG_UNSUCCESS_UPDATE);
    end else begin
      if isSaved then begin
//        if Confirmed(MSG_PRINT) then begin
//          data := Get_faktur_beli_master(SeqMst);
//          Application.CreateForm(TfrmPrintPenerimaanSparepart, frmPrintPenerimaanSparepart);
//          frmPrintPenerimaanSparepart.OnClose := frmMainMenu.ChildFormSingle;
//          frmPrintPenerimaanSparepart.Executes(Data);
//        end;
        if Confirmed(MSG_ADD_DATA) then begin
          initform; Setgrid(0); statSimpan := true;
          txtNomor.SetFocus;
        end else ModalResult := mrOk;
      end else begin
        ActiveControl := btnBatal;
      end;
    end;
  end;
  btnSimpan.Enabled := True;
end;

procedure TfrmInputDataJual.chbPPnClick(Sender: TObject);
begin
//  setPPN;
//  updateTotal;
end;

procedure TfrmInputDataJual.dtpTanggalChange(Sender: TObject);
begin
  PPN_default := Get_nilai_ppn(dtpTanggal.Date);
  lblPPN.Caption := 'PPN '+FloatToStrFmt( PPN_default )+'%';
  setppn(0);
  updatetotal(1);
  arrangecolsize(1);
end;

function TfrmInputDataJual.execute(ASeqMst: integer; isEdit: boolean): boolean;
begin
  dateNow    := ServerNow(True);
  browseMode := (AseqMst <> 0); editMode := isEdit;
  initform;
  SeqMst      := ASeqMst;
  if editMode then begin
    Self.Caption := 'Edit Data Jual';
    btnSimpan.Caption := '&Simpan';
  end else if browsemode then begin
    Self.Caption := 'Detail Data Jual';
    btnSimpan.Caption := '&Simpan';
    btnSimpan.enabled := false;
  end else begin
    Self.Caption := 'Tambah Data Jual';
    btnSimpan.Caption := '&Simpan';
  end;
  setgrid(0);
  pgCons.tabVisible := false;
  pgGen.tabVisible := false;
  pgLog.tabVisible := true;
  AopPager.ActivePage := pgLog;

  if browseMode then loaddata;
  run(self);
  result := ModalResult = mrOk;
end;

procedure TfrmInputDataJual.InitForm;
begin
  txtSetAllEmpty(self);
  dtpTanggal.Date := Datenow;

  PPN_default := Get_nilai_ppn(dtpTanggal.Date);
  seqCard   := 0;
  seqPek    := 0;
  chbPPn.Checked:= False;
  lblPPN.Caption := 'PPN '+FloatToStrFmt( PPN_default )+'%';

  dtpTanggal.Enabled :=  not (browseMode xor editMode);
  btnPek.Enabled     :=  not (browseMode);
  chbPPn.Enabled     :=  not (browseMode xor editMode);
  txtTempo.readonly  := (browseMode xor editMode);
  mmKet.readonly     := (browseMode xor editMode);
end;

function TfrmInputDataJual.isSaved: Boolean;
var dtMst : TR_data_jual_master;
    trDet : TR_data_jual_detail;
    row, i : integer;
begin
  dtMst.tanggal    := dtpTanggal.Date;
  dtMst.nomor_po   := txtNomor.Text;
  dtMst.keterangan := TrimAllString(mmKet.Lines.Text);
  dtMst.card_seq   := seqCard;
  dtMst.job_seq    := seqPek;
  dtMst.subtotal   := txtSubTotal.FloatValue;
  dtMst.ppn        := txtPPN.FloatValue;
  dtMst.total      := txtTotal.FloatValue;
  dtMst.tempo_byr  := txttempo.intvalue;

  SeqMst := Save_data_jual_master(dtMst);

  if Copy(ErrorSave, 1, 9) = ERR_LOCKED_TABLE then begin
    Result := False;
    Inform(MSG_SERVER_BUSY);
    Exit;
  end;
  if SeqMst = 0 then begin
    Result := False;
    Inform(MSG_UNSUCCESS_SAVING);
    Exit;
  end;

  //save detail faktur beli
  myConnection.BeginSQL;
  try
//    if txtUnitBisnis.Text = TIPE_DIVISI_LOGISTIC_TEXT then begin
    for i := 1 to asgInput.RowCount-2 do begin
      trDet.seq:= CreateNewSeq('seq', 'data_jual_detail');
      trDet.master_seq  := SeqMst;
      trDet.spesifikasi := TrimAllString(asgInput.Cells[colUraian, i]);
      trDet.harga       := asgInput.floats[colharga, i];
      trDet.ppn         := asgInput.floats[colppn, i];
      trDet.qty         := asgInput.floats[colqty, i];
      trDet.keterangan  := TrimAllString(asgInput.cells[colKeterangan, i]);


      trDet.serial      := trimallstring(asgInput.Cells[colserial, i]);
      trDet.origin      := trimallstring(asgInput.Cells[colOrigin, i]);
      trDet.clust       := trimallstring(asgInput.Cells[colcluster, i]);
      trDet.destination := trimallstring(asgInput.cells[colDest, i]);
      trDet.bpid        := trimallstring(asgInput.cells[colbpid, i]);
      trDet.satuan      := TrimAllString(asgInput.cells[colSatuan, i]);
      trDet.site_name   := TrimAllString(asgInput.cells[colSite, i]);

      save_data_jual_detail(trDet);
    end;
//    end;

    myConnection.EndSQL;
    Result:=True;
  except
    myConnection.undosql;
    myConnection.BeginSQL;
    delete_data_jual_master(seqMst);
    myConnection.endsql;
    Result:=false;
  end;
end;

function TfrmInputDataJual.isSavedEdit : Boolean;
var dtMst,dtTemp : TR_data_jual_master;
    trDet : TR_data_jual_detail;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
    i, SeqDummy : Integer;
begin
  dtMst := get_data_jual_master(seqMst);
  dtTemp := dtMst;

  dtMst.tanggal  := dtpTanggal.Date;
  dtMst.nomor_po    := txtNomor.Text;
  dtMst.keterangan := TrimAllString(mmKet.Lines.Text);
  dtMst.card_seq   := seqCard;
  dtMst.job_seq    := seqPek;
  dtMst.subtotal  := txtSubTotal.FloatValue;
  dtMst.ppn       := txtPPN.FloatValue;
  dtMst.total     := txtTotal.FloatValue;
  dtMst.tempo_byr  := txttempo.intvalue;
  dtmst.sisa       := dtMst.total - dtMst.terbayar;
  try
    myConnection.BeginSQL;
    update_data_jual_master(dtMst);
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result:=False;
    Exit;
  end;

  //save detail faktur beli
  myConnection.BeginSQL;
  try
    SeqDummy := save_dummy_data_jual_master(dtTemp);
    save_dummy_data_jual_detail(Seqdummy,seqMst);

    delete_data_jual_detail(SeqMst);

    // save ke log file
    Ket.IsEdit    := True;
    Ket.NamaTrans := 'Data Jual';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := '';
    Ket.NoTrans   := dtTemp.Nomor_po;
    Ket.TglTrans  := dtTemp.tanggal;

    Log.Nama_Tabel := 'data_jual_master';
    Log.Tanggal    := dateNow;
    Log.Seq_Dummy  := SeqDummy;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := seqMst;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := '';
    Save_Log_File(Log);

//    if txtUnitBisnis.Text = TIPE_DIVISI_LOGISTIC_TEXT then begin
    for i := 1 to asgInput.RowCount-2 do begin
      if asgInput.Ints[colSeqDetail, i] > 0 then
        trDet.seq:= asgInput.Ints[colSeqDetail, i]
      else trDet.seq:= CreateNewSeq('seq', 'data_jual_detail');

      trDet.master_seq  := SeqMst;
      trDet.spesifikasi := TrimAllString(asgInput.Cells[colUraian, i]);
      trDet.harga       := asgInput.floats[colharga, i];
      trDet.ppn         := asgInput.floats[colppn, i];
      trDet.qty         := asgInput.floats[colqty, i];
      trDet.keterangan  := TrimAllString(asgInput.cells[colKeterangan, i]);

      trDet.serial      := trimallstring(asgInput.Cells[colserial, i]);
      trDet.origin      := trimallstring(asgInput.Cells[colOrigin, i]);
      trDet.clust       := trimallstring(asgInput.Cells[colcluster, i]);
      trDet.destination := trimallstring(asgInput.cells[colDest, i]);
      trDet.bpid        := trimallstring(asgInput.cells[colbpid, i]);
      trDet.satuan      := TrimAllString(asgInput.cells[colSatuan, i]);   
      trDet.site_name   := TrimAllString(asgInput.cells[colSite, i]);

      save_data_jual_detail(trDet);
    end;
//    end;

    myConnection.EndSQL;
    Result:=True;
  except
    myConnection.undosql;
    myConnection.BeginSQL;
    update_data_jual_master(dtTemp);
    myConnection.endsql;
    Result:=false;
  end;
end;             

function TfrmInputDataJual.isValid: Boolean;
var i : integer;
begin
  Result := True;
  if TrimAllString(txtNomor.Text) = '' then begin
    inform('No. PO harus diisi.');
    txtNomor.SetFocus;
    Result := false;
    Exit;
  end;

  if seqPek= 0 then begin
    inform('Pekerjaan harus dipilih.');
    btnPek.SetFocus;
    Result := false;
    Exit;
  end;

  i := 0;
  While (i <= asgInput.RowCount-2) do begin
    i := i + 1;
    if (i <> asgInput.RowCount) and ((asgInput.Floats[ColQty,i] = 0) and (asgInput.Cells[colUraian,i]
      = '')) then begin
      if (asgInput.RowCount > 3) then begin
        asgInput.RemoveRows(i,1);
        i:= i - 1;
      end else asgInput.ClearRows(i,1);
    end;
    if (asgInput.RowCount = 3) and (asgInput.Cells[colUraian, 1] = '') then begin
      ArrangeColSize(0);
      Inform('Tidak ada uraian yang diinput.');
      Result := False;
      Exit;
    end;
  end;
  arrangecolsize(0);

  for i := 1 to asgInput.rowcount-2 do begin
    if TrimAllString(asgInput.Cells[colUraian, i]) = '' then begin
      Inform('Uraian belum diinput.');
      asgInput.Col  := colUraian;
      asgInput.Row  := i;
      Result := False;
      Exit;
    end;

    if txtUnitBisnis.text = TIPE_DIVISI_LOGISTIC_TEXT then begin        
      if (TrimAllString(asgInput.cells[colSite, i]) = '') then begin
        Inform('Site Name belum diinput.');
        asgInput.Col  := colSite;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
      if (TrimAllString(asgInput.cells[colOrigin, i]) = '') then begin
        Inform('Origin belum diinput.');
        asgInput.Col  := colOrigin;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
      if (TrimAllString(asgInput.cells[colCluster, i]) = '') then begin
        Inform('Cluster belum diinput.');
        asgInput.Col  := colCluster;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
      if (TrimAllString(asgInput.cells[colDest, i]) = '') then begin
        Inform('Destination belum diinput.');
        asgInput.Col  := colDest;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
      if (TrimAllString(asgInput.cells[colBpid, i]) = '') then begin
        Inform('Bpid belum diinput.');
        asgInput.Col  := colBpid;
        asgInput.Row  := i;
        Result := False;
        Exit;
      end;
    end;

    if (asgInput.floats[colqty, i] = 0) then begin
      Inform('Qty belum diinput.');
      asgInput.Col  := colqty;
      asgInput.Row  := i;
      Result := False;
      Exit;
    end;

    if (asgInput.floats[colharga, i] = 0) then begin
      Inform('Harga belum diinput.');
      asgInput.Col  := colharga;
      asgInput.Row  := i;
      Result := False;
      Exit;
    end;

  end;
end;

procedure TfrmInputDataJual.loaddata;
var dtMst : TR_data_jual_master;
    dtPek : Tr_master_job;
    arDet : AR_data_jual_detail;
    row, i : integer;
begin
  dtMst := get_data_jual_master(seqMst);
  seqPek := dtMst.job_seq;
  dtPek:= Get_master_job(seqPek);
  dtpTanggal.Date := dtMst.tanggal;
  txtNomor.Text   := dtMst.nomor_po;
  mmKet.Lines.Text:= dtMst.keterangan;
  txtTempo.IntValue := dtMst.tempo_byr;
  dtPek.no_po := txtnomor.text;

  txtPek.text := EkstrakString(ListJobGlobal.Values[IntToStr(seqPek)],'#',1) + ' - ' +
                  EkstrakString(ListJobGlobal.Values[IntToStr(seqPek)],'#',2);
  seqCard := dtPek.card_seq;
  txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                  EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  mmDesk.Lines.Text := dtPek.deskripsi;

  if dtpek.unit_bisnis = TIPE_DIVISI_LOGISTIC then
    txtUnitBIsnis.text := tiPE_DIVISI_LOGISTIC_text
  else if dtpek.unit_bisnis = TIPE_DIVISI_TRADING then
    txtUnitBIsnis.text := TIPE_DIVISI_TRADING_text
  else if dtpek.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
    txtUnitBIsnis.text := TIPE_DIVISI_CONSTRUCTION_text;

  txtSubTotal.FloatValue := dtMst.subtotal;
  txtPPN.FloatValue      := dtMst.ppn;
  chbPPn.Checked := dtMst.ppn <> 0;
  txtTotal.FloatValue    := dtMst.total;

  pgCons.tabVisible := false;
  pgGen.tabVisible := false;
  pgLog.tabVisible := true;
  AopPager.ActivePage := pgLog;
  if dtPek.unit_bisnis = TIPE_DIVISI_LOGISTIC then begin
    pgLog.caption := TIPE_DIVISI_LOGISTIC_TEXT;
  end else
  if dtPek.unit_bisnis = TIPE_DIVISI_TRADING then begin
    pgLog.caption := TIPE_DIVISI_TRADING_TEXT;
//      pgLog.tabVisible := false;
//      pgCons.tabVisible := false;
//      pgGen.tabVisible := true;
//      AopPager.ActivePage := pggen;
  end else
  if dtPek.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then begin
    pgLog.caption := TIPE_DIVISI_CONSTRUCTION_TEXT;
//      pgLog.tabVisible := false;
//      pgGen.tabVisible := false;
//      pgCons.tabVisible := true;
//      AopPager.ActivePage := pgCons;
  end;

  PPN_default := Get_nilai_ppn(dtpTanggal.Date);
  lblPPN.Caption := 'PPN '+FloatToStrFmt( PPN_default )+'%';

  arDet := Get_Arr_data_jual_detail(seqMst);
  for i := 0 to Length(ardet)-1 do begin
    if i > 0 then asgInput.AddRow;
    row := asgInput.RowCount-2;
    if arDet[i].ppn > 0 then begin
      asgInput.Cells[colPajak, row] := 'Ada'
    end
    else begin
      asgInput.Cells[colPajak, row] := 'Tidak';
    end;

    asgInput.Cells[colserial,      row] := ardet[i].serial;
    asgInput.Cells[colUraian ,     row] := ardet[i].Spesifikasi;
    asgInput.floats[colharga      ,row] := ardet[i].harga;
    asgInput.floats[colppn        ,row] := ardet[i].ppn;
    asgInput.floats[colqty        ,row] := ardet[i].qty;
    asgInput.ints[colSeqDetail    ,row] := ardet[i].seq;
    asgInput.cells[colSatuan,      row] := ardet[i].satuan;
    asgInput.Cells[colketerangan  ,row] := ardet[i].keterangan;
    asgInput.cells[colOrigin,      row] := ardet[i].origin;
    asgInput.Cells[colCluster     ,row] := ardet[i].clust;
    asgInput.cells[colDest,        row] := ardet[i].destination;
    asgInput.Cells[colBpid        ,row] := ardet[i].bpid;
    asgInput.cells[colSite,        row] := ardet[i].site_name;
  end;
  setPPN(1);
  updatetotal(1);
  arrangecolsize(1);
end;

procedure TfrmInputDataJual.arrangecolsize(idx : integer);
begin
  if idx in [0,1] then begin
    asgInput.AutoNumberCol(colNo);
    asgInput.Cells[colUraian,asgInput.RowCount-1]:='Total';
    asgInput.FloatingFooter.ColumnCalc[colqty]  :=acSUM;
    asgInput.FloatingFooter.ColumnCalc[colppn]  :=acSUM;
    asgInput.FloatingFooter.ColumnCalc[colTotal]  :=acSUM;
    asgInput.AutoSizeColumns(true);
    if txtUnitBisnis.text <> TIPE_DIVISI_LOGISTIC_TEXT then begin
      asgInput.colwidths[colOrigin] := 0;
      asgInput.colwidths[colCluster] := 0;
      asgInput.colwidths[colDest] := 0;
      asgInput.colwidths[colBpid] := 0;
      asgInput.colwidths[colSite] := 0;
      asgInput.colwidths[colSerial] := 0;
    end;
    asgInput.colwidths[colSeqDetail] := 0;
  end;
  if idx in [0,2] then begin
//    asgInput.AutoNumberCol(colNo);
//    asgInput.Cells[colUraian,asgInput.RowCount-1]:='Total';
//    asgInput.FloatingFooter.ColumnCalc[colqty]  :=acSUM;
//    asgInput.FloatingFooter.ColumnCalc[colppn]  :=acSUM;
//    asgInput.FloatingFooter.ColumnCalc[colTotal]  :=acSUM;
//    asgInput.AutoSizeColumns(true);
  end;
  if idx in [0,3] then begin
//    asgInput.AutoNumberCol(colNo);
//    asgInput.Cells[colUraian,asgInput.RowCount-1]:='Total';
//    asgInput.FloatingFooter.ColumnCalc[colqty]  :=acSUM;
//    asgInput.FloatingFooter.ColumnCalc[colppn]  :=acSUM;
//    asgInput.FloatingFooter.ColumnCalc[colTotal]  :=acSUM;
//    asgInput.AutoSizeColumns(true);
  end;
end;

procedure TfrmInputDataJual.setgrid(idx : integer);
begin
  if idx in [0,1] then begin
    asgInput.ClearNormalCells;
    asgInput.ColCount  := 18;
    asgInput.RowCount  := 3;
    asgInput.FixedCols := 1;
    asgInput.FixedRows := 1;

    asgInput.Cells[colNo,0] :=  'No.';
    asgInput.Cells[colUraian,0] :=  'Uraian';
    asgInput.Cells[colqty,0] :=  'Qty';
    asgInput.Cells[colPajak,0] :=  'Pajak';
    asgInput.Cells[colharga,0] :=  'Harga';
    asgInput.Cells[colppn,0] :=  'PPN';
    asgInput.Cells[colHrgAKhir,0] :=  'Harga Akhir';
    asgInput.Cells[coltotal,0] :=  'Total';
    asgInput.Cells[colketerangan,0] :=  'Keterangan';

    asgInput.Cells[colSerial,   0] :=  'Serial Number';
    asgInput.Cells[colSite,     0] :=  'Site Name';
    asgInput.Cells[colOrigin   ,0] :=  'Origin';
    asgInput.Cells[colCluster  ,0] :=  'Cluster';
    asgInput.Cells[colDest     ,0] :=  'Destination';
    asgInput.Cells[colBpid     ,0] :=  'BPID';
    asgInput.Cells[colSatuan   ,0] :=  'Satuan';
  end;
  if idx in [0,2] then begin
//    asgInput.ClearNormalCells;
//    asgInput.ColCount  := 10;
//    asgInput.RowCount  := 3;
//    asgInput.FixedCols := 1;
//    asgInput.FixedRows := 1;
  end;
  if idx in [0,3] then begin
//    asgInput.ClearNormalCells;
//    asgInput.ColCount  := 10;
//    asgInput.RowCount  := 3;
//    asgInput.FixedCols := 1;
//    asgInput.FixedRows := 1;
  end;

  arrangecolsize(idx);
end;

procedure TfrmInputDataJual.setPPN(idx : integer;row : integer = 0);
var i, dari, sampai : integer;
begin
  if row = 0 then begin
    dari  := 1;
    sampai := asgInput.RowCount-2;
  end else begin
    dari := row;
    sampai := row;
  end;

  for i := dari to sampai do begin
    if asgInput.Cells[colPajak, i] = 'Ada' then
      asgInput.Floats[colppn, i] := asgInput.Floats[colharga, i] *  (PPN_default/100) // 0.1
    else asgInput.Floats[colppn, i] := 0;
    asgInput.Floats[colHrgAKhir, i] := asgInput.Floats[colharga, i] + asgInput.Floats[colppn, i];
    asgInput.Floats[coltotal, i] := asgInput.Floats[colHrgAKhir, i] * asgInput.Floats[colqty, i];
  end;
  arrangecolsize(1);
end;

procedure TfrmInputDataJual.updateTotal(idx : integer);
var subtotal,ppn: real; i :integer;
begin
  subtotal := 0; ppn := 0;

  if idx = 1 then begin
    for i := 1 to asgInput.RowCount-2 do begin
      subtotal := subtotal + (asgInput.Floats[ColQty, i] * asgInput.Floats[colHarga, i]);
      PPn := ppn + (asgInput.Floats[ColQty, i] * asgInput.Floats[colPPN, i]);
    end;
  end;
  if idx = 2 then begin
//    for i := 1 to asgInput.RowCount-2 do begin
//      subtotal := subtotal + (asgInput.Floats[ColQty, i] * asgInput.Floats[colHarga, i]);
//      PPn := ppn + (asgInput.Floats[ColQty, i] * asgInput.Floats[colPPN, i]);
//    end;
  end;
  if idx = 3 then begin
//    for i := 1 to asgInput.RowCount-2 do begin
//      subtotal := subtotal + (asgInput.Floats[ColQty, i] * asgInput.Floats[colHarga, i]);
//      PPn := ppn + (asgInput.Floats[ColQty, i] * asgInput.Floats[colPPN, i]);
//    end;
  end;
  txtPpn.FloatValue      := ppn;
  txtSubTotal.FloatValue := subtotal;
  txtTotal.FloatValue    := (ppn+subtotal);
end;

procedure TfrmInputDataJual.asgInputAutoAddRow(Sender: TObject;
  ARow: Integer);
begin
  arrangecolsize(1);
end;

procedure TfrmInputDataJual.asgInputAutoDeleteRow(Sender: TObject;
  ARow: Integer);
begin
  arrangecolsize(1);
  updateTotal(1);
end;

procedure TfrmInputDataJual.asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
  var CanDelete: Boolean);
begin
  CanDelete := not(BrowseMode xor EditMode);
end;

procedure TfrmInputDataJual.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if Not (browseMode xor editMode) then begin
    if txtUnitBisnis.text <> TIPE_DIVISI_LOGISTIC_TEXT then begin
      CanEdit := Acol in [colUraian, COLpAJAK, colharga, colqty, colKeterangan, colSatuan]
    end else begin
      CanEdit := Acol in [colUraian, colSerial, COLpAJAK, colharga, colqty, colKeterangan,
      colDest, colCluster, colSatuan,  colOrigin, colBpid, colsite ]
    end;
  end
  else canEdit := false;
end;

procedure TfrmInputDataJual.asgInputCellValidate(Sender: TObject; ACol,
  ARow: Integer; var Value: string; var Valid: Boolean);
begin
  value := TrimAllString(Value);
  case acol of
    colqty, colharga: begin
      if   asgInput.Cells[colpajak, arow] =  'Ada' then begin
        asgInput.Floats[colppn, arow] := asgInput.Floats[colharga, arow] * 0.1;
      end else begin
        asgInput.Floats[colppn, arow] := 0;
      end;
    end;
  end;
  setppn(1);
  updatetotal(1);
  arrangecolsize(1);
end;

procedure TfrmInputDataJual.asgInputComboChange(Sender: TObject; ACol,
  ARow, AItemIndex: Integer; ASelection: string);
begin
  if (arow > 0) and (arow < asgINput.rowcount-1) then begin
    setPPN(1, arow);
  end;
end;

procedure TfrmInputDataJual.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then HAlign := taCenter
  else if Acol in [ColNo,ColQty, ColHarga, coltotal
  , colppn, colHrgAKhir] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInputDataJual.asgInputGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case ACol of
//    colUraian, colketerangan: AEditor := edMixedCase;
    colQty: AEditor := edPositiveNumeric;
    coltotal, colHrgAKhir,colppn,colHArga    : AEditor := edPositiveFloat;
    colPajak:
    begin
      AEditor:= edComboList;
      asgInput.Combobox.Items.Clear;
      asgInput.Combobox.Items.Add('Ada');
      asgInput.Combobox.Items.Add('Tidak');
    end;
  end;
end;

procedure TfrmInputDataJual.asgInputGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := acol in [ColNo,ColQty,colharga,colppn,colHrgAKhir,coltotal];
  if isfloat then begin
    case ACol of
      ColNo: FloatFormat := '%.0n';
      colqty,colharga,colHrgAKhir,colPPN,coltotal : FloatFormat := '%.2n';
    end;
  end else FloatFormat := '';
end;

procedure TfrmInputDataJual.asgInputKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (asginput.col = colUraian) and (key = #13) then asginput.col := colQty;
end;

procedure TfrmInputDataJual.asgInputMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize(1);
end;

procedure TfrmInputDataJual.asgInputMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize(1);
end;

procedure TfrmInputDataJual.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.

