unit DataBeliInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker, Grids, AdvObj, BaseGrid, AdvGrid;

type
  TfrmInputDataBeli = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    txtNomor: TAdvEdit;
    Label1: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    txtCust: TAdvEdit;
    btnPek: TAdvGlowButton;
    dtpTanggal: TAdvDateTimePicker;
    mmKet: TMemo;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    asgInput: TAdvStringGrid;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label11: TLabel;
    txtSubTotal: TAdvEdit;
    txtPPN: TAdvEdit;
    txtTotal: TAdvEdit;
    chbPPn: TAdvOfficeCheckBox;
    txtPek: TAdvEdit;
    Label2: TLabel;
    txtTempo: TAdvEdit;
    Label7: TLabel;
    mmDesk: TMemo;
    Label8: TLabel;
    lblPPN: TLabel;
    Label10: TLabel;
    txtVendor: TAdvEdit;
    btnVendor: TAdvGlowButton;
    txtUnitBisnis: TAdvEdit;
    Label12: TLabel;
    procedure btnBatalClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure btnPekClick(Sender: TObject);
    procedure asgInputAutoDeleteRow(Sender: TObject; ARow: Integer);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgInputGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgInputGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgInputMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgInputMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chbPPnClick(Sender: TObject);
    procedure asgInputAutoAddRow(Sender: TObject; ARow: Integer);
    procedure btnVendorClick(Sender: TObject);
    procedure asgInputCanAddRow(Sender: TObject; var CanAdd: Boolean);
    procedure asgInputComboChange(Sender: TObject; ACol, ARow,
      AItemIndex: Integer; ASelection: string);
    procedure asgInputDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure dtpTanggalChange(Sender: TObject);
  private
    { Private declarations }
    seqjob, seqCard, seqJual, SeqMst : integer;
    isAdaDataJual,browseMode, EditMode : Boolean;
    dateNow : TDateTime;
    PPN_default : real;
    procedure setgrid;
    procedure arrangecolsize;
    function isValid : Boolean;
    function isSaved : Boolean;
    function isSavedEdit : Boolean;
    procedure loaddata;
    procedure InitForm;
    procedure updateTotal;
    procedure setPPN(row : integer = 0);
    procedure loaddatajual;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(ASeqMst : integer; isEdit: boolean):boolean;
  end;

var
  frmInputDataBeli: TfrmInputDataBeli;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi, UDummy;

const
  colNo          = 0;
  colUraian      = 1;
  colSite        = 2;
  colOrigin      = 3;
  colCluster     = 4;
  colDest        = 5;
  colBpid        = 6;
  colSerial      = 7;
  colqty         = 8;
  colSatuan      = 9;
  colHrgJual     = 10;
  colharga       = 11;
  colPajak       = 12;
  colppn         = 13;
  colHrgAKhir    = 14;
  coltotal       = 15;
  colVendor      = 16;
  colketerangan  = 17;
  colSeqVendor   = 18;
  colSeqDetailJual =19;

{$R *.dfm}

procedure TfrmInputDataBeli.btnPekClick(Sender: TObject);
begin
  setgrid;
  seqJual:=0; seqjob := 0;
  txtCust.Text :=''; txtpek.Text :=''; txtTempo.IntValue := 0; mmDesk.Lines.Clear;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqjob := frmLov.ExecuteJob('', botrue, ' and seq not in (select job_seq from data_beli_master)',
    boNone);
  if seqjob <> 0 then begin
    loaddatajual;
  end;
  updateTotal;
  setPPN;
  arrangecolsize;
end;

procedure TfrmInputDataBeli.btnSimpanClick(Sender: TObject);
begin
  btnSimpan.Enabled := False;
  if (browsemode) and (not editmode) then begin
//    data := Get_faktur_beli_master(seqMst);
//    Application.CreateForm(TfrmPrintPenerimaanSparepart, frmPrintPenerimaanSparepart);
//    frmPrintPenerimaanSparepart.OnClose := frmMainMenu.ChildFormSingle;
//    frmPrintPenerimaanSparepart.Executes(Data);
  end else if (not (browseMode xor editMode)) and (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if editmode then begin
      if isSavedEdit then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else Inform(MSG_UNSUCCESS_UPDATE);
    end else begin
      if isSaved then begin
//        if Confirmed(MSG_PRINT) then begin
//          data := Get_faktur_beli_master(SeqMst);
//          Application.CreateForm(TfrmPrintPenerimaanSparepart, frmPrintPenerimaanSparepart);
//          frmPrintPenerimaanSparepart.OnClose := frmMainMenu.ChildFormSingle;
//          frmPrintPenerimaanSparepart.Executes(Data);
//        end;
        if Confirmed(MSG_ADD_DATA) then begin
          initform; Setgrid; statSimpan := true;
          txtNomor.SetFocus;
        end else ModalResult := mrOk;
      end else begin
        ActiveControl := btnBatal;
      end;
    end;
  end;
  btnSimpan.Enabled := True;
end;

procedure TfrmInputDataBeli.btnVendorClick(Sender: TObject);
begin
  txtVendor.Text:= '';
  seqcard:= 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.ExecuteCard('', botrue, ' and tipe = '+FormatSQLString(TIPE_VENDOR));
  if SeqCard <> 0 then begin
    txtVendor.Text:= EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                     EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmInputDataBeli.chbPPnClick(Sender: TObject);
begin
  setPPN;
  updateTotal;
end;

procedure TfrmInputDataBeli.dtpTanggalChange(Sender: TObject);
begin
  PPN_default := Get_nilai_ppn(dtpTanggal.Date);
  lblPPN.Caption := 'PPN '+FloatToStrFmt( PPN_default )+'%';
  setppn(0);
  updatetotal;
  arrangecolsize;
end;

function TfrmInputDataBeli.execute(ASeqMst: integer; isEdit: boolean): boolean;
begin
  dateNow    := ServerNow(True);
  browseMode := (AseqMst <> 0); editMode := isEdit;
  initform;
  SeqMst      := ASeqMst;
  if editMode then begin
    Self.Caption := 'Edit Data Beli';
    btnSimpan.Caption := '&Simpan';
  end else if browsemode then begin
    Self.Caption := 'Detail Data Beli';
    btnSimpan.Caption := '&Simpan';
    btnSimpan.enabled := false;
  end else begin
    Self.Caption := 'Tambah Data Beli';
    btnSimpan.Caption := '&Simpan';
  end;
  setgrid;
  if browseMode then loaddata;
  run(self);
  result := ModalResult = mrOk;
end;

procedure TfrmInputDataBeli.InitForm;
begin
  txtSetAllEmpty(self);
  dtpTanggal.Date := Datenow;
  seqCard   := 0;
  seqJual    := 0;
  chbPPn.Checked:= False;
  isAdaDataJual := false;
  
  PPN_default := Get_nilai_ppn(dtpTanggal.Date);
  lblPPN.Caption := 'PPN '+FloatToStrFmt( PPN_default )+'%';

  dtpTanggal.Enabled :=  not (browseMode xor editMode);
  btnPek.Enabled     :=  not (browseMode);
  btnVendor.Enabled     :=  not (browseMode xor editMode);
  chbPPn.Enabled     :=  not (browseMode xor editMode);
  txtTempo.readonly  := (browseMode xor editMode);
  mmKet.readonly     := (browseMode xor editMode);
  txtNomor.readonly     := (browseMode xor editMode);
end;

function TfrmInputDataBeli.isSaved: Boolean;
var dtMst : TR_data_Beli_master;
    trDet : TR_data_Beli_detail;
    row, i : integer;
begin
  dtMst.tanggal    := dtpTanggal.Date;
  dtMst.keterangan := TrimAllString(mmKet.Lines.Text);
  dtMst.jual_seq   := seqJual;
  dtMst.job_seq    := seqJob;
  dtMst.card_seq   := seqCard;
  dtMst.subtotal   := txtSubTotal.FloatValue;
  dtMst.ppn        := txtPPN.FloatValue;
  dtMst.total      := txtTotal.FloatValue;
  dtMst.Nomor_po   := TrimAllString(txtNomor.Text);

  SeqMst := Save_data_Beli_master(dtMst);

  if Copy(ErrorSave, 1, 9) = ERR_LOCKED_TABLE then begin
    Result := False;
    Inform(MSG_SERVER_BUSY);
    Exit;
  end;
  if SeqMst = 0 then begin
    Result := False;
    Inform(MSG_UNSUCCESS_SAVING);
    Exit;
  end;

  //save detail faktur beli
  myConnection.BeginSQL;
  try
    for i := 1 to asgInput.RowCount-2 do begin
      trDet.seq:= CreateNewSeq('seq', 'data_Beli_detail');
      trDet.master_seq  := SeqMst;
      trDet.spesifikasi := TrimAllString(asgInput.Cells[colUraian, i]);
      trDet.harga       := asgInput.floats[colharga, i];
      trDet.ppn         := asgInput.floats[colppn, i];
      trDet.qty         := asgInput.floats[colqty, i];
      trDet.keterangan  := TrimAllString(asgInput.cells[colKeterangan, i]);
      trDet.seq_detail_jual := asgInput.ints[colSeqDetailJual, i];

      trDet.serial      := trimallstring(asgInput.Cells[colserial, i]);
      trDet.site_name   := trimallstring(asgInput.Cells[colsite, i]);
      trDet.origin      := trimallstring(asgInput.Cells[colOrigin, i]);
      trDet.clust       := trimallstring(asgInput.Cells[colcluster, i]);
      trDet.destination := trimallstring(asgInput.cells[colDest, i]);
      trDet.bpid        := trimallstring(asgInput.cells[colbpid, i]);
      trDet.satuan      := TrimAllString(asgInput.cells[colSatuan, i]);

      trDet.card_seq    := asgInput.ints[colSeqVendor, i];

      save_data_Beli_detail(trDet);
    end;

    myConnection.EndSQL;
    Result:=True;
  except
    myConnection.undosql;
    myConnection.BeginSQL;
    delete_data_Beli_master(seqMst);
    myConnection.endsql;
    Result:=false;
  end;
end;

function TfrmInputDataBeli.isSavedEdit : Boolean;
var dtMst,dtTemp : TR_data_Beli_master;
    trDet : TR_data_Beli_detail;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
    i, SeqDummy : Integer;
begin
  dtMst := get_data_Beli_master(seqMst);
  dtTemp := dtMst;

  dtMst.tanggal  := dtpTanggal.Date;
  dtMst.keterangan := TrimAllString(mmKet.Lines.Text);
  dtMst.card_seq   := seqCard;
  dtMst.job_Seq    := seqJob;
  dtMst.jual_seq    := seqJual;
  dtMst.subtotal  := txtSubTotal.FloatValue;
  dtMst.ppn       := txtPPN.FloatValue;
  dtMst.total     := txtTotal.FloatValue;
  dtMst.Nomor_po   := TrimAllString(txtNomor.Text);

  try
    myConnection.BeginSQL;
    update_data_Beli_master(dtMst);
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result:=False;
    Exit;
  end;

  //save detail faktur beli
  myConnection.BeginSQL;
  try
    SeqDummy := save_dummy_data_Beli_master(dtTemp);
    save_dummy_data_Beli_detail(Seqdummy,seqMst);

    delete_data_Beli_detail(SeqMst);

    // save ke log file
    Ket.IsEdit    := True;
    Ket.NamaTrans := 'Data Beli';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := '';
    Ket.NoTrans   := '';
    Ket.TglTrans  := dtTemp.tanggal;

    Log.Nama_Tabel := 'data_Beli_master';
    Log.Tanggal    := dateNow;
    Log.Seq_Dummy  := SeqDummy;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := seqMst;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := '';
    Save_Log_File(Log);

    for i := 1 to asgInput.RowCount-2 do begin
      trDet.seq:= CreateNewSeq('seq', 'data_Beli_detail');
      trDet.master_seq  := SeqMst;
      trDet.spesifikasi := TrimAllString(asgInput.Cells[colUraian, i]);
      trDet.harga       := asgInput.floats[colharga, i];
      trDet.ppn         := asgInput.floats[colppn, i];
      trDet.qty         := StrToIntDef( asgInput.cells[colqty, i],0);
      trDet.keterangan  := TrimAllString(asgInput.cells[colKeterangan, i]);
      trDet.seq_detail_jual := asgInput.ints[colSeqDetailJual, i];

      trDet.serial      := trimallstring(asgInput.Cells[colserial, i]);
      trDet.site_name   := trimallstring(asgInput.Cells[colsite, i]);
      trDet.origin      := trimallstring(asgInput.Cells[colOrigin, i]);
      trDet.clust       := trimallstring(asgInput.Cells[colppn, i]);
      trDet.destination := trimallstring(asgInput.cells[colDest, i]);
      trDet.bpid        := trimallstring(asgInput.cells[colbpid, i]);
      trDet.satuan      := TrimAllString(asgInput.cells[colSatuan, i]);

      trDet.card_seq    := asgInput.ints[colSeqVendor, i];
      save_data_Beli_detail(trDet);
    end;

    myConnection.EndSQL;
    Result:=True;
  except
    myConnection.undosql;
    myConnection.BeginSQL;
    update_data_Beli_master(dtTemp);
    myConnection.endsql;
    Result:=false;
  end;
end;



function TfrmInputDataBeli.isValid: Boolean;
var i : integer;
begin
  Result := True;
  if TrimAllString(txtNomor.Text) = '' then begin
    inform('No. PO harus diisi.');
    txtNomor.SetFocus;
    Result := false;
    Exit;
  end;

  if seqJob= 0 then begin
    inform('Pekerjaan harus dipilih.');
    btnPek.SetFocus;
    Result := false;
    Exit;
  end;

  i := 0;
  While (i <= asgInput.RowCount-2) do begin
    i := i + 1;
    if (i <> asgInput.RowCount) and ((asgInput.Floats[ColQty,i] = 0) and (asgInput.Cells[colUraian,i]
      = '')) then begin
      if (asgInput.RowCount > 3) then begin
        asgInput.RemoveRows(i,1);
        i:= i - 1;
      end else asgInput.ClearRows(i,1);
    end;
    if (asgInput.RowCount = 3) and (asgInput.Cells[colUraian, 1] = '') then begin
      ArrangeColSize;
      Inform('Tidak ada uraian yang diinput.');
      Result := False;
      Exit;
    end;
  end;
  arrangecolsize;

  for i := 1 to asgInput.rowcount-2 do begin
    if TrimAllString(asgInput.Cells[colUraian, i]) = '' then begin
      Inform('Uraian belum diinput.');
      asgInput.Col  := colUraian;
      asgInput.Row  := i;
      Result := False;
      Exit;
    end;

    if (asgInput.floats[colqty, i] = 0) then begin
      Inform('Qty belum diinput.');
      asgInput.Col  := colqty;
      asgInput.Row  := i;
      Result := False;
      Exit;
    end;

    if (asgInput.floats[colharga, i] = 0) then begin
      Inform('Harga belum diinput.');
      asgInput.Col  := colharga;
      asgInput.Row  := i;
      Result := False;
      Exit;
    end;

    if (asgInput.ints[colSeqVendor, i] = 0) then begin
      inform('Vendor harus dipilih.');
      asgInput.Col  := colvendor;
      asgInput.Row  := i;
      Result := false;
      Exit;
    end;                
  end;
end;

procedure TfrmInputDataBeli.loaddata;
var dtMst : TR_data_Beli_master;
    dtjual: TR_data_jual_master;
    dtPek : Tr_master_job;
    arDet : AR_data_Beli_detail;
    row, i : integer;
begin
  isAdaDataJual := false;
  txtNomor.readonly:= false;
  txtTempo.readonly:= false;

  dtMst := get_data_Beli_master(seqMst);
  seqJual := dtMst.jual_seq;
  seqjob  := dtMst.job_seq;
  seqCard := dtMst.card_seq;

  dtPek:= Get_master_job(seqJob);
  txtPek.text := EkstrakString(ListJobGlobal.Values[IntToStr(seqJob)],'#',1) + ' - ' +
                 EkstrakString(ListJobGlobal.Values[IntToStr(seqJob)],'#',2);

  txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(dtPek.card_seq)],'#',1) + ' - ' +
                  EkstrakString(ListCardGlobal.Values[IntToStr(dtPek.card_seq)],'#',2);

  txtVendor.Text:= EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                   EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);

  dtpTanggal.Date   := dtMst.tanggal;
  mmKet.Lines.Text  := dtMst.keterangan;
  txtTempo.IntValue := dtMst.tempo_byr;
  txtNomor.Text     := dtMst.nomor_po;
                         
  PPN_default := Get_nilai_ppn(dtpTanggal.Date);
  lblPPN.Caption := 'PPN '+FloatToStrFmt( PPN_default )+'%';
  
  dtPek:= Get_master_job(seqJob);
  if seqJual <> 0 then begin
    isAdaDataJual := true;
    txtNomor.readonly:= true;
    txtTempo.readonly:= true;
    dtjual := Get_data_jual_master(seqJual);

    txtNomor.Text     := dtJual.nomor_po;
    txtTempo.IntValue := dtJual.tempo_byr;

    if dtpek.unit_bisnis = TIPE_DIVISI_LOGISTIC then
      txtUnitBIsnis.text := tiPE_DIVISI_LOGISTIC_text
    else if dtpek.unit_bisnis = TIPE_DIVISI_TRADING then
      txtUnitBIsnis.text := TIPE_DIVISI_TRADING_text
    else if dtpek.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
      txtUnitBIsnis.text := TIPE_DIVISI_CONSTRUCTION_text;

  end;

  mmDesk.Lines.Text := dtPek.deskripsi;

  txtSubTotal.FloatValue := dtMst.subtotal;
  txtPPN.FloatValue      := dtMst.ppn;
//  chbPPn.Checked         := dtMst.ppn <> 0;
  txtTotal.FloatValue    := dtMst.total;

  arDet := Get_Arr_data_Beli_detail(seqMst);
  for i := 0 to Length(ardet)-1 do begin
    if i > 0 then asgInput.AddRow;
    row := asgInput.RowCount-2;
    if arDet[i].ppn > 0 then begin
      asgInput.Cells[colPajak, row] := 'Ada'
    end
    else begin
      asgInput.Cells[colPajak, row] := 'Tidak';
    end;

    asgInput.Cells[colserial, row] := ardet[i].serial;
    asgInput.cells[colOrigin,      row] := ardet[i].origin;
    asgInput.Cells[colCluster     ,row] := ardet[i].clust;
    asgInput.cells[colDest,        row] := ardet[i].destination;
    asgInput.Cells[colBpid        ,row] := ardet[i].bpid;
    asgInput.ints[colSeqDetailJual, row] := ardet[i].seq_detail_jual;
    asgInput.Cells[colsite,        row] := ardet[i].site_name;
    asgInput.Cells[colUraian ,     row] := ardet[i].Spesifikasi;
    asgInput.floats[colharga      ,row] := ardet[i].harga;
    asgInput.floats[colppn        ,row] := ardet[i].ppn;
    asgInput.floats[colqty        ,row] := ardet[i].qty;
    asgInput.Cells[colketerangan  ,row] := ardet[i].keterangan;
    asgInput.Cells[colsatuan      ,row] := ardet[i].satuan;
    asgInput.ints[colSeqVendor    ,row] := ardet[i].card_seq;
    asgInput.Cells[colVendor      ,row] := EkstrakString(ListCardGlobal.Values[IntToStr(ardet[i].card_seq)],'#',2);
  end;
  setPPN;
  updatetotal;
  arrangecolsize;
end;

procedure TfrmInputDataBeli.loaddatajual;
var dtPek : TR_master_job;
    dtJual :  TR_data_jual_master;
    arDet : ar_data_jual_detail;
    row, i : integer;
begin
  isAdaDataJual := false;
  txtNomor.readonly:= false;
  txtTempo.readonly:= false;
  dtPek:= Get_master_job(seqJob);
  txtPek.text := EkstrakString(ListJobGlobal.Values[IntToStr(seqJob)],'#',1) + ' - ' +
                 EkstrakString(ListJobGlobal.Values[IntToStr(seqJob)],'#',2);
  txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(dtPek.card_seq)],'#',1) + ' - ' +
                  EkstrakString(ListCardGlobal.Values[IntToStr(dtPek.card_seq)],'#',2);
  mmDesk.Lines.Text := dtPek.deskripsi;

  if dtpek.unit_bisnis = TIPE_DIVISI_LOGISTIC then
    txtUnitBIsnis.text := tiPE_DIVISI_LOGISTIC_text
  else if dtpek.unit_bisnis = TIPE_DIVISI_TRADING then
    txtUnitBIsnis.text := TIPE_DIVISI_TRADING_text
  else if dtpek.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
    txtUnitBIsnis.text := TIPE_DIVISI_CONSTRUCTION_text;

  dtjual := Get_data_jual_master(0, seqJob);
  if dtjual.seq <> 0 then  begin
    isAdaDataJual := true;
    txtNomor.readonly:= true;
    txtTempo.readonly:= true;
    arDet:= Get_Arr_data_jual_detail(dtJual.seq);

    seqJual := dtJual.seq;
    seqCard := dtjual.card_seq;
    txtNomor.Text := dtJual.nomor_po;
    txtTempo.IntValue := dtJual.tempo_byr;

//    chbPPn.Checked := dtJual.ppn>0;
    for i := 0 to Length(arDet)-1 do begin
      if i > 0 then asgInput.AddRow;
      row := asgInput.RowCount-2;
//      if arDet[i].ppn > 0 then begin
        asgInput.Cells[colPajak, row] := 'Ada';
//      end
//      else begin
//        asgInput.Cells[colPajak, row] := 'Tidak';
//      end;
      asgInput.cells[colOrigin,      row] := ardet[i].origin;
      asgInput.Cells[colCluster     ,row] := ardet[i].clust;
      asgInput.cells[colDest,        row] := ardet[i].destination;
      asgInput.Cells[colBpid        ,row] := ardet[i].bpid;
      asgInput.ints[colSeqDetailJual, row] := ardet[i].seq;
      asgInput.Cells[colsite,        row] := ardet[i].site_name;
      asgInput.Cells[colSerial,        row] := ardet[i].serial;


      asgInput.Cells[colUraian ,     row] := ardet[i].Spesifikasi;
      asgInput.floats[colharga      ,row] := 0;//ardet[i].harga;
      asgInput.floats[colppn        ,row] := 0;//ardet[i].ppn;
      asgInput.floats[colqty        ,row] := ardet[i].qty;
      asgInput.Cells[colketerangan  ,row] := ardet[i].keterangan;
      asgInput.Cells[colsatuan      ,row] := ardet[i].satuan;
//    asgInput.ints[colSeqVendor    ,row] := ardet[i].card_seq;
//    asgInput.Cells[colVendor      ,row] := EkstrakString(ListCardGlobal.Values[IntToStr(ardet[
//      i].card_seq)],'#',2);
    end;
  end;
  setPpn;
  updatetotal;
  arrangecolsize;
end;

procedure TfrmInputDataBeli.arrangecolsize;
begin
  asgInput.AutoNumberCol(colNo);
  asgInput.Cells[colUraian,asgInput.RowCount-1]:='Total';
  asgInput.FloatingFooter.ColumnCalc[colqty]  :=acSUM;
  asgInput.FloatingFooter.ColumnCalc[colppn]  :=acSUM;
  asgInput.FloatingFooter.ColumnCalc[colTotal]  :=acSUM;
  asgInput.AutoSizeColumns(true);
  asgInput.ColWidths[colHrgJual]:=0;
  if txtUnitBisnis.text <> TIPE_DIVISI_LOGISTIC_TEXT then begin
    asgInput.colwidths[colOrigin] := 0;
    asgInput.colwidths[colCluster] := 0;
    asgInput.colwidths[colDest] := 0;
    asgInput.colwidths[colBpid] := 0;
    asgInput.colwidths[colsite] := 0;
    asgInput.colwidths[colserial] := 0;
  end;
  asgInput.colwidths[colSeqVendor] := 0;
  asgInput.colwidths[colSeqDetailJual] := 0;
end;

procedure TfrmInputDataBeli.setgrid;
begin
  asgInput.ClearNormalCells;
  asgInput.ColCount  := 21;
  asgInput.RowCount  := 3;
  asgInput.FixedCols := 1;
  asgInput.FixedRows := 1;


  asgInput.Cells[colNo,0] :=  'No.';
  asgInput.Cells[colUraian,0] :=  'Uraian';
  asgInput.Cells[colqty,0] :=  'Qty';
  asgInput.Cells[colPajak,0] :=  'Pajak';
  asgInput.Cells[colharga,0] :=  'Harga';
  asgInput.Cells[colppn,0] :=  'PPN';
  asgInput.Cells[colHrgAKhir,0] :=  'Harga Akhir';
  asgInput.Cells[coltotal,0] :=  'Total';
  asgInput.Cells[colketerangan,0] :=  'Keterangan';

  asgInput.Cells[colSerial,   0] :=  'Serial Number';
  asgInput.Cells[colSite,     0] :=  'Site Name';
  asgInput.Cells[colOrigin   ,0] :=  'Origin';
  asgInput.Cells[colCluster  ,0] :=  'Cluster';
  asgInput.Cells[colDest     ,0] :=  'Destination';
  asgInput.Cells[colBpid     ,0] :=  'BPID';
  asgInput.Cells[colSatuan   ,0] :=  'Satuan';

  asgInput.Cells[colSeqVendor,0] :=  'SeqVend';
  asgInput.Cells[colVendor,0] :=  'Vendor';
  asgInput.Cells[colSeqDetailJual,0] :=  'SeqDetJual';

  arrangecolsize;
end;

procedure TfrmInputDataBeli.setPPN(row : integer = 0);
var i, dari, sampai : integer;
begin
  if row = 0 then begin
    dari  := 1;
    sampai := asgInput.RowCount-2;
  end else begin
    dari := row;
    sampai := row;
  end;

  for i := dari to sampai do begin
    if asgInput.Cells[colPajak, i] = 'Ada' then
      asgInput.Floats[colppn, i] := asgInput.Floats[colharga, i] *  (PPN_default/100) // 0.1
    else asgInput.Floats[colppn, i] := 0;
    asgInput.Floats[colHrgAKhir, i] := asgInput.Floats[colharga, i] + asgInput.Floats[colppn, i];
    asgInput.Floats[coltotal, i] := asgInput.Floats[colHrgAKhir, i] * asgInput.Floats[colqty, i];
  end;
  arrangecolsize;
end;

procedure TfrmInputDataBeli.updateTotal;
var subtotal,ppn: real; i :integer;
begin
  subtotal := 0; ppn := 0;
  for i := 1 to asgInput.RowCount-2 do begin
    subtotal := subtotal + (asgInput.Floats[ColQty, i] * asgInput.Floats[colHarga, i]);
    PPn := ppn + (asgInput.Floats[ColQty, i] * asgInput.Floats[colPPN, i]);
  end;
  txtPpn.FloatValue      := ppn;
  txtSubTotal.FloatValue := subtotal;
  txtTotal.FloatValue    := (ppn+subtotal);
end;

procedure TfrmInputDataBeli.asgInputAutoAddRow(Sender: TObject;
  ARow: Integer);
begin
  arrangecolsize;
end;

procedure TfrmInputDataBeli.asgInputAutoDeleteRow(Sender: TObject;
  ARow: Integer);
begin
  arrangecolsize;
  updateTotal;
end;

procedure TfrmInputDataBeli.asgInputCanAddRow(Sender: TObject;
  var CanAdd: Boolean);
begin
  CanAdd:= not isAdaDataJual ;//not(BrowseMode xor EditMode);
end;

procedure TfrmInputDataBeli.asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
  var CanDelete: Boolean);
begin
  CanDelete := not isAdaDataJual ;//not(BrowseMode xor EditMode);
end;

procedure TfrmInputDataBeli.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if Not (browseMode xor editMode) then  begin
    if isAdaDataJual then
      CanEdit := Acol in [colharga, colvendor, COLpAJAK, colKeterangan]
    else CanEdit := Acol in [colUraian, colvendor, colqty, colharga, COLpAJAK, colKeterangan];
  end
  else canEdit := false;
end;

procedure TfrmInputDataBeli.asgInputCellValidate(Sender: TObject; ACol,
  ARow: Integer; var Value: string; var Valid: Boolean);
var seqVendor : integer;
    dtCard : tr_master_card;
begin
  value := TrimAllString(Value);
  case acol of
    colharga: begin
      if  asgINput.cells[colpajak, arow]='Ada' then begin
        asgInput.Floats[colppn, arow] := asgInput.Floats[colharga, arow] * 0.1;
      end else begin
        asgInput.Floats[colppn, arow] := 0;
      end;
    end;

    colVendor: begin
      seqVendor  := Get_Seq_By_KodeNama(Value,botrue, 'master_card', ' and tipe = '+
        FormatSQLString(TIPE_VENDOR));
      if seqVendor = 0 then begin
        Application.CreateForm(TfrmLov, frmLov);
        frmLov.OnClose := frmMainMenu.ChildFormSingle;
        seqVendor := frmLov.ExecuteCard(Value, botrue, ' and tipe = '+FormatSQLString(TIPE_VENDOR));
      end;
      if seqVendor <> 0 then begin
        dtCard := Get_master_card(seqVendor);
        if dtCard.seq > 0 then begin
          asgInput.Cells[colVendor,    arow] := dtCard.nama;
          asgInput.Ints[colSeqVendor,  arow] := dtCard.seq;
          value := dtCard.nama;
        end;
      end else begin
        asgInput.Cells[colVendor,    arow] := '';
        asgInput.Ints[colSeqVendor,  arow] := 0;
      end;


    end;
  end;
  setppn;
  updatetotal;
  arrangecolsize;
end;

procedure TfrmInputDataBeli.asgInputComboChange(Sender: TObject; ACol,
  ARow, AItemIndex: Integer; ASelection: string);
begin
  if (arow > 0) and (arow < asgINput.rowcount-1) then begin
    setPPN(arow);
  end;
end;

procedure TfrmInputDataBeli.asgInputDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
var seqVendor:integer;
    dtcard : tr_master_card;
begin
  if (arow > 0) and (arow <  asginput.rowcount-1) and (not (browsemode xor editmode)) then begin
    case acol of                                            
      colVendor : begin
        Application.CreateForm(TfrmLov, frmLov);
        frmLov.OnClose := frmMainMenu.ChildFormSingle;
        seqvendor := frmLov.ExecuteCard('', botrue, ' and tipe = '+FormatSQLString(TIPE_VENDOR));
        if Seqvendor <> 0 then begin
          dtCard := Get_master_card(seqVendor);
          if dtCard.seq > 0 then begin
            asgInput.Cells[colVendor,    arow] := dtCard.nama;
            asgInput.Ints[colSeqVendor,  arow] := dtCard.seq;
          end;
        end;
      end;
    end;
    arrangecolsize;
  end;

end;

procedure TfrmInputDataBeli.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then HAlign := taCenter
  else if Acol in [ColNo,ColQty, ColHarga,colHrgJual, coltotal
  , colppn, colHrgAKhir] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmInputDataBeli.asgInputGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case ACol of
    colUraian, colketerangan: AEditor := edMixedCase;
    colQty: AEditor := edPositiveNumeric;
    coltotal, colHrgAKhir,colppn,colHArga, colHrgJual  : AEditor := edPositiveFloat;
    colPajak:
    begin
      AEditor:= edComboList;
      asgInput.Combobox.Items.Clear;
      asgInput.Combobox.Items.Add('Ada');
      asgInput.Combobox.Items.Add('Tidak');
    end;
  end;
end;

procedure TfrmInputDataBeli.asgInputGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := acol in [ColNo,ColQty,colharga,colhrgjual,colppn,colHrgAKhir,coltotal];
  if isfloat then begin
    case ACol of
      colqty,ColNo: FloatFormat := '%.0n';
      colharga,colHrgAKhir,colPPN,coltotal, colhrgjual : FloatFormat := '%.2n';
    end;
  end else FloatFormat := '';
end;

procedure TfrmInputDataBeli.asgInputMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize;
end;

procedure TfrmInputDataBeli.asgInputMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize;
end;

procedure TfrmInputDataBeli.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
