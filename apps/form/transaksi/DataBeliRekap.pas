unit DataBeliRekap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls;

type
  TfrmRekapDataBeli = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnHapus: TAdvGlowButton;
    btnTambah: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label19: TLabel;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    btnDetail: TAdvGlowButton;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    txtVendor: TAdvEdit;
    btnVendor: TAdvGlowButton;
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapSelectionChanged(Sender: TObject; ALeft, ATop, ARight,
      ABottom: Integer);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnDetailClick(Sender: TObject);
    procedure btnVendorClick(Sender: TObject);
  private
    seqCard,seqVendor, AMenuid : integer;
    datenow: TDate;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
    function isDelete(alasanhapus: string) :boolean;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmRekapDataBeli: TfrmRekapDataBeli;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, StrUtils, LOV, DataBeliInput;

{$R *.dfm}

const
  ColNo         = 0;
  ColTanggal    = 1;
  ColNomor      = 2;
  colcust       = 3;
  colvendor     = 4;
  colUnit       = 5;
  colTempo      = 6;
  ColSubtotal   = 7;
  ColPpn        = 8;
  ColTotal      = 9;
  ColKet        = 10;
  colUserId     = 11;
  colTglInput   = 12;
  ColSeq        = 13;


procedure TfrmRekapDataBeli.btnVendorClick(Sender: TObject);
begin
  seqVendor := 0;
  txtVendor.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqVendor := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_VENDOR));
  if seqVendor <> 0 then begin
    txtVendor.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqVendor)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqVendor)],'#',2);
  end;
end;

procedure TfrmRekapDataBeli.arrangecolsize;
begin
  asgRekap.AutoNumberCol(colno);
  asgrekap.cells[colTempo, asgrekap.rowcount-1]:= 'Total';
  asgrekap.floatingfooter.columncalc[colsubtotal] :=  acsum;
  asgrekap.floatingfooter.columncalc[colppn]      :=  acsum;
  asgrekap.floatingfooter.columncalc[coltotal]    :=  acsum;
  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
  end;
  asgRekap.ColWidths[colVendor]   := 0;
end;

procedure TfrmRekapDataBeli.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  btnEdit.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
  btnHapus.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
  btnDetail.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
//  btnCetak.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
end;

procedure TfrmRekapDataBeli.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow > 0) then btndetail.Click;
end;

procedure TfrmRekapDataBeli.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [ColSubtotal,ColPpn,ColTotal,colSeq, colNo] then HAlign := taRightJustify;
end;

procedure TfrmRekapDataBeli.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
//  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
//    AFont.Color := clRed;
end;

procedure TfrmRekapDataBeli.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
//  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
//    AFont.Color := clRed;
end;

procedure TfrmRekapDataBeli.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, ColSubtotal,ColPpn,ColTotal,colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol in [colseq] then FloatFormat    := '%.0f'
    else FloatFormat := '%.2n'
  end;
end;

procedure TfrmRekapDataBeli.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapDataBeli.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapDataBeli.asgRekapSelectionChanged(Sender: TObject;
  ALeft, ATop, ARight, ABottom: Integer);
begin
  asgRekap.OnClickCell(Self,asgRekap.Row,asgRekap.Col);
end;

procedure TfrmRekapDataBeli.btnHapusClick(Sender: TObject);
var Tanggal : tdate;
    alasanhapus : string;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.Row] <> 0 then begin
    if Confirmed('No. Transaksi "'+asgRekap.Cells[ColNomor, asgRekap.Row]+'" akan dihapus ?') then begin
      alasanhapus := TrimAllString(UGeneral.InputNote);
      if alasanhapus = '' then begin
        Inform('Catatan hapus tidak boleh kosong.');
        Exit;
      end;
      if isDelete(alasanhapus) then begin
        inform(MSG_SUCCESS_DELETING);
        if AsgRekap.RowCount > 3  then AsgRekap.RemoveRows(AsgRekap.Row,1)
        else AsgRekap.ClearRows(AsgRekap.Row,1);
        Arrangecolsize;
        AsgRekap.Col := colNomor;
        AsgRekap.Row := 1;
      end else Inform(MSG_UNSUCCESS_DELETING);
    end;
  end;
end;

procedure TfrmRekapDataBeli.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Rekap Data Beli');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 2;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 2;
  arrangecolsize;
end;

procedure TfrmRekapDataBeli.btnCustClick(Sender: TObject);
begin
  seqCard := 0;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmRekapDataBeli.btnDetailClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    Application.CreateForm(TfrmInputDataBeli, frmInputDataBeli);
    frmInputDataBeli.OnClose := frmMainMenu.ChildFormSingle;
    frmInputDataBeli.execute(asgRekap.Ints[colseq, asgRekap.Row], false);
  end;
end;

procedure TfrmRekapDataBeli.btnEditClick(Sender: TObject);
begin
  if not BisaEdit(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    Application.CreateForm(TfrmInputDataBeli, frmInputDataBeli);
    frmInputDataBeli.OnClose := frmMainMenu.ChildFormSingle;
    if frmInputDataBeli.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then btnLoad.Click;
  end;
end;

procedure TfrmRekapDataBeli.btnLoadClick(Sender: TObject);
begin
  loaddata;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmRekapDataBeli.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapDataBeli.btnTambahClick(Sender: TObject);
begin
  if not BisaNambah(AmenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  Application.CreateForm(TfrmInputDataBeli, frmInputDataBeli);
  frmInputDataBeli.OnClose := frmMainMenu.ChildFormSingle;
  if (frmInputDataBeli.execute(0,false)) or (frmInputDataBeli.statSimpan = true)  then btnLoad.Click;
end;

procedure TfrmRekapDataBeli.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapDataBeli.Execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  datenow  :=ServerNow;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmRekapDataBeli.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmRekapDataBeli.initform;
begin
  dtpAwal.Date    := datenow;
  dtpAkhir.Date    := datenow;
  seqVendor       := 0;
  txtCust.Text    := '';
  seqCard         := 0;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

function TfrmRekapDataBeli.isDelete(alasanhapus: string): boolean;
var MstTmp  : TR_data_Beli_master;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
    SeqMst, SeqDummy : Integer;
begin
  myConnection.BeginSQL;
  try
    SeqMst   := asgRekap.Ints[ColSeq, asgRekap.Row];
    MstTmp   := Get_data_Beli_master(seqMst);
    SeqDummy := Save_Dummy_data_Beli_master(MstTmp);

    Save_Dummy_data_Beli_detail(SeqDummy,SeqMst);
    Delete_data_Beli_detail(MstTmp.Seq);
    Delete_data_Beli_master(MstTmp.Seq);

    Ket.IsEdit    := False;
    Ket.NamaTrans := 'Data Beli';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := EkstrakString(ListCardGlobal.Values[inttostr(MstTmp.card_seq)], '#', 2);
    Ket.NoTrans   := '';
    Ket.TglTrans  := MstTmp.Tanggal;

    Log.Nama_Tabel := 'data_Beli_master';
    Log.Tanggal    := ServerNow;
    Log.Seq_Dummy  := SeqDummy;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := 0;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := alasanhapus;
    Save_Log_File(Log);

    Result := True;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

procedure TfrmRekapDataBeli.loaddata;
var buffer      : _recordset;
    i,Row       : Integer;
    Filter, unitTmp,Divisi : string;
    Periode : TR_FilterPeriode;
begin
  asgRekap.RowCount := 3;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  periode.FPeriodeAwal      := dtpAwal.Date;
  periode.FPeriodeAkhir     := dtpAkhir.Date;
  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;

  Divisi := '';
  case cmbDes.ItemIndex of
    1 : Divisi := TIPE_DIVISI_LOGISTIC;
    2 : Divisi := TIPE_DIVISI_TRADING;
    3 : Divisi := TIPE_DIVISI_CONSTRUCTION;
  end;

  filter := filter +ifthen(seqCard<>0,' and bm.card_seq = '+formatsqlnumber(seqCard));
  
//  (seq = 0)  (tanggal = 1)  (jual_seq = 2)  (card_seq = 3)  (subtotal = 4)  (ppn = 5)  (total =
//6)  (keterangan = 7)  (user_id = 8)  (tgl_input = 9)  (user_edit = 10)  (tgl_edit = 11)   12

  Load_data_Beli_master(buffer,periode,0,seqVendor, divisi, Filter);
  for i := 0 to buffer.RecordCount - 1 do begin
    AdvProgressBar1.Position := round((i+1)/buffer.RecordCount*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -2;
    Asgrekap.dates[ColTanggal  , row] := BufferToDateTime(buffer.Fields[1].Value)  ;
    Asgrekap.cells[ColNomor    , row] := BufferToString(buffer.Fields[13].Value)  ;
    Asgrekap.cells[colcust     , row] := EkstrakString(ListCardGlobal.Values[buffertoString(
      buffer.Fields[15].Value)],'#',2);

    Asgrekap.cells[colvendor     , row] := EkstrakString(ListCardGlobal.Values[buffertoString(
      buffer.Fields[3].Value)],'#',2);

    unitTmp := EkstrakString(ListJobGlobal.Values[buffertoString(buffer.Fields[12].Value)],'#',3);
    if unitTmp = TIPE_DIVISI_LOGISTIC then
      Asgrekap.cells[colUnit     , row] :=TIPE_DIVISI_LOGISTIC_text
    else if unitTmp = TIPE_DIVISI_TRADING then
      Asgrekap.cells[colUnit     , row] :=TIPE_DIVISI_TRADING_text
    else if unitTmp = TIPE_DIVISI_CONSTRUCTION then
      Asgrekap.cells[colUnit     , row] :=TIPE_DIVISI_CONSTRUCTION_text;

    Asgrekap.cells[colTempo     , row] := BufferTostring(buffer.Fields[14].Value)+' hari';
    Asgrekap.Floats[colSubtotal   , row] := BufferToFloat(buffer.Fields[4].Value);
    Asgrekap.Floats[colPpn        , row] := BufferToFloat(buffer.Fields[5].Value);
    Asgrekap.Floats[colTotal      , row] := BufferToFloat(buffer.Fields[6].Value);
    Asgrekap.cells[colKet      , row] := BufferToString(buffer.Fields[7].Value);
    Asgrekap.cells[colUserId   , row] := BufferToString(buffer.Fields[8].Value);
    Asgrekap.dates[colTglInput  , row] := BufferToDateTime(buffer.Fields[9].Value);
    Asgrekap.ints[colSeq        , row] := BufferToINteger(buffer.Fields[0].Value);

    buffer.MoveNext;
  end;
  buffer.Close;
  AdvProgressBar1.Hide;
  arrangecolsize;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapDataBeli.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 15;
  asgRekap.RowCount := 3;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;

  arrangecolsize;
end;

procedure TfrmRekapDataBeli.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

end.
