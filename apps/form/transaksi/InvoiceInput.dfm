object frmInputInvoice: TfrmInputInvoice
  Left = 130
  Top = 170
  BorderStyle = bsDialog
  Caption = 'Tambah Invoice'
  ClientHeight = 427
  ClientWidth = 1017
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 172
    Width = 27
    Height = 13
    Caption = 'Nama'
  end
  object Label22: TLabel
    Left = 16
    Top = 201
    Width = 26
    Height = 13
    Caption = 'State'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1017
    Height = 427
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 761
    DesignSize = (
      1017
      427)
    object Label6: TLabel
      Left = 9
      Top = 352
      Width = 56
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Keterangan'
      ExplicitTop = 336
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 2
      Width = 1001
      Height = 91
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 2
      ExplicitWidth = 953
      object Label5: TLabel
        Left = 230
        Top = 40
        Width = 64
        Height = 13
        Caption = 'Jumlah Bayar'
        Visible = False
      end
      object Label23: TLabel
        Left = 230
        Top = 13
        Width = 38
        Height = 13
        Caption = 'Tanggal'
      end
      object Label2: TLabel
        Left = 6
        Top = 67
        Width = 46
        Height = 13
        Caption = 'Customer'
      end
      object Label3: TLabel
        Left = 400
        Top = 9
        Width = 42
        Height = 13
        Caption = 'Deskripsi'
      end
      object Label7: TLabel
        Left = 6
        Top = 13
        Width = 55
        Height = 13
        Caption = 'No. Invoice'
      end
      object Label8: TLabel
        Left = 6
        Top = 39
        Width = 59
        Height = 13
        Caption = 'No. Kwitansi'
      end
      object lblNoInv: TLabel
        Left = 116
        Top = 13
        Width = 97
        Height = 13
        Caption = '/INV-JMG/BIT/III/20'
      end
      object lblNoKw: TLabel
        Left = 116
        Top = 40
        Width = 97
        Height = 13
        Caption = '/INV-JMG/BIT/III/20'
      end
      object Label12: TLabel
        Left = 401
        Top = 67
        Width = 23
        Height = 13
        Caption = 'Bank'
      end
      object txtCust: TAdvEdit
        Left = 71
        Top = 63
        Width = 292
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        ReadOnly = True
        TabOrder = 0
        Visible = True
        Version = '2.9.2.1'
      end
      object btnCust: TAdvGlowButton
        Left = 369
        Top = 63
        Width = 18
        Height = 20
        Caption = '...'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 2
        OnClick = btnCustClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object dtpTanggal: TAdvDateTimePicker
        Left = 279
        Top = 9
        Width = 84
        Height = 21
        Date = 43870.000000000000000000
        Time = 43870.000000000000000000
        Kind = dkDate
        TabOrder = 1
        BorderStyle = bsSingle
        Ctl3D = True
        DateTime = 43870.000000000000000000
        Version = '1.2.0.0'
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
      end
      object txtJumlah: TAdvEdit
        Left = 300
        Top = 36
        Width = 63
        Height = 21
        EditAlign = eaRight
        EditType = etMoney
        ReturnIsTab = True
        Precision = 2
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 3
        Text = '0,00'
        Visible = False
        Version = '2.9.2.1'
      end
      object sttNo: TStaticText
        Left = 552
        Top = 17
        Width = 30
        Height = 17
        Caption = 'sttNo'
        TabOrder = 4
        Visible = False
      end
      object mmDesk: TMemo
        Left = 448
        Top = 9
        Width = 257
        Height = 47
        ScrollBars = ssVertical
        TabOrder = 5
      end
      object txtNoInv: TAdvEdit
        Left = 71
        Top = 9
        Width = 40
        Height = 21
        EditAlign = eaRight
        EditType = etNumeric
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 3
        ReadOnly = True
        TabOrder = 6
        Text = '0'
        Visible = True
        Version = '2.9.2.1'
      end
      object txtNoKwitansi: TAdvEdit
        Left = 71
        Top = 36
        Width = 40
        Height = 21
        EditAlign = eaRight
        EditType = etNumeric
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 3
        ReadOnly = True
        TabOrder = 7
        Text = '0'
        Visible = True
        Version = '2.9.2.1'
      end
      object GroupBox3: TGroupBox
        Left = 711
        Top = 6
        Width = 210
        Height = 62
        TabOrder = 8
        object Label15: TLabel
          Left = 7
          Top = 13
          Width = 45
          Height = 13
          Caption = 'No. BAST'
        end
        object Label16: TLabel
          Left = 7
          Top = 39
          Width = 46
          Height = 13
          Caption = 'No. BAUT'
        end
        object lblNoBast: TLabel
          Left = 105
          Top = 13
          Width = 97
          Height = 13
          Caption = '/INV-JMG/BIT/III/20'
        end
        object lblNoBaut: TLabel
          Left = 103
          Top = 39
          Width = 97
          Height = 13
          Caption = '/INV-JMG/BIT/III/20'
        end
        object txtNoBaut: TAdvEdit
          Left = 58
          Top = 35
          Width = 40
          Height = 21
          EditAlign = eaRight
          EditType = etNumeric
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          MaxLength = 3
          ReadOnly = True
          TabOrder = 0
          Text = '0'
          Visible = True
          Version = '2.9.2.1'
        end
        object txtNoBast: TAdvEdit
          Left = 58
          Top = 9
          Width = 40
          Height = 21
          EditAlign = eaRight
          EditType = etNumeric
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          MaxLength = 3
          ReadOnly = True
          TabOrder = 1
          Text = '0'
          Visible = True
          Version = '2.9.2.1'
        end
      end
      object txtBank: TAdvEdit
        Left = 448
        Top = 62
        Width = 233
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        ReadOnly = True
        TabOrder = 9
        Visible = True
        Version = '2.9.2.1'
      end
      object btnBank: TAdvGlowButton
        Left = 687
        Top = 62
        Width = 18
        Height = 20
        Caption = '...'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 10
        OnClick = btnBankClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
    end
    object btnSimpan: TAdvGlowButton
      Left = 936
      Top = 352
      Width = 73
      Height = 30
      Anchors = [akRight, akBottom]
      Caption = '&Simpan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000013E49444154384F6364A03260A4B2790C70034D67DDB6FFFF9FC1E1CB
        8B7B603B3AED78F0DAD57C4B8CE1DF3F86EBE7325557212B841B6832F3763DC3
        7F8686AF5003DBB118D8DEDAC260636BC7606B67C7D002341004FE33FC2F389B
        A136116628490696951433B8BAB901B13BDC4090418C0C4CC1A73394D741D850
        408C0B71190834A5E14CBA6A235E03DB6CB919181951E3ACAEA69AC1D1C90988
        9D515C88D7C05F5F3F0003E63F83182723839E182BD688F9F99791E1E4072E84
        1C3E1792958CE86AE0E97415921C693AF30E286A71470AC8C0FB4F9F337CFCF2
        0DAFC13C9C1C0C2A72D20C4419488A13893290EA2EFCFEF327C3DFBFFFF03A94
        9989898193839D382F7FFDFE83E1CF9FBFF80D646662E0E1E2246CE0BE044552
        8290C169C17DECB10C2BBE48320DAA1898430F9C4E533D889297C931089B1EAA
        97D800DC86C11578169C520000000049454E44AE426082}
      TabOrder = 0
      TabStop = True
      OnClick = btnSimpanClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object btnBatal: TAdvGlowButton
      Left = 936
      Top = 388
      Width = 73
      Height = 30
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Batal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000024A49444154384FAD94C16BD3501CC7BF2F699BAD8975154477503CB8
        3127C8305B87C2C4D293FFC58E524174C80E0A7A109987E1D845D8C9B3FF839B
        456160BBA06C6363B307A5A03BE8DCEC9226B6C9F3BDCC84A48D8D03DF2590F7
        7E9FDFF7F7FBFEDE23F8CF8B74E351807C55D5DE1E20C9CF65812634ADC182D8
        56F48A047EBC715EEAB74ECC80D022634AE1506AC11116E4DAF76952AD5AEDD8
        0EE0FEB5910131915A050113D655BF456D3A7AACB4B21E3C1502BAB0646AFB28
        6D751C6728F37A65CB8BF181747838659C56F62841EF518084A299DE3950C8C6
        C62F1EE7037FE673CF04017743F2CF9C03AD7D0AF149C43F66D19CB2549EF281
        DC4DBD30D6081AC003D32F5EA2555E8675FF308FF4E829121379348A9370B637
        0389A8252F5664A6CE76157E51D574A64FD4DB4B951ECF22716502F6DA07C030
        208E5F3D4CF08089A1E1C9716CFB64A6A47D7381BBAA7A3CD527EE45F5AE67F6
        39C411D5DDB2B73661DE9A8C6CB1D0A467D36F2AB578E0933957990B5C7D0F73
        EA663C90B292F5A892FFF48C2B4343674A47D15A7E0BEBE1BD0EA8CC4A265EC9
        DC14A39033D8D71F66DF9477BC6771A6C09417CB8A6F0A4F77901F9F87406F07
        530B034370AA6C660306088317DA1C661194CE2B4B953BA1397407BB5FD90FAA
        FCC7016FC99F7715EF5E87AE5E3D3F769108247437E3A049625F925E696BDEB9
        8EC7C12CE4066D8A757605DD27ABCB6A31D8E5202C54723090975F3FA5CC8802
        8AED2D600A4C4AE982BCA34F7BF737181BF7C08AF5EB6AB6D5449A0765933050
        D27E7037FFA6BC2B30AE7F51FBBF01AD04E81511DF13380000000049454E44AE
        426082}
      TabOrder = 1
      TabStop = True
      OnClick = btnBatalClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object mmKet: TMemo
      Left = 71
      Top = 352
      Width = 859
      Height = 66
      Anchors = [akLeft, akRight, akBottom]
      ScrollBars = ssVertical
      TabOrder = 3
    end
    object GroupBox4: TGroupBox
      Left = 823
      Top = 306
      Width = 186
      Height = 36
      Anchors = [akRight, akBottom]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Calibri'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Visible = False
      ExplicitLeft = 567
      DesignSize = (
        186
        36)
      object Label4: TLabel
        Left = 241
        Top = 17
        Width = 40
        Height = 13
        Caption = 'Subtotal'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 10
        Top = 14
        Width = 24
        Height = 13
        Caption = 'Total'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 241
        Top = 44
        Width = 48
        Height = 13
        Caption = 'PPN 10 %'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object txtSubTotal: TAdvEdit
        Left = 310
        Top = 13
        Width = 126
        Height = 21
        EditAlign = eaRight
        EditType = etMoney
        Precision = 2
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '0,00'
        Visible = True
        Version = '2.9.2.1'
      end
      object txtPPN: TAdvEdit
        Left = 310
        Top = 40
        Width = 126
        Height = 21
        EditAlign = eaRight
        EditType = etMoney
        Precision = 2
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        Text = '0,00'
        Visible = True
        Version = '2.9.2.1'
      end
      object txtTotal: TAdvEdit
        Left = 57
        Top = 10
        Width = 121
        Height = 21
        EditAlign = eaRight
        EditType = etMoney
        Precision = 2
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        Text = '0,00'
        Visible = True
        Version = '2.9.2.1'
      end
      object chbPPn: TAdvOfficeCheckBox
        Left = 38
        Top = 41
        Width = 14
        Height = 20
        Anchors = [akTop, akRight]
        TabOrder = 3
        Visible = False
        OnClick = chbPPnClick
        Alignment = taLeftJustify
        ButtonVertAlign = tlCenter
        Caption = 'PPN 10%'
        ReturnIsTab = True
        Version = '1.3.4.1'
      end
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 92
      Width = 1001
      Height = 255
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 4
      DesignSize = (
        1001
        255)
      object asgInput: TAdvStringGrid
        Left = 5
        Top = 11
        Width = 989
        Height = 237
        Cursor = crDefault
        TabStop = False
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 17
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goTabs]
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
        OnMouseWheelDown = asgInputMouseWheelDown
        OnMouseWheelUp = asgInputMouseWheelUp
        OnGetAlignment = asgInputGetAlignment
        OnCanAddRow = asgInputCanAddRow
        OnAutoAddRow = asgInputAutoAddRow
        OnCanDeleteRow = asgInputCanDeleteRow
        OnAutoDeleteRow = asgInputAutoDeleteRow
        OnCanEditCell = asgInputCanEditCell
        OnCellValidate = asgInputCellValidate
        OnGetEditorType = asgInputGetEditorType
        OnGetFloatFormat = asgInputGetFloatFormat
        OnCheckBoxChange = asgInputCheckBoxChange
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = []
        Bands.Active = True
        ColumnSize.Stretch = True
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        ControlLook.NoDisabledButtonLook = True
        ExcelStyleDecimalSeparator = True
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedFooters = 1
        FixedRowHeight = 22
        FixedColAlways = True
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = []
        FloatFormat = '%.2n'
        FloatingFooter.Visible = True
        Navigation.AllowDeleteRow = True
        Navigation.AdvanceOnEnter = True
        Navigation.AdvanceInsert = True
        Navigation.AutoComboDropSize = True
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'Tahoma'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'Tahoma'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'Tahoma'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'Tahoma'
        PrintSettings.FooterFont.Style = []
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'Tahoma'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurrence'
        SearchFooter.HintFindPrev = 'Find previous occurrence'
        SearchFooter.HintHighlight = 'Highlight occurrences'
        SearchFooter.MatchCaseCaption = 'Match case'
        ShowDesignHelper = False
        SizeWhileTyping.Width = True
        SortSettings.Column = 1
        SortSettings.NormalCellsOnly = True
        SortSettings.Row = -1
        VAlignment = vtaCenter
        Version = '6.0.4.2'
        WordWrap = False
        ColWidths = (
          64
          36
          40
          27
          50
          64
          46
          32
          30
          23
          64
          64
          162
          64
          464
          103
          64)
        RowHeights = (
          22
          22
          22
          22
          22
          22
          22
          22
          22
          22)
      end
    end
  end
end
