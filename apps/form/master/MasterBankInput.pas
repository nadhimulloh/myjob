unit MasterBankInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker;

type
  TfrmInputMasterBank = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    txtCabang: TAdvEdit;
    txtNama: TAdvEdit;
    Label1: TLabel;
    Label7: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    txtAtasNama: TAdvEdit;
    Label9: TLabel;
    Label22: TLabel;
    txtNoRek: TAdvEdit;
    Label8: TLabel;
    procedure btnBatalClick(Sender: TObject);
    procedure txtCabangKeyPress(Sender: TObject; var Key: Char);
    procedure btnSimpanClick(Sender: TObject);
    procedure mmKetKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    datenow: tdate;
    seqCard , seqmaster : integer;
    EditMode : Boolean;
    function isValid : Boolean;
    function isSaved : Boolean;
    procedure loaddata;
    procedure InitForm;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(seq : integer; statedit : boolean):boolean;
  end;

var
  frmInputMasterBank: TfrmInputMasterBank;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi;

{$R *.dfm}

procedure TfrmInputMasterBank.btnSimpanClick(Sender: TObject);
begin
  if IsValid and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if not EditMode then begin
      if IsSaved then begin
        if Confirmed(MSG_ADD_DATA) then begin
          InitForm; seqmaster := 0; txtNama.SetFocus; statSimpan := True;
        end else ModalResult := mrOk;
      end else begin
        InForm(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
    end else begin
      if IsSaved then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else begin
        InForm(MSG_UNSUCCESS_UPDATE);
        ActiveControl := btnBatal;
      end;
    end;
  end;
end;

function TfrmInputMasterBank.execute(seq: integer;
  statedit: boolean): boolean;
begin
  datenow:=ServerNow;
  InitForm;
  seqmaster := seq;
  EditMode := seqmaster <> 0;
  if Editmode then begin
    self.Caption := 'Edit Pekerjaan';
    loaddata;
  end else self.Caption := 'Tambah Pekerjaan';
  Run(Self, True);
  Result := ModalResult = mrOk;
end;

procedure TfrmInputMasterBank.InitForm;
begin
  txtSetAllEmpty(self);
  seqMaster := 0;
end;

function TfrmInputMasterBank.isSaved: Boolean;
var dataMaster : TR_master_Bank;
begin
  try
    myConnection.BeginSQL;
    dataMaster.nama        := TrimAllString(txtNama.Text);
    dataMaster.cabang      := TrimAllString(txtCabang.Text);
    dataMaster.norek       := TrimAllString(txtNoRek.Text);
    dataMaster.an          := TrimAllString(txtAtasNama.Text);
    dataMaster.user_id     := GlobalSystemUser.UserId;
    dataMaster.tgl_input   := ServerNow;

    if EditMode then begin
      DataMaster.seq := seqmaster;
      Update_master_Bank(DataMaster);
    end else begin
      seqmaster := Save_master_Bank(DataMaster);
    end;

    Get_List_master_Bank(ListBankGlobal, seqmaster);

    myConnection.EndSQL;
    Result := true;
  Except
    myConnection.UndoSQL;
    inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

function TfrmInputMasterBank.isValid: Boolean;
begin
  Result := True;

  if TrimAllString(txtNama.Text) = '' then begin
    inform('Nama Bank harus diisi.');
    txtNama.SetFocus;
    Result := false;
    Exit;
  end;

  if TrimAllString(txtCabang.Text) = '' then begin
    inform('Cabang harus diisi.');
    txtNama.SetFocus;
    Result := false;
    Exit;
  end;

  if TrimAllString(txtNoRek.Text) = '' then begin
    inform('No. Rekening harus diisi.');
    txtNoRek.SetFocus;
    Result := false;
    Exit;
  end;
end;

procedure TfrmInputMasterBank.loaddata;
var datamaster : TR_master_Bank;
begin
  DataMaster          := Get_master_Bank(seqmaster);

  txtNama.Text      := dataMaster.nama;
  txtCabang.Text    := dataMaster.cabang;
  txtAtasNama.text  := dataMaster.an;
  txtNoRek.text     := dataMaster.norek;

end;

procedure TfrmInputMasterBank.mmKetKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnSimpan.SetFocus;
end;

procedure TfrmInputMasterBank.txtCabangKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNama.SetFocus;
end;

procedure TfrmInputMasterBank.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
