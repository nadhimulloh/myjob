unit UpdateTglByr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker;

type
  TfrmUpdateTglByr = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    txtNama: TAdvEdit;
    Label1: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Label22: TLabel;
    Label23: TLabel;
    tglByr: TAdvDateTimePicker;
    txtNoPo: TAdvEdit;
    Label8: TLabel;
    procedure btnBatalClick(Sender: TObject);
    procedure txtKodeKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure mmKetKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    datenow: tdate;
    seqCard ,BeliSeq, seqmaster : integer;
    EditMode : Boolean;
    function isValid : Boolean;
    function isSaved : Boolean;
    procedure loaddata;
    procedure InitForm;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(seq : integer; statedit : boolean; seqBeli : integer = 0):boolean;
  end;

var
  frmUpdateTglByr: TfrmUpdateTglByr;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi;

{$R *.dfm}

procedure TfrmUpdateTglByr.btnSimpanClick(Sender: TObject);
begin
  if IsValid and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if not EditMode then begin
      if IsSaved then begin
        if Confirmed(MSG_ADD_DATA) then begin
          InitForm; seqmaster := 0; tglByr.SetFocus; statSimpan := True;
        end else ModalResult := mrOk;
      end else begin
        InForm(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
    end else begin
      if IsSaved then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else begin
        InForm(MSG_UNSUCCESS_UPDATE);
        ActiveControl := btnBatal;
      end;
    end;
  end;
end;

function TfrmUpdateTglByr.execute(seq: integer;
  statedit: boolean; seqBeli : integer = 0): boolean;
begin
  datenow:=ServerNow;
  InitForm;
  BeliSeq := seqBeli;
  seqmaster := seq;
  EditMode := seqmaster <> 0;
  loaddata;
  Run(Self, True);
  Result := ModalResult = mrOk;
end;

procedure TfrmUpdateTglByr.FormShow(Sender: TObject);
begin
  tglbyr.SetFocus;
end;

procedure TfrmUpdateTglByr.InitForm;
begin
  txtSetAllEmpty(self);
  seqMaster := 0;
  seqCard := 0;
  txtNama.ReadOnly := true;
  txtNoPo.ReadOnly := True;
end;

function TfrmUpdateTglByr.isSaved: Boolean;
var dataMaster : TR_master_Job;
    dtBeli :  TR_data_beli_master;
begin
  try
    myConnection.BeginSQL;
    if BeliSeq = 0 then begin
      dataMaster := Get_master_job(seqmaster);
      dataMaster.tgl_bayar  := tglbyr.date;

      if EditMode then begin
        Update_master_Job(DataMaster, true);
  //      seqmaster := Save_master_Job(DataMaster);
      end;

      Get_List_master_Job(ListJobGlobal, seqmaster);
    end else begin
      dtBeli := Get_data_beli_master(BeliSeq);
      dtBeli.tgl_byr := tglbyr.date;
      Update_data_beli_master(dtBeli, true);
    end;

    myConnection.EndSQL;
    Result := true;
  Except
    myConnection.UndoSQL;
    inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

function TfrmUpdateTglByr.isValid: Boolean;
begin
  Result := True;
end;

procedure TfrmUpdateTglByr.loaddata;
var datamaster : TR_master_Job;
begin              
  DataMaster          := Get_master_Job(seqmaster);
  txtNama.Text :=  dataMaster.nama+' - '+dataMaster.deskripsi;
  txtNoPo.text       := dataMaster.no_po;
end;

procedure TfrmUpdateTglByr.mmKetKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnSimpan.SetFocus;
end;

procedure TfrmUpdateTglByr.txtKodeKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNama.SetFocus;
end;

procedure TfrmUpdateTglByr.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
