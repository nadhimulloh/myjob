unit MasterPPNRekap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls;

type
  TfrmRekapMasterPPN = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnAktivasi: TAdvGlowButton;
    btnTambah: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    btnHapus: TAdvGlowButton;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label19: TLabel;
    cmbStatus: TAdvComboBox;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapSelectionChanged(Sender: TObject; ALeft, ATop, ARight,
      ABottom: Integer);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
  private
    seqCard,AMenuid : integer;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddatapua;
    procedure loadedit(Arow : integer =0; isTambahBaru : boolean = false);
    function isDelete(alasan: string): boolean;
    procedure loaddata;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmRekapMasterPPN: TfrmRekapMasterPPN;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, MasterPPNInput, StrUtils, LOV;

{$R *.dfm}

const
  ColNo         = 0;
  colTglBerlaku = 1;
  colPPN        = 2;
  ColUserId     = 3;
  ColAktif      = 4;
  ColSeq        = 5;



procedure TfrmRekapMasterPPN.arrangecolsize;
begin
  asgRekap.AutoNumberCol(colno);
  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
    asgRekap.ColWidths[colaktif] := 0;
    asgRekap.ColWidths[ColUserId] := 0;
  end;
end;

procedure TfrmRekapMasterPPN.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  btnEdit.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0) and (asgRekap.cells[colAktif,asgRekap.Row] = '');
  btnAktivasi.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
end;

procedure TfrmRekapMasterPPN.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow > 0) then btnEdit.Click;
end;

procedure TfrmRekapMasterPPN.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [colNo] then HAlign := taRightJustify;
end;

procedure TfrmRekapMasterPPN.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmRekapMasterPPN.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmRekapMasterPPN.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol = colseq then FloatFormat    := '%.0f'
  end;
end;

procedure TfrmRekapMasterPPN.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapMasterPPN.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapMasterPPN.asgRekapSelectionChanged(Sender: TObject;
  ALeft, ATop, ARight, ABottom: Integer);
begin
  asgRekap.OnClickCell(Self,asgRekap.Row,asgRekap.Col);
end;

procedure TfrmRekapMasterPPN.btnAktivasiClick(Sender: TObject);
var Tanggal : tdate;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Cells[ColAktif, asgRekap.Row]= '' then  begin
    Tanggal := 0;
  end else
    Tanggal := VarToDateTime(asgRekap.Cells[ColAktif, asgRekap.Row]);
  if Confirmed(MSG_CONFIRMED_AKTIVASI) then begin
    UEngine.Aktivasi_Master(asgRekap.Ints[ColSeq, asgRekap.Row], Tanggal, 'master_PPN', true, 'tgl_nonaktif');
    loaddata;
  end;
end;

procedure TfrmRekapMasterPPN.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('List PPN');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 3;
  arrangecolsize;
end;

procedure TfrmRekapMasterPPN.btnCustClick(Sender: TObject);
begin
  seqCard := 0;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmRekapMasterPPN.btnEditClick(Sender: TObject);
begin
  if not BisaEdit(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) and (asgRekap.cells[colAktif, asgRekap.Row] = '') then begin
    Application.CreateForm(TfrmInputMasterPPN, frmInputMasterPPN);
    frmInputMasterPPN.OnClose := frmMainMenu.ChildFormSingle;
    if frmInputMasterPPN.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then LoadEdit(asgRekap.Row, false);
  end;
end;

procedure TfrmRekapMasterPPN.btnLoadClick(Sender: TObject);
begin
  loaddata;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmRekapMasterPPN.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapMasterPPN.btnTambahClick(Sender: TObject);
begin
  if not BisaNambah(AmenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  Application.CreateForm(TfrmInputMasterPPN, frmInputMasterPPN);
  frmInputMasterPPN.OnClose := frmMainMenu.ChildFormSingle;
  if (frmInputMasterPPN.execute(0,false)) or (frmInputMasterPPN.statSimpan = true)  then btnLoad.Click;
end;

procedure TfrmRekapMasterPPN.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapMasterPPN.Execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmRekapMasterPPN.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmRekapMasterPPN.initform;
begin
  dtpAwal.Date  := ServerNow();
  dtpAkhir.Date := ServerNow();
  txtCust.Text    := '';
  seqCard         := 0;
  cmbStatus.ItemIndex           := 1;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

function TfrmRekapMasterPPN.isDelete(alasan: string): boolean;
var seqMst : integer;
    MstTmp   : tr_master_PPN;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
begin
  myConnection.BeginSQL;
  try
    SeqMst   := asgRekap.Ints[ColSeq, asgRekap.Row];
    MstTmp   := Get_master_PPN(seqMst);
    Delete_master_PPN(MstTmp.Seq);

    Ket.IsEdit    := False;
    Ket.NamaTrans := 'List PPN';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := '';
    Ket.NoTrans   := '';
    Ket.TglTrans  := ServerNow;

    Log.Nama_Tabel := 'master_PPN';
    Log.Tanggal    := ServerNow;
    Log.Seq_Dummy  := 0;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := 0;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := alasan;
    Save_Log_File(Log);

    Result := True;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

procedure TfrmRekapMasterPPN.loaddata;
var i,Row       : Integer;
    Astatus     : TBooleanOperator;
    Filter, Divisi : string;
    ArData      : AR_master_PPN;
begin
  asgRekap.RowCount := 2;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  AStatus := boNone;
  
  ArData := Get_Arr_master_PPN('',Filter);
  for i := 0 to Length(ArData) - 1 do begin
    AdvProgressBar1.Position := round((i+1)/Length(ArData)*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -1;
    Asgrekap.Dates[colTglBerlaku   ,Row] := arData[i].tgl_berlaku;
    Asgrekap.cells[colPPN   ,       Row] := FloatToStrFmt( arData[i].ppn )+'%';
    //Asgrekap.cells[colUserId       ,Row] := arData[i].user_id;
    
    if arData[i].tgl_nonaktif <> 0 then
      Asgrekap.Dates[colAktif        ,Row] := arData[i].tgl_nonaktif;
    Asgrekap.ints[colSeq          ,Row] := arData[i].seq;


  end;
  AdvProgressBar1.Hide;
  arrangecolsize;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapMasterPPN.loaddatapua;
begin

end;

procedure TfrmRekapMasterPPN.loadedit(Arow: integer;
  isTambahBaru: boolean);
var Row, seqMst : Integer;
    dtMaster : TR_master_PPN;
begin
  if isTambahBaru = false then begin
    dtMaster := Get_master_PPN(asgRekap.Ints[colseq,arow]);
    row := arow
  end else begin
    seqMst := GetCurrentSeq('seq','Master_PPN','');
    dtMaster := Get_master_PPN(seqMst);
    if (asgRekap.RowCount > 2) or ((asgRekap.RowCount = 2) and (asgRekap.cells[colSeq,1] <> '')) then asgRekap.AddRow;
    row := asgRekap.RowCount-1;
  end;
                                                      
  Asgrekap.cells[colPPN          ,Row] := FloatToStrFmt( dtMaster.ppn ) +' %';
  Asgrekap.Dates[colTglBerlaku   ,Row] := dtMaster.tgl_berlaku;
  //Asgrekap.cells[colUserId       ,Row] := dtMaster.user_id;

  if dtMaster.tgl_nonaktif <> 0 then
    Asgrekap.Dates[colAktif     ,Row] := dtMaster.tgl_nonaktif;
  Asgrekap.ints[colSeq          ,Row] := dtMaster.seq;

  ArrangeColSize;
end;

procedure TfrmRekapMasterPPN.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 7;
  asgRekap.RowCount := 2;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;
  arrangecolsize;
end;

procedure TfrmRekapMasterPPN.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapMasterPPN.btnHapusClick(Sender: TObject);
var Tanggal : tdate;
    alasanhapus : string;
begin
  {
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.Row] <> 0 then begin
    if Get_Count('PPN_seq', 'data_jual_master', inttostr(asgRekap.Ints[ColSeq,asgRekap.Row]), '', 'string') > 0 then begin
      Inform(asgRekap.Cells[colCabang, asgRekap.Row]+' tidak bisa dihapus, ' +
          'sudah ada Data Jual. ');
      exit;
    end;

    if Confirmed('Pekerjaan "'+asgRekap.Cells[colCabang, asgRekap.Row]+'" akan dihapus ?') then begin
      alasanhapus := TrimAllString(UGeneral.InputNote);
      if alasanhapus = '' then begin
        Inform('Catatan hapus tidak boleh kosong.');
        Exit;
      end;
      if isDelete(alasanhapus) then begin
        inform(MSG_SUCCESS_DELETING);
        if AsgRekap.RowCount > 2  then AsgRekap.RemoveRows(AsgRekap.Row,1)
        else AsgRekap.ClearRows(AsgRekap.Row,1);
        Arrangecolsize;
        AsgRekap.Col := colCabang;
        AsgRekap.Row := 1;
      end else Inform(MSG_UNSUCCESS_DELETING);
    end;
  end;
  }
end;

end.
