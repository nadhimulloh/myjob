object frmInputMasterCard: TfrmInputMasterCard
  Left = 337
  Top = 223
  BorderStyle = bsDialog
  Caption = 'Add List Card'
  ClientHeight = 365
  ClientWidth = 627
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 172
    Width = 27
    Height = 13
    Caption = 'Nama'
  end
  object Label22: TLabel
    Left = 16
    Top = 201
    Width = 26
    Height = 13
    Caption = 'State'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 627
    Height = 365
    Align = alClient
    TabOrder = 0
    DesignSize = (
      627
      365)
    object Label7: TLabel
      Left = 152
      Top = 332
      Width = 115
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = '* Kolomn harus diisi.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 1
      Width = 611
      Height = 315
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 2
      object Label2: TLabel
        Left = 8
        Top = 71
        Width = 36
        Height = 13
        Caption = 'Id Card'
      end
      object Label3: TLabel
        Left = 8
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Nama'
      end
      object Label4: TLabel
        Left = 8
        Top = 148
        Width = 33
        Height = 13
        Caption = 'Alamat'
      end
      object Label5: TLabel
        Left = 306
        Top = 44
        Width = 49
        Height = 13
        Caption = 'No. Telp 1'
      end
      object Label8: TLabel
        Left = 306
        Top = 98
        Width = 29
        Height = 13
        Caption = 'NPWP'
      end
      object Label16: TLabel
        Left = 286
        Top = 40
        Width = 6
        Height = 13
        Caption = '*'
      end
      object Label17: TLabel
        Left = 141
        Top = 94
        Width = 6
        Height = 13
        Caption = '*'
      end
      object Label6: TLabel
        Left = 306
        Top = 125
        Width = 24
        Height = 13
        Caption = 'Email'
      end
      object Label9: TLabel
        Left = 306
        Top = 179
        Width = 33
        Height = 13
        Caption = 'Kontak'
      end
      object Label12: TLabel
        Left = 306
        Top = 71
        Width = 49
        Height = 13
        Caption = 'No. Telp 2'
      end
      object Label19: TLabel
        Left = 8
        Top = 17
        Width = 20
        Height = 13
        Caption = 'Tipe'
      end
      object Label20: TLabel
        Left = 8
        Top = 206
        Width = 22
        Height = 13
        Caption = 'Kota'
      end
      object Label21: TLabel
        Left = 8
        Top = 233
        Width = 37
        Height = 13
        Caption = 'Provinsi'
      end
      object Label23: TLabel
        Left = 8
        Top = 287
        Width = 44
        Height = 13
        Caption = 'Kode Pos'
      end
      object Label24: TLabel
        Left = 8
        Top = 260
        Width = 35
        Height = 13
        Caption = 'Negara'
      end
      object Label18: TLabel
        Left = 306
        Top = 17
        Width = 24
        Height = 13
        Caption = 'Jenis'
      end
      object Label13: TLabel
        Left = 306
        Top = 152
        Width = 39
        Height = 13
        Caption = 'Website'
      end
      object Label10: TLabel
        Left = 306
        Top = 206
        Width = 91
        Height = 13
        Caption = 'Penanggung Jwb 1'
      end
      object Label11: TLabel
        Left = 306
        Top = 232
        Width = 39
        Height = 13
        Caption = 'Jabatan'
      end
      object Label14: TLabel
        Left = 306
        Top = 257
        Width = 91
        Height = 13
        Caption = 'Penanggung Jwb 2'
      end
      object Label15: TLabel
        Left = 306
        Top = 283
        Width = 39
        Height = 13
        Caption = 'Jabatan'
      end
      object Label25: TLabel
        Left = 8
        Top = 98
        Width = 81
        Height = 13
        Caption = 'Kode Penomoran'
      end
      object Label26: TLabel
        Left = 8
        Top = 123
        Width = 48
        Height = 13
        Caption = 'Unit Bisnis'
      end
      object Label27: TLabel
        Left = 220
        Top = 121
        Width = 6
        Height = 13
        Caption = '*'
      end
      object Label28: TLabel
        Left = 284
        Top = 148
        Width = 6
        Height = 13
        Caption = '*'
      end
      object txtKode: TAdvEdit
        Left = 95
        Top = 67
        Width = 121
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        CharCase = ecUpperCase
        Color = clWindow
        ReadOnly = True
        TabOrder = 2
        Visible = True
        OnKeyPress = txtKodeKeyPress
        Version = '2.9.2.1'
      end
      object txtNama: TAdvEdit
        Left = 95
        Top = 40
        Width = 185
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 1
        Visible = True
        OnKeyPress = txtNamaKeyPress
        Version = '2.9.2.1'
      end
      object txtNoFax: TAdvEdit
        Left = 405
        Top = 94
        Width = 121
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        TabOrder = 11
        Visible = True
        OnKeyPress = txtNoFaxKeyPress
        Version = '2.9.2.1'
      end
      object txtNoTelp: TAdvEdit
        Left = 405
        Top = 40
        Width = 121
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        TabOrder = 9
        Visible = True
        OnKeyPress = txtNoTelpKeyPress
        Version = '2.9.2.1'
      end
      object mmAlamat: TMemo
        Left = 95
        Top = 148
        Width = 185
        Height = 48
        ScrollBars = ssVertical
        TabOrder = 3
        OnKeyPress = mmAlamatKeyPress
      end
      object txtEmail: TAdvEdit
        Left = 405
        Top = 121
        Width = 156
        Height = 21
        EditType = etLowerCase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        TabOrder = 12
        Visible = True
        OnKeyPress = txtEmailKeyPress
        Version = '2.9.2.1'
      end
      object txtNoTelp2: TAdvEdit
        Left = 405
        Top = 67
        Width = 121
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        TabOrder = 10
        Visible = True
        OnKeyPress = txtNoTelp2KeyPress
        Version = '2.9.2.1'
      end
      object txtContact: TAdvEdit
        Left = 405
        Top = 175
        Width = 185
        Height = 21
        EditType = etMixedCase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 14
        Visible = True
        OnKeyPress = txtContactKeyPress
        Version = '2.9.2.1'
      end
      object cmbTipe: TAdvComboBox
        Left = 95
        Top = 13
        Width = 122
        Height = 21
        Color = clWindow
        Version = '1.4.0.0'
        Visible = True
        Style = csDropDownList
        DropWidth = 0
        Enabled = True
        ItemIndex = -1
        ItemHeight = 13
        Items.Strings = (
          'Customer'
          'Vendor')
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        TabOrder = 0
        OnChange = cmbTipeChange
      end
      object txtCity: TAdvEdit
        Left = 95
        Top = 202
        Width = 185
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 4
        Visible = True
        OnKeyPress = txtCityKeyPress
        Version = '2.9.2.1'
      end
      object txtState: TAdvEdit
        Left = 95
        Top = 229
        Width = 185
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 5
        Visible = True
        OnKeyPress = txtStateKeyPress
        Version = '2.9.2.1'
      end
      object txtPos: TAdvEdit
        Left = 95
        Top = 283
        Width = 66
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 8
        Visible = True
        OnKeyPress = txtPosKeyPress
        Version = '2.9.2.1'
      end
      object txtCountry: TAdvEdit
        Left = 95
        Top = 256
        Width = 185
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 6
        Visible = True
        OnKeyPress = txtCountryKeyPress
        Version = '2.9.2.1'
      end
      object txtWebsite: TAdvEdit
        Left = 405
        Top = 148
        Width = 156
        Height = 21
        EditType = etLowerCase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        TabOrder = 13
        Visible = True
        OnKeyPress = txtWebsiteKeyPress
        Version = '2.9.2.1'
      end
      object cmbDes: TComboBox
        Left = 405
        Top = 13
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 7
        Items.Strings = (
          'Company'
          'Individual')
      end
      object txtPng1: TAdvEdit
        Left = 405
        Top = 202
        Width = 185
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 15
        Visible = True
        OnKeyPress = txtPng1KeyPress
        Version = '2.9.2.1'
      end
      object txtJab1: TAdvEdit
        Left = 405
        Top = 229
        Width = 185
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 16
        Visible = True
        OnKeyPress = txtJab1KeyPress
        Version = '2.9.2.1'
      end
      object txtPng2: TAdvEdit
        Left = 405
        Top = 256
        Width = 185
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 17
        Visible = True
        OnKeyPress = txtPng2KeyPress
        Version = '2.9.2.1'
      end
      object txtJab2: TAdvEdit
        Left = 405
        Top = 281
        Width = 185
        Height = 21
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 50
        TabOrder = 18
        Visible = True
        Version = '2.9.2.1'
      end
      object txtKdNmr: TAdvEdit
        Left = 95
        Top = 94
        Width = 42
        Height = 21
        EditType = etUppercase
        ReturnIsTab = True
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Separator = ';'
        Color = clWindow
        MaxLength = 3
        TabOrder = 19
        Visible = True
        OnKeyPress = txtKdNmrKeyPress
        Version = '2.9.2.1'
      end
      object cmbUnit: TAdvComboBox
        Left = 95
        Top = 121
        Width = 122
        Height = 21
        Color = clWindow
        Version = '1.4.0.0'
        Visible = True
        Style = csDropDownList
        DropWidth = 0
        Enabled = True
        ItemIndex = -1
        ItemHeight = 13
        Items.Strings = (
          'Logistic'
          'General Services'
          'Construction')
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        TabOrder = 20
        OnKeyPress = cmbUnitKeyPress
      end
      object chbBautBast: TAdvOfficeCheckBox
        Left = 160
        Top = 94
        Width = 120
        Height = 20
        TabOrder = 21
        Alignment = taLeftJustify
        Caption = 'Ada BAUT & BAST'
        ReturnIsTab = False
        Version = '1.3.4.1'
      end
    end
    object btnSimpan: TAdvGlowButton
      Left = 8
      Top = 322
      Width = 65
      Height = 34
      Anchors = [akLeft, akBottom]
      Caption = '&Simpan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000013E49444154384F6364A03260A4B2790C70034D67DDB6FFFF9FC1E1CB
        8B7B603B3AED78F0DAD57C4B8CE1DF3F86EBE7325557212B841B6832F3763DC3
        7F8686AF5003DBB118D8DEDAC260636BC7606B67C7D002341004FE33FC2F389B
        A136116628490696951433B8BAB901B13BDC4090418C0C4CC1A73394D741D850
        408C0B71190834A5E14CBA6A235E03DB6CB919181951E3ACAEA69AC1D1C90988
        9D515C88D7C05F5F3F0003E63F83182723839E182BD688F9F99791E1E4072E84
        1C3E1792958CE86AE0E97415921C693AF30E286A71470AC8C0FB4F9F337CFCF2
        0DAFC13C9C1C0C2A72D20C4419488A13893290EA2EFCFEF327C3DFBFFFF03A94
        9989898193839D382F7FFDFE83E1CF9FBFF80D646662E0E1E2246CE0BE044552
        8290C169C17DECB10C2BBE48320DAA1898430F9C4E533D889297C931089B1EAA
        97D800DC86C11578169C520000000049454E44AE426082}
      TabOrder = 0
      TabStop = True
      OnClick = btnSimpanClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object btnBatal: TAdvGlowButton
      Left = 79
      Top = 322
      Width = 65
      Height = 34
      Anchors = [akLeft, akBottom]
      Cancel = True
      Caption = '&Batal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000024A49444154384FAD94C16BD3501CC7BF2F699BAD8975154477503CB8
        3127C8305B87C2C4D293FFC58E524174C80E0A7A109987E1D845D8C9B3FF839B
        456160BBA06C6363B307A5A03BE8DCEC9226B6C9F3BDCC84A48D8D03DF2590F7
        7E9FDFF7F7FBFEDE23F8CF8B74E351807C55D5DE1E20C9CF65812634ADC182D8
        56F48A047EBC715EEAB74ECC80D022634AE1506AC11116E4DAF76952AD5AEDD8
        0EE0FEB5910131915A050113D655BF456D3A7AACB4B21E3C1502BAB0646AFB28
        6D751C6728F37A65CB8BF181747838659C56F62841EF518084A299DE3950C8C6
        C62F1EE7037FE673CF04017743F2CF9C03AD7D0AF149C43F66D19CB2549EF281
        DC4DBD30D6081AC003D32F5EA2555E8675FF308FF4E829121379348A9370B637
        0389A8252F5664A6CE76157E51D574A64FD4DB4B951ECF22716502F6DA07C030
        208E5F3D4CF08089A1E1C9716CFB64A6A47D7381BBAA7A3CD527EE45F5AE67F6
        39C411D5DDB2B73661DE9A8C6CB1D0A467D36F2AB578E0933957990B5C7D0F73
        EA663C90B292F5A892FFF48C2B4343674A47D15A7E0BEBE1BD0EA8CC4A265EC9
        DC14A39033D8D71F66DF9477BC6771A6C09417CB8A6F0A4F77901F9F87406F07
        530B034370AA6C660306088317DA1C661194CE2B4B953BA1397407BB5FD90FAA
        FCC7016FC99F7715EF5E87AE5E3D3F769108247437E3A049625F925E696BDEB9
        8EC7C12CE4066D8A757605DD27ABCB6A31D8E5202C54723090975F3FA5CC8802
        8AED2D600A4C4AE982BCA34F7BF737181BF7C08AF5EB6AB6D5449A0765933050
        D27E7037FFA6BC2B30AE7F51FBBF01AD04E81511DF13380000000049454E44AE
        426082}
      TabOrder = 1
      TabStop = True
      OnClick = btnBatalClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
  end
end
