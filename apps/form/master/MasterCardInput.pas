unit MasterCardInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo;

type
  TfrmInputMasterCard = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    txtKode: TAdvEdit;
    txtNama: TAdvEdit;
    txtNoFax: TAdvEdit;
    txtNoTelp: TAdvEdit;
    mmAlamat: TMemo;
    Label1: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    txtEmail: TAdvEdit;
    txtNoTelp2: TAdvEdit;
    txtContact: TAdvEdit;
    Label9: TLabel;
    Label12: TLabel;
    Label19: TLabel;
    cmbTipe: TAdvComboBox;
    txtCity: TAdvEdit;
    Label20: TLabel;
    txtState: TAdvEdit;
    Label21: TLabel;
    Label22: TLabel;
    txtPos: TAdvEdit;
    Label23: TLabel;
    txtCountry: TAdvEdit;
    Label24: TLabel;
    Label18: TLabel;
    txtWebsite: TAdvEdit;
    Label13: TLabel;
    cmbDes: TComboBox;
    Label10: TLabel;
    txtPng1: TAdvEdit;
    Label11: TLabel;
    txtJab1: TAdvEdit;
    Label14: TLabel;
    txtPng2: TAdvEdit;
    Label15: TLabel;
    txtJab2: TAdvEdit;
    txtKdNmr: TAdvEdit;
    Label25: TLabel;
    Label26: TLabel;
    cmbUnit: TAdvComboBox;
    Label27: TLabel;
    Label28: TLabel;
    chbBautBast: TAdvOfficeCheckBox;
    procedure btnBatalClick(Sender: TObject);
    procedure txtKodeKeyPress(Sender: TObject; var Key: Char);
    procedure txtNamaKeyPress(Sender: TObject; var Key: Char);
    procedure txtNoTelpKeyPress(Sender: TObject; var Key: Char);
    procedure txtNoFaxKeyPress(Sender: TObject; var Key: Char);
    procedure txtEmailKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure mmAlamatKeyPress(Sender: TObject; var Key: Char);
    procedure txtNoTelp2KeyPress(Sender: TObject; var Key: Char);
    procedure mmKetKeyPress(Sender: TObject; var Key: Char);
    procedure txtCityKeyPress(Sender: TObject; var Key: Char);
    procedure txtStateKeyPress(Sender: TObject; var Key: Char);
    procedure txtCountryKeyPress(Sender: TObject; var Key: Char);
    procedure txtPosKeyPress(Sender: TObject; var Key: Char);
    procedure txtWebsiteKeyPress(Sender: TObject; var Key: Char);
    procedure cmbTipeChange(Sender: TObject);
    procedure txtKdNmrKeyPress(Sender: TObject; var Key: Char);
    procedure cmbUnitKeyPress(Sender: TObject; var Key: Char);
    procedure txtContactKeyPress(Sender: TObject; var Key: Char);
    procedure txtPng1KeyPress(Sender: TObject; var Key: Char);
    procedure txtJab1KeyPress(Sender: TObject; var Key: Char);
    procedure txtPng2KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    tmpBautBast, tipe : string;
    seqmaster : integer;
    EditMode,isorder : Boolean;
    function isValid : Boolean;
    function isSaved : Boolean;
    procedure loaddata;
    procedure InitForm;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(seq : integer; statedit : boolean):boolean;
  end;

var
  frmInputMasterCard: TfrmInputMasterCard;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, ADOInt, UTransaksi;

{$R *.dfm}

procedure TfrmInputMasterCard.btnSimpanClick(Sender: TObject);
begin
  if IsValid and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if not EditMode then begin
      if IsSaved then begin
        if isorder = true then ModalResult := mrOk
        else begin
          if Confirmed(MSG_ADD_DATA) then begin
            InitForm; seqmaster := 0; txtKode.SetFocus; statSimpan := True;
          end else ModalResult := mrOk;
        end;
      end else begin
        InForm(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
    end else begin
      if IsSaved then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else begin
        InForm(MSG_UNSUCCESS_UPDATE);
        ActiveControl := btnBatal;
      end;
    end;
  end;
end;

procedure TfrmInputMasterCard.cmbTipeChange(Sender: TObject);
begin
  cmbUnit.ItemIndex := -1;
  chbBautBast.checked := false;
  if cmbTipe.ItemIndex = 0 then begin
    cmbUnit.Enabled := False;
    txtKode.Text := GetNextNumberMaster(master_card, tipe_CUSTOMER);
    chbBautBast.visible := true;
  end else
  if cmbTipe.ItemIndex = 1 then begin
    cmbUnit.Enabled := true;
    txtKode.Text := GetNextNumberMaster(master_card, TIPE_VENDOR);
    chbBautBast.visible := false;
  end;                          
end;

procedure TfrmInputMasterCard.cmbUnitKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then mmAlamat.SetFocus;
end;

function TfrmInputMasterCard.execute(seq: integer;
  statedit: boolean): boolean;
begin
  InitForm;
  seqmaster := seq;
  EditMode := seqmaster <> 0;
  if Editmode then begin
    self.Caption := 'Edit Card';
    loaddata;
  end else self.Caption := 'Tambah Card';
  Run(Self, True);
  Result := ModalResult = mrOk;
end;

procedure TfrmInputMasterCard.FormShow(Sender: TObject);
begin
  cmbTipe.SetFocus;
end;

procedure TfrmInputMasterCard.InitForm;
begin
  txtSetAllEmpty(self);
  txtNAma.Text:='';
  tipe := '';
  cmbTipe.text := '';
  cmbDes.text  := '';
  cmbTipe.ItemIndex := -1;
  cmbDes.ItemIndex  := -1;
  seqMaster := 0;
end;

function TfrmInputMasterCard.isSaved: Boolean;
var dataMaster : TR_master_card;
    sql :  string;
    buffer : _recordset;
    i : integer;
    dtInv : TR_invoice_master;
begin
  try
    myConnection.BeginSQL;
    dataMaster.kode        := TrimAllString(txtKode.Text);
    dataMaster.kode_nomor  := TrimAllString(txtKdNmr.Text);
    dataMaster.nama        := TrimAllString(txtNama.Text);
    dataMaster.alamat      := TrimAllString(mmAlamat.Lines.Text);
    dataMaster.tipe        := ifthen(cmbTipe.ItemIndex=0,TIPE_CUSTOMER,TIPE_VENDOR);
    dataMaster.jenis       := ifthen(cmbDes.ItemIndex=0,TIPE_COMPANY,TIPE_INDIE);

    if cmbUnit.ItemIndex = 0 then
      dataMaster.unit_bisnis:= TIPE_DIVISI_Logistic
    else if cmbUnit.ItemIndex = 1 then
      dataMaster.unit_bisnis:= TIPE_DIVISI_TRADING
    else if cmbUnit.ItemIndex = 2 then
      dataMaster.unit_bisnis:= TIPE_DIVISI_CONSTRUCTION;

    dataMaster.kota        := TrimAllString(txtCity.Text);
    dataMaster.provinsi   := TrimAllString(txtState.Text);
    dataMaster.negara     := TrimAllString(txtCountry.Text);
    dataMaster.kodepos    := TrimAllString(txtPos.Text);
    dataMaster.no_telp1   := TrimAllString(txtNoTelp.Text);
    dataMaster.no_telp2   := TrimAllString(txtNoTelp2.Text);
    dataMaster.no_fax     := TrimAllString(txtNoFax.Text);
    dataMaster.email      := TrimAllString(txtEmail.Text);
    dataMaster.website    := TrimAllString(txtWebsite.Text);
    dataMaster.kontak     := TrimAllString(txtContact.Text);
    dataMaster.png_jwb1   := TrimAllString(txtPng1.Text);
    dataMaster.jabatan1   := TrimAllString(txtJab1.Text);
    dataMaster.png_jwb2   := TrimAllString(txtPng2.Text);
    dataMaster.jabatan2   := TrimAllString(txtJab2.Text);
    dataMaster.user_id    := GlobalSystemUser.UserId;
    dataMaster.tgl_input  := ServerNow;

    dataMaster.isBautBast := ifthen(chbBautBast.Checked, TRUE_STRING, FALSE_STRING);

    if EditMode then begin
      DataMaster.seq := seqmaster;
      Update_master_card(DataMaster);

      if (dataMaster.isBautBast = true_string) and (tmpBautBast = false_string) then begin
        sql := ' select seq, tanggal from invoice_master where nomor_baut is null and nomor_bast is null '+
                ' and card_seq = '+formatsqlnumber(dataMaster.seq);
        buffer := myConnection.OpenSQL(sql);
        for I := 0 to buffer.RecordCount - 1 do begin
          dtInv := Get_invoice_master(BufferToInteger(buffer.Fields[0].Value));
          if dtInv.seq <> 0 then begin
            dtInv.nomor_baut := GetNextNumber(BufferToDateTime(buffer.Fields[1].Value), TRANS_BAUT, dataMaster.seq);
            dtInv.nomor_bast := GetNextNumber(BufferToDateTime(buffer.Fields[1].Value), TRANS_BAST, dataMaster.seq);
            Update_invoice_master(dtInv);
          end;
          buffer.MoveNext;
        end;
        buffer.Close;
      end;

    end else begin
      seqmaster := Save_master_card(DataMaster);
    end;

    Get_List_master_card(ListCardGlobal, seqmaster);

    myConnection.EndSQL;
    Result := true;
  Except
    myConnection.UndoSQL;
    inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

function TfrmInputMasterCard.isValid: Boolean;
begin
  Result := True;
  if cmbTipe.ItemIndex = -1 then begin
    inform('Tipe harus dipilih.');
    cmbTipe.SetFocus;
    Result := false;
    Exit;
  end;

  if TrimAllString(txtNama.Text) = '' then begin
    inform('Nama harus diisi.');
    txtNama.SetFocus;
    Result := false;
    Exit;
  end;

  if TrimAllString(txtKdNmr.Text) = '' then begin
    inform('Kode Penomoran harus diisi.');
    txtKdNmr.SetFocus;
    Result := false;
    Exit;
  end;

  if UEngine.Is_Kode_Field_Exist('master_card','kode_nomor',TrimAllString(txtKdNmr.Text),
    seqmaster) then begin
    inform('Kode Penomoran sudah ada.');
    txtKdNmr.SetFocus;
    Result := false;
    Exit;
  end;

  if cmbTipe.ItemIndex = 1 then begin
    if cmbUnit.ItemIndex = -1 then begin
      inform('Unit Bisnis harus dipilih.');
      cmbUnit.SetFocus;
      Result := false;
      Exit;
    end;
  end;

  if TrimAllString(mmAlamat.lines.Text) = '' then begin
    inform('Alamat harus diisi.');
    mmAlamat.SetFocus;
    Result := false;
    Exit;
  end;
end;

procedure TfrmInputMasterCard.loaddata;
var datamaster : TR_master_card;
begin
  DataMaster := Get_master_card(seqmaster);

  txtKdNmr.Text := dataMaster.kode_nomor;
  txtKode.Text :=  dataMaster.kode;
  txtNama.Text :=  dataMaster.nama;
  mmAlamat.Lines.Text := dataMaster.alamat;
  if dataMaster.tipe = TIPE_CUSTOMER then begin
    cmbTipe.ItemIndex    := 0;
    cmbUnit.Enabled      := False;
  end
  else begin
    cmbTipe.ItemIndex := 1;
    cmbUnit.Enabled   := true;
  end;

  chbBautBast.Checked := dataMaster.isBautBast = TRUE_STRING;
  tmpBautBast := dataMaster.isBautBast;

  if dataMaster.jenis = TIPE_COMPANY then
    cmbDes.ItemIndex    := 0
  else cmbDes.ItemIndex := 1;

  if dataMaster.unit_bisnis= TIPE_DIVISI_Logistic then
    cmbUnit.ItemIndex := 0
  else if dataMaster.unit_bisnis = TIPE_DIVISI_TRADING then
    cmbUnit.ItemIndex := 1
  else if dataMaster.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
    cmbUnit.ItemIndex := 2;

  txtCity.Text    := dataMaster.kota;
  txtState.Text   := dataMaster.provinsi;
  txtCountry.Text := dataMaster.negara;
  txtPos.Text     := dataMaster.kodepos;
  txtNoTelp.Text  := dataMaster.no_telp1;
  txtNoTelp2.Text := dataMaster.no_telp2;
  txtNoFax.Text   := dataMaster.no_fax;
  txtEmail.Text   := dataMaster.email;
  txtWebsite.Text := dataMaster.website;
  txtContact.Text := dataMaster.kontak;
  txtPng1.Text   := dataMaster.png_jwb1;
  txtJab1.Text   := dataMaster.jabatan1;
  txtPng2.Text   := dataMaster.png_jwb2;
  txtJab2.Text   := dataMaster.jabatan2;

end;

procedure TfrmInputMasterCard.mmAlamatKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNoTelp.SetFocus;
end;

procedure TfrmInputMasterCard.mmKetKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnSimpan.SetFocus;
end;

procedure TfrmInputMasterCard.txtEmailKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtWebsite.SetFocus;
end;

procedure TfrmInputMasterCard.txtKdNmrKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then cmbUnit.SetFocus;
end;

procedure TfrmInputMasterCard.txtKodeKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNama.SetFocus;
end;

procedure TfrmInputMasterCard.txtCityKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then txtState.SetFocus;
end;

procedure TfrmInputMasterCard.txtContactKeyPress(Sender: TObject;
  var Key: Char);
begin
if key = #13 then txtPng1.SetFocus;
end;

procedure TfrmInputMasterCard.txtCountryKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then cmbDes.SetFocus;
end;

procedure TfrmInputMasterCard.txtNamaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtKdNmr.SetFocus;
end;

procedure TfrmInputMasterCard.txtNoFaxKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtEmail.SetFocus;
end;

procedure TfrmInputMasterCard.txtNoTelp2KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNoFax.SetFocus;
end;

procedure TfrmInputMasterCard.txtNoTelpKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNoTelp2.SetFocus;
end;                            

procedure TfrmInputMasterCard.txtPosKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then txtNoTelp.SetFocus;
end;

procedure TfrmInputMasterCard.txtStateKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then txtCountry.SetFocus;
end;

procedure TfrmInputMasterCard.txtWebsiteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtContact.SetFocus;
end;

procedure TfrmInputMasterCard.txtPng1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtJab1.SetFocus;
end;

procedure TfrmInputMasterCard.txtJab1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtPng2.SetFocus;
end;

procedure TfrmInputMasterCard.txtPng2KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtJab2.SetFocus;
end;

procedure TfrmInputMasterCard.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
