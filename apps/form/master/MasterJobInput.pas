unit MasterJobInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker;

type
  TfrmInputMasterJob = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    txtKode: TAdvEdit;
    txtNama: TAdvEdit;
    mmDesk: TMemo;
    Label1: TLabel;
    Label7: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    txtContact: TAdvEdit;
    Label9: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label18: TLabel;
    cmbDiv: TComboBox;
    txtCust: TAdvEdit;
    btnCust: TAdvGlowButton;
    dtpStartDate: TAdvDateTimePicker;
    dtpFinishDate: TAdvDateTimePicker;
    Label6: TLabel;
    mmLokasi: TMemo;
    txtNoPo: TAdvEdit;
    Label8: TLabel;
    Label10: TLabel;
    procedure btnBatalClick(Sender: TObject);
    procedure txtKodeKeyPress(Sender: TObject; var Key: Char);
    procedure txtNamaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure mmKetKeyPress(Sender: TObject; var Key: Char);
    procedure txtWebsiteKeyPress(Sender: TObject; var Key: Char);
    procedure btnCustClick(Sender: TObject);
    procedure txtContactKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    datenow: tdate;
    seqCard , seqmaster : integer;
    EditMode : Boolean;
    function isValid : Boolean;
    function isSaved : Boolean;
    procedure loaddata;
    procedure InitForm;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(seq : integer; statedit : boolean):boolean;
  end;

var
  frmInputMasterJob: TfrmInputMasterJob;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi;

{$R *.dfm}

procedure TfrmInputMasterJob.btnCustClick(Sender: TObject);
begin
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.ExecuteCard('', botrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmInputMasterJob.btnSimpanClick(Sender: TObject);
begin
  if IsValid and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if not EditMode then begin
      if IsSaved then begin
        if Confirmed(MSG_ADD_DATA) then begin
          InitForm; seqmaster := 0; txtKode.SetFocus; statSimpan := True;
        end else ModalResult := mrOk;
      end else begin
        InForm(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
    end else begin
      if IsSaved then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else begin
        InForm(MSG_UNSUCCESS_UPDATE);
        ActiveControl := btnBatal;
      end;
    end;
  end;
end;

function TfrmInputMasterJob.execute(seq: integer;
  statedit: boolean): boolean;
begin
  datenow:=ServerNow;
  InitForm;
  seqmaster := seq;
  EditMode := seqmaster <> 0;
  if Editmode then begin
    self.Caption := 'Edit Pekerjaan';
    loaddata;
  end else self.Caption := 'Tambah Pekerjaan';
  Run(Self, True);
  Result := ModalResult = mrOk;
end;

procedure TfrmInputMasterJob.FormShow(Sender: TObject);
begin
  btnCust.SetFocus;
end;

procedure TfrmInputMasterJob.InitForm;
begin
  txtSetAllEmpty(self);
  txtKode.Text := GetNextNumberMaster(master_Job);
  cmbDiv.text  := '';
  cmbDiv.ItemIndex  := -1;
  seqMaster := 0;
  seqCard := 0;
  dtpstartdate.date := datenow;
  dtpfinishdate.date := datenow;
end;

function TfrmInputMasterJob.isSaved: Boolean;
var dataMaster : TR_master_Job;
    dtJual : TR_data_jual_master;
begin
  try
    myConnection.BeginSQL;
    dataMaster.kode        := TrimAllString(txtKode.Text);
    dataMaster.nama        := TrimAllString(txtNama.Text);
    if cmbDiv.ItemIndex = 0 then
      dataMaster.unit_bisnis    := TIPE_DIVISI_Logistic
    else if cmbDiv.ItemIndex = 1 then
      dataMaster.unit_bisnis    := TIPE_DIVISI_TRADING
    else if cmbDiv.ItemIndex = 2 then
      dataMaster.unit_bisnis    := TIPE_DIVISI_CONSTRUCTION;

    dataMaster.card_seq    := seqCard;
    dataMaster.deskripsi   := TrimAllString(mmDesk.lines.Text);
    dataMaster.contact     := TrimAllString(txtContact.Text);
    dataMaster.tgl_mulai   := dtpStartDate.Date;
    dataMaster.tgl_target  := dtpFinishDate.Date;
    dataMaster.user_id     := GlobalSystemUser.UserId;
    dataMaster.tgl_input   := ServerNow;
    dataMaster.lokasi      := TrimAllString(mmLokasi.lines.text);
    dataMaster.no_po       := TrimAllString(txtNoPo.text);

    if EditMode then begin
      DataMaster.seq := seqmaster;
      Update_master_Job(DataMaster);

      dtJual := Get_data_jual_master(0, seqmaster);
      if dtJual.seq > 0 then begin
        dtJual.nomor_po := dataMaster.no_po;
        Update_data_jual_master(dtJual);
      end;

    end else begin
      seqmaster := Save_master_Job(DataMaster);
    end;

    Get_List_master_Job(ListJobGlobal, seqmaster);

    myConnection.EndSQL;
    Result := true;
  Except
    myConnection.UndoSQL;
    inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

function TfrmInputMasterJob.isValid: Boolean;
begin
  Result := True;
  if seqCard = 0 then begin
    inform('Customer harus dipilih.');
    txtCust.SetFocus;
    Result := false;
    Exit;
  end;

  if cmbDiv.ItemIndex = -1 then begin
    inform('Unit Bisnis harus dipilih.');
    cmbDiv.SetFocus;
    Result := false;
    Exit;
  end;

//  if UEngine.Is_Kode_Field_Exist('master_Job','kode',TrimAllString(txtKode.Text),seqmaster) then begin
//    inform('Kode sudah ada.');
//    txtKode.SetFocus;
//    Result := false;
//    Exit;
//  end;

  if TrimAllString(txtNama.Text) = '' then begin
    inform('Nama Pekerjaan harus diisi.');
    txtNama.SetFocus;
    Result := false;
    Exit;
  end;

  if TrimAllString(txtNoPO.Text) = '' then begin
    inform('No.PO/SPK harus diisi.');
    txtNama.SetFocus;
    Result := false;
    Exit;
  end;

  if TrimAllString(mmDesk.Lines.Text) = '' then begin
    inform('Alamat harus diisi.');
    mmDesk.SetFocus;
    Result := false;
    Exit;
  end;
end;

procedure TfrmInputMasterJob.loaddata;
var datamaster : TR_master_Job;
begin
  DataMaster          := Get_master_Job(seqmaster);

  txtKode.Text :=  dataMaster.kode;
  txtNama.Text :=  dataMaster.nama;
  txtContact.Text := dataMaster.contact;
  mmLokasi.lines.text := dataMaster.lokasi;

  if dataMaster.unit_bisnis = TIPE_DIVISI_LOGISTIC then
    cmbDiv.ItemIndex := 0
  else if dataMaster.unit_bisnis = TIPE_DIVISI_TRADING then
    cmbDiv.ItemIndex := 1
  else if dataMaster.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
    cmbDiv.ItemIndex := 2;

  seqCard := dataMaster.card_seq;
  txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                  EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);

  txtNoPo.text       := dataMaster.no_po;
  mmDesk.lines.Text  := dataMaster.deskripsi;
  dtpStartDate.Date  := dataMaster.tgl_mulai;
  dtpFinishDate.Date := dataMaster.tgl_target;
end;

procedure TfrmInputMasterJob.mmKetKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnSimpan.SetFocus;
end;

procedure TfrmInputMasterJob.txtKodeKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtNama.SetFocus;
end;

procedure TfrmInputMasterJob.txtContactKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then dtpStartDate.SetFocus;
end;

procedure TfrmInputMasterJob.txtNamaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then mmDesk.SetFocus;
end;

procedure TfrmInputMasterJob.txtWebsiteKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtContact.SetFocus;
end;

procedure TfrmInputMasterJob.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
