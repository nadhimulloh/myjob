object frmRekapMasterJob: TfrmRekapMasterJob
  Left = 265
  Top = 178
  Caption = 'frmRekapMasterJob'
  ClientHeight = 361
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 706
    Height = 361
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 704
      Height = 359
      Align = alClient
      TabOrder = 0
      DesignSize = (
        704
        359)
      object AdvProgressBar1: TAdvProgressBar
        Left = 569
        Top = 338
        Width = 128
        Height = 15
        Anchors = [akRight, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Level0ColorTo = 14811105
        Level1ColorTo = 13303807
        Level2Color = 5483007
        Level2ColorTo = 11064319
        Level3ColorTo = 13290239
        Level1Perc = 70
        Level2Perc = 90
        Position = 50
        ShowBorder = True
        Version = '1.2.0.2'
        ExplicitTop = 248
      end
      object Shape1: TShape
        Left = 7
        Top = 340
        Width = 13
        Height = 11
        Anchors = [akLeft, akBottom]
        Brush.Color = clBlack
        ExplicitTop = 250
      end
      object Shape2: TShape
        Left = 62
        Top = 340
        Width = 13
        Height = 11
        Anchors = [akLeft, akBottom]
        Brush.Color = clRed
        ExplicitTop = 250
      end
      object Label1: TLabel
        Left = 26
        Top = 339
        Width = 22
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Aktif'
      end
      object Label2: TLabel
        Left = 81
        Top = 339
        Width = 40
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Nonaktif'
      end
      object btnAktivasi: TAdvGlowButton
        Left = 88
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Aktivasi'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000002A749444154484BBD955D48936114C7FFC76DA9E55083410415CEB6A0
          A2BA90480C15C9822E2A1A894A19413150C8A01B89482804BBE8222F04C382FC
          4E28EA22A8EC03FB80EC4683A29AD2EA6215097E6DAE9CDB7B3AEFEBD7FC78E7
          56DA0B0F6CCF73CEF9FDCF73CE795FC2323FB4CCF1311B50C7A634F61D84C276
          30B9DCC6A47B70D2F8BF889806ACA9F15A120D782CC86D3301E9EDAF20E7FF38
          6DEEFF5BC83420ADD6D700F0B10C43370A56BDC21DFF2EBC0E66485C6A709726
          1D5F0280774882243F587B05F6758731EAA9458EE71C0694D421779939752900
          831224A539B51299493F01DF1B5CFB5D826A7FB92E806F6CB420014780B8A754
          ECFAB8908879579465EC42A3B954B3F57302F67BDB1A3B9D9B4BC29DB9DD9A0C
          AF49C14ABE2BFB79B2FA3112B491F3F3F05CC8EC221BD12106DBDBCC27B1D3D8
          ADD98EB3E1EA8AE20F67A61CB9D9962D8DF050D698D42724FBABB533522AA9B0
          EFA22E403B9036B52ABE03A7E26F665624D69C9D70643FE2C6D31132A9901C30
          8641B46F3210AB16DA6FE62118692B15B83CE110DD41E356DB33F1CD9D84DC96
          B970CC0B3A572EB8868A7ACBA303DC92AB50A873529D823822511F79F2195E8C
          05D6D3892F6A474E689B27226C835BED4FE4AF5AC4181EBE20595C8A0ED062DF
          2D125EC4105D357551916B535400AD762DB60E29EA9E18200101C4470F684ACF
          82C1F03206805B00565D00D7C104CBDE7A1853B660B4E7B918DE973CCE4F77D4
          6224E22A2AEC157B9D22CB95E463C3E547084A237CAB9EB29BE9F7C8806E0C0C
          665359BF4F1FD06CB3C2E27887314F227C5D8BE99D3A0F4896D711E20A3ADA37
          12EEB4609B72BB6D0742A4BE900EC9B2E852B4D7059A10A02A2AF9E45EC82EF2
          1CB4C380903D57D4396492F3A465D32488AAF63D14A94D42A09E1C5FBF474AF3
          3F7F93A3BDF118EC963D833FEB00E51952C772E70000000049454E44AE426082}
        TabOrder = 0
        OnClick = btnAktivasiClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnTambah: TAdvGlowButton
        Left = 7
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Tambah'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001F349444154484BBD964D4EC25010C7E7D558F108468C8961E14A177A
          092C4A0B022A9E4023317A012FC006A327F01BA4AD14B8842E74C542A389E819
          F8086F9CA22095B64002BEA48BBE8FF9CDFC67DEB40C463CD888ED832BC05FF0
          4F8C553CEBC050418465C660CA7408013EE9E00320571BE57AA69828569D1C75
          04046E8331C22781B169B7280956A6E7B0A0683776FBBA014720488BF2312DEC
          0C241FE289F1A427E00878E7B92E80A4CA27031BFFB148919CE6156DD711B0A2
          CA5101E07A20CFFF6CE6D08815945C5BAE7604FE1425D42BBEB869BE391F6B9A
          BB2C39FB40C550E69EAAAFB8F29DF83620905D8B0313CEDCBCCFC96A73795553
          DC8344BE6D84EECEAD0055CED044783800CC18213D62014859F98DEA7C762800
          805743D1E6FE46606A2676024CCDB7E6375CE5B8285DD9E5A44A00CFFF02FE43
          A2A1251919A4F3B216B548B4AA05B71059B3B49C46BF65CA38C67361FDC20230
          2F9A3033F14C17C3EB04E8E7A2D1D9F7C9FAB82F1D4DD72C00F345CA2A11C6D0
          B62BBADFACDF55441ECD87EED2AD19BB6697A2C9BD7E0D76EE4386C779993A6A
          C7B06DD78185608A7A92A52BF6022290F1477DBF67BB6E1992B26B249790A4F7
          991EC6DF81E38111D6CD2AEC1AAE9FCCC84D44AC88B575E4A050444B74BAF575
          FBA052BC1700354F4DCCB4123A30A0972CFDAC8FFCAFE20B6E37C619BA920FF3
          0000000049454E44AE426082}
        TabOrder = 1
        OnClick = btnTambahClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnEdit: TAdvGlowButton
        Left = 169
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Edit'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000019C49444154484B6364A03160A4B1F90C700BEC03B716FEFFCFC047AE
          85FF19183F1EDEE035015D3FB205FFC9351CA6EFE07A6F8C10A19F05BBEFFDA6
          D807AE4AACB87D40570BAE3FFB0A8F8E1BCFBEE18C1A0D292E064D296EB83CD1
          3E18B500394CE1B18E1CC9E404D17F602ED59E51BD8CE1DF3FE17FFFFE05CAF6
          F77F0759441D0BFEFF65F8F1E61E03F7831BCFD44E1D1567FCFF7F9F745F9F1B
          C516805290A6241B83CEA55886A7AC060C8FB91C19F85FBF7CAE72EAF801D9DE
          EE288A2C0079DD458B97C1F2762283F8CB35E060BF2856FCE0BE40D05901C65F
          118E8E8E7FC8B64046889DC14C910BE8F218A0E16B91E374CD01F5864847C706
          B0E1645980CDE520835E8A0532885F5ACFCAD8C000371CA705F84A55C6FF7FC0
          610E0B1698E157F49632B8A870515616E133FC3F132B03FEA2E2FEEF7A42F581
          E2FB0E0BA5AB8D968C5FFFF083D4FE6215DC79C4EED1F17F2C1C60ADAE8AAC8D
          E8669054655E5D2EB65A4BEA8307C383DFBF18BEFDDFCFF09221023DCC29B220
          36D6F165B9D7D543DAA21FD6321CF9B58690E128914C2878C895272988C8B104
          009E9BF519AA8F99290000000049454E44AE426082}
        TabOrder = 2
        OnClick = btnEditClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnEkspor: TAdvGlowButton
        Left = 331
        Top = 81
        Width = 75
        Height = 36
        Caption = 'E&kspor'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001FC49444154484B6364A03160A4B1F90C036F815E891EF75F4E260366
          4646BDFFFF190D803E36FEFFFFDFDA2B2D17DA89F13D8A0F342A35845999390D
          41863032301A3230FED7F9CFC0A0096433A118F69FA1E152CBB946922DD0AB35
          029A4718F04BF2338030360034E0E3D6C00D023039141F50C30290C15B0237C0
          CD25CB026B5D2B061B5D6B86E5375632446A84C33D72F9CD15862B6FAE12B6C0
          5BCF934156589661D5A9350CEFBEBE633055346130563062587F7623C3CB4F2F
          19321CD318321DD3197C3704326C0E580FB760D98D15604B09FAC052D9826146
          FC5486F98717324CDB3F836173FE7A86171F5F3024CC4961F80F84F8E280E820
          9A18D5CF60A268CCB0F0E8228674875486C8E9310CB75EDE06BB96621F800C91
          1592615897B39A81099842579C5AC5D0BDBD171E1414C701C824255145869599
          CB1818191919369FDFC2D0B8A9857A16800C9D9F3C87418C579461CBC56D0CA9
          F6C90C71B393182E3FB94C9D200A330B65A8F6A960A85E5BCBB0F7FA7E866D05
          9B185E7D7ECD10353396E1EFBFBF9447728C6514033B0B3BC3BC230B18FEFFFF
          CF60AD62C5A02BABC3B015E89BC7EF9E502792F115185489647C1650251F906B
          0130443F6E0DC251D82117D7C0F4A9C3C8F0DF90AAC5353657832A9CFF9CCC7A
          408BF481F58229B0743464F8FF6FF525722A1CC23501E92A06BE4E26DDCDA83A
          00FCFBFC193DEFE1C10000000049454E44AE426082}
        TabOrder = 3
        OnClick = btnEksporClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnCetak: TAdvGlowButton
        Left = 412
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Cetak'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000019249444154484B6364A03160A4B1F90C582D9874EAB7FD7FC67F0EA4
          58CEF89FE9409E19EB41743D582D9878FA67FDFFFF0C0D2459C0C8D0906FCADE
          38022C707272AA5777896E507789212584186EEE5902C44B1BF6EDDB87124CF0
          38F0F4F46497909048F8F6ED5B049F8C8683B0921E4916BCBD7789E1D3931B07
          383939577EFDFA75E1EAD5ABBF830C805B909090B096919131882453712B5E37
          7FFEFC60140B1213137F03055828B1E096310703E37F0606D5733FFE002D6045
          B70028453D00B4001C3AF02002FA80B61604060652D582F5EBD7A3FA00983CA9
          6A0130B962B7E0CF9F3F0CC06446566470737333B0B040D2094E0B4086FFD10D
          65E077CC24C9928FFBA733B05C5ECD00B2049705F0640ACCC90C64E66498A3FE
          007D809A4C1D1D1D3700339A3F4805B2059D6B6EE2F589B59630838D9608ACA8
          00ABFDFFFFFFDAFDFBF787A024535051F1E3C78F78A02592C651550ED27AB60E
          A484D1D34B870F9C5DD6760068F8F3C78F1F2FBC73E7CE4F140B900D43AE0F70
          F900E672983EC641551FD0BCCA2425EC09A9A579AB0200EA53BC1998AD60B800
          00000049454E44AE426082}
        TabOrder = 4
        OnClick = btnCetakClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object asgRekap: TAdvStringGrid
        Left = 7
        Top = 123
        Width = 690
        Height = 211
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 17
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 5
        OnMouseWheelDown = asgRekapMouseWheelDown
        OnMouseWheelUp = asgRekapMouseWheelUp
        OnGetCellColor = asgRekapGetCellColor
        OnGetCellPrintColor = asgRekapGetCellPrintColor
        OnGetAlignment = asgRekapGetAlignment
        OnClickCell = asgRekapClickCell
        OnDblClickCell = asgRekapDblClickCell
        OnGetFloatFormat = asgRekapGetFloatFormat
        OnSelectionChanged = asgRekapSelectionChanged
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = [fsBold]
        ColumnHeaders.Strings = (
          'No.'
          'Unit Bisnis'
          'Nama Pekerjaan'
          'Customer '
          'Nomor Pekerjaan'
          'No. PO/ SPK'
          'Deskripsi'
          'Kontak'
          'Tgl. Mulai'
          'Target Tgl. Selesai'
          'Lokasi'
          'User Id'
          'Aktivasi'
          'Seq')
        ColumnSize.Stretch = True
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedRowHeight = 22
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = [fsBold]
        FloatFormat = '%.2f'
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'Tahoma'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'Tahoma'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'Tahoma'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'Tahoma'
        PrintSettings.FooterFont.Style = []
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'Tahoma'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurrence'
        SearchFooter.HintFindPrev = 'Find previous occurrence'
        SearchFooter.HintHighlight = 'Highlight occurrences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SearchFooter.ShowClose = False
        SearchFooter.ShowHighLight = False
        VAlignment = vtaCenter
        Version = '6.0.4.2'
        WordWrap = False
        ColWidths = (
          64
          64
          64
          64
          64
          64
          64
          64
          69
          64
          64
          64
          64
          64
          64
          64
          64)
        RowHeights = (
          22
          22
          22
          22
          22
          22
          22
          22
          22
          22)
      end
      object btnLoad: TAdvGlowButton
        Left = 493
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Load'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000008B49444154484B6364A03160A4B1F90CF4B7E0FCA1E7DE4CCC4C26E4
          F8ECDFDF7F670CED24B722EBC5F0C1C5C32F7E303032B2936301D0B01F7A36E2
          9CF82D38F2F23F3986C3F4E8DB88A3381AD30734B780D64174E9C82BAFFF8CFF
          4DC90926C6FF8CA7F56CC4B6E18D83D154349A8AF026AED1544454DE1B4D4594
          A722A2029A0445F4AF9349701C514A0179396619DA9374400000000049454E44
          AE426082}
        TabOrder = 6
        OnClick = btnLoadClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnReset: TAdvGlowButton
        Left = 574
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Reset'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001A949444154484BC594CF4B025110C767B414CA4DA32890ACD5FA1322
          E8187429F05AA7AE950A42A78E6DB7A04B1DDCF0DA293C06D929BA17FD079A4B
          BF0E1A62AE4684F97A6B4AFEDA1D5793DEF5CD7C3F33EF3B6F10FA7CB0CFFAD0
          33602AF236F7147226F40AED19209EA87BC07046490F6F8284A566D09F009081
          C4805D16D9C75A263451A887B40262CC26660A8788B0C10347CD78C418DC592D
          B09A0C08E95A5E0B408CA8C75C3C6C46B82196C1552A242CEB036435CBA9A62A
          FF05E08D0599DFB003AFACB25A82B5E47026C29837EA4633B9E20162BC587E5F
          273DA807A4820239043F00F4A4D2B7DB202DD1535419BBEA5102C23EE585279A
          9B7DDC7225FBF60FA802C827A004A87B43C0B49CF33D045DF79488D1B3B60748
          D703DEC979998FD373273E180D460BC01D6543F6927A06887E86205100DF41D6
          C9460673B52E9B27AF01E08EE6C7ED5F7001800BD4B3B4BBE71D6795A030A6BB
          8B44391F47C0956EC4B51C0E38E2801D5D80D681AD8CE7FC672E9A816895F3F8
          5325EDD8E52BFBD3709B9AF5802AA4FD14C598557C55656EF40B657277806A16
          B5062871EDFE7F7F7227155231DF32F49D19DFE1983D0000000049454E44AE42
          6082}
        TabOrder = 7
        OnClick = btnResetClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object chbSF: TAdvOfficeCheckBox
        Left = 126
        Top = 335
        Width = 163
        Height = 20
        Anchors = [akLeft, akBottom]
        TabOrder = 8
        OnClick = chbSFClick
        Alignment = taLeftJustify
        Caption = 'Tampilkan Kolom Pencarian'
        ReturnIsTab = False
        Version = '1.3.4.1'
      end
      object GroupBox1: TGroupBox
        Left = 7
        Top = 2
        Width = 690
        Height = 73
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Filter'
        TabOrder = 9
        object Label5: TLabel
          Left = 8
          Top = 47
          Width = 34
          Height = 13
          Caption = 'Sampai'
        end
        object Label6: TLabel
          Left = 8
          Top = 20
          Width = 19
          Height = 13
          Caption = 'Dari'
        end
        object Label3: TLabel
          Left = 153
          Top = 21
          Width = 31
          Height = 13
          Caption = 'Status'
          Transparent = True
        end
        object Label4: TLabel
          Left = 378
          Top = 21
          Width = 48
          Height = 13
          Caption = 'Unit Bisnis'
          Transparent = True
        end
        object Label19: TLabel
          Left = 152
          Top = 47
          Width = 46
          Height = 13
          Caption = 'Customer'
        end
        object dtpAwal: TDateTimePicker
          Left = 55
          Top = 16
          Width = 89
          Height = 21
          Date = 41127.626447476840000000
          Time = 41127.626447476840000000
          TabOrder = 0
        end
        object dtpAkhir: TDateTimePicker
          Left = 55
          Top = 43
          Width = 89
          Height = 21
          Date = 41127.626776620360000000
          Time = 41127.626776620360000000
          TabOrder = 1
        end
        object cmbStatus: TAdvComboBox
          Left = 204
          Top = 17
          Width = 143
          Height = 21
          Color = clWindow
          Version = '1.4.0.0'
          Visible = True
          Style = csDropDownList
          DropWidth = 0
          Enabled = True
          ItemIndex = -1
          ItemHeight = 13
          Items.Strings = (
            'Semua'
            'Aktif'
            'Nonaktif')
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          TabOrder = 2
        end
        object cmbDes: TComboBox
          Left = 432
          Top = 17
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 3
          Items.Strings = (
            'Semua'
            'Logistic'
            'General Services'
            'Construction')
        end
        object btnCust: TAdvGlowButton
          Left = 351
          Top = 43
          Width = 21
          Height = 20
          Caption = '...'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          TabOrder = 4
          OnClick = btnCustClick
          Appearance.ColorChecked = 16111818
          Appearance.ColorCheckedTo = 16367008
          Appearance.ColorDisabled = 15921906
          Appearance.ColorDisabledTo = 15921906
          Appearance.ColorDown = 16111818
          Appearance.ColorDownTo = 16367008
          Appearance.ColorHot = 16117985
          Appearance.ColorHotTo = 16372402
          Appearance.ColorMirrorHot = 16107693
          Appearance.ColorMirrorHotTo = 16775412
          Appearance.ColorMirrorDown = 16102556
          Appearance.ColorMirrorDownTo = 16768988
          Appearance.ColorMirrorChecked = 16102556
          Appearance.ColorMirrorCheckedTo = 16768988
          Appearance.ColorMirrorDisabled = 11974326
          Appearance.ColorMirrorDisabledTo = 15921906
        end
        object txtCust: TAdvEdit
          Left = 204
          Top = 44
          Width = 143
          Height = 21
          EditType = etUppercase
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          MaxLength = 50
          ReadOnly = True
          TabOrder = 5
          Visible = True
          Version = '2.9.2.1'
        end
      end
      object btnHapus: TAdvGlowButton
        Left = 250
        Top = 81
        Width = 75
        Height = 36
        Caption = '&Hapus'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000019949444154484B6364A03160A4B1F90C4459F0DF8241818185211ECD
          318B198F30DC23E4400C0BFE175A0A31FC654F036A64876B7EFD3481E1CF6F05
          14C398591E3188C9CC4312FBC9F0F5E72CC6B9C7DF21ABC3B420CFA102A8A09D
          90CBB0CAFF63A8649C72A003BF05B98EF60C1F5E3530FCF8EA4092251CDC0718
          04C41A1827EF3F88D70290E47F1B867A60EC349064C17F8606609C34A2EBC11A
          C9C3CF82FF1C5C0C8C3FBEA1F81E438CDC20FA2F2ECFF0A7773703D39A090CCC
          1BA6812DF9EB97CEF02FA28481A5D88D81F1F97D88C5E45AF0CF2D86E16FD95C
          B0194C930B8006FD63F8973709CC67EE4E6560DAB988320BC02EF64D63F8973F
          19258898A614C27D44910F60A6FEA959C2F0DF310CCC653CB48E81A531023545
          921B44F03087060BCC54AAF9E09F5B2C300EE6D02E0EFE8BC94252D1DA498854
          048A93F062482A7AF990F248A6693E20BA3C2231926B80855D33D18643325A0D
          B0B06B45D783BDB0B36630015A700C885989B4E437D0024BA0056789B200EC20
          4B06430666066F50862560C95FA0FC766C8683F30C912E245B19CD2D0000FF04
          C019FC3522A60000000049454E44AE426082}
        TabOrder = 10
        OnClick = btnHapusClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
    end
  end
end
