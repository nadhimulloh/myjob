unit ImportDataBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, AdvGlowButton, Gauges, ExtCtrls, tmsAdvGridExcel, tmsUFlxFormats, StdCtrls, ShellAPI;

type
  TfrmImportDataBarang = class(TForm)
    mainpanel: TPanel;
    Gauge1: TGauge;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    asgImport: TAdvStringGrid;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Addxls: TAdvGridExcelIO;
    OpenDialog1: TOpenDialog;
    chbSSF: TCheckBox;
    asgTampung: TAdvStringGrid;
    procedure btnLoadClick(Sender: TObject);
    procedure btnBatalClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure asgImportAutoDeleteRow(Sender: TObject; ARow: Integer);
    procedure asgImportCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgImportGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure AddxlsCellFormat(Sender: TAdvStringGrid; const GridCol, GridRow, XlsCol, XlsRow: Integer; const Value: WideString; var Format: TFlxFormat);
    procedure FormDestroy(Sender: TObject);
    procedure chbSSFClick(Sender: TObject);
    procedure asgImportCanEditCell(Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
    procedure asgImportGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
    procedure asgImportDblClickCell(Sender: TObject; ARow, ACol: Integer);
  private
    { Private declarations }
    ListKategori : TStringList;
    StatusSimpan : Boolean;
    procedure SetGrid;
    procedure ArrangeColSize;
    function IsValid : boolean;
    function IsSaved : boolean;
    procedure SetDataExist(Row : integer;IsBarang,IsKategori: boolean);
    procedure arrangeXls;
  public
    { Public declarations }
    function Execute : boolean;
  end;

var
  frmImportDataBarang: TfrmImportDataBarang;

implementation

uses
  OracleConnection, UGeneral, UEngine, URecord, USystemMenu, UConst, MainMenu, Loading, LOV;

const
  colno          = 0;
  colPartNo      = 1;
  colNama        = 2;
  colMerek       = 3;
  colHarga       = 4;
  colKategori    = 5;
  colMinStok     = 6;
  colseqKategori = 7;


{$R *.dfm}

procedure TfrmImportDataBarang.AddxlsCellFormat(Sender: TAdvStringGrid; const GridCol, GridRow, XlsCol, XlsRow: Integer; const Value: WideString; var Format: TFlxFormat);
begin
  if (GridCol in [colHarga]) then format.format := '#.##0,00';

end;

procedure TfrmImportDataBarang.ArrangeColSize;
begin
  asgImport.AutoNumberCol(colNo);
  asgImport.AutoSizeColumns(true);

//  asgImport.AutosizeCol(ColNo, 5);
//  asgImport.AutosizeCol(colJenis, 5);
//  asgImport.AutosizeCol(colKode, 5);
//  asgImport.AutosizeCol(colNama, 5);
//  asgImport.AutosizeCol(colKategori, 5);
//  asgImport.AutosizeCol(colSatuan, 5);
//  asgImport.AutosizeCol(colMargin, 5);
//  asgImport.AutosizeCol(colHargaBeli, 5);
//  asgImport.AutosizeCol(colHargaJual, 5);
//  asgImport.AutosizeCol(colMinimalStok, 5);
//
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgImport.ColWidths[colSeqKategori] := 0;
  end;
//  asgImport.ColWidths[colkemasan] := 0;
//  asgImport.ColWidths[colactual] := 0;
//  asgImport.ColWidths[colHModal] := 0;
//  asgImport.ColWidths[colJual1] := 0;
//  asgImport.ColWidths[colJual2] := 0;
//  asgImport.ColWidths[colJual3] := 0;
//  asgImport.ColWidths[coljml1] := 0;
//  asgImport.ColWidths[coljml2] := 0;
//  asgImport.ColWidths[coljml3] := 0;
//  asgImport.ColWidths[coljasa] := 0;
//  asgImport.ColWidths[colGudang] := 0;
end;

procedure TfrmImportDataBarang.arrangeXls;
var i,Row : integer;
    isAddrow : boolean;
begin
  isAddrow := false;
  for i := 0 to asgTampung.rowcount-1 do begin
    if (asgTampung.Cells[colPartNo,i] <> '') then begin
      if (asgTampung.Cells[colPartNo,i] = 'CATATAN :') then break;
      if isAddrow then begin
        asgImport.addrow;
      end else isAddrow := true;
      row := asgImport.rowcount-1;
      asgImport.Cells[colPartNo,Row]  := asgTampung.Cells[colharga,i];
      asgImport.Cells[colNama,Row]    := asgTampung.Cells[colPartNo,i];
      asgImport.Cells[colMerek,Row]   := asgTampung.Cells[colKategori,i];
      asgImport.Cells[colHarga,Row]   := asgTampung.Cells[colMinstok,i];
      asgImport.Cells[colMinStok,Row] := asgTampung.Cells[colnama,i];
      asgImport.ints[colseqkategori,Row] := asgTampung.ints[10,i];
      asgImport.Cells[colKategori,Row]   := EkstrakString(ListKategoriSparPartGlobal.Values[asgImport.Cells[colseqkategori,row]],'#',2);
    end;
  end;
  ArrangeColSize;
end;

procedure TfrmImportDataBarang.asgImportAutoDeleteRow(Sender: TObject; ARow: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmImportDataBarang.asgImportCanEditCell(Sender: TObject; ARow, ACol: Integer; var CanEdit: Boolean);
begin
  if ARow > 0 then begin
//    if asgImport.Ints[colSeq,ARow] <> 0 then CanEdit := acol = colNama
//    else
    CanEdit := acol in [colNama,colPartNo];
  end;
end;

procedure TfrmImportDataBarang.asgImportCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
var Seq : integer;
begin
  value := TrimAllString(value);
  if acol = colNama then begin
    SetDataExist(ARow,true,false);
  end;

  if acol = colKategori then begin
    value := TrimAllString(Value);
    asgImport.ints[colSeqKategori,ARow] := 0;
    Seq := Get_Seq_By_KodeNama(value,boTrue, 'master_kategori_spare_part');
//    if Seq = 0 then begin
//      Application.CreateForm(TfrmLOV, frmLOV);
//      frmLOV.OnClose := frmMainMenu.ChildFormSingle;
//      Seq :=  frmLOV.ExecuteKategoriSparePart(value, botrue);
//    end;
    if Seq = 0 then begin
      asgImport.ints[colSeqKategori,ARow] := 0;
//      asgImport.cells[acol,arow] := '';
//      asgImport.Col := ACol;
//      asgImport.ints[colSeqKategori,ARow] := seq;
//      value := '';
      exit;
    end else if Seq <> 0 then begin
      asgImport.Cells[Acol,ARow] := EkstrakString(ListKategoriSparPartGlobal.values[IntToStr(seq)],'#',2);
      asgImport.ints[colSeqKategori,ARow] := seq;
      value := asgImport.Cells[acol,ARow];
    end; 
  end;
  ArrangeColSize;
end;

procedure TfrmImportDataBarang.asgImportDblClickCell(Sender: TObject; ARow, ACol: Integer);
var seq: integer;
begin
  if arow > 0 then begin
    if acol = colKategori then begin 
//      if asgImport.Ints[colSeq,ARow] = 0 then begin
        Application.CreateForm(TfrmLOV, frmLOV);
        frmLOV.OnClose := frmMainMenu.ChildFormSingle;
        Seq :=  frmLOV.ExecuteKategoriSparePart('', botrue);
        if Seq = 0 then begin
          asgImport.cells[acol,arow] := '';
          asgImport.Col := ACol;
          asgImport.ints[colSeqKategori,ARow] := 0;
          exit;
        end else if Seq <> 0 then begin
          asgImport.Cells[Acol,ARow] := EkstrakString(ListKategoriSparPartGlobal.values[IntToStr(seq)],'#',2);
          asgImport.ints[colSeqKategori,ARow] := seq;
        end;
//      end;
    end;
  end;
end;

procedure TfrmImportDataBarang.asgImportGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if acol in [colNo, colHarga] then HAlign := taRightJustify;
  VAlign := vtaCenter

end;

procedure TfrmImportDataBarang.asgImportGetEditorType(Sender: TObject; ACol, ARow: Integer; var AEditor: TEditorType);
begin
  AEditor := edUpperCase;
end;

procedure TfrmImportDataBarang.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmImportDataBarang.btnLoadClick(Sender: TObject);
begin
  OpenDialog1.Filter := 'Excel 97-2003 Workbook|*xls';
  OpenDialog1.InitialDir := GetAppPath;
//  if OpenDialog1.Execute then begin
  SetGrid;

  asgTampung.ClearNormalCells;
  asgTampung.RowCount := 3;
  asgTampung.FixedCols := 1;
  asgTampung.FixedRows := 1;

  asgImportFromExcell(asgTampung, OpenDialog1, 2, 2, 1, 1);
//    asgTampung.LoadFromXLS(OpenDialog1.FileName);
  arrangeXls;
  ArrangeColSize;
//  end;
//  asgImport.ColumnSize.Stretch := false;
//  asgImport.ColumnSize.StretchColumn := asgImport.RowCount-1;
//  asgImportFromExcell(asgTampung, OpenDialog1, 2, 2, 1, 1);
//  asgImportFromExcell(asgImport, OpenDialog1, 2, 3, 1, 1);
//  asgImport.ColCount := asgImport.ColCount+2;
//  asgImport.ColumnSize.StretchColumn := asgImport.ColCount-1;
//  asgImport.ColumnSize.Stretch := true;
//
//  asgImport.Cells[colno       ,0] := 'No.';
//  asgImport.Cells[colNama     ,0] := 'Nama';
//  asgImport.Cells[colJumlah   ,0] := 'Jumlah';
//  asgImport.Cells[colHarga    ,0] := 'Harga';
//  asgImport.Cells[colTotal    ,0] := 'Total';
//  asgImport.Cells[colTanggal  ,0] := 'Tanggal';
//  asgImport.Cells[colToko     ,0] := 'Toko';
//  asgImport.Cells[colPartNo   ,0] := 'No. Part';
//  asgImport.Cells[colKategori ,0] := 'Kategori';
//  asgImport.Cells[colSeq ,0]      := 'seq';
//  asgImport.Cells[colSeqKategori,0] := 'Seq Kategori';
//
//
//  asgImport.Cells[colNo, asgImport.RowCount-1] := '';
//  asgImport.ColumnSize.StretchColumn := 0;
//  asgImport.ColumnSize.Stretch := false;
//  asgImport.ColCount := 12;
//  asgImport.ColumnSize.StretchColumn := asgImport.ColCount-1;
//  asgImport.ColumnSize.Stretch := True;
//  asgImport.ClearCols(asgImport.ColCount-1, 1);
//
//  i := 0;
//  While (i <= asgImport.RowCount-2) do begin
//    i := i + 1;
//    if (asgImport.Cells[colNama,i] = '') then begin
//      if (asgImport.RowCount > 2) then begin
//        asgImport.RemoveRows(i,1);
//        i:= i - 1;
//      end else asgImport.ClearRows(i,1);
//    end;
//  end;
//
//  //SetDataExist(0,true,true);
//  ArrangeColSize;
end;

procedure TfrmImportDataBarang.btnResetClick(Sender: TObject);
begin
  chbSSF.Checked := false;
  asgImport.SearchFooter.Visible := chbSSF.Checked;
  SetGrid;
end;

procedure TfrmImportDataBarang.btnSimpanClick(Sender: TObject);
begin
  if (IsValid) and (Confirmed(UConst.MSG_SAVE_CONFIRMATION)) then begin
    if IsSaved then begin
      SetGrid;
      ModalResult := mrOk;
      Inform(MSG_SUCCESS_SAVING);
      if StatusSimpan = true then begin
        Inform('Ada data yang tidak tersimpan');
        //jika ada data yang ga ke simpen karna sudah ada di DB maka data tsb di tampilin di notepade
        ShellExecute(HANDLE_FLAG_INHERIT, 'open', pchar('List Barang tidak tersimpan.txt'), '','', SW_SHOWNORMAL);
      end;
    end else Inform(MSG_UNSUCCESS_SAVING);
  end;
end;

procedure TfrmImportDataBarang.chbSSFClick(Sender: TObject);
begin
  asgImport.SearchFooter.Visible := chbSSF.Checked;
end;

function TfrmImportDataBarang.Execute: boolean;
begin
  ListKategori := TStringList.Create;
  StatusSimpan          := false;
  Get_list_seq_kategori_spare_part_per_nama(ListKategori,botrue);
  Caption := 'Import Data Barang';
  
  SetGrid;
  Run(Self);
  Result := ModalResult = mrOk;
end;

procedure TfrmImportDataBarang.FormDestroy(Sender: TObject);
begin
  ListKategori.Destroy;
end;

function TfrmImportDataBarang.IsSaved: boolean;
var datamaster : TR_master_spare_part;
    i : integer;
    ListBarangExist : TStringlist;
begin    
  ListBarangExist := TStringList.Create;
  ListBarangExist.Clear;
  try
    myConnection.BeginSQL;
    Gauge1.show;
    for i := 1 to asgImport.RowCount-1 do begin
      Gauge1.Progress := round((i)/asgImport.RowCount * 100);
//      if (asgImport.Ints[colSeqKategori,i] = 0) and
//          (ListKategori.Values[TrimAllString(UpperCase(asgImport.Cells[colKategori,i]))] = '') then begin
//        kategori.kode       := TrimAllString(asgImport.Cells[colKategori,i]);
//        kategori.nama       := TrimAllString(asgImport.Cells[colKategori,i]);
//        Kategori.Akun_Seq   := 0;
//        Kategori.keterangan := '';
//        Kategori.Seq := Save_master_kategori_spare_part(Kategori);
//        ListKategoriSparPartGlobal.Add(inttostr(Kategori.seq)+'='+Kategori.kode+'#'+Kategori.nama+'#'+Kategori.User_id+'#'+Kategori.keterangan+'#'+
//                             DateToStr(Kategori.disable_date)+'#'+inttostr(Kategori.Akun_Seq));
//        ListKategori.Add(kategori.nama+'='+IntToStr(Kategori.Seq));
//      end else begin
//        if asgImport.Ints[colSeqKategori,i] <> 0 then Kategori.Seq := asgImport.Ints[colSeqKategori,i]
//        else Kategori.Seq := StrToIntDef(ListKategori.Values[TrimAllString(UpperCase(asgImport.Cells[colKategori,i]))],0);
//      end;
      //if (asgImport.Ints[colSeq,i] = 0) and (ListBarangExist.Values[TrimAllString(asgImport.Cells[colPartNo,i])] = '') then begin


      datamaster.part_number := TrimAllString(asgImport.Cells[colPartNo,i]);
      datamaster.nama             := TrimAllString(asgImport.Cells[colNama,i]);
      datamaster.nama_dagang      := datamaster.nama;
      datamaster.no_rak           := '';
      DataMaster.armada_seq       := 0;
      datamaster.kategori_seq     := asgImport.Ints[colseqKategori,i];
      datamaster.harga_beli_akhir := asgImport.Floats[colHarga,i];
      datamaster.min_stok         := asgImport.Ints[colMinStok,i];
      DataMaster.keterangan       := '';
      dataMaster.user_id          := GlobalSystemUser.UserId;
      datamaster.akun_seq         := 0;
      datamaster.is_ada_stok      := TRUE_STRING;
      datamaster.merek            := asgImport.Cells[colMerek,i];

      DataMaster.seq := Save_master_spare_part(DataMaster);
      ListsparepartGlobal.Add(inttostr(DataMaster.seq)+'='+DataMaster.part_number+'#'+DataMaster.nama+'#'+DataMaster.nama_dagang+'#'+DataMaster.no_rak+'#'+
                                      inttostr(DataMaster.armada_seq)+'#'+inttostr(DataMaster.kategori_seq)+'#'+floatToStr(DataMaster.harga_beli_akhir)+'#'+inttostr(DataMaster.min_stok)+'#'+DataMaster.user_id+'#'+
                                      DataMaster.keterangan+'#'+''+'#'+DataMaster.is_ada_stok+'#'+inttostr(datamaster.akun_seq)+'#'+DataMaster.merek+'#');
      ListBarangExist.Add(datamaster.part_number+'=Sudah');

    end;
    Gauge1.Hide;
    myConnection.EndSQL;
    Result := true;
  Except
    myConnection.UndoSQL;
    Result := false;
  end;
  ListBarangExist.Destroy;
end;

function TfrmImportDataBarang.IsValid: boolean;
var i,j : integer;
begin
  Result := true;
  Application.CreateForm(TfrmLoading, frmLoading);
  frmLoading.OnClose := frmMainMenu.ChildFormSingle;
  
  i := 0;
  While (i <= asgImport.RowCount-2) do begin
    i := i + 1;
    if (asgImport.Cells[colNama,i] = '') then begin
      if (asgImport.RowCount > 2) then begin
        asgImport.RemoveRows(i,1);
        i:= i - 1;
      end else asgImport.ClearRows(i,1);
    end;
  end;
  if (asgImport.rowcount = 2) and (asgImport.Cells[colNama,1] = '') then begin
    Inform('Tidak ada data barang.');
    btnLoad.SetFocus;
    Result := false;
    Exit;
  end;
  for i := 1 to asgImport.RowCount-1 do begin
    frmLoading.Execute('Barang sedang dicek '+IntToStrFmt(round((i)/asgImport.RowCount * 100))+'%',asgImport.RowCount,i);
    if TrimAllString(asgImport.Cells[colNama, i]) = '' then begin
      Inform('Nama  baris ke '+IntToStr(i)+' belum diisi.');
      asgImport.Col := colNama;
      asgImport.Row := i;
      asgImport.SetFocus;
      Result := false;
      Exit;
    end;

    
    if TrimAllString(asgImport.Cells[colPartNo, i]) = '' then begin
      Inform('No. Part baris ke '+IntToStr(i)+' belum diisi.');
      asgImport.Col := colPartNo;
      asgImport.Row := i;
      asgImport.SetFocus;
      Result  := false;
      Exit;
    end;

//    if asgImport.ints[colSeq, i] = 0 then begin
    if UEngine.Is_Kode_Field_Exist('master_spare_part','part_number',TrimAllString(asgImport.Cells[colPartNo, i]),0) then begin
      inform('No. Part sudah ada.');
      asgImport.Col := colPartNo;
      asgImport.Row := i;
      asgImport.SetFocus;
      Result := false;
      Exit;
    end;
//    end;
         
    if TrimAllString(asgImport.Cells[colKategori, i]) = '' then begin
      Inform('Kategori baris ke '+IntToStr(i)+' belum diisi atau dipilih.');
      asgImport.Col := colKategori;
      asgImport.Row := i;
      asgImport.SetFocus;
      Result  := false;
      Exit;
    end;

    for j := i + 1 to asgImport.RowCount-1  do begin
      if (TrimAllString(asgImport.Cells[colPartNo, i]) = TrimAllString(asgImport.Cells[colPartNo, j])) and (TrimAllString(asgImport.Cells[colnama, i]) <> TrimAllString(asgImport.Cells[colnama, j])) then begin
        Inform('No. Part baris ke '+IntToStr(j)+' sama dengan No. Part baris ke '+IntToStr(i)+' dengan nama yang beda.');
        asgImport.Col := colPartNo;
        asgImport.Row := j;
        asgImport.SetFocus;
        Result  := false;
        Exit;
      end;
    end;

    frmLoading.Close;
  end;
  frmLoading.Close;
  ArrangeColSize;
end;

procedure TfrmImportDataBarang.SetGrid;
begin
//  asgResetGrid(asgImport, 2, 11, 1, 1);
//  asgImport.ColumnSize.StretchColumn := 0;
//  asgImport.ColumnSize.Stretch := false;
  asgImport.ROwCount := 2;
  asgImport.COlCount := 9;

//  asgImport.Cells[colno       ,0] := 'No.';
//  asgImport.Cells[colNama     ,0] := 'Nama';
//  asgImport.Cells[colJumlah   ,0] := 'Jumlah';
//  asgImport.Cells[colHarga    ,0] := 'Harga';
//  asgImport.Cells[colTotal    ,0] := 'Total';
//  asgImport.Cells[colTanggal  ,0] := 'Tanggal';
//  asgImport.Cells[colToko     ,0] := 'Toko';
//  asgImport.Cells[colPartNo   ,0] := 'No. Part';
//  asgImport.Cells[colKategori ,0] := 'Kategori';
//  asgImport.Cells[colSeq ,0]      := 'seq';
//  asgImport.Cells[colSeqKategori,0] := 'Seq Kategori';

//  asgImport.ColumnSize.StretchColumn := asgImport.colCount-1;
//  asgImport.ColumnSize.Stretch := true;
  asgImport.ClearNormalCells;
  ArrangeColSize;
end;

procedure TfrmImportDataBarang.SetDataExist(Row : integer;IsBarang,IsKategori: boolean);
var RowSampai,i,Seqkat : integer;
    Barang : TR_master_spare_part;
    IsLewat : boolean;
begin
  if row = 0 then begin
    Row := 1;
    RowSampai := asgImport.RowCount -1;
  end else RowSampai := Row;

  for i := row to RowSampai do begin
    IsLewat := false;
    if IsBarang then begin
      Barang := Get_master_spare_part(0,asgImport.Cells[colNama,i]);
      if Barang.seq <> 0 then begin
        asgImport.Cells[colPartNo,i] := Barang.part_number;
//        asgImport.ints[colSeq,i] := Barang.seq;
        IsLewat := true;
        asgImport.ints[colSeqKategori,i] := Barang.kategori_seq;
        asgImport.Cells[colKategori,i] := EkstrakString(ListKategoriSparPartGlobal.Values[IntToStr(Barang.kategori_seq)],'#',2)
      end;// else asgImport.ints[colSeq,i] := 0;
    end;
    if not IsLewat then begin
      if IsKategori then begin
        Seqkat := StrToIntDef(ListKategori.Values[UpperCase(TrimAllString(asgImport.Cells[colKategori,i]))],0);
        if Seqkat <> 0 then begin
          asgImport.ints[colSeqKategori,i] := Seqkat;
        end else asgImport.ints[colSeqKategori,i] := 0;
      end;
    end;
  end;
end;

end.
