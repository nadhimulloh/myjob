unit MasterCardRekap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo;

type
  TfrmRekapMasterCard = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnAktivasi: TAdvGlowButton;
    btnTambah: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    xt: TLabel;
    Label4: TLabel;
    cmbStatus: TAdvComboBox;
    cmbTipe: TAdvComboBox;
    cmbDes: TComboBox;
    btnHapus: TAdvGlowButton;
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapSelectionChanged(Sender: TObject; ALeft, ATop, ARight,
      ABottom: Integer);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
  private
    AMenuid : integer;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
    procedure loadedit(Arow : integer =0; isTambahBaru : boolean = false);
    function isDelete(alasan: string): boolean;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmRekapMasterCard: TfrmRekapMasterCard;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, MasterCardInput, StrUtils;

{$R *.dfm}

const
  ColNo         = 0;
  colTypeCard   = 1;
  colDes        = 2;
  ColKode       = 3;
  ColNama       = 4;
  colKodeNmr    = 5;
  colUnit       = 6;
  ColAlamat     = 7;
  colKota       = 8;
  colProv       = 9;
  colNegara     = 10;
  colPost       = 11;
  ColNoTelp1    = 12;
  ColNoTelp2    = 13;
  ColNoFax      = 14;
  ColEmail      = 15;
  ColWebsite    = 16;
  ColContact    = 17;
  Colpng_jwb1   = 18;
  Coljabatan1   = 19;
  Colpng_jwb2   = 20;
  Coljabatan2   = 21;
  ColUserId     = 22;
  ColAktif      = 23;
  ColSeq        = 24;

procedure TfrmRekapMasterCard.arrangecolsize;
begin
  asgRekap.AutoNumberCol(colno);
  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
    asgRekap.ColWidths[colaktif] := 0;
  end;
end;

procedure TfrmRekapMasterCard.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  btnEdit.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0) and (asgRekap.cells[colAktif,asgRekap.Row] = '');
  btnAktivasi.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
end;

procedure TfrmRekapMasterCard.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow > 0) then btnEdit.Click;
end;

procedure TfrmRekapMasterCard.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [colNo] then HAlign := taRightJustify;
end;

procedure TfrmRekapMasterCard.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmRekapMasterCard.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmRekapMasterCard.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol = colseq then FloatFormat    := '%.0f'
  end;
end;

procedure TfrmRekapMasterCard.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapMasterCard.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapMasterCard.asgRekapSelectionChanged(Sender: TObject;
  ALeft, ATop, ARight, ABottom: Integer);
begin
  asgRekap.OnClickCell(Self,asgRekap.Row,asgRekap.Col);
end;

procedure TfrmRekapMasterCard.btnAktivasiClick(Sender: TObject);
var Tanggal : tdate;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Cells[ColAktif, asgRekap.Row]= '' then  begin
    Tanggal := 0;
  end else
    Tanggal := VarToDateTime(asgRekap.Cells[ColAktif, asgRekap.Row]);
  if Confirmed(MSG_CONFIRMED_AKTIVASI) then begin
    UEngine.Aktivasi_Master(asgRekap.Ints[ColSeq, asgRekap.Row], Tanggal, 'master_Card', true, 'tgl_hapus');
    loaddata;
  end;
end;

procedure TfrmRekapMasterCard.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('List Card');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;      
  asgRekap.ColCount := asgRekap.ColCount + 3;
  asgRekap.ColumnSize.Stretch := True;
  arrangecolsize;
end;

procedure TfrmRekapMasterCard.btnEditClick(Sender: TObject);
begin
  if not BisaEdit(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) and (asgRekap.cells[colAktif, asgRekap.Row] = '') then begin
    Application.CreateForm(TfrmInputMasterCard, frmInputMasterCard);
    frmInputMasterCard.OnClose := frmMainMenu.ChildFormSingle;
    if frmInputMasterCard.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then LoadEdit(asgRekap.Row, false);
end;
end;

procedure TfrmRekapMasterCard.btnLoadClick(Sender: TObject);
begin
  loaddata;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmRekapMasterCard.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapMasterCard.btnTambahClick(Sender: TObject);
begin
  if not BisaNambah(AmenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  Application.CreateForm(TfrmInputMasterCard, frmInputMasterCard);
  frmInputMasterCard.OnClose := frmMainMenu.ChildFormSingle;
  if (frmInputMasterCard.execute(0,false)) or (frmInputMasterCard.statSimpan = true)  then btnLoad.Click;
end;

procedure TfrmRekapMasterCard.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapMasterCard.Execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmRekapMasterCard.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmRekapMasterCard.initform;
begin
  
  cmbStatus.ItemIndex           := 1;
  cmbTipe.ItemIndex             := 0;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

function TfrmRekapMasterCard.isDelete(alasan: string): boolean;
var seqMst : integer;
    MstTmp   : TR_master_card;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
begin
  myConnection.BeginSQL;
  try
    SeqMst   := asgRekap.Ints[ColSeq, asgRekap.Row];
    MstTmp   := Get_master_card(seqMst);
    Delete_master_card(MstTmp.Seq);

    Ket.IsEdit    := False;
    Ket.NamaTrans := 'List Card';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := MstTmp.nama;
    Ket.NoTrans   := '';
    Ket.TglTrans  := ServerNow;

    Log.Nama_Tabel := 'master_card';
    Log.Tanggal    := ServerNow;
    Log.Seq_Dummy  := 0;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := 0;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := alasan;
    Save_Log_File(Log);

    Result := True;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

procedure TfrmRekapMasterCard.loaddata;
var i,Row       : Integer;
    Astatus     : TBooleanOperator;
    Filter, Atipe, ADes : string;
    ArData      : AR_master_card;
begin
  asgRekap.RowCount := 2;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  AStatus := boNone;
  case cmbStatus.ItemIndex of
    1 : Filter := ' and tgl_hapus is null ';
    2 : Filter := ' and tgl_hapus is not null ';
  end;
  Atipe := '';
  case cmbTipe.ItemIndex of
    1 : Atipe := TIPE_CUSTOMER;
    2 : Atipe := TIPE_VENDOR;
  end;

  Ades := '';
  case cmbDes.ItemIndex of
    1 : ADes := TIPE_COMPANY;
    2 : ADes := TIPE_INDIE;
  end;  

  filter := filter + ifthen(Atipe<>'',' and tipe = '+FormatSQLString(Atipe));
  filter := filter + ifthen(ADes<>'',' and jenis = '+FormatSQLString(ADes));

  ArData := Get_Arr_master_card('',Filter);
  for i := 0 to Length(ArData) - 1 do begin
    AdvProgressBar1.Position := round((i+1)/Length(ArData)*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -1;
    Asgrekap.cells[colTypeCard ,Row] := ifthen(arData[i].tipe=TIPE_CUSTOMER,TIPE_CUSTOMER_TEXT,
      TIPE_VENDOR_TEXT);
    Asgrekap.cells[colDes      ,Row] := ifthen(arData[i].jenis=TIPE_COMPANY,TIPE_COMPANY_TEXT,
      TIPE_indie_TEXT);
    Asgrekap.cells[colKode     ,Row] := arData[i].kode;
    Asgrekap.cells[colNama     ,Row] := arData[i].nama;
    Asgrekap.cells[colAlamat   ,Row] := arData[i].alamat;
    Asgrekap.cells[colKota     ,Row] := arData[i].kota;
    Asgrekap.cells[colProv     ,Row] := arData[i].provinsi;
    Asgrekap.cells[colNegara  ,Row] := arData[i].negara;
    Asgrekap.cells[colPost     ,Row] := arData[i].kodepos;
    Asgrekap.cells[colNoTelp1  ,Row] := arData[i].no_telp1;
    Asgrekap.cells[colNoTelp2  ,Row] := arData[i].no_telp2;
    Asgrekap.cells[colNoFax    ,Row] := arData[i].no_fax;
    Asgrekap.cells[colEmail    ,Row] := arData[i].email;
    Asgrekap.cells[colWebsite  ,Row] := arData[i].website;
    Asgrekap.cells[colContact  ,Row] := arData[i].kontak;
    Asgrekap.cells[colUserId   ,Row] := arData[i].user_id;

    if arData[i].tgl_hapus <> 0 then      
      Asgrekap.Dates[colAktif    ,Row] := arData[i].tgl_hapus;
    Asgrekap.ints[colSeq       ,Row] := arData[i].seq;
    Asgrekap.cells[colKodeNmr  ,Row] := arData[i].kode_nomor;
    if arData[i].unit_bisnis = TIPE_DIVISI_LOGISTIC then
      Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_LOGISTIC_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_TRADING then
      Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_TRADING_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
      Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_CONSTRUCTION_TEXT;

    Asgrekap.cells[Colpng_jwb1 ,Row] := arData[i].png_jwb1;
    Asgrekap.cells[Coljabatan1 ,Row] := arData[i].jabatan1;
    Asgrekap.cells[Colpng_jwb2 ,Row] := arData[i].png_jwb2;
    Asgrekap.cells[Coljabatan2 ,Row] := arData[i].jabatan2;
  end;
  AdvProgressBar1.Hide;
  arrangecolsize;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapMasterCard.loadedit(Arow: integer;
  isTambahBaru: boolean);
var Row, seqMst : Integer;
    dtMaster : TR_master_Card;
begin
  if isTambahBaru = false then begin
    dtMaster := Get_master_Card(asgRekap.Ints[colseq,arow]);
    row := arow
  end else begin
    seqMst := GetCurrentSeq('seq','Master_Card','');
    dtMaster := Get_master_Card(seqMst);
    if (asgRekap.RowCount > 2) or ((asgRekap.RowCount = 2) and (asgRekap.cells[colSeq,1] <> '')) then asgRekap.AddRow;
    row := asgRekap.RowCount-1;
  end;

  Asgrekap.cells[colTypeCard ,Row] := ifthen(DtMaster.tipe=TIPE_CUSTOMER,TIPE_CUSTOMER_TEXT,
    TIPE_VENDOR_TEXT);
  Asgrekap.cells[colDes      ,Row] := ifthen(DtMaster.jenis=TIPE_COMPANY,TIPE_COMPANY_TEXT,
    TIPE_indie_TEXT);
  Asgrekap.cells[colKode     ,Row] := DtMaster.kode;
  Asgrekap.cells[colNama     ,Row] := DtMaster.nama;
  Asgrekap.cells[colAlamat   ,Row] := DtMaster.alamat;
  Asgrekap.cells[colKota     ,Row] := DtMaster.kota;
  Asgrekap.cells[colProv     ,Row] := DtMaster.provinsi;
  Asgrekap.cells[colNegara  ,Row] := DtMaster.negara;
  Asgrekap.cells[colPost     ,Row] := DtMaster.kodepos;
  Asgrekap.cells[colNoTelp1  ,Row] := DtMaster.no_telp1;
  Asgrekap.cells[colNoTelp2  ,Row] := DtMaster.no_telp2;
  Asgrekap.cells[colNoFax    ,Row] := DtMaster.no_fax;
  Asgrekap.cells[colEmail    ,Row] := DtMaster.email;
  Asgrekap.cells[colWebsite  ,Row] := DtMaster.website;
  Asgrekap.cells[colContact  ,Row] := DtMaster.kontak;
  Asgrekap.cells[colUserId   ,Row] := DtMaster.user_id;
  if DtMaster.tgl_hapus <> 0 then
    Asgrekap.Dates[colAktif    ,Row] := DtMaster.tgl_hapus;
  Asgrekap.ints[colSeq       ,Row] := DtMaster.seq;
  Asgrekap.cells[colKodeNmr  ,Row] := DtMaster.kode_nomor;
  if DtMaster.unit_bisnis = TIPE_DIVISI_LOGISTIC then
    Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_LOGISTIC_TEXT
  else if DtMaster.unit_bisnis = TIPE_DIVISI_TRADING then
    Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_TRADING_TEXT
  else if DtMaster.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
    Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_CONSTRUCTION_TEXT;

  Asgrekap.cells[Colpng_jwb1 ,Row] := DtMaster.png_jwb1;
  Asgrekap.cells[Coljabatan1 ,Row] := DtMaster.jabatan1;
  Asgrekap.cells[Colpng_jwb2 ,Row] := DtMaster.png_jwb2;
  Asgrekap.cells[Coljabatan2 ,Row] := DtMaster.jabatan2;

  ArrangeColSize;
end;

procedure TfrmRekapMasterCard.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 26;
  asgRekap.RowCount := 2;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;
  arrangecolsize;
end;

procedure TfrmRekapMasterCard.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapMasterCard.btnHapusClick(Sender: TObject);
var Tanggal : tdate;
    alasanhapus : string;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.Row] <> 0 then begin
    if Get_Count('card_seq', 'data_jual_master', inttostr(asgRekap.Ints[ColSeq,asgRekap.Row]), '', 'string') > 0 then begin
      Inform(asgRekap.Cells[Colnama, asgRekap.Row]+' tidak bisa dihapus, ' +
          'sudah ada Data Jual. ');
      exit;
    end;

    if Confirmed(asgRekap.Cells[ColNama, asgRekap.Row]+' akan dihapus ?') then begin
      alasanhapus := TrimAllString(UGeneral.InputNote);
      if alasanhapus = '' then begin
        Inform('Catatan hapus tidak boleh kosong.');
        Exit;
      end;
      if isDelete(alasanhapus) then begin
        inform(MSG_SUCCESS_DELETING);
        if AsgRekap.RowCount > 2  then AsgRekap.RemoveRows(AsgRekap.Row,1)
        else AsgRekap.ClearRows(AsgRekap.Row,1);
        Arrangecolsize;
        AsgRekap.Col := ColNama;
        AsgRekap.Row := 1;
      end else Inform(MSG_UNSUCCESS_DELETING);
    end;
  end;
end;

end.
