unit LOV;

interface

uses
  MainMenu,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvEdit,
  ExtCtrls,OracleConnection;

type
  TfrmLOV = class(TForm)
    mainpanel: TPanel;
    Label1: TLabel;
    txtcari: TAdvEdit;
    btnpilih: TAdvGlowButton;
    btnbatal: TAdvGlowButton;
    chbsearch: TCheckBox;
    asgLov: TAdvStringGrid;
    procedure btnpilihClick(Sender: TObject);
    procedure chbsearchClick(Sender: TObject);
    procedure btnbatalClick(Sender: TObject);
    procedure AsgLovDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure AsgLovGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure AsgLovKeyPress(Sender: TObject; var Key: Char);
    procedure AsgLovMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure AsgLovMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure AsgLovCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure AsgLovCheckBoxChange(Sender: TObject; ACol, ARow: Integer;
      State: Boolean);
    procedure txtcariKeyPress(Sender: TObject; var Key: Char);
    procedure cmbTipeArmadaSelect(Sender: TObject);
    procedure txtcariKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    Typemaster:string;
    status ,istglKeluar, IsAsset: TBooleanOperator;
    tipe, level,tipeMst, Addpar : string;
    IsChecked, isDariDepan : Boolean;
    
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure LoaddataCard;
    procedure LoaddataJob;          
    procedure LoaddataBank;
  public
    { Public declarations }
    function ExecuteCard(cari:string; state : TBooleanOperator = botrue; addParameter : string =
      ''):integer;
    function ExecuteJob(cari:string; state : TBooleanOperator = botrue; addParameter : string =
      ''; adaDataJual : TBooleanOperator = bonone):integer;
    function ExecuteBank(cari:string; state : TBooleanOperator = botrue; addParameter : string =
      ''):integer;
  end;

var
  frmLOV: TfrmLOV;

implementation

uses
  UEngine, USystemMenu, URecord, UConst, UGeneral, ADOInt, StrUtils,
  UCreateForm, UDummy, UTransaksi, UFinance, UReport;

{$R *.dfm}

const
  colno   = 0;
  colcheck= 0;
  colkode = 1;
  colnama = 2;
  Colseq  = 3;
  colTipe = 4;

{ TfrmLOV }

procedure TfrmLOV.arrangecolsize;
begin
  if IsChecked = false then asgLov.AutoNumberCol(ColNo);
  asgLov.AutoSizeColumns(True, 2);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
   asgLov.ColWidths[colseq]  := 0;
   asgLov.ColWidths[colTipe] := 0;
  end;
end;

procedure TfrmLOV.AsgLovCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  canedit := (ACol in [ColCheck]) and (IsChecked = True)
end;

procedure TfrmLOV.AsgLovCheckBoxChange(Sender: TObject; ACol, ARow: Integer;
  State: Boolean);
  var i : integer;
begin
   if arow = 0 then begin
    asglov.GetCheckBoxState(ColCheck, arow, state);
    if State then begin
      for i := 1 to asglov.RowCount-1  do begin
        asglov.SetCheckBoxState(ColCheck, i, true);
      end;
    end else begin
      for i := 1 to asglov.RowCount-1  do begin
        asglov.SetCheckBoxState(ColCheck, i, false);
      end;
    end;
  end;
end;

procedure TfrmLOV.AsgLovDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if IsChecked = false then if arow > 0 then ModalResult:=mrOk;
end;

procedure TfrmLOV.AsgLovGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ischecked = false then begin
    if (ARow = 0) then HAlign := taCenter
    else if acol in [colNo,colseq] then  HAlign := taRightJustify;
  end else begin
    if (ARow = 0) then HAlign := taCenter
    else if acol in [colcheck] then halign := taCenter
    else if acol in [colseq] then  HAlign := taRightJustify;
  end;
  VAlign := vtaCenter;
end;

procedure TfrmLOV.AsgLovKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    if asgLov.Ints[Colseq, asgLov.Row] <> 0 then btnpilih.Click; 
  end;
end;

procedure TfrmLOV.AsgLovMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  arrangecolsize
end;

procedure TfrmLOV.AsgLovMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  arrangecolsize
end;

procedure TfrmLOV.btnbatalClick(Sender: TObject);
begin
  ModalResult := MRCancel;
end;

procedure TfrmLOV.btnpilihClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmLOV.chbsearchClick(Sender: TObject);
begin
  asgLov.searchfooter.Visible:=chbsearch.Checked;
end;

procedure TfrmLOV.cmbTipeArmadaSelect(Sender: TObject);
begin
  if Typemaster = MASTER_CARD then LoaddataCard
  else if Typemaster = MASTER_CARD then LoaddataJob;
end;

function TfrmLOV.ExecuteBank(cari: string; state: TBooleanOperator;
  addParameter: string): integer;
begin
  InitForm;
  txtCari.Text := Cari;
  typemaster   := MASTER_BANK;
  asgLov.Cells[colno,0] :='No.';
  asgLov.Cells[colkode,0]:= 'Bank';
  asgLov.Cells[colnama,0]:= 'Cabang - No Rek - A/N';
  SetGrid;
  arrangecolsize;
  status       := state;
  IsChecked := False;
  addPar := addParameter;
  LoaddataBANK;
  Self.Caption := 'List Bank';
  Run(Self);
  if ModalResult = mrOk then Result := asgLov.Ints[ColSeq, asgLov.Row]
  else Result  := 0;

end;

function TfrmLOV.ExecuteCard(cari:string; state : TBooleanOperator = botrue; addParameter : string =
  ''):integer;
begin
  InitForm;
  txtCari.Text := Cari;
  typemaster   := MASTER_CARD;
  asgLov.Cells[colno,0] :='No.';
  SetGrid;
  arrangecolsize;
  status       := state;
  IsChecked := False;
  addPar := addParameter;
  LoaddataCard;
  Self.Caption := 'List Card';
  Run(Self);
  if ModalResult = mrOk then Result := asgLov.Ints[ColSeq, asgLov.Row]
  else Result  := 0;
end;


function TfrmLOV.ExecuteJob(cari:string; state : TBooleanOperator = botrue; addParameter : string =
      ''; adaDataJual : TBooleanOperator = bonone):integer;
begin
  InitForm;
  txtCari.Text := Cari;
  typemaster   := MASTER_JOB;
  asgLov.Cells[colno,0] :='No.';
  SetGrid;
  arrangecolsize;
  status    := state;
  IsChecked := False;
  if adaDataJual <> bonone then begin
    if adaDataJual = botrue then addPar := ' and seq in (select job_seq from ' +
      'data_jual_master) '
    else addPar := ' and seq not in (select job_seq from ' +
      'data_jual_master) ';
  end;
  addPar := addPar + addParameter;
  LoaddataJob;
  Self.Caption := 'List Job';
  Run(Self);
  if ModalResult = mrOk then Result := asgLov.Ints[ColSeq, asgLov.Row]
  else Result  := 0;
end;

procedure TfrmLOV.initform;
begin
  txtcari.Text := '';
  asgLov.SearchFooter.Visible := false;
  chbsearch.Checked := false;
  status := boTrue;
end;

procedure TfrmLOV.LoaddataBank;
var arData : AR_master_bank;
    i,vrow:Integer;
    filter : string;
begin
  filter := ifthen(status=boTrue, ' and tgl_hapus is null', ifthen(status=boFalse, ' and tgl_hapus is not null'));
  filter := filter + addpar;

  arData := Get_Arr_master_bank(TrimAllString(txtcari.text), filter);
  asgLov.RowCount := 2;
  asgLov.ClearNormalCells;
  
  for i := 0 to Length(arData) -1 do begin
    if i>0 then asgLov.AddRow;
    vrow := asgLov.RowCount-1;
    if IsChecked = True then asgLov.AddCheckBox(ColCheck, vRow, False, False);
    asgLov.Cells[colkode,vrow]:= ardata[i].nama;
    asgLov.Cells[colnama,vrow]:= ardata[i].cabang +' - '+ ardata[i].norek + ' - ' + ardata[i].an;
    asgLov.Ints [colseq, vrow]:= ardata[i].seq;
  end;
  arrangecolsize;       
end;

procedure TfrmLOV.LoaddataCard;
var arData : AR_master_card;
    i,vrow:Integer;
    filter : string;
begin
  filter := ifthen(status=boTrue, ' and tgl_hapus is null', ifthen(status=boFalse, ' and tgl_hapus ' +
    'is not null'));
  filter := filter + addpar;

  arData := Get_Arr_master_card(TrimAllString(txtcari.text), filter);
  asgLov.RowCount := 2;
  asgLov.ClearNormalCells;
  for i := 0 to Length(arData) -1 do begin
    if i>0 then asgLov.AddRow;
    vrow := asgLov.RowCount-1;
    if IsChecked = True then asgLov.AddCheckBox(ColCheck, vRow, False, False);
    asgLov.Cells[colkode,vrow]:= ardata[i].kode;
    asgLov.Cells[colnama,vrow]:= ardata[i].nama;
    asgLov.Ints [colseq, vrow]:= ardata[i].seq;
  end;
  arrangecolsize;
end;

procedure TfrmLOV.LoaddataJob;
var arData : AR_master_job;
    i,vrow:Integer;
    filter : string;
begin
  filter := ifthen(status=boTrue, ' and tgl_hapus is null', ifthen(status=boFalse, ' and tgl_hapus ' +
    'is not null'));
  filter := filter + addpar;

  arData := Get_Arr_master_job(TrimAllString(txtcari.text), 0,filter);
  asgLov.RowCount := 2;
  asgLov.ClearNormalCells;
  for i := 0 to Length(arData) -1 do begin
    if i>0 then asgLov.AddRow;
    vrow := asgLov.RowCount-1;
    if IsChecked = True then asgLov.AddCheckBox(ColCheck, vRow, False, False);
    asgLov.Cells[colkode,vrow]:= ardata[i].kode;
    asgLov.Cells[colnama,vrow]:= ardata[i].nama;
    asgLov.Ints [colseq, vrow]:= ardata[i].seq;
  end;
  arrangecolsize;
end;

procedure TfrmLOV.setgrid;
begin
  asgLov.ClearNormalCells;
  if IsChecked = false then asglov.ColumnHeaders[colno] := 'No.'
  else asglov.AddCheckBox(ColCheck, 0, False, False);
  asgLov.ColCount  := 6;
  asgLov.RowCount  := 2;
  asgLov.FixedCols := 1;
  asgLov.FixedRows := 1;
  arrangecolsize;
end;

procedure TfrmLOV.txtcariKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_UP then begin
    if asgLOV.Row > 1 then asgLOV.Row := asgLOV.Row - 1;
  end;
  if Key = VK_DOWN then begin
    if asgLOV.Row < asgLOV.RowCount - 1 then asgLOV.Row := asgLOV.Row + 1;
  end;
//  if Key = VK_RETURN then begin
//    btnPilih.Click;
//  end;
end;

procedure TfrmLOV.txtcariKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then begin
    asgLov.ScrollBars := ssNone;
    if Typemaster = MASTER_CARD then BEGIN
      LoaddataCard;
    end else if Typemaster = MASTER_JOB then begin
      LoaddataJob;
    end;
    asglov.ScrollBars := ssBoth;
    asgLov.Setfocus;
  end;
end;

end.
