unit MasterPPNInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker;

type
  TfrmInputMasterPPN = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    txtPPN: TAdvEdit;
    Label1: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Label22: TLabel;
    dtpTglBerlaku: TAdvDateTimePicker;
    Label4: TLabel;
    procedure btnBatalClick(Sender: TObject);
    procedure txtPPNKeyPress(Sender: TObject; var Key: Char);
    procedure btnSimpanClick(Sender: TObject);
    procedure mmKetKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    datenow: tdate;
    seqCard , seqmaster : integer;
    EditMode : Boolean;
    function isValid : Boolean;
    function isSaved : Boolean;
    procedure loaddata;
    procedure InitForm;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(seq : integer; statedit : boolean):boolean;
  end;

var
  frmInputMasterPPN: TfrmInputMasterPPN;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi;

{$R *.dfm}

procedure TfrmInputMasterPPN.btnSimpanClick(Sender: TObject);
begin
  if IsValid and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if not EditMode then begin
      if IsSaved then begin
        if Confirmed(MSG_ADD_DATA) then begin
          InitForm; seqmaster := 0; dtpTglBerlaku.SetFocus; statSimpan := True;
        end else ModalResult := mrOk;
      end else begin
        InForm(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
    end else begin
      if IsSaved then begin
        ModalResult := mrOk;
        inform(MSG_SUCCESS_UPDATE);
      end else begin
        InForm(MSG_UNSUCCESS_UPDATE);
        ActiveControl := btnBatal;
      end;
    end;
  end;
end;

function TfrmInputMasterPPN.execute(seq: integer;
  statedit: boolean): boolean;
begin
  datenow:=ServerNow;
  InitForm;
  seqmaster := seq;
  EditMode := seqmaster <> 0;
  if Editmode then begin
    self.Caption := 'Edit PPN';
    loaddata;
  end else self.Caption := 'Tambah PPN';
  Run(Self, True);
  Result := ModalResult = mrOk;
end;

procedure TfrmInputMasterPPN.InitForm;
begin
  txtSetAllEmpty(self);
  seqMaster := 0;
end;

function TfrmInputMasterPPN.isSaved: Boolean;
var dataMaster : TR_master_PPN;
begin
  try
    myConnection.BeginSQL;
    dataMaster.tgl_berlaku := dtpTglBerlaku.date;
    dataMaster.ppn         := txtPPN.floatvalue;

    if EditMode then begin
      DataMaster.seq := seqmaster;
      Update_master_PPN(DataMaster);
    end else begin
      seqmaster := Save_master_PPN(DataMaster);
    end;

    //Get_List_master_PPN(ListPPNGlobal, seqmaster);

    myConnection.EndSQL;
    Result := true;
  Except
    myConnection.UndoSQL;
    inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

function TfrmInputMasterPPN.isValid: Boolean;
begin
  Result := True;

  if TrimAllString(txtPPN.Text) = '' then begin
    inform('Nama PPN harus diisi.');
    txtPPN.SetFocus;
    Result := false;
    Exit;
  end;

end;

procedure TfrmInputMasterPPN.loaddata;
var datamaster : TR_master_PPN;
begin
  DataMaster          := Get_master_PPN(seqmaster);

  txtPPN.Text           := FloatToStrFmt( dataMaster.ppn );
  dtpTglBerlaku.Date    := dataMaster.tgl_berlaku;

end;

procedure TfrmInputMasterPPN.mmKetKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnSimpan.SetFocus;
end;

procedure TfrmInputMasterPPN.txtPPNKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then txtPPN.SetFocus;
end;

procedure TfrmInputMasterPPN.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
