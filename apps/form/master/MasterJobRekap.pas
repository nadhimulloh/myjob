unit MasterJobRekap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls;

type
  TfrmRekapMasterJob = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnAktivasi: TAdvGlowButton;
    btnTambah: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    btnHapus: TAdvGlowButton;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label19: TLabel;
    cmbStatus: TAdvComboBox;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapSelectionChanged(Sender: TObject; ALeft, ATop, ARight,
      ABottom: Integer);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnHapusClick(Sender: TObject);
  private
    seqCard,AMenuid : integer;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddatapua;
    procedure loadedit(Arow : integer =0; isTambahBaru : boolean = false);
    function isDelete(alasan: string): boolean;
    procedure loaddata;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmRekapMasterJob: TfrmRekapMasterJob;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, MasterJobInput, StrUtils, LOV;

{$R *.dfm}

const
  ColNo           = 0;
  ColBUnit        = 1;
  ColName         = 2;
  colCust         = 3;
  ColJobNumber    = 4;
  colNOpo         = 5;
  ColDescription  = 6;
  ColContact      = 7;
  ColStartDate    = 8;
  ColTarget       = 9;
  colLokasi       = 10;
  ColUserId       = 11;
  ColAktif        = 12;
  ColSeq          = 13;



procedure TfrmRekapMasterJob.arrangecolsize;
begin
  asgRekap.AutoNumberCol(colno);
  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
    asgRekap.ColWidths[colaktif] := 0;
  end;
end;

procedure TfrmRekapMasterJob.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  btnEdit.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0) and (asgRekap.cells[colAktif,asgRekap.Row] = '');
  btnAktivasi.enabled := (asgRekap.Ints[colSeq,asgRekap.Row] <> 0);
end;

procedure TfrmRekapMasterJob.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow > 0) then btnEdit.Click;
end;

procedure TfrmRekapMasterJob.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [colNo] then HAlign := taRightJustify;
end;

procedure TfrmRekapMasterJob.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmRekapMasterJob.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmRekapMasterJob.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol = colseq then FloatFormat    := '%.0f'
  end;
end;

procedure TfrmRekapMasterJob.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapMasterJob.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmRekapMasterJob.asgRekapSelectionChanged(Sender: TObject;
  ALeft, ATop, ARight, ABottom: Integer);
begin
  asgRekap.OnClickCell(Self,asgRekap.Row,asgRekap.Col);
end;

procedure TfrmRekapMasterJob.btnAktivasiClick(Sender: TObject);
var Tanggal : tdate;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Cells[ColAktif, asgRekap.Row]= '' then  begin
    Tanggal := 0;
  end else
    Tanggal := VarToDateTime(asgRekap.Cells[ColAktif, asgRekap.Row]);
  if Confirmed(MSG_CONFIRMED_AKTIVASI) then begin
    UEngine.Aktivasi_Master(asgRekap.Ints[ColSeq, asgRekap.Row], Tanggal, 'master_Job', true, 'tgl_hapus');
    loaddata;
  end;
end;

procedure TfrmRekapMasterJob.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('List Job');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 3;
  arrangecolsize;
end;

procedure TfrmRekapMasterJob.btnCustClick(Sender: TObject);
begin
  seqCard := 0;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmRekapMasterJob.btnEditClick(Sender: TObject);
begin
  if not BisaEdit(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) and (asgRekap.cells[colAktif, asgRekap.Row] = '') then begin
    Application.CreateForm(TfrmInputMasterJob, frmInputMasterJob);
    frmInputMasterJob.OnClose := frmMainMenu.ChildFormSingle;
    if frmInputMasterJob.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then LoadEdit(asgRekap.Row, false);
  end;
end;

procedure TfrmRekapMasterJob.btnLoadClick(Sender: TObject);
begin
  loaddata;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmRekapMasterJob.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapMasterJob.btnTambahClick(Sender: TObject);
begin
  if not BisaNambah(AmenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  Application.CreateForm(TfrmInputMasterJob, frmInputMasterJob);
  frmInputMasterJob.OnClose := frmMainMenu.ChildFormSingle;
  if (frmInputMasterJob.execute(0,false)) or (frmInputMasterJob.statSimpan = true)  then btnLoad.Click;
end;

procedure TfrmRekapMasterJob.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapMasterJob.Execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmRekapMasterJob.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmRekapMasterJob.initform;
begin
  dtpAwal.Date  := ServerNow();
  dtpAkhir.Date := ServerNow();
  txtCust.Text    := '';
  seqCard         := 0;
  cmbStatus.ItemIndex           := 1;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

function TfrmRekapMasterJob.isDelete(alasan: string): boolean;
var seqMst : integer;
    MstTmp   : tr_master_job;
    Ket : TR_KetEditHapus;
    Log : TR_Log_File;
begin
  myConnection.BeginSQL;
  try
    SeqMst   := asgRekap.Ints[ColSeq, asgRekap.Row];
    MstTmp   := Get_master_job(seqMst);
    Delete_master_job(MstTmp.Seq);

    Ket.IsEdit    := False;
    Ket.NamaTrans := 'List Job';
    Ket.IsMasuk   := BoFalse;
    Ket.NamaVC    := EkstrakString(ListCardGlobal.Values[inttostr(MstTmp.card_seq)], '#', 2);
    Ket.NoTrans   := '';
    Ket.TglTrans  := ServerNow;

    Log.Nama_Tabel := 'master_job';
    Log.Tanggal    := ServerNow;
    Log.Seq_Dummy  := 0;
    Log.User_id    := GlobalSystemUser.UserId;
    Log.Seq_Edit   := 0;
    Log.Keterangan := GetKetEditHapusTrans(Ket);
    Log.Checked    := UConst.STATUS_FALSE;
    log.catatan_hapus := alasan;
    Save_Log_File(Log);

    Result := True;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := False;
  end;
end;

procedure TfrmRekapMasterJob.loaddata;
var i,Row       : Integer;
    Astatus     : TBooleanOperator;
    Filter, Divisi : string;
    ArData      : AR_master_Job;
begin
  asgRekap.RowCount := 2;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  AStatus := boNone;
  case cmbStatus.ItemIndex of
    1 : Filter := ' and tgl_hapus is null ';
    2 : Filter := ' and tgl_hapus is not null ';
  end;
  Divisi := '';
  case cmbDes.ItemIndex of
    1 : Divisi := TIPE_DIVISI_LOGISTIC;
    2 : Divisi := TIPE_DIVISI_TRADING;
    3 : Divisi := TIPE_DIVISI_CONSTRUCTION;
  end;

  filter := filter + ' and tgl_mulai between '+FormatSQLDate(dtpAwal.date)+' and '+FormatSQLDate(dtpAkhir.date);

  filter := filter + ifthen(Divisi<>'',' and unit_bisnis = '+FormatSQLString(Divisi));

  ArData := Get_Arr_master_Job('',seqCard, Filter);
  for i := 0 to Length(ArData) - 1 do begin
    AdvProgressBar1.Position := round((i+1)/Length(ArData)*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -1;
    if arData[i].unit_bisnis = TIPE_DIVISI_LOGISTIC then
      Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_LOGISTIC_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_TRADING then
      Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_TRADING_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
      Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_CONSTRUCTION_TEXT;

    Asgrekap.cells[colName         ,Row] := arData[i].nama;
    Asgrekap.cells[colJobNumber    ,Row] := arData[i].kode;
    Asgrekap.cells[colNOpo    ,     Row] := arData[i].no_po;
    Asgrekap.cells[colDescription  ,Row] := arData[i].deskripsi;
    Asgrekap.cells[colContact      ,Row] := arData[i].contact;
    Asgrekap.Dates[colStartDate    ,Row] := arData[i].tgl_mulai;
    Asgrekap.Dates[colTarget       ,Row] := arData[i].tgl_target;
    Asgrekap.cells[colUserId       ,Row] := arData[i].user_id;
    Asgrekap.cells[colLokasi,       Row] := arData[i].lokasi;
    asgrekap.cells[colcust,         row] :=
    EkstrakString(ListCardGlobal.Values[IntToStr(arData[i].card_seq)],'#',1) + ' - ' +
    EkstrakString(ListCardGlobal.Values[IntToStr(arData[i].card_seq)],'#',2);

    if arData[i].tgl_hapus <> 0 then
      Asgrekap.Dates[colAktif        ,Row] := arData[i].tgl_hapus;
    Asgrekap.ints[colSeq          ,Row] := arData[i].seq;


  end;
  AdvProgressBar1.Hide;
  arrangecolsize;
  asgRekap.OnClickCell(Self,1,1);
end;

procedure TfrmRekapMasterJob.loaddatapua;
begin

end;

procedure TfrmRekapMasterJob.loadedit(Arow: integer;
  isTambahBaru: boolean);
var Row, seqMst : Integer;
    dtMaster : TR_master_Job;
begin
  if isTambahBaru = false then begin
    dtMaster := Get_master_Job(asgRekap.Ints[colseq,arow]);
    row := arow
  end else begin
    seqMst := GetCurrentSeq('seq','Master_Job','');
    dtMaster := Get_master_Job(seqMst);
    if (asgRekap.RowCount > 2) or ((asgRekap.RowCount = 2) and (asgRekap.cells[colSeq,1] <> '')) then asgRekap.AddRow;
    row := asgRekap.RowCount-1;
  end;

  if dtMaster.unit_bisnis = TIPE_DIVISI_LOGISTIC then
    Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_LOGISTIC_TEXT
  else if dtMaster.unit_bisnis = TIPE_DIVISI_TRADING then
    Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_TRADING_TEXT
  else if dtMaster.unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
    Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_CONSTRUCTION_TEXT;

  Asgrekap.cells[colName         ,Row] := dtMaster.nama;
  Asgrekap.cells[colJobNumber    ,Row] := dtMaster.kode;
  Asgrekap.cells[colDescription  ,Row] := dtMaster.deskripsi;
  Asgrekap.cells[colNOpo    ,     Row] := dtMaster.no_po;
  Asgrekap.cells[colContact      ,Row] := dtMaster.contact;
  Asgrekap.Dates[colStartDate    ,Row] := dtMaster.tgl_mulai;
  Asgrekap.Dates[colTarget       ,Row] := dtMaster.tgl_target;
  Asgrekap.cells[colUserId       ,Row] := dtMaster.user_id;
  Asgrekap.cells[colLokasi,       Row] := dtMaster.lokasi;
  asgrekap.cells[colcust,         row] :=
    EkstrakString(ListCardGlobal.Values[IntToStr(dtMaster.card_seq)],'#',1) + ' - ' +
    EkstrakString(ListCardGlobal.Values[IntToStr(dtMaster.card_seq)],'#',2);

  if dtMaster.tgl_hapus <> 0 then
    Asgrekap.Dates[colAktif        ,Row] := dtMaster.tgl_hapus;
  Asgrekap.ints[colSeq          ,Row] := dtMaster.seq;

  ArrangeColSize;
end;

procedure TfrmRekapMasterJob.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 15;
  asgRekap.RowCount := 2;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;
  arrangecolsize;
end;

procedure TfrmRekapMasterJob.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapMasterJob.btnHapusClick(Sender: TObject);
var Tanggal : tdate;
    alasanhapus : string;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.Row] <> 0 then begin
    if Get_Count('job_seq', 'data_jual_master', inttostr(asgRekap.Ints[ColSeq,asgRekap.Row]), '', 'string') > 0 then begin
      Inform(asgRekap.Cells[Colname, asgRekap.Row]+' tidak bisa dihapus, ' +
          'sudah ada Data Jual. ');
      exit;
    end;

    if Confirmed('Pekerjaan "'+asgRekap.Cells[ColName, asgRekap.Row]+'" akan dihapus ?') then begin
      alasanhapus := TrimAllString(UGeneral.InputNote);
      if alasanhapus = '' then begin
        Inform('Catatan hapus tidak boleh kosong.');
        Exit;
      end;
      if isDelete(alasanhapus) then begin
        inform(MSG_SUCCESS_DELETING);
        if AsgRekap.RowCount > 2  then AsgRekap.RemoveRows(AsgRekap.Row,1)
        else AsgRekap.ClearRows(AsgRekap.Row,1);
        Arrangecolsize;
        AsgRekap.Col := ColName;
        AsgRekap.Row := 1;
      end else Inform(MSG_UNSUCCESS_DELETING);
    end;
  end;
end;

end.
