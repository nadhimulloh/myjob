unit LampiranInvoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AdvEdit, AdvGlowButton, AdvOfficeButtons,
  AdvCombo, ComCtrls, AdvDateTimePicker, Grids, AdvObj, BaseGrid, AdvGrid;

type
  TfrmLampiranInvoice = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Label22: TLabel;
    GroupBox2: TGroupBox;
    asgInput: TAdvStringGrid;
    btnEkspor: TAdvGlowButton;
    txtJudul: TAdvEdit;
    Label2: TLabel;
    btnCetak2: TAdvGlowButton;
    procedure btnBatalClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgInputGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgInputMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgInputMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgInputCanAddRow(Sender: TObject; var CanAdd: Boolean);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetak2Click(Sender: TObject);
  private
    { Private declarations }
    seqCard, SeqMst : integer;
    dateNow : TDateTime;
    isDataBeli : BOOLEAN;
    tipe : string;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
    procedure loaddatabeli;
    procedure InitForm;
  public
    { Public declarations }
    statSimpan : boolean;
    function execute(ASeqMst : integer; isbeli: boolean):boolean;
  end;

var
  frmLampiranInvoice: TfrmLampiranInvoice;

implementation

uses
  UGeneral, URecord, OracleConnection, USystemMenu, UEngine, UConst, MainMenu,
  StrUtils, LOV, UTransaksi, UDummy, ADOInt, LampiranPrint, LampiranPrintJual,
  LampiranTruckingPrint, LampiranTruckingPrintJual, LampiranGSPrint, LampiranGSPrintJual;

const
  colNo          = 0;
  colTgl         = 1;
  colCust        = 2;
  colOrig        = 3;
  colClus        = 4;
  colDest        = 5;
  colBPid        = 6;
  colserial      = 7;
  colQty         = 8;
  colunit        = 9;
  colHrgbELI     = 10;
  colTotalbELI   = 11;
  colHrg         = 12;
  colTotal       = 13;
  colketerangan  = 14;
  colSeq         = 15;
  colSeqVendor   = 16;

  colGSUraian      = 1;
  colGSQty         = 2;
  colGSunit        = 3;
  colGSHrgbELI     = 4;
  colGSTotalbELI   = 5;
  colGSHrg         = 6;
  colGSTotal       = 7;
  colGSketerangan  = 8;
  colGSSeq         = 9;   

  TIPE_GS = 'TIPE_GS';
  TIPE_LOGISTICS = 'TIPE_LG';

{$R *.dfm}

procedure TfrmLampiranInvoice.btnSimpanClick(Sender: TObject);
var judul : string;
begin
  if trimallstring(txtJudul.text) = '' then begin
    Inform('Judul harus diisi');
    txtJudul.setfocus;
    Exit;
  end;
  btnSimpan.Enabled := False;
  judul :=  txtJudul.Text;

  if tipe = TIPE_LOGISTICS then begin
    if not isDataBeli then begin
      Application.CreateForm(TfrmLampiranPrintJual, frmLampiranPrintJual);
      frmLampiranPrintJual.OnClose := frmMainMenu.ChildFormSingle;
      frmLampiranPrintJual.Execute(judul,asgInput);
    end else begin
      Application.CreateForm(TfrmLampiranPrint, frmLampiranPrint);
      frmLampiranPrint.OnClose := frmMainMenu.ChildFormSingle;
      frmLampiranPrint.Execute(judul,asgInput);
    end;
  end else begin
    if not isDataBeli then begin
      Application.CreateForm(TfrmLampiranGSPrintJual, frmLampiranGSPrintJual);
      frmlampiranGSPrintJual.OnClose := frmMainMenu.ChildFormSingle;
      frmlampiranGSPrintJual.Execute(judul,asgInput);
    end else begin
      Application.CreateForm(TfrmlampiranGSPrint, frmlampiranGSPrint);
      frmlampiranGSPrint.OnClose := frmMainMenu.ChildFormSingle;
      frmlampiranGSPrint.Execute(judul,asgInput);
    end;
  end;

  txtJudul.text := judul;
//  myConnection.advPrint.Grid:= asgInput;
//  asgInput.PrintSettings.TitleLines.Clear;
//  asgInput.PrintSettings.TitleLines.Add('');
//  asgInput.PrintSettings.TitleLines.Add('');
//  asgInput.PrintSettings.TitleLines.Add('');
//  asgInput.ColumnSize.Stretch := False;
//  asgInput.ColCount := asgInput.ColCount - 2;
//  if not isDataBeli then begin
//    asgInput.HideColumn(colHrgbELI);
//    asgInput.HideColumn(colTotalbELI);
//  end;
//  SetingPrint(asgInput);
//  myConnection.advPrint.Execute;
//  asgInput.ColumnSize.Stretch := True;
//  asgInput.UnHideColumnsAll;
//  asgInput.ColCount := asgInput.ColCount + 2;
//  arrangecolsize;
  btnSimpan.Enabled := True;
end;

function TfrmLampiranInvoice.execute(ASeqMst: integer; isbeli : boolean): boolean;
var arDet : aR_invoice_detail;
    dtMstJual : TR_data_jual_master;
    job : TR_master_job;
    i : integer;
begin
  arDet := Get_Arr_invoice_detail(ASeqMst);
  for i := 0 to Length(arDet)-1 do begin
    dtMstJual := Get_data_jual_master(arDet[i].jual_seq);
    job := Get_master_job(dtMstJual.job_seq);
    break;
  end;
  if job.unit_bisnis = TIPE_DIVISI_LOGISTIC then tipe := TIPE_LOGISTICS
  else begin
    tipe := TIPE_GS;
    btnSimpan.Caption := '&Cetak';
    btnCetak2.Enabled :=  false;
  end;

  dateNow    := ServerNow(True);
  initform;
  SeqMst      := ASeqMst;
  Self.Caption := 'Lampiran Invoice';
  isDataBeli := isBeli;
  setgrid;
  if isbeli then
    loaddatabeli
  else loaddata;
  run(self);
  result := ModalResult = mrOk;
end;

procedure TfrmLampiranInvoice.InitForm;
begin
  txtSetAllEmpty(self);;
  seqCard   := 0;
end;

procedure TfrmLampiranInvoice.loaddata;
var dtMst : TR_invoice_master;
    arDet : ar_invoice_detail;
    dtMstJual : TR_data_jual_master;
    buffer  :  _recordset;
    j, row,z, i : integer;
    Periode : TR_FilterPeriode;
    sql, seqin : string;

begin
  dtMst := get_invoice_master(seqMst);
  seqCard := dtMst.card_seq;

  arDet := Get_Arr_invoice_detail(dtMst.seq);
  for z := 0 to length(arDet)-1 do begin
    seqin := seqin + ifthen(seqin<>'', ',') + inttostr(arDet[z].jual_seq);
  end;
                    // 0       1         2       3            4                 5  6         7
                    //   8      9     10
  sql := ' select mj.tgl_mulai, d.site_name, d.origin , d.clust , d.destination, d.bpid, d.qty, d.satuan, ' +
        'd.harga, d.keterangan, m.seq, d.serial, d.spesifikasi '+
        ' from data_jual_master m, data_jual_detail d, master_job mj '+
        ' where m.seq = d.master_seq and mj.seq = m.job_Seq and m.seq in ('+seqin+') order by m.tanggal, d.site_name ';
  buffer := myConnection.OpenSQL(sql);

  for i := 0 to buffer.RecordCount-1 do begin
    if i > 0 then asgInput.AddRow;
    row := asgInput.RowCount-2;

    if tipe = TIPE_LOGISTICS then begin
      asgInput.Dates[colTgl         ,row] := BufferToDateTime(buffer.Fields[0].Value);
      asgInput.Cells[colCust        ,row] := BufferToString(buffer.Fields[1].Value);;
      asgInput.Cells[colOrig        ,row] := BufferToString(buffer.Fields[2].Value);
      asgInput.Cells[colClus        ,row] := BufferToString(buffer.Fields[3].Value);
      asgInput.Cells[colDest        ,row] := BufferToString(buffer.Fields[4].Value);
      asgInput.Cells[colBPid        ,row] := BufferToString(buffer.Fields[5].Value);
      asgInput.Floats[colQty        ,row] := BufferToFloat(buffer.Fields[6].Value);
      asgInput.Cells[colunit        ,row] := BufferToString(buffer.Fields[7].Value);
      asgInput.floats[colHrg        ,row] := BufferToFloat(buffer.Fields[8].Value);
      asgInput.floats[colTotal      ,row] := asgInput.Floats[colHrg,row] * asgInput.Floats[colqty,row];
      asgInput.Cells[colketerangan  ,row] := BufferToString(buffer.Fields[9].Value);
      asgInput.Cells[colserial      ,row] := BufferToString(buffer.Fields[11].Value);
    end else begin
      asgInput.Cells[colGSUraian      ,row] := BufferToString(buffer.Fields[12].Value);
      asgInput.Floats[colGSQty        ,row] := BufferToFloat(buffer.Fields[6].Value);
      asgInput.Cells[colGSunit        ,row] := BufferToString(buffer.Fields[7].Value);
      asgInput.floats[colGSHrg        ,row] := BufferToFloat(buffer.Fields[8].Value);
      asgInput.floats[colGSTotal      ,row] := asgInput.Floats[colGSHrg,row] * asgInput.Floats[colgsqty,row];
      asgInput.Cells[colGSketerangan  ,row] := BufferToString(buffer.Fields[9].Value);
    end;

    buffer.MoveNext;
  end;
  buffer.Close;
  arrangecolsize;
end;

procedure TfrmLampiranInvoice.loaddatabeli;
var dtMst : TR_invoice_master;
    arDet : ar_invoice_detail;
    dtMstJual : TR_data_jual_master;
    buffer  :  _recordset;
    j, row,z, i : integer;
    Periode : TR_FilterPeriode;
    sql, seqin : string;
    list : Tstringlist;
begin
  list := tstringlist.create;
  dtMst := get_invoice_master(seqMst);
  seqCard := dtMst.card_seq;

  arDet := Get_Arr_invoice_detail(dtMst.seq);
  for z := 0 to length(arDet)-1 do begin
    seqin := seqin + ifthen(seqin<>'', ',') + inttostr(arDet[z].jual_seq);
  end;
                    // 0       1         2       3            4                 5  6         7
                    //   8      9    10
  sql := ' select mj.tgl_mulai, d.site_name, d.origin , d.clust , d.destination, d.bpid, d.qty, d.satuan, '+
         ' d.harga, d.keterangan, m.seq, d.seq , d.serial, d.spesifikasi '+
         ' from data_jual_master m, data_jual_detail d, master_job mj '+
         '  where m.seq = d.master_seq and mj.seq = m.job_Seq and m.seq in ('+seqin+') order by m.tanggal, d.site_name';
  buffer := myConnection.OpenSQL(sql);

  for i := 0 to buffer.RecordCount-1 do begin
    if i > 0 then asgInput.AddRow;
    row := asgInput.RowCount-2;

    if tipe = TIPE_LOGISTICS then begin
      asgInput.Dates[colTgl         ,row] := BufferToDateTime(buffer.Fields[0].Value);
      asgInput.Cells[colCust        ,row] := BufferToString(buffer.Fields[1].Value);
      asgInput.Cells[colOrig        ,row] := BufferToString(buffer.Fields[2].Value);
      asgInput.Cells[colClus        ,row] := BufferToString(buffer.Fields[3].Value);
      asgInput.Cells[colDest        ,row] := BufferToString(buffer.Fields[4].Value);
      asgInput.Cells[colBPid        ,row] := BufferToString(buffer.Fields[5].Value);
      asgInput.Floats[colQty        ,row] := BufferToFloat(buffer.Fields[6].Value);
      asgInput.Cells[colunit        ,row] := BufferToString(buffer.Fields[7].Value);
      asgInput.floats[colHrg        ,row] := BufferToFloat(buffer.Fields[8].Value);
      asgInput.floats[colTotal      ,row] := asgInput.Floats[colHrg,row] * asgInput.Floats[colqty,row];
      asgInput.Cells[colketerangan  ,row] := BufferToString(buffer.Fields[9].Value);
      asgInput.Cells[colSeq,         row] :=  BufferToString(buffer.Fields[10].Value)+'#'+BufferToString(buffer.Fields[11].Value);
      asgInput.Cells[colserial      ,row] := BufferToString(buffer.Fields[12].Value);
    end else begin
      asgInput.Cells[colGSUraian      ,row] := BufferToString(buffer.Fields[13].Value);
      asgInput.Floats[colGSQty        ,row] := BufferToFloat(buffer.Fields[6].Value);
      asgInput.Cells[colGSunit        ,row] := BufferToString(buffer.Fields[7].Value);
      asgInput.floats[colGSHrg        ,row] := BufferToFloat(buffer.Fields[8].Value);
      asgInput.floats[colGSTotal      ,row] := asgInput.Floats[colGSHrg,row] * asgInput.Floats[colGSqty,row];
      asgInput.Cells[colGSketerangan  ,row] := BufferToString(buffer.Fields[9].Value);
      asgInput.Cells[colGSSeq,         row] :=  BufferToString(buffer.Fields[10].Value)+'#'+BufferToString(buffer.Fields[11].Value);
    end;

    buffer.MoveNext;
  end;
  buffer.Close;

  sql := ' select m.jual_seq, d.SEQ_DETAIL_JUAL, d.harga, d.CARD_SEQ '+
        ' from data_beli_master m, data_beli_detail d '+
        '  where m.seq = d.master_seq and m.jual_seq in ('+seqin+')';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount-1 do begin
    list.add(BufferToString(buffer.Fields[0].Value)+'#'+BufferToString(buffer.Fields[1].Value) +'='+BufferToString(buffer.Fields[2].Value)+'#'+BufferToString(buffer.Fields[3].Value));
    buffer.movenext;
  end;
  buffer.close;

  for i := 2 to asgInput.RowCount-2 do begin
    if tipe = TIPE_LOGISTICS then begin
      asgInput.floats[colHrgbELI    ,i] := StrToFloatdef( ekstrakstring( list.Values[asgInput.Cells[colSeq,i]] ,'#',1) ,0);
      asgInput.floats[colTotalbELI  ,i] := asgInput.Floats[colHrgbeli,i] * asgInput.Floats[colqty,i];
      asgInput.ints[colSeqVendor    ,i] := StrTointdef( ekstrakstring( list.Values[asgInput.Cells[colSeq,i]] ,'#',2) ,0);
    end else begin
      asgInput.floats[colgsHrgbELI    ,i] := StrToFloatdef( ekstrakstring( list.Values[asgInput.Cells[colgsSeq,i]] ,'#',1) ,0);
      asgInput.floats[colgsTotalbELI  ,i] := asgInput.Floats[colgsHrgbeli,i] * asgInput.Floats[colgsqty,i];
    end;

  end;

  arrangecolsize;
end;

procedure TfrmLampiranInvoice.arrangecolsize;
begin
  asgInput.AutoNumberCol(colNo);
  if tipe = TIPE_LOGISTICS then begin
    asgInput.Cells[colunit,asgInput.RowCount-1]:='Total';
    asgInput.FloatingFooter.ColumnCalc[colTotal]  :=acSUM;
    asgInput.FloatingFooter.ColumnCalc[colTotalbELI]  :=acSUM;
    asgInput.AutoSizeColumns(true);

    if not isDataBeli then begin
      asgInput.ColWidths[colHrgbELI] := 0;
      asgInput.ColWidths[colTotalbELI] := 0;
    end;
    asgInput.ColWidths[colSeq] := 0;
    asgInput.ColWidths[colSeqVendor] := 0;
  end else begin
    asgInput.Cells[colGSUraian,asgInput.RowCount-1]:='Total';
    asgInput.FloatingFooter.ColumnCalc[colGSTotal]  :=acSUM;
    asgInput.FloatingFooter.ColumnCalc[colGSTotalbELI]  :=acSUM;
    asgInput.AutoSizeColumns(true);

    if not isDataBeli then begin
      asgInput.ColWidths[colGSHrgbELI] := 0;
      asgInput.ColWidths[colGSTotalbELI] := 0;
    end;
    asgInput.ColWidths[colGSSeq] := 0;
  end;
end;

procedure TfrmLampiranInvoice.setgrid;
begin
  asgInput.ClearNormalCells;
  asgInput.RowCount  := 4;
  asgInput.FixedCols := 1;
  asgInput.FixedRows := 2;

  if tipe = TIPE_LOGISTICS then begin
    asgInput.ColCount  := 18;

    asgInput.MergeCells(colNo, 0, 1, 2);
    asgInput.Cells[colNo,0] :=  'No.';

    asgInput.MergeCells(colTgl, 0, 1, 2);
    asgInput.Cells[colTgl,0] :=  'Tanggal Pengiriman';

    asgInput.MergeCells(colCust, 0, 1, 2);
    asgInput.Cells[colCust,0] :=  'Nama Customer';

    asgInput.MergeCells(colOrig, 0, 3, 1);
    asgInput.Cells[colOrig,0] :=  'Rute Pengiriman';
    asgInput.Cells[colOrig,1] :=  'Origin';
    asgInput.Cells[colClus,1] :=  'Cluster';
    asgInput.Cells[colDest,1] :=  'Destination';

    asgInput.MergeCells(colBPid, 0, 1, 2);
    asgInput.Cells[colBPid,0] :=  'BPID';

    asgInput.MergeCells(colserial, 0, 1, 2);
    asgInput.Cells[colserial,0] :=  'Serial Number';

    asgInput.MergeCells(colQty, 0, 1, 2);
    asgInput.Cells[colQty,0] :=  'Qty';

    asgInput.MergeCells(colunit, 0, 1, 2);
    asgInput.Cells[colUnit,0] :=  'Unit';

    asgInput.MergeCells(colHrgBeli, 0, 2, 1);
    asgInput.Cells[colHrgBeli,0] :=  'Harga Beli';
    asgInput.Cells[colHrgBeli,1] :=  'Harga';
    asgInput.Cells[colTotalBeli,1] :=  'Total';

    asgInput.MergeCells(colHrg, 0, 2, 1);
    if isDataBeli then
      asgInput.Cells[colHrg,0]    := 'Harga Jual'
    else asgInput.Cells[colHrg,0] := 'Harga';
    asgInput.Cells[colHrg,1] :=  'Harga';
    asgInput.Cells[colTotal,1] :=  'Total';

    asgInput.MergeCells(colketerangan, 0, 1, 2);
    asgInput.Cells[colketerangan,0] :=  'Keterangan';
  end else begin
    asgInput.ColCount  := 11;

    asgInput.MergeCells(colNo, 0, 1, 2);
    asgInput.Cells[colNo,0] :=  'No.';

    asgInput.MergeCells(colGSUraian, 0, 1, 2);
    asgInput.Cells[colGSUraian,0] :=  'Uraian';

    asgInput.MergeCells(colGSQty, 0, 1, 2);
    asgInput.Cells[colGSQty,0] :=  'Qty';

    asgInput.MergeCells(colGSunit, 0, 1, 2);
    asgInput.Cells[colGSUnit,0] :=  'Unit';

    asgInput.MergeCells(colGSHrgBeli, 0, 2, 1);
    asgInput.Cells[colGSHrgBeli,0] :=  'Harga Beli';
    asgInput.Cells[colGSHrgBeli,1] :=  'Harga';
    asgInput.Cells[colGSTotalBeli,1] :=  'Total';

    asgInput.MergeCells(colGSHrg, 0, 2, 1);
    if isDataBeli then
      asgInput.Cells[colGSHrg,0]    := 'Harga Jual'
    else asgInput.Cells[colGSHrg,0] := 'Harga';
    asgInput.Cells[colGSHrg,1] :=  'Harga';
    asgInput.Cells[colGSTotal,1] :=  'Total';

    asgInput.MergeCells(colGSketerangan, 0, 1, 2);
    asgInput.Cells[colGSketerangan,0] :=  'Keterangan';
  end;

  asgInput.MergeCells(asgInput.ColCount-1, 0, 1, 2);

  arrangecolsize;
end;


procedure TfrmLampiranInvoice.asgInputCanAddRow(Sender: TObject;
  var CanAdd: Boolean);
begin
  CanAdd:= false;//not isAdaDataJual ;//not(BrowseMode xor EditMode);
end;

procedure TfrmLampiranInvoice.asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
  var CanDelete: Boolean);
begin
  CanDelete := false;//not isAdaDataJual ;//not(BrowseMode xor EditMode);
end;

procedure TfrmLampiranInvoice.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
  var state : boolean;
begin
end;

procedure TfrmLampiranInvoice.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (arow <= 1)  then HAlign := taCenter
  else if (Acol in [ColNo, coltotal, colTotalbELI, colhrgbELI, colqty, colHrg]) and (tipe = tipe_logistics) then HAlign :=
    taRightJustify
  else if (Acol in [ColNo, colgstotal, colgsTotalbELI, colgshrgbELI, colgsqty, colgsHrg]) and (tipe = tipe_gs) then HAlign :=
    taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLampiranInvoice.asgInputGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if tipe = tipe_logistics then begin
    isfloat := acol in [ColNo, coltotal, colTotalbELI, colhrgbELI, colqty, colHrg];
    if isfloat then begin
      case ACol of
        colqty,ColNo: FloatFormat := '%.0n';
        colTotalbELI, colhrgbELI, coltotal, colHrg : FloatFormat := '%.2n';
      end;
    end else FloatFormat := '';
  end else begin
    isfloat := acol in [ColNo, colgstotal, colgsqty, colgsTotalbELI, colgshrgbELI, colgsHrg];
    if isfloat then begin
      case ACol of
        colgsqty,ColNo: FloatFormat := '%.0n';
        colgstotalbeli, colgsHrgbeli, colgstotal, colgsHrg : FloatFormat := '%.2n';
      end;
    end else FloatFormat := '';
  end;
end;

procedure TfrmLampiranInvoice.asgInputMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize;
end;

procedure TfrmLampiranInvoice.asgInputMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then arrangecolsize;
end;

procedure TfrmLampiranInvoice.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmLampiranInvoice.btnCetak2Click(Sender: TObject);
var judul : string;
begin
  if trimallstring(txtJudul.text) = '' then begin
    Inform('Judul harus diisi');
    txtJudul.setfocus;
    Exit;
  end;
  btnSimpan.Enabled := False;
  judul :=  txtJudul.Text;

  if not isDataBeli then begin
    Application.CreateForm(TfrmLampiranTruckingPrintJual, frmLampiranTruckingPrintJual);
    frmLampiranTruckingPrintJual.OnClose := frmMainMenu.ChildFormSingle;
    frmLampiranTruckingPrintJual.Execute(judul,asgInput);
  end else begin
    Application.CreateForm(TfrmLampiranTruckingPrint, frmLampiranTruckingPrint);
    frmLampiranTruckingPrint.OnClose := frmMainMenu.ChildFormSingle;
    frmLampiranTruckingPrint.Execute(judul,asgInput);
  end;

  txtJudul.text := judul;
  btnSimpan.Enabled := True;
end;

procedure TfrmLampiranInvoice.btnEksporClick(Sender: TObject);
begin
  asgExportToExcell(asgInput, myConnection.SaveToExcell);
end;

end.
