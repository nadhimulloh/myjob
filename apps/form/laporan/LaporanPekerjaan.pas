unit LaporanPekerjaan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls;

type
  TfrmLaporanPekerjaan = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label19: TLabel;
    cmbStatus: TAdvComboBox;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    chbSF: TAdvOfficeCheckBox;
    btnAtur: TAdvGlowButton;
    dtpAwal: TDateTimePicker;
    Label6: TLabel;
    Label5: TLabel;
    dtpAkhir: TDateTimePicker;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnAturClick(Sender: TObject);
  private
    seqCard,AMenuid : integer;  
    listHideCol : Tstringlist;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmLaporanPekerjaan: TfrmLaporanPekerjaan;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, MasterJobInput, StrUtils, LOV, AturKolom,
  DateUtils;

{$R *.dfm}

const
  ColNo           = 0;
  ColBUnit        = 1;
  ColName         = 2;
  colCust         = 3;
  ColJobNumber    = 4;
  ColDescription  = 5;
  ColContact      = 6;
  ColStartDate    = 7;
  ColTarget       = 8;
  colLokasi       = 9;
  colSubtotal     = 10;
  colPpn          = 11;
  colTotal        = 12;
  colSubtotalJual = 13;
  colPpnJual      = 14;
  colTotalJual    = 15;
  ColUserId       = 16;
  ColAktif        = 17;
  ColSeq          = 18;


procedure TfrmLaporanPekerjaan.arrangecolsize;
var col, i : integer;
begin
  asgRekap.AutoNumberCol(colno);
  asgrekap.cells[colLokasi, asgrekap.rowcount-1]:= 'Total';
  asgrekap.floatingfooter.columncalc[colSubtotal]    :=  acsum;
  asgrekap.floatingfooter.columncalc[colPpn]    :=  acsum;
  asgrekap.floatingfooter.columncalc[colTotal]    :=  acsum;
  asgrekap.floatingfooter.columncalc[colSubtotaljual]    :=  acsum;
  asgrekap.floatingfooter.columncalc[colPpnjual]    :=  acsum;
  asgrekap.floatingfooter.columncalc[colTotaljual]    :=  acsum;

  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
    asgRekap.ColWidths[colaktif] := 0;
  end;
  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.ColWidths[col]:= 0;
    end;
  end;
end;

procedure TfrmLaporanPekerjaan.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow < 2 then  HAlign := taCenter
  else if acol in [colNo, colSubtotal,colSubtotalJual,colPpn,colPpnJual,colTotal,colTotalJual] then HAlign := taRightJustify;
end;

procedure TfrmLaporanPekerjaan.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 1) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmLaporanPekerjaan.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 1) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmLaporanPekerjaan.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, colSeq, colSubtotal,colSubtotalJual,colPpn,colPpnJual,colTotal,colTotalJual] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol = colseq then FloatFormat    := '%.0f'
    else FloatFormat    := '%.2n';
  end;
end;

procedure TfrmLaporanPekerjaan.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanPekerjaan.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanPekerjaan.btnAktivasiClick(Sender: TObject);
var Tanggal : tdate;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Cells[ColAktif, asgRekap.Row]= '' then  begin
    Tanggal := 0;
  end else
    Tanggal := VarToDateTime(asgRekap.Cells[ColAktif, asgRekap.Row]);
  if Confirmed(MSG_CONFIRMED_AKTIVASI) then begin
    UEngine.Aktivasi_Master(asgRekap.Ints[ColSeq, asgRekap.Row], Tanggal, 'master_Job', true, 'tgl_hapus');
    loaddata;
  end;
end;

procedure TfrmLaporanPekerjaan.btnAturClick(Sender: TObject);
begin
  listHideCol.clear;
  arrangecolsize;
  Application.CreateForm(TfrmAturKolom, frmAturKolom);
  frmAturKolom.OnClose := frmMainMenu.ChildFormSingle;
  frmAturKolom.execute(listHideCol, asgRekap);
  arrangecolsize;
end;

procedure TfrmLaporanPekerjaan.btnCetakClick(Sender: TObject);
var col, I : integer;
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('List Job');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.HideColumn(col);
    end;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;            
  asgRekap.UnHideColumnsAll;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 3;
  arrangecolsize;
end;

procedure TfrmLaporanPekerjaan.btnCustClick(Sender: TObject);
begin
  seqCard := 0;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmLaporanPekerjaan.btnLoadClick(Sender: TObject);
begin
  loaddata;
end;

procedure TfrmLaporanPekerjaan.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
end;

procedure TfrmLaporanPekerjaan.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanPekerjaan.Execute(AidMenu: integer);
begin
  listHideCol := Tstringlist.create;
  AMenuId := AIdMenu;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmLaporanPekerjaan.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLaporanPekerjaan.initform;
begin                 
  listHideCol.clear;
  dtpAwal.date:= startofthemonth(ServerNow);
  dtpAkhir.date:= ServerNow;

  txtCust.Text    := '';
  seqCard         := 0;
  cmbStatus.ItemIndex           := 1;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanPekerjaan.loaddata;
var i,Row       : Integer;
    Astatus     : TBooleanOperator;
    Filter, Divisi : string;
    ArData      : AR_master_Job;
    listbeli, listjual    : tstringlist;
    buffer: _Recordset;
begin
  asgRekap.RowCount := 4;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';

  listbeli := TStringList.Create;
  listjual := TStringList.Create;
  sql := ' select job_seq, subtotal, ppn, total from data_jual_master ';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount-1 do begin
    listjual.Add(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value)+'#'+
                 BufferToString(buffer.Fields[2].Value)+'#'+
                 BufferToString(buffer.Fields[3].Value));
    buffer.MoveNext;
  end;
  buffer.Close;

  sql := ' select job_seq, subtotal, ppn, total from data_beli_master ';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount-1 do begin
    listbeli.Add(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value)+'#'+
                 BufferToString(buffer.Fields[2].Value)+'#'+
                 BufferToString(buffer.Fields[3].Value));
    buffer.MoveNext;
  end;
  buffer.Close;


  AStatus := boNone;
  case cmbStatus.ItemIndex of
    1 : Filter := ' and tgl_hapus is null ';
    2 : Filter := ' and tgl_hapus is not null ';
  end;
  Divisi := '';
  case cmbDes.ItemIndex of
    1 : Divisi := TIPE_DIVISI_LOGISTIC;
    2 : Divisi := TIPE_DIVISI_TRADING;
    3 : Divisi := TIPE_DIVISI_CONSTRUCTION;
  end;

  filter := filter + ifthen(Divisi<>'',' and unit_bisnis = '+FormatSQLString(Divisi));
  filter := Filter + ' and tgl_mulai between '+ FormatSQLDate(dtpAwal.Date)+' and '+FormatSQLDate(dtpAkhir.Date);

  ArData := Get_Arr_master_Job('',seqCard, Filter);
  for i := 0 to Length(ArData) - 1 do begin
    AdvProgressBar1.Position := round((i+1)/Length(ArData)*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -2;
    if arData[i].unit_bisnis = TIPE_DIVISI_LOGISTIC then
      Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_LOGISTIC_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_TRADING then
      Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_TRADING_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
      Asgrekap.cells[colBUnit      ,Row] := TIPE_DIVISI_CONSTRUCTION_TEXT;

    Asgrekap.cells[colName         ,Row] := arData[i].nama;
    Asgrekap.cells[colJobNumber    ,Row] := arData[i].kode;
    Asgrekap.cells[colDescription  ,Row] := arData[i].deskripsi;
    Asgrekap.cells[colContact      ,Row] := arData[i].contact;
    Asgrekap.Dates[colStartDate    ,Row] := arData[i].tgl_mulai;
    Asgrekap.Dates[colTarget       ,Row] := arData[i].tgl_target;
    Asgrekap.cells[colUserId       ,Row] := arData[i].user_id;
    Asgrekap.cells[colLokasi,       Row] := arData[i].lokasi;
    asgrekap.cells[colcust,         row] :=
    EkstrakString(ListCardGlobal.Values[IntToStr(arData[i].card_seq)],'#',1) + ' - ' +
    EkstrakString(ListCardGlobal.Values[IntToStr(arData[i].card_seq)],'#',2);

    Asgrekap.Floats[colSubtotal, Row] := StrToIntDef( EkstrakString(listbeli.Values[IntToStr(arData[i].seq)],'#',1) ,0);
    Asgrekap.Floats[colPpn,      Row] := StrToIntDef( EkstrakString(listbeli.Values[IntToStr(arData[i].seq)],'#',2) ,0);
    Asgrekap.Floats[colTotal,    Row] := StrToIntDef( EkstrakString(listbeli.Values[IntToStr(arData[i].seq)],'#',3) ,0);

    Asgrekap.Floats[colSubtotalJual, Row] := StrToIntDef( EkstrakString(listJual.Values[IntToStr(arData[i].seq)],'#',1) ,0);
    Asgrekap.Floats[colPpnJual,      Row] := StrToIntDef( EkstrakString(listJual.Values[IntToStr(arData[i].seq)],'#',2) ,0);
    Asgrekap.Floats[colTotalJual,    Row] := StrToIntDef( EkstrakString(listJual.Values[IntToStr(arData[i].seq)],'#',3) ,0);

    if arData[i].tgl_hapus <> 0 then
      Asgrekap.Dates[colAktif     ,Row] := arData[i].tgl_hapus;
    Asgrekap.ints[colSeq          ,Row] := arData[i].seq;

  end;
  AdvProgressBar1.Hide;
  arrangecolsize;
end;

procedure TfrmLaporanPekerjaan.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 20;
  asgRekap.RowCount := 4;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 2;

  asgRekap.MergeCells(colNo,0,1,2);
  asgRekap.cells[colNo           , 0] := 'No.' ;

  asgRekap.MergeCells(colBUnit,0,1,2);
  asgRekap.cells[colBUnit        , 0] := 'Unit Bisnis' ;

  asgRekap.MergeCells(colName,0,1,2);
  asgRekap.cells[colName         , 0] := 'Nama Pekerjaan' ;

  asgRekap.MergeCells(colCust,0,1,2);
  asgRekap.cells[colCust         , 0] := 'Customer' ;

  asgRekap.MergeCells(colJobNumber,0,1,2);
  asgRekap.cells[colJobNumber    , 0] := 'Nomor Pekerjaan' ;

  asgRekap.MergeCells(colDescription,0,1,2);
  asgRekap.cells[colDescription  , 0] := 'Deskripsi' ;

  asgRekap.MergeCells(colContact,0,1,2);
  asgRekap.cells[colContact      , 0] := 'Kontak' ;

  asgRekap.MergeCells(colStartDate,0,1,2);
  asgRekap.cells[colStartDate    , 0] := 'Tgl. Mulai' ;

  asgRekap.MergeCells(colTarget,0,1,2);
  asgRekap.cells[colTarget       , 0] := 'Target Tgl. Selesai' ;

  asgRekap.MergeCells(colLokasi,0,1,2);
  asgRekap.cells[colLokasi       , 0] := 'Lokasi' ;
  asgRekap.MergeCells(colSubtotal,0,3,1);
  asgRekap.cells[colSubtotal     , 0] := 'Beli' ;
  asgRekap.cells[colSubtotal     , 1] := 'Subtotal' ;
  asgRekap.cells[colPpn          , 1] := 'PPN' ;
  asgRekap.cells[colTotal        , 1] := 'Total' ;
  asgRekap.MergeCells(colSubtotalJual,0,3,1);
  asgRekap.cells[colSubtotalJual , 0] := 'Jual' ;
  asgRekap.cells[colSubtotalJual , 1] := 'Subtotal' ;
  asgRekap.cells[colPpnJual      , 1] := 'PPN' ;
  asgRekap.cells[colTotalJual    , 1] := 'Total' ;

  asgRekap.MergeCells(colUserId,0,1,2);
  asgRekap.cells[colUserId       , 0] := 'User Id' ;
  asgRekap.MergeCells(colAktif,0,1,2);
  asgRekap.cells[colAktif        , 0] := 'Aktivasi' ;
  asgRekap.MergeCells(colSeq,0,1,2);
  asgRekap.cells[colSeq          , 0] := 'Seq' ;
  asgRekap.MergeCells(asgRekap.colcount-1,0,1,2);
  arrangecolsize;
end;

procedure TfrmLaporanPekerjaan.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

end.
