unit AturKolom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, AdvObj, BaseGrid, AdvGrid, AdvGlowButton;

type
  TfrmAturKolom = class(TForm)
    Panel1: TPanel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    asgInput: TAdvStringGrid;
    procedure asgInputCanAddRow(Sender: TObject; var CanAdd: Boolean);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
    procedure asgInputCanSort(Sender: TObject; ACol: Integer;
      var DoSort: Boolean);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnSimpanClick(Sender: TObject);
  private
    { Private declarations }
    listKolom : TStringList;
    procedure setgrid;
    procedure arrangecolsize;

  public
    { Public declarations }
    function execute(var alist : TStringList; grid: TAdvStringGrid):boolean;
  end;

var
  frmAturKolom: TfrmAturKolom;

implementation

uses
  OracleConnection, MainMenu;

{$R *.dfm}

const
  colNama = 0;
  colChk  = 1;
  
{ TfrmAturKolom }

procedure TfrmAturKolom.arrangecolsize;
begin
   asgInput.AutoSizeColumns(true);
end;

procedure TfrmAturKolom.asgInputCanAddRow(Sender: TObject;
  var CanAdd: Boolean);
begin
   canadd := false;
end;

procedure TfrmAturKolom.asgInputCanDeleteRow(Sender: TObject;
  ARow: Integer; var CanDelete: Boolean);
begin
  CanDelete := False;
end;

procedure TfrmAturKolom.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if (arow > 0) and (ACol=   colChk) then CanEdit := true;
end;

procedure TfrmAturKolom.asgInputCanSort(Sender: TObject; ACol: Integer;
  var DoSort: Boolean);
begin
  DoSort:= false;
end;

procedure TfrmAturKolom.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ACol = colChk) or (ARow = 0) then HAlign:= taCenter;
end;

procedure TfrmAturKolom.asgInputGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin

  case acol of
    colChk: AEditor := edCheckBox;
  end;
end;

procedure TfrmAturKolom.btnSimpanClick(Sender: TObject);
var i :  integer;
    stateStr : string;
    state : boolean;
begin
  listKolom.Clear;
  for i := 1 to asgInput.RowCount-1 do begin
    asgInput.GetCheckBoxState(colchk,i,state);
    if state then statestr := TRUE_STRING
    else statestr := false_STRING;

    listKolom.Add(IntToStr(i)+'='+stateStr);
  end;
  modalresult := mrOk;

end;

function TfrmAturKolom.execute(var alist: TStringList;
  grid: TAdvStringGrid): boolean;
var I, row:integer;
begin
  listKolom := TStringList.Create;
  setgrid;
  for i := 1 to grid.colcount-3 do begin
    if (grid.ColWidths[i] > 0) and (grid.Cells[i, 0]<>'') then begin
      if asgInput.Cells[colNama, 1] <> '' then asgInput.AddRow;
      row := asgInput.RowCount-1;

      asgInput.cells[colNama, row] := grid.Cells[i, 0] ;
      asgInput.AddCheckBox(colChk, row, true, true);

      alist.Add(IntToStr(i)+'='+TRUE_STRING);
    end else if (grid.ColWidths[i] > 0) and (grid.Cells[i, 0]<>'') then begin
      if asgInput.Cells[colNama, 1] <> '' then asgInput.AddRow;
      row := asgInput.RowCount-1;

      asgInput.cells[colNama, row] := grid.Cells[i, 0] ;
      asgInput.AddCheckBox(colChk, row, false, false);

      alist.Add(IntToStr(i)+'='+FALSE_STRING);
    end;
  end;
  arrangecolsize;
  listKolom := alist;

  Run(Self, True);
  if ModalResult = mrOk then begin
    alist := listKolom;
  end;
end;

procedure TfrmAturKolom.setgrid;
begin
  asgInput.ClearNormalCells;
  asgInput.RowCount := 2;
  asgInput.ColCount := 2;
  asgInput.FixedCols := 1;
  asgInput.FixedRows := 1;
end;

end.
