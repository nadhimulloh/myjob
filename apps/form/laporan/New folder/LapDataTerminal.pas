
unit LapDataTerminal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, Gauges,
  AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, StdCtrls, Menus, AdvOfficePager,
  AdvEdit;

type
  TfrmLapDataTerminal = class(TForm)
    MainPanel: TPanel;
    pnlfilter: TPanel;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    Panel2: TPanel;
    Gauge: TGauge;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    btnFilter: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label4: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label1: TLabel;
    btnImport: TAdvGlowButton;
    pmImport: TPopupMenu;
    Import1: TMenuItem;
    Import2: TMenuItem;
    aofTerminal: TAdvOfficePager;
    pgData: TAdvOfficePage;
    pgLaporan: TAdvOfficePage;
    asgRekap: TAdvStringGrid;
    asgLap: TAdvStringGrid;
    pgRit: TAdvOfficePage;
    asgRit: TAdvStringGrid;
    AdvPanel1: TAdvPanel;
    txtArmada: TAdvEdit;
    btnlovarmada: TAdvGlowButton;
    pgUnit: TAdvOfficePage;
    asgUnit: TAdvStringGrid;
    btnUpd: TAdvGlowButton;
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapContractNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgRekapExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chbSFClick(Sender: TObject);
    procedure Import1Click(Sender: TObject);
    procedure Import2Click(Sender: TObject);
    procedure asgLapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgLapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgLapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure aofTerminalChange(Sender: TObject);
    procedure asgRitGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRitMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRitMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRitClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRitContractNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgRitExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure btnlovarmadaClick(Sender: TObject);
    procedure asgUnitClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgUnitExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgUnitContractNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgUnitGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgUnitMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgUnitMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgUnitGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgUnitButtonClick(Sender: TObject; ACol, ARow: Integer);
    procedure asgRekapButtonClick(Sender: TObject; ACol, ARow: Integer);
    procedure asgRekapCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgRekapGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure asgRekapCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure btnUpdClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
  private
    { Private declarations }
    listkotajurusan : tstringlist;
    AMenuid,SeqArmada : integer;
    procedure InitForm;
    procedure SetGrid(idx : integer);
    procedure ArrangeColSize(idx : integer);
    procedure LoadData;
    procedure loaddataLap;
    procedure loaddataRit;
    procedure loaddataUnit;
    function IsSavedPerbaris(seqmst, row : integer) : Boolean ;
    function Isdelete(seqmst : integer) : Boolean ;
    procedure LoadDataUnitNew;
  public
    { Public declarations }
    procedure execute(AidMenu:integer);
  end;

var
  frmLapDataTerminal: TfrmLapDataTerminal;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, MainMenu, OracleConnection, ImportDataTerminal,
  DateUtils, UConst_Temp, ADOInt, StrUtils, ImportDataTerminalWB, LOV;

const
  colNode     = 0;
  colTgl      = 1;
  colArmada   = 1;
  colNoOrder  = 2;
  colSupir    = 3;
  colKota1    = 4;
  colJamKota1 = 5;
  colJmlKota1 = 6;
  colKota2    = 7;
  colJamKota2 = 8;
  colJmlKota2 = 9;
  colket      = 10;
  coledit     = 11;
  colhapus    = 12;
  colseq      = 13;
  colBisaEdit = 14;

  //laporan
  colno       = 0;
  colJurusan  = 2;
  colTerminal = 3;
  colKotaKe1  = 4;
  colKotaKe2  = 5;

  // asgUnit
  colNobody   = 1;
  colorder    = 2;
  colPengemudi= 3;
  colTempArmada = 4;


{$R *.dfm}

procedure TfrmLapDataTerminal.btnUpdClick(Sender: TObject);
var buffer : _recordset;
    i,seqsumber : integer;
    kotasumber, sql : string;
    dtkotaSumber : TR_master_Kota;
    listkota: TStringList;
begin
  listkota:=TStringList.Create;
  sql := ' select nama, seq from master_kota';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount-1 do begin
    listkota.Add(BufferToString(buffer.Fields[0].Value)+'='+BufferToString(buffer.Fields[1].Value));
    buffer.movenext;
  end;
  buffer.close;
  try
    myConnection.BeginSQL;
    sql := ' select seq, ket1 from master_OPERASIONAL_TERMINAL where Kota_Sumber_Seq is null  ';
    buffer := myConnection.OpenSQL(sql);
    Gauge.Show; Gauge.Progress := 0;
    for i := 0 to buffer.RecordCount-1 do begin
      Gauge.Progress := round((i+1)/buffer.RecordCount*100);
      kotasumber := EkstrakString(BufferToString(buffer.Fields[1].Value),' ',4);
      if kotasumber = '' then kotasumber := EkstrakString(BufferToString(buffer.Fields[1].Value),' ',3);

      seqsumber := strtointdef(listkota.values[kotasumber],0);
      if seqsumber <> 0 then begin
        sql := ' update master_OPERASIONAL_TERMINAL set Kota_Sumber_Seq = '+FormatSQLNumber(seqsumber)+
               ' where seq = '+FormatSQLNumber(BufferToInteger(buffer.Fields[0].Value));
        ExecTransaction3(sql);
      end;
      buffer.movenext;
    end;
    buffer.close;
    Gauge.Progress := 0; Gauge.Hide;
    inform('Update data berhasil.');
    myConnection.EndSQL;
  except
    inform('Update data gagal.');
    Gauge.Progress := 0; Gauge.Hide;
    myConnection.UndoSQL;
  end;
end;

procedure TfrmLapDataTerminal.aofTerminalChange(Sender: TObject);
begin
  chbSF.Checked := False;
  asgRekap.SearchFooter.Visible :=  false;
  asgLap.SearchFooter.Visible :=  false;
end;

procedure TfrmLapDataTerminal.ArrangeColSize(idx : integer);
var i : integer;
begin
  if (idx = 0) or (idx = 1) then begin
    asgRekap.AutoSizeColumns(true);
    if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
      asgRekap.ColWidths[colBisaEdit] := 0;
      asgRekap.ColWidths[colseq]      := 0;
    end;
  end;
  if (idx = 0) or (idx = 2) then begin
//    asgLap.AutoNumberCol(colno);
    asgLap.AutoSizeColumns(true);
  end;
  if (idx = 0) or (idx = 3) then begin
    asgRit.AutoSizeColumns(true);
    for i := 3 to asgrit.colcount-1 do begin
      if asgRit.Cells[i,0] = '' then
        asgRit.ColWidths[I] := 0;
    end;
  end;
    if (idx = 0) or (idx = 4) then begin
    asgUnit.AutoSizeColumns(true);
    for i := 4 to asgUnit.colcount-1 do begin
      if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
        asgUnit.ColWidths[colTempArmada] := 0;
        if asgUnit.Cells[i,0] = '' then
          asgUnit.ColWidths[I] := 0;
      end;
    end;
  end;
end;

procedure TfrmLapDataTerminal.asgLapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 1 then HAlign := taCenter
  else if acol in [colno]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapDataTerminal.asgLapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(2);
end;

procedure TfrmLapDataTerminal.asgLapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(2);
end;

procedure TfrmLapDataTerminal.asgRekapButtonClick(Sender: TObject; ACol,
  ARow: Integer);
begin
  if ACol = coledit then begin
    if asgRekap.Cells[colBisaEdit,ARow] <> TRUE_STRING then begin
      asgRekap.Cells[colBisaEdit,ARow] := TRUE_STRING;
      asgRekap.SetButtonText(coledit,ARow,'Simpan');
      asgRekap.setfocus;
      asgRekap.col:= colNoOrder;
      asgRekap.row:= arow;
    end else begin
      if asgRekap.Floats[colJamKota1, ARow]= 0 then begin
        inform('Jam Kota 1 pada baris ke '+inttostr(ARow)+' harus diisi.');
        asgRekap.SetFocus;
        asgRekap.Col := colJamKota1;
        asgRekap.Row := ARow;
        exit;
      end;
      if asgRekap.Floats[colJamKota2, ARow]= 0 then begin
        inform('Jam Kota 2 pada baris ke '+inttostr(ARow)+' harus diisi.');
        asgRekap.SetFocus;
        asgRekap.Col := colJamKota2;
        asgRekap.Row := ARow;
        exit;
      end;
      if IsSavedPerbaris(asgRekap.Ints[colseq,arow], arow) then begin
        Inform(MSG_SUCCESS_UPDATE);
        asgRekap.Cells[colBisaEdit,ARow] := '';
        asgRekap.SetButtonText(coledit,ARow,'Edit');
      end else Inform(MSG_UNSUCCESS_UPDATE);
    end;
  end;
  if ACol = colhapus then begin
    if Confirmed(MSG_DELETE_CONFIRMATION) then begin
      if Isdelete(asgRekap.Ints[colseq,arow]) then begin
        Inform(MSG_SUCCESS_DELETING);
        asgRekap.RemoveRows(arow,1);
      end else Inform(MSG_UNSUCCESS_DELETING);
    end;
  end;
end;

procedure TfrmLapDataTerminal.asgRekapCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  CanEdit := false;
  if (Arow > 1) and (asgrekap.cells[colBisaEdit, arow ] = TRUE_STRING) then begin
    if ACol in [colNoOrder,colJmlKota1,colKota2,colJamKota1,colJamKota2,colJmlKota2,colket] then
      CanEdit := true;
  end;
end;

procedure TfrmLapDataTerminal.asgRekapCellValidate(Sender: TObject; ACol,
  ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(1);
end;

procedure TfrmLapDataTerminal.asgRekapContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(1);
end;

procedure TfrmLapDataTerminal.asgRekapExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(1);
end;

procedure TfrmLapDataTerminal.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 3 then HAlign := taCenter
  else if acol in [colJmlKota2, colJmlKota1]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapDataTerminal.asgRekapGetEditorType(Sender: TObject; ACol,
  ARow: Integer; var AEditor: TEditorType);
begin
  case acol of
    colNoOrder: AEditor := edUpperCase;
    colJmlKota1,colJmlKota2: AEditor := edNumeric;
    colJamKota1,colJamKota2: AEditor := edTimeEdit;
    coledit,colhapus : AEditor := edButton;
  end;
end;

procedure TfrmLapDataTerminal.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := false;
  IsFloat := acol in [colJmlKota1,colJmlKota2];
  if IsFloat then begin
    if ACol in [colJmlKota1,colJmlKota2] then FloatFormat := '%.0n'
    else FloatFormat := '%.2n';
  end;
end;

procedure TfrmLapDataTerminal.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapDataTerminal.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapDataTerminal.asgRitClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow < 2) and (ACol = 0) then begin
    asgRit.Cells[0,0] := IfThen(asgRit.Cells[0,0]='[-]','[+]','[-]');
    if asgRit.Cells[0,0] = '[+]' then asgRit.ContractAll
    else asgRit.ExpandAll;
    arrangecolsize(3);
  end;
end;

procedure TfrmLapDataTerminal.asgRitContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(3);
end;

procedure TfrmLapDataTerminal.asgRitExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(3);
end;

procedure TfrmLapDataTerminal.asgRitGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 2 then HAlign := taCenter
//  else if acol in [colno]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapDataTerminal.asgRitMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(3);
end;

procedure TfrmLapDataTerminal.asgRitMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(3);
end;

procedure TfrmLapDataTerminal.asgUnitButtonClick(Sender: TObject; ACol,
  ARow: Integer);
begin
//  if ACol = colSimpan then begin
//    if asgunit.Cells[colBisaEdit,ARow] <> TRUE_STRING then begin
//      asgunit.Cells[colBisaEdit,ARow] := TRUE_STRING;
//      asgunit.SetButtonText(colSimpan,ARow,'Simpan');
//      asgunit.setfocus;
//      asgunit.col:= coljumlah;
//      asgunit.row:= arow;
//    end else begin
//      if asgunit.Floats[colJumlah, ARow]= 0 then begin
//        inform('Jumlah pada baris ke '+inttostr(ARow)+' harus diisi.');
//        asgunit.SetFocus;
//        asgunit.Col := colJumlah;
//        asgunit.Row := ARow;
//        exit;
//      end;
//      if IsSavedPerbaris(seqMst,asgunit.Ints[colSeqDet,arow], arow) then begin
//        Inform(MSG_SUCCESS_UPDATE);
//        asgunit.Cells[colBisaEdit,ARow] := '';
//        asgunit.SetButtonText(colSimpan,ARow,'Edit');
//      end else Inform(MSG_UNSUCCESS_UPDATE);
//    end;
//  end;
end;

procedure TfrmLapDataTerminal.asgUnitClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow < 2) and (ACol = 0) then begin
    asgUnit.Cells[0,0] := IfThen(asgUnit.Cells[0,0]='[-]','[+]','[-]');
    if asgUnit.Cells[0,0] = '[+]' then asgUnit.ContractAll
    else asgUnit.ExpandAll;
    arrangecolsize(4);
  end;
end;

procedure TfrmLapDataTerminal.asgUnitContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(4);
end;

procedure TfrmLapDataTerminal.asgUnitExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(4);
end;

procedure TfrmLapDataTerminal.asgUnitGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 2 then HAlign := taCenter
  else if (asgUnit.colcount <> 6) and (acol = asgUnit.colcount-2)then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapDataTerminal.asgUnitGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := false;
  IsFloat := (asgUnit.colcount <> 6) and (acol = asgUnit.colcount-2);
  if IsFloat then begin
    if ACol in [asgUnit.colcount-2] then FloatFormat := '%.2n'
    else FloatFormat := '%.0n';
  end;
end;

procedure TfrmLapDataTerminal.asgUnitMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(4);
end;

procedure TfrmLapDataTerminal.asgUnitMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(4);
end;

procedure TfrmLapDataTerminal.btnCetakClick(Sender: TObject);
var i : integer; //gunawan +141019
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if aofTerminal.ActivePage = pgData then begin
    SetingPrint(asgRekap);
    myConnection.advPrint.Grid := asgRekap;
    asgRekap.PrintSettings.TitleLines.Clear;
    asgRekap.PrintSettings.TitleLines.Add('');
    asgRekap.PrintSettings.TitleLines.Add('Laporan Data Terminal');
    asgRekap.PrintSettings.TitleLines.Add('');
    asgRekap.ColumnSize.Stretch := false ;
    asgRekap.ColCount := asgRekap.ColCount-1;
    SetingPrint(asgRekap);
    myConnection.advPrint.Execute ;
    asgRekap.ColCount := asgRekap.ColCount+1;
    asgRekap.ColumnSize.Stretch := true;
    ArrangeColSize(1);
  end else if aofTerminal.ActivePage = pgLaporan then begin
    SetingPrint(asgLap);
    myConnection.advPrint.Grid := asgLap;
    asgLap.PrintSettings.TitleLines.Clear;
    asgLap.PrintSettings.TitleLines.Add('');
    asgLap.PrintSettings.TitleLines.Add('Laporan Data Terminal');
    asgLap.PrintSettings.TitleLines.Add('');
    asgLap.ColumnSize.Stretch := false ;
    asgLap.ColCount := asgLap.ColCount-1;
    SetingPrint(asgLap);
    myConnection.advPrint.Execute ;
    asgLap.ColCount := asgLap.ColCount+1;
    asgLap.ColumnSize.Stretch := true;
    ArrangeColSize(2);
  end else if aofTerminal.ActivePage = pgUnit then begin
    SetingPrint(asgUnit);
    myConnection.advPrint.Grid := asgUnit;
    asgUnit.PrintSettings.TitleLines.Clear;
    asgUnit.PrintSettings.TitleLines.Add('');
    asgUnit.PrintSettings.TitleLines.Add('Laporan Data Terminal Per Unit');
    asgUnit.PrintSettings.TitleLines.Add('');
    asgUnit.ColumnSize.Stretch := false ;
    asgUnit.ColCount := asgUnit.ColCount-1; 
    //gunawan +141019
    for i := 0 to asgUnit.ColCount-1 do begin
      if (asgUnit.Cells[i,0] = '') AND (asgUnit.Cells[i,1] = '') then begin
        asgUnit.HideColumn(i);
      end;
    end;
    SetingPrint(asgUnit);
    myConnection.advPrint.Execute ;
    asgUnit.UnHideColumnsAll; //Gunawan +141019
    asgUnit.ColCount := asgUnit.ColCount+1;
    asgUnit.ColumnSize.Stretch := true;
    ArrangeColSize(4);
  end else begin
    SetingPrint(asgRit);
    myConnection.advPrint.Grid := asgRit;
    asgRit.PrintSettings.TitleLines.Clear;
    asgRit.PrintSettings.TitleLines.Add('');
    asgRit.PrintSettings.TitleLines.Add('Laporan Data Terminal');
    asgRit.PrintSettings.TitleLines.Add('');
    asgRit.ColumnSize.Stretch := false ;
    asgRit.ColCount := asgRit.ColCount-1;
    SetingPrint(asgRit);
    myConnection.advPrint.Execute ;
    asgRit.ColCount := asgRit.ColCount+1;
    asgRit.ColumnSize.Stretch := true;
    ArrangeColSize(3);
  end;
end;

procedure TfrmLapDataTerminal.btnEksporClick(Sender: TObject);
var i : integer; //gunawan +141019
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if aofTerminal.ActivePage = pgData then
    asgExportToExcell(asgRekap, myConnection.SaveToExcell)
  else if aofTerminal.ActivePage = pgRit then
    asgExportToExcell(asgRit, myConnection.SaveToExcell)
  else if aofTerminal.ActivePage = pgUnit then begin
      //gunawan +141019
    for i := 0 to asgUnit.ColCount-1 do begin
      if (asgUnit.Cells[i,0] = '') AND (asgUnit.Cells[i,1] = '') then begin
        asgUnit.HideColumn(i);
      end;
    end;
    asgExportToExcell(asgUnit, myConnection.SaveToExcell);
    asgUnit.UnHideColumnsAll; //Gunawan +141019
    ArrangeColSize(4); 
  end else asgExportToExcell(asgLap, myConnection.SaveToExcell);
end;

procedure TfrmLapDataTerminal.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlfilter, btnFilter, 90);
end;

procedure TfrmLapDataTerminal.btnImportClick(Sender: TObject);
begin
//  Application.CreateForm(TfrmImportDataTerminal, frmImportDataTerminal);
//  frmImportDataTerminal.OnClose := frmMainMenu.ChildFormSingle;
//  if frmImportDataTerminal.execute then btnLoad.Click;
//  Application.CreateForm(TfrmImportDataTerminalWB, frmImportDataTerminalWB);
//    frmImportDataTerminalWB.OnClose := frmMainMenu.ChildFormSingle;
//  if frmImportDataTerminalWB.execute then btnLoad.Click;

  PopupMenu := pmImport;
  pmImport.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  PopupMenu := nil;
  ArrangeColSize(0);

end;

procedure TfrmLapDataTerminal.btnLoadClick(Sender: TObject);
begin
  if aofTerminal.ActivePage = pgData then
    loaddata
  else if aofTerminal.ActivePage = pgRit then
    loaddataRit
  else if aofTerminal.ActivePage = pgUnit then
  //iqbal 050917 ganti loadDataUnit dengan LoadDataUnitNew
//    loaddataUnit
    loaddataUnitNew
  else loaddataLap;
end;

procedure TfrmLapDataTerminal.btnlovarmadaClick(Sender: TObject);
begin
  txtArmada.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada         := frmLov.ExecuteArmada('',boNone);
  if SeqArmada <> 0 then begin
    txtArmada.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',1)+' | '
    +EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapDataTerminal.btnResetClick(Sender: TObject);
begin
  // Oggi 161018 +
  InitForm;
  SetGrid(0);
end;

procedure TfrmLapDataTerminal.chbSFClick(Sender: TObject);
begin
  if aofTerminal.ActivePage = pgData then
    asgRekap.SearchFooter.Visible := chbSF.Checked
  else if aofTerminal.ActivePage = pgRit then
    asgRIT.SearchFooter.Visible := chbSF.Checked
  else if aofTerminal.ActivePage = pgUnit then
    asgunit.SearchFooter.Visible := chbSF.Checked
  else asgLap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLapDataTerminal.execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  listkotajurusan := TStringList.Create;
  if (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
     and (not BisaNambah(AMenuId)) and (not BisaLihatRekap(AMenuId)) and (not BisaLihatLaporan(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  initform;
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then
    btnUpd.visible := False;
  Setgrid(0);
  run(self);
end;

procedure TfrmLapDataTerminal.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapDataTerminal.Import1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportDataTerminal, frmImportDataTerminal);
  frmImportDataTerminal.OnClose := frmMainMenu.ChildFormSingle;
  if frmImportDataTerminal.execute then btnLoad.Click;
end;

procedure TfrmLapDataTerminal.Import2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportDataTerminalWB, frmImportDataTerminalWB);
    frmImportDataTerminalWB.OnClose := frmMainMenu.ChildFormSingle;
  if frmImportDataTerminalWB.execute then btnLoad.Click;
end;

procedure TfrmLapDataTerminal.InitForm;
begin
  dtpAwal.date := StartOfTheMonth(ServerNow);
  dtpAkhir.Date := ServerNow;
  chbSF.Checked := false;
  txtArmada.Clear;
  SeqArmada := 0;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
  asgRIT.SearchFooter.Visible := chbSF.Checked;
  asgunit.SearchFooter.Visible := chbSF.Checked;
  asgLap.SearchFooter.Visible := chbSF.Checked;

end;

function TfrmLapDataTerminal.Isdelete(seqmst: integer): Boolean;
begin
  myConnection.BeginSQL;
  try
    Delete_master_OPERASIONAL_TERMINAL(seqmst);
    Result := True;
    myConnection.EndSQL;
  Except
    Result := False;
    myConnection.UndoSQL;
  end;
end;

function TfrmLapDataTerminal.IsSavedPerbaris(seqmst, row: integer): Boolean;
var dtMst, tempMst : TR_MASTER_OPERASIONAL_TERMINAL;
begin
  dtMst := Get_master_OPERASIONAL_TERMINAL(seqmst);
  tempMst := dtMst;
  dtMst.Noorder       := asgRekap.Cells[colnoorder,row];
  dtMst.Jamkota1      := asgRekap.TIMES[colJamKota1,row];
  dtMst.jmlPenumpang1 := asgRekap.ints[colJmlKota1,row];
  dtMst.Jamkota2      := asgRekap.Times[colJamKota2,row];
  dtMst.jmlPenumpang2 := asgRekap.ints[colJmlKota2,row];
  dtMst.ket2          := asgRekap.cells[colket,row];

  try
    myConnection.BeginSQL;
    Update_master_OPERASIONAL_TERMINAL(dtMst);
    myConnection.EndSQL;
    Result := true;
  except
    myConnection.UndoSQL;
    Result := False;
    Exit;
  end;
end;

procedure TfrmLapDataTerminal.LoadData;
VAR Periode : TR_FilterPeriode;
    i, row, Row2, row3 :integer;
    buffer : _recordset;
    tempTgl : tdate;
    tempket : string;
begin
  Gauge.Show;
  begin periode.FPeriodeAwal      := dtpAwal.Date;
        periode.FPeriodeAkhir     := dtpAkhir.Date;
        periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
        periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
  end;
  asgRekap.ClearNormalCells;
  asgRekap.RowCount := 4 ;
  Gauge.Show;
  asgRekap.Cells[ColNode, 0] := '[-]';
//  0       1       2       3       4        5              6          7
// SEQ, Nobody, Noorder, supir, Jamkota1, jmlPenumpang1, Jamkota2, jmlPenumpang2, '+
//   8     9      10     11     12       13         14        15           16        17
// ket2, ket1, Kota1, Kota2, tanggal, Supir_SEQ, Armada_SEQ, order_SEQ, kota1_SEQ, kota2_SEQ
  Load_master_OPERASIONAL_TERMINAL(buffer, periode,'','', SeqArmada);

  tempTgl := 0; tempket := ''; Row2 := 0; Row := 0;
  for i := 0 to buffer.RecordCount-1 do begin
    Gauge.Progress := round((i+1)/buffer.RecordCount*100);
    if tempTgl <> BufferToDateTime(buffer.Fields[12].Value) then begin
      if asgRekap.cells[coltgl,3] <> '' then asgRekap.AddRow;
      row := asgRekap.RowCount-1;
      asgRekap.Dates[colTgl, row] := BufferToDateTime(buffer.Fields[12].Value);
      asgRekap.MergeCells(colTgl+1,row,11,1);
    end;
    tempTgl :=  BufferToDateTime(buffer.Fields[12].Value);
    if tempKet <> BufferToString(buffer.Fields[9].Value) then begin
      asgRekap.AddRow;
      row2 := asgRekap.RowCount-1;
      asgRekap.Cells[colTgl, row2] := BufferToString(buffer.Fields[9].Value);
      asgRekap.MergeCells(colTgl+1,row2,11,1);
    end;
    tempKet := BufferToString(buffer.Fields[9].Value);
    asgRekap.AddRow;
    row3 := asgRekap.rowcount-1;
    asgRekap.Cells[colArmada  ,row3] := BufferToString(buffer.Fields[1].Value);
    asgRekap.Cells[colNoOrder ,row3] := BufferToString(buffer.Fields[2].Value);
    asgRekap.Cells[colSupir   ,row3] := BufferToString(buffer.Fields[3].Value);
    asgRekap.Cells[colKota1   ,row3] := BufferToString(buffer.Fields[10].Value);
    asgRekap.Cells[colJamKota1,row3] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
    if (asgRekap.Cells[colJamKota1,row3] = '00.00') or (asgRekap.Cells[colJamKota1,row3] = '00:00')  then
      asgRekap.Cells[colJamKota1,row3] := '';
    asgRekap.Cells[colJmlKota1,row3] := BufferToString(buffer.Fields[5].Value);
    asgRekap.Cells[colKota2   ,row3] := BufferToString(buffer.Fields[11].Value);
    asgRekap.Cells[colJamKota2,row3] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
    if (asgRekap.Cells[colJamKota2,row3] = '00.00') or (asgRekap.Cells[colJamKota2,row3] = '00:00')  then
      asgRekap.Cells[colJamKota2,row3] := '';
    asgRekap.Cells[colJmlKota2,row3] := BufferToString(buffer.Fields[7].Value);
    asgRekap.Cells[colket     ,row3] := BufferToString(buffer.Fields[8].Value);
    asgRekap.Cells[colseq     ,row3] := BufferToString(buffer.Fields[0].Value);
    asgRekap.AddButton(coledit,row3,70,18,'Edit',haCenter,vaCenter);
    asgRekap.AddButton(colhapus,row3,70,18,'Hapus',haCenter,vaCenter);

    asgRekap.RemoveNode(Row2);
    asgRekap.AddNode(Row2, (Row3-Row2)+1);

    asgRekap.RemoveNode(Row);
    asgRekap.AddNode(Row, (Row3-Row)+1);

    buffer.MoveNext;
  end;
  gauge.hide;
  buffer.close;
  arrangecolsize(1);
end;

procedure TfrmLapDataTerminal.loaddataLap;
VAR Periode : TR_FilterPeriode;
    i, row, Row2 :integer;
    buffer : _recordset;
    tempkt1 : string;
    tempkt2 : string;
begin
  Gauge.Show;
  begin periode.FPeriodeAwal      := dtpAwal.Date;
        periode.FPeriodeAkhir     := dtpAkhir.Date;
        periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
        periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
  end;
  asgLap.ClearNormalCells;
  asgLap.RowCount := 2 ;
  Gauge.Show;
//  0       1       2       3       4        5              6          7
// SEQ, Nobody, Noorder, supir, Jamkota1, jmlPenumpang1, Jamkota2, jmlPenumpang2, '+
//   8     9      10     11     12       13         14        15           16        17
// ket2, ket1, Kota1, Kota2, tanggal, Supir_SEQ, Armada_SEQ, order_SEQ, kota1_SEQ, kota2_SEQ
  Load_master_OPERASIONAL_TERMINAL(buffer, periode,'','',SeqArmada);

  tempKt1 := ''; tempkt2 := '';
  for i := 0 to buffer.RecordCount-1 do begin
    Gauge.Progress := round((i+1)/buffer.RecordCount*100);

    if (tempKt1 <> BufferToString(buffer.Fields[10].Value)) or (tempKt2 <> BufferToString(buffer.Fields[11].Value)) then begin
      if asgLap.cells[coltgl,3] <> '' then asgLap.AddRow;
      row := asgLap.RowCount-1;
      asgLap.cells[colKotaKe1, row] := BufferToString(buffer.Fields[10].Value);
      asgLap.cells[colKotaKe2, row] := BufferToString(buffer.Fields[11].Value);
      asgLap.MergeCells(colno+1,row,3,1);
    end;
    tempKt1 :=  BufferToString(buffer.Fields[10].Value);
    tempKt2 :=  BufferToString(buffer.Fields[11].Value);
    asgLap.AddRow;
    row2 := asgLap.rowcount-1;
    asgLap.ints[colno , row2]        := i+1;
    asgLap.Cells[colArmada   , row2] := BufferToString(Buffer.Fields[1].Value);
    asgLap.Cells[colJurusan  , row2] := EkstrakString(ListJurusanGlobal.Values[BufferToString(Buffer.Fields[18].Value)],'#',2);
    asgLap.Cells[colTerminal , row2] := EkstrakString(BufferToString(Buffer.Fields[9].Value),' ',HitungChar(BufferToString(Buffer.Fields[9].Value),' ')+1);
    asgLap.Cells[colKotaKe1  , row2] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
    asgLap.Cells[colKotaKe2  , row2] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
    if (asglap.Cells[colJamKota1,row2] = '00.00') or (asglap.Cells[colJamKota1,row2] = '00:00')  then
      asglap.Cells[colJamKota1,row2] := '';
    if (asglap.Cells[colJamKota2,row2] = '00.00') or (asglap.Cells[colJamKota2,row2] = '00:00')  then
      asglap.Cells[colJamKota2,row2] := '';
    buffer.MoveNext;
  end;
  gauge.hide;
  buffer.close;
  arrangecolsize(2);
end;


procedure TfrmLapDataTerminal.loaddataRit;
VAR Periode : TR_FilterPeriode;
    i,j, row, Row2, colcount,jmlSeqKota,tempArmada :integer;
    buffer : _recordset;
    temptgl : tdate;
    seqKota,tempkt2, listseqkota,seqJurusan  : string;
    isaddrow : boolean;
begin
  Gauge.Show;
  begin periode.FPeriodeAwal      := dtpAwal.Date;
        periode.FPeriodeAkhir     := dtpAkhir.Date;
        periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
        periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
  end;
  asgRit.ClearNormalCells;
  asgRit.RowCount := 3 ;
  asgRit.colcount := 3;
  asgRekap.Cells[ColNode, 0] := '[-]';
  Gauge.Show;
//    0       1            2       3            4        5              6
// m.SEQ, m.Nobody, m.Noorder, m.supir, m.Jamkota1, m.jmlPenumpang1, m.Jamkota2, '+
//          7          8       9      10        11      12           13
// m.jmlPenumpang2, m.ket2, m.ket1, m.Kota1, m.Kota2, m.tanggal, m.Supir_SEQ, '+
//       14          15            16           17         18
// m.Armada_SEQ, m.order_SEQ, m.kota1_SEQ, m.kota2_SEQ, ma.jurusan_seq '+
  Load_master_OPERASIONAL_TERMINAL_4_lap(buffer, periode,'','',  SeqArmada);
  Get_List_detail_jurusan_per_jurusan(listkotajurusan);
  temptgl := 0; tempkt2 := ''; Row := 0; tempArmada:=0;
  isaddrow := false;
  colcount := asgRit.ColCount-1;
  for i := 0 to buffer.RecordCount-1 do begin
    Gauge.Progress := round((i+1)/buffer.RecordCount*100);
    if (temptgl <> BufferToDateTime(buffer.Fields[12].Value)) then begin
      if asgRit.cells[coltgl,2] <> '' then asgRit.AddRow;
      row := asgRit.RowCount-1;
      asgRit.Dates[colTgl, row] := BufferToDateTime(buffer.Fields[12].Value);
      asgRit.MergeCells(colno+1,row,2,1);
    end;
    temptgl :=  BufferToDateTime(buffer.Fields[12].Value);
    listseqkota := '';
    if tempArmada <> BufferToInteger(Buffer.Fields[14].Value) then begin
      asgRit.AddRow;
      row2 := asgRit.rowcount-1;
      asgRit.Cells[colArmada, row2] := ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2);
      seqJurusan := BufferToString(Buffer.Fields[18].Value);

      isaddrow := true;
//      asgRit.AddRow;
//       isaddrow := true;
    end;// else isaddrow := false;
    tempArmada := BufferToInteger(Buffer.Fields[14].Value);
    row2 := asgRit.rowcount-1;
//    asgRit.addrow;
    listseqkota := listkotajurusan.values[seqJurusan];
    if listseqkota <> '' then begin
      jmlSeqKota := HitungChar(listseqkota,'#');
//      asgRit.AddRow;
      for j :=  0 to jmlSeqKota do begin
        seqKota := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j);
        if seqKota <> '' then begin
          if colcount > asgRit.colcount-2 then begin
            asgRit.AddColumn;
            asgRit.SplitColumnCells(2);
            asgRit.Cells[asgRit.colcount-2,0] := 'Kota';
            asgRit.Cells[asgRit.colcount-2,1] := 'Jam';
            asgRit.AddColumn;
          end;
          if isaddrow then begin
            asgRit.addrow;
            isaddrow := false;
          end;
          row2 := asgRit.rowcount-1;

          if asgRit.Cells[colArmada, row2-1]  = ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2) then begin
            asgRit.Cells[colcount,row2-1] := EkstrakString(ListKotaGlobal.Values[seqKota],'#',2);
            asgRit.Cells[colcount+1,row2-1] := seqKota;
          end;
          if seqKota = BufferToString(Buffer.Fields[16].Value) then begin
//            if formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value)) <> '00.00' then
            asgRit.Cells[colcount,row2] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value))
          end else if seqKota = BufferToString(Buffer.Fields[17].Value) then begin
            asgRit.Cells[colcount,row2] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
          end;
          if (asgRekap.Cells[colcount,row2] = '00.00') or (asgRekap.Cells[colcount,row2] = '00:00')  then
              asgRekap.Cells[colcount,row2] := '';


//          if isaddrow then begin
//            asgRit.addrow;
//            isaddrow := false;
//          end;
//          for K := 3 to asgRit.colcount-1 do begin
//            if asgRit.cells[k,row2] = BufferToString(Buffer.Fields[16].Value) then begin
//              asgRit.Cells[k-1,row2+1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
//            end;
//            if asgRit.cells[k,row2] = BufferToString(Buffer.Fields[17].Value) then begin
//              asgRit.Cells[k-1,row2+1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[7].Value));
//            end;
//          end;
          colcount := colcount+2;

//          if seqKota = BufferToString(Buffer.Fields[16].Value) then begin
//            asgRit.Cells[colcount+j,row2+1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
//          end else if seqKota = BufferToString(Buffer.Fields[17].Value) then begin
//            asgRit.Cells[colcount+j,row2+1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[7].Value));
//          end;
        end;
      end;
      colcount := 2;
    end;
    asgRit.RemoveNode(Row);
    asgRit.AddNode(Row, (Row2-Row)+1);
    buffer.MoveNext;
  end;
  asgRit.MergeCells(asgRit.ColCount-1,0,1,2);
  asgRit.contractall;
  gauge.hide;
  buffer.close;
  arrangecolsize(3);
end;


procedure TfrmLapDataTerminal.loaddataUnit;
VAR Periode : TR_FilterPeriode;
    i,j, row1,row, Row2, colcount,jmlSeqKota,tempArmada :integer;
    buffer : _recordset;
    seqKota,seqKota2,tempkt2,temptglarmada, listseqkota,seqJurusan, tampungkota1, tampungkota2 : string;
    listsetoran : tstringlist;
begin
  listsetoran := TStringList.Create;
  listsetoran.Clear;
  Gauge.Show;
  begin periode.FPeriodeAwal      := dtpAwal.Date;
        periode.FPeriodeAkhir     := dtpAkhir.Date;
        periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
        periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
  end;
  asgUnit.ClearNormalCells;
  asgUnit.SplitAllCells;
  asgUnit.RowCount := 3;
  asgUnit.colcount := 6;
  asgRekap.Cells[ColNode, 0] := '[+]';
  asgUnit.MergeCells(colNode,0,1,2);
  asgUnit.MergeCells(colorder,0,2,1);
  Gauge.Show;
//    0       1            2       3            4        5              6
// m.SEQ, m.Nobody, m.Noorder, m.supir, m.Jamkota1, m.jmlPenumpang1, m.Jamkota2, '+
//          7          8       9      10        11      12           13
// m.jmlPenumpang2, m.ket2, m.ket1, m.Kota1, m.Kota2, m.tanggal, m.Supir_SEQ, '+
//       14          15            16           17         18                 19
// m.Armada_SEQ, m.order_SEQ, m.kota1_SEQ, m.kota2_SEQ, ma.jurusan_seq,s.jumlah_setoran '+
  Load_master_OPERASIONAL_TERMINAL_4_lap_perunit(buffer, periode,'','',  SeqArmada);
  Get_List_detail_jurusan_per_jurusan(listkotajurusan);
  seqKota:= ''; seqKota2 := ''; row := 0;  row1 := 0;
  tempkt2 := ''; tempArmada:=0;
  colcount := asgUnit.ColCount-1;
  for i := 0 to buffer.RecordCount-1 do begin
    Gauge.Progress := round((i+1)/buffer.RecordCount*100);
    if tempArmada <> BufferToInteger(Buffer.Fields[14].Value) then begin
      if asgUnit.cells[colArmada,2] <> '' then asgUnit.AddRow;
      row := asgUnit.rowcount-1;
      asgUnit.MergeCells(colorder,row,2,1);
      asgUnit.Cells[colArmada  , row]  := ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2);
      seqJurusan := BufferToString(Buffer.Fields[18].Value);
    end;
    if temptglarmada <> BufferToString(buffer.Fields[12].Value)+'^'+BufferToString(buffer.Fields[14].Value) then begin
      asgUnit.AddRow;
      row1 := asgUnit.RowCount-1;
      asgUnit.Cells[colTempArmada,row1] := ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2);
      asgUnit.Dates[colTgl,       row1] := BufferToDateTime(buffer.Fields[12].Value);
//      asgUnit.cells[colorder,     row1] := BufferTostring(buffer.Fields[2].Value);
      if  BufferTostring(buffer.Fields[20].Value) <> '' then
         asgUnit.cells[colorder,     row1] :=  BufferTostring(buffer.Fields[20].Value)
      else asgUnit.cells[colorder,     row1] := BufferTostring(buffer.Fields[2].Value);
      asgUnit.cells[colpengemudi, row1] := BufferTostring(buffer.Fields[3].Value);
      listsetoran.add(asgUnit.Cells[colTempArmada,row1]+'^'+asgUnit.cells[colTgl,row1]+'='+FloatToStrFmt2(BufferTofloat(buffer.Fields[19].Value)));
    end;
    tempArmada := BufferToInteger(Buffer.Fields[14].Value);
    temptglarmada := BufferToString(buffer.Fields[12].Value)+'^'+BufferToString(buffer.Fields[14].Value);
    listseqkota := '';
    row2 := asgUnit.rowcount-1;
    listseqkota := listkotajurusan.values[seqJurusan];
    if listseqkota <> '' then begin
      jmlSeqKota := HitungChar(listseqkota,'#');
      for j :=  0 to jmlSeqKota-1 do begin
        seqKota := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j);
        seqKota2 := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j+1);
        tampungkota1 := '';
        tampungkota2 := '';
        if seqKota <> '' then begin
          if colcount > asgUnit.colcount-2 then begin
            asgUnit.AddColumn;
            asgUnit.SplitColumnCells(2);
            asgUnit.Cells[asgUnit.colcount-2,0] := 'Kota';
            asgUnit.Cells[asgUnit.colcount-2,1] := 'Jam';
            asgUnit.AddColumn;
          end;
          row2 := asgUnit.rowcount-1;

          if asgUnit.Cells[colArmada, row]  = ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2) then begin
            asgUnit.Cells[colcount,  row] := EkstrakString(ListKotaGlobal.Values[seqKota],'#',2);
            if seqKota2 <> '' then
              asgUnit.Cells[colcount,  row] := asgUnit.Cells[colcount,  row] + ' - '+ EkstrakString(ListKotaGlobal.Values[seqKota2],'#',2);
            asgUnit.Cells[colcount+1,row] := seqKota+'^'+seqKota2;
          end;
//          iqbal 310817 tutup dulu
//          if {((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[16].Value))) or}
//             ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) then  begin
          if (seqKota = BufferToString(Buffer.Fields[16].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value)) then  begin
            tampungkota1 := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
            if (tampungkota1 = '00.00') or (tampungkota1 = '00:00') then
              tampungkota1 := '';
            tampungkota2 :=  formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
            if (tampungkota2 = '00.00') or (tampungkota2 = '00:00')  then
              tampungkota2 := '';

            if (tampungkota1 <> '') and (tampungkota2 <> '') then
              asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
            else if(tampungkota1 <> '') and (tampungkota2 = '') then
              asgUnit.Cells[colcount,row1] := tampungkota1
            else if(tampungkota1 = '') and (tampungkota2 <> '') then
              asgUnit.Cells[colcount,row1] := tampungkota2
            else asgUnit.Cells[colcount,row1] := '';
          end;
          colcount := colcount+2;
        end;
      end;
      for j := jmlSeqKota-1 downto 0 do begin
        seqKota := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j);
        seqKota2 := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j+1);
        if seqKota <> '' then begin
          if colcount > asgUnit.colcount-2 then begin
            asgUnit.AddColumn;
            asgUnit.SplitColumnCells(2);
            asgUnit.Cells[asgUnit.colcount-2,0] := 'Kota';
            asgUnit.Cells[asgUnit.colcount-2,1] := 'Jam';
            asgUnit.AddColumn;
          end;
          row2 := asgUnit.rowcount-1;

          if asgUnit.Cells[colArmada, row]  = ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2) then begin
            asgUnit.Cells[colcount,  row] := EkstrakString(ListKotaGlobal.Values[seqKota],'#',2);
            if seqKota2 <> '' then
              asgUnit.Cells[colcount,  row] := EkstrakString(ListKotaGlobal.Values[seqKota2],'#',2) + ' - '+ asgUnit.Cells[colcount,  row];
            asgUnit.Cells[colcount+1,row] := seqKota2+'^'+seqKota;
          end;
//          iqbal 310817 tutup dulu
//          if {((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[16].Value))) or}
//             ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value))) then  begin
          if (seqKota2 = BufferToString(Buffer.Fields[16].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value)) then  begin
            tampungkota1 := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
            if (tampungkota1 = '00.00') or (tampungkota1 = '00:00') then
              tampungkota1 := '';
            tampungkota2 :=  formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
            if (tampungkota2 = '00.00') or (tampungkota2 = '00:00')  then
              tampungkota2 := '';                                               

            if (tampungkota1 <> '') and (tampungkota2 <> '') then
              asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
            else if(tampungkota1 <> '') and (tampungkota2 = '') then
              asgUnit.Cells[colcount,row1] := tampungkota1
            else if(tampungkota1 = '') and (tampungkota2 <> '') then
              asgUnit.Cells[colcount,row1] := tampungkota2
            else asgUnit.Cells[colcount,row1] := '';
          end;
//          if seqKota = BufferToString(Buffer.Fields[16].Value) then  begin
//            asgUnit.Cells[colcount,row1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
//            if (asgUnit.Cells[colcount,row1] = '00.00') or (asgUnit.Cells[colcount,row1] = '00:00') then
//              asgUnit.Cells[colcount,row1] := '';
//          end else if seqKota = BufferToString(Buffer.Fields[17].Value) then begin
//            asgUnit.Cells[colcount,row1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
//            if (asgUnit.Cells[colcount,row1] = '00.00') or (asgUnit.Cells[colcount,row1] = '00:00')  then
//              asgUnit.Cells[colcount,row1] := '';
//          end;
          colcount := colcount+2;
        end;
      end;
      colcount := 5;
    end;
    asgUnit.RemoveNode(Row);
    asgUnit.AddNode(Row, (Row2-Row)+1);
    buffer.MoveNext;
  end;
  asgUnit.ExpandAll;
  for i := 2 to asgUnit.rowcount - 1 do begin
    if i = 2 then begin
      asgUnit.AddColumn;
      asgUnit.MergeCells(asgUnit.ColCount-2,0,1,2);
      asgUnit.Cells[asgUnit.colcount-2,0] := 'Setoran';
    end;
    if (asgUnit.Cells[colTempArmada,i] <> '') and (asgUnit.cells[colTgl,i] <> '') then begin
      asgUnit.floats[asgUnit.colcount-2, i] := StrfmtToFloatDef(listsetoran.Values[asgUnit.Cells[colTempArmada,i]+'^'+asgUnit.cells[colTgl,i]],0);
    end else asgUnit.cells[asgUnit.colcount-2, i] := '';
  end;
  listsetoran.destroy;
  asgUnit.MergeCells(asgUnit.ColCount-1,0,1,2);
  asgUnit.contractall;
  gauge.hide;
  buffer.close;
  arrangecolsize(4);
end;

procedure TfrmLapDataTerminal.LoadDataUnitNew;
VAR Periode : TR_FilterPeriode;
    i,j, row1,row, Row2, colcount,jmlSeqKota,tempArmada,K :integer;
    buffer : _recordset;
    seqKota,seqKota2,tempkt2,temptglarmada, listseqkota,seqJurusan, tampungkota1, tampungkota2 : string;
    listsetoran : tstringlist;
begin
  listsetoran := TStringList.Create;
  listsetoran.Clear;
  Gauge.Show;
  begin periode.FPeriodeAwal      := dtpAwal.Date;
        periode.FPeriodeAkhir     := dtpAkhir.Date;
        periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
        periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
  end;
  asgUnit.ClearNormalCells;
  asgUnit.SplitAllCells;
  asgUnit.RowCount := 3;
  asgUnit.colcount := 6;
  asgRekap.Cells[ColNode, 0] := '[+]';
  asgUnit.MergeCells(colNode,0,1,2);
  asgUnit.MergeCells(colorder,0,2,1);
  Gauge.Show;
//    0       1            2       3            4        5              6
// m.SEQ, m.Nobody, m.Noorder, m.supir, m.Jamkota1, m.jmlPenumpang1, m.Jamkota2, '+
//          7          8       9      10        11      12           13
// m.jmlPenumpang2, m.ket2, m.ket1, m.Kota1, m.Kota2, m.tanggal, m.Supir_SEQ, '+
//       14          15            16           17         18                 19            20             21
// m.Armada_SEQ, m.order_SEQ, m.kota1_SEQ, m.kota2_SEQ, ma.jurusan_seq,s.jumlah_setoran, sj.nomor, m.Kota_Sumber_Seq
  Load_master_OPERASIONAL_TERMINAL_4_lap_perunit(buffer, periode,'','',  SeqArmada);
  Get_List_detail_jurusan_per_jurusan(listkotajurusan);
  seqKota:= ''; seqKota2 := ''; row := 0;  row1 := 0;
  tempkt2 := ''; tempArmada:=0;
  colcount := asgUnit.ColCount-1;
  for i := 0 to buffer.RecordCount-1 do begin
    Gauge.Progress := round((i+1)/buffer.RecordCount*100);
    if tempArmada <> BufferToInteger(Buffer.Fields[14].Value) then begin
      if asgUnit.cells[colArmada,2] <> '' then asgUnit.AddRow;
      row := asgUnit.rowcount-1;
      asgUnit.MergeCells(colorder,row,2,1);
      asgUnit.Cells[colArmada  , row]  := ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2);
      seqJurusan := BufferToString(Buffer.Fields[18].Value);
    end;
//    if temptglarmada <> BufferToString(buffer.Fields[12].Value)+'^'+BufferToString(buffer.Fields[14].Value)+'^'+BufferToString(buffer.Fields[21].Value) then begin
    if temptglarmada <> BufferToString(buffer.Fields[12].Value)+'^'+BufferToString(buffer.Fields[14].Value) then begin
      asgUnit.AddRow;
      row1 := asgUnit.RowCount-1;
      asgUnit.Cells[colTempArmada,row1] := ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2);
      asgUnit.Dates[colTgl,       row1] := BufferToDateTime(buffer.Fields[12].Value);
//      asgUnit.cells[colorder,     row1] := BufferTostring(buffer.Fields[2].Value);
      if  BufferTostring(buffer.Fields[20].Value) <> '' then
         asgUnit.cells[colorder,     row1] :=  BufferTostring(buffer.Fields[20].Value)
      else asgUnit.cells[colorder,     row1] := BufferTostring(buffer.Fields[2].Value);
      asgUnit.cells[colpengemudi, row1] := BufferTostring(buffer.Fields[3].Value);
      listsetoran.add(asgUnit.Cells[colTempArmada,row1]+'^'+asgUnit.cells[colTgl,row1]+'='+FloatToStrFmt2(BufferTofloat(buffer.Fields[19].Value)));
    end;
    tempArmada := BufferToInteger(Buffer.Fields[14].Value);
    temptglarmada := BufferToString(buffer.Fields[12].Value)+'^'+BufferToString(buffer.Fields[14].Value);
    listseqkota := '';
    row2 := asgUnit.rowcount-1;
    listseqkota := listkotajurusan.values[seqJurusan];
    if listseqkota <> '' then begin
      jmlSeqKota := HitungChar(listseqkota,'#');
      for j :=  0 to jmlSeqKota-1 do begin
        seqKota := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j);
        seqKota2 := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j+1);
        tampungkota1 := '';
        tampungkota2 := '';
        if seqKota <> '' then begin
          if colcount > asgUnit.colcount-2 then begin
            asgUnit.AddColumn;
            asgUnit.SplitColumnCells(2);
            asgUnit.Cells[asgUnit.colcount-2,0] := 'Kota';
            asgUnit.Cells[asgUnit.colcount-2,1] := 'Jam';
            asgUnit.AddColumn;
          end;
          row2 := asgUnit.rowcount-1;

          if asgUnit.Cells[colArmada, row]  = ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2) then begin
            asgUnit.Cells[colcount,  row] := EkstrakString(ListKotaGlobal.Values[seqKota],'#',2);
            if seqKota2 <> '' then
              asgUnit.Cells[colcount,  row] := asgUnit.Cells[colcount,  row] + ' - '+ EkstrakString(ListKotaGlobal.Values[seqKota2],'#',2);
            asgUnit.Cells[colcount+1,row] := seqKota+'^'+seqKota2;
          end;
          if tampungkota1 = '' then
            tampungkota1 := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));         
          if tampungkota2 = '' then
            tampungkota2 :=  formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
          if (tampungkota1 = '00.00') or (tampungkota1 = '00:00') then tampungkota1 := '';
          if (tampungkota2 = '00.00') or (tampungkota2 = '00:00') then tampungkota2 := '';
//          for k := StrToInt(seqkota) to StrToInt(seqKota2) do begin
//            if (seqKota = BufferToString(Buffer.Fields[21].Value)) and ((StrToInt(seqKota2) = BufferToInteger(Buffer.Fields[16].Value)) or
//            (StrToInt(seqKota2) = BufferToInteger(Buffer.Fields[17].Value))) then begin

              if (tampungkota1 <> '') or (tampungkota2 <> '') then begin
//                if ((seqKota = BufferToString(Buffer.Fields[16].Value))
//                and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) then  begin
//                   asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
//                end else
                if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[16].Value))) and
                   ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) then  begin
                   if (tampungkota1 <> '-') then
                     asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
//                end else//juli tambah
//                if (tampungkota1 <> '') and (seqKota = BufferToString(Buffer.Fields[21].Value)) and (tampungkota1 <> '-') then begin
//                  asgUnit.Cells[colcount,row1] := tampungkota1;
//                  tampungkota1 := '-';
//                end else if (tampungkota1 <> '') and (seqKota = BufferToString(Buffer.Fields[16].Value)) and (tampungkota1 <> '-') then begin
//                  if asgUnit.Cells[colcount,row1] <> '' then BEGIN
//                    asgUnit.Cells[colcount,row1] := tampungkota1;     //+ christian 121017
//                    tampungkota1 := '-';
//                  END;
                end else if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[16].Value)))  then  begin
                   if (tampungkota1 <> '-') and (tampungkota1 <> '') then begin
                     asgUnit.Cells[colcount,row1] := tampungkota1;
                     tampungkota1 := '-';
                   end;
                end else if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value)))  then  begin
                   if (tampungkota2 <> '-') and (tampungkota2 <> '') then begin
                     asgUnit.Cells[colcount,row1] := tampungkota2;
                     tampungkota2 := '-';
                   end;

//                end else if (tampungkota2 <> '') and (seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (tampungkota2 <> '-') then begin
//                  asgUnit.Cells[colcount,row1] := tampungkota2;
//                  tampungkota2 := '-';

                end;                                   //

//              end else if(tampungkota1 <> '') and (tampungkota2 = '') then begin
//                if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[16].Value))) then  begin
//                   asgUnit.Cells[colcount,row1] := tampungkota1;
////                end else if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) then  begin
////                   asgUnit.Cells[colcount,row1] := tampungkota1;
//                end;
//              end else if(tampungkota1 = '') and (tampungkota2 <> '') then begin
//                if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) then  begin
//                   asgUnit.Cells[colcount,row1] := tampungkota2;
//                end else if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[16].Value))) then  begin
//                   asgUnit.Cells[colcount,row1] := tampungkota2;
//                end;
              end;
//            end;
//          end;

//          iqbal 310817 tutup dulu
          {if {((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[16].Value))) or
             ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) then  begin
//          if (seqKota = BufferToString(Buffer.Fields[16].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value)) then  begin
            tampungkota1 := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
            if (tampungkota1 = '00.00') or (tampungkota1 = '00:00') then
              tampungkota1 := '';
            tampungkota2 :=  formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
            if (tampungkota2 = '00.00') or (tampungkota2 = '00:00')  then
              tampungkota2 := '';

            if (tampungkota1 <> '') and (tampungkota2 <> '') then
              asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
            else if(tampungkota1 <> '') and (tampungkota2 = '') then
              asgUnit.Cells[colcount,row1] := tampungkota1
            else if(tampungkota1 = '') and (tampungkota2 <> '') then
              asgUnit.Cells[colcount,row1] := tampungkota2
            else asgUnit.Cells[colcount,row1] := '';
          end;}
          colcount := colcount+2;
        end;
      end;
      for j := jmlSeqKota-1 downto 0 do begin
        seqKota := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j);
        seqKota2 := ekstrakstring(listkotajurusan.values[seqJurusan],'#',j+1);
        if seqKota <> '' then begin
          if colcount > asgUnit.colcount-2 then begin
            asgUnit.AddColumn;
            asgUnit.SplitColumnCells(2);
            asgUnit.Cells[asgUnit.colcount-2,0] := 'Kota';
            asgUnit.Cells[asgUnit.colcount-2,1] := 'Jam';
            asgUnit.AddColumn;
          end;
          row2 := asgUnit.rowcount-1;

          if asgUnit.Cells[colArmada, row]  = ekstrakstring(listArmadaGlobal.values[BufferToString(Buffer.Fields[14].Value)],'#',2) then begin
            asgUnit.Cells[colcount,  row] := EkstrakString(ListKotaGlobal.Values[seqKota],'#',2);
            if seqKota2 <> '' then
              asgUnit.Cells[colcount,  row] := EkstrakString(ListKotaGlobal.Values[seqKota2],'#',2) + ' - '+ asgUnit.Cells[colcount,  row];
            asgUnit.Cells[colcount+1,row] := seqKota2+'^'+seqKota;
          end;
          tampungkota1 := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
          if tampungkota2 = '' then
            tampungkota2 :=  formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
          if (tampungkota1 = '00.00') or (tampungkota1 = '00:00') then tampungkota1 := '';
          if (tampungkota2 = '00.00') or (tampungkota2 = '00:00') then tampungkota2 := '';
          if (tampungkota1 <> '') or (tampungkota2 <> '') then begin
//            if ((seqKota2 = BufferToString(Buffer.Fields[16].Value))
//            and (seqKota = BufferToString(Buffer.Fields[17].Value))) then  begin
//              asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
//            end else
            if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[16].Value))) and
            ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value))) then  begin
              asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
            end else if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[16].Value)))  then  begin
              if (tampungkota1 <> '-') and (tampungkota1 <> '') then begin
                asgUnit.Cells[colcount,row1] := tampungkota1;
                tampungkota1 := '-';
              end;
            end else if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value)))  then  begin
              if (tampungkota2 <> '-') and (tampungkota2 <> '') then begin
                asgUnit.Cells[colcount,row1] := tampungkota2;
                tampungkota2 := '-';
              end;
//            end else//juli tambah
//            if (tampungkota2 <> '') and (seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (tampungkota2 <> '-') then begin
//              asgUnit.Cells[colcount,row1] := tampungkota2;
//              tampungkota2 := '-';
//            end else if (tampungkota2 <> '') and (seqKota2 = BufferToString(Buffer.Fields[17].Value)) and (tampungkota2 <> '-') then begin
//              if asgUnit.Cells[colcount,row1] = '' then begin
//                asgUnit.Cells[colcount,row1] := tampungkota2;    //+ christian 121017
//                tampungkota2 := '-';
//              end;
//            end else if ((seqKota = BufferToString(Buffer.Fields[21].Value)) and (seqKota2 = BufferToString(Buffer.Fields[17].Value))) and (tampungkota2 <> '') and (tampungkota2 <> '-') then  begin
//              asgUnit.Cells[colcount,row1] := tampungkota2;
//              tampungkota2 := '-';
//            end else if (tampungkota2 <> '') and (seqKota2 = BufferToString(Buffer.Fields[16].Value)) and (tampungkota2 <> '-') then begin
//              asgUnit.Cells[colcount,row1] := tampungkota2;    //+ christian 121017  fungsinya data pertama gatau kenapa
//              tampungkota2 := '-';
//            end else if (tampungkota2 <> '') and (tampungkota1 = '-') and (seqKota2 = BufferToString(Buffer.Fields[17].Value)) and (tampungkota2 <> '-') then begin
//              asgUnit.Cells[colcount,row1] := tampungkota2;
//              tampungkota2 := '-';
            end;                                   //
//          end else if(tampungkota1 <> '') and (tampungkota2 = '') then begin
//            if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[16].Value))) then  begin
//              asgUnit.Cells[colcount,row1] := tampungkota1;
////            end else if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value))) then  begin
////              asgUnit.Cells[colcount,row1] := tampungkota1;
//            end;
//          end else if(tampungkota1 = '') and (tampungkota2 <> '') then begin
//            if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value))) then  begin
//              asgUnit.Cells[colcount,row1] := tampungkota2;
////            end else if ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[16].Value))) then  begin
////              asgUnit.Cells[colcount,row1] := tampungkota2;
//            end;
          end;

//          end;
//          iqbal 310817 tutup dulu
//          if {((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[16].Value))) or}
//             ((seqKota2 = BufferToString(Buffer.Fields[21].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value))) then  begin
////          if (seqKota2 = BufferToString(Buffer.Fields[16].Value)) and (seqKota = BufferToString(Buffer.Fields[17].Value)) then  begin
//            tampungkota1 := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
//            if (tampungkota1 = '00.00') or (tampungkota1 = '00:00') then
//              tampungkota1 := '';
//            tampungkota2 :=  formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
//            if (tampungkota2 = '00.00') or (tampungkota2 = '00:00')  then
//              tampungkota2 := '';
//
//            if (tampungkota1 <> '') and (tampungkota2 <> '') then
//              asgUnit.Cells[colcount,row1] := tampungkota1 + ' - ' + tampungkota2
//            else if(tampungkota1 <> '') and (tampungkota2 = '') then
//              asgUnit.Cells[colcount,row1] := tampungkota1
//            else if(tampungkota1 = '') and (tampungkota2 <> '') then
//              asgUnit.Cells[colcount,row1] := tampungkota2
//            else asgUnit.Cells[colcount,row1] := '';
//          end;
//          if seqKota = BufferToString(Buffer.Fields[16].Value) then  begin
//            asgUnit.Cells[colcount,row1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[4].Value));
//            if (asgUnit.Cells[colcount,row1] = '00.00') or (asgUnit.Cells[colcount,row1] = '00:00') then
//              asgUnit.Cells[colcount,row1] := '';
//          end else if seqKota = BufferToString(Buffer.Fields[17].Value) then begin
//            asgUnit.Cells[colcount,row1] := formatdatetime('HH:mm',BufferToDateTime(buffer.Fields[6].Value));
//            if (asgUnit.Cells[colcount,row1] = '00.00') or (asgUnit.Cells[colcount,row1] = '00:00')  then
//              asgUnit.Cells[colcount,row1] := '';
//          end;
          colcount := colcount+2;
        end;
      end;
      colcount := 5;
    end;
    asgUnit.RemoveNode(Row);
    asgUnit.AddNode(Row, (Row2-Row)+1);
    buffer.MoveNext;
  end;
  asgUnit.ExpandAll;
  for i := 2 to asgUnit.rowcount - 1 do begin
    if i = 2 then begin
      asgUnit.AddColumn;
      asgUnit.MergeCells(asgUnit.ColCount-2,0,1,2);
      asgUnit.Cells[asgUnit.colcount-2,0] := 'Setoran';
    end;
    if (asgUnit.Cells[colTempArmada,i] <> '') and (asgUnit.cells[colTgl,i] <> '') then begin
      asgUnit.floats[asgUnit.colcount-2, i] := StrfmtToFloatDef(listsetoran.Values[asgUnit.Cells[colTempArmada,i]+'^'+asgUnit.cells[colTgl,i]],0);
    end else asgUnit.cells[asgUnit.colcount-2, i] := '';
  end;
  listsetoran.destroy;
  asgUnit.MergeCells(asgUnit.ColCount-1,0,1,2);
  asgUnit.contractall;
  gauge.hide;
  buffer.close;
  arrangecolsize(4);
end;

procedure TfrmLapDataTerminal.SetGrid(idx : integer);
begin
  if (idx = 0) or (idx = 1) then begin
    asgRekap.ClearNormalCells;
    asgRekap.ColCount := 16;
    asgRekap.RowCount := 4;
    asgRekap.FixedCols := 0;
    asgRekap.FixedRows := 3;

    asgRekap.MergeCells(colNode,0,1,3);
    asgRekap.Cells[colNode,0] := '[+]';

    asgRekap.MergeCells(colTgl+1,0,11,2);
    asgRekap.Cells[colTgl,0] := 'Tanggal';

  //  asgRekap.MergeCells(colTgl+1,1,10,1);
    asgRekap.Cells[colTgl,1] := 'Laporan Terminal';

    asgRekap.Cells[colArmada,2] := 'No. Body';
    asgRekap.Cells[colNoOrder,2] := 'No. Order';
    asgRekap.Cells[colSupir,2] := 'Supir';
    asgRekap.Cells[colKota1,2] := 'Kota 1';
    asgRekap.Cells[colJamKota1,2] := 'Jam Kota 1';
    asgRekap.Cells[colJmlKota1,2] := 'Jml. Kota 1';
    asgRekap.Cells[colKota2,2] := 'Kota 2';
    asgRekap.Cells[colJamKota2,2] := 'Jam Kota 2';
    asgRekap.Cells[colJmlKota2,2] := 'Jml. Kota 2';
    asgRekap.Cells[colket,2] := 'Keterangan';
    asgRekap.Cells[coledit,2] := 'Edit';
    asgRekap.Cells[colhapus,2] := 'Hapus';

    asgRekap.MergeCells(asgRekap.ColCount-1,0,1,3);
  end;
  if (idx = 0) or (idx = 2) then begin
    asgLap.ClearNormalCells;
    asgLap.ColCount := 7;
    asgLap.RowCount := 2;
    asgLap.FixedCols:= 1;
    asgLap.FixedRows:= 1;
  end;
  if (idx = 0) or (idx = 3) then begin
    asgRit.RemoveAllNodes;
    asgRit.Clear;
    asgRit.ColCount := 3;
    asgRit.RowCount := 3;
    asgRit.FixedCols := 0;
    asgRit.FixedRows := 2;

    asgRit.MergeCells(colNode,0,1,2);
    asgRit.Cells[colNode,0] := '[+]';

    asgRit.Cells[colTgl,0] := 'Tanggal';
    asgRit.Cells[colTgl,1] := 'Armada';

//    asgRit.MergeCells(asgRit.ColCount-1,0,1,2);
  end;
  if (idx = 0) or (idx = 4) then begin
    asgUnit.RemoveAllNodes;
    asgUnit.Clear;
    asgUnit.ColCount := 6;
    asgUnit.RowCount := 3;
    asgUnit.FixedCols := 0;
    asgUnit.FixedRows := 2;

    asgUnit.MergeCells(colNode,0,1,2);
    asgUnit.MergeCells(colorder,0,2,1);
    asgUnit.Cells[colNode,0] := '[+]';

    asgUnit.Cells[colNobody,0] := 'No. Body';
    asgUnit.Cells[coltgl,   1] := 'Armada';
    asgUnit.Cells[colorder, 1] := 'No. Order';
    asgUnit.Cells[colpengemudi, 1] := 'No. Pengemudi';
    asgUnit.Cells[coltemparmada, 0] := 'No. Pengemudi';
    asgUnit.MergeCells(asgUnit.ColCount-1,0,1,2);
  end;
  ArrangeColSize(idx);
end;

end.

