unit RekapJurnal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, AdvOfficeButtons, Gauges,
  AdvGlowButton, StdCtrls, AdvEdit, ComCtrls, AdvPanel, ExtCtrls, dateutils, Urecord, ADOInt;

type
  TfrmRekapJurnal = class(TForm)
    MainPanel: TPanel;
    pnlFilter: TPanel;
    AdvPanel1: TAdvPanel;
    Label1: TLabel;
    Label2: TLabel;
    dtpSampai: TDateTimePicker;
    dtpDari: TDateTimePicker;
    AdvPanel2: TAdvPanel;
    Label5: TLabel;
    Label6: TLabel;
    cmbTipeAkun: TComboBox;
    txtCari: TAdvEdit;
    btnCari: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    cmbTransaksi: TComboBox;
    Panel1: TPanel;
    Gauge: TGauge;
    chbSF: TAdvOfficeCheckBox;
    btnCetak: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapGetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure btnCariClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure cmbTipeAkunSelect(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvGlowButton2Click(Sender: TObject);
  private
    { Private declarations }
    SeqAkun,AidMenu : integer;
    ListTransaksi : TStringList;
    tipeAkun : string;
    procedure SetGrid;
    procedure LoadData;
    procedure arrangecolsize;
    procedure initform;
    procedure DetailPatchSaldo;
    procedure updateUangJalan;
//    procedure updateSetoran;
  public
    { Public declarations }
    procedure Execute(AmenuId : integer);
  end;

var
  frmRekapJurnal: TfrmRekapJurnal;

implementation

uses LOV, MainMenu, StrUtils, UGeneral, UEngine, UConst,UTransaksi,UFinance,
  OracleConnection, USystemMenu, UDummy, UReport, UTransaksi_temp,
//  Pilih Transaksi,Faktur Beli Spare Part,Retur Beli,Pemakaian Spare Part,Work Shop,Stok Opname
//Penjualan KS,Setoran,Penerimaan Jaminan Supir,Pengembalian Jaminan Supir,Terima DP Pariwisata,Kembali DP Pariwisata
//Pembayaran Pariwisata,Komisi Agen,Claim Pariwisata,Uang Jalan,Mutasi Dana,Pengeluaran Dana ke Cabang,Penerimaan Dana dari Cabang
//Keluar DP ke Supplier,Kembali DP dari Supplier,Pelunasan Hutang,Pengeluaran Biaya,Penerimaan Lain - lain,Pembelian Asset
//Pelunasan Hutang Asset,Penjualan Aset,Pelunasan Piutang Aset,Terima Pinjaman dari Relasi,Bayar Pinjaman ke Relasi,Keluar Pinjaman ke Relasi
//Terima Kembali Pinjaman dari Relasi,Kasbon Pegawai,Pelunasan Kasbon Pegawai,Bayar Gaji Pegawai,Setor Modal,Pengambilan Laba,Sewa dibayar dimuka

//pacthsaldo
  PatchKasbonInput, PatchKasBankInput, PatchHutangInput, PatchBarangInput, PatchAssetInput, PatchAkunInput, PatchPiutangInput,
//transaksi
  SettingAkunInput, FakturBeliSparePartInput, ReturBeliInput, InputPemakaianSparepart, InputWorkShop,InputStokOpname,
  penjualanKSInput, SetoranInput, PenerimaanJaminanSupirInput, KembaliJaminanSupirInput, TerimaDpPariwisataInput,KembaliDpPariwisataInput,
  PembayaranPariwisataInput, KomisiAgenInput, ClaimPariwisataInput, UangJalanInput, MutasiDanaInput, KeluarDanaCabangInput, TerimaDanaCabangInput,
  KeluarDPKeSuppInput,KembaliDPDariSuppInput,PelunasanHutangSuppInput, KeluarBiayaInput,MasukPendapatanInput,BeliAssetInput,
  BayarHutangAssetInput,JualAssetInput,TerimaPiutangAssetInput,TerimaPinjamanDariRelasiInput,KembaliPinjamanRelasiInput, KeluarPinjamanKeRelasiInput,
  TrmKblPinjamanRelasiInput,KasBonPegawaiInput, PelunasanKasbonPegawaiInput,BayarGajiPegawaiInput,SetoranModalInput,AmbilLabaInput,SewaDibayarDimukaInput,JurnalUmumInput, 
  UUpdate_Jurnal;

const
  colNo         = 0;
  colTanggal    = 1;
  colNomor      = 2;
  colNomorTrans = 3;
  colTipe_Trans = 4;
  colNoAkun     = 5;
  colNamaAkun   = 6;
  colDebit      = 7;
  colKredit     = 8;
  colKet        = 9;
  colSeq        = 10;


{$R *.dfm}

{ TfrmRekapJurnal }

procedure TfrmRekapJurnal.AdvGlowButton1Click(Sender: TObject);
begin
  updateUangJalan;
end;

procedure TfrmRekapJurnal.AdvGlowButton2Click(Sender: TObject);
begin
//  if Update_jurnal_pemakaian_sparepart(dtpDari.Date, Gauge, False) then
  if Update_trans_jurnal(dtpDari.Date, Gauge) then
    Inform(MSG_SUCCESS_UPDATE);
end;

procedure TfrmRekapJurnal.arrangecolsize;
begin
  asgRekap.AutoNumberCol(ColNo);
  asgRekap.AutoSizeColumns(true, 2);
  asgrekap.cells[colNamaAkun, asgrekap.rowcount-1]  := 'Total';
  asgrekap.floatingfooter.columncalc[colDebit] := acSUM;
  asgrekap.floatingfooter.columncalc[colKredit] := acSUM;
  if GlobalSystemUser.AccessLevel <> uconst.LEVEL_DEVELOPER then
    asgrekap.ColWidths[ColSeq] := 0;
end;

procedure TfrmRekapJurnal.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);

var SumberSari : string;
    Buffer : _Recordset;
    tipe, tp,TpLunas, pkt, income,cek1,cek2,cek3,cek4 : string;
    Master   : TR_Jurnal_Master;
    FBMaster : TR_faktur_beli_master;
    RBMaster : TR_retur_beli_master;
    PSMaster : TR_pemakaian_sp_master;
    WSMaster : TR_work_shop_master;
    SOMaster : TR_stok_opname_master;
    FJMaster : TR_faktur_jual_ks_master;
    STMaster : TR_setoran_master;
    JSMaster : TR_jaminan_supir_master;
    KJMaster : TR_kembali_jaminan_supir_mst;
    TBMaster : TR_terima_dp_pariwisata_master;
    KDMaster : TR_kembali_dp_pariwisata_master;
    MPMaster : TR_bayar_pariwisata_master;
    KAMaster : TR_komisi_agen_master;
    CPMaster : TR_claim_pariwisata_master;
    UJMaster : TR_uang_jalan_master;
    MDMaster : TR_mutasi_dana_master;
    KCMaster : TR_keluar_dana_k_cbng_mst;
    TCMaster : TR_terima_dana_dr_cbng_mst;
    KLMaster : TR_keluar_dp_master;
    KMMaster : TR_kembali_dp_master;
    HSMaster : TR_bayar_hutang_master;
    BAMaster : TR_beli_asset_master;
    KBMAster : TR_keluar_biaya_master;
    TPMaster : TR_terima_pendapatan_master;
    PTMaster : TR_PENDAPATAN_TERBAYAR;
    BHMaster : TR_bayar_hutang_asset_master;
    JAMaster : TR_jual_asset_master;
    TAMaster : TR_terima_piutang_asset_master;
    PRMaster : TR_pinjaman_dr_relasi_master;
    BRMaster : TR_bayar_pinjam_ke_relasi_master;
    JRMaster : TR_pinjaman_ke_relasi_master;
    KRMaster : TR_kembali_dr_relasi_master;
    KPMaster : TR_kasbon_pegawai_master;
    LKMaster : TR_lunas_kasbon_pegawai_master;
    GPMaster : TR_GAJI_PEGAWAI_MASTER;
    SMMaster : TR_setor_modal_master;
    PBMaster : TR_ambil_laba_master;
    SWMaster : TR_sewa_dibayar_dimuka_master;
    JUMaster : TR_jurnal_master;

begin
  if (Arow > 0) and (Acol > colNo) then begin
    cek1 := RightStr(AsgRekap.Cells[colTipe_Trans,asgRekap.Row],15);
    cek1 := LeftStr(cek1,3);
    cek2 := RightStr(AsgRekap.Cells[colTipe_Trans,asgRekap.Row],14);
    cek2 := LeftStr(cek2,3);
    cek3 := RightStr(AsgRekap.Cells[colTipe_Trans,asgRekap.Row],13);
    cek3 := LeftStr(cek3,3);
    cek4 := RightStr(AsgRekap.Cells[colTipe_Trans,asgRekap.Row],12);
    cek4 := LeftStr(cek4,3);

    if (cek1='PSB')or(cek1='PSH')or(cek1='PKB')or(cek1='PSP')or(cek1='PSA')or(cek1='PKP')or(cek1='PSN') or
       (cek2='PSB')or(cek2='PSH')or(cek2='PKB')or(cek2='PSP')or(cek2='PSA')or(cek2='PKP')or(cek2='PSN') or
       (cek3='PSB')or(cek3='PSH')or(cek3='PKB')or(cek3='PSP')or(cek3='PSA')or(cek3='PKP')or(cek3='PSN') or
       (cek4='PSB')or(cek4='PSH')or(cek4='PKB')or(cek4='PSP')or(cek4='PSA')or(cek4='PKP')or(cek4='PSN') then begin
       DetailPatchSaldo;
    end else begin
      master := Get_Jurnal_Master(AsgRekap.Ints[ColSeq, asgRekap.Row]);
     //--------------------FAKTUR_BELI-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_FAKTUR_BELI then begin
        FBMaster := Get_faktur_beli_master(Master.trans_seq);
        Tipe := LeftStr(FBMaster.Nomor,2);
        if Tipe = TRANS_FAKTUR_BELI then begin
          Application.CreateForm(TfrmInputFakturBeliSparePart, frmInputFakturBeliSparePart);
          frmInputFakturBeliSparePart.OnClose := frmMainMenu.ChildFormSingle;
          frmInputFakturBeliSparePart.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------RETUR_BELI-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_RETUR_BELI then begin
        RBMaster := Get_retur_beli_master(Master.trans_seq);
        Tipe := LeftStr(RBMaster.Nomor,2);
        if Tipe = TRANS_RETUR_BELI then begin
          Application.CreateForm(TfrmInputReturBeli, frmInputReturBeli);
          frmInputReturBeli.OnClose := frmMainMenu.ChildFormSingle;
          frmInputReturBeli.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------PEMAKAIAN_SP-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PEMAKAIAN_SP then begin
        PSMaster := Get_pemakaian_sp_master(Master.trans_seq);
        Tipe := LeftStr(PSMaster.Nomor,2);
        if Tipe = TRANS_PEMAKAIAN_SP then begin
          Application.CreateForm(TfrmInputPemakaianSparepart, frmInputPemakaianSparepart);
          frmInputPemakaianSparepart.OnClose := frmMainMenu.ChildFormSingle;
          frmInputPemakaianSparepart.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------WORK_SHOP-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_WORK_SHOP then begin
        WSMaster := Get_work_shop_master(Master.trans_seq);
        Tipe := LeftStr(WSMaster.Nomor,2);
        if Tipe = TRANS_WORK_SHOP then begin
          Application.CreateForm(TFrmInputWorkShop, FrmInputWorkShop);
          FrmInputWorkShop.OnClose := frmMainMenu.ChildFormSingle;
          FrmInputWorkShop.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------STOK_OPNAME-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_STOK_OPNAME then begin
        SOMaster := Get_stok_opname_master(Master.trans_seq);
        Tipe := LeftStr(SOMaster.Nomor,2);
        if Tipe = TRANS_STOK_OPNAME then begin
          Application.CreateForm(TfrmInputStokOpname, frmInputStokOpname);
          frmInputStokOpname.OnClose := frmMainMenu.ChildFormSingle;
          frmInputStokOpname.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------PENJUALAN_KS-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENJUALAN_KS then begin
        FJMaster := Get_faktur_jual_ks_master(Master.trans_seq);
        Tipe := LeftStr(FJMaster.Nomor,2);
        if Tipe = TRANS_FAKTUR_JUAL_KS then begin
          Application.CreateForm(TfrmInputPenjualanKS, frmInputPenjualanKS);
          frmInputPenjualanKS.OnClose := frmMainMenu.ChildFormSingle;
          frmInputPenjualanKS.Execute(Master.trans_seq,false);
        end;
      end;
      //-------------------SETORAN_KS-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_SETORAN_KS then begin
        STMaster := Get_setoran_master(Master.trans_seq);
        Tipe := LeftStr(STMaster.Nomor,2);
        if Tipe = TRANS_SETORAN_KS then begin
          Application.CreateForm(TfrmInputSetoran, frmInputSetoran);
          frmInputSetoran.OnClose := frmMainMenu.ChildFormSingle;
          frmInputSetoran.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------PENERIMAAN_JAMINAN-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENERIMAAN_JAMINAN then begin
        JSMaster := Get_jaminan_supir_master(Master.trans_seq);
        Tipe := LeftStr(JSMaster.Nomor,2);
        if Tipe = TRANS_PENERIMAAN_JAMINAN then begin
          Application.CreateForm(TfrmInputPenerimaanJaminanSupir, frmInputPenerimaanJaminanSupir);
          frmInputPenerimaanJaminanSupir.OnClose := frmMainMenu.ChildFormSingle;
          frmInputPenerimaanJaminanSupir.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------KEMBALI_JAMINAN-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KEMBALI_JAMINAN then begin
        KJMaster := Get_kembali_jaminan_supir_mst(Master.trans_seq);
        Tipe := LeftStr(KJMaster.Nomor,2);
        if Tipe = TRANS_KEMBALI_JAMINAN then begin
          Application.CreateForm(TfrmInputKembaliJaminanSupir, frmInputKembaliJaminanSupir);
          frmInputKembaliJaminanSupir.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKembaliJaminanSupir.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------TERIMA_DP_PARIWISATA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_TERIMA_DP_PARIWISATA then begin
        TPMaster := Get_terima_pendapatan_master(Master.trans_seq);
        Tipe := LeftStr(TPMaster.Nomor,2);
        if Tipe = TRANS_TERIMA_DP_PARIWISATA then begin
          Application.CreateForm(TfrmInputMasukPendapatan, frmInputMasukPendapatan);
          frmInputMasukPendapatan.OnClose := frmMainMenu.ChildFormSingle;
          frmInputMasukPendapatan.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------KEMBALI_DP_PARIWISATA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KEMBALI_DP_PARIWISATA then begin
        KDMaster := Get_kembali_dp_pariwisata_master(Master.trans_seq);
        Tipe := LeftStr(KDMaster.Nomor,2);
        if Tipe = TRANS_KEMBALI_DP_PARIWISATA then begin
          Application.CreateForm(TfrmInputKembaliDpPariwisata, frmInputKembaliDpPariwisata);
          frmInputKembaliDpPariwisata.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKembaliDpPariwisata.Execute(Master.trans_seq, false);
        end;
      end;
//      //-------------------PEMBAYARAN_PARIWISATA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PEMBAYARAN_PARIWISATA then begin
        MPMaster := Get_bayar_pariwisata_master(Master.trans_seq);
        Tipe := LeftStr(MPMaster.Nomor,2);
        if Tipe = TRANS_PEMBAYARAN_PARIWISATA then begin
          Application.CreateForm(TfrmInputPembayaranPariwisata, frmInputPembayaranPariwisata);
          frmInputPembayaranPariwisata.OnClose := frmMainMenu.ChildFormSingle;
          frmInputPembayaranPariwisata.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------KOMISI_AGEN-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KOMISI_AGEN then begin
        KAMaster := Get_komisi_agen_master(Master.trans_seq);
        Tipe := LeftStr(KAMaster.Nomor,2);
        if Tipe = TRANS_KOMISI_AGEN then begin
          Application.CreateForm(TfrmInputKomisiAgen, frmInputKomisiAgen);
          frmInputKomisiAgen.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKomisiAgen.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------CLAIM_PARIWISATA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_CLAIM_PARIWISATA then begin
        CPMaster := Get_claim_pariwisata_master(Master.trans_seq);
        Tipe := LeftStr(CPMaster.Nomor,2);
        if Tipe = TRANS_CLAIM_PARIWISATA then begin
          Application.CreateForm(TfrmInputClaimPariwisata, frmInputClaimPariwisata);
          frmInputClaimPariwisata.OnClose := frmMainMenu.ChildFormSingle;
          frmInputClaimPariwisata.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------UANG_JALAN-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_UANG_JALAN then begin
        UJMaster := Get_uang_jalan_master(Master.trans_seq);
        Tipe := LeftStr(UJMaster.nomor,2);
        if Tipe = TRANS_UANG_JALAN then begin
          Application.CreateForm(TfrmInputUangJalan, frmInputUangJalan);
          frmInputUangJalan.OnClose := frmMainMenu.ChildFormSingle;
          frmInputUangJalan.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------MUTASI_DANA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_MUTASI_DANA then begin
        MDMaster := Get_mutasi_dana_master(Master.trans_seq);
        Tipe := LeftStr(MDMaster.nomor,2);
        if Tipe = TRANS_MUTASI_DANA then begin
          Application.CreateForm(TfrmInputMutasiDana, frmInputMutasiDana);
          frmInputMutasiDana.OnClose := frmMainMenu.ChildFormSingle;
          frmInputMutasiDana.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------PENGELUARAN_DANA_KE_CABANG-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENGELUARAN_DANA_CABANG then begin
        KCMaster := Get_keluar_dana_k_cbng_mst(Master.trans_seq);
        Tipe := LeftStr(KCMaster.nomor,2);
        if Tipe = TRANS_KELUAR_DANA_K_CBNG then begin
          Application.CreateForm(TfrmInputKeluarDanaCabang, frmInputKeluarDanaCabang);
          frmInputKeluarDanaCabang.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKeluarDanaCabang.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------PENERIMAAN_DANA_DARI_CABANG-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENERIMAAN_DANA_CABANG then begin
        TCMaster := Get_terima_dana_dr_cbng_mst(Master.trans_seq);
        Tipe := LeftStr(TCMaster.nomor,2);
        if Tipe = TRANS_terima_DANA_dr_CBNG then begin
          Application.CreateForm(TfrmInputTerimaDanaCabang, frmInputTerimaDanaCabang);
          frmInputTerimaDanaCabang.OnClose := frmMainMenu.ChildFormSingle;
          frmInputTerimaDanaCabang.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------KELUAR_DP_KE_SUPPLIER-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KELUAR_DP_SUPPLIER then begin
        KLMaster := Get_keluar_dp_master(Master.trans_seq);
        Tipe := LeftStr(KLMaster.nomor,2);
        if Tipe = TRANS_KELUAR_DP_SUPPLIER then begin
          Application.CreateForm(TfrmInputKeluarDPKeSupp, frmInputKeluarDPKeSupp);
          frmInputKeluarDPKeSupp.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKeluarDPKeSupp.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------KEMBALI_DP_DARI_SUPPLIER-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KEMBALI_DP_SUPPLIER then begin
        KMMaster := Get_kembali_dp_master(Master.trans_seq);
        Tipe := LeftStr(KMMaster.nomor,2);
        if Tipe = TRANS_KEMBALI_DP_SUPPLIER then begin
          Application.CreateForm(TfrmInputKeluarDPKeSupp, frmInputKembaliDPDariSupp);
          frmInputKembaliDPDariSupp.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKembaliDPDariSupp.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------PELUNASAN_HUTANG-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PELUNASAN_HUTANG then begin
        HSMaster := Get_bayar_hutang_master(Master.trans_seq);
        Tipe := LeftStr(HSMaster.nomor,2);
        if Tipe = TRANS_PELUNASAN_HUTANG then begin
          Application.CreateForm(TfrmInputPelunasanHutangSupp, frmInputPelunasanHutangSupp);
          frmInputPelunasanHutangSupp.OnClose := frmMainMenu.ChildFormSingle;
          frmInputPelunasanHutangSupp.Execute(Master.trans_seq, false);
        end;
      end;
      //---------------------PENGELUARAN_BIAYA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENGELUARAN_BIAYA then begin
        KBMaster := Get_keluar_biaya_master(Master.trans_seq);
        Tipe := LeftStr(KBMaster.nomor,2);
        if Tipe = TRANS_PENGELUARAN_BIAYA then begin
          Application.CreateForm(TfrmInputKeluarBiaya, frmInputKeluarBiaya);
          frmInputKeluarBiaya.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKeluarBiaya.Execute(Master.trans_seq, false);
        end;
      end;
      //-------------------PENERIMAAN_LAIN_LAIN-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENERIMAAN_LAIN_LAIN then begin
        TPMaster := Get_terima_pendapatan_master(Master.trans_seq);
        Tipe := LeftStr(TPMaster.Nomor,2);
        if Tipe = TRANS_PENERIMAAN_LAIN_LAIN then begin
          Application.CreateForm(TfrmInputMasukPendapatan, frmInputMasukPendapatan);
          frmInputMasukPendapatan.OnClose := frmMainMenu.ChildFormSingle;
          frmInputMasukPendapatan.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------PEMBELIAN_ASSET-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PEMBELIAN_ASSET then begin
        BAMaster := Get_beli_asset_master(Master.trans_seq);
        Tipe := LeftStr(BAMaster.Nomor,2);
        if Tipe = TRANS_PEMBELIAN_ASSET then begin
          Application.CreateForm(TfrmBeliAssetInput, frmBeliAssetInput);
          frmBeliAssetInput.OnClose := frmMainMenu.ChildFormSingle;
          frmBeliAssetInput.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------BAYAR_HUTANG_ASSET-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PELUNASAN_BELI_ASSET then begin
        BHMaster := Get_bayar_hutang_asset_master(Master.trans_seq);
        Tipe := LeftStr(BHMaster.Nomor,2);
        if Tipe = TRANS_PELUNASAN_BELI_ASSET then begin
          Application.CreateForm(TfrmInputBayarHutangAsset, frmInputBayarHutangAsset);
          frmInputBayarHutangAsset.OnClose := frmMainMenu.ChildFormSingle;
          frmInputBayarHutangAsset.Execute(Master.trans_seq, false);
        end;
      end;
     //------------------------JUAL_ASSET-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PENJUALAN_ASSET then begin
        JAMaster := Get_jual_asset_master(Master.trans_seq);
        Tipe := LeftStr(JAMaster.Nomor,2);
        if Tipe = TRANS_PENJUALAN_ASSET then begin
          Application.CreateForm(TfrmJualAssetInput, frmJualAssetInput);
          frmJualAssetInput.OnClose := frmMainMenu.ChildFormSingle;
          frmJualAssetInput.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------TERIMA_PIUTANG_ASSET-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PELUNASAN_PIUTANG_ASSET then begin
        TAMaster := Get_terima_piutang_asset_master(Master.trans_seq);
        Tipe := LeftStr(TAMaster.Nomor,2);
        if Tipe = TRANS_PELUNASAN_PIUTANG_ASSET then begin
          Application.CreateForm(TfrmInputTerimaPiutangAsset, frmInputTerimaPiutangAsset);
          frmInputTerimaPiutangAsset.OnClose := frmMainMenu.ChildFormSingle;
          frmInputTerimaPiutangAsset.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------TERIMA_PINJAMAN_DARI_PIHAK3-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PINJAMAN_DR_RELASI then begin
        PRMaster := Get_pinjaman_dr_relasi_master(Master.trans_seq);
        Tipe := LeftStr(PRMaster.Nomor,2);
        if Tipe = TRANS_PINJAMAN_DR_RELASI then begin
          Application.CreateForm(TfrmInputTerimaPinjamPihak3, frmInputTerimaPinjamPihak3);
          frmInputTerimaPinjamPihak3.OnClose := frmMainMenu.ChildFormSingle;
          frmInputTerimaPinjamPihak3.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------KEMBALI_PINJAMAN_DARI_PIHAK3-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_BAYAR_PINJAM_KE_RELASI then begin
        BRMaster := Get_bayar_pinjam_ke_relasi_master(Master.trans_seq);
        Tipe := LeftStr(BRMaster.Nomor,2);
        if Tipe = TRANS_KEMBALI_DR_RELASI then begin
          Application.CreateForm(TfrmInputKembaliPinjamanRelasi, frmInputKembaliPinjamanRelasi);
          frmInputKembaliPinjamanRelasi.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKembaliPinjamanRelasi.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------PINJAMAN_KE_RELASI-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_PINJAMAN_KE_RELASI  then begin
        JRMaster := Get_pinjaman_ke_relasi_master(Master.trans_seq);
        Tipe := LeftStr(JRMaster.Nomor,2);
        if Tipe = TRANS_PINJAMAN_KE_RELASI then begin
          Application.CreateForm(TfrmInputKeluarPinjamKeRelasi, frmInputKeluarPinjamKeRelasi);
          frmInputKeluarPinjamKeRelasi.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKeluarPinjamKeRelasi.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------TERIMA_KEMBALI_PINJAMAN_PIHAK3-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KEMBALI_DR_RELASI then begin
        KRMaster := Get_kembali_dr_relasi_master(Master.trans_seq);
        Tipe := LeftStr(KRMaster.Nomor,2);
        if Tipe = TRANS_KEMBALI_DR_RELASI then begin
          Application.CreateForm(TfrmInputTrmKblPinjamanRelasi, frmInputTrmKblPinjamanRelasi);
          frmInputTrmKblPinjamanRelasi.OnClose := frmMainMenu.ChildFormSingle;
          frmInputTrmKblPinjamanRelasi.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------KASBON_PEGAWAI-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_KASBON_PEGAWAI then begin
        KPMaster := Get_kasbon_pegawai_master(Master.trans_seq);
        Tipe := LeftStr(KPMaster.Nomor,2);
        if Tipe = TRANS_KASBON_PEGAWAI then begin
          Application.CreateForm(TfrmInputKasbonPegawai, frmInputKasbonPegawai);
          frmInputKasbonPegawai.OnClose := frmMainMenu.ChildFormSingle;
          frmInputKasbonPegawai.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------PELUNASAN_KASBON_PEGAWAI-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_LUNAS_KASBON_PEGAWAI  then begin
        LKMaster := Get_lunas_kasbon_pegawai_master(Master.trans_seq);
        Tipe := LeftStr(LKMaster.Nomor,2);
        if Tipe = TRANS_LUNAS_KASBON_PEGAWAI then begin
          Application.CreateForm(TfrmInputPelunasanKasbonPegawai, frmInputPelunasanKasbonPegawai);
          frmInputPelunasanKasbonPegawai.OnClose := frmMainMenu.ChildFormSingle;
          frmInputPelunasanKasbonPegawai.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------BAYAR_GAJI_PEGAWAI-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_BAYAR_GAJI_PEGAWAI then begin
        GPMaster := Get_GAJI_PEGAWAI_MASTER(Master.trans_seq);
        Tipe := LeftStr(GPMaster.Nomor,2);
        if Tipe = TRANS_BAYAR_GAJI_PEGAWAI then begin
          Application.CreateForm(TfrmInputBayarGajiPegawai, frmInputBayarGajiPegawai);
          frmInputBayarGajiPegawai.OnClose := frmMainMenu.ChildFormSingle;
          frmInputBayarGajiPegawai.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------SETOR_MODAL-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_SETOR_MODAL then begin
        SMMaster := Get_setor_modal_master(Master.trans_seq);
        Tipe := LeftStr(SMMaster.Nomor,2);
        if Tipe = TRANS_SETOR_MODAL then begin
          Application.CreateForm(TfrmInputSetoranModal, frmInputSetoranModal);
          frmInputSetoranModal.OnClose := frmMainMenu.ChildFormSingle;
          frmInputSetoranModal.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------AMBIL_LABA-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_AMBIL_LABA then begin
        PBMaster := Get_ambil_laba_master(Master.trans_seq);
        Tipe := LeftStr(PBMaster.Nomor,2);
        if Tipe = TRANS_AMBIL_LABA then begin
          Application.CreateForm(TfrmInputAmbilLaba, frmInputAmbilLaba);
          frmInputAmbilLaba.OnClose := frmMainMenu.ChildFormSingle;
          frmInputAmbilLaba.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------sewa_dibayar_dimuka-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_SEWA_DIBAYAR_DIMUKA then begin
        SWMaster := Get_sewa_dibayar_dimuka_master(Master.trans_seq);
        Tipe := LeftStr(SWMaster.Nomor,2);
        if Tipe = TRANS_SEWA_DIBAYAR_DIMUKA  then begin
          Application.CreateForm(TfrmInputSewaDibayarDimuka, frmInputSewaDibayarDimuka);
          frmInputSewaDibayarDimuka.OnClose := frmMainMenu.ChildFormSingle;
          frmInputSewaDibayarDimuka.Execute(Master.trans_seq, false);
        end;
      end;
     //-------------------Jurnal Umum-------------------------
      if Master.Tipe_Transaksi = TIPE_JURNAL_UMUM then begin
        JUMaster := Get_jurnal_master(Master.trans_seq);
        Tipe := LeftStr(JUMaster.Nomor,2);
        if Tipe = TRANS_JURNAL_UMUM  then begin
          Application.CreateForm(TfrmInputJurnalUmum, frmInputJurnalUmum);
          frmInputJurnalUmum.OnClose := frmMainMenu.ChildFormSingle;
          frmInputJurnalUmum.Execute(Master.trans_seq, false);
        end;
      end;
    end;
  end;
end;

procedure TfrmRekapJurnal.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then HAlign := tacenter
  else if acol in [ColNo, colNoAkun, colDebit, colKredit, ColSeq] then halign := taRightJustify;
  valign := vtacenter;
end;

procedure TfrmRekapJurnal.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if acol in [colKredit ,colDebit, ColNo, ColSeq] then begin
    isFLoat := true;
    if acol in [colKredit ,colDebit] then floatformat := '%.2n'
    else floatformat := '%.0f';
  end else isFLoat := False;
end;

procedure TfrmRekapJurnal.asgRekapGetFormat(Sender: TObject; ACol: Integer;
  var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if acol in [colDebit, colKredit] then AStyle := ssFinancial
  else if acol in [colTanggal] then Astyle := ssDate;
end;

procedure TfrmRekapJurnal.btnCariClick(Sender: TObject);
var Akun : TR_Master_Akun;
begin
  txtCari.Text:= '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqAkun := frmLOV.ExecuteAkun('',botrue,tipeakun);
  if SeqAKun <> 0 then begin
    Akun := Get_master_akun(SeqAkun);
    txtCari.Text := Akun.kode+' | '+Akun.nama;
  end else begin
    txtCari.Text:= '';
    SeqAkun := 0;
  end;
end;

procedure TfrmRekapJurnal.btnCetakClick(Sender: TObject);
begin
  if (not BisaPrint(AidMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Laporan Rekap Jurnal');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 2;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount + 2;
  asgRekap.ColumnSize.Stretch := True;
  ArrangeColSize;
end;

procedure TfrmRekapJurnal.btnEksporClick(Sender: TObject);
begin
  if  (not BisaEkspor(AidMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapJurnal.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 83);
end;

procedure TfrmRekapJurnal.btnLoadClick(Sender: TObject);
begin
  setgrid;
  loaddata;
end;

procedure TfrmRekapJurnal.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
end;

procedure TfrmRekapJurnal.chbSFClick(Sender: TObject);
begin                          
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapJurnal.cmbTipeAkunSelect(Sender: TObject);
begin
  case cmbTipeAkun.ItemIndex of
    0 : tipeakun := '';
    1 : tipeakun := TIPE_AKUN_AKTIVA;
    2 : tipeakun := TIPE_AKUN_PASIVA;
    3 : tipeakun := TIPE_AKUN_MODAL;
    4 : tipeakun := TIPE_AKUN_PENDAPATAN;
    5 : tipeakun := TIPE_AKUN_HPP;
    6 : tipeakun := TIPE_AKUN_BIAYA;
    7 : tipeAkun := TIPE_AKUN_BIAYA_LAIN_LAIN;
  end;
end;

procedure TfrmRekapJurnal.DetailPatchSaldo;
var PSMaster : TR_patch_saldo_master;
    tipe : string;
begin
  PSMaster := Get_patch_saldo_master(AsgRekap.Ints[ColSeq, asgRekap.Row]);
  tipe := LeftStr(PSMaster.nomor,3);
  if tipe = H_Patch_Saldo_Spare_part  then begin
    Application.CreateForm(TfrmInputPatchBarang, frmInputPatchBarang);
    frmInputPatchBarang.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchBarang.Execute(AsgRekap.Ints[ColSeq, asgRekap.Row], false);
  end else if tipe = H_Patch_Saldo_Kasbank then begin
    Application.CreateForm(TfrmInputPatchKasBank, frmInputPatchKasBank);
    frmInputPatchKasBank.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchKasBank.Execute(AsgRekap.Ints[ColSeq, asgRekap.Row], false);
  end else if tipe = H_Patch_Saldo_Asset then begin
    Application.CreateForm(TfrmInputPatchAsset, frmInputPatchAsset);
    frmInputPatchAsset.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchAsset.Execute(AsgRekap.Ints[ColSeq, asgRekap.Row], false);
  end else if tipe = H_Patch_Saldo_Kasbon_Pgw then begin
    Application.CreateForm(TfrmInputPatchKasbon, frmInputPatchKasbon);
    frmInputPatchKasbon.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchKasbon.Execute(AsgRekap.Ints[ColSeq, asgRekap.Row], false);
  end else if tipe = H_Patch_Saldo_Hutang then begin
    Application.CreateForm(TfrmInputPatchHutang, frmInputPatchHutang);
    frmInputPatchHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchHutang.Execute(AsgRekap.Ints[ColSeq, asgRekap.Row], false);
  end else if tipe = H_Patch_Saldo_AKUN then begin
    Application.CreateForm(TfrmInputPatchAkun, frmInputPatchAkun);
    frmInputPatchAkun.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchAkun.Execute(asgRekap.Ints[colSeq, asgRekap.Row]);
  end else if tipe = H_Patch_Saldo_Piutang then begin
    Application.CreateForm(TfrmInputPatchPiutang, frmInputPatchPiutang);
    frmInputPatchPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchPiutang.Execute(AsgRekap.Ints[ColSeq, asgRekap.Row], false);
  end;
end;

procedure TfrmRekapJurnal.Execute(AmenuId: integer);
var i : Integer;
begin
  AidMenu := AMenuId;
  if (not BisaPrint(aidMenu)) and (not BisaEkspor(AidMenu)) and (not BisaLihatRekap(aidMenu)) and (not BisaLihatLaporan(AidMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
//  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then asgrekap.ColWidths[ColSeq] := 0;
  ListTransaksi := TStringList.Create;
  ListTransaksi.Clear; cmbTransaksi.Items.Clear;
//  Get_List_Tipe_Transaksi_Jurnal(ListTransaksi); //+ christian 251017 diganti new karena ngilangin filter penjualan KS
  Get_List_Tipe_Transaksi_Jurnal_new(ListTransaksi);
  for i := 0 to ListTransaksi.Count-1 do begin
    cmbTransaksi.Items.Append(EkstrakString(ListTransaksi.ValueFromIndex[i], '#', 2));
  end;
  cmbTipeAkun.Clear;
  cmbTipeAkun.Items.Append('Semua');
  cmbTipeAkun.Items.Append(TIPE_AKUN_AKTIVA_TEXT);
  cmbTipeAkun.Items.Append(TIPE_AKUN_PASIVA_TEXT);
  cmbTipeAkun.Items.Append(TIPE_AKUN_MODAL_TEXT);
  cmbTipeAkun.Items.Append(TIPE_AKUN_PENDAPATAN_TEXT);
  cmbTipeAkun.Items.Append(TIPE_AKUN_HPP_TEXT);
  cmbTipeAkun.Items.Append(TIPE_AKUN_BIAYA_TEXT);
  cmbTipeAkun.Items.Append(TIPE_AKUN_BIAYA_LAIN_LAIN_TEXT);
  initform;
  setgrid;
//  loaddata;
  run(self);
end;

procedure TfrmRekapJurnal.FormShow(Sender: TObject);
begin
  execute(ACurrMenuId);
end;

procedure TfrmRekapJurnal.initform;
begin
  tipeAkun       := '';
  txtCari.Text   :='';
  dtpSampai.Date :=  EndOfTheMonth(ServerNow);
  dtpDari.Date   := StartOfTheMonth(ServerNow);
  chbSF.Checked  := false;
  cmbTipeAkun.ItemIndex  := 0;
  cmbTransaksi.ItemIndex := 0;
  asgRekap.SearchFooter.Visible := false;
  SeqAKun := 0;
end;

procedure TfrmRekapJurnal.LoadData;
var tipe : string; tipe_trans : integer;
begin
  Tipe := '';
  if cmbTipeAkun.ItemIndex = 0 then
    Tipe := ''
  else begin
    if cmbTipeAkun.Text = TIPE_AKUN_AKTIVA_TEXT then tipe   := TIPE_AKUN_AKTIVA
    else if cmbTipeAkun.Text = TIPE_AKUN_PASIVA_TEXT then tipe := TIPE_AKUN_PASIVA
    else if cmbTipeAkun.Text = TIPE_AKUN_MODAL_TEXT then tipe := TIPE_AKUN_MODAL
    else if cmbTipeAkun.Text = TIPE_AKUN_PENDAPATAN_TEXT then tipe := TIPE_AKUN_PENDAPATAN
    else if cmbTipeAkun.Text = TIPE_AKUN_HPP_TEXT then tipe := TIPE_AKUN_HPP
    else if cmbTipeAkun.Text = TIPE_AKUN_BIAYA_TEXT then tipe := TIPE_AKUN_BIAYA
    else if cmbTipeAkun.Text = TIPE_AKUN_BIAYA_LAIN_LAIN_TEXT then tipe := TIPE_AKUN_BIAYA_LAIN_LAIN;
  end;
  if cmbTransaksi.ItemIndex <= 0 then
    tipe_trans := 0
  else begin
    tipe_trans := StrToIntDef(EkstrakString(ListTransaksi.Values[inttostr(cmbTransaksi.ItemIndex)], '#', 1), 0);
  end;
  
  asgRekap.RowCount := 3;
  asgRekap.ClearNormalCells;
//  Load_lap_rekap_Journal(asgRekap, dtpDari.Date, dtpSampai.Date, tipe, SeqAkun, Gauge, tipe_trans);
  Load_lap_rekap_Journal_new(asgRekap, dtpDari.Date, dtpSampai.Date, tipe, SeqAkun, Gauge, tipe_trans);
  ArrangeColSize;
end;

procedure TfrmRekapJurnal.SetGrid;
begin
  asgrekap.clearnormalcells;
  asgrekap.ColCount := 12;
  asgrekap.rowcount := 3;
  asgrekap.fixedrows := 1;
  asgrekap.fixedcols := 1;
  arrangecolsize;
end;

//procedure TfrmRekapJurnal.updateSetoran;
//var arr : AR_setoran_master;
//    arSetoran : AR_setoran;
//    ArDetail : AR_setting_jurnal;
//    periode : TR_FilterPeriode;
//    jurnaldet : TR_jurnal_detail;
//    dtmst : TR_master_jurusan;
//    akunMst : TR_master_akun;
//    i,j,akunJurSeq : integer;
//    value : string;
//begin
//   get_list_jurnal_not_balance();



//  periode.FPeriodeAwal      := dtpDari.Date;
//  periode.FPeriodeAkhir     := dtpSampai.Date;
//  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
//  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
//
//  arr := Get_Arr_setoran_master(periode);
//  for i := 0 to length(arr)-1 do begin
//    Delete_jurnal_detail(arr[i].jurnal_seq);
//    if (arr[i].tipe = SETORAN_TIPE_PIUTANG_DAN_KS) then begin
//      arSetoran := Get_Arr_setoran(Arr[i].seq);
//      for j := 1 to length(arSetoran)-1 do begin
//        if (arSetoran[i].jurusan_seq <> 0) and (arSetoran[i].jumlah_setoran <> 0) then begin
//           //(AsgKS.Ints[colseqJurusanPK,  i] <> 0) and (AsgKS.Floats[ColSetoranPk,  i] <> 0)  then begin
//          dtmst := Get_master_jurusan(arSetoran[i].jurusan_seq);
//          akunJurSeq := dtMst.akun_seq;
////          if asgAkun.Ints[ColSeqAkun, asgAkun.RowCount-2] <> 0 then asgAkun.AddRow;
////          vRow := asgAkun.RowCount-2;
//          jurnaldet.master_seq     := arr[i].jurnal_seq;
//          jurnaldet.akun_seq       := akunJurSeq;
//          jurnaldet.db_cr_tipe     := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 4);
//          jurnaldet.db_cr_tipe_mst := ;
//          jurnaldet.jumlah         := arSetoran[i].jumlah_setoran;
//          jurnaldet.keterangan     := '';
//          jurnaldet.tipe_akun      := ;
//
////          asgAkun.Cells[ColNoAkun,   vRow] := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 2);
////          asgAkun.Cells[ColNamaAcc,  vRow] := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 3);
////          asgAkun.Ints[ColSeqAkun,   vRow] := akunJurSeq;
////          asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 4);
////           if (Tipe = SETORAN_TIPE_PIUTANG_DAN_KS) then//(Tipe = SETORAN_TIPE_PIUTANG_SETORAN) or (Tipe = SETORAN_TIPE_KS) then
////             asgAkun.Floats[ColCR,   vRow] := AsgKS.Floats[ColSetoranPk, i];
//        end;
//      end;
//    end else if (arr[i].tipe = SETORAN_TIPE_DANA_KECELAKAAN) or (arr[i].tipe = SETORAN_TIPE_SETORAN) then begin
//      if akunJurSeq <> 0 then begin
////        if asgAkun.Ints[ColSeqAkun, asgAkun.RowCount-2] <> 0 then asgAkun.AddRow;
////        vRow := asgAkun.RowCount-2;
//        jurnaldet.master_seq     := arr[i].jurnal_seq;
//        jurnaldet.akun_seq       := akunJurSeq;
//        jurnaldet.db_cr_tipe     := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 4);
//        jurnaldet.db_cr_tipe_mst := '';
//        jurnaldet.keterangan     := '';
//        jurnaldet.tipe_akun      := '';
//        if (arr[i].tipe = SETORAN_TIPE_DANA_KECELAKAAN) then
//           jurnaldet.jumlah      := arSetoran[i].jumlah_bayar
//        else if (arr[i].tipe = SETORAN_TIPE_SETORAN) then
//           jurnaldet.jumlah      := arSetoran[i].jumlah_setoran;;
//
////        asgAkun.Cells[ColNoAkun,   vRow] := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 2);
////        asgAkun.Cells[ColNamaAcc,  vRow] := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 3);
////        asgAkun.Ints[ColSeqAkun,   vRow] := akunJurSeq;
////        asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(ListAkun.Values[inttostr(akunJurSeq)], '#', 4);
////        if (Tipe = SETORAN_TIPE_DANA_KECELAKAAN) then
////           asgAkun.Floats[ColCR,   vRow] := txtBayar.FloatValue
////        else if (Tipe = SETORAN_TIPE_SETORAN) then
////           asgAkun.Floats[ColCR,   vRow] := txtSetoran.FloatValue;
//      end;
//    end;
//
//    if ((arSetoran[i].jumlah_setoran - arSetoran[i].jumlah_bayar) > 0) or
//       ((arSetoran[i].jumlah) > 0) then begin
////      if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
////      vRow := asgAkun.RowCount-2;
//      jurnaldet.master_seq     := arr[i].jurnal_seq;
//      jurnaldet.akun_seq       := 13;
//      jurnaldet.db_cr_tipe     := EkstrakString(ListAkun.Values[inttostr(13)], '#', 4);
//      jurnaldet.db_cr_tipe_mst := '';
//      jurnaldet.keterangan     := '';
//      jurnaldet.tipe_akun      := '';
//      if (arSetoran[i].Tipe = SETORAN_TIPE_SETORAN) then
//        jurnaldet.jumlah       := arSetoran[i].jumlah_setoran  - arSetoran[i].jumlah_bayar
//      else jurnaldet.jumlah    := (arSetoran[i].jumlah_setoran+arSetoran[i].jumlah) - arSetoran[i].jumlah_bayar;
////
////      asgAkun.Cells[ColNoAkun,   vRow] := EkstrakString(ListAkun.Values[inttostr(13)], '#', 2);
////      asgAkun.Cells[ColNamaAcc,  vRow] := EkstrakString(ListAkun.Values[inttostr(13)], '#', 3);
////      asgAkun.Ints[ColSeqAkun,   vRow] := 13;
////      asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(ListAkun.Values[inttostr(13)], '#', 4);
////      asgAkun.floats[ColCR,       vRow] := 0;
////      if (Tipe = SETORAN_TIPE_SETORAN) then
////        asgAkun.Floats[ColDB,    vRow] := txtSetoran.FloatValue - txtBayar.FloatValue
////      else
////        asgAkun.Floats[ColDB,    vRow] := txtTotal.FloatValue - txtBayar.FloatValue;
//    end;
//
//    if ((arSetoran[i].jumlah_setoran - arSetoran[i].jumlah_bayar) > 0) or
//       ((arSetoran[i].jumlah) > 0) then begin
//      akunMst := Get_master_akun(0,'6500');
////      if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
////      vRow := asgAkun.RowCount-2;
//      jurnaldet.master_seq     := arr[i].jurnal_seq;
//      jurnaldet.akun_seq       := akunMst.seq;
//      jurnaldet.db_cr_tipe     := akunMst.db_cr_tipe;
//      jurnaldet.db_cr_tipe_mst := ;
//      jurnaldet.jumlah         := arSetoran[i].jumlah_setoran;
//      jurnaldet.keterangan     := '';
//      jurnaldet.tipe_akun      := ;
//
////      asgAkun.Cells[ColNoAkun,   vRow] := akunMst.kode;
////      asgAkun.Cells[ColNamaAcc,  vRow] := akunMst.nama;
////      asgAkun.Ints[ColSeqAkun,   vRow] := akunMst.seq;
////      asgAkun.Cells[ColDBCRType, vRow] := akunMst.tipe;
////      asgAkun.Cells[ColCR,      vRow] := AsgKS.Cells[ColHutangPK, asgKS.RowCount-1];
////      asgAkun.Floats[ColDB,      vRow] := 0;
////      asgAkun.Cells[ColDBCRSet,  vRow] := akunMst.db_cr_tipe;
//    end;
//
//    for i := 0 to ListSettingJurnal.Count-1 do begin
////      if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
////      vROw := asgAkun.RowCount-2;
//      value := ListSettingJurnal.Values[ListSettingJurnal.Names[i]];
//      if EkstrakString(value, '#', 7) = DB_CR_TYPE_CREDIT then
//        row_cr := vRow;
//      asgAkun.Cells[ColNoAkun, vRow]     := EkstrakString(value, '#', 1);
//      asgAkun.Cells[ColNamaAcc, vRow]    := EkstrakString(value, '#', 2);
//      asgAkun.Floats[ColDB, vRow]        := StrToFloatDef(EkstrakString(value, '#', 4),0);
//      asgAkun.Floats[ColCR, vRow]        := StrToFloatDef(EkstrakString(value, '#', 5),0);
//      asgAkun.Ints[ColSeqAkun, vRow]     := StrToIntDef(ListSettingJurnal.Names[i],0);
//      asgAkun.Cells[ColDBCRType, vRow]   := EkstrakString(value, '#', 3);
//      asgAkun.Cells[ColDBCRSet, vRow]    := EkstrakString(value, '#', 7);
//      asgAkun.Cells[ColKeterangan, vRow] := EkstrakString(value, '#', 6);
//    end;
//
//    for i := 1 to asgDetail.RowCount-2 do begin
//      if asgDetail.Ints[ColSeqAkunKasBank, i] <> 0 then begin
//
//        IsAdaData := False;
//        for j := 1 to asgAkun.RowCount-2 do begin
//          if asgAkun.Ints[ColSeqAkun, j] = asgDetail.Ints[ColSeqAkunKasBank, i] then begin
//            vRow := j;
//            IsAdaData := True;
//            Break;
//          end;
//        end;
//        if IsAdaData = False then begin
//          if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
//          vROw := asgAkun.RowCount-2;
//        end;
//        asgAkun.Cells[ColNoAkun, vRow] := EkstrakString(ListAkun.Values[asgDetail.Cells[ColSeqAkunKasBank, i]], '#', 2);
//        asgAkun.Cells[ColNamaAcc, vRow] := EkstrakString(ListAkun.Values[asgDetail.Cells[ColSeqAkunKasBank, i]], '#', 3);
//
//        if (asgAkun.Floats[ColDB, vRow] <> 0) or (asgAkun.Floats[ColCR, vRow] <> 0) then begin
//          if asgAkun.Floats[ColDB, vRow] <> 0 then
//            asgAkun.Floats[ColDB, vRow] := asgAkun.Floats[ColDB, vRow] + asgDetail.Floats[ColJml, i]
//          else begin
//            totakhir := asgDetail.Floats[ColJml, i] + asgAkun.Floats[ColDB, vRow];
//            if totakhir < 0 then begin
//              asgAkun.Floats[ColDB, vRow] := abs(totakhir);
//              asgAkun.Cells[ColCR, vRow] := '';
//            end else begin
//              asgAkun.Floats[ColCR, vRow] := totakhir;
//              asgAkun.Cells[ColDB, vRow] := '';
//            end;
//          end;
//        end else begin
//          asgAkun.Floats[ColDB, vRow] := asgDetail.Floats[ColJml, i];
//          asgAkun.Floats[ColCR, vRow] := 0;
//        end;
//        asgAkun.Ints[ColSeqAkun, vRow] := asgDetail.Ints[ColSeqAkunKasBank, i];
//        asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(ListAkun.Values[asgDetail.Cells[ColSeqAkunKasBank, i]], '#', 4);
//        asgAkun.Ints[ColRowDet, vRow] := i;
//      end;
//    end;
//
//    for i := 1 to AsgJaminan.RowCount-2 do begin
//      if AsgJaminan.Ints[ColSeqAkunJM, i] <> 0 then begin
//
//        IsAdaData := False;
//        for j := 1 to asgAkun.RowCount-2 do begin
//          if asgAkun.Ints[ColSeqAkun, j] = AsgJaminan.Ints[ColSeqAkunJM, i] then begin
//            vRow := j;
//            IsAdaData := True;
//            Break;
//          end;
//        end;
//        if IsAdaData = False then begin
//          if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
//          vROw := asgAkun.RowCount-2;
//        end;
//        asgAkun.Cells[ColNoAkun, vRow] := EkstrakString(ListAkun.Values[AsgJaminan.Cells[ColSeqAkunJM, i]], '#', 2);
//        asgAkun.Cells[ColNamaAcc, vRow] := EkstrakString(ListAkun.Values[AsgJaminan.Cells[ColSeqAkunJM, i]], '#', 3);
//
//        if (asgAkun.Floats[ColDB, vRow] <> 0) or (asgAkun.Floats[ColCR, vRow] <> 0) then begin
//          if asgAkun.Floats[ColDB, vRow] <> 0 then
//            asgAkun.Floats[ColDB, vRow] := asgAkun.Floats[ColDB, vRow] + AsgJaminan.Floats[ColJmlBayarJM, i]
//          else begin
//            totakhir := AsgJaminan.Floats[ColJmlBayarJM, i] + asgAkun.Floats[ColDB, vRow];
//            if totakhir < 0 then begin
//              asgAkun.Floats[ColDB, vRow] := abs(totakhir);
//              asgAkun.Cells[ColCR, vRow] := '';
//            end else begin
//              asgAkun.Floats[ColCR, vRow] := totakhir;
//              asgAkun.Cells[ColDB, vRow] := '';
//            end;
//          end;
//        end else begin
//          asgAkun.Floats[ColDB, vRow] := AsgJaminan.Floats[ColJmlBayarJM, i];
//          asgAkun.Floats[ColCR, vRow] := 0;
//        end;
//        asgAkun.Ints[ColSeqAkun,   vRow] := AsgJaminan.Ints[ColSeqAkunJM, i];
//        asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(ListAkun.Values[AsgJaminan.Cells[ColSeqAkunJM, i]], '#', 4);
//        asgAkun.Ints[ColRowDet,    vRow] := i;
//      end;
//    end;
//  end;
//end;

procedure TfrmRekapJurnal.updateUangJalan;
var arr : AR_Uang_jalan_master;
    periode : TR_FilterPeriode;
    ARJurnalDet : AR_jurnal_detail;
    saldoAkun : TR_saldo_akun;
    i ,j:integer;
    ListNotbalance : TStringList;
    seqJurnal,transSeq : integer;
    jurnal : TR_jurnal_master;
    JurnalDet : TR_jurnal_detail;
    akunMst : TR_master_akun;
    order : TR_order_rombongan_master;
begin
  myConnection.BeginSQL;
  try
    ListNotbalance := TStringList.Create;
    get_list_jurnal_not_balance(ListNotbalance,TIPE_JURNAL_UANG_JALAN);
    for i := 0 to ListNotbalance.Count - 1 do begin
      seqJurnal := StrToInt(ListNotbalance.Names[i]);
      transSeq := StrToInt(ListNotbalance.ValueFromIndex[i]);
      ARJurnalDet := Get_Arr_jurnal_detail(seqJurnal);
      for j := 0 to length(ARJurnalDet)-1 do begin
        SaldoAkun.akun_seq        := ARJurnalDet[j].akun_seq;
        SaldoAkun.tanggal         := Get_jurnal_master(seqJurnal).tanggal;
        SaldoAkun.jumlah          := ARJurnalDet[j].jumlah;
        SaldoAkun.qty             := 0;
        SaldoAkun.harga           := 0;
        if ((EkstrakString(ListAkunGlobal.Values[inttostr( ARJurnalDet[j].akun_seq)], '#', 8) = DB_CR_TYPE_DEBIT) and (ARJurnalDet[i].db_cr_tipe = DB_CR_TYPE_DEBIT))
            or ((EkstrakString(ListAkunGlobal.Values[inttostr( ARJurnalDet[j].akun_seq)], '#', 8) = DB_CR_TYPE_CREDIT) and (ARJurnalDet[i].db_cr_tipe = DB_CR_TYPE_CREDIT) ) then
            Save_Saldo_Akun(SaldoAkun, False)
        else Save_Saldo_Akun(SaldoAkun, True);
      end;
      Set_Jurnal_transaksi(TIPE_JURNAL_UANG_JALAN,transSeq,0,0);
      delete_jurnal_master(seqJurnal);
      delete_jurnal_detail(seqJurnal);
    end;
    periode.FPeriodeAwal      := 0;
    periode.FPeriodeAkhir     := 0;
    periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
    periode.FOprPeriodeAkhir  := soLessThanEqualsTo;

    arr := Get_Arr_uang_jalan_master(periode,' and jurnal_seq = 0 ');
    JurnalDet.seq := CreateNewSeq('seq','jurnal_detail');
    for i := 0 to Length(arr)-1 do begin
      order := Get_order_rombongan_master(Arr[i].order_seq);
       //save jurnal mst
      jurnal.tanggal        := arr[i].tanggal;
      jurnal.jumlah         := arr[i].total + order.total;
      jurnal.tipe_transaksi := TIPE_JURNAL_UANG_JALAN;
      jurnal.trans_seq      := arr[i].seq;
      jurnal.keterangan     := '';
      jurnal.user_id        := arr[i].user_id;
      jurnal.tgl_input      := arr[i].tgl_input;

      SeqJurnal := Save_jurnal_master(jurnal);
      Set_Jurnal_transaksi(TIPE_JURNAL_UANG_JALAN,arr[i].seq,SeqJurnal,0);

      if arr[i].total <> 0 then begin
        akunMst := Get_master_akun(0,'7330');
        JurnalDet.master_seq     := SeqJurnal;
        JurnalDet.akun_seq       := akunMst.seq;
        JurnalDet.db_cr_tipe     := akunMst.db_cr_tipe;
        JurnalDet.db_cr_tipe_mst := '';
        JurnalDet.jumlah         := arr[i].total;
        JurnalDet.keterangan     := '';
        JurnalDet.tipe_akun      := akunMst.tipe;
        Save_jurnal_detail(JurnalDet);

        SaldoAkun.akun_seq       := akunMst.seq;
        SaldoAkun.tanggal        := jurnal.tanggal;
        SaldoAkun.jumlah         := JurnalDet.jumlah;
        SaldoAkun.qty            := 0;
        SaldoAkun.harga          := 0;
        if (JurnalDet.db_cr_tipe  = DB_CR_TYPE_DEBIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_DEBIT)
            or (JurnalDet.db_cr_tipe  = DB_CR_TYPE_CREDIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_CREDIT) then
            Save_Saldo_Akun(SaldoAkun, True)
        else Save_Saldo_Akun(SaldoAkun, False);
        JurnalDet.seq := JurnalDet.seq + 1;

        akunMst := Get_master_akun(0,'6200');
        JurnalDet.master_seq     := SeqJurnal;
        JurnalDet.akun_seq       := akunMst.seq;
        JurnalDet.db_cr_tipe     := akunMst.db_cr_tipe;
        JurnalDet.db_cr_tipe_mst := '';
        JurnalDet.jumlah         := order.total;
        JurnalDet.keterangan     := '';
        JurnalDet.tipe_akun      := akunMst.tipe;
        Save_jurnal_detail(JurnalDet);

        SaldoAkun.akun_seq       := akunMst.seq;
        SaldoAkun.tanggal        := jurnal.tanggal;
        SaldoAkun.jumlah         := JurnalDet.jumlah;
        SaldoAkun.qty            := 0;
        SaldoAkun.harga          := 0;
        if (JurnalDet.db_cr_tipe  = DB_CR_TYPE_DEBIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_DEBIT)
            or (JurnalDet.db_cr_tipe  = DB_CR_TYPE_CREDIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_CREDIT) then
            Save_Saldo_Akun(SaldoAkun, True)
        else Save_Saldo_Akun(SaldoAkun, False);
        JurnalDet.seq := JurnalDet.seq + 1;

        akunMst := Get_master_akun(0,'1200');
        JurnalDet.master_seq     := SeqJurnal;
        JurnalDet.akun_seq       := akunMst.seq;
        JurnalDet.db_cr_tipe     := akunMst.db_cr_tipe;
        JurnalDet.db_cr_tipe_mst := '';
        JurnalDet.jumlah         := order.total;
        JurnalDet.keterangan     := '';
        JurnalDet.tipe_akun      := akunMst.tipe;
        Save_jurnal_detail(JurnalDet);

        SaldoAkun.akun_seq       := akunMst.seq;
        SaldoAkun.tanggal        := jurnal.tanggal;
        SaldoAkun.jumlah         := JurnalDet.jumlah;
        SaldoAkun.qty            := 0;
        SaldoAkun.harga          := 0;
        if (JurnalDet.db_cr_tipe  = DB_CR_TYPE_DEBIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_DEBIT)
            or (JurnalDet.db_cr_tipe  = DB_CR_TYPE_CREDIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_CREDIT) then
            Save_Saldo_Akun(SaldoAkun, True)
        else Save_Saldo_Akun(SaldoAkun, False);

        JurnalDet.seq := JurnalDet.seq + 1;

        akunMst := Get_master_akun(0,'1100');
        JurnalDet.master_seq     := SeqJurnal;
        JurnalDet.akun_seq       := akunMst.seq;
        JurnalDet.db_cr_tipe     := 'C';
        JurnalDet.db_cr_tipe_mst := '';
        JurnalDet.jumlah         := arr[i].total;
        JurnalDet.keterangan     := '';
        JurnalDet.tipe_akun      := akunMst.tipe;
        Save_jurnal_detail(JurnalDet);

        SaldoAkun.akun_seq       := akunMst.seq;
        SaldoAkun.tanggal        := jurnal.tanggal;
        SaldoAkun.jumlah         := JurnalDet.jumlah;
        SaldoAkun.qty            := 0;
        SaldoAkun.harga          := 0;
        if (JurnalDet.db_cr_tipe  = DB_CR_TYPE_DEBIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_DEBIT)
            or (JurnalDet.db_cr_tipe  = DB_CR_TYPE_CREDIT) and (JurnalDet.db_cr_tipe = DB_CR_TYPE_CREDIT) then
            Save_Saldo_Akun(SaldoAkun, True)
        else Save_Saldo_Akun(SaldoAkun, False);
        JurnalDet.seq := JurnalDet.seq + 1;
      end;
    end;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
  end;


//  periode.FPeriodeAwal      := dtpDari.Date;
//  periode.FPeriodeAkhir     := dtpSampai.Date;
//  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
//  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
//  ListSettingJurnal := TStringList.Create;
//  Get_List_setting_jurnal(ListSettingJurnal);
//
//  arr := Get_Arr_uang_jalan_master(periode,'');
//  for i := 0 to Length(arr)-1 do begin
//    Delete_jurnal_detail(arr[i].jurnal_seq);
//
//
//    Get_Arr_setting_jurnal(ArDetail, TRANS_UANG_JALAN, ifthen(arr[i].seq <> 0, ' and akun_seq in(select akun_seq from jurnal_detail where master_seq = '+FormatSQLNumber(Arr[i].jurnal_seq)+')'));
//    for i := 0 to Length(ArDetail) -1 do begin
//      value := ListSettingJurnal.Values[ListSettingJurnal.Names[i]];
//      ARJurnalDet.master_seq := arr[i].jurnal_seq;
//      ARJurnalDet.akun_seq := EkstrakString(value, '#', 1);
//      ARJurnalDet.db_cr_tipe := EkstrakString(value, '#', 3);
//      ARJurnalDet.db_cr_tipe_mst :=
//      ARJurnalDet.jumlah :=
//      ARJurnalDet.keterangan := EkstrakString(value, '#', 6);
//      ARJurnalDet.tipe_akun :=
//
//      asgAkun.Cells[ColNamaAcc, vRow] := EkstrakString(value, '#', 2);
//      asgAkun.Floats[ColDB, vRow] := StrToFloatDef(EkstrakString(value, '#', 4),0);
//      asgAkun.Floats[ColCR, vRow] := StrToFloatDef(EkstrakString(value, '#', 5),0);
//      asgAkun.Ints[ColSeqAkun, vRow] := StrToIntDef(ListSettingJurnal.Names[i],0);
//      asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(value, '#', 3);
//      asgAkun.Cells[ColKeterangan, vRow] := EkstrakString(value, '#', 6);
//      asgAkun.Cells[ColDBCRSet, vRow] := EkstrakString(value, '#', 7);
//    end;
//
//      akunMst := Get_master_akun(0,'7330');
//      if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
//      vRow := asgAkun.RowCount-2;
//      asgAkun.Cells[ColNoAkun,   vRow] := akunMst.kode;
//      asgAkun.Cells[ColNamaAcc,  vRow] := akunMst.nama;
//      asgAkun.Ints[ColSeqAkun,   vRow] := akunMst.seq;
//      asgAkun.Cells[ColDBCRType, vRow] := akunMst.tipe;
//      asgAkun.floats[ColCR,      vRow] := 0;
//      asgAkun.Floats[ColDB,      vRow] := asgAlatBayar.Floats[ColJml,1];
//      asgAkun.Cells[ColDBCRSet,  vRow] := akunMst.db_cr_tipe;
//
//
//    order := Get_order_rombongan_master(SeqOrder);
//
//    if asgAlatBayar.Floats[ColJml,1] <> 0 then begin
//      akunMst := Get_master_akun(0,'6200');
//      if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
//      vRow := asgAkun.RowCount-2;
//      asgAkun.Cells[ColNoAkun,   vRow] := akunMst.kode;
//      asgAkun.Cells[ColNamaAcc,  vRow] := akunMst.nama;
//      asgAkun.Ints[ColSeqAkun,   vRow] := akunMst.seq;
//      asgAkun.Cells[ColDBCRType, vRow] := akunMst.tipe;
//      asgAkun.floats[ColCR,      vRow] := order.total;
//      asgAkun.Floats[ColDB,      vRow] := 0;
//      asgAkun.Cells[ColDBCRSet,  vRow] := akunMst.db_cr_tipe;
//    end;
//
//    if asgAlatBayar.Floats[ColJml,1] <> 0 then begin
//      akunMst := Get_master_akun(0,'1200');
//      if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
//      vRow := asgAkun.RowCount-2;
//      asgAkun.Cells[ColNoAkun,   vRow] := akunMst.kode;
//      asgAkun.Cells[ColNamaAcc,  vRow] := akunMst.nama;
//      asgAkun.Ints[ColSeqAkun,   vRow] := akunMst.seq;
//      asgAkun.Cells[ColDBCRType, vRow] := akunMst.tipe;
//      asgAkun.floats[ColCR,      vRow] := 0;
//      asgAkun.Floats[ColDB,      vRow] := order.total;
//      asgAkun.Cells[ColDBCRSet,  vRow] := akunMst.db_cr_tipe;
//    end;
//
//    for i := 1 to asgAlatBayar.RowCount-2 do begin
//      if asgAlatBayar.Ints[ColSeqAkunKasBank, i] <> 0 then begin
//        IsAdaData := False;
//        for j := 1 to asgAkun.RowCount-2 do begin
//          if asgAkun.Ints[ColSeqAkun, j] = asgAlatBayar.Ints[ColSeqAkunKasBank, i] then begin
//            vRow := j;
//            IsAdaData := True;
//            Break;
//          end;
//        end;
//        if IsAdaData = False then begin
//          if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
//          vROw := asgAkun.RowCount-2;
//        end;
////        if asgAkun.Ints[ColSeqAkun, 1] <> 0 then asgAkun.AddRow;
////        vROw := asgAkun.RowCount-2;
//        asgAkun.Cells[ColNoAkun, vRow] := EkstrakString(ListAkunGlobal.Values[asgAlatBayar.Cells[ColSeqAkunKasBank, i]], '#', 1);
//        asgAkun.Cells[ColNamaAcc, vRow] := EkstrakString(ListAkunGlobal.Values[asgAlatBayar.Cells[ColSeqAkunKasBank, i]], '#', 2);
//        if (asgAkun.Floats[ColDB, vRow] <> 0) or (asgAkun.Floats[ColCR, vRow] <> 0) then begin
//          if asgAkun.Floats[ColCR, vRow] <> 0 then
//            asgAkun.Floats[ColCR, vRow] := asgAkun.Floats[ColCR, vRow] + asgAlatBayar.Floats[ColJml, i]
//          else begin
//            totakhir := asgAlatBayar.Floats[ColJml, i] - asgAkun.Floats[ColDB, vRow];
////            if totakhir < 0 then begin
////              asgAkun.Floats[ColDB, vRow] := abs(totakhir);
////              asgAkun.Cells[ColCR, vRow] := '';
////            end else begin
////              asgAkun.Floats[ColCR, vRow] := totakhir;
////              asgAkun.Cells[ColDB, vRow] := '';
////            end;
//            // Oggi 311016
//            if totakhir < 0 then begin
//              asgAkun.Floats[ColCR, vRow] := totakhir;
//              asgAkun.Cells[ColDB, vRow] := '';
//            end else begin
//              asgAkun.Floats[ColDB, vRow] := totakhir;
//              asgAkun.Cells[ColCR, vRow] := '';
//            end;
//          end;
//        end else begin
//          asgAkun.Cells[ColDB, vRow] := '';
//          asgAkun.Floats[ColCR, vRow] := asgAlatBayar.Floats[ColJml, i];
//        end;
//        if vrow > 1 then asgAkun.Cells[ColKeterangan, vRow] := asgAlatBayar.Cells[colKetDetJurnal,vrow - 1] ;
//        asgAkun.Ints[ColSeqAkun, vRow] := asgAlatBayar.Ints[ColSeqAkunKasBank, i];
//        asgAkun.Cells[ColDBCRType, vRow] := EkstrakString(ListAkunGlobal.Values[asgAlatBayar.Cells[ColSeqAkunKasBank, i]], '#', 8);
//      end;
//    end;
////    if row_db <> 0 then asgAkun.Floats[ColDB, row_db] := asgAlatBayar.ColumnSum(ColJml, 1, asgAlatBayar.RowCount-2);
//    if row_db <> 0 then asgAkun.Floats[ColDB, row_db] := asgArmada.ColumnSum(ColJumlah_A, 1, asgArmada.RowCount-2);
//  end;
end;

end.
