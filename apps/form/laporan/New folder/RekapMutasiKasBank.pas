unit RekapMutasiKasBank;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, Gauges, ComCtrls,
  StdCtrls, AdvGlowButton, AdvPanel, ExtCtrls, AdvOfficePager;

type
  TfrmRekapMutasiKasBank = class(TForm)
    advOfPager: TAdvOfficePager;
    pgcKas: TAdvOfficePage;
    pnlFilterKas: TPanel;
    PnlStatus: TAdvPanel;
    cmbKas: TComboBox;
    btnLoadKas: TAdvGlowButton;
    btnResetKas: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label1: TLabel;
    dtpAkhirKas: TDateTimePicker;
    dtpAwalKas: TDateTimePicker;
    Panel3: TPanel;
    GaugeKas: TGauge;
    btnFilterKas: TAdvGlowButton;
    btnEksporKas: TAdvGlowButton;
    btnCetakKas: TAdvGlowButton;
    asgRekapKas: TAdvStringGrid;
    chbSFKas: TAdvOfficeCheckBox;
    pgcBank: TAdvOfficePage;
    Panel4: TPanel;
    GaugeBank: TGauge;
    btnFilterBank: TAdvGlowButton;
    btnEksporBank: TAdvGlowButton;
    btnCetakBank: TAdvGlowButton;
    asgRekapBank: TAdvStringGrid;
    chbSFBank: TAdvOfficeCheckBox;
    pnlFilterBank: TPanel;
    AdvPanel1: TAdvPanel;
    cmbBank: TComboBox;
    btnLoadBank: TAdvGlowButton;
    btnResetBank: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    Label2: TLabel;
    dtpAkhirBank: TDateTimePicker;
    dtpAwalBank: TDateTimePicker;
    btnSinkronKas: TAdvGlowButton;
    AdvGlowButton1: TAdvGlowButton;
    procedure btnFilterKasClick(Sender: TObject);
    procedure btnEksporKasClick(Sender: TObject);
    procedure btnCetakKasClick(Sender: TObject);
    procedure chbSFKasClick(Sender: TObject);
    procedure btnLoadKasClick(Sender: TObject);
    procedure btnResetKasClick(Sender: TObject);
    procedure btnFilterBankClick(Sender: TObject);
    procedure btnEksporBankClick(Sender: TObject);
    procedure btnCetakBankClick(Sender: TObject);
    procedure btnLoadBankClick(Sender: TObject);
    procedure btnResetBankClick(Sender: TObject);
    procedure asgRekapKasClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapKasContractNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgRekapKasExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgRekapKasGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapKasGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapKasMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapKasMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapBankCanClickCell(Sender: TObject; ARow, ACol: Integer;
      var Allow: Boolean);
    procedure asgRekapBankContractNode(Sender: TObject; ARow,
      ARowreal: Integer);
    procedure asgRekapBankExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgRekapBankGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapBankGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapBankMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapBankMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbSFBankClick(Sender: TObject);
    procedure btnSinkronKasClick(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
  private
    { Private declarations }
    AIdMenu : integer;
    ListKas, ListBank : TStringList;
    DateNow : TDate;
    procedure InitForm(idx : integer);
    procedure SetGrid(AGrid : TAdvStringGrid);
    procedure ArrangeColSize(AGrid : TAdvStringGrid);
    procedure SetXcelling(AIndex : integer);
    procedure SetPrinting(AIndex : integer);
    procedure LoadData(AIndex : integer);
  public
    { Public declarations }
    procedure Execute(AMenuId : integer);
  end;

var
  frmRekapMutasiKasBank: TfrmRekapMutasiKasBank;

implementation


uses
  MainMenu, ureport, ADOInt, UEngine, URecord, USystemMenu, UConst,
  UGeneral, UCreateForm, UDummy, UTransaksi, UFinance, OracleConnection, 
  DateUtils, UReport_temp, Sinkronisasi;


  {$R *.dfm}
Const
  ColNode     = 0;
  ColNo       = 1;
  ColTgl      = 2;
  ColNmr      = 3;
  ColKet      = 4;
  ColDebet    = 5;
  ColKredit   = 6;
  ColSaldo    = 7;
  ColKetTrans = 8;
  ColSeq      = 9;

procedure TfrmRekapMutasiKasBank.AdvGlowButton1Click(Sender: TObject);
var aseq : integer;
begin
  Aseq := StrToInt(ListBank.Names[cmbBank.ItemIndex]);
  if aSeq <= 0 then begin
    inform('Silahkan pilih satu bank.');
    exit;
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.ExecuteKasBank(StartOfTheMonth(dtpAkhirKas.Date)-1, aSeq);
end;

procedure TfrmRekapMutasiKasBank.ArrangeColSize(AGrid: TAdvStringGrid);
begin
  AGrid.Cells[ColKredit, AGrid.RowCount-1] := 'Total';
  AGrid.AutoSizeColumns(True, 10);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    AGrid.ColWidths[ColSeq] := 0;
  end;

end;

procedure TfrmRekapMutasiKasBank.asgRekapBankCanClickCell(Sender: TObject; ARow,
  ACol: Integer; var Allow: Boolean);
begin
  if (ARow < 1) and (Acol = ColNode) then begin
    if ACol in [ColNode] then begin
      if asgRekapBank.Cells[ColNode, 0] = '[+]' then begin
        asgRekapBank.Cells[ColNode, 0] := '[-]';
        asgRekapBank.ExpandAll
      end else if asgRekapBank.Cells[ColNode, 0] = '[-]' then begin
        asgRekapBank.Cells[ColNode, 0] := '[+]';
        asgRekapBank.ContractAll;
      end;
    end;
    ArrangeColSize(asgRekapBank);
  end;
end;

procedure TfrmRekapMutasiKasBank.asgRekapBankContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(asgRekapBank);
end;

procedure TfrmRekapMutasiKasBank.asgRekapBankExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(asgRekapBank);
end;

procedure TfrmRekapMutasiKasBank.asgRekapBankGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) or (ACol = ColNode) then begin
    HAlign := taCenter;
    VAlign := vtaCenter
  end else if (ARow < asgRekapBank.RowCount-1) and (ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq]) then
  HAlign := taRightJustify
  else if (ARow = asgRekapBank.RowCount-1) and (ACol = ColSaldo) then
  HALign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmRekapMutasiKasBank.asgRekapBankGetFloatFormat(Sender: TObject;
  ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then begin
    isFloat := true;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapMutasiKasBank.asgRekapBankMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(asgRekapBank);
end;

procedure TfrmRekapMutasiKasBank.asgRekapBankMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(asgRekapBank);
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow < 1) and (ACol = ColNode) then begin
    if ACol in [ColNode] then begin
      if asgRekapKas.Cells[ColNode, 0] = '[+]' then begin
        asgRekapKas.Cells[ColNode, 0] := '[-]';
        asgRekapKas.ExpandAll
      end else if asgRekapKas.Cells[ColNode, 0] = '[-]' then begin
        asgRekapKas.Cells[ColNode, 0] := '[+]';
        asgRekapKas.ContractAll;
      end;
    end;
    ArrangeColSize(asgRekapKas);
  end;
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(asgRekapKas);
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize(asgRekapKas);
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) or (ACol = COlNode) then begin
    HAlign := taCenter;
    VAlign := vtaCenter
  end else if (ARow < asgRekapKas.RowCount-1) and (ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq]) then
  HAlign := taRightJustify
  else if (ARow = asgRekapKas.RowCount-1) and (ACol = ColSaldo) then
  HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasGetFloatFormat(Sender: TObject;
  ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then begin
    isFloat := true;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(asgRekapKas);
end;

procedure TfrmRekapMutasiKasBank.asgRekapKasMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(asgRekapKas);
end;

procedure TfrmRekapMutasiKasBank.btnCetakBankClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmRekapMutasiKasBank.btnCetakKasClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmRekapMutasiKasBank.btnEksporBankClick(Sender: TObject);
begin
   if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmRekapMutasiKasBank.btnEksporKasClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmRekapMutasiKasBank.btnFilterBankClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterBank, btnFilterBank, 62);
end;

procedure TfrmRekapMutasiKasBank.btnFilterKasClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterKas, btnFilterKas, 62);
end;

procedure TfrmRekapMutasiKasBank.btnLoadBankClick(Sender: TObject);
begin
  if dtpAkhirBank.Date >= dtpAwalBank.Date then begin
    LoadData(advOfPager.ActivePageIndex);
  end else begin
    InForm('periode akhir harus >= periode awal.');
    dtpAkhirBank.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapMutasiKasBank.btnLoadKasClick(Sender: TObject);
begin
  if dtpAkhirKas.Date >= dtpAwalKas.Date then begin
    LoadData(advOfPager.ActivePageIndex);
  end else begin
    Inform('Periode akhir harus >= periode awal.');
    dtpAkhirKas.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapMutasiKasBank.btnResetBankClick(Sender: TObject);
begin
  InitForm(2);
  SetGrid(asgRekapBank);
end;

procedure TfrmRekapMutasiKasBank.btnResetKasClick(Sender: TObject);
begin
  InitForm(1);
  SetGrid(asgRekapKas);
end;

procedure TfrmRekapMutasiKasBank.btnSinkronKasClick(Sender: TObject);
var aseq : integer;
begin
  Aseq := StrToInt(ListKas.Names[cmbKas.ItemIndex]);
  if aSeq <= 0 then begin
    inform('Silahkan pilih satu kas.');
    exit;
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.ExecuteKasBank(StartOfTheMonth(dtpAkhirKas.Date)-1, aSeq);
end;

procedure TfrmRekapMutasiKasBank.chbSFBankClick(Sender: TObject);
begin
  asgRekapBank.SearchFooter.Visible := chbSFBank.Checked;
end;

procedure TfrmRekapMutasiKasBank.chbSFKasClick(Sender: TObject);
begin
  asgRekapKas.SearchFooter.Visible := chbSFKas.Checked;
end;

procedure TfrmRekapMutasiKasBank.Execute(AMenuId: integer);
var 
    i : integer;
begin     
  DateNow := ServerNow;
  AIdMenu := AMenuId;
  if (not BisaPrint(aidMenu)) and (not BisaEkspor(aidMenu)) and (not BisaLihatRekap(aidMenu)) and (not BisaLihatLaporan(aidMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  ListKas  := TStringList.Create;
  ListBank := TStringList.Create;
  ListKas.Clear;
  cmbKas.Clear;
  ListBank.Clear;
  cmbBank.Clear;
  get_list_master_kas_bank(ListKas, boTrue, True, KASBANK_KAS);
  get_list_master_kas_bank(ListBank, boTrue, True, KasBank_Bank);
  for i := 0 to ListKas.Count-1 do begin
    if i = 0 then cmbKas.Items.Add(EkstrakString(ListKas.Values[ListKas.Names[i]], '#', 2))
    else cmbKas.Items.Add(EkstrakString(ListKas.Values[ListKas.Names[i]], '#', 3));
  end;
  for i := 0 to ListBank.Count-1 do begin
    if i = 0 then cmbBank.Items.Add(EkstrakString(ListBank.Values[ListBank.Names[i]], '#', 2))
    else cmbBank.Items.Add((EkstrakString(ListBank.Values[ListBank.Names[i]], '#', 3))+' '+(EkstrakString(ListBank.Values[ListBank.Names[i]], '#', 4)));
  end;
  AdvOfPager.ActivePageIndex := 0;
  InitForm(0);
  SetGrid(asgRekapKas);
  SetGrid(asgRekapBank);
  Run(Self);
end;

procedure TfrmRekapMutasiKasBank.FormDestroy(Sender: TObject);
begin
  ListKas.Destroy;
  ListBank.Destroy;
end;

procedure TfrmRekapMutasiKasBank.FormShow(Sender: TObject);
begin
  Execute(AcurrMenuId);
end;

procedure TfrmRekapMutasiKasBank.InitForm(idx: integer);
begin
  if (idx = 0) or (idx = 1) then begin //Kas
    cmbKas.ItemIndex := 0;
    dtpAwalKas.Date  := StartOfTheMonth(DateNow);
    dtpAkhirKas.Date := DateNow;
    asgRekapKas.SearchFooter.Visible := False;
    asgRekapKas.SearchFooter.Visible := False;
    chbSFKas.Checked := False;
  end;
  if (idx = 0) or (idx = 2) then begin //Bank
    cmbBank.ItemIndex := 0;
    dtpAwalBank.Date  := StartOfTheMonth(DateNow);
    dtpAkhirBank.Date := DateNow;
    asgRekapBank.SearchFooter.Visible := False;
    asgRekapBank.SearchFooter.Visible := False;
    chbSFBank.Checked := False;
  end;
end;

procedure TfrmRekapMutasiKasBank.LoadData(AIndex: integer);
var
  AKasBankSeq: Integer;
  ATipeBayar: string;
begin
  case AIndex of
    0 : begin
//          SetGrid(asgRekapKas);
          asgRekapKas.ClearNormalCells;
          asgRekapKas.RowCount := 3;
          asgRekapKas.ClearRows(asgRekapKas.RowCount-1, 1);

          AKasBankSeq := StrToInt(ListKas.Names[cmbKas.ItemIndex]);
          LoadMutasiKasBank(asgRekapKas, dtpAwalKas.Date, dtpAkhirKas.Date, AKasBankSeq, KASBANK_KAS, ATipeBayar, GaugeKas);
          ArrangeColSize(asgRekapKas);
        end;
    1 : begin
//          SetGrid(asgRekapBank);
          asgRekapBank.ClearNormalCells;
          asgRekapBank.RowCount := 3;
          asgRekapBank.ClearRows(asgRekapBank.RowCount-1, 1);

          AKasBankSeq := StrToInt(ListBank.Names[cmbBank.ItemIndex]);
          LoadMutasiKasBank(asgRekapBank, dtpAwalBank.Date, dtpAkhirBank.Date, AKasBankSeq, KASBANK_BANK, ATipeBayar, GaugeBank);
          ArrangeColSize(asgRekapBank);
        end;
  end;

end;

procedure TfrmRekapMutasiKasBank.SetGrid(AGrid: TAdvStringGrid);
begin
  AGrid.ClearNormalCells;
  asgResetGrid(AGrid,3,11,1,0);
  ArrangeColSize(AGrid);
end;

procedure TfrmRekapMutasiKasBank.SetPrinting(AIndex: integer);
procedure PreparePrint(AGrid : TAdvStringGrid);
  begin
    myConnection.advPrint.Grid := AGrid;
    AGrid.PrintSettings.TitleLines.Clear;
    AGrid.PrintSettings.TitleLines.Add('');
    if AGrid = asgRekapKas then AGrid.PrintSettings.TitleLines.Add('REKAP LAPORAN MUTASI KAS')
    else AGrid.PrintSettings.TitleLines.Add('REKAP LAPORAN MUTASI BANK');
    AGrid.PrintSettings.TitleLines.Add('');
    AGrid.ColumnSize.Stretch := False;
    AGrid.HideColumn(ColSeq);

    AGrid.ColCount := 9;
    SetingPrint(AGrid);
    myConnection.advPrint.Execute;
    AGrid.ColCount := 11;

    AGrid.UnHideColumn(ColSeq);
    AGrid.ColumnSize.Stretch := True;
    ArrangeColSize(AGrid);
  end;
begin
  case AIndex of
    0 : PreparePrint(asgRekapKas);
    1 : PreparePrint(asgRekapBank);
  end;
end;

procedure TfrmRekapMutasiKasBank.SetXcelling(AIndex: integer);
begin
  case AIndex of
    0 : asgExportToExcell(asgRekapKas, myConnection.SaveToExcell);
    1 : asgExportToExcell(asgRekapBank, myConnection.SaveToExcell);
  end;

end;

end.

