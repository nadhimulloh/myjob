unit KartuPiutang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, AdvEdit, AdvPanel, AdvOfficeButtons, Grids, AdvObj, BaseGrid,
  AdvGrid, AdvGlowButton, Gauges, AdvOfficePager, ExtCtrls, OracleConnection, UConst, URecord,
  UGeneral, UReport, USystemMenu, UEngine_Temp, DateUtils, AdvCombo;

type
  TfrmKartuPiutang = class(TForm)
    MainPanel: TPanel;
    advOfPager: TAdvOfficePager;
    pcgSupplier: TAdvOfficePage;
    Panel3: TPanel;
    GaugeHtgSupp: TGauge;
    btnFilterHtgSupp: TAdvGlowButton;
    btnEksporHtgSupp: TAdvGlowButton;
    btnCetakHtgSupp: TAdvGlowButton;
    chbSFHtgSupp: TAdvOfficeCheckBox;
    pnlFilterHtgSupp: TPanel;
    btnResetHtgSupp: TAdvGlowButton;
    btnLoadHtgSupp: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    txtCariHtgSupp: TAdvEdit;
    btnCariHtgSupp: TAdvGlowButton;
    PnlStatus: TAdvPanel;
    Label1: TLabel;
    Label5: TLabel;
    dtpAwalHtgSupp: TDateTimePicker;
    dtpAkhirHtgSupp: TDateTimePicker;
    pcgSupir: TAdvOfficePage;
    pnlFilterHtgSupir: TPanel;
    btnResetHtgSupir: TAdvGlowButton;
    btnLoadHtgSupir: TAdvGlowButton;
    AdvPanel5: TAdvPanel;
    Label2: TLabel;
    Label7: TLabel;
    dtpAwalHtgSupir: TDateTimePicker;
    dtpAkhirHtgSupir: TDateTimePicker;
    Panel2: TPanel;
    GaugeHtgSupir: TGauge;
    btnFilterHtgSupir: TAdvGlowButton;
    btnEksporHtgSupir: TAdvGlowButton;
    btnCetakHtgSupir: TAdvGlowButton;
    chbSFHtgSupir: TAdvOfficeCheckBox;
    pcgPihak3: TAdvOfficePage;
    pnlFilterHtgPihak3: TPanel;
    AdvPanel3: TAdvPanel;
    txtCariHtgPihak3: TAdvEdit;
    btnCariHtgPihak3: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label3: TLabel;
    Label4: TLabel;
    dtpAwalHtgPihak3: TDateTimePicker;
    dtpAkhirHtgPihak3: TDateTimePicker;
    btnLoadHtgPihak3: TAdvGlowButton;
    btnResetHtgPihak3: TAdvGlowButton;
    Panel4: TPanel;
    GaugeHtgPihak3: TGauge;
    btnFilterHtgPihak3: TAdvGlowButton;
    btnEksporHtgPihak3: TAdvGlowButton;
    btnCetakHtgPihak3: TAdvGlowButton;
    chbSFHtgPihak3: TAdvOfficeCheckBox;
    pcgAsset: TAdvOfficePage;
    pnlFilterHtgAssetSupp: TPanel;
    AdvPanel10: TAdvPanel;
    Label11: TLabel;
    Label6: TLabel;
    dtpAwalPtgAssetCust: TDateTimePicker;
    dtpAkhirPtgAssetCust: TDateTimePicker;
    btnLoadPtgAssetCust: TAdvGlowButton;
    btnResetPtgAssetCust: TAdvGlowButton;
    Panel10: TPanel;
    GaugePtgAssetCust: TGauge;
    btnFilterPtgAssetCust: TAdvGlowButton;
    btnEksporPtgAssetCust: TAdvGlowButton;
    btnCetakPtgAssetCust: TAdvGlowButton;
    chbSFPtgAssetCust: TAdvOfficeCheckBox;
    pcgPariwisata: TAdvOfficePage;
    pnlFilterHtgDPCust: TPanel;
    AdvPanel16: TAdvPanel;
    Label17: TLabel;
    Label8: TLabel;
    dtpAwalHtgDPCust: TDateTimePicker;
    dtpAkhirHtgDPCust: TDateTimePicker;
    AdvPanel17: TAdvPanel;
    txtCariHtgDPCust: TAdvEdit;
    btnCariHtgDPCust: TAdvGlowButton;
    btnLoadHtgDPCust: TAdvGlowButton;
    btnResetHtgDPCust: TAdvGlowButton;
    Panel16: TPanel;
    GaugeHtgDPCust: TGauge;
    btnFilterHtgDPCust: TAdvGlowButton;
    btnEksporHtgDPCust: TAdvGlowButton;
    btnCetakHtgDPCust: TAdvGlowButton;
    chbSFHtgDPCust: TAdvOfficeCheckBox;
    asgRekapHtgSupp: TAdvStringGrid;
    asgRekapHtgSupir: TAdvStringGrid;
    asgRekapHtgPihak3: TAdvStringGrid;
    asgRekapPtgAssetCust: TAdvStringGrid;
    asgRekapHtgDPCust: TAdvStringGrid;
    AdvPanel11: TAdvPanel;
    txtCariPtgAssetCust: TAdvEdit;
    btnCariPtgAssetCust: TAdvGlowButton;
    pcgPegawai: TAdvOfficePage;
    pnlFilterPegawai: TPanel;
    AdvPanel6: TAdvPanel;
    Label9: TLabel;
    Label10: TLabel;
    dtpAwalPegawai: TDateTimePicker;
    dtpAkhirPegawai: TDateTimePicker;
    AdvPanel7: TAdvPanel;
    txtPegawai: TAdvEdit;
    btnPegawai: TAdvGlowButton;
    btnLoadPegawai: TAdvGlowButton;
    btnResetPegawai: TAdvGlowButton;
    Panel5: TPanel;
    gaugePegawai: TGauge;
    btnFilterPegawai: TAdvGlowButton;
    btnEksporPegawai: TAdvGlowButton;
    btnCetakPegawai: TAdvGlowButton;
    chbSFPegawai: TAdvOfficeCheckBox;
    asgRekapPegawai: TAdvStringGrid;
    AdvPanel4: TAdvPanel;
    Label12: TLabel;
    Label13: TLabel;
    txtCariPtgSupir: TAdvEdit;
    btnCariPtgSupir: TAdvGlowButton;
    cmbTipePiutangSupir: TAdvComboBox;
    procedure btnLoadHtgSuppClick(Sender: TObject);
    procedure btnLoadHtgSupirClick(Sender: TObject);
    procedure btnLoadHtgPihak3Click(Sender: TObject);
    procedure btnLoadPtgAssetCustClick(Sender: TObject);
    procedure btnResetHtgSuppClick(Sender: TObject);
    procedure btnResetHtgSupirClick(Sender: TObject);
    procedure btnResetHtgPihak3Click(Sender: TObject);
    procedure btnResetPtgAssetCustClick(Sender: TObject);
    procedure btnResetHtgDPCustClick(Sender: TObject);
    procedure btnFilterHtgSuppClick(Sender: TObject);
    procedure btnFilterHtgSupirClick(Sender: TObject);
    procedure btnFilterHtgPihak3Click(Sender: TObject);
    procedure btnFilterPtgAssetCustClick(Sender: TObject);
    procedure btnFilterHtgDPCustClick(Sender: TObject);
    procedure btnEksporHtgSuppClick(Sender: TObject);
    procedure btnEksporHtgSupirClick(Sender: TObject);
    procedure btnEksporHtgPihak3Click(Sender: TObject);
    procedure btnEksporPtgAssetCustClick(Sender: TObject);
    procedure btnEksporHtgDPCustClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbSFHtgSuppClick(Sender: TObject);
    procedure chbSFHtgSupirClick(Sender: TObject);
    procedure chbSFHtgPihak3Click(Sender: TObject);
    procedure chbSFPtgAssetCustClick(Sender: TObject);
    procedure chbSFHtgDPCustClick(Sender: TObject);
    procedure asgRekapHtgSuppCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSupirCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgPihak3CellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapPtgAssetCustCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgDPCustCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPtgAssetCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgDPCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPtgAssetCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgDPCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgAssetCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgAssetCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgDPCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgDPCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnLoadHtgDPCustClick(Sender: TObject);
    procedure btnCariHtgSuppClick(Sender: TObject);
    procedure btnCariHtgSupirClick(Sender: TObject);
    procedure btnCariHtgPihak3Click(Sender: TObject);
    procedure btnCariHtgAssetSuppClick(Sender: TObject);
    procedure btnCariHtgDPCustClick(Sender: TObject);
    procedure btnCetakHtgSuppClick(Sender: TObject);
    procedure btnCetakHtgSupirClick(Sender: TObject);
    procedure btnCetakHtgPihak3Click(Sender: TObject);
    procedure btnCetakPtgAssetCustClick(Sender: TObject);
    procedure btnCetakHtgDPCustClick(Sender: TObject);
    procedure btnCariPtgAssetCustClick(Sender: TObject);
    procedure asgRekapPegawaiCellValidate(Sender: TObject; ACol,
      ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapPegawaiGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPegawaiGetFloatFormat(Sender: TObject; ACol,
      ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPegawaiMouseWheelDown(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPegawaiMouseWheelUp(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnPegawaiClick(Sender: TObject);
    procedure btnLoadPegawaiClick(Sender: TObject);
    procedure btnResetPegawaiClick(Sender: TObject);
    procedure chbSFPegawaiClick(Sender: TObject);
    procedure btnCetakPegawaiClick(Sender: TObject);
    procedure btnEksporPegawaiClick(Sender: TObject);
    procedure btnFilterPegawaiClick(Sender: TObject);
    procedure btnCariPtgSupirClick(Sender: TObject);
  private
    { Private declarations }
    AIdMenu : integer;
    DateNow : TDate;
    BrowseMode : boolean;
    TipePihak3 : String;
    SeqSupp, SeqSupir, SeqPihak3, SeqAsset, SeqDPCust, SeqPegawai : integer;
    procedure InitForm(idx : integer);
    procedure SetGrid(AGrid : TAdvStringGrid);
    procedure ArrangeColSize(AGrid : TAdvStringGrid);
    procedure SetPrinting(AIndex : integer);
    procedure SetXcelling(AIndex : integer);
    procedure LoadData(AIndex : integer);
  public
    { Public declarations }
    procedure Execute(AMenuId : integer); overload;
    procedure Execute(AMenuId, seqVC, PageIndex : integer; Tgl1, Tgl2 : TDate; PihakKe3 : string = ''; tipe:integer=0); overload;
  end;

var
  frmKartuPiutang: TfrmKartuPiutang;

implementation

uses MainMenu, Lov, LOVRelasi, UEngine;

{$R *.dfm}

const
  ColNo      = 0;
  ColTgl     = 1;
  ColNoTrans = 2;
  ColDesk    = 3;
  ColDebet   = 4;
  ColKredit  = 5;
  ColSaldo   = 6;
  ColKet     = 7;
  ColSeq     = 8;

{ TfrmKartuPiutang }

procedure TfrmKartuPiutang.ArrangeColSize(AGrid: TAdvStringGrid);
begin
  AGrid.FloatFormat := '%.2n';
  AGrid.AutoSizeColumns(True, 10);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    AGrid.ColWidths[ColSeq] := 0;
  end;
end;

procedure TfrmKartuPiutang.Execute(AMenuId, seqVC, PageIndex: integer; Tgl1, Tgl2: TDate; PihakKe3: string; tipe:integer);
begin
  AIdMenu := AMenuId;
  if (not BisaLihatLaporan(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  DateNow    := ServerNow;
  BrowseMode := true;
  TipePihak3 := PihakKe3;
  InitForm(0);
  SetGrid(asgRekapHtgSupp);
  SetGrid(asgRekapHtgSupir);
  SetGrid(asgRekapHtgPihak3);
  SetGrid(asgRekapPtgAssetCust);
  SetGrid(asgRekapHtgDPCust);         
  SetGrid(asgRekapPegawai); 
  advOfPager.ActivePageIndex := PageIndex;

  if PageIndex = 0 then begin
    SeqSupp := seqVC;
    if SeqSupp <> 0 then begin
      txtCariHtgSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqSupp)],'#',2);;
    end;
    dtpAwalHtgSupp.Date  := Tgl1;
    dtpAkhirHtgSupp.Date := Tgl2;
    btnLoadHtgSupp.Click;
  end;
  if PageIndex = 1 then begin
    SeqSupir := seqVC;
    if SeqSupir <> 0 then begin
      txtCariPtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
    end;
    dtpAwalHtgSupir.Date := Tgl1;
    dtpAkhirHtgSupir.Date := Tgl2;              
    cmbTipePiutangSupir.ItemIndex := tipe;
    btnLoadHtgSupir.Click;
  end;
  if PageIndex = 2 then begin
    TipePihak3 := PihakKe3;
    SeqPihak3  := SeqVc;
    if SeqPihak3 <> 0 then begin
      if TipePihak3 = VC_SUPPLIER then begin
        txtCariHtgPihak3.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqPihak3)],'#',2);
      end else begin
        txtCariHtgPihak3.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqPihak3)],'#',2);
      end;
    end;
    dtpAwalHtgPihak3.Date  := Tgl1;
    dtpAkhirHtgPihak3.Date := Tgl2;
    btnLoadHtgPihak3.Click;
  end;
  if PageIndex = 3 then begin
    SeqAsset := seqVC;
    if SeqAsset <> 0 then begin
      txtCariPtgAssetCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqAsset)],'#',2);
    end;
    dtpAwalPtgAssetCust.Date := Tgl1;
    dtpAkhirPtgAssetCust.Date := Tgl2;
    btnLoadPtgAssetCust.Click;
  end;
  if PageIndex = 4 then begin
    SeqDPCust := seqVC;
    if SeqDPCust <> 0 then begin
      txtCariHtgDPCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqDPCust)],'#',2);
    end;
    dtpAwalHtgDPCust.Date := Tgl1;
    dtpAkhirHtgDPCust.Date := Tgl2;
    btnLoadHtgDPCUst.Click;
  end;
  if PageIndex = 5 then begin
    SeqPegawai := seqVC;
    if SeqPegawai <> 0 then begin
      txtPegawai.Text := EkstrakString(ListGlobalPegawai.Values[IntToStr(SeqPegawai)],'#',2);
    end;
    dtpAwalPegawai.Date := Tgl1;
    dtpAkhirPegawai.Date := Tgl2;
    btnLoadPegawai.Click;
  end;
  Run(Self);
end;

procedure TfrmKartuPiutang.asgRekapPegawaiCellValidate(Sender: TObject;
  ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin              
  ArrangeColSize(asgRekapPegawai);
end;

procedure TfrmKartuPiutang.asgRekapPegawaiGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuPiutang.asgRekapPegawaiGetFloatFormat(Sender: TObject;
  ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuPiutang.asgRekapPegawaiMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPegawai);
end;

procedure TfrmKartuPiutang.asgRekapPegawaiMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPegawai);
end;

procedure TfrmKartuPiutang.asgRekapPtgAssetCustCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapPtgAssetCust);
end;

procedure TfrmKartuPiutang.asgRekapPtgAssetCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuPiutang.asgRekapPtgAssetCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuPiutang.asgRekapPtgAssetCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgAssetCust);
end;

procedure TfrmKartuPiutang.asgRekapPtgAssetCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgAssetCust);
end;

procedure TfrmKartuPiutang.asgRekapHtgDPCustCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmKartuPiutang.asgRekapHtgDPCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuPiutang.asgRekapHtgDPCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuPiutang.asgRekapHtgDPCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmKartuPiutang.asgRekapHtgDPCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmKartuPiutang.asgRekapHtgPihak3CellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmKartuPiutang.asgRekapHtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuPiutang.asgRekapHtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuPiutang.asgRekapHtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmKartuPiutang.asgRekapHtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmKartuPiutang.asgRekapHtgSupirCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmKartuPiutang.asgRekapHtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuPiutang.asgRekapHtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuPiutang.asgRekapHtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmKartuPiutang.asgRekapHtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmKartuPiutang.asgRekapHtgSuppCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmKartuPiutang.asgRekapHtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuPiutang.asgRekapHtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuPiutang.asgRekapHtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmKartuPiutang.asgRekapHtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmKartuPiutang.btnCariHtgAssetSuppClick(Sender: TObject);
begin
  SeqAsset       := 0;
  txtCariPtgAssetCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqAsset        := frmLov.ExecuteSupplier('',boNone);
  if SeqAsset <> 0 then begin
    txtCariPtgAssetCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqAsset)],'#',2);
  end;
end;

procedure TfrmKartuPiutang.btnCariHtgDPCustClick(Sender: TObject);
begin
  SeqDPCust      := 0;
  txtCariHtgDPCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqDPCust      := frmLOV.ExecuteCustomer('',BoNone);
  if SeqDPCust <> 0 then begin
    txtCariHtgDPCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqDPCust)],'#',2);
  end;
end;

procedure TfrmKartuPiutang.btnCariHtgPihak3Click(Sender: TObject);
var vTemp : string;
begin
  Application.CreateForm(TfrmLovRelasi, frmLovRelasi);
  frmLovRelasi.OnClose := frmMainMenu.ChildFormSingle;
  vTemp := frmLovRelasi.ExecuteGabungan('', false, true);
  if vTemp <> '' then begin
    TipePihak3 := EkstrakString(vTemp, '#', 1);
    SeqPihak3  := StrToIntDef(EkstrakString(vTemp, '#', 2),0);
    if TipePihak3 = UConst.VC_SUPPLIER then begin
      txtCariHtgPihak3.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end else begin
      txtCariHtgPihak3.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end;
  end else begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    txtCariHtgPihak3.Text := '';
  end;
end;

procedure TfrmKartuPiutang.btnCariHtgSupirClick(Sender: TObject);
begin
  SeqSupir       := 0;
  txtCariPtgSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupir       := frmLov.ExecuteSupir('',boNone);
  if SeqSupir <> 0 then begin
    txtCariPtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
  end;
end;

procedure TfrmKartuPiutang.btnCariHtgSuppClick(Sender: TObject);
begin
  SeqSupp        := 0;
  txtCariHtgSupp.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupp        := frmLov.ExecuteSupplier('',boNone);
  if SeqSupp <> 0 then begin
    txtCariHtgSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqSupp)],'#',2);
  end;
end;

procedure TfrmKartuPiutang.btnCariPtgAssetCustClick(Sender: TObject);
begin
  SeqAsset       := 0;
  txtCariPtgAssetCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqAsset        := frmLov.ExecuteCustomer('',boNone);
  if SeqAsset <> 0 then begin
    txtCariPtgAssetCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqAsset)],'#',2);
  end;
end;

procedure TfrmKartuPiutang.btnCariPtgSupirClick(Sender: TObject);
begin
  SeqSupir       := 0;
  txtCariPtgSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupir        := frmLov.ExecuteSupir('',boNone);
  if SeqSupir <> 0 then begin
    txtCariPtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
  end;
end;

procedure TfrmKartuPiutang.btnCetakPegawaiClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnCetakPtgAssetCustClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnCetakHtgDPCustClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnCetakHtgPihak3Click(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnCetakHtgSupirClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnCetakHtgSuppClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnEksporPegawaiClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnEksporPtgAssetCustClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnEksporHtgDPCustClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnEksporHtgPihak3Click(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnEksporHtgSupirClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnEksporHtgSuppClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuPiutang.btnFilterPegawaiClick(Sender: TObject);
begin  
  SetFilterSizeAdv(pnlFilterPegawai, btnFilterPegawai, 92);
end;

procedure TfrmKartuPiutang.btnFilterPtgAssetCustClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgAssetSupp, btnFilterPtgAssetCust, 92);
end;

procedure TfrmKartuPiutang.btnFilterHtgDPCustClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgDPCust, btnFilterHtgDPCust, 92);
end;

procedure TfrmKartuPiutang.btnFilterHtgPihak3Click(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgPihak3, btnFilterHtgPihak3, 92);
end;

procedure TfrmKartuPiutang.btnFilterHtgSupirClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgSupir, btnFilterHtgSupir, 92);
end;

procedure TfrmKartuPiutang.btnFilterHtgSuppClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgSupp, btnFilterHtgSupp, 92);
end;

procedure TfrmKartuPiutang.btnLoadPegawaiClick(Sender: TObject);
begin
  if SeqPegawai = 0 then begin
    InForm('Pegawai belum dipilih.');
    btnPegawai.SetFocus;
  end else begin
    if dtpAkhirPegawai.Date >= dtpAwalPegawai.Date then begin
      LoadData(5);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirPegawai.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuPiutang.btnLoadPtgAssetCustClick(Sender: TObject);
begin
  if SeqAsset = 0 then begin
    InForm('Customer belum dipilih.');
    btnCariPtgAssetCust.SetFocus;
  end else begin
    if dtpAkhirPtgAssetCust.Date >= dtpAwalPtgAssetCust.Date then begin
      LoadData(3);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirPtgAssetCust.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuPiutang.btnPegawaiClick(Sender: TObject);
var Mst : TR_Master_pegawai;
begin
  txtPegawai.Text := '';
  Application.CreateForm(TfrmLov, frmlov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqPegawai := frmLov.ExecutePegawai('',BoNone);
  if SeqPegawai <> 0 then begin
    if ListGlobalPegawai.Values[IntToStr(SeqPegawai)] = '' then begin
      //jika gk ada dilist global ambil pegawai dan masukan list
      Mst := Get_Master_pegawai(SeqPegawai);
      txtPegawai.Text := Mst.nama;
      Update_List_Global_Master_Pegawai(Mst);
    end else begin
      txtPegawai.Text := EkstrakString(ListGlobalPegawai.Values[IntToStr(SeqPegawai)],'#',2);
    end;
  end;
end;

procedure TfrmKartuPiutang.btnLoadHtgDPCustClick(Sender: TObject);
begin
  if SeqDPCust = 0 then begin
    InForm('Customer belum dipilih.');
    btnCariHtgDPCust.SetFocus;
  end else begin
    if dtpAkhirHtgDPCust.Date >= dtpAwalHtgDPCust.Date then begin
      LoadData(4);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgDPCust.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuPiutang.btnLoadHtgPihak3Click(Sender: TObject);
begin
  if SeqPihak3 = 0 then begin
    InForm('Relasi belum dipilih.');
    btnCariHtgPihak3.SetFocus;
  end else begin
    if dtpAkhirHtgPihak3.Date >= dtpAwalHtgPihak3.Date then begin
      LoadData(2);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgPihak3.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuPiutang.btnLoadHtgSupirClick(Sender: TObject);
begin
  if SeqSupir = 0 then begin
    InForm('Supir belum dipilih.');
    btnCariPtgSupir.SetFocus;
  end else begin
    if dtpAkhirHtgSupir.Date >= dtpAwalHtgSupir.Date then begin
      LoadData(1);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgSupir.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuPiutang.btnLoadHtgSuppClick(Sender: TObject);
begin
  if SeqSupp = 0 then begin
    InForm('Supplier belum dipilih.');
    btnCariHtgSupp.SetFocus;
  end else begin
    if dtpAkhirHtgSupp.Date >= dtpAwalhtgSupp.Date then begin
      LoadData(0);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgSupp.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuPiutang.btnResetPegawaiClick(Sender: TObject);
begin
  InitForm(6);
  SetGrid(asgRekapPegawai);
end;

procedure TfrmKartuPiutang.btnResetPtgAssetCustClick(Sender: TObject);
begin
  InitForm(4);
  SetGrid(asgRekapPtgAssetCust);
end;

procedure TfrmKartuPiutang.btnResetHtgDPCustClick(Sender: TObject);
begin
  InitForm(5);
  SetGrid(asgRekapHtgDPCust);
end;

procedure TfrmKartuPiutang.btnResetHtgPihak3Click(Sender: TObject);
begin
  InitForm(3);
  SetGrid(asgRekapHtgPihak3);
end;

procedure TfrmKartuPiutang.btnResetHtgSupirClick(Sender: TObject);
begin
  InitForm(2);
  SetGrid(asgRekapHtgSupir);
end;

procedure TfrmKartuPiutang.btnResetHtgSuppClick(Sender: TObject);
begin
  InitForm(1);
  SetGrid(asgRekapHtgSupp);
end;

procedure TfrmKartuPiutang.chbSFPegawaiClick(Sender: TObject);
begin        
  asgRekapPegawai.SearchFooter.Visible := chbSFPegawai.Checked;
end;

procedure TfrmKartuPiutang.chbSFPtgAssetCustClick(Sender: TObject);
begin
  asgRekapPtgAssetCust.SearchFooter.Visible := chbSFPtgAssetCust.Checked;
end;

procedure TfrmKartuPiutang.chbSFHtgDPCustClick(Sender: TObject);
begin
  asgRekapHtgDPCust.SearchFooter.Visible := chbSFHtgDPCust.Checked;
end;

procedure TfrmKartuPiutang.chbSFHtgPihak3Click(Sender: TObject);
begin
  asgRekapHtgPihak3.SearchFooter.Visible := chbSFHtgPihak3.Checked;
end;

procedure TfrmKartuPiutang.chbSFHtgSupirClick(Sender: TObject);
begin
  asgRekapHtgSupir.SearchFooter.Visible := chbSFHtgSupir.Checked;
end;

procedure TfrmKartuPiutang.chbSFHtgSuppClick(Sender: TObject);
begin
  asgRekapHtgSupp.SearchFooter.Visible := chbSFHtgSupp.Checked;
end;

procedure TfrmKartuPiutang.Execute(AMenuId: integer);
begin
  AIdMenu := AMenuId; browsemode := false;
  if (not BisaLihatLaporan(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  DateNow    := ServerNow;
  BrowseMode := false;
  InitForm(0);
  AdvOfPager.ActivePageIndex := 0; 
  SetGrid(asgRekapHtgSupp);
  SetGrid(asgRekapHtgSupir);
  SetGrid(asgRekapHtgPihak3);
  SetGrid(asgRekapPtgAssetCust);
  SetGrid(asgRekapHtgDPCust);          
  SetGrid(asgRekapPegawai);
  Run(Self);
end;

procedure TfrmKartuPiutang.FormShow(Sender: TObject);
begin
  if not browsemode then Execute(ACurrMenuID);
end;

procedure TfrmKartuPiutang.InitForm(idx: integer);
begin
  if (idx = 0) or (idx = 1) then begin
    SeqSupp := 0;
    txtCariHtgSupp.Text       := '';
    chbSFHtgSupp.Checked      := False;
    dtpAwalHtgSupp.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgSupp.Date      := DateNow;
    asgRekapHtgSupp.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 2) then begin
    SeqSupir := 0;
    txtCariPtgSupir.Text        := '';
    chbSFHtgSupir.Checked       := False;
    dtpAwalHtgSupir.Date        := StartOfTheMonth(DateNow);
    dtpAkhirHtgSupir.Date       := DateNow;
    asgRekapHtgSupir.SearchFooter.Visible := False;
    cmbTipePiutangSupir.ItemIndex := 0;
  end;
  if (idx = 0) or (idx = 3) then begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    txtCariHtgPihak3.Text       := '';
    chbSFHtgPihak3.Checked      := False;
    dtpAwalHtgPihak3.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgPihak3.Date      := DateNow;
    asgRekapHtgPihak3.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 4) then begin
    SeqAsset := 0;
    txtCariPtgAssetCust.Text    := '';
    chbSFPtgAssetCust.Checked   := False;
    dtpAwalPtgAssetCust.Date    := StartOfTheMonth(DateNow);
    dtpAkhirPtgAssetCust.Date   := DateNow;
    asgRekapPtgAssetCust.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 5) then begin
    SeqDPCust := 0;
    txtCariHtgDPCust.Text       := '';
    chbSFHtgDPCust.Checked      := False;
    dtpAwalHtgDPCust.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgDPCust.Date      := DateNow;
    asgRekapHtgDPCust.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 6) then begin
    SeqPegawai := 0;
    txtPegawai.Text           := '';
    chbSFPegawai.Checked      := False;
    dtpAwalPegawai.Date       := StartOfTheMonth(DateNow);
    dtpAkhirPegawai.Date      := DateNow;
    asgRekapPegawai.SearchFooter.Visible := False;
  end;
end;

procedure TfrmKartuPiutang.LoadData(AIndex: integer);
var tipe, tipeIn : string;
begin
  case AIndex of
    0 : begin
        asgRekapHtgSupp.ClearNormalCells;
        asgRekapHtgSupp.RowCount := 3;
        asgRekapHtgSupp.ClearRows(asgRekapHtgSupp.RowCount-1, 1);

        UReport.LoadKartuPiutang(asgRekapHtgSupp, GaugeHtgSupp, dtpAwalHtgSupp.date, dtpAkhirHtgSupp.Date, SeqSupp,
        SALDO_PIUTANG_DP_SUPP);
        ArrangeColSize(asgRekapHtgSupp);
    end;
    1 : begin
        tipeIn := '';
        if cmbTipePiutangSupir.ItemIndex = 0 then begin                                             // ditutup dzikri 281217 yg sisa setoran jd gausah nampil, jd pending
          tipeIn := FormatSQLString(SALDO_PIUTANG_KS)+', '+FormatSQLString(SALDO_PIUTANG_SETORAN);//+', '+FormatSQLString(SALDO_PIUTANG_SISA_SETORAN);
          tipe := '';
        end else if cmbTipePiutangSupir.ItemIndex = 1 then tipe := SALDO_PIUTANG_KS
        else if cmbTipePiutangSupir.ItemIndex = 2 then tipe := SALDO_PIUTANG_SETORAN;
//        else tipe := SALDO_PIUTANG_SISA_SETORAN;

        asgRekapHtgSupir.ClearNormalCells;
        asgRekapHtgSupir.RowCount := 3;
        asgRekapHtgSupir.ClearRows(asgRekapHtgSupir.RowCount-1, 1);

        UReport.LoadKartuPiutang(asgRekapHtgSupir, GaugeHtgSupir, dtpAwalHtgSupir.date, dtpAkhirHtgSupir.Date, SeqSupir,
                  tipe, false,tipeIn);
        ArrangeColSize(asgRekapHtgSupir);
    end;
    2 : begin
        asgRekapHtgPihak3.ClearNormalCells;
        asgRekapHtgPihak3.RowCount := 3;
        asgRekapHtgPihak3.ClearRows(asgRekapHtgPihak3.RowCount-1, 1);

        if TipePihak3 = VC_SUPPLIER then begin
          UReport.LoadKartuPiutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  SALDO_PIUTANG_PINJAMAN_SUPP);
        end else if TipePihak3 = VC_CUSTOMER then begin
          UReport.LoadKartuPiutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  SALDO_PIUTANG_PINJAMAN_CUST);
        end;
        ArrangeColSize(asgRekapHtgPihak3);
    end;
    3 : begin
        asgRekapPtgAssetCust.ClearNormalCells;
        asgRekapPtgAssetCust.RowCount := 3;
        asgRekapPtgAssetCust.ClearRows(asgRekapPtgAssetCust.RowCount-1, 1);

        UReport.LoadKartuPiutang(asgRekapPtgAssetCust, GaugePtgAssetCust, dtpAwalPtgAssetCust.date, dtpAkhirPtgAssetCust.Date, SeqAsset,
                  SALDO_PIUTANG_ASSET_CUST);
        ArrangeColSize(asgRekapPtgAssetCust);
    end;
    4 : begin
        asgRekapHtgDPCust.ClearNormalCells;
        asgRekapHtgDPCust.RowCount := 3;
        asgRekapHtgDPCust.ClearRows(asgRekapHtgDPCust.RowCount-1, 1);

        UReport.LoadKartuPiutang(asgRekapHtgDPCust, GaugeHtgDPCust, dtpAwalHtgDPCust.date, dtpAkhirHtgDPCust.Date, SeqDPCust,
                  SALDO_PIUTANG_PARIWISATA);
        ArrangeColSize(asgRekapHtgDPCust);
    end;
    5 : begin
        asgRekapPegawai.ClearNormalCells;
        asgRekapPegawai.RowCount := 3;
        asgRekapPegawai.ClearRows(asgRekapPegawai.RowCount-1, 1);

        UReport.LoadKartuPiutang(asgRekapPegawai, gaugePegawai, dtpAwalPegawai.date, dtpAkhirPegawai.Date, SeqPegawai,
                  SALDO_KASBON);
        ArrangeColSize(asgRekapPegawai);
    end;
  end;
end;

procedure TfrmKartuPiutang.SetGrid(AGrid: TAdvStringGrid);
begin
  AGrid.ClearNormalCells;
  asgResetGrid(AGrid,3,10,1,1);
  ArrangeColSize(AGrid);
end;

procedure TfrmKartuPiutang.SetPrinting(AIndex: integer);
procedure PreparePrint(AGrid: TAdvStringGrid);
  begin
    myConnection.advPrint.Grid:= AGrid;
    AGrid.PrintSettings.TitleLines.Clear;
    AGrid.PrintSettings.TitleLines.Add('');
    if AGrid = asgRekapHtgSupp then AGrid.PrintSettings.TitleLines.Add('Kartu Piutang DP Supplier')
    else if AGrid = asgRekapHtgSupir then AGrid.PrintSettings.TitleLines.Add('Kartu Piutang Supir')
    else if AGrid = asgRekapHtgPihak3 then AGrid.PrintSettings.TitleLines.Add('Kartu Piutang Relasi')
    else if AGrid = asgRekapPtgAssetCust then AGrid.PrintSettings.TitleLines.Add('Kartu Piutang Asset')
    else if AGrid = asgRekapHtgDPCust then AGrid.PrintSettings.TitleLines.Add('Kartu Piutang DP Pariwisata')    
    else if AGrid = asgRekapPegawai then AGrid.PrintSettings.TitleLines.Add('Kartu Piutang Pegawai');
    AGrid.PrintSettings.TitleLines.Add('');
    AGrid.ColumnSize.Stretch := False;
    AGrid.HideColumn(ColSeq);
    AGrid.ColCount := 8;
    SetingPrint(AGrid);
    myConnection.advPrint.Execute;
    agrid.ColCount := 9;
    AGrid.UnHideColumn(ColSeq);
    AGrid.ColumnSize.Stretch := True;
    ArrangeColSize(AGrid);
  end;
begin
  case AIndex of
    0 : PreparePrint(asgRekapHtgSupp);
    1 : PreparePrint(asgRekapHtgSupir);
    2 : PreparePrint(asgRekapHtgPihak3);
    3 : PreparePrint(asgRekapPtgAssetCust);
    4 : PreparePrint(asgRekapHtgDPCust);      
    5 : PreparePrint(asgRekapPegawai);
  end;
end;

procedure TfrmKartuPiutang.SetXcelling(AIndex: integer);
begin
  case AIndex of
    0 : asgExportToExcell(asgRekapHtgSupp, myConnection.SaveToExcell);
    1 : asgExportToExcell(asgRekapHtgSupir, myConnection.SaveToExcell);
    2 : asgExportToExcell(asgRekapHtgPihak3, myConnection.SaveToExcell);
    3 : asgExportToExcell(asgRekapPtgAssetCust, myConnection.SaveToExcell);
    4 : asgExportToExcell(asgRekapHtgDPCust, myConnection.SaveToExcell);     
    5 : asgExportToExcell(asgRekapPegawai, myConnection.SaveToExcell);
  end;
end;

end.

