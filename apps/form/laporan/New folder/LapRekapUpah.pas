unit LapRekapUpah;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, clisted, ComCtrls, StdCtrls, AdvEdit, AdvOfficeButtons, Grids,
  AdvObj, BaseGrid, AdvGrid, AdvGlowButton, AdvProgressBar, ExtCtrls, AdvPanel,
  Menus, AdvCombo;

type
  TfrmLapRekapUpah = class(TForm)
    MainPanel: TAdvPanel;
    AdvPanel4: TAdvPanel;
    Gauge: TAdvProgressBar;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    chbShowSF: TAdvOfficeCheckBox;
    chbShowDecimal: TAdvOfficeCheckBox;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    Label2: TLabel;
    Label1: TLabel;
    txtPegawai: TAdvEdit;
    btnLOVPeg: TAdvGlowButton;
    txtNorek: TAdvEdit;
    AdvPanel1: TAdvPanel;
    Label3: TLabel;
    Label7: TLabel;
    dtpPeriode: TDateTimePicker;
    cmbDept: TCheckListEdit;
    btnCetakSlip: TAdvGlowButton;
    ppPrint: TPopupMenu;
    CetakSemua1: TMenuItem;
    CetakPer1: TMenuItem;
    AdvPanel2: TAdvPanel;
    Label4: TLabel;
    cmbTipe: TAdvComboBox;
    CetakSemuaFormatLama1: TMenuItem;
    CetakSemuaFormatLama2: TMenuItem;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapGetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure cmbDeptClickCheck(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbShowSFClick(Sender: TObject);
    procedure chbShowDecimalClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnLOVPegClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnCetakSlipClick(Sender: TObject);
    procedure CetakPer1Click(Sender: TObject);
    procedure CetakSemua1Click(Sender: TObject);
    procedure CetakSemuaFormatLama1Click(Sender: TObject);
    procedure CetakSemuaFormatLama2Click(Sender: TObject);
  private
//    Bank : string;
    AIDMenu, SeqPegawai{, jumlahBank },jumlahDept : integer;
    ListBank, LiStDept : TStringList;
    Dept : String;
    procedure InitForm;
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
    procedure ClearComboCheckList(comboChek :TCheckListEdit);
  public
    { Public declarations }
    procedure Execute(AMenuID : integer);
  end;

var
  frmLapRekapUpah: TfrmLapRekapUpah;

implementation

uses
  StrUtils, UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy,
  UTransaksi, UFinance, UReport, MainMenu, OracleConnection, DateUtils, LOV,
  BuktiKeluarCetak, HtgGajiPegawaiPrint, HtgGajiPegawaiPrintLama;

Const
  ColNo        = 0;
  ColNIK       = 1;
  ColNama      = 2;
  ColNominal   = 3;
  ColBank      = 4;
  ColNoRek     = 5;
  ColKet       = 6;
  ColSeq       = 7;
  ColSeqMst    = 8;


{$R *.dfm}

procedure TfrmLapRekapUpah.btnCetakSlipClick(Sender: TObject);
//var Mst : TR_gaji_pegawai_master;
begin
//  if (asgRekap.Cells[ColSeq, asgRekap.Row] <> '') and (asgRekap.ints[ColSeq, asgRekap.Row] <> 0)  then begin
////    btnCetakSlip.Enabled := False;
//    Mst := Get_gaji_pegawai_master(asgRekap.Ints[ColSeqMst,asgRekap.row]);
//    Application.CreateForm(TfrmPrintHtgGajiPegawai, frmPrintHtgGajiPegawai);
//    frmPrintHtgGajiPegawai.OnClose := frmMainMenu.ChildFormSingle;
//    frmPrintHtgGajiPegawai.Execute(Mst,asgRekap.Ints[ColSeq,asgRekap.row]);
////    btnCetakSlip.Enabled := True;
//  end;
  btnCetakSlip.enabled := false;
  PopupMenu := ppPrint;
  ppPrint.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  PopupMenu := nil;
end;

procedure TfrmLapRekapUpah.ArrangeColSize;
begin
  asgRekap.AutoNumberCol(ColNo);
  asgRekap.ClearRows(asgRekap.RowCount-1, 1);
  asgRekap.Cells[ColNama, asgRekap.RowCount-1] := 'Total : ';
  asgRekap.FloatingFooter.ColumnCalc[ColNominal] := acSUM;
  asgRekap.AutoSizeColumns(True);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[ColSeq]   := 0;
    asgRekap.ColWidths[ColSeqMst]:= 0;
    asgRekap.ColWidths[ColBank]  := 0;
  end;
end;

procedure TfrmLapRekapUpah.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if ACol in [ColNo, ColNominal] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapRekapUpah.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := ACol in [ColNo, ColNominal];
  if IsFloat then begin
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else FloatFormat := IfThen(chbShowDecimal.Checked,'%.2n','%.0n');
  end;
end;

procedure TfrmLapRekapUpah.asgRekapGetFormat(Sender: TObject; ACol: Integer;
  var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if Acol in [ColNominal] then
    AStyle := ssFinancial;
end;

procedure TfrmLapRekapUpah.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize;
end;

procedure TfrmLapRekapUpah.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize;
end;

procedure TfrmLapRekapUpah.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AIDMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Rekap Upah');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount-2;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount+2;
  asgRekap.ColumnSize.Stretch := True;        
  ArrangeColSize;
end;

procedure TfrmLapRekapUpah.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AIDMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
//    asgExportToExcell(asgRekap, myConnection.SaveToExcell);   //gunawan +310818 ganti baru
  asgExportToExcell_2(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmLapRekapUpah.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 94);
end;

procedure TfrmLapRekapUpah.btnLoadClick(Sender: TObject);
begin
  LoadData;
  asgRekap.Row := 1;
  asgRekap.Col := ColNIK;
end;

procedure TfrmLapRekapUpah.btnLOVPegClick(Sender: TObject);
var Mst : TR_Master_pegawai;
begin
  txtPegawai.Text := '';
  Application.CreateForm(TfrmLov, frmlov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqPegawai := frmLov.ExecutePegawai('',BoNone);
  if SeqPegawai <> 0 then begin
    if ListGlobalPegawai.Values[IntToStr(SeqPegawai)] = '' then begin
      //jika gk ada dilist global ambil pegawai dan masukan list
      Mst := Get_Master_pegawai(SeqPegawai);
      txtPegawai.Text := Mst.nama;
      Update_List_Global_Master_Pegawai(Mst);
    end else begin
      txtPegawai.Text := EkstrakString(ListGlobalPegawai.Values[IntToStr(SeqPegawai)],'#',2);
    end;
  end;
end;

procedure TfrmLapRekapUpah.btnResetClick(Sender: TObject);
begin
  InitForm;
  SetGrid;
  asgRekap.Row := 1;
  asgRekap.Col := ColNIK;
  asgRekap.SetFocus;
end;

procedure TfrmLapRekapUpah.CetakPer1Click(Sender: TObject);
var Mst : TR_gaji_pegawai_master;
begin
  if (asgRekap.Cells[ColSeq, asgRekap.Row] <> '') and (asgRekap.ints[ColSeq, asgRekap.Row] <> 0)  then begin
    Mst := Get_gaji_pegawai_master(asgRekap.Ints[ColSeqMst,asgRekap.row]);
    Application.CreateForm(TfrmPrintHtgGajiPegawai, frmPrintHtgGajiPegawai);
    frmPrintHtgGajiPegawai.OnClose := frmMainMenu.ChildFormSingle;
    frmPrintHtgGajiPegawai.Execute(Mst,asgRekap.Ints[ColSeq,asgRekap.row]);
  end;
  btnCetakSlip.enabled := true;
end;

procedure TfrmLapRekapUpah.CetakSemua1Click(Sender: TObject);
var Mst : TR_gaji_pegawai_master;
begin
  if (asgRekap.Cells[ColSeq, asgRekap.Row] <> '') and (asgRekap.ints[ColSeq, asgRekap.Row] <> 0)  then begin
    Mst := Get_gaji_pegawai_master(asgRekap.Ints[ColSeqMst,asgRekap.row]);
    Application.CreateForm(TfrmPrintHtgGajiPegawai, frmPrintHtgGajiPegawai);
    frmPrintHtgGajiPegawai.OnClose := frmMainMenu.ChildFormSingle;
    frmPrintHtgGajiPegawai.Execute(Mst,0,StartOfTheMonth(dtpPeriode.Date));
  end;
  btnCetakSlip.enabled := true;
end;

procedure TfrmLapRekapUpah.CetakSemuaFormatLama1Click(Sender: TObject);
var Mst : TR_gaji_pegawai_master;
begin
  if (asgRekap.Cells[ColSeq, asgRekap.Row] <> '') and (asgRekap.ints[ColSeq, asgRekap.Row] <> 0)  then begin
    Mst := Get_gaji_pegawai_master(asgRekap.Ints[ColSeqMst,asgRekap.row]);
    Application.CreateForm(TfrmPrintHtgGajiPegawaiLama, frmPrintHtgGajiPegawaiLama);
    frmPrintHtgGajiPegawaiLama.OnClose := frmMainMenu.ChildFormSingle;
    frmPrintHtgGajiPegawaiLama.Execute(Mst,0,StartOfTheMonth(dtpPeriode.Date));
  end;
  btnCetakSlip.enabled := true;
end;

procedure TfrmLapRekapUpah.CetakSemuaFormatLama2Click(Sender: TObject);
var Mst : TR_gaji_pegawai_master;
begin
  if (asgRekap.Cells[ColSeq, asgRekap.Row] <> '') and (asgRekap.ints[ColSeq, asgRekap.Row] <> 0)  then begin
    Mst := Get_gaji_pegawai_master(asgRekap.Ints[ColSeqMst,asgRekap.row]);
    Application.CreateForm(TfrmPrintHtgGajiPegawaiLama, frmPrintHtgGajiPegawaiLama);
    frmPrintHtgGajiPegawaiLama.OnClose := frmMainMenu.ChildFormSingle;
    frmPrintHtgGajiPegawaiLama.Execute(Mst,asgRekap.Ints[ColSeq,asgRekap.row]);
  end;
  btnCetakSlip.enabled := true;
end;

procedure TfrmLapRekapUpah.chbShowDecimalClick(Sender: TObject);
begin
  ArrangeColSize;
end;

procedure TfrmLapRekapUpah.chbShowSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbShowSF.Checked;
end;

procedure TfrmLapRekapUpah.ClearComboCheckList(comboChek: TCheckListEdit);
var i : integer;
begin
  for i := 0 to comboChek.Items.Count - 1 do
    comboChek.Checked[i] := False;
end;

//procedure TfrmLapRekapUpah.cmbBankClickCheck(Sender: TObject);
//var i : integer; isAda : boolean;
//begin
//  jumlahBank := 0;
//  Bank := '';
//  if (cmbBank.Selected[0]) and (cmbBank.Checked[0]) then begin
//    for i := 1 to cmbBank.Items.Count-1 do begin
//      cmbBank.Checked[i] := false;
//    end;
//  end else begin
//    for i := 0 to cmbBank.Items.Count-1 do begin
//      if (cmbBank.Checked[i] = True) and (cmbBank.Checked[0] = true) then begin
//        cmbBank.Checked[0] := False;
//        Break;
//      end;
//    end;
//  end;
//
//  isAda := False;
//  for i := 0 to cmbBank.Items.Count-1 do begin
//    if cmbBank.Checked[i] then begin
//      isAda := True;
//      Break;
//    end else isAda := False;
//  end;
//
//
//  for i := 1 to cmbBank.Items.Count-1 do begin
//    if cmbBank.Checked[i] then begin
//      jumlahBank := jumlahBank + 1;
//      if Bank = '' then Bank := (ListBank.Names[i])
//      else Bank := Bank + ',' + (ListBank.Names[i]);
//    end;
//  end;
//
//  if not isAda then
//    cmbBank.Checked[0] := True;
//end;

procedure TfrmLapRekapUpah.cmbDeptClickCheck(Sender: TObject);
var i : integer; isAda : boolean;
begin
  jumlahDept := 0;
  Dept := '';
  if (cmbDept.Selected[0]) and (cmbDept.Checked[0]) then begin
    for i := 1 to cmbDept.Items.Count-1 do begin
      cmbDept.Checked[i] := false;
    end;
  end else begin
    for i := 0 to cmbDept.Items.Count-1 do begin
      if (cmbDept.Checked[i] = True) and (cmbDept.Checked[0] = true) then begin
        cmbDept.Checked[0] := False;
        Break;
      end;
    end;
  end;

  isAda := False;
  for i := 0 to cmbDept.Items.Count-1 do begin
    if cmbDept.Checked[i] then begin
      isAda := True;
      Break;
    end else isAda := False;
  end;


  for i := 1 to cmbDept.Items.Count-1 do begin
    if cmbDept.Checked[i] then begin
      jumlahDept := jumlahDept + 1;
      if Dept = '' then Dept := FormatSQLString(ListDept.Strings[i])
      else Dept := Dept + ',' + FormatSQLString(ListDept.Strings[i]);
    end;
  end;

  if not isAda then
    cmbDept.Checked[0] := True;
end;

procedure TfrmLapRekapUpah.Execute(AMenuID: integer);
begin
  ListBank := TStringList.Create;
  ListDept := TStringList.Create;
  aidMenu := AMenuId;
  if (not BisaPrint(aidMenu)) and (not BisaEkspor(aidMenu)) and (not BisaLihatRekap(aidMenu)) and (not BisaLihatLaporan(aidMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  InitForm;
  SetGrid;
  run(self);
end;

procedure TfrmLapRekapUpah.FormDestroy(Sender: TObject);
begin
  ListBank.free;
  ListDept.Free;
end;

procedure TfrmLapRekapUpah.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapRekapUpah.InitForm;
begin
  SeqPegawai := 0;
  dtpPeriode.Date   := StartOfTheMonth(ServerNow);
  txtPegawai.Text := '';
  txtNorek.Text := '';
  chbShowSF.Checked := False;
  asgRekap.SearchFooter.Visible := False;
  chbShowDecimal.Checked := false;
  cmbTipe.ItemIndex := 0;
  ListBank.Clear;
  ListBank.AddStrings(ListGlobalBank);
  ListBank.Insert(0,'0=SEMUA#SEMUA');
//  cmbBank.Items.Clear;
//  for i := 0 to ListBank.Count-1 do begin
//    cmbBank.Items.Add(EkstrakString(ListBank.ValueFromIndex[i],'#',2));
//  end;
//  ClearComboCheckList(cmbBank);
//  cmbBank.Checked[0] := True;
//  jumlahBank := 0;
//  Bank := '';
  ListDept.Clear;
  ListDept.AddStrings(ListGlobalDepartemen);
  ListDept.Insert(0,'SEMUA');
  cmbDept.Items.Clear;
  cmbDept.Items.AddStrings(ListDept);
  ClearComboCheckList(cmbDept);
  cmbDept.Checked[0] := True;
  jumlahDept := 0;
  Dept := '';
end;

procedure TfrmLapRekapUpah.LoadData;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1, 1);
  asgRekap.RowCount := 3;

  LoadRekapUpah(asgRekap, Gauge,  StartOfTheMonth(dtpPeriode.Date), SeqPegawai, TrimAllString(txtNorek.Text), '', dept,cmbTipe.Text);

  ArrangeColSize;
end;

procedure TfrmLapRekapUpah.SetGrid;
begin
  asgResetGrid(asgRekap, 3, 10, 1, 1);
  ArrangeColSize;
end;

end.
