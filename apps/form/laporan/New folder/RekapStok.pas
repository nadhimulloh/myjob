unit RekapStok;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvEdit,
  ComCtrls, AdvPanel, AdvGlowButton, ExtCtrls, OracleConnection, UEngine, UConst, UGeneral,
  USystemMenu, URecord, DateUtils, UReport, Gauges;

type
  TfrmRekapStok = class(TForm)
    MainPanel: TPanel;
    pnlFilter: TPanel;
    btnOk: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label1: TLabel;
    Label2: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    AdvPanel2: TAdvPanel;
    Label7: TLabel;
    txtSparePart: TAdvEdit;
    btnSparePart: TAdvGlowButton;
    Panel2: TPanel;
    asgRekap: TAdvStringGrid;
    btnFilter: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    btnKartu: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    chbShowNol: TAdvOfficeCheckBox;
    Label3: TLabel;
    txtKategori: TAdvEdit;
    btnKategori: TAdvGlowButton;
    Label6: TLabel;
    cmbGudang: TComboBox;
    btnSinkronAsset: TAdvGlowButton;
    btnSinkronisasiHargaRata: TAdvGlowButton;
    Gauge: TGauge;
    Label9: TLabel;
    Label4: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure txtSparePartKeyPress(Sender: TObject; var Key: Char);
    procedure txtKategoriKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnSparePartClick(Sender: TObject);
    procedure btnKategoriClick(Sender: TObject);
    procedure btnKartuClick(Sender: TObject);
    procedure btnSinkronAssetClick(Sender: TObject);
    procedure btnSinkronisasiHargaRataClick(Sender: TObject);
    procedure asgRekapGetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
  private
    { Private declarations }
    DateNow : TDate;
    SeqSPart, SeqKat, idMenu : Integer;
    ListGudang : TStringList;
    procedure ArrangeColSize;
    procedure InitForm;
    procedure Setgrid;
    procedure LoadData;
  public
    { Public declarations }
    procedure Execute(AMenuId: Integer);
  end;

var
  frmRekapStok: TfrmRekapStok;

implementation

uses MainMenu, KartuStok, LovMasterSparePart, LOV, SinkronisasiBarang,
  SinkronisasiHargaRata;

{$R *.dfm}

const
  ColNo         = 0;
  ColNoPart     = 1;
  ColNama       = 2;
  ColNamaDagang = 3;
  colNoRak      = 4;
  ColAwal       = 5;
  ColMasuk      = 6;
  ColKeluar     = 7;
  ColPinjam     = 8;
  ColKPinjam      = 9;
  ColMeminjamkan  = 10;
  ColKMeminjamkan = 11;
  ColAkhir        = 12;
  ColMin          = 13; //gunawan +151119
  ColSeq          = 14;

{ TfrmRekapStok }

procedure TfrmRekapStok.ArrangeColSize;
begin
  asgRekap.AutoNumberCol(ColNo);
  asgRekap.Cells[ColNamaDagang, asgRekap.RowCount-1] := 'Total';
  asgRekap.FloatingFooter.ColumnCalc[ColAwal]    := acsum;
  asgRekap.FloatingFooter.ColumnCalc[ColMasuk]   := acsum;
  asgRekap.FloatingFooter.ColumnCalc[ColKeluar]  := acsum;
  asgRekap.FloatingFooter.ColumnCalc[ColAkhir]   := acsum;
  asgRekap.FloatingFooter.ColumnCalc[ColPinjam]  := acSUM;
  asgRekap.FloatingFooter.ColumnCalc[ColMeminjamkan] := acSUM;
  asgRekap.FloatingFooter.ColumnCalc[ColKPinjam]  := acSUM;
  asgRekap.FloatingFooter.ColumnCalc[ColKMeminjamkan] := acSUM;
  asgRekap.AutoSizeColumns(true,5);
  if GlobalSystemUser.AccessLevel <> UConst.LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[ColSeq] := 0;                                        
    asgRekap.ColWidths[ColMin] := 0;
  end;
end;

procedure TfrmRekapStok.asgRekapCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  arrangecolsize;
end;

procedure TfrmRekapStok.asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) then begin
    btnKartu.Enabled := (asgRekap.Ints[ColSeq, ARow] <> 0);
  end;
end;

procedure TfrmRekapStok.asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (Acol > 0) and (ARow < asgRekap.RowCount-1) then begin
    if (asgRekap.Ints[ColSeq, ARow] > 0) then
      btnKartu.Click;
  end;
end;

procedure TfrmRekapStok.asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then
    HAlign := taCenter
  else if ACol in[ColNo, ColAwal..ColAkhir] then
    HAlign := taRightJustify;
end;

procedure TfrmRekapStok.asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isFloat := ACol in[ColNo, ColAwal, ColMasuk..ColAkhir];
  case ACol of
    ColNo  : FloatFormat := '%.0n';
    ColSeq : FloatFormat := '%.0f';
    ColAwal..ColAkhir: FloatFormat := '%.2n';
  end;
end;

procedure TfrmRekapStok.asgRekapGetFormat(Sender: TObject; ACol: Integer;
  var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if acol in [ColAwal..ColAkhir, ColSeq] then AStyle := ssFinancial
  else AStyle := ssAlphabetic;
end;

procedure TfrmRekapStok.asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmRekapStok.asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmRekapStok.btnCetakClick(Sender: TObject);
begin
  if (not BisaPrint(idMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Rekap Stok Spare Part');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 2;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount + 2;
  asgRekap.ColumnSize.Stretch := True;
  ArrangeColSize;
end;

procedure TfrmRekapStok.btnEksporClick(Sender: TObject);
begin
  if  (not BisaEkspor(idMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapStok.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 90);
end;

procedure TfrmRekapStok.btnKartuClick(Sender: TObject);
var seqGudang : integer;
begin
  SeqSPart := asgRekap.Ints[ColSeq, asgRekap.Row];
  SeqGudang := strtointdef(ListGudang.Names[cmbGudang.ItemIndex], 0);
  if asgRekap.Cells[ColSeq, asgRekap.Row] <> '' then begin
    Application.CreateForm(TfrmKartuStok, frmKartuStok);
    frmKartuStok.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuStok.Execute(504, SeqSPart, SeqGudang, 0, dtpAwal.date, dtpAkhir.date);
  end;
  if txtSparePart.Text = '' then SeqSPart := 0;
end;

procedure TfrmRekapStok.btnKategoriClick(Sender: TObject);
begin
  txtKategori.Text := '';
	SeqKat := 0;
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqKat         :=  frmLOV.ExecuteKategoriSparePart('', botrue);
  if SeqKat <> 0 then begin
    txtKategori.Text := EkstrakString(ListKategoriSparPartGlobal.Values[IntToStr(SeqKat)],'#',1)+' | '+
                        EkstrakString(ListKategoriSparPartGlobal.Values[IntToStr(SeqKat)],'#',2);
  end;
end;

procedure TfrmRekapStok.btnOkClick(Sender: TObject);
begin
  //gunawan +311018 jika gudang belum dipilih
  if cmbGudang.ItemIndex = -1 then begin
    inform('Gudang belum dipilih.');
    cmbGudang.SetFocus;
    exit;
  end;
  Loaddata;
end;

procedure TfrmRekapStok.btnResetClick(Sender: TObject);
begin
  InitForm;
  Setgrid;
end;

procedure TfrmRekapStok.btnSinkronAssetClick(Sender: TObject);
begin
  Application.CreateForm(TfrmSinkronisasiBarang,frmSinkronisasiBarang);
  frmSinkronisasiBarang.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasiBarang.Execute(dtpAwal.Date);
end;

procedure TfrmRekapStok.btnSinkronisasiHargaRataClick(Sender: TObject);
begin
  Application.CreateForm(TfrmSinkronisasiHargaRata,frmSinkronisasiHargaRata);
  frmSinkronisasiHargaRata.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasiHargaRata.Execute(dtpAwal.Date);
end;

procedure TfrmRekapStok.btnSparePartClick(Sender: TObject);
begin
  txtSparePart.Text := '';
  SeqSPart := 0;
  Application.CreateForm(TfrmLovMasterSparePart, frmLovMasterSparePart);
  frmLovMasterSparePart.OnClose := frmMainMenu.ChildFormSingle;
  SeqSPart := frmLovMasterSparePart.Execute('',bonone,BoTrue);
  if SeqSPart <> 0 then begin
    txtSparePart.Text := EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 1)+ ' | ' +
                         EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 2);
  end;
end;

procedure TfrmRekapStok.chbSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapStok.Execute(AMenuId: Integer);
var i : integer;
begin
  idMenu  := AMenuId;
  DateNow := ServerNow;
  if (not BisaPrint(idMenu)) and (not BisaEkspor(idMenu)) and (not BisaLihatRekap(idMenu)) and (not BisaLihatLaporan(idMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;

  ListGudang := TStringList.Create;
  GetListMasterGudang(ListGudang,'Nama',Bonone,False);
  for i := 0 to ListGudang.Count-1 do begin
    CmbGudang.Items.Add(EkstrakString(ListGudang.Values[ListGudang.Names[i]],'=',1));
  end;

  InitForm;
  Setgrid;     
  asgRekap.OnClickCell(asgRekap,1,1);
  Run(Self);
end;

procedure TfrmRekapStok.FormDestroy(Sender: TObject);
begin
  ListGudang.Destroy;
end;

procedure TfrmRekapStok.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmRekapStok.InitForm;
begin
  dtpAwal.Date        := StartOfTheMonth(DateNow);
  dtpAkhir.Date       := DateNow;
  SeqSPart            := 0;
  SeqKat              := 0;
  txtSparePart.Text   := '';
  txtKategori.Text    := '';
  cmbGudang.ItemIndex := -1;
  chbShowNol.Checked  := false;
  chbSF.Checked       := False;
  asgRekap.SearchFooter.Visible := False;
end;

procedure TfrmRekapStok.LoadData;
var SeqGudang : integer;
begin
  asgRekap.Row := 1;
  asgRekap.Col := 1;
  SeqGudang := strtointdef(ListGudang.Names[cmbGudang.ItemIndex], 0);
  LoadRekapStok(asgRekap, dtpAwal.Date, dtpAkhir.Date, SeqSPart, SeqGudang, SeqKat, chbShowNol.checked, Gauge);
  if asgRekap.RowCount = 2 then asgRekap.AddRow;
  ArrangeColSize;
end;

procedure TfrmRekapStok.Setgrid;
begin
  asgResetGrid(asgRekap, 3, 16{15}, 1,1);
  ArrangeColSize;
end;

procedure TfrmRekapStok.txtKategoriKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then btnKategori.SetFocus; 
end;

procedure TfrmRekapStok.txtSparePartKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then btnSparePart.SetFocus;
end;

end.
