unit LapPendapatanPengeluaranMobil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, Gauges,
  AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, StdCtrls, Menus, AdvOfficePager,
  AdvEdit, AdvProgressBar;

type
  TfrmLapPendapatanPengeluaranMobil = class(TForm)
    MainPanel: TPanel;
    pnlfilter: TPanel;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    Panel2: TPanel;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    btnFilter: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label4: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label1: TLabel;
    asgLap: TAdvStringGrid;
    AdvPanel1: TAdvPanel;
    Label3: TLabel;
    txtArmada: TAdvEdit;
    btnlovarmada: TAdvGlowButton;
    AdvProgressBar1: TAdvProgressBar;
    chbSupir: TCheckBox;
    Label5: TLabel;
    txtSupir: TAdvEdit;
    btnSupir: TAdvGlowButton;
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure Import1Click(Sender: TObject);
    procedure Import2Click(Sender: TObject);
    procedure asgLapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgLapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgLapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure aofTerminalChange(Sender: TObject);
    procedure asgLapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgLapContractNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgLapExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgLapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure btnlovarmadaClick(Sender: TObject);
    procedure btnSupirClick(Sender: TObject);
    procedure asgLapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
    AMenuid : integer;
    seqArmada, seqSupir : integer;
    listHrgrata : TStringList;
    datenow : tdate;
    procedure InitForm;
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
  public
    { Public declarations }
    procedure execute(AidMenu:integer);
  end;

var
  frmLapPendapatanPengeluaranMobil: TfrmLapPendapatanPengeluaranMobil;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, MainMenu, OracleConnection, ImportDataTerminal,
  DateUtils, UConst_Temp, ADOInt, StrUtils, ImportDataTerminalWB, LOV,
  LovMasterSparePart;

const
  colNode   = 0;
  colNobody = 1;
  colNopol  = 2;
  colPendapatan  = 3;
  colPengeluaran = 4;
  colselisih     = 5;
{$R *.dfm}

procedure TfrmLapPendapatanPengeluaranMobil.aofTerminalChange(Sender: TObject);
begin
  chbSF.Checked := False;
  asgLap.SearchFooter.Visible :=  false;
end;

procedure TfrmLapPendapatanPengeluaranMobil.ArrangeColSize;
begin
  asgLap.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin

  end;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow < 2) and (ACol = 0) then begin
    asgLap.Cells[0,0] := IfThen(asgLap.Cells[0,0]='[-]','[+]','[-]');
    if asgLap.Cells[0,0] = '[+]' then asgLap.ContractAll
    else asgLap.ExpandAll;
  end;
  arrangecolsize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 2 then HAlign := taCenter
  else if acol in [colPendapatan , colPengeluaran, colselisih]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;      
  if chbSupir.checked then begin
   if acol in [colnode] then HAlign := taRightJustify;
  end;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if chbSupir.checked then begin
    if asgLap.cells[colnode, arow] <> '' then Afont.style := [fsBold];
  end;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := false;
  IsFloat := acol in [colPendapatan , colPengeluaran, colselisih];
  if IsFloat then begin
    if ACol in [colPendapatan , colPengeluaran, colselisih] then FloatFormat := '%.2n'
    else FloatFormat := '%.0n';
  end;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  arrangecolsize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.asgLapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  arrangecolsize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  SetingPrint(asgLap);
  myConnection.advPrint.Grid := asgLap;
  asgLap.PrintSettings.TitleLines.Clear;
  asgLap.PrintSettings.TitleLines.Add('');
  asgLap.PrintSettings.TitleLines.Add('Laporan Pendapatan Pengeluaran Mobil');
  asgLap.PrintSettings.TitleLines.Add('');
  asgLap.ColumnSize.Stretch := false ;
  asgLap.ColCount := asgLap.ColCount-1;
  SetingPrint(asgLap);
  myConnection.advPrint.Execute ;
  asgLap.ColCount := asgLap.ColCount+1;
  asgLap.ColumnSize.Stretch := true;
  ArrangeColSize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgLap, myConnection.SaveToExcell);
end;                                  

procedure TfrmLapPendapatanPengeluaranMobil.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlfilter, btnFilter, 90);
end;

procedure TfrmLapPendapatanPengeluaranMobil.btnLoadClick(Sender: TObject);
begin
  loaddata
end;

procedure TfrmLapPendapatanPengeluaranMobil.btnlovarmadaClick(Sender: TObject);
begin
  txtArmada.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada         := frmLov.ExecuteArmada('',boNone, False, '', BoFalse);
  if SeqArmada <> 0 then begin
    txtArmada.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',1)+' | '
    +EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapPendapatanPengeluaranMobil.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
end;

procedure TfrmLapPendapatanPengeluaranMobil.btnSupirClick(Sender: TObject);
begin
  txtSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  seqSupir := frmLov.ExecuteSupir('',boNONE);
  if seqSupir <> 0 then begin
    txtSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(seqSupir)],'#',2);
  end;
end;

procedure TfrmLapPendapatanPengeluaranMobil.chbSFClick(Sender: TObject);
begin
  asgLap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLapPendapatanPengeluaranMobil.execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  listHrgrata := tstringlist.create;
  datenow := ServerNow; 
  if (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
     and (not BisaNambah(AMenuId)) and (not BisaLihatRekap(AMenuId)) and (not BisaLihatLaporan(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  initform;
  Setgrid;
  run(self);
end;

procedure TfrmLapPendapatanPengeluaranMobil.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapPendapatanPengeluaranMobil.Import1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportDataTerminal, frmImportDataTerminal);
  frmImportDataTerminal.OnClose := frmMainMenu.ChildFormSingle;
  if frmImportDataTerminal.execute then btnLoad.Click;
end;

procedure TfrmLapPendapatanPengeluaranMobil.Import2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportDataTerminalWB, frmImportDataTerminalWB);
    frmImportDataTerminalWB.OnClose := frmMainMenu.ChildFormSingle;
  if frmImportDataTerminalWB.execute then btnLoad.Click;
end;

procedure TfrmLapPendapatanPengeluaranMobil.InitForm;
begin
  dtpAwal.date := StartOfTheMonth(ServerNow);
  dtpAkhir.Date := ServerNow;
  chbSF.Checked := false;
  seqArmada := 0;
  txtArmada.Text := '';
  asgLap.SearchFooter.Visible := False;
  seqSupir := 0; txtSupir.Text := ''; chbSupir.Checked := false; //yn+121219
end;

procedure TfrmLapPendapatanPengeluaranMobil.LoadData;
begin
  setgrid;
  if chbSupir.Checked = false then begin//yn+131219
    LoadRekapPendapatanPengeluaran(asgLap,AdvProgressBar1,dtpAwal.Date, dtpAkhir.Date,seqArmada, seqSupir);  
    // CH 100120 lap ganti format
    asgLap.FixedCols := 0;
  end
  else begin
    asgLap.Cells[colNobody,0] := 'No. Body';
    asgLap.Cells[colNobody,1] := 'Kode Supir';
    asgLap.Cells[colNopol,0]  := 'No. Polisi';
    asgLap.Cells[colNopol,1]  := 'Nama Supir';
//    LoadRekapPendapatanPengeluaranBySupir(asgLap,AdvProgressBar1,dtpAwal.Date, dtpAkhir.Date,seqArmada, seqSupir);
    // CH 100120 lap ganti format
    LoadRekapPendapatanPengeluaranBySupir_New(asgLap,AdvProgressBar1,dtpAwal.Date, dtpAkhir.Date,seqArmada, seqSupir);
    asgLap.FixedCols := 1;
  end;
  ArrangeColSize;
end;

procedure TfrmLapPendapatanPengeluaranMobil.SetGrid;
begin
  asgLap.ClearNormalCells;
  asgLap.ColCount := 7;
  asgLap.RowCount := 3;
  asgLap.FixedCols := 0;
  asgLap.FixedRows := 2;

  asgLap.MergeCells(colNode,0,1,2);
  asgLap.Cells[colNode,0] := '[+]';
  asgLap.Cells[colNobody,0] := 'No. Body';
  asgLap.Cells[colNobody,1] := 'Tanggal';
  asgLap.Cells[colNopol,0]  := 'No. Polisi';
  asgLap.Cells[colNopol,1]  := 'Nomor';
  asgLap.MergeCells(colPendapatan,0,1,2);
  asgLap.Cells[colPendapatan, 0] := 'Pendapatan';
  asgLap.MergeCells(colPengeluaran,0,1,2);
  asgLap.Cells[colPengeluaran,0] := 'Pengeluaran';
  asgLap.MergeCells(colselisih,0,1,2);
  asgLap.Cells[colselisih,1] := 'Selisih';

  asgLap.MergeCells(asgLap.ColCount-1,0,1,2);
  ArrangeColSize;

end;

end.
