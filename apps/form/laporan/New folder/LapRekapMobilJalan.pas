unit LapRekapMobilJalan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, AdvOfficeButtons, StdCtrls, AdvEdit,
  AdvCombo, AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, OracleConnection, USystemMenu,
  UGeneral, UConst, URecord, UReport, StrUtils, DateUtils, UEngine, Gauges,
  AdvProgressBar;

type
  TfrmLapRekapMobilJalan = class(TForm)
    MainPanel: TPanel;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    panel: TPanel;
    btnExport: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    chbShowSF: TAdvOfficeCheckBox;
    btnEkspor: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    Label8: TLabel;
    txtJurusan: TAdvEdit;
    btnLOVjurusan: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label3: TLabel;
    dtpDari: TDateTimePicker;
    Label1: TLabel;
    dtpSampai: TDateTimePicker;
    asgRekap: TAdvStringGrid;
    Label2: TLabel;
    Label4: TLabel;
    txtNobody: TAdvEdit;
    btnLovArmada: TAdvGlowButton;
    AdvProgressBar1: TAdvProgressBar;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure chbShowSFClick(Sender: TObject);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnLOVArmadaClick(Sender: TObject);
    procedure btnLOVjurusanClick(Sender: TObject);
    procedure txtNoPolisiKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    DateNow : TDate;
    Seqjurusan, SeqArmada, IdMenu : integer;
    arArmada : arInteger;
    procedure ArrangeColSize;
    procedure Initform;
    procedure LoadData;
    procedure SetGrid;
  public
    { Public declarations }
    procedure Execute(AMenuID : integer);
  end;

var
  frmLapRekapMobilJalan: TfrmLapRekapMobilJalan;

implementation

uses MainMenu, LOV, LovMekanik;

{$R *.dfm}

const
  ColNo    = 0;
  Colnobody= 1;
  Colrit   = 2;

procedure TfrmLapRekapMobilJalan.ArrangeColSize;
begin
  asgRekap.AutoNumberCol(ColNo);
  asgRekap.AutoSizeColumns(True);
//  if GlobalSystemUser.AccessLevel <> UConst.LEVEL_DEVELOPER then begin
//    asgRekap.ColWidths[ColSeq] := 0;
//  end;
end;

procedure TfrmLapRekapMobilJalan.asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if ACol in[ColNo, colrit] then HAlign := taRightJustify;
end;

procedure TfrmLapRekapMobilJalan.asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmLapRekapMobilJalan.asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmLapRekapMobilJalan.btnCetakClick(Sender: TObject);
begin
  if (not BisaPrint(IdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Grid := asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Lap. Rekap Mobil Jalan');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := false;
  asgRekap.ColCount := asgRekap.ColCount-1;
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount+1;
  asgRekap.ColumnSize.Stretch := True;
  ArrangeColSize;
end;

procedure TfrmLapRekapMobilJalan.btnEksporClick(Sender: TObject);
begin
  if  (not BisaEkspor(idMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmLapRekapMobilJalan.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 87);
end;

procedure TfrmLapRekapMobilJalan.btnLoadClick(Sender: TObject);
begin
  if Seqjurusan = 0  then begin
    Inform('Jurusan belum dipilih.');
    btnlovJurusan.setfocus;
    exit;
  end else Loaddata;
end;

procedure TfrmLapRekapMobilJalan.btnLOVArmadaClick(Sender: TObject);
var i : integer;
begin
  txtNoBody.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  arArmada      := frmLov.ExecuteArmadachecked('' , boNone);
  if Length(arArmada) <> 0 then begin
    for i := 0  to Length(arArmada) do  begin
      txtNoBody.Text := txtNoBody.Text + ifthen((txtNoBody.Text <> '') and (EkstrakString(ListArmadaGlobal.Values[IntToStr(arArmada[i])],'#',2) <> '') ,', ')+ EkstrakString(ListArmadaGlobal.Values[IntToStr(arArmada[i])],'#',2);
    end;
  end;// else txtNoBody.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(arArmada)],'#',2);
end;

procedure TfrmLapRekapMobilJalan.btnLOVjurusanClick(Sender: TObject);
begin
  Seqjurusan := 0;
  txtJurusan.Text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  Seqjurusan := frmLov.Executejurusan('');
  if Seqjurusan <> 0 then begin
    txtJurusan.Text := EkstrakString(ListJurusanGlobal.Values[IntToStr(Seqjurusan)],'#',2);
  end;
end;

procedure TfrmLapRekapMobilJalan.btnResetClick(Sender: TObject);
begin
  InitForm;
  SetGrid;
end;

procedure TfrmLapRekapMobilJalan.chbShowSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbShowSF.Checked;
end;

procedure TfrmLapRekapMobilJalan.Execute(AMenuID: integer);
begin
  IdMenu := AMenuID;
  if (not BisaLihatLaporan(IdMenu)) and (not BisaPrint(IdMenu)) and (not BisaEkspor(IdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  DateNow := ServerNow;
  InitForm;
  SetGrid;
  Run(Self);
end;

procedure TfrmLapRekapMobilJalan.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapRekapMobilJalan.Initform;
begin
  Seqjurusan         := 0;
  SeqArmada          := 0;
  arArmada           := nil;
  dtpDari.Date       := StartOfTheMonth(DateNow);
  dtpSampai.Date     := DateNow;
  chbShowSF.Checked  := False;
//  txtNoPolisi.Text   := '';
  txtNoBody.Text     := '';
  txtJurusan.Text    := '';
  asgRekap.SearchFooter.Visible := chbShowSF.Checked;
end;

procedure TfrmLapRekapMobilJalan.LoadData;
begin
  asgRekap.ClearNormalCells;
  asgRekap.RowCount := 2;
  LoadRekapMobilJalan(asgRekap,AdvProgressBar1,dtpDari.date,dtpsampai.date,arArmada,Seqjurusan);
  ArrangeColSize;
end;

procedure TfrmLapRekapMobilJalan.SetGrid;
begin
  asgResetGrid(asgRekap, 2, 4, 1, 1);
  ArrangeColSize;
end;

procedure TfrmLapRekapMobilJalan.txtNoPolisiKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnLoad.Click;
end;

end.
