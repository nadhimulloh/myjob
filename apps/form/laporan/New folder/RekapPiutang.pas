unit RekapPiutang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficePager, ExtCtrls, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid,
  Gauges, ComCtrls, StdCtrls, AdvEdit, AdvPanel, AdvGlowButton, OracleConnection, UConst, URecord,
  UEngine, USystemMenu, UGeneral, DateUtils, StrUtils, AdoInt, AdvCombo;

type
  TfrmRekapPiutang = class(TForm)
    MainPanel: TPanel;
    AdvOfficePager1: TAdvOfficePager;
    pcgSupplier: TAdvOfficePage;
    pcgSupir: TAdvOfficePage;
    pcgPihak3: TAdvOfficePage;
    pcgAsset: TAdvOfficePage;
    pcgCustomer: TAdvOfficePage;
    pnlFilterPtgPihak3: TPanel;
    AdvPanel3: TAdvPanel;
    txtCariPtgPihak3: TAdvEdit;
    btnCariPtgPihak3: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label3: TLabel;
    Label4: TLabel;
    dtpAwalPtgPihak3: TDateTimePicker;
    dtpAkhirPtgPihak3: TDateTimePicker;
    btnLoadPtgPihak3: TAdvGlowButton;
    btnResetPtgPihak3: TAdvGlowButton;
    Panel4: TPanel;
    GaugePtgPihak3: TGauge;
    btnFilterPtgPihak3: TAdvGlowButton;
    btnEksporPtgPihak3: TAdvGlowButton;
    btnCetakPtgPihak3: TAdvGlowButton;
    asgRekapPtgPihak3: TAdvStringGrid;
    chbSFPtgPihak3: TAdvOfficeCheckBox;
    btnDetailPtgPihak3: TAdvGlowButton;
    chbTPtgPihak3: TAdvOfficeCheckBox;
    Panel3: TPanel;
    GaugePtgSupp: TGauge;
    btnFilterPtgSupp: TAdvGlowButton;
    btnEksporPtgSupp: TAdvGlowButton;
    btnCetakPtgSupp: TAdvGlowButton;
    asgRekapPtgSupp: TAdvStringGrid;
    chbSFPtgSupp: TAdvOfficeCheckBox;
    btnDetailPtgSupp: TAdvGlowButton;
    chbTPtgSupp: TAdvOfficeCheckBox;
    pnlFilterPtgSupp: TPanel;
    btnResetPtgSupp: TAdvGlowButton;
    btnLoadPtgSupp: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    txtCariPtgSupp: TAdvEdit;
    btnCariPtgSupp: TAdvGlowButton;
    btnCariPtgSupir: TAdvGlowButton;
    PnlStatus: TAdvPanel;
    Label1: TLabel;
    Label5: TLabel;
    dtpAwalPtgSupp: TDateTimePicker;
    dtpAkhirPtgSupp: TDateTimePicker;
    pnlFilterPtgAssetCust: TPanel;
    AdvPanel10: TAdvPanel;
    Label11: TLabel;
    Label6: TLabel;
    dtpAwalPtgAssetCust: TDateTimePicker;
    dtpAkhirPtgAssetCust: TDateTimePicker;
    AdvPanel11: TAdvPanel;
    txtCariPtgAssetCust: TAdvEdit;
    btnCariPtgAssetCust: TAdvGlowButton;
    btnLoadPtgAssetCust: TAdvGlowButton;
    btnResetPtgAssetCust: TAdvGlowButton;
    Panel10: TPanel;
    GaugePtgAssetCust: TGauge;
    btnFilterPtgAssetCust: TAdvGlowButton;
    btnEksporPtgAssetCust: TAdvGlowButton;
    btnCetakPtgAssetCust: TAdvGlowButton;
    asgRekapPtgAssetCust: TAdvStringGrid;
    chbSFPtgAssetCust: TAdvOfficeCheckBox;
    btnDetailPtgAssetCust: TAdvGlowButton;
    chbTPtgAssetCust: TAdvOfficeCheckBox;
    pnlFilterPtgCust: TPanel;
    AdvPanel16: TAdvPanel;
    Label17: TLabel;
    Label8: TLabel;
    dtpAwalPtgCust: TDateTimePicker;
    dtpAkhirPtgCust: TDateTimePicker;
    AdvPanel17: TAdvPanel;
    txtCariPtgCust: TAdvEdit;
    btnCariPtgCust: TAdvGlowButton;
    btnLoadPtgCust: TAdvGlowButton;
    btnResetPtgCust: TAdvGlowButton;
    Panel16: TPanel;
    GaugePtgCust: TGauge;
    btnFilterPtgCust: TAdvGlowButton;
    btnEksporPtgCust: TAdvGlowButton;
    btnCetakPtgCust: TAdvGlowButton;
    asgRekapPtgCust: TAdvStringGrid;
    chbSFPtgCust: TAdvOfficeCheckBox;
    btnDetailPtgCust: TAdvGlowButton;
    chbTPtgCust: TAdvOfficeCheckBox;
    pnlFilterPtgSupir: TPanel;
    AdvPanel4: TAdvPanel;
    txtCariPtgSupir: TAdvEdit;
    AdvPanel5: TAdvPanel;
    Label2: TLabel;
    Label7: TLabel;
    dtpAwalPtgSupir: TDateTimePicker;
    dtpAkhirPtgSupir: TDateTimePicker;
    Panel2: TPanel;
    GaugePtgSupir: TGauge;
    btnFilterPtgSupir: TAdvGlowButton;
    btnEksporPtgSupir: TAdvGlowButton;
    btnCetakPtgSupir: TAdvGlowButton;
    asgRekapPtgSupir: TAdvStringGrid;
    chbSFPtgSupir: TAdvOfficeCheckBox;
    btnDetailPtgSupir: TAdvGlowButton;
    chbTPtgSupir: TAdvOfficeCheckBox;
    btnLoadPtgSupir: TAdvGlowButton;
    btnSinkronSupp: TAdvGlowButton;
    btnSinkronSupir: TAdvGlowButton;
    btnSinkronRelasi: TAdvGlowButton;
    btnSinkronAsset: TAdvGlowButton;
    btnSinkronPariwisata: TAdvGlowButton;
    pcgPegawai: TAdvOfficePage;
    pnlFilterPegawai: TPanel;
    AdvPanel6: TAdvPanel;
    Label9: TLabel;
    Label10: TLabel;
    dtpAwalPegawai: TDateTimePicker;
    dtpAkhirPegawai: TDateTimePicker;
    AdvPanel7: TAdvPanel;
    txtPegawai: TAdvEdit;
    btnPegawai: TAdvGlowButton;
    btnLoadPtgPegawai: TAdvGlowButton;
    btnResetPtgPegawai: TAdvGlowButton;
    Panel5: TPanel;
    gaugePegawai: TGauge;
    btnFilterPegawai: TAdvGlowButton;
    btnEksporPegawai: TAdvGlowButton;
    btnCetakPegawai: TAdvGlowButton;
    asgRekapPegawai: TAdvStringGrid;
    chbSFPegawai: TAdvOfficeCheckBox;
    btnKartuPegawai: TAdvGlowButton;
    chbTPegawai: TAdvOfficeCheckBox;
    btnSinkronisasiPegawai: TAdvGlowButton;
    Label12: TLabel;
    Label13: TLabel;
    cmbTipePiutangSupir: TAdvComboBox;
    procedure asgRekapPtgSuppDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapPtgSupirDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapPtgPihak3DblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapPtgAssetCustDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapPtgCustDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapPtgCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPtgAssetCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPtgAssetCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPtgCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPtgCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgAssetCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgAssetCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnCariPtgPihak3Click(Sender: TObject);
    procedure btnCariPtgCustClick(Sender: TObject);
    procedure btnCariPtgSupirClick(Sender: TObject);
    procedure btnCariPtgSuppClick(Sender: TObject);
    procedure btnCariPtgAssetCustClick(Sender: TObject);
    procedure btnFilterPtgPihak3Click(Sender: TObject);
    procedure btnEksporPtgPihak3Click(Sender: TObject);
    procedure btnCetakPtgPihak3Click(Sender: TObject);
    procedure btnDetailPtgPihak3Click(Sender: TObject);
    procedure btnFilterPtgAssetCustClick(Sender: TObject);
    procedure btnFilterPtgCustClick(Sender: TObject);
    procedure btnFilterPtgSupirClick(Sender: TObject);
    procedure btnFilterPtgSuppClick(Sender: TObject);
    procedure btnEksporPtgSuppClick(Sender: TObject);
    procedure btnEksporPtgSupirClick(Sender: TObject);
    procedure btnEksporPtgAssetCustClick(Sender: TObject);
    procedure btnEksporPtgCustClick(Sender: TObject);
    procedure btnCetakPtgAssetCustClick(Sender: TObject);
    procedure btnCetakPtgCustClick(Sender: TObject);
    procedure btnCetakPtgSupirClick(Sender: TObject);
    procedure btnCetakPtgSuppClick(Sender: TObject);
    procedure btnResetPtgSuppClick(Sender: TObject);
    procedure btnResetPtgSupirClick(Sender: TObject);
    procedure btnResetPtgPihak3Click(Sender: TObject);
    procedure btnResetPtgAssetCustClick(Sender: TObject);
    procedure btnResetPtgCustClick(Sender: TObject);
    procedure btnLoadPtgSuppClick(Sender: TObject);
    procedure btnLoadPtgSupirClick(Sender: TObject);
    procedure btnLoadPtgPihak3Click(Sender: TObject);
    procedure btnLoadPtgAssetCustClick(Sender: TObject);
    procedure btnLoadPtgCustClick(Sender: TObject);
    procedure btnDetailPtgSuppClick(Sender: TObject);
    procedure btnDetailPtgSupirClick(Sender: TObject);
    procedure btnDetailPtgAssetCustClick(Sender: TObject);
    procedure btnDetailPtgCustClick(Sender: TObject);
    procedure chbSFPtgSuppClick(Sender: TObject);
    procedure chbSFPtgCustClick(Sender: TObject);
    procedure chbSFPtgSupirClick(Sender: TObject);
    procedure chbSFPtgPihak3Click(Sender: TObject);
    procedure chbSFPtgAssetCustClick(Sender: TObject);
    procedure btnSinkronSuppClick(Sender: TObject);
    procedure btnSinkronRelasiClick(Sender: TObject);
    procedure btnSinkronAssetClick(Sender: TObject);
    procedure btnSinkronPariwisataClick(Sender: TObject);
    procedure btnSinkronSupirClick(Sender: TObject);
    procedure asgRekapPegawaiDblClickCell(Sender: TObject; ARow,
      ACol: Integer);
    procedure asgRekapPegawaiGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapPegawaiGetFloatFormat(Sender: TObject; ACol,
      ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapPegawaiMouseWheelDown(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapPegawaiMouseWheelUp(Sender: TObject;
      Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnLoadPtgPegawaiClick(Sender: TObject);
    procedure btnResetPtgPegawaiClick(Sender: TObject);
    procedure btnPegawaiClick(Sender: TObject);
    procedure btnFilterPegawaiClick(Sender: TObject);
    procedure btnEksporPegawaiClick(Sender: TObject);
    procedure btnCetakPegawaiClick(Sender: TObject);
    procedure chbSFPegawaiClick(Sender: TObject);
    procedure btnKartuPegawaiClick(Sender: TObject);
    procedure btnSinkronisasiPegawaiClick(Sender: TObject);
  private
    { Private declarations }
    DateNow : TDate;
    AIdMenu : integer;
    TipePihak3 : String;
    SeqSupp, SeqSupir, SeqPihak3, SeqAsset, SeqDPCust, SeqPegawai : integer;
    procedure InitForm(idx : integer);
    procedure LoadData(AIndex : integer);
    procedure SetXceling(AIndex: Integer);
    procedure SetPrinting(AIndex: Integer);
    procedure SetGrid(AGrid : TAdvStringGrid);
    procedure ArrangeColSize(AGrid : TAdvStringGrid);
  public
    { Public declarations }
    procedure Execute(AMenuId : integer);
  end;

var
  frmRekapPiutang: TfrmRekapPiutang;

implementation

uses MainMenu, UReport, LOV, LOVRelasi, KartuPiutang, Sinkronisasi,
  SinkronisasiPiutang;

{$R *.dfm}

const
  ColNo         = 0;
  ColKode       = 1;
  ColNama       = 2;
  ColAwal       = 3;
  ColBeli       = 4;
  ColPendapatan = 5;
  ColLunas      = 6;
  ColBiaya      = 7;
  ColAkhir      = 8;
  ColSeq        = 9;

  {Pegawai}
  ColLunasPeg   = 5;
  ColAkhirPeg   = 6;
  ColSeqPeg     = 7;


  Msg = 'Apakah sudah yakin?';

procedure TfrmRekapPiutang.asgRekapPegawaiDblClickCell(Sender: TObject;
  ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapPegawai.rowcount-1) then btnKartuPegawai.Click;
end;

procedure TfrmRekapPiutang.asgRekapPegawaiGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColLunasPeg, ColAkhirPeg] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapPiutang.asgRekapPegawaiGetFloatFormat(Sender: TObject;
  ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColLunasPeg, ColAkhirPeg] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else FloatFormat := '%.2n'
  end else isFloat := False;
end;

procedure TfrmRekapPiutang.asgRekapPegawaiMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin                
  if Shift <> [] then ArrangeColSize(asgRekapPegawai);
end;

procedure TfrmRekapPiutang.asgRekapPegawaiMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin                 
  if Shift <> [] then ArrangeColSize(asgRekapPegawai);
end;

procedure TfrmRekapPiutang.ArrangeColSize(AGrid: TAdvStringGrid);
begin            
  if AGrid = asgRekapPegawai then begin
    AGrid.ClearRows(AGrid.RowCount-1,1);
    AGrid.AutoNumberCol(ColNo);
    AGrid.Cells[ColNama, AGrid.RowCount-1]     := 'Total';
    AGrid.FloatFormat := '%.2n';
    AGrid.FloatingFooter.ColumnCalc[ColAwal]       := acSum;
    AGrid.FloatingFooter.ColumnCalc[COlAkhirPeg]   := acSum;
    AGrid.FloatingFooter.ColumnCalc[ColBeli]       := acSum;
    AGrid.FloatingFooter.ColumnCalc[ColLunasPeg]   := acSum;
    AGrid.AutoSizeColumns(True, 10);
    if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
      AGrid.ColWidths[ColSeqPeg] := 0;
    end;
  end else begin
    AGrid.ClearRows(AGrid.RowCount-1,1);
    AGrid.AutoNumberCol(ColNo);
    AGrid.Cells[ColNama, AGrid.RowCount-1]     := 'Total';
    AGrid.FloatFormat := '%.2n';
    AGrid.FloatingFooter.ColumnCalc[ColAwal]       := acSum;
    AGrid.FloatingFooter.ColumnCalc[COlAkhir]      := acSum;
    AGrid.FloatingFooter.ColumnCalc[ColBeli]       := acSum;
    AGrid.FloatingFooter.ColumnCalc[ColBiaya]      := acSum;
    AGrid.FloatingFooter.ColumnCalc[ColLunas]      := acSum;
    AGrid.FloatingFooter.ColumnCalc[colPendapatan] := acSum;
    AGrid.AutoSizeColumns(True, 10);
    if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
      AGrid.ColWidths[ColSeq] := 0;
    end;
    if AGrid = asgRekapPtgSupir then begin
      AGrid.ColWidths[ColBiaya] := 0;
      AGrid.ColWidths[ColPendapatan] := 0;
    end;
  end;
end;

procedure TfrmRekapPiutang.asgRekapPtgAssetCustDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapPtgAssetCust.rowcount-1) then btnDetailPtgAssetCust.Click;
end;

procedure TfrmRekapPiutang.asgRekapPtgAssetCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapPiutang.asgRekapPtgAssetCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n'
  end else isFloat := False;
end;

procedure TfrmRekapPiutang.asgRekapPtgAssetCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgAssetCust);
end;

procedure TfrmRekapPiutang.asgRekapPtgAssetCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgAssetCust);
end;

procedure TfrmRekapPiutang.asgRekapPtgCustDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapPtgCust.rowcount-1) then btnDetailPtgCust.Click;
end;

procedure TfrmRekapPiutang.asgRekapPtgCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapPiutang.asgRekapPtgCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapPiutang.asgRekapPtgCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgCust);
end;

procedure TfrmRekapPiutang.asgRekapPtgCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgCust);
end;

procedure TfrmRekapPiutang.asgRekapPtgPihak3DblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapPtgPihak3.rowcount-1) then btnDetailPtgPihak3.Click;
end;

procedure TfrmRekapPiutang.asgRekapPtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapPiutang.asgRekapPtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapPiutang.asgRekapPtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgPihak3);
end;

procedure TfrmRekapPiutang.asgRekapPtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgPihak3);
end;

procedure TfrmRekapPiutang.asgRekapPtgSupirDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapPtgSupir.rowcount-1) then btnDetailPtgSupir.Click;
end;

procedure TfrmRekapPiutang.asgRekapPtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapPiutang.asgRekapPtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapPiutang.asgRekapPtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgSupir);
end;

procedure TfrmRekapPiutang.asgRekapPtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgSupir);
end;

procedure TfrmRekapPiutang.asgRekapPtgSuppDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapPtgSupp.rowcount-1) then btnDetailPtgSupp.Click;
end;

procedure TfrmRekapPiutang.asgRekapPtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapPiutang.asgRekapPtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapPiutang.asgRekapPtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgSupp);
end;

procedure TfrmRekapPiutang.asgRekapPtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapPtgSupp);
end;

procedure TfrmRekapPiutang.btnCariPtgAssetCustClick(Sender: TObject);
begin
  SeqAsset       := 0;
  txtCariPtgAssetCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqAsset        := frmLov.ExecuteCustomer('',boNone);
  if SeqAsset <> 0 then begin
    txtCariPtgAssetCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqAsset)],'#',2);
  end;
end;

procedure TfrmRekapPiutang.btnCariPtgCustClick(Sender: TObject);
begin
  SeqDPCust      := 0;
  txtCariPtgCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqDPCust      := frmLOV.ExecuteCustomer('',BoNone);
  if SeqDPCust <> 0 then begin
    txtCariPtgCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqDPCust)],'#',2);
  end;
end;

procedure TfrmRekapPiutang.btnCariPtgSupirClick(Sender: TObject);
begin
  SeqSupir       := 0;
  txtCariPtgSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupir       := frmLov.ExecuteSupir('',boNone);
  if SeqSupir <> 0 then begin
    txtCariPtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
  end;
end;

procedure TfrmRekapPiutang.btnCariPtgSuppClick(Sender: TObject);
begin
  SeqSupp        := 0;
  txtCariPtgSupp.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupp        := frmLov.ExecuteSupplier('',boNone);
  if SeqSupp <> 0 then begin
    txtCariPtgSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqSupp)],'#',2);
  end;
end;

procedure TfrmRekapPiutang.btnCariPtgPihak3Click(Sender: TObject);
var vTemp : string;
begin
  Application.CreateForm(TfrmLovRelasi, frmLovRelasi);
  frmLovRelasi.OnClose := frmMainMenu.ChildFormSingle;
  vTemp := frmLovRelasi.ExecuteGabungan('',false,true);
  if vTemp <> '' then begin
    TipePihak3 := EkstrakString(vTemp, '#', 1);
    SeqPihak3  := StrToIntDef(EkstrakString(vTemp, '#', 2),0);
    if TipePihak3 = UConst.VC_SUPPLIER then begin
      txtCariPtgPihak3.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end else begin
      txtCariPtgPihak3.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end;
  end else begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    txtCariPtgPihak3.Text := '';
  end;
end;

procedure TfrmRekapPiutang.btnCetakPegawaiClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnCetakPtgAssetCustClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnCetakPtgCustClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnCetakPtgPihak3Click(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnCetakPtgSupirClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnCetakPtgSuppClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnDetailPtgAssetCustClick(Sender: TObject);
begin
  if (asgRekapPtgAssetCust.Cells[ColSeq, asgRekapPtgAssetCust.Row] <> '') or (asgRekapPtgAssetCust.Ints[ColSeq, asgRekapPtgAssetCust.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuPiutang, frmKartuPiutang);
    frmKartuPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuPiutang.Execute(509, asgRekapPtgAssetCust.Ints[ColSeq, asgRekapPtgAssetCust.Row],AdvOfficePager1.ActivePageIndex, dtpAwalPtgAssetCust.Date, dtpAkhirPtgAssetCust.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadPtgAssetCust.SetFocus;
  end;
end;

procedure TfrmRekapPiutang.btnDetailPtgCustClick(Sender: TObject);
begin
  if (asgRekapPtgCust.Cells[ColSeq, asgRekapPtgCust.Row] <> '') or (asgRekapPtgCust.Ints[ColSeq, asgRekapPtgCust.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuPiutang, frmKartuPiutang);
    frmKartuPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuPiutang.Execute(506, asgRekapPtgCust.Ints[ColSeq, asgRekapPtgCust.Row],AdvOfficePager1.ActivePageIndex, dtpAwalPtgCust.Date, dtpAkhirPtgCust.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadPtgCust.SetFocus;
  end;
end;

procedure TfrmRekapPiutang.btnDetailPtgPihak3Click(Sender: TObject);
begin
  if (asgRekapPtgPihak3.Cells[ColSeq, asgRekapPtgPihak3.Row] <> '') or (asgRekapPtgPihak3.Ints[ColSeq, asgRekapPtgPihak3.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuPiutang, frmKartuPiutang);
    frmKartuPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuPiutang.Execute(506, StrToInt(EkstrakString(asgRekapPtgPihak3.Cells[ColSeq, asgRekapPtgPihak3.Row],'#',1)),AdvOfficePager1.ActivePageIndex, dtpAwalPtgPihak3.Date, dtpAkhirPtgPihak3.Date, EkstrakString(asgRekapPtgPihak3.Cells[ColSeq, asgRekapPtgPihak3.Row],'#',2));
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadPtgPihak3.SetFocus;
  end;
end;

procedure TfrmRekapPiutang.btnDetailPtgSupirClick(Sender: TObject);
var ATipe : integer;
begin
  if (asgRekapPtgSupir.Cells[ColSeq, asgRekapPtgSupir.Row] <> '') or (asgRekapPtgSupir.Ints[ColSeq, asgRekapPtgSupir.Row] <> 0) then begin
    ATipe := cmbTipePiutangSupir.ItemIndex;
    Application.CreateForm(TfrmKartuPiutang, frmKartuPiutang);
    frmKartuPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuPiutang.Execute(506, asgRekapPtgSupir.Ints[ColSeq, asgRekapPtgSupir.Row],AdvOfficePager1.ActivePageIndex, dtpAwalPtgSupir.Date, dtpAkhirPtgSupir.Date, '', ATipe);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadPtgSupir.SetFocus;
  end;
end;

procedure TfrmRekapPiutang.btnDetailPtgSuppClick(Sender: TObject);
begin
  if (asgRekapPtgSupp.Cells[ColSeq, asgRekapPtgSupp.Row] <> '') or (asgRekapPtgSupp.Ints[ColSeq, asgRekapPtgSupp.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuPiutang, frmKartuPiutang);
    frmKartuPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuPiutang.Execute(506, asgRekapPtgSupp.Ints[ColSeq, asgRekapPtgSupp.Row],AdvOfficePager1.ActivePageIndex, dtpAwalPtgSupp.Date, dtpAkhirPtgSupp.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadPtgSupp.SetFocus;
  end;
end;

procedure TfrmRekapPiutang.btnEksporPegawaiClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnEksporPtgAssetCustClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnEksporPtgCustClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnEksporPtgPihak3Click(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnEksporPtgSupirClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnEksporPtgSuppClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapPiutang.btnFilterPegawaiClick(Sender: TObject);
begin        
  SetFilterSizeAdv(pnlFilterPegawai, btnFilterPegawai, 92);
end;

procedure TfrmRekapPiutang.btnFilterPtgAssetCustClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterPtgAssetCust, btnFilterPtgAssetCust, 92);
end;

procedure TfrmRekapPiutang.btnFilterPtgCustClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterPtgCust, btnFilterPtgCust, 92);
end;

procedure TfrmRekapPiutang.btnFilterPtgSupirClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterPtgSupir, btnFilterPtgSupir, 92);
end;

procedure TfrmRekapPiutang.btnFilterPtgSuppClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterPtgSupp, btnFilterPtgSupp, 92);
end;

procedure TfrmRekapPiutang.btnKartuPegawaiClick(Sender: TObject);
begin
  if (asgRekapPegawai.Cells[ColSeqPeg, asgRekapPegawai.Row] <> '') or (asgRekapPegawai.Ints[ColSeqPeg, asgRekapPegawai.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuPiutang, frmKartuPiutang);
    frmKartuPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuPiutang.Execute(506, asgRekapPegawai.Ints[ColSeqPeg, asgRekapPegawai.Row],AdvOfficePager1.ActivePageIndex, dtpAwalPegawai.Date, dtpAkhirPegawai.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadPtgPegawai.SetFocus;
  end;
end;

procedure TfrmRekapPiutang.btnLoadPtgAssetCustClick(Sender: TObject);
begin
  if dtpAkhirPtgAssetCust.Date >= dtpAwalPtgAssetCust.Date then begin
    LoadData(3);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirPtgAssetCust.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapPiutang.btnLoadPtgCustClick(Sender: TObject);
begin
  if dtpAkhirPtgCust.Date >= dtpAwalPtgCust.Date then begin
    LoadData(4);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirPtgCust.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapPiutang.btnLoadPtgPegawaiClick(Sender: TObject);
begin
  if dtpAkhirPegawai.Date >= dtpAwalPegawai.Date then begin
    LoadData(5);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirPegawai.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapPiutang.btnLoadPtgPihak3Click(Sender: TObject);
begin
  if dtpAkhirPtgPihak3.Date >= dtpAwalPtgPihak3.Date then begin
    LoadData(2);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirPtgPihak3.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapPiutang.btnLoadPtgSupirClick(Sender: TObject);
begin
  if dtpAkhirPtgSupir.Date >= dtpAwalPtgSupir.Date then begin
    LoadData(1);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirPtgSupir.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapPiutang.btnLoadPtgSuppClick(Sender: TObject);
begin
  if dtpAkhirPtgSupp.Date >= dtpAwalPtgSupp.Date then begin
    LoadData(0);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirPtgSupp.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapPiutang.btnPegawaiClick(Sender: TObject);
var Mst : TR_Master_pegawai;
begin
  txtPegawai.Text := '';
  Application.CreateForm(TfrmLov, frmlov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqPegawai := frmLov.ExecutePegawai('',BoNone);
  if SeqPegawai <> 0 then begin
    if ListGlobalPegawai.Values[IntToStr(SeqPegawai)] = '' then begin
      //jika gk ada dilist global ambil pegawai dan masukan list
      Mst := Get_Master_pegawai(SeqPegawai);
      txtPegawai.Text := Mst.nama;
      Update_List_Global_Master_Pegawai(Mst);
    end else begin
      txtPegawai.Text := EkstrakString(ListGlobalPegawai.Values[IntToStr(SeqPegawai)],'#',2);
    end;
  end;
end;

procedure TfrmRekapPiutang.btnResetPtgAssetCustClick(Sender: TObject);
begin
  InitForm(4);
  SetGrid(asgRekapPtgAssetCust);
end;

procedure TfrmRekapPiutang.btnResetPtgCustClick(Sender: TObject);
begin
  InitForm(5);
  SetGrid(asgRekapPtgCust);
end;

procedure TfrmRekapPiutang.btnResetPtgPegawaiClick(Sender: TObject);
begin
  initform(6);
  SetGrid(asgRekapPegawai);
end;

procedure TfrmRekapPiutang.btnResetPtgPihak3Click(Sender: TObject);
begin
  InitForm(3);
  SetGrid(asgRekapPtgPihak3);
end;

procedure TfrmRekapPiutang.btnResetPtgSupirClick(Sender: TObject);
begin
  InitForm(2);
  SetGrid(asgRekapPtgSupir);
end;

procedure TfrmRekapPiutang.btnResetPtgSuppClick(Sender: TObject);
begin
  InitForm(1);
  SetGrid(asgRekapPtgSupp);
end;

procedure TfrmRekapPiutang.btnSinkronAssetClick(Sender: TObject);
var aseq : integer;
begin
  aSeq := 0;
  if (asgRekapPtgAssetCust.Cells[ColSeq, asgRekapPtgAssetCust.Row] <> '') and (asgRekapPtgAssetCust.Row > 0) then begin
    aSeq := asgRekapPtgAssetCust.Ints[ColSeq, asgRekapPtgAssetCust.Row];
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.Execute4PiutAsset(StartOfTheMonth(dtpAwalPtgAssetCust.Date)-1, aSeq);
end;

procedure TfrmRekapPiutang.btnSinkronisasiPegawaiClick(Sender: TObject);
var aseq : integer;
begin
  aSeq := 0;
  if (asgRekapPegawai.Cells[ColSeqPeg, asgRekapPegawai.Row] <> '') and (asgRekapPegawai.Row > 0) then begin
    aSeq := asgRekapPegawai.Ints[ColSeqPeg, asgRekapPegawai.Row];
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.Execute4Pegawai(StartOfTheMonth(dtpAwalPtgCust.Date)-1, aSeq);
end;

procedure TfrmRekapPiutang.btnSinkronPariwisataClick(Sender: TObject);
var aseq : integer;
begin
  aSeq := 0;
  if (asgRekapPtgCust.Cells[ColSeq, asgRekapPtgCust.Row] <> '') and (asgRekapPtgCust.Row > 0) then begin
    aSeq := asgRekapPtgCust.Ints[ColSeq, asgRekapPtgCust.Row];
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.Execute4PiutPariwisata(StartOfTheMonth(dtpAwalPtgCust.Date)-1, aSeq);
end;


procedure TfrmRekapPiutang.btnSinkronRelasiClick(Sender: TObject);
var aseq : integer;
    Tipe : String;
begin
  aSeq := 0;
  Tipe := '';
  if (asgRekapPtgPihak3.Cells[ColSeq, asgRekapPtgPihak3.Row] <> '') and (asgRekapPtgPihak3.Row > 0) then begin
    aSeq := StrToIntDef(Ekstrakstring(asgRekapPtgPihak3.cells[ColSeq,asgRekapPtgPihak3.Row],'#',1),0);
    Tipe := Ekstrakstring(asgRekapPtgPihak3.cells[ColSeq,asgRekapPtgPihak3.Row],'#',2);
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.Execute4PiutRelasi(StartOfTheMonth(dtpAwalPtgPihak3.Date)-1, aSeq,Tipe);
end;

procedure TfrmRekapPiutang.btnSinkronSupirClick(Sender: TObject);
var aseq : integer;
    tipe : string;
begin
//  if cmbTipePiutangSupir.ItemIndex = 0 then begin
//    inform('Tipe Piutang harus dipilih.');
//    cmbTipePiutangSupir.SetFocus;
//    exit;
//  end;
//
//  if cmbTipePiutangSupir.ItemIndex = 1 then tipe := SALDO_PIUTANG_KS
//  else tipe := SALDO_PIUTANG_SETORAN;


  if (asgRekapPtgSupir.Cells[ColSeq, asgRekapPtgSupir.Row] <> '') and (asgRekapPtgSupir.Row > 0) then begin
    aSeq := asgRekapPtgSupir.Ints[ColSeq, asgRekapPtgSupir.Row];
  end else aSeq := 0;
  Application.CreateForm(TfrmSinkronisasipiutang, frmSinkronisasipiutang);
  frmSinkronisasipiutang.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasipiutang.Execute4PiutSupir(StartOfTheMonth(dtpAwalPtgSupir.Date)-1, aSeq, tipe);
end;

procedure TfrmRekapPiutang.btnSinkronSuppClick(Sender: TObject);
var aseq : integer;
begin  
  aSeq := 0;
  if (asgRekapPtgSupp.Cells[ColSeq, asgRekapPtgSupp.Row] <> '') and (asgRekapPtgSupp.Row > 0) then begin
    aSeq := asgRekapPtgSupp.Ints[ColSeq, asgRekapPtgSupp.Row];
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.Execute4PiutSupplier(StartOfTheMonth(dtpAwalPtgAssetCust.Date)-1, aSeq);
end;

procedure TfrmRekapPiutang.chbSFPegawaiClick(Sender: TObject);
begin                                                
  asgRekapPegawai.SearchFooter.Visible := chbSFPegawai.Checked;
end;

procedure TfrmRekapPiutang.chbSFPtgAssetCustClick(Sender: TObject);
begin
  asgRekapPtgAssetCust.SearchFooter.Visible := chbSFPtgAssetCust.Checked;
end;

procedure TfrmRekapPiutang.chbSFPtgCustClick(Sender: TObject);
begin
  asgRekapPtgCust.SearchFooter.Visible := chbSFPtgCust.Checked;
end;

procedure TfrmRekapPiutang.chbSFPtgPihak3Click(Sender: TObject);
begin
  asgRekapPtgPihak3.SearchFooter.Visible := chbSFPtgPihak3.Checked;
end;

procedure TfrmRekapPiutang.chbSFPtgSupirClick(Sender: TObject);
begin
  asgRekapPtgSupir.SearchFooter.Visible := chbSFPtgSupir.Checked;
end;

procedure TfrmRekapPiutang.chbSFPtgSuppClick(Sender: TObject);
begin
  asgRekapPtgSupp.SearchFooter.Visible := chbSFPtgSupp.Checked;
end;

procedure TfrmRekapPiutang.btnFilterPtgPihak3Click(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterPtgPihak3, btnFilterPtgPihak3, 92);
end;

procedure TfrmRekapPiutang.Execute(AMenuId: integer);
begin
  AIdMenu := AMenuId;
  if (not BisaLihatLaporan(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  
  DateNow := ServerNow;
  AdvOfficePager1.ActivePageIndex := 0;
  InitForm(0);
  SetGrid(asgRekapPtgSupp);
  SetGrid(asgRekapPtgSupir);
  SetGrid(asgRekapPtgPihak3);
  SetGrid(asgRekapPtgAssetCust);
  SetGrid(asgRekapPtgCust);               
  SetGrid(asgRekapPegawai);

  Run(Self);
end;

procedure TfrmRekapPiutang.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuID);
end;

procedure TfrmRekapPiutang.InitForm(idx: integer);
begin
  if (idx = 0) or (idx = 1) then begin
    SeqSupp := 0;
    txtCariPtgSupp.Text       := '';
    chbTPtgSupp.Checked       := False;
    chbSFPtgSupp.Checked      := False;
    dtpAwalPtgSupp.Date       := StartOfTheMonth(DateNow);
    dtpAkhirPtgSupp.Date      := DateNow;
    asgRekapPtgSupp.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 2) then begin
    SeqSupir := 0;
    txtCariPtgSupir.Text        := '';
    chbTPtgSupir.Checked        := False;
    chbSFPtgSupir.Checked       := False;
    dtpAwalPtgSupir.Date        := StartOfTheMonth(DateNow);
    dtpAkhirPtgSupir.Date       := DateNow;
    asgRekapPtgSupir.SearchFooter.Visible := False;
    cmbTipePiutangSupir.ItemIndex := 0;
  end;
  if (idx = 0) or (idx = 3) then begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    txtCariPtgPihak3.Text       := '';
    chbTPtgPihak3.Checked       := False;
    chbSFPtgPihak3.Checked      := False;
    dtpAwalPtgPihak3.Date       := StartOfTheMonth(DateNow);
    dtpAkhirPtgPihak3.Date      := DateNow;
    asgRekapPtgPihak3.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 4) then begin
    SeqAsset := 0;
    txtCariPtgAssetCust.Text    := '';
    chbTPtgAssetCust.Checked    := False;
    chbSFPtgAssetCust.Checked   := False;
    dtpAwalPtgAssetCust.Date    := StartOfTheMonth(DateNow);
    dtpAkhirPtgAssetCust.Date   := DateNow;
    asgRekapPtgAssetCust.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 5) then begin
    SeqDPCust := 0;
    txtCariPtgCust.Text       := '';
    chbTPtgCust.Checked       := False;
    chbSFPtgCust.Checked      := False;
    dtpAwalPtgCust.Date       := StartOfTheMonth(DateNow);
    dtpAkhirPtgCust.Date      := DateNow;
    asgRekapPtgCust.SearchFooter.Visible := False;
  end;
  if (idx = 0) or (idx = 6) then begin
    SeqPegawai := 0;
    txtPegawai.Text           := '';
    chbTPegawai.Checked       := False;
    chbSFPegawai.Checked      := False;
    dtpAwalPegawai.Date       := StartOfTheMonth(DateNow);
    dtpAkhirPegawai.Date      := DateNow;
    asgRekapPegawai.SearchFooter.Visible := False;
  end;

end;

procedure TfrmRekapPiutang.LoadData(AIndex: integer);
var tipe, tipeIn : string;
begin
  case AIndex of
    0 : begin
        asgRekapPtgSupp.ClearNormalCells;
        asgRekapPtgSupp.RowCount := 3;
        asgRekapPtgSupp.ClearRows(asgRekapPtgSupp.RowCount-1, 1);

        LoadRekapPiutang(asgRekapPtgSupp, GaugePtgSupp, dtpAwalPtgSupp.date, dtpAkhirPtgSupp.Date, SeqSupp,
                  SALDO_PIUTANG_DP_SUPP, chbTPtgSupp.checked);
        ArrangeColSize(asgRekapPtgSupp);
    end;
    1 : begin
        tipeIn := '';
        if cmbTipePiutangSupir.ItemIndex = 0 then begin                                             // ditutup dzikri 281217 yg sisa setoran jd gausah nampil, jd pending
          tipeIn := FormatSQLString(SALDO_PIUTANG_KS)+', '+FormatSQLString(SALDO_PIUTANG_SETORAN);//+', '+FormatSQLString(SALDO_PIUTANG_SISA_SETORAN);
          tipe := '';
        end else if cmbTipePiutangSupir.ItemIndex = 1 then tipe := SALDO_PIUTANG_KS
        else if cmbTipePiutangSupir.ItemIndex = 2 then tipe := SALDO_PIUTANG_SETORAN;
        //else tipe := SALDO_PIUTANG_SISA_SETORAN;

        asgRekapPtgSupir.ClearNormalCells;
        asgRekapPtgSupir.RowCount := 3;
        asgRekapPtgSupir.ClearRows(asgRekapPtgSupir.RowCount-1, 1);

        LoadRekapPiutang(asgRekapPtgSupir, GaugePtgSupir, dtpAwalPtgSupir.date, dtpAkhirPtgSupir.Date, SeqSupir,
                  tipe, chbTPtgSupir.checked, tipeIn);
        ArrangeColSize(asgRekapPtgSupir);
    end;
    2 : begin
        asgRekapPtgPihak3.ClearNormalCells;
        asgRekapPtgPihak3.RowCount := 3;
        asgRekapPtgPihak3.ClearRows(asgRekapPtgPihak3.RowCount-1, 1);

        if TipePihak3 = VC_SUPPLIER then begin
          LoadRekapPiutang(asgRekapPtgPihak3, GaugePtgPihak3, dtpAwalPtgPihak3.date, dtpAkhirPtgPihak3.Date, SeqPihak3,
                  SALDO_PIUTANG_PINJAMAN_SUPP, chbTPtgPihak3.checked);
        end else if TipePihak3 = VC_CUSTOMER then begin
          LoadRekapPiutang(asgRekapPtgPihak3, GaugePtgPihak3, dtpAwalPtgPihak3.date, dtpAkhirPtgPihak3.Date, SeqPihak3,
                  SALDO_PIUTANG_PINJAMAN_CUST, chbTPtgPihak3.checked);
        end else begin
          LoadRekapPiutang(asgRekapPtgPihak3, GaugePtgPihak3, dtpAwalPtgPihak3.date, dtpAkhirPtgPihak3.Date, SeqPihak3,
                  FormatSQLString(SALDO_PIUTANG_PINJAMAN_SUPP)+', '+FormatSQLString(SALDO_PIUTANG_PINJAMAN_CUST), chbTPtgPihak3.checked);
        end;
        ArrangeColSize(asgRekapPtgPihak3);
    end;
    3 : begin
        asgRekapPtgAssetCust.ClearNormalCells;
        asgRekapPtgAssetCust.RowCount := 3;
        asgRekapPtgAssetCust.ClearRows(asgRekapPtgAssetCust.RowCount-1, 1);

        LoadRekapPiutang(asgRekapPtgAssetCust, GaugePtgAssetCust, dtpAwalPtgAssetCust.date, dtpAkhirPtgAssetCust.Date, SeqAsset,
                  SALDO_PIUTANG_ASSET_CUST, chbTPtgAssetCust.checked);
        ArrangeColSize(asgRekapPtgAssetCust);
    end;
    4 : begin
        asgRekapPtgCust.ClearNormalCells;
        asgRekapPtgCust.RowCount := 3;
        asgRekapPtgCust.ClearRows(asgRekapPtgCust.RowCount-1, 1);

        LoadRekapPiutang(asgRekapPtgCust, GaugePtgCust, dtpAwalPtgCust.date, dtpAkhirPtgCust.Date, SeqDPCust,
                  SALDO_PIUTANG_PARIWISATA, chbTPtgCust.Checked);
        ArrangeColSize(asgRekapPtgCust);
    end;
    5 : begin
        asgRekapPegawai.ClearNormalCells;
        asgRekapPegawai.RowCount := 3;
        asgRekapPegawai.ClearRows(asgRekapPegawai.RowCount-1, 1);

        LoadRekapPiutang(asgRekapPegawai, gaugePegawai, dtpAwalPegawai.date, dtpAkhirPegawai.Date, SeqPegawai,
                         SALDO_KASBON, chbTPegawai.Checked);
        ArrangeColSize(asgRekapPegawai);
    end;
  end;
end;

procedure TfrmRekapPiutang.SetGrid(AGrid: TAdvStringGrid);
begin
  if AGrid = asgRekapPegawai then begin
    AGrid.ClearNormalCells;
    asgResetGrid(AGrid,3,9,1,1);
    ArrangeColSize(AGrid);
  end else begin
    AGrid.ClearNormalCells;
    asgResetGrid(AGrid,3,11,1,1);
    ArrangeColSize(AGrid);
  end;
end;

procedure TfrmRekapPiutang.SetPrinting(AIndex: Integer);
  procedure PreparePrint(AGrid: TAdvStringGrid);
  begin
//    AGrid.ClearNormalCells;
    myConnection.advPrint.Grid:= AGrid;
    AGrid.PrintSettings.TitleLines.Clear;
    AGrid.PrintSettings.TitleLines.Add('');
    if AGrid = asgRekapPtgSupp then AGrid.PrintSettings.TitleLines.Add('Rekap Piutang DP Supplier')
    else if AGrid = asgRekapPtgSupir then AGrid.PrintSettings.TitleLines.Add('Rekap Piutang Supir')
    else if AGrid = asgRekapPtgPihak3 then AGrid.PrintSettings.TitleLines.Add('Rekap Piutang Relasi')
    else if AGrid = asgRekapPtgAssetCust then AGrid.PrintSettings.TitleLines.Add('Rekap Piutang Asset')
    else if AGrid = asgRekapPtgCust then AGrid.PrintSettings.TitleLines.Add('Rekap Piutang DP Pariwisata')   
    else if AGrid = asgRekapPegawai then AGrid.PrintSettings.TitleLines.Add('Rekap Piutang Pegawai');
    AGrid.PrintSettings.TitleLines.Add('');
    if AGrid = asgRekapPegawai then begin
      AGrid.ColumnSize.Stretch := False;
      AGrid.HideColumn(ColSeqPeg);
      AGrid.ColCount := AGrid.ColCount - 1;
      SetingPrint(AGrid);
      myConnection.advPrint.Execute;
      AGrid.ColCount := AGrid.ColCount + 1;
      AGrid.UnHideColumn(ColSeqPeg);
      AGrid.ColumnSize.Stretch := True;
    end else if AGrid <> asgRekapPtgSupir then begin
      AGrid.ColumnSize.Stretch := False;
      AGrid.HideColumn(ColSeq);
      AGrid.ColCount := AGrid.ColCount - 1;
      SetingPrint(AGrid);
      myConnection.advPrint.Execute;
      AGrid.ColCount := AGrid.ColCount + 1;
      AGrid.UnHideColumn(ColSeq);
      AGrid.ColumnSize.Stretch := True;
    end else begin
      AGrid.ColumnSize.StretchColumn := 0;
      AGrid.ColumnSize.Stretch := False;
      AGrid.HideColumn(ColSeq);
      AGrid.HideColumn(ColBiaya);
      AGrid.HideColumn(ColPendapatan);
      AGrid.ColCount := AGrid.ColCount - 1;
      SetingPrint(AGrid);
      myConnection.advPrint.Execute;
      AGrid.ColCount := AGrid.ColCount + 1;
      AGrid.UnHideColumn(ColSeq);
      AGrid.UnHideColumn(ColBiaya);
      AGrid.UnHideColumn(ColPendapatan);  
      AGrid.ColumnSize.StretchColumn := AGrid.ColCount-1;
      AGrid.ColumnSize.Stretch := True;
    end;
    ArrangeColSize(AGrid);
  end;
begin
  case AIndex of
    0 : PreparePrint(asgRekapPtgSupp);
    1 : PreparePrint(asgRekapPtgSupir);
    2 : PreparePrint(asgRekapPtgPihak3);
    3 : PreparePrint(asgRekapPtgAssetCust);
    4 : PreparePrint(asgRekapPtgCust);     
    5 : PreparePrint(asgRekapPegawai);
  end;
end;

procedure TfrmRekapPiutang.SetXceling(AIndex: Integer);
begin
  case AIndex of
    0 : asgExportToExcell(asgRekapPtgSupp, myConnection.SaveToExcell);
    1 : asgExportToExcell(asgRekapPtgSupir, myConnection.SaveToExcell);
    2 : asgExportToExcell(asgRekapPtgPihak3, myConnection.SaveToExcell);
    3 : asgExportToExcell(asgRekapPtgAssetCust, myConnection.SaveToExcell);
    4 : asgExportToExcell(asgRekapPtgCust, myConnection.SaveToExcell);
    5 : asgExportToExcell(asgRekapPegawai, myConnection.SaveToExcell);
  end;
end;


end.
