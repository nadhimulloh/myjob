//Gunawan +161018
unit LapPeminjamanBarangPersediaan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, Gauges,
  AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, StdCtrls, Menus, AdvOfficePager,
  AdvEdit;

type
  TfrmLapPeminjamanBarangPersediaan = class(TForm)
    MainPanel: TPanel;
    pnlfilter: TPanel;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    Panel2: TPanel;
    Gauge: TGauge;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    btnFilter: TAdvGlowButton;
    OPLAP: TAdvOfficePager;
    OPSPA: TAdvOfficePage;
    OPSPM: TAdvOfficePage;
    asgSPA: TAdvStringGrid;
    asgSPM: TAdvStringGrid;
    AdvPanel1: TAdvPanel;
    txtSparePart: TAdvEdit;
    btnLovSparePart: TAdvGlowButton;
    procedure chbSFClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgSPAGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgSPMGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgSPMGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgSPAGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure btnLovSparePartClick(Sender: TObject);
  private
    { Private declarations }
    AMenuid, SeqSparePart : integer;
    datenow : tdate;      
    procedure InitForm;
    procedure SetGrid(idx : integer);
    procedure ArrangeColSize(idx : integer);
    procedure LoadData(idx : integer);
  public
    { Public declarations }
    procedure execute(AidMenu:integer);
  end;

var
  frmLapPeminjamanBarangPersediaan: TfrmLapPeminjamanBarangPersediaan;

implementation
uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, MainMenu, OracleConnection, ImportDataTerminal,
  DateUtils, UConst_Temp, ADOInt, StrUtils, ImportDataTerminalWB, LOV,
  LovMasterSparePart;

const
  Colno            = 0;
  ColNamaSparePart = 1;
  ColTglPinjam     = 2;
  ColJmlBrgPinjam  = 3;
  ColTglKembali    = 4;
  colJmlBrgKembali = 5;
  colSelisih       = 6;
  
{$R *.dfm}

{ TfrmLapPeminjamanBarangPersediaan }

procedure TfrmLapPeminjamanBarangPersediaan.ArrangeColSize(idx: integer);
begin
  if idx in [0,1] then begin
    asgSPA.AutoNumberCol(Colno);
    asgSPA.Cells[ColTglPinjam, asgSPA.RowCount-1] := 'Total';
    asgSPA.FloatingFooter.ColumnCalc[ColJmlBrgPinjam] := acSUM;
    asgSPA.FloatingFooter.ColumnCalc[colJmlBrgKembali] := acSUM;
    asgSPA.FloatingFooter.ColumnCalc[colSelisih] := acSUM;
    asgSPA.AutoSizeColumns(True);
  end;
  if idx in [0,2] then begin
    asgSPM.AutoNumberCol(Colno);                             
    asgSPM.Cells[ColTglPinjam, asgSPM.RowCount-1] := 'Total';
    asgSPM.FloatingFooter.ColumnCalc[ColJmlBrgPinjam] := acSUM;
    asgSPM.FloatingFooter.ColumnCalc[colJmlBrgKembali] := acSUM;
    asgSPM.FloatingFooter.ColumnCalc[colSelisih] := acSUM;
    asgSPM.AutoSizeColumns(True);
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.asgSPAGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow = 0 then HAlign := taCenter
  else if Acol in [Colno,ColJmlBrgPinjam, colJmlBrgKembali, colSelisih] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapPeminjamanBarangPersediaan.asgSPAGetFloatFormat(
  Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean;
  var FloatFormat: string);
begin
  if Arow > 0 then begin
    if Acol in [Colno, ColJmlBrgPinjam, colJmlBrgKembali, colSelisih] then begin
      if Acol = Colno then FloatFormat := '%.0n'
      else FloatFormat := '%.2n';
      IsFloat := True;
    end else IsFloat := False;
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.asgSPMGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow = 0 then HAlign := taCenter
  else if Acol in [Colno,ColJmlBrgPinjam, colJmlBrgKembali, colSelisih] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapPeminjamanBarangPersediaan.asgSPMGetFloatFormat(
  Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean;
  var FloatFormat: string);
begin
  if Arow > 0 then begin
    if Acol in [Colno, ColJmlBrgPinjam, colJmlBrgKembali, colSelisih] then begin
      if Acol = Colno then FloatFormat := '%.0n'
      else FloatFormat := '%.2n';
      IsFloat := True;
    end else IsFloat := False;
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if OPLAP.ActivePageIndex = 0 then begin
    SetingPrint(asgSPA);
    myConnection.advPrint.Grid := asgSPA;
    asgSPA.PrintSettings.TitleLines.Clear;
    asgSPA.PrintSettings.TitleLines.Add('');
    asgSPA.PrintSettings.TitleLines.Add('Laporan Peminjaman Barang SPA Ke SPM');
    asgSPA.PrintSettings.TitleLines.Add('');
    asgSPA.ColumnSize.Stretch := false ;
    asgSPA.ColCount := asgSPA.ColCount-1;
    SetingPrint(asgSPA);
    myConnection.advPrint.Execute ;
    asgSPA.ColCount := asgSPA.ColCount+1;
    asgSPA.ColumnSize.Stretch := true;
    ArrangeColSize(1);
  end else begin
    myConnection.advPrint.Grid := asgSPM;
    asgSPM.PrintSettings.TitleLines.Clear;
    asgSPM.PrintSettings.TitleLines.Add('');
    asgSPM.PrintSettings.TitleLines.Add('Laporan Peminjaman Barang SPM Ke SPA');
    asgSPM.PrintSettings.TitleLines.Add('');
    asgSPM.ColumnSize.Stretch := false ;
    asgSPM.ColCount := asgSPM.ColCount-1;
    SetingPrint(asgSPM);
    myConnection.advPrint.Execute ;
    asgSPM.ColCount := asgSPM.ColCount+1;
    asgSPM.ColumnSize.Stretch := true;
    ArrangeColSize(2);
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if OPLAP.ActivePageIndex = 0 then begin
    asgExportToExcell(asgSPA, myConnection.SaveToExcell);
  end else begin
    asgExportToExcell(asgSPM, myConnection.SaveToExcell);
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlfilter, btnFilter, 64);
end;

procedure TfrmLapPeminjamanBarangPersediaan.btnLoadClick(Sender: TObject);
begin
  if OPLAP.ActivePageIndex = 0 then begin
    LoadData(0);
  end else begin
    LoadData(1);
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.btnLovSparePartClick(
  Sender: TObject);
var data : TR_master_spare_part;
begin
  txtSparePart.Clear;
  Application.CreateForm(TfrmLovMasterSparePart, frmLovMasterSparePart);
  frmLovMasterSparePart.OnClose := frmMainMenu.ChildFormSingle;
  SeqSparePart := frmLovMasterSparePart.Execute(txtSparePart.text);
  if SeqSparePart <> 0 then begin
    data := Get_master_spare_part(SeqSparePart);
    txtSparePart.Text := data.part_number+' | '+data.nama;
  end else begin
    txtSparePart.Text:= '';
    SeqSparePart := 0;
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.btnResetClick(Sender: TObject);
begin
  if OPLAP.ActivePageIndex = 0 then begin
    InitForm;
    SetGrid(1);
  end else begin
    InitForm;
    SetGrid(2);  
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.chbSFClick(Sender: TObject);
begin
  if OPLAP.ActivePageIndex = 0 then begin
    asgSPA.SearchFooter.Visible := chbSF.Checked;
  end else begin
    asgSPM.SearchFooter.Visible := chbSF.Checked;
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  if (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
  and (not BisaNambah(AMenuId)) and (not BisaLihatRekap(AMenuId)) and (not BisaLihatLaporan(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  OPLAP.ActivePageIndex := 0;
  initform;
  Setgrid(0);
  run(self);
end;

procedure TfrmLapPeminjamanBarangPersediaan.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapPeminjamanBarangPersediaan.InitForm;
begin
  datenow := Servernow;
  SeqSparePart := 0;
  txtSparePart.Text := '';
  chbSF.Checked := False;
  asgSPA.SearchFooter.Visible := chbSF.Checked;
  asgSPM.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLapPeminjamanBarangPersediaan.LoadData(idx: integer);
begin
  if idx = 0 then begin
    Setgrid(1);
    Load_Lap_Peminjaman_Barang_SPA(asgSPA,Gauge,SeqSparePart);
    ArrangeColSize(1);
  end;
  if idx = 1 then begin
    Setgrid(2);
    Load_Lap_Peminjaman_Barang_SPM(asgSPM,Gauge,SeqSparePart);
    ArrangeColSize(2);
  end;
end;

procedure TfrmLapPeminjamanBarangPersediaan.SetGrid(idx: integer);
begin
  if idx in [0,1] then begin
    asgSPA.ClearNormalCells;
    asgSPA.ColCount  := 8;
    asgSPA.RowCount  := 3;
    asgSPA.FixedRows := 1;
    asgSPA.FixedCols := 1;
  end;
  if idx in [0,2] then begin
    asgSPM.ClearNormalCells;
    asgSPM.ColCount  := 8;
    asgSPM.RowCount  := 3;
    asgSPM.FixedRows := 1;
    asgSPM.FixedCols := 1;
  end;
  ArrangeColSize(idx);
end;

end.
