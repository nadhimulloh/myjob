unit RekapLabaRugi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, BaseGrid, AdvGrid, StdCtrls, AdvEdit,
  ComCtrls, AdvPanel, AdvGlowButton, Gauges, ExtCtrls, UGeneral, UEngine,
  USystemMenu, URecord, DateUtils, OracleConnection, UConst, AdvObj;

type
  TfrmRekapLabaRugi = class(TForm)
    pnlFilter: TPanel;
    Gauge1: TGauge;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    PnlStatus: TAdvPanel;
    dtpTgl: TDateTimePicker;
    Panel2: TPanel;
    asgRekap: TAdvStringGrid;
    btnAmbil: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    btnCetakKas: TAdvGlowButton;
    rbtTahun: TRadioButton;
    rbtBln: TRadioButton;
    rbtTanggal: TRadioButton;
    chbIncNol: TAdvOfficeCheckBox;
    chbTampilDecimal: TAdvOfficeCheckBox;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure rbtBlnClick(Sender: TObject);
    procedure rbtTahunClick(Sender: TObject);
    procedure dtpTglChange(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure btnEksporClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCetakKasClick(Sender: TObject);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure rbtTanggalClick(Sender: TObject);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
    AIdMenu : integer;
    isFromNeraca : boolean;
    browsemode : boolean;
    procedure InitForm;
    procedure ArrangeColSize;
    procedure SetGrid;
    procedure LoadData;
  public
    { Public declarations }
    procedure Execute(AMenuId : integer);
    procedure Execute4Neraca(isBulan: boolean; tglDate: tdate; AMenuId : integer; tampilkanDesimal:boolean=false);
  end;

var
  frmRekapLabaRugi: TfrmRekapLabaRugi;

implementation

uses
  MainMenu, ureport, StrUtils, Math, Urecord_Temp, UReport_temp;

Const
  ColDesk = 0;
  ColRp   = 1;


{$R *.dfm}

{ TfrmRekapLabaRugi }

procedure TfrmRekapLabaRugi.ArrangeColSize;
begin
  asgRekap.AutoSizeColumns(True, 5);
  asgRekap.ColWidths[ColDesk-1] := 70;
  asgRekap.FloatFormat := '%.2n';
  asgRekap.ColumnSize.StretchColumn := 0;
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColumnSize.StretchColumn := asgRekap.ColCount -1;
  asgRekap.ColumnSize.Stretch := True;
end;

procedure TfrmRekapLabaRugi.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
//  if (ARow > 3) then begin
//    btnAmbil.Enabled := (asgRekap.Cells[ColRp2, asgRekap.Row] <> '') or (asgRekap.Floats[ColRp2, asgRekap.Row] <> 0);
//  end;
end;

procedure TfrmRekapLabaRugi.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 2) then HAlign := taCenter
  else if (Acol in [ColRp]) then HAlign := taRightJustify;
//  else if (ARow = asgRekap.RowCount-1) and (ACol = ColKet) then HALign := taCenter
//  else if (ARow = asgRekap.RowCount-1) and (ACol = ColRp2) then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmRekapLabaRugi.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
//  AFont := asgRekap.Fonts[ACol, ARow];
end;

procedure TfrmRekapLabaRugi.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if chbTampilDecimal.Checked = false then begin
    if Acol in [ColRp] then begin
      isFLoat := True;
      if ACol in [ColRp] then FloatFormat := '%.0n';
    end else isFLoat := False;
  end else begin
    if Acol in [ColRp] then begin
      isFLoat := True;
      if ACol in [ColRp] then FloatFormat := '%.2n';
    end else isFLoat := False;
  end;
end;

procedure TfrmRekapLabaRugi.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmRekapLabaRugi.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmRekapLabaRugi.btnCetakKasClick(Sender: TObject);
//var
//  ADataAr: AR_Print;
//  idx, i : Integer;
begin
//  if  (not BisaPrint(AIdMenu)) then begin
//    Alert(MSG_UNAUTHORISED_ACCESS);
//    Exit;
//  end;
//  idx := 0;
//  for i := 3 to asgRekap.RowCount-1 do begin
//    if (asgRekap.Cells[ColNama, i] <> '') then begin
//      if (asgRekap.Floats[ColDb, i] <> 0) or (asgRekap.Floats[ColIndukSeq, i] <> 0) and (asgRekap.Floats[ColLevel, i] > 1) then  begin
//        SetLength(ADataAr, idx+1);
//        ADataAr[idx].AccNum := asgRekap.Cells[ColRoot, i];
//        ADataAr[idx].AccName  := asgRekap.Cells[colNama, i];
//        if (asgRekap.Floats[ColDb, i] <> 0) then
//          ADataAr[idx].vDb:= FloatToStrFmt(asgRekap.Floats[ColDb, i])
//        else ADataAr[idx].vDb:= '';
//        inc(idx);
//      end;
//    end;
//  end;
//
//  if idx <> 0 then begin
//    btnCetakKas.Enabled := false;
//    Application.CreateForm(TqrpRugiLaba,qrpRugiLaba);
//    qrpRugiLaba.Executes('Laporan Laba/Rugi', 'Periode '+FormatDateTime('MMMM yyyy', dtpTgl.Date), ADataAr);
//
//    qrpRugiLaba.Destroy;
//    btnCetakKas.Enabled := true;
//  end
//  else begin
//    Inform(MSG_NO_DATA_FOUND);
//  end;

  if not(BisaPrint(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;

  myConnection.advPrint.Grid := asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Laporan Laba Rugi');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.StretchColumn := 0;
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.UnHideColumnsAll;
  asgRekap.ColCount := asgRekap.ColCount -1;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount + 1;
  asgRekap.UnHideColumnsAll;
  asgRekap.ColumnSize.StretchColumn := asgRekap.ColCount -1;
  asgRekap.ColumnSize.Stretch := True;
  ArrangeColSize;
end;

procedure TfrmRekapLabaRugi.btnEksporClick(Sender: TObject);
begin
  if not (BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapLabaRugi.btnLoadClick(Sender: TObject);
begin
  LoadData;
  if not browsemode then
    btnAmbil.Enabled := True;
end;

procedure TfrmRekapLabaRugi.btnResetClick(Sender: TObject);
begin
  InitForm;
  SetGrid;
  asgRekap.OnClickCell(asgRekap,4,0);
end;

procedure TfrmRekapLabaRugi.chbSFClick(Sender: TObject);
begin
  asgRekap.setfocus;
  asgRekap.row := asgRekap.rowcount -2;
  asgRekap.col := 0;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapLabaRugi.dtpTglChange(Sender: TObject);
begin
  if rbtBln.Checked then
    dtpTgl.Date := StartOfTheMonth(dtpTgl.Date);
end;

procedure TfrmRekapLabaRugi.Execute(AMenuId: integer);
begin
  AIdMenu := AMenuId;
  if (not BisaPrint(aidMenu)) and (not BisaEkspor(aidMenu)) and (not BisaLihatRekap(aidMenu)) and (not BisaLihatLaporan(aidMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  browsemode := false;
  isFromNeraca := false;
  InitForm;
  SetGrid;
  Run(Self);
end;

procedure TfrmRekapLabaRugi.Execute4Neraca(isBulan: boolean;
  tglDate: tdate; AMenuId : integer; tampilkanDesimal:boolean=false);
begin
//  AIdMenu := AMenuId;
//  if (not BisaEkspor(AIdMenu)) and (not BisaPrint(AIdMenu)) then begin
//    Alert(MSG_UNAUTHORISED_ACCESS);
//    Exit;
//  end;
//  browsemode := true;
//  isFromNeraca := true;
//  pnlFilter.Hide;
//  InitForm; SetGrid;
//  if isBulan then begin
//    rbtBln.Checked := true;
//    dtpTgl.Date := tglDate;
//    dtpTgl.Format := 'MMMM- yyyy';
//  end else begin
//    if isHari then begin
//      rbtTanggal.Checked := true;
//      dtpTgl.Format := 'dd MMMM yyyy';
//      dtpTgl.Date := tglDate;
//      dtpTgl.DateMode := dmComboBox;
//    end else begin
//      rbtTahun.Checked := true;
//      dtpTgl.Date := tglDate;
//      dtpTgl.Format := 'yyyy';
//    end;
//  end;
//  self.Caption := 'Laporan Laba Rugi';
//
//  asgRekap.RowCount := 4;
//  asgRekap.ClearNormalCells;
//  btnLoad.Click;
//  ArrangeColSize;
//  Run(Self);
  AIdMenu := AMenuId;
  if (not BisaEkspor(AIdMenu)) and (not BisaPrint(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;                                
  pnlFilter.Hide;
  chbIncNol.Hide;
  chbTampilDecimal.Hide;
  browsemode := true;
  isFromNeraca := true;
  InitForm; SetGrid;
  chbTampilDecimal.Checked := tampilkanDesimal;
  if isBulan then begin
    rbtBln.Checked := true;
    dtpTgl.Date := tglDate;
    dtpTgl.Format := 'MMMM- yyyy';
  end else begin
    //pertanggal
    rbtTanggal.Checked := true;
    dtpTgl.Date := tglDate;
    dtpTgl.Format := 'dd/MM/yyyy';

    rbtTahun.Checked := False;//true;
//    dtpTgl.Date := tglDate;
//    dtpTgl.Format := 'yyyy';
  end;
  self.Caption := 'Laporan Laba Rugi';

  asgRekap.RowCount := 4;
  asgRekap.ClearNormalCells;
  btnLoad.Click;
  ArrangeColSize;
  Run(Self);
end;

procedure TfrmRekapLabaRugi.FormShow(Sender: TObject);
begin
  if not browsemode then Execute(ACurrMenuId);
end;

procedure TfrmRekapLabaRugi.InitForm;
begin
  dtpTgl.Date := ServerNow;
  chbSF.Checked := False;
  asgRekap.SearchFooter.Visible := False;
  chbIncNol.Checked := false;
  chbTampilDecimal.Checked := false;
  rbtbln.Checked := true;
  rbtTahun.Checked := false;
  rbtTanggal.Checked := false;
  if isFromNeraca then begin
    rbtTanggal.Enabled := false;
    rbtBln.Enabled := false;
  end;
  asgRekap.OnClickCell(asgRekap,4,0);
//  rbtbln.Visible := GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER;
//  rbtTahun.Visible := GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER;
end;


procedure TfrmRekapLabaRugi.LoadData;
begin
  asgRekap.RowCount := 3;
  asgRekap.ClearNormalCells;
//    load_lap_rugi_laba(asgRekap, dtpTgl.Date, Gauge1,rbtTahun.Checked, rbtTanggal.Checked, chbIncNol.Checked, chbTampilDecimal.Checked, False);
    load_lap_rugi_laba_New(asgRekap, dtpTgl.Date, Gauge1,rbtTahun.Checked, rbtTanggal.Checked, chbIncNol.Checked, chbTampilDecimal.Checked, False);
  ArrangeColSize;
end;

procedure TfrmRekapLabaRugi.rbtBlnClick(Sender: TObject);
begin
  if rbtBln.Checked = True then begin
    dtpTgl.Format := 'MMMM-yyyy';
    dtpTgl.DateMode := dmUpDown;
    dtpTgl.Date := StartOfTheMonth(dtpTgl.Date);
  end;
end;

procedure TfrmRekapLabaRugi.rbtTahunClick(Sender: TObject);
begin
  if rbtTahun.Checked = True then begin
    dtpTgl.Format := 'MMMM-yyyy';     
    dtpTgl.DateMode := dmComboBox;
  end;
end;

procedure TfrmRekapLabaRugi.rbtTanggalClick(Sender: TObject);
begin
  if rbtTanggal.Checked then begin
    rbtBln.Checked := false;
    dtpTgl.Format := 'dd MMMM yyyy';
    dtpTgl.DateMode := dmComboBox;
  end;
end;

procedure TfrmRekapLabaRugi.SetGrid;
begin
//  UGeneral.asgResetGrid(asgRekap,6,4,4,0);
  asgRekap.UnHideColumnsAll;
  asgrekap.ClearNormalCells;
  asgRekap.ColCount := 3;
  asgRekap.RowCount := 4;
  asgRekap.FixedCols := 0;
  asgRekap.FixedRows := 2;                           
  asgRekap.MergeCells(ColDesk, 0, 2, 1);    
  asgSetRowFontStyleNormal(asgRekap, 0);
  asgRekap.Cells[ColDesk, 0] := 'LABA RUGI';
  asgRekap.Cells[ColDesk, 1] := 'DESKRIPSI';
  asgRekap.Cells[ColRp, 1] := 'Rp';


  asgRekap.ClearRows(asgRekap.RowCount-1, 1);
//  asgRekap.WordWrap := true;
  asgRekap.MergeCells(asgRekap.ColCount -1, 0, 1, 2);

  ArrangeColSize;
end;

end.




