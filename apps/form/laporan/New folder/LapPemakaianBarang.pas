unit LapPemakaianBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, Gauges,
  AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, StdCtrls, Menus, AdvOfficePager,
  AdvEdit;

type
  TfrmLapPemakaianBarang = class(TForm)
    MainPanel: TPanel;
    pnlfilter: TPanel;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    Panel2: TPanel;
    Gauge: TGauge;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    btnFilter: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label4: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label1: TLabel;
    asgLap: TAdvStringGrid;
    AdvPanel1: TAdvPanel;
    Label2: TLabel;
    Label3: TLabel;
    txtArmada: TAdvEdit;
    txtBarang: TAdvEdit;
    btnlovarmada: TAdvGlowButton;
    btnlov: TAdvGlowButton;
    chbKatBan: TAdvOfficeCheckBox;
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure Import1Click(Sender: TObject);
    procedure Import2Click(Sender: TObject);
    procedure asgLapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgLapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgLapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure aofTerminalChange(Sender: TObject);
    procedure asgLapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgLapContractNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgLapExpandNode(Sender: TObject; ARow, ARowreal: Integer);
    procedure asgLapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure btnlovarmadaClick(Sender: TObject);
    procedure btnlovClick(Sender: TObject);
  private
    { Private declarations }
    AMenuid : integer;
    seqArmada, seqBarang : integer;
    listHrgrata : TStringList;
    datenow : tdate;
    procedure InitForm;
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
  public
    { Public declarations }
    procedure execute(AidMenu:integer);
  end;

var
  frmLapPemakaianBarang: TfrmLapPemakaianBarang;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, MainMenu, OracleConnection, ImportDataTerminal,
  DateUtils, UConst_Temp, ADOInt, StrUtils, ImportDataTerminalWB, LOV,
  LovMasterSparePart;

const
  colNode     = 0;
  colArmada   = 1;
  colTanggal  = 2;
  colBarang   = 3;
  colqty      = 4;
  colharga    = 5;
  coltotal    = 6;
  colseq      = 7;

{$R *.dfm}

procedure TfrmLapPemakaianBarang.aofTerminalChange(Sender: TObject);
begin
  chbSF.Checked := False;
  asgLap.SearchFooter.Visible :=  false;
end;

procedure TfrmLapPemakaianBarang.ArrangeColSize;
begin
  asgLap.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgLap.ColWidths[colseq] := 0;
  end;


end;

procedure TfrmLapPemakaianBarang.asgLapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (ARow < 3) and (ACol = 0) then begin
    asgLap.Cells[0,0] := IfThen(asgLap.Cells[0,0]='[-]','[+]','[-]');
    if asgLap.Cells[0,0] = '[+]' then asgLap.ContractAll
    else asgLap.ExpandAll;
  end;
end;

procedure TfrmLapPemakaianBarang.asgLapContractNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmLapPemakaianBarang.asgLapExpandNode(Sender: TObject; ARow,
  ARowreal: Integer);
begin
  ArrangeColSize;
end;

procedure TfrmLapPemakaianBarang.asgLapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 2 then HAlign := taCenter
  else if acol in [colharga , colqty, coltotal]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapPemakaianBarang.asgLapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := false;
  IsFloat := acol in [colqty,colTotal];
  if IsFloat then begin
    if ACol in [colharga,colTotal] then FloatFormat := '%.2n'
    else FloatFormat := '%.0n';
  end;
end;

procedure TfrmLapPemakaianBarang.asgLapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize;
end;

procedure TfrmLapPemakaianBarang.asgLapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize;
end;

procedure TfrmLapPemakaianBarang.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  SetingPrint(asgLap);
  myConnection.advPrint.Grid := asgLap;
  asgLap.PrintSettings.TitleLines.Clear;
  asgLap.PrintSettings.TitleLines.Add('');
  asgLap.PrintSettings.TitleLines.Add('Laporan Pemakaian Barang');
  asgLap.PrintSettings.TitleLines.Add('');
  asgLap.ColumnSize.Stretch := false ;
  asgLap.ColCount := asgLap.ColCount-2;
  SetingPrint(asgLap);
  myConnection.advPrint.Execute ;
  asgLap.ColCount := asgLap.ColCount+2;
  asgLap.ColumnSize.Stretch := true;
  ArrangeColSize;
end;

procedure TfrmLapPemakaianBarang.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgLap, myConnection.SaveToExcell);
end;

procedure TfrmLapPemakaianBarang.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlfilter, btnFilter, 90);
end;

procedure TfrmLapPemakaianBarang.btnLoadClick(Sender: TObject);
begin
  loaddata
end;

procedure TfrmLapPemakaianBarang.btnlovarmadaClick(Sender: TObject);
begin
  txtArmada.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada         := frmLov.ExecuteArmada('',boNone);
  if SeqArmada <> 0 then begin
    txtArmada.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',1)+' | '
    +EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapPemakaianBarang.btnlovClick(Sender: TObject);
begin
  txtbarang.Clear;
  Seqbarang := 0;
  Application.CreateForm(TfrmLovMasterSparePart, frmLovMasterSparePart);
  frmLovMasterSparePart.OnClose := frmMainMenu.ChildFormSingle;
  Seqbarang :=  frmLovMasterSparePart.Execute('',boTrue);
  if Seqbarang <> 0 then begin
    txtbarang.Text := EkstrakString(ListsparepartGlobal.Values[IntToStr(Seqbarang)],'#',1)+' | '
    +EkstrakString(ListsparepartGlobal.Values[IntToStr(Seqbarang)],'#',2);
  end;
end;

procedure TfrmLapPemakaianBarang.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
end;

procedure TfrmLapPemakaianBarang.chbSFClick(Sender: TObject);
begin
  asgLap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLapPemakaianBarang.execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  listHrgrata := tstringlist.create;
  datenow := ServerNow; 
  if (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
     and (not BisaNambah(AMenuId)) and (not BisaLihatRekap(AMenuId)) and (not BisaLihatLaporan(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  initform;
  Setgrid;
  run(self);
end;

procedure TfrmLapPemakaianBarang.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapPemakaianBarang.Import1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportDataTerminal, frmImportDataTerminal);
  frmImportDataTerminal.OnClose := frmMainMenu.ChildFormSingle;
  if frmImportDataTerminal.execute then btnLoad.Click;
end;

procedure TfrmLapPemakaianBarang.Import2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmImportDataTerminalWB, frmImportDataTerminalWB);
    frmImportDataTerminalWB.OnClose := frmMainMenu.ChildFormSingle;
  if frmImportDataTerminalWB.execute then btnLoad.Click;
end;

procedure TfrmLapPemakaianBarang.InitForm;
begin
  dtpAwal.date := StartOfTheMonth(ServerNow);
  dtpAkhir.Date := ServerNow;
  chbSF.Checked := false;
  seqArmada := 0;
  seqBarang := 0;
  txtArmada.Text := '';
  txtBarang.Text := '';
  asgLap.SearchFooter.Visible := False;
  chbKatBan.Checked := False; //gunawan +031219
end;

procedure TfrmLapPemakaianBarang.LoadData;
VAR Periode : TR_FilterPeriode;
    i, row, Row2 :integer;
    buffer : _recordset;
    temparmada : integer;
    tempTgl : tdate;
    total : real;
begin
  Gauge.Show;
  begin periode.FPeriodeAwal      := dtpAwal.Date;
        periode.FPeriodeAkhir     := dtpAkhir.Date;
        periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
        periode.FOprPeriodeAkhir  := soLessThanEqualsTo;
  end;
  asgLap.ClearNormalCells;
  asgLap.RowCount := 4;
  Gauge.Show;
  asgLap.Cells[ColNode, 0] := '[-]';
//   0               1            2          3              4            5
// m.tanggal, m.armada_seq, m.gudang_seq, m.status, d.spare_part_seq, sum(d.QTY)
  Load_pemakaian_sp_master4_lap(buffer, periode, seqArmada, seqBarang,chbKatBan.Checked);
//  get_list_hrg_rata(listHrgrata,0,datenow);                         //gunawan +031219

  temparmada := 0;  tempTgl := 0; total := 0; row := 0;
  for i := 0 to buffer.RecordCount-1 do begin
    Gauge.Progress := round((i+1)/buffer.RecordCount*100);
    if (temparmada <> BufferToInteger(buffer.Fields[1].Value)) or (tempTgl <> BufferToDateTime(buffer.Fields[0].Value)) then begin
      if asgLap.cells[colTanggal,3] <> '' then asgLap.AddRow;
      row := asgLap.RowCount-2;
      asgLap.Cells[colArmada,  row] := EkstrakString(ListArmadaGlobal.Values[BufferToString(buffer.Fields[1].Value)],'#',2);
      asgLap.Dates[colTanggal, row] := BufferToDateTime(buffer.Fields[0].Value);
      listHrgrata.clear;
//      get_list_hrg_rata(listHrgrata,0,BufferToDateTime(buffer.Fields[0].Value));
      asgLap.MergeCells(colTanggal+1,row,4,1);
    end;
    temparmada := BufferToInteger(buffer.Fields[1].Value);
    tempTgl    := BufferToDateTime(buffer.Fields[0].Value);
    asgLap.AddRow;
    row2 := asgLap.rowcount-2;
    asgLap.Cells[colTanggal ,row2] := EkstrakString(ListsparepartGlobal.Values[BufferToString(buffer.Fields[4].Value)],'#',1);
    asgLap.Cells[colBarang  ,row2] := EkstrakString(ListsparepartGlobal.Values[BufferToString(buffer.Fields[4].Value)],'#',2);
    asgLap.Floats[colqty    ,row2] := BufferToFloat(buffer.Fields[5].Value);
//    asgLap.Floats[colharga  ,row2] := StrToFloatdef(listHrgrata.Values[BufferToString(buffer.Fields[4].Value)],0); dziganti dzikri jd ngambil dr tabel detailny nya
    asgLap.Floats[colharga  ,row2] := BufferTofloat(buffer.Fields[6].Value);
    asgLap.Floats[coltotal  ,row2] := (asgLap.Floats[colqty,row2])*(asgLap.Floats[colharga,row2]);
    total := total + asgLap.Floats[coltotal  ,row2];
    //asgLap.Cells[colseq     ,row2] := BufferToString(buffer.Fields[0].Value);

//    asgLap.RemoveNode(Row2);
//    asgLap.AddNode(Row2, (Row3-Row2)+1);

    asgLap.RemoveNode(Row);
    asgLap.AddNode(Row, (Row2-Row)+1);

    buffer.MoveNext;
  end;
  asgLap.Floats[coltotal, asgLap.RowCount-1] := total;
  gauge.hide;
  buffer.close;
  arrangecolsize;
end;

procedure TfrmLapPemakaianBarang.SetGrid;
begin
  asgLap.ClearNormalCells;
  asgLap.ColCount := 9;
  asgLap.RowCount := 4;
  asgLap.FixedCols := 0;
  asgLap.FixedRows := 2;

  asgLap.MergeCells(colNode,0,1,2);
  asgLap.Cells[colNode,0] := '[+]';
  asgLap.MergeCells(colArmada,0,1,2);
  asgLap.Cells[colArmada,0] := 'Armada';
  asgLap.Cells[colTanggal,0] := 'Tanggal';
  asgLap.Cells[colTanggal,1] := 'No. Part';
  asgLap.MergeCells(colBarang,0,1,2);
  asgLap.Cells[colBarang,0] := 'Barang';
  asgLap.MergeCells(colqty,0,1,2);
  asgLap.Cells[colqty,0] := 'Qty';
  asgLap.MergeCells(colharga,0,1,2);
  asgLap.Cells[colharga,0] := 'Harga';
  asgLap.MergeCells(coltotal,0,1,2);
  asgLap.Cells[coltotal,0] := 'Total';

  asgLap.MergeCells(asgLap.ColCount-1,0,1,2);
  ArrangeColSize;

end;

end.
