unit KartuHutang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, AdvEdit, AdvPanel, AdvOfficeButtons, Grids, AdvObj, BaseGrid,
  AdvGrid, AdvGlowButton, Gauges, AdvOfficePager, ExtCtrls, OracleConnection, UConst, URecord,
  UGeneral, UReport, USystemMenu, UEngine_Temp, DateUtils;

type
  TfrmKartuHutang = class(TForm)
    MainPanel: TPanel;
    advOfPager: TAdvOfficePager;
    pcgSupplier: TAdvOfficePage;
    Panel3: TPanel;
    GaugeHtgSupp: TGauge;
    btnFilterHtgSupp: TAdvGlowButton;
    btnEksporHtgSupp: TAdvGlowButton;
    btnCetakHtgSupp: TAdvGlowButton;
    chbSFHtgSupp: TAdvOfficeCheckBox;
    pnlFilterHtgSupp: TPanel;
    btnResetHtgSupp: TAdvGlowButton;
    btnLoadHtgSupp: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    txtCariHtgSupp: TAdvEdit;
    btnCariHtgSupp: TAdvGlowButton;
    PnlStatus: TAdvPanel;
    Label1: TLabel;
    Label5: TLabel;
    dtpAwalHtgSupp: TDateTimePicker;
    dtpAkhirHtgSupp: TDateTimePicker;
    pcgSupir: TAdvOfficePage;
    pnlFilterHtgSupir: TPanel;
    btnResetHtgSupir: TAdvGlowButton;
    btnLoadHtgSupir: TAdvGlowButton;
    AdvPanel4: TAdvPanel;
    txtCariHtgSupir: TAdvEdit;
    btnCariHtgSupir: TAdvGlowButton;
    AdvPanel5: TAdvPanel;
    Label2: TLabel;
    Label7: TLabel;
    dtpAwalHtgSupir: TDateTimePicker;
    dtpAkhirHtgSupir: TDateTimePicker;
    Panel2: TPanel;
    GaugeHtgSupir: TGauge;
    btnFilterHtgSupir: TAdvGlowButton;
    btnEksporHtgSupir: TAdvGlowButton;
    btnCetakHtgSupir: TAdvGlowButton;
    chbSFHtgSupir: TAdvOfficeCheckBox;
    pcgPihak3: TAdvOfficePage;
    pnlFilterHtgPihak3: TPanel;
    AdvPanel3: TAdvPanel;
    txtCariHtgPihak3: TAdvEdit;
    btnCariHtgPihak3: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label3: TLabel;
    Label4: TLabel;
    dtpAwalHtgPihak3: TDateTimePicker;
    dtpAkhirHtgPihak3: TDateTimePicker;
    btnLoadHtgPihak3: TAdvGlowButton;
    btnResetHtgPihak3: TAdvGlowButton;
    Panel4: TPanel;
    GaugeHtgPihak3: TGauge;
    btnFilterHtgPihak3: TAdvGlowButton;
    btnEksporHtgPihak3: TAdvGlowButton;
    btnCetakHtgPihak3: TAdvGlowButton;
    chbSFHtgPihak3: TAdvOfficeCheckBox;
    pcgAsset: TAdvOfficePage;
    pnlFilterHtgAssetSupp: TPanel;
    AdvPanel10: TAdvPanel;
    Label11: TLabel;
    Label6: TLabel;
    dtpAwalHtgAssetSupp: TDateTimePicker;
    dtpAkhirHtgAssetSupp: TDateTimePicker;
    AdvPanel11: TAdvPanel;
    txtCariHtgAssetSupp: TAdvEdit;
    btnCariHtgAssetSupp: TAdvGlowButton;
    btnLoadHtgAssetSupp: TAdvGlowButton;
    btnResetHtgAssetSupp: TAdvGlowButton;
    Panel10: TPanel;
    GaugeHtgAssetSupp: TGauge;
    btnFilterHtgAssetSupp: TAdvGlowButton;
    btnEksporHtgAssetSupp: TAdvGlowButton;
    btnCetakHtgAssetSupp: TAdvGlowButton;
    chbSFHtgAssetSupp: TAdvOfficeCheckBox;
    pcgDPPariwisata: TAdvOfficePage;
    pnlFilterHtgDPCust: TPanel;
    AdvPanel16: TAdvPanel;
    Label17: TLabel;
    Label8: TLabel;
    dtpAwalHtgDPCust: TDateTimePicker;
    dtpAkhirHtgDPCust: TDateTimePicker;
    AdvPanel17: TAdvPanel;
    txtCariHtgDPCust: TAdvEdit;
    btnCariHtgDPCust: TAdvGlowButton;
    btnLoadHtgDPCust: TAdvGlowButton;
    btnResetHtgDPCust: TAdvGlowButton;
    Panel16: TPanel;
    GaugeHtgDPCust: TGauge;
    btnFilterHtgDPCust: TAdvGlowButton;
    btnEksporHtgDPCust: TAdvGlowButton;
    btnCetakHtgDPCust: TAdvGlowButton;
    chbSFHtgDPCust: TAdvOfficeCheckBox;
    asgRekapHtgSupp: TAdvStringGrid;
    asgRekapHtgSupir: TAdvStringGrid;
    asgRekapHtgPihak3: TAdvStringGrid;
    asgRekapHtgAsset: TAdvStringGrid;
    asgRekapHtgDPCust: TAdvStringGrid;
    procedure btnLoadHtgSuppClick(Sender: TObject);
    procedure btnLoadHtgSupirClick(Sender: TObject);
    procedure btnLoadHtgPihak3Click(Sender: TObject);
    procedure btnLoadHtgAssetSuppClick(Sender: TObject);
    procedure btnResetHtgSuppClick(Sender: TObject);
    procedure btnResetHtgSupirClick(Sender: TObject);
    procedure btnResetHtgPihak3Click(Sender: TObject);
    procedure btnResetHtgAssetSuppClick(Sender: TObject);
    procedure btnResetHtgDPCustClick(Sender: TObject);
    procedure btnFilterHtgSuppClick(Sender: TObject);
    procedure btnFilterHtgSupirClick(Sender: TObject);
    procedure btnFilterHtgPihak3Click(Sender: TObject);
    procedure btnFilterHtgAssetSuppClick(Sender: TObject);
    procedure btnFilterHtgDPCustClick(Sender: TObject);
    procedure btnEksporHtgSuppClick(Sender: TObject);
    procedure btnEksporHtgSupirClick(Sender: TObject);
    procedure btnEksporHtgPihak3Click(Sender: TObject);
    procedure btnEksporHtgAssetSuppClick(Sender: TObject);
    procedure btnEksporHtgDPCustClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbSFHtgSuppClick(Sender: TObject);
    procedure chbSFHtgSupirClick(Sender: TObject);
    procedure chbSFHtgPihak3Click(Sender: TObject);
    procedure chbSFHtgAssetSuppClick(Sender: TObject);
    procedure chbSFHtgDPCustClick(Sender: TObject);
    procedure asgRekapHtgSuppCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSupirCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgPihak3CellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgAssetCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgDPCustCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapHtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgAssetGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgDPCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgAssetGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgDPCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgAssetMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgAssetMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgDPCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgDPCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure btnLoadHtgDPCustClick(Sender: TObject);
    procedure btnCariHtgSuppClick(Sender: TObject);
    procedure btnCariHtgSupirClick(Sender: TObject);
    procedure btnCariHtgPihak3Click(Sender: TObject);
    procedure btnCariHtgAssetSuppClick(Sender: TObject);
    procedure btnCariHtgDPCustClick(Sender: TObject);
    procedure btnCetakHtgSuppClick(Sender: TObject);
    procedure btnCetakHtgSupirClick(Sender: TObject);
    procedure btnCetakHtgPihak3Click(Sender: TObject);
    procedure btnCetakHtgAssetSuppClick(Sender: TObject);
    procedure btnCetakHtgDPCustClick(Sender: TObject);
  private
    { Private declarations }
    AIdMenu : integer;
    DateNow : TDate;
    BrowseMode : boolean;
    TipePihak3 : String;
    SeqSupp, SeqSupir, SeqPihak3, SeqAsset, SeqDPCust : integer;
    procedure InitForm(idx : integer);
    procedure SetGrid(AGrid : TAdvStringGrid);
    procedure ArrangeColSize(AGrid : TAdvStringGrid);
    procedure SetPrinting(AIndex : integer);
    procedure SetXcelling(AIndex : integer);
    procedure LoadData(AIndex : integer);
  public
    { Public declarations }
    procedure Execute(AMenuId : integer); overload;
    procedure Execute(AMenuId, seqVC, PageIndex : integer; Tgl1, Tgl2 : TDate; PihakKe3 : string = ''); overload;
  end;

var
  frmKartuHutang: TfrmKartuHutang;

implementation

uses MainMenu, Lov, LOVRelasi;

{$R *.dfm}

const
  ColNo      = 0;
  ColTgl     = 1;
  ColNoTrans = 2;
  ColDesk    = 3;
  ColDebet   = 4;
  ColKredit  = 5;
  ColSaldo   = 6;
  ColKet     = 7;
  ColSeq     = 8;

{ TfrmKartuHutang }

procedure TfrmKartuHutang.ArrangeColSize(AGrid: TAdvStringGrid);
begin
  AGrid.FloatFormat := '%.2n';
  AGrid.AutoSizeColumns(True, 10);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    AGrid.ColWidths[ColSeq] := 0;
  end;
end;

procedure TfrmKartuHutang.Execute(AMenuId, seqVC, PageIndex: integer; Tgl1, Tgl2: TDate; PihakKe3: string);
begin
  AIdMenu := AMenuId;
  if (not BisaLihatLaporan(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  DateNow    := ServerNow;
  BrowseMode := true;
  InitForm(0);
  SetGrid(asgRekapHtgSupp);
  SetGrid(asgRekapHtgSupp);
  SetGrid(asgRekapHtgPihak3);
  SetGrid(asgRekapHtgAsset);
  SetGrid(asgRekapHtgDPCust);
  advOfPager.ActivePageIndex := PageIndex;

  if PageIndex = 0 then begin
    SeqSupp := seqVC;
    if SeqSupp <> 0 then begin
      txtCariHtgSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqSupp)],'#',2);;
    end;
    dtpAwalHtgSupp.Date  := Tgl1;
    dtpAkhirHtgSupp.Date := Tgl2;
    btnLoadHtgSupp.Click;
  end;
  if PageIndex = 1 then begin
    SeqSupir := seqVC;
    if SeqSupir <> 0 then begin
      txtCariHtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
    end;
    dtpAwalHtgSupir.Date := Tgl1;
    dtpAkhirHtgSupir.Date := Tgl2;
    btnLoadHtgSupir.Click;
  end;
  if PageIndex = 2 then begin
    TipePihak3 := PihakKe3;
    SeqPihak3  := SeqVc;
    if SeqPihak3 <> 0 then begin
      if TipePihak3 = VC_SUPPLIER then begin
        txtCariHtgPihak3.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqPihak3)],'#',2);
      end else begin
        txtCariHtgPihak3.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqPihak3)],'#',2);
      end;
    end;
    dtpAwalHtgPihak3.Date  := Tgl1;
    dtpAkhirHtgPihak3.Date := Tgl2;
    btnLoadHtgPihak3.Click;
  end;
  if PageIndex = 3 then begin
    SeqAsset := seqVC;
    if SeqAsset <> 0 then begin
      txtCariHtgAssetSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqAsset)],'#',2);
    end;
    dtpAwalHtgAssetSupp.Date := Tgl1;
    dtpAkhirHtgAssetSUpp.Date := Tgl2;
    btnLoadHtgAssetSupp.Click;
  end;
  if PageIndex = 4 then begin
    SeqDPCust := seqVC;
    if SeqDPCust <> 0 then begin
      txtCariHtgDPCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqDPCust)],'#',2);
    end;
    dtpAwalHtgDPCust.Date := Tgl1;
    dtpAkhirHtgDPCust.Date := Tgl2;
    btnLoadHtgDPCUst.Click;
  end;
  Run(Self);
end;

procedure TfrmKartuHutang.asgRekapHtgAssetCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgAsset);
end;

procedure TfrmKartuHutang.asgRekapHtgAssetGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuHutang.asgRekapHtgAssetGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuHutang.asgRekapHtgAssetMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgAsset);
end;

procedure TfrmKartuHutang.asgRekapHtgAssetMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgAsset);
end;

procedure TfrmKartuHutang.asgRekapHtgDPCustCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmKartuHutang.asgRekapHtgDPCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuHutang.asgRekapHtgDPCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuHutang.asgRekapHtgDPCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmKartuHutang.asgRekapHtgDPCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmKartuHutang.asgRekapHtgPihak3CellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmKartuHutang.asgRekapHtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuHutang.asgRekapHtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuHutang.asgRekapHtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmKartuHutang.asgRekapHtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmKartuHutang.asgRekapHtgSupirCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmKartuHutang.asgRekapHtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuHutang.asgRekapHtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuHutang.asgRekapHtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmKartuHutang.asgRekapHtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmKartuHutang.asgRekapHtgSuppCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmKartuHutang.asgRekapHtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColDebet, ColKredit, ColSaldo, ColSeq] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmKartuHutang.asgRekapHtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColDebet, ColKredit, COlSaldo, ColSeq] then begin
    IsFloat := True;
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else IsFloat := False;
end;

procedure TfrmKartuHutang.asgRekapHtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmKartuHutang.asgRekapHtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmKartuHutang.btnCariHtgAssetSuppClick(Sender: TObject);
begin
  SeqAsset       := 0;
  txtCariHtgAssetSupp.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqAsset        := frmLov.ExecuteSupplier('',boNone);
  if SeqAsset <> 0 then begin
    txtCariHtgAssetSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqAsset)],'#',2);
  end;
end;

procedure TfrmKartuHutang.btnCariHtgDPCustClick(Sender: TObject);
begin
  SeqDPCust      := 0;
  txtCariHtgDPCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqDPCust      := frmLOV.ExecuteCustomer('',BoNone);
  if SeqDPCust <> 0 then begin
    txtCariHtgDPCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqDPCust)],'#',2);
  end;
end;

procedure TfrmKartuHutang.btnCariHtgPihak3Click(Sender: TObject);
var vTemp : string;
begin
  Application.CreateForm(TfrmLovRelasi, frmLovRelasi);
  frmLovRelasi.OnClose := frmMainMenu.ChildFormSingle;
  vTemp := frmLovRelasi.ExecuteGabungan('', false, true);
  if vTemp <> '' then begin
    TipePihak3 := EkstrakString(vTemp, '#', 1);
    SeqPihak3  := StrToIntDef(EkstrakString(vTemp, '#', 2),0);
    if TipePihak3 = UConst.VC_SUPPLIER then begin
      txtCariHtgPihak3.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end else begin
      txtCariHtgPihak3.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end;
  end else begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    txtCariHtgPihak3.Text := '';
  end;
end;

procedure TfrmKartuHutang.btnCariHtgSupirClick(Sender: TObject);
begin
  SeqSupir       := 0;
  txtCariHtgSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupir       := frmLov.ExecuteSupir('',boNone);
  if SeqSupir <> 0 then begin
    txtCariHtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
  end;
end;

procedure TfrmKartuHutang.btnCariHtgSuppClick(Sender: TObject);
begin
  SeqSupp        := 0;
  txtCariHtgSupp.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupp        := frmLov.ExecuteSupplier('',boNone);
  if SeqSupp <> 0 then begin
    txtCariHtgSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqSupp)],'#',2);
  end;
end;

procedure TfrmKartuHutang.btnCetakHtgAssetSuppClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnCetakHtgDPCustClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnCetakHtgPihak3Click(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnCetakHtgSupirClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnCetakHtgSuppClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnEksporHtgAssetSuppClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnEksporHtgDPCustClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnEksporHtgPihak3Click(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnEksporHtgSupirClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnEksporHtgSuppClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXcelling(advOfPager.ActivePageIndex);
end;

procedure TfrmKartuHutang.btnFilterHtgAssetSuppClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgAssetSupp, btnFilterHtgAssetSupp, 92);
end;

procedure TfrmKartuHutang.btnFilterHtgDPCustClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgDPCust, btnFilterHtgDPCust, 92);
end;

procedure TfrmKartuHutang.btnFilterHtgPihak3Click(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgPihak3, btnFilterHtgPihak3, 92);
end;

procedure TfrmKartuHutang.btnFilterHtgSupirClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgSupir, btnFilterHtgSupir, 92);
end;

procedure TfrmKartuHutang.btnFilterHtgSuppClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgSupp, btnFilterHtgSupp, 92);
end;

procedure TfrmKartuHutang.btnLoadHtgAssetSuppClick(Sender: TObject);
begin
  if SeqAsset = 0 then begin
    InForm('Supplier belum dipilih.');
    btnCariHtgAssetSupp.SetFocus;
  end else begin
    if dtpAkhirHtgAssetSupp.Date >= dtpAwalHtgAssetSupp.Date then begin
      LoadData(3);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgAssetSupp.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuHutang.btnLoadHtgDPCustClick(Sender: TObject);
begin
  if SeqDPCust = 0 then begin
    InForm('Customer belum dipilih.');
    btnCariHtgDPCust.SetFocus;
  end else begin
    if dtpAkhirHtgDPCust.Date >= dtpAwalHtgDPCust.Date then begin
      LoadData(4);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgDPCust.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuHutang.btnLoadHtgPihak3Click(Sender: TObject);
begin
  if SeqPihak3 = 0 then begin
    InForm('Relasi belum dipilih.');
    btnCariHtgPihak3.SetFocus;
  end else begin
    if dtpAkhirHtgPihak3.Date >= dtpAwalHtgPihak3.Date then begin
      LoadData(2);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgPihak3.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuHutang.btnLoadHtgSupirClick(Sender: TObject);
begin
  if SeqSupir = 0 then begin
    InForm('Supir belum dipilih.');
    btnCariHtgSupir.SetFocus;
  end else begin
    if dtpAkhirHtgSupir.Date >= dtpAwalHtgSupir.Date then begin
      LoadData(1);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgSupir.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuHutang.btnLoadHtgSuppClick(Sender: TObject);
begin
  if SeqSupp = 0 then begin
    InForm('Supplier belum dipilih.');
    btnCariHtgSupp.SetFocus;
  end else begin
    if dtpAkhirHtgSupp.Date >= dtpAwalhtgSupp.Date then begin
      LoadData(0);
    end else begin
      InForm('Periode akhir harus >= periode awal.');
      dtpAkhirHtgSupp.SetFocus;
      Exit;
    end;
  end;
end;

procedure TfrmKartuHutang.btnResetHtgAssetSuppClick(Sender: TObject);
begin
  InitForm(4);
  SetGrid(asgRekapHtgAsset);
end;

procedure TfrmKartuHutang.btnResetHtgDPCustClick(Sender: TObject);
begin
  InitForm(5);
  SetGrid(asgRekapHtgDPCust);
end;

procedure TfrmKartuHutang.btnResetHtgPihak3Click(Sender: TObject);
begin
  InitForm(3);
  SetGrid(asgRekapHtgPihak3);
end;

procedure TfrmKartuHutang.btnResetHtgSupirClick(Sender: TObject);
begin
  InitForm(2);
  SetGrid(asgRekapHtgSupir);
end;

procedure TfrmKartuHutang.btnResetHtgSuppClick(Sender: TObject);
begin
  InitForm(1);
  SetGrid(asgRekapHtgSupp);
end;

procedure TfrmKartuHutang.chbSFHtgAssetSuppClick(Sender: TObject);
begin
  asgRekapHtgAsset.SearchFooter.Visible := chbSFHtgAssetSupp.Checked;
end;

procedure TfrmKartuHutang.chbSFHtgDPCustClick(Sender: TObject);
begin
  asgRekapHtgDPCust.SearchFooter.Visible := chbSFHtgDPCust.Checked;
end;

procedure TfrmKartuHutang.chbSFHtgPihak3Click(Sender: TObject);
begin
  asgRekapHtgPihak3.SearchFooter.Visible := chbSFHtgPihak3.Checked;
end;

procedure TfrmKartuHutang.chbSFHtgSupirClick(Sender: TObject);
begin
  asgRekapHtgSupir.SearchFooter.Visible := chbSFHtgSupir.Checked;
end;

procedure TfrmKartuHutang.chbSFHtgSuppClick(Sender: TObject);
begin
  asgRekapHtgSupp.SearchFooter.Visible := chbSFHtgSupp.Checked;
end;

procedure TfrmKartuHutang.Execute(AMenuId: integer);
begin
  AIdMenu := AMenuId; browsemode := false;
  if (not BisaLihatLaporan(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  DateNow    := ServerNow;
  BrowseMode := false;
  InitForm(0);
  AdvOfPager.ActivePageIndex := 0; 
  SetGrid(asgRekapHtgSupp);
  SetGrid(asgRekapHtgSupir);
  SetGrid(asgRekapHtgPihak3);
  SetGrid(asgRekapHtgAsset);
  SetGrid(asgRekapHtgDPCust);
  Run(Self);
end;

procedure TfrmKartuHutang.FormShow(Sender: TObject);
begin
  if not browsemode then Execute(ACurrMenuID);
end;

procedure TfrmKartuHutang.InitForm(idx: integer);
begin
  if (idx = 0) or (idx = 1) then begin
    SeqSupp := 0;
    SetGrid(asgRekapHtgSupp);
    txtCariHtgSupp.Text       := '';
    chbSFHtgSupp.Checked      := False;
    dtpAwalHtgSupp.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgSupp.Date      := DateNow;
    asgRekapHtgSupp.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgSupp);
  end;
  if (idx = 0) or (idx = 2) then begin
    SeqSupir := 0;
    SetGrid(asgRekapHtgSupir);
    txtCariHtgSupir.Text        := '';
    chbSFHtgSupir.Checked       := False;
    dtpAwalHtgSupir.Date        := StartOfTheMonth(DateNow);
    dtpAkhirHtgSupir.Date       := DateNow;
    asgRekapHtgSupir.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgSupir);
  end;
  if (idx = 0) or (idx = 3) then begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    SetGrid(asgRekapHtgPihak3);
    txtCariHtgPihak3.Text       := '';
    chbSFHtgPihak3.Checked      := False;
    dtpAwalHtgPihak3.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgPihak3.Date      := DateNow;
    asgRekapHtgPihak3.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgPihak3);
  end;
  if (idx = 0) or (idx = 4) then begin
    SeqAsset := 0;
    SetGrid(asgRekapHtgAsset);
    txtCariHtgAssetSupp.Text    := '';
    chbSFHtgAssetSupp.Checked   := False;
    dtpAwalHtgAssetSupp.Date    := StartOfTheMonth(DateNow);
    dtpAkhirHtgAssetSupp.Date   := DateNow;
    asgRekapHtgAsset.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgAsset);
  end;
  if (idx = 0) or (idx = 5) then begin
    SeqDPCust := 0;
    SetGrid(asgRekapHtgDPCust);
    txtCariHtgDPCust.Text       := '';
    chbSFHtgDPCust.Checked      := False;
    dtpAwalHtgDPCust.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgDPCust.Date      := DateNow;
    asgRekapHtgDPCust.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgDPCust);
  end;
end;

procedure TfrmKartuHutang.LoadData(AIndex: integer);
begin
  case AIndex of
    0 : begin
        asgRekapHtgSupp.ClearNormalCells;
        asgRekapHtgSupp.RowCount := 3;
        asgRekapHtgSupp.ClearRows(asgRekapHtgSupp.RowCount-1, 1);

        UReport.LoadKartuHutang(asgRekapHtgSupp, GaugeHtgSupp, dtpAwalHtgSupp.date, dtpAkhirHtgSupp.Date, SeqSupp,
                  SALDO_HUTANG_SUPP);
        ArrangeColSize(asgRekapHtgSupp);
    end;
    1 : begin
        asgRekapHtgSupir.ClearNormalCells;
        asgRekapHtgSupir.RowCount := 3;
        asgRekapHtgSupir.ClearRows(asgRekapHtgSupir.RowCount-1, 1);

        UReport.LoadKartuHutang(asgRekapHtgSupir, GaugeHtgSupir, dtpAwalHtgSupir.date, dtpAkhirHtgSupir.Date, SeqSupir,
                  SALDO_HUTANG_JAMINAN_SUPIR);
        ArrangeColSize(asgRekapHtgSupir);
    end;
    2 : begin
        asgRekapHtgPihak3.ClearNormalCells;
        asgRekapHtgPihak3.RowCount := 3;
        asgRekapHtgPihak3.ClearRows(asgRekapHtgPihak3.RowCount-1, 1);

        if TipePihak3 = VC_SUPPLIER then begin
          UReport.LoadKartuHutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  SALDO_HUTANG_PINJAMAN_RELASI_SUPP);
        end else if TipePihak3 = VC_CUSTOMER then begin
          UReport.LoadKartuHutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  SALDO_HUTANG_PINJAMAN_RELASI_CUST);
        end;
        ArrangeColSize(asgRekapHtgPihak3);
    end;
    3 : begin
        asgRekapHtgAsset.ClearNormalCells;
        asgRekapHtgAsset.RowCount := 3;
        asgRekapHtgAsset.ClearRows(asgRekapHtgAsset.RowCount-1, 1);

        UReport.LoadKartuHutang(asgRekapHtgAsset, GaugeHtgAssetSupp, dtpAwalHtgAssetSupp.date, dtpAkhirHtgAssetSupp.Date, SeqAsset,
                  SALDO_HUTANG_ASSET_SUPP);
        ArrangeColSize(asgRekapHtgAsset);
    end;
    4 : begin
        asgRekapHtgDPCust.ClearNormalCells;
        asgRekapHtgDPCust.RowCount := 3;
        asgRekapHtgDPCust.ClearRows(asgRekapHtgDPCust.RowCount-1, 1);

        UReport.LoadKartuHutang(asgRekapHtgDPCust, GaugeHtgDPCust, dtpAwalHtgDPCust.date, dtpAkhirHtgDPCust.Date, SeqDPCust,
                  SALDO_HUTANG_DP_PARIWISATA);
        ArrangeColSize(asgRekapHtgDPCust);
    end;
  end;
end;

procedure TfrmKartuHutang.SetGrid(AGrid: TAdvStringGrid);
begin
  AGrid.ClearNormalCells;
  asgResetGrid(AGrid,3,10,1,1);
  ArrangeColSize(AGrid);
end;

procedure TfrmKartuHutang.SetPrinting(AIndex: integer);
procedure PreparePrint(AGrid: TAdvStringGrid);
  begin
    myConnection.advPrint.Grid:= AGrid;
    AGrid.PrintSettings.TitleLines.Clear;
    AGrid.PrintSettings.TitleLines.Add('');
    if AGrid = asgRekapHtgSupp then AGrid.PrintSettings.TitleLines.Add('Kartu Hutang Supplier')
    else if AGrid = asgRekapHtgSupir then AGrid.PrintSettings.TitleLines.Add('Kartu Hutang Supir')
    else if AGrid = asgRekapHtgPihak3 then AGrid.PrintSettings.TitleLines.Add('Kartu Hutang Relasi')
    else if AGrid = asgRekapHtgAsset then AGrid.PrintSettings.TitleLines.Add('Kartu Hutang Asset')
    else if AGrid = asgRekapHtgDPCust then AGrid.PrintSettings.TitleLines.Add('Kartu Hutang DP Pariwisata');
    AGrid.PrintSettings.TitleLines.Add('');
    AGrid.ColumnSize.Stretch := False;
    AGrid.HideColumn(ColSeq);
    AGrid.ColCount := 8;
    SetingPrint(AGrid);
    myConnection.advPrint.Execute;
    agrid.ColCount := 9;
    AGrid.UnHideColumn(ColSeq);
    AGrid.ColumnSize.Stretch := True;
    ArrangeColSize(AGrid);
  end;
begin
  case AIndex of
    0 : PreparePrint(asgRekapHtgSupp);
    1 : PreparePrint(asgRekapHtgSupir);
    2 : PreparePrint(asgRekapHtgPihak3);
    3 : PreparePrint(asgRekapHtgAsset);
    4 : PreparePrint(asgRekapHtgDPCust);
  end;
end;

procedure TfrmKartuHutang.SetXcelling(AIndex: integer);
begin
  case AIndex of
    0 : asgExportToExcell(asgRekapHtgSupp, myConnection.SaveToExcell);
    1 : asgExportToExcell(asgRekapHtgSupir, myConnection.SaveToExcell);
    2 : asgExportToExcell(asgRekapHtgPihak3, myConnection.SaveToExcell);
    3 : asgExportToExcell(asgRekapHtgAsset, myConnection.SaveToExcell);
    4 : asgExportToExcell(asgRekapHtgDPCust, myConnection.SaveToExcell);
  end;
end;

end.

