unit ImportDataTerminal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, tmsAdvGridExcel, StdCtrls, AdvEdit, AdvOfficeButtons, AdvGlowButton,
  Grids, AdvObj, BaseGrid, AdvGrid, AdvProgressBar, ExtCtrls, AdvPanel,
  AdvGridWorkbook, ADOInt;
                                                                                
type
  TfrmImportDataTerminal = class(TForm)
    MainPanel: TAdvPanel;
    Gauge1: TAdvProgressBar;
    Label4: TLabel;
    asgInput: TAdvStringGrid;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    chbSFooter: TAdvOfficeCheckBox;
    btnImport: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    btnimportxlsx: TAdvGlowButton;
    txtSheet: TAdvEdit;
    AddXls: TAdvGridExcelIO;
    OpenDialog1: TOpenDialog;
    asgTampung: TAdvGridWorkbook;
    asgWBGrid: TAdvStringGrid;
    Label2: TLabel;
    procedure btnResetClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure btnimportxlsxClick(Sender: TObject);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgInputMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgInputMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure txtSheetKeyPress(Sender: TObject; var Key: Char);
    procedure btnBatalClick(Sender: TObject);
    procedure chbSFooterClick(Sender: TObject);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
  private
    { Private declarations }
    procedure ArrangeColSize;
    procedure InitForm;
    procedure SetGrid;
    function IsSaved : Boolean;
    function IsValid : Boolean;
    procedure ArrangeXLS;
    procedure ArrangeXLSX;
    procedure ChangeDate; //Gunawan +280918
    procedure SetNoOrder; // Oggi 161018 +
  public
    { Public declarations }
    StatSimpan : Boolean;
    function Execute : boolean;
  end;

var
  frmImportDataTerminal: TfrmImportDataTerminal;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, OracleConnection, MainMenu, DateUtils, StrUtils;

const
  ColNo             = 0;
  ColArmada         = 1;
  ColNoOrder        = 2;
  ColSupir          = 3;
  ColJamkota1       = 4;
  ColJmlPenumpang1  = 5;
  ColJamkota2       = 6;
  ColJmlPenumpang2  = 7;
  ColKet2           = 8;
  ColKet1           = 9;
  ColKota1          = 10;
  ColKota2          = 11;
  ColTanggal        = 12;
  ColSeqSup         = 13;
  ColKotaSumber     = 14; // iqbal 240817 tambah Kolom
  ColTglTemp        = 15; // Gunawan +280918
  ColRit            = 16; // Oggi 181018 +
  ColSeqJurusan     = 17; // Oggi 181018 +

{$R *.dfm}

{ TfrmImportDataTerm= 11;inal }

procedure TfrmImportDataTerminal.ArrangeColSize;
begin
  asgInput.AutoNumberCol(colno);
  asgInput.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgInput.ColWidths[ColSeqSup]     := 0;
    asgInput.ColWidths[ColKotaSumber] := 0;
    asgInput.ColWidths[colNoorder]    := 0; // Gunawan +270918
    asgInput.ColWidths[ColTglTemp]    := 0; // Gunawan +270918
    asgInput.ColWidths[ColRit]        := 0; // Oggi 181018 +
    asgInput.ColWidths[ColSeqJurusan] := 0; // Oggi 181018 +
  end;
end;

procedure TfrmImportDataTerminal.ArrangeXLS;
var j,Row : integer;
    Tempket1,TempKota1,TempKota2, Temptanggal, jam1,jam2 : string;
    tempjam1,tempjam2 : TTime;
    dtSupir : TR_master_supir;
begin
  for j := 1 to asgWBGrid.RowCount-1 do begin
    if ((asgWBGrid.Cells[colArmada, j] = 'NO_BODY') and (asgWBGrid.Cells[colArmada-1, j] = 'NO')) or (asgWBGrid.Cells[colArmada, j] = '') then begin
      if ((asgWBGrid.Cells[colArmada, j] = 'NO_BODY') and (asgWBGrid.Cells[colArmada-1, j] = 'NO'))  then begin
        Tempket1    := asgWBGrid.Cells[colno, 1];
        TempKota1   := asgWBGrid.Cells[colJamkota1, j+1];
        TempKota2   := asgWBGrid.Cells[colJamkota2, j+1];
        Temptanggal := asgWBGrid.Cells[colsupir, 2];
      end;
      Continue;
    end;
    if asgInput.Cells[colArmada,1] <> '' then asgInput.AddRow;
    Row := asgInput.RowCount-1;

    asgInput.Cells[colArmada       ,Row]:= Trimallstring(asgWBGrid.Cells[Colarmada  ,j]);
    asgInput.Cells[colNoorder      ,Row]:= Trimallstring(asgWBGrid.Cells[colNoorder ,j]);
    asgInput.Cells[colsupir        ,Row]:= Trimallstring(asgWBGrid.Cells[colsupir   ,j]);
//    asgInput.Cells[colJamkota1     ,Row]:= asgWBGrid.Cells[colJamkota1     ,j];
    asgInput.Cells[coljmlPenumpang1,Row]:= Trimallstring(asgWBGrid.Cells[coljmlPenumpang1,j]);
//    asgInput.Cells[colJamkota2     ,Row]:= asgWBGrid.Cells[colJamkota2     ,j];
    asgInput.Cells[coljmlPenumpang2,Row]:= Trimallstring(asgWBGrid.Cells[coljmlPenumpang2,j]);
    asgInput.Cells[colket2         ,Row]:= Trimallstring(asgWBGrid.Cells[colket2         ,j]);
    asgInput.Cells[colket1         ,Row]:= Tempket1;
    asgInput.Cells[colKota1        ,Row]:= TempKota1;
    asgInput.Cells[colKota2        ,Row]:= TempKota2;
    asgInput.cells[coltanggal      ,Row]:= Temptanggal;

    tempJam1 := strtotimedef(asgWBGrid.Cells[colJamkota1,j],0);
    if tempjam1 = 0 then begin
      if CekFloat(asgWBGrid.Cells[colJamkota1,j]) then begin
        jam1 := TimeToStr(asgWBGrid.Floats[colJamkota1,j]);
        tempjam1 := StrToTime(jam1);
      end else tempJam1 := StrToTimeDef(ReplaceStr(asgWBGrid.Cells[colJamkota1,j],'.',':'),0);
      if tempJam1 = 0 then asgInput.cells[colJamkota1    ,Row]:= ''
      else asgInput.Times[colJamkota1    ,Row]:= tempJam1;
    end else asgInput.Times[colJamkota1  ,Row]:= tempJam1;

    tempJam2 := strtotimedef(asgWBGrid.Cells[colJamkota2,j],0);
    if tempjam2 = 0 then begin
      if CekFloat(asgWBGrid.Cells[colJamkota2,j]) then begin
        jam2 := TimeToStr(asgWBGrid.Floats[colJamkota2,j]);
        tempjam2 := StrToTime(jam2);
      end else tempJam2 := StrToTimeDef(ReplaceStr(asgWBGrid.Cells[colJamkota2,j],'.',':'),0);
      if tempJam2 = 0 then asgInput.cells[colJamkota2    ,Row]:= ''
      else asgInput.Times[colJamkota2    ,Row]:= tempJam2;
    end else asgInput.Times[colJamkota2  ,Row]:= tempJam2;

    dtSupir := Get_master_supir(0,Trimallstring(asgInput.Cells[colsupir,Row]));
    if dtSupir.seq <> 0 then
      asgInput.ints[colseqSup,Row] := dtSupir.seq
    else asgInput.ints[colseqSup,Row] := 0;
    asgInput.Cells[ColKotaSumber, Row] := TrimAllString(txtSheet.Text);
//    tempJam1 := strtotimedef(asgWBGrid.Cells[colJamkota1,j],0);
//    if tempjam1 = 0 then begin
//      tempJam1 := StrToTimeDef(ReplaceStr(asgWBGrid.Cells[colJamkota1,j],'.',':'),0);
//      if tempJam1 = 0 then asgInput.cells[colJamkota1    ,Row]:= ''
//      else asgInput.Times[colJamkota1    ,Row]:= tempJam1;
//    end else asgInput.Times[colJamkota1    ,Row]:= tempJam1;
//
//    tempJam2 := strtotimedef(asgWBGrid.Cells[colJamkota2,j],0);
//    if tempjam2 = 0 then begin
//      tempJam2 := StrToTimeDef(ReplaceStr(asgWBGrid.Cells[colJamkota2,j],'.',':'),0);
//      if tempJam2 = 0 then asgInput.cells[colJamkota2    ,Row]:= ''
//      else asgInput.Times[colJamkota2    ,Row]:= tempJam2;
//    end else asgInput.Times[colJamkota2    ,Row]:= tempJam2;

  end;
  asgInput.SortByColumn(ColArmada); // Oggi 181018 + agar berurutan coz mau menambahkan rits
end;

procedure TfrmImportDataTerminal.ArrangeXLSX;
var i,j,Row : integer;
    Tempket1,TempKota1,TempKota2, Temptanggal,jam1,jam2 : string;
    tempJam1,tempJam2: ttime;
    dtSupir : TR_master_supir;
begin
  for i := 0 to asgTampung.Sheets.Count-1 do begin
    asgTampung.ActiveSheet := i;
    for j := 2 to asgWBGrid.RowCount-1 do begin
      if ((asgWBGrid.Cells[colArmada, j] = 'NO_BODY') and (asgWBGrid.Cells[colArmada-1, j] = 'NO')) or (asgWBGrid.Cells[colArmada, j] = '') then begin
        if ((asgWBGrid.Cells[colArmada, j] = 'NO_BODY') and (asgWBGrid.Cells[colArmada-1, j] = 'NO'))  then begin
          Tempket1    := asgWBGrid.Cells[colno, 1];
          TempKota1   := asgWBGrid.Cells[colJamkota1, j+1];
          TempKota2   := asgWBGrid.Cells[colJamkota2, j+1];
          Temptanggal := asgWBGrid.Cells[colsupir, 2];
        end;
        Continue;
      end;
      if asgInput.Cells[colArmada,1] <> '' then asgInput.AddRow;
      Row := asgInput.RowCount-1;

      asgInput.Cells[colArmada       ,Row]:= Trimallstring(asgWBGrid.Cells[Colarmada  ,j]);
      asgInput.Cells[colNoorder      ,Row]:= Trimallstring(asgWBGrid.Cells[colNoorder ,j]);
      asgInput.Cells[colsupir        ,Row]:= Trimallstring(asgWBGrid.Cells[colsupir   ,j]);
//      asgInput.Cells[colJamkota1     ,Row]:= asgWBGrid.Cells[colJamkota1     ,j];
      asgInput.Cells[coljmlPenumpang1,Row]:= Trimallstring(asgWBGrid.Cells[coljmlPenumpang1,j]);
//      asgInput.Cells[colJamkota2     ,Row]:= asgWBGrid.Cells[colJamkota2     ,j];
      asgInput.Cells[coljmlPenumpang2,Row]:= Trimallstring(asgWBGrid.Cells[coljmlPenumpang2,j]);
      asgInput.Cells[colket2         ,Row]:= Trimallstring(asgWBGrid.Cells[colket2         ,j]);
      asgInput.Cells[colket1         ,Row]:= Tempket1;
      asgInput.Cells[colKota1        ,Row]:= TempKota1;
      asgInput.Cells[colKota2        ,Row]:= TempKota2;
      asgInput.cells[coltanggal      ,Row]:= Temptanggal;

      dtSupir := Get_master_supir(0,Trimallstring(asgInput.Cells[colsupir,Row]));
      if dtSupir.seq <> 0 then
        asgInput.ints[colseqSup,Row] := dtSupir.seq
      else asgInput.ints[colseqSup,Row] := 0;

      tempJam1 := strtotimedef(asgWBGrid.Cells[colJamkota1,j],0);
      if tempjam1 = 0 then begin
        if CekFloat(asgWBGrid.Cells[colJamkota1,j]) then begin
          jam1 := TimeToStr(asgWBGrid.Floats[colJamkota1,j]);
          tempjam1 := StrToTime(jam1);
        end else tempJam1 := StrToTimeDef(ReplaceStr(asgWBGrid.Cells[colJamkota1,j],'.',':'),0);
        if tempJam1 = 0 then asgInput.cells[colJamkota1    ,Row]:= ''
        else asgInput.Times[colJamkota1,   Row]:= tempJam1;
      end else asgInput.Times[colJamkota1, Row]:= tempJam1;

      tempJam2 := strtotimedef(asgWBGrid.Cells[colJamkota2,j],0);
      if tempjam2 = 0 then begin
        if CekFloat(asgWBGrid.Cells[colJamkota2,j]) then begin
          jam2 := TimeToStr(asgWBGrid.Floats[colJamkota2,j]);
          tempjam2 := StrToTime(jam2);
        end else tempJam2 := StrToTimeDef(ReplaceStr(asgWBGrid.Cells[colJamkota2,j],'.',':'),0);
        if tempJam2 = 0 then asgInput.cells[colJamkota2    ,Row]:= ''
        else asgInput.Times[colJamkota2,   Row]:= tempJam2;
      end else asgInput.Times[colJamkota2, Row]:= tempJam2;
      //iqbal 240817 tambah kolom kota sumber
      asgInput.Cells[ColKotaSumber, Row] := TrimAllString(txtSheet.Text);
    end;
  end;
  asgInput.SortByColumn(ColArmada); // Oggi 181018 + agar berurutan coz mau menambahkan rits
end;


procedure TfrmImportDataTerminal.asgInputCanDeleteRow(Sender: TObject;
  ARow: Integer; var CanDelete: Boolean);
begin
  if (arow > 0)and (asginput.cells[colarmada,arow] <> '') then CanDelete := true;
end;

procedure TfrmImportDataTerminal.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  canedit := false;
  if ARow > 0 then begin
    if acol in [coljmlPenumpang2, coljmlPenumpang1] then canedit := true
    else canedit := false
  end;
end;

procedure TfrmImportDataTerminal.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
   if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, coljmlPenumpang1, coljmlPenumpang2] then HAlign := taRightJustify;
  VAlign := vtaCenter;

end;

procedure TfrmImportDataTerminal.asgInputMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  arrangecolsize;
end;

procedure TfrmImportDataTerminal.asgInputMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  arrangecolsize;
end;

procedure TfrmImportDataTerminal.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmImportDataTerminal.btnImportClick(Sender: TObject);
var i : integer;
begin
  OpenDialog1.Filter := 'Excel 97-2003 Workbook|*xls';
  if TrimAllString(txtSheet.Text) = '' then begin
    Inform('Nama sheet belum diisi.');
    txtSheet.SetFocus;
    exit;
  end else begin
  OpenDialog1.InitialDir := GetAppPath;
    if OpenDialog1.Execute then begin
      SetGrid;

      asgTampung.Sheets.Clear;
      asgWBGrid.ClearNormalCells;
      asgWBGrid.RowCount := 3;
      asgWBGrid.FixedCols := 1;
      asgWBGrid.FixedRows := 2;

                                                      //iqbal 050917 tambah Sheet
      asgWBGrid.LoadFromXLSSheet(OpenDialog1.FileName,TrimAllString(txtSheet.Text));
      i := 2;
      while (i <= asgWBGrid.RowCount-1) do begin
        i := i + 1;
        if (i <> asgWBGrid.RowCount) and ((TrimAllString(asgWBGrid.Cells[ColNo, i])= '') and
          (TrimAllString(asgWBGrid.Cells[Colarmada,i]) = '') and (TrimAllString(asgWBGrid.Cells[colJamkota1,i]) = '')) then begin
          if (asgWBGrid.RowCount > 3) then begin
            asgWBGrid.RemoveRows(i,1);
            i := i - 1;
          end else asgWBGrid.ClearRows(i,1);
        end;
      end;

  //    SetGrid;

      ArrangeXLS;
      ChangeDate;
      SetNoOrder; // Oggi 161018 +
      ArrangeColSize;
    end;
  end;
end;

procedure TfrmImportDataTerminal.btnimportxlsxClick(Sender: TObject);
var i : integer;
begin
  OpenDialog1.Filter:= 'Excel Workbook|*xlsx';
  if TrimAllString(txtSheet.Text) = '' then begin
    Inform('Nama sheet belum diisi.');
    txtSheet.SetFocus;
    exit;
  end else begin
    OpenDialog1.InitialDir := GetAppPath;
    if OpenDialog1.Execute then begin
      SetGrid;

      asgTampung.Sheets.Clear;
      asgTampung.AddSheet(TrimAllString(txtSheet.Text));
      asgWBGrid.ClearNormalCells;
      asgWBGrid.RowCount  := 3;
      asgWBGrid.FixedCols := 1;
      asgWBGrid.FixedRows := 2;

      asgWBGrid.LoadFromXLSSheet(OpenDialog1.FileName, TrimAllString(txtSheet.Text));
      i := 2;
      while (i <= asgWBGrid.RowCount-1) do begin
        i := i + 1;
        if (i <> asgWBGrid.RowCount) and ((TrimAllString(asgWBGrid.Cells[ColNo, i])= '') and
          (TrimAllString(asgWBGrid.Cells[Colarmada,i]) = '') and (TrimAllString(asgWBGrid.Cells[colJamkota1,i]) = '')) then begin
          if (asgWBGrid.RowCount > 3) then begin
            asgWBGrid.RemoveRows(i,1);
            i := i - 1;
          end else asgWBGrid.ClearRows(i,1);
        end;
      end;

      SetGrid;
      ArrangeXLSX;
      ChangeDate;
      SetNoOrder; // Oggi 161018 +
      ArrangeColSize;
    end;
  end;
end;


procedure TfrmImportDataTerminal.btnResetClick(Sender: TObject);
begin
  InitForm; SetGrid;
end;

procedure TfrmImportDataTerminal.btnSimpanClick(Sender: TObject);
begin
  btnSimpan.Enabled := false;
  if (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
//    if editmode then begin
//      if isSavedEdit then begin
//        Inform(MSG_SUCCESS_UPDATE);
//        ModalResult := mrOk;
//      end else Inform(MSG_UNSUCCESS_UPDATE);
//    end else begin
      if IsSaved then begin
        if Confirmed(MSG_ADD_DATA) then begin
          initform; Setgrid; statSimpan := true;
        end else ModalResult := mrOk;
      end else begin
        Inform(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
//    end;
  end ;
  btnSimpan.Enabled := true;
end;

procedure TfrmImportDataTerminal.ChangeDate;
var i : integer;
    Temp,tglTemp, BulanTemp, TahunTemp : String;
begin
  tglTemp:=''; BulanTemp:=''; TahunTemp:='';
  Temp :='';
  for i := 1 to asgInput.RowCount-1 do begin
    Temp := EkstrakString(asgInput.Cells[ColTanggal, i],',',2);
    tglTemp := EkstrakString(Temp,' ',2);
    BulanTemp := EkstrakString(Temp, ' ',3);
    if BulanTemp = 'JANUARI' then BulanTemp := '01';
    if BulanTemp = 'FEBRUARI' then BulanTemp := '02';
    if BulanTemp = 'MARET' then BulanTemp := '03';
    if BulanTemp = 'APRIL' then BulanTemp := '04';
    if BulanTemp = 'MEI' then BulanTemp := '05';
    if BulanTemp = 'JUNI' then BulanTemp := '06';
    if BulanTemp = 'JULI' then BulanTemp := '07';
    if BulanTemp = 'AGUSTUS' then BulanTemp := '08';
    if BulanTemp = 'SEPTEMBER' then BulanTemp := '09';
    if BulanTemp = 'OKTOBER' then BulanTemp := '10';
    if BulanTemp = 'NOVEMBER' then BulanTemp := '11';
    if BulanTemp = 'DESEMBER' then BulanTemp := '12';
    TahunTemp := EkstrakString(Temp, ' ',4);
    asgInput.Dates[ColTglTemp, i] := varToDateTime(tglTemp+'/'+bulanTemp+'/'+TahunTemp);
  end;
  ArrangeColSize;
end;

procedure TfrmImportDataTerminal.chbSFooterClick(Sender: TObject);
begin
  asgInput.SearchFooter.Visible := chbSFooter.Checked;
end;

function TfrmImportDataTerminal.execute: boolean;
begin
//  Columns := asgData.ColumnHeaders.Text;
  InitForm;
  SetGrid;
  StatSimpan := False;
  Self.Caption := 'Import Data Terminal';
  Run(Self,False);
  Result := ModalResult = mrok;
end;

procedure TfrmImportDataTerminal.InitForm;
begin
  txtSheet.Text := '';
  chbSFooter.Checked := False;
end;

function TfrmImportDataTerminal.IsSaved: Boolean;
var dtMst : TR_MASTER_OPERASIONAL_TERMINAL;
    dtSupir : TR_master_supir;
    dtArmada : TR_master_armada;
    dtsj : TR_sj_armada_master;
    dtKota , dtKota2, dtKotaSumber: TR_master_kota;
    i : integer;
begin
  try
    for i := 1 to asgInput.RowCount-1 do begin
      dtMst.nobody        := asgInput.Cells[colArmada, i];
      dtMst.Noorder       := asgInput.Cells[colNoorder, i]; //gunawan tutup 270918  // Oggi 161018 buka lg
//      dtMst.Noorder       := '';
      dtMst.supir         := asgInput.Cells[colsupir, i];

      if asgInput.cells[colJamkota1,i] <> '' then
        dtMst.Jamkota1    := vartodatetime(asgInput.cells[colJamkota1,i])
      else dtMst.Jamkota1 :=  0;
      dtMst.jmlPenumpang1 := asgInput.Ints[coljmlPenumpang1, i];
      
      if asgInput.cells[colJamkota2,i] <> '' then
        dtMst.Jamkota2    := vartodatetime(asgInput.cells[colJamkota2,i])
      else dtMst.Jamkota2 :=  0;
      dtMst.jmlPenumpang2 := asgInput.Ints[coljmlPenumpang2, i];
                              
      dtMst.ket2          := asgInput.Cells[colket2, i];
      dtMst.ket1          := asgInput.Cells[colket1, i];
      dtMst.Kota1         := asgInput.Cells[colKota1, i];
      dtMst.Kota2         := asgInput.Cells[colKota2, i];
//      dtMst.tanggal     := DateStrToDate(asgInput.Cells[coltanggal, i]);  //gunawan ngambilnya dari ColtglTemp 280918
      dtMst.tanggal       := asgInput.Dates[coltglTemp, i];

//      dtSupir := Get_master_supir(0,dtMst.supir);
//      dtMst.Supir_SEQ := dtSupir.seq;
      dtMst.Supir_SEQ       := asgInput.Ints[colseqSup,i];
      dtArmada := Get_master_armada(0,dtMst.nobody);
      dtMst.Armada_SEQ      := dtArmada.seq;
      dtSJ     := Get_sj_armada_master(0,dtMst.Noorder);
      dtMst.order_SEQ       := dtSJ.seq;
      dtKota   := Get_master_kota(0,dtMst.Kota1);
      dtMst.kota1_SEQ       := dtKota.seq;
      dtKota2  := Get_master_kota(0,dtMst.Kota2);
      dtMst.kota2_SEQ       := dtKota2.seq;
      //iqbal 310817 tambah save untuk Kota Sumber
      dtKotaSumber          := Get_master_kota(0,TrimAllString(asgInput.Cells[ColKotaSumber, i]));
      dtMst.Kota_Sumber_seq := dtKotaSumber.Seq;
      dtMst.jurusan_seq     := asgInput.Ints[ColSeqJurusan, i]; // Oggi 181018 +
      if (dtMst.nobody <> '' ) and (dtMst.supir <> '') then begin
        Save_master_OPERASIONAL_TERMINAL(dtMst);
      end;
    end;
    Result := true;
    myConnection.EndSQL;
  except
    Result := false;
    myConnection.UndoSQL;
  end;
end;

function TfrmImportDataTerminal.IsValid: Boolean;
var i: integer;
    tempJml1,tempJml2 : integer;
begin
  result := true;
  for i := 1 to asgInput.RowCount-1 do begin
    if asgInput.ints[colseqSup,i] = 0 then begin
      inform('Supir tidak ada dalam Database.');
      asgInput.row := i;
      asgInput.Col := colsupir;
      asgInput.SetFocus;
      result := false;
      exit;
    end;

    if asgInput.Cells[coljmlPenumpang1,i] <> '' then begin
      tempJml1 := StrToIntDef(asgInput.Cells[coljmlPenumpang1,i],0);
      if tempJml1 = 0 then begin
        inform('Pada baris ke - '+inttostr(i)+', jumlah penumpang tidak sesuai.');
        asgInput.row := I;
        asgInput.Col := coljmlPenumpang1;
        asgInput.SetFocus;
        result := false;
        exit;
      end;
    end;

    if asgInput.Cells[coljmlPenumpang2,i] <> '' then begin
      tempJml2 := StrToIntDef(asgInput.Cells[coljmlPenumpang2,i],0);
      if tempJml2 = 0 then begin
        inform('Pada baris ke - '+inttostr(i)+', Jumlah penumpang tidak sesuai.');
        asgInput.row := I;
        asgInput.Col := coljmlPenumpang2;
        asgInput.SetFocus;
        result := false;
        exit;
      end;
    end;

  end;
  if asgInput.cells[colArmada,1] = '' then begin
    inform('Tidak ada data yang diimport.');
    asgInput.row := 1;
    asgInput.Col := colArmada;
    asgInput.SetFocus;
    result := false;
    exit;
  end;
end;

procedure TfrmImportDataTerminal.SetGrid;
begin
  asgInput.ClearNormalCells;
  asgInput.RowCount  := 2;  //iqbal 240817 tambah kolom 
  asgInput.ColCount  := 19{15};
  asgInput.FixedCols := 1;
  asgInput.FixedRows := 1;
  ArrangeColSize;
end;

procedure TfrmImportDataTerminal.SetNoOrder;
var vBuff : _RecordSet;
    i, rit : integer;
    TempArmada : String;
begin
  {// isi rit
  TempArmada := ''; rit := 0;
  for i := 1 to asgInput.RowCount-1 do begin
    if TempArmada <> asgInput.Cells[ColArmada, i] then begin
      rit := 0;
      rit := rit+1;
      asgInput.Ints[ColRit, i] := rit;
    end else begin
      rit := rit+1;
      asgInput.Ints[ColRit, i] := rit;
    end;
    TempArmada := asgInput.Cells[ColArmada, i];
  end;}

  for i := 1 to asgInput.RowCount-1 do begin
    sql := 'SELECT m.nomor, j.seq '+
              ' FROM sj_armada_master m, master_armada a, master_supir s, master_jurusan j '+
              ' WHERE m.armada_seq = a.seq AND m.supir_seq = s.seq AND m.jurusan_seq = j.seq '+
              ' AND m.tanggal = '+FormatSQLDate(asgInput.Dates[ColTglTemp, i])+
              ' AND a.no_body = '+FormatSQLString(asgInput.Cells[ColArmada, i])+
              ' AND s.nama = '+FormatSQLString(asgInput.Cells[ColSupir, i])+
//              ' AND m.rit = '+FormatSQLNumber(asgInput.Ints[ColRit, i])+
              ' AND m.jurusan_seq IN (SELECT master_seq FROM detail_jurusan dj, master_kota k '+
                    ' WHERE dj.master_seq = j.seq AND dj.kota_seq = k.seq '+
                    ' AND k.nama = '+FormatSQLString(asgInput.Cells[ColKota1, i])+
                    ' AND URUTAN = 1) '+
              ' AND m.jurusan_seq IN (SELECT master_seq FROM detail_jurusan dj, master_kota k '+
                    ' WHERE dj.master_seq = j.seq AND dj.kota_seq = k.seq '+
                    ' AND k.nama = '+FormatSQLString(asgInput.Cells[ColKota2, i])+
                    ' AND URUTAN = (SELECT MAX(urutan) FROM detail_jurusan dj WHERE dj.master_seq = j.seq)) ';
    vBuff := myConnection.OpenSQL(sql);
    if vBuff.RecordCount > 0 then begin
      asgInput.Cells[ColNoOrder,    i] := BufferToString(vBuff.Fields[0].Value);
      asgInput.Ints[ColSeqJurusan,  i] := BufferToInteger(vBuff.Fields[1].Value);
    end;
    vBuff.Close;
  end;
end;

procedure TfrmImportDataTerminal.txtSheetKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then btnimportxlsx.SetFocus;
end;

end.
