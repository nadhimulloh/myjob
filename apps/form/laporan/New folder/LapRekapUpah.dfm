object frmLapRekapUpah: TfrmLapRekapUpah
  Left = 299
  Top = 157
  Caption = 'frmLapRekapUpah'
  ClientHeight = 542
  ClientWidth = 817
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TAdvPanel
    Left = 0
    Top = 0
    Width = 817
    Height = 542
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    UseDockManager = True
    Version = '2.1.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clWindowText
    Caption.Font.Height = -11
    Caption.Font.Name = 'Tahoma'
    Caption.Font.Style = []
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    ExplicitTop = 8
    FullHeight = 200
    object AdvPanel4: TAdvPanel
      Left = 1
      Top = 96
      Width = 815
      Height = 445
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      UseDockManager = True
      Version = '2.1.0.3'
      Caption.Color = clHighlight
      Caption.ColorTo = clNone
      Caption.Font.Charset = DEFAULT_CHARSET
      Caption.Font.Color = clWindowText
      Caption.Font.Height = -11
      Caption.Font.Name = 'Tahoma'
      Caption.Font.Style = []
      StatusBar.Font.Charset = DEFAULT_CHARSET
      StatusBar.Font.Color = clWindowText
      StatusBar.Font.Height = -11
      StatusBar.Font.Name = 'Tahoma'
      StatusBar.Font.Style = []
      ExplicitLeft = 8
      ExplicitTop = 119
      ExplicitWidth = 860
      ExplicitHeight = 407
      DesignSize = (
        815
        445)
      FullHeight = 200
      object Gauge: TAdvProgressBar
        Left = 678
        Top = 11
        Width = 128
        Height = 18
        Anchors = [akTop, akRight]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Level0ColorTo = 14811105
        Level1ColorTo = 13303807
        Level2Color = 5483007
        Level2ColorTo = 11064319
        Level3ColorTo = 13290239
        Level1Perc = 70
        Level2Perc = 90
        Position = 50
        ShowBorder = True
        Version = '1.2.0.2'
        Visible = False
      end
      object btnEkspor: TAdvGlowButton
        Left = 88
        Top = 8
        Width = 75
        Height = 25
        Caption = 'E&kspor'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 0
        OnClick = btnEksporClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnCetak: TAdvGlowButton
        Left = 169
        Top = 8
        Width = 75
        Height = 25
        Caption = '&Cetak'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 1
        OnClick = btnCetakClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnFilter: TAdvGlowButton
        Left = 7
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Tutup &Filter'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 2
        OnClick = btnFilterClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object asgRekap: TAdvStringGrid
        Tag = 1
        Left = 7
        Top = 39
        Width = 799
        Height = 398
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 9
        Ctl3D = True
        RowCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
        ParentCtl3D = False
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 3
        OnMouseWheelDown = asgRekapMouseWheelDown
        OnMouseWheelUp = asgRekapMouseWheelUp
        OnGetAlignment = asgRekapGetAlignment
        OnGetFormat = asgRekapGetFormat
        OnGetFloatFormat = asgRekapGetFloatFormat
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'MS Sans Serif'
        ActiveCellFont.Style = [fsBold]
        AutoSize = True
        ColumnHeaders.Strings = (
          'No.'
          'NIK'
          'Nama'
          'Nominal'
          'Nama Bank'
          'No. Rekening'
          'Keterangan'
          'Seq'
          '')
        ColumnSize.Stretch = True
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.ControlStyle = csClassic
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        DragScrollOptions.Delays.InitialDelay = 11535336
        EnhRowColMove = False
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedFooters = 1
        FixedRowHeight = 22
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = []
        FloatFormat = '%.2n'
        FloatingFooter.Visible = True
        PrintSettings.FooterSize = 70
        PrintSettings.HeaderSize = 70
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.PageNr = ppTopRight
        PrintSettings.Title = ppTopCenter
        PrintSettings.TitleLines.Strings = (
          'TES')
        PrintSettings.Font.Charset = ANSI_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -16
        PrintSettings.Font.Name = 'Arial'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'MS Sans Serif'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'MS Sans Serif'
        PrintSettings.HeaderFont.Style = [fsBold]
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWhite
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'MS Sans Serif'
        PrintSettings.FooterFont.Style = []
        PrintSettings.LeftSize = 70
        PrintSettings.RightSize = 70
        PrintSettings.TitleSpacing = 70
        PrintSettings.PageNumSep = '/'
        PrintSettings.NoAutoSizeRow = True
        ScrollSynch = True
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'MS Sans Serif'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurence'
        SearchFooter.HintFindPrev = 'Find previous occurence'
        SearchFooter.HintHighlight = 'Highlight occurences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SearchFooter.ShowHighLight = False
        SearchFooter.ShowMatchCase = False
        SelectionColor = clHighlight
        SelectionTextColor = clHighlightText
        ShowDesignHelper = False
        Version = '6.0.4.2'
        WordWrap = False
        ExplicitWidth = 844
        ExplicitHeight = 355
        ColWidths = (
          64
          29
          39
          49
          65
          76
          68
          30
          374)
      end
      object chbShowSF: TAdvOfficeCheckBox
        Left = 331
        Top = 10
        Width = 120
        Height = 20
        TabOrder = 4
        OnClick = chbShowSFClick
        Alignment = taLeftJustify
        Caption = 'Show Search Footer'
        ReturnIsTab = False
        Version = '1.3.4.1'
      end
      object chbShowDecimal: TAdvOfficeCheckBox
        Left = 456
        Top = 10
        Width = 116
        Height = 20
        TabOrder = 5
        OnClick = chbShowDecimalClick
        Alignment = taLeftJustify
        Caption = 'Show Decimal (.00)'
        ReturnIsTab = False
        Version = '1.3.4.1'
      end
      object btnCetakSlip: TAdvGlowButton
        Left = 250
        Top = 8
        Width = 75
        Height = 25
        Caption = 'C&etak Slip Gaji'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 6
        OnClick = btnCetakSlipClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
    end
    object pnlFilter: TPanel
      Left = 1
      Top = 1
      Width = 815
      Height = 95
      Align = alTop
      TabOrder = 1
      ExplicitLeft = 2
      ExplicitTop = 3
      object btnLoad: TAdvGlowButton
        Left = 439
        Top = 64
        Width = 75
        Height = 25
        Caption = '&Load'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 0
        TabStop = True
        OnClick = btnLoadClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnReset: TAdvGlowButton
        Left = 520
        Top = 64
        Width = 75
        Height = 25
        Caption = '&Reset'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        TabOrder = 1
        TabStop = True
        OnClick = btnResetClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object AdvPanel3: TAdvPanel
        Left = 188
        Top = 8
        Width = 245
        Height = 81
        BevelInner = bvLowered
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        UseDockManager = True
        Version = '2.1.0.3'
        Caption.Color = clHighlight
        Caption.ColorTo = clNone
        Caption.Font.Charset = DEFAULT_CHARSET
        Caption.Font.Color = clHighlightText
        Caption.Font.Height = -11
        Caption.Font.Name = 'MS Sans Serif'
        Caption.Font.Style = []
        Caption.Indent = 2
        Caption.Text = 'Filter Laporan'
        Caption.TopIndent = 2
        Caption.Visible = True
        StatusBar.Font.Charset = DEFAULT_CHARSET
        StatusBar.Font.Color = clWindowText
        StatusBar.Font.Height = -11
        StatusBar.Font.Name = 'Tahoma'
        StatusBar.Font.Style = []
        FullHeight = 83
        object Label2: TLabel
          Left = 8
          Top = 29
          Width = 40
          Height = 13
          Caption = 'Pegawai'
        end
        object Label1: TLabel
          Left = 8
          Top = 56
          Width = 64
          Height = 13
          Caption = 'No. Rekening'
        end
        object txtPegawai: TAdvEdit
          Left = 78
          Top = 26
          Width = 133
          Height = 21
          EditType = etUppercase
          FocusColor = clWindow
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'MS Sans Serif'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          TabOrder = 0
          Visible = True
          Version = '2.9.2.1'
        end
        object btnLOVPeg: TAdvGlowButton
          Left = 217
          Top = 26
          Width = 20
          Height = 21
          Caption = '...'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          TabOrder = 1
          OnClick = btnLOVPegClick
          Appearance.ColorChecked = 16111818
          Appearance.ColorCheckedTo = 16367008
          Appearance.ColorDisabled = 15921906
          Appearance.ColorDisabledTo = 15921906
          Appearance.ColorDown = 16111818
          Appearance.ColorDownTo = 16367008
          Appearance.ColorHot = 16117985
          Appearance.ColorHotTo = 16372402
          Appearance.ColorMirrorHot = 16107693
          Appearance.ColorMirrorHotTo = 16775412
          Appearance.ColorMirrorDown = 16102556
          Appearance.ColorMirrorDownTo = 16768988
          Appearance.ColorMirrorChecked = 16102556
          Appearance.ColorMirrorCheckedTo = 16768988
          Appearance.ColorMirrorDisabled = 11974326
          Appearance.ColorMirrorDisabledTo = 15921906
        end
        object txtNorek: TAdvEdit
          Left = 78
          Top = 53
          Width = 159
          Height = 21
          EditType = etRange
          FocusColor = clWindow
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'MS Sans Serif'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          TabOrder = 2
          Visible = True
          Version = '2.9.2.1'
        end
      end
      object AdvPanel1: TAdvPanel
        Left = 7
        Top = 8
        Width = 175
        Height = 81
        BevelInner = bvLowered
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        UseDockManager = True
        Version = '2.1.0.3'
        Caption.Color = clHighlight
        Caption.ColorTo = clNone
        Caption.Font.Charset = DEFAULT_CHARSET
        Caption.Font.Color = clHighlightText
        Caption.Font.Height = -11
        Caption.Font.Name = 'MS Sans Serif'
        Caption.Font.Style = []
        Caption.Indent = 2
        Caption.Text = 'Filter Periode & Departemen'
        Caption.TopIndent = 2
        Caption.Visible = True
        StatusBar.Font.Charset = DEFAULT_CHARSET
        StatusBar.Font.Color = clWindowText
        StatusBar.Font.Height = -11
        StatusBar.Font.Name = 'Tahoma'
        StatusBar.Font.Style = []
        FullHeight = 83
        object Label3: TLabel
          Left = 8
          Top = 29
          Width = 36
          Height = 13
          Caption = 'Periode'
        end
        object Label7: TLabel
          Left = 8
          Top = 57
          Width = 23
          Height = 13
          Caption = 'Dept'
        end
        object dtpPeriode: TDateTimePicker
          Left = 50
          Top = 26
          Width = 120
          Height = 21
          Date = 38775.514171921300000000
          Format = 'MMMM yyyy'
          Time = 38775.514171921300000000
          TabOrder = 0
        end
        object cmbDept: TCheckListEdit
          Left = 50
          Top = 53
          Width = 120
          Height = 21
          AutoDropWidthSize = False
          TabOrder = 1
          DropWidth = 121
          DropHeight = 70
          DropFont.Charset = DEFAULT_CHARSET
          DropFont.Color = clWindowText
          DropFont.Height = -11
          DropFont.Name = 'Tahoma'
          DropFont.Style = []
          TextDelimiter = ','
          TextEndChar = ']'
          TextStartChar = '['
          OnClickCheck = cmbDeptClickCheck
          Version = '1.3.6.4'
        end
      end
      object AdvPanel2: TAdvPanel
        Left = 439
        Top = 8
        Width = 133
        Height = 50
        BevelInner = bvLowered
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        UseDockManager = True
        Version = '2.1.0.3'
        Caption.Color = clHighlight
        Caption.ColorTo = clNone
        Caption.Font.Charset = DEFAULT_CHARSET
        Caption.Font.Color = clHighlightText
        Caption.Font.Height = -11
        Caption.Font.Name = 'MS Sans Serif'
        Caption.Font.Style = []
        Caption.Indent = 2
        Caption.Text = 'Filter Tipe'
        Caption.TopIndent = 2
        Caption.Visible = True
        StatusBar.Font.Charset = DEFAULT_CHARSET
        StatusBar.Font.Color = clWindowText
        StatusBar.Font.Height = -11
        StatusBar.Font.Name = 'Tahoma'
        StatusBar.Font.Style = []
        FullHeight = 83
        object Label4: TLabel
          Left = 8
          Top = 29
          Width = 20
          Height = 13
          Caption = 'Tipe'
        end
        object cmbTipe: TAdvComboBox
          Left = 48
          Top = 26
          Width = 79
          Height = 21
          Color = clWindow
          Version = '1.4.0.0'
          Visible = True
          Style = csDropDownList
          DropWidth = 0
          Enabled = True
          ItemIndex = 0
          ItemHeight = 13
          Items.Strings = (
            'SEMUA'
            'SPM'
            'SPA')
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          TabOrder = 0
          Text = 'SEMUA'
        end
      end
    end
  end
  object ppPrint: TPopupMenu
    Left = 585
    Top = 104
    object CetakSemua1: TMenuItem
      Caption = 'Cetak Semua'
      OnClick = CetakSemua1Click
    end
    object CetakPer1: TMenuItem
      Caption = 'Cetak Salah Satu'
      OnClick = CetakPer1Click
    end
    object CetakSemuaFormatLama1: TMenuItem
      Caption = 'Cetak Semua (Format Lama)'
      OnClick = CetakSemuaFormatLama1Click
    end
    object CetakSemuaFormatLama2: TMenuItem
      Caption = 'Cetak Salah Satu (Format Lama)'
      OnClick = CetakSemuaFormatLama2Click
    end
  end
end
