unit RekapHutang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficePager, ExtCtrls, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid,
  Gauges, ComCtrls, StdCtrls, AdvEdit, AdvPanel, AdvGlowButton, OracleConnection, UConst, URecord,
  UEngine, UReport, USystemMenu, UGeneral, DateUtils;

type
  TfrmRekapHutang = class(TForm)
    MainPanel: TPanel;
    AdvOfficePager1: TAdvOfficePager;
    pcgSupplier: TAdvOfficePage;
    pcgSupir: TAdvOfficePage;
    pcgPihak3: TAdvOfficePage;
    pcgAsset: TAdvOfficePage;
    pcgDPPariwisata: TAdvOfficePage;
    pnlFilterHtgPihak3: TPanel;
    AdvPanel3: TAdvPanel;
    txtCariHtgPihak3: TAdvEdit;
    btnCariSupplierPihak3: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label3: TLabel;
    Label4: TLabel;
    dtpAwalHtgPihak3: TDateTimePicker;
    dtpAkhirHtgPihak3: TDateTimePicker;
    btnLoadHtgPihak3: TAdvGlowButton;
    btnResetHtgPihak3: TAdvGlowButton;
    Panel4: TPanel;
    GaugeHtgPihak3: TGauge;
    btnFilterHtgPihak3: TAdvGlowButton;
    btnEksporHtgPihak3: TAdvGlowButton;
    btnCetakHtgPihak3: TAdvGlowButton;
    asgRekapHtgPihak3: TAdvStringGrid;
    chbSFHtgPihak3: TAdvOfficeCheckBox;
    btnDetailHtgPihak3: TAdvGlowButton;
    chbTHtgPihak3: TAdvOfficeCheckBox;
    Panel3: TPanel;
    GaugeHtgSupp: TGauge;
    btnFilterHtgSupp: TAdvGlowButton;
    btnEksporHtgSupp: TAdvGlowButton;
    btnCetakHtgSupp: TAdvGlowButton;
    asgRekapHtgSupp: TAdvStringGrid;
    chbSFHtgSupp: TAdvOfficeCheckBox;
    btnDetailHtgSupp: TAdvGlowButton;
    chbTHtgSupp: TAdvOfficeCheckBox;
    pnlFilterHtgSupp: TPanel;
    btnResetHtgSupp: TAdvGlowButton;
    btnLoadHtgSupp: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    txtCariHtgSupp: TAdvEdit;
    btnCariHtgSupp: TAdvGlowButton;
    PnlStatus: TAdvPanel;
    Label1: TLabel;
    Label5: TLabel;
    dtpAwalHtgSupp: TDateTimePicker;
    dtpAkhirHtgSupp: TDateTimePicker;
    pnlFilterHtgAssetSupp: TPanel;
    AdvPanel10: TAdvPanel;
    Label11: TLabel;
    Label6: TLabel;
    dtpAwalHtgAssetSupp: TDateTimePicker;
    dtpAkhirHtgAssetSupp: TDateTimePicker;
    AdvPanel11: TAdvPanel;
    txtCariHtgAssetSupp: TAdvEdit;
    btnCariHtgAssetSupp: TAdvGlowButton;
    btnLoadHtgAssetSupp: TAdvGlowButton;
    btnResetHtgAssetSupp: TAdvGlowButton;
    Panel10: TPanel;
    GaugeHtgAssetSupp: TGauge;
    btnFilterHtgAssetSupp: TAdvGlowButton;
    btnEksporHtgAssetSupp: TAdvGlowButton;
    btnCetakHtgAssetSupp: TAdvGlowButton;
    asgRekapHtgAssetSupp: TAdvStringGrid;
    chbSFHtgAssetSupp: TAdvOfficeCheckBox;
    btnDetailHtgAssetSupp: TAdvGlowButton;
    chbTHtgAssetSupp: TAdvOfficeCheckBox;
    pnlFilterHtgDPCust: TPanel;
    AdvPanel16: TAdvPanel;
    Label17: TLabel;
    Label8: TLabel;
    dtpAwalHtgDPCust: TDateTimePicker;
    dtpAkhirHtgDPCust: TDateTimePicker;
    AdvPanel17: TAdvPanel;
    txtCariHtgDPCust: TAdvEdit;
    btnCariHtgDPCust: TAdvGlowButton;
    btnLoadHtgDPCust: TAdvGlowButton;
    btnResetHtgDPCust: TAdvGlowButton;
    Panel16: TPanel;
    GaugeHtgDPCust: TGauge;
    btnFilterHtgDPCust: TAdvGlowButton;
    btnEksporHtgDPCust: TAdvGlowButton;
    btnCetakHtgDPCust: TAdvGlowButton;
    asgRekapHtgDPCust: TAdvStringGrid;
    chbSFHtgDPCust: TAdvOfficeCheckBox;
    btnDetailHtgDPCust: TAdvGlowButton;
    chbTHtgDPCust: TAdvOfficeCheckBox;
    pnlFilterHtgSupir: TPanel;
    btnResetHtgSupir: TAdvGlowButton;
    btnLoadHtgSupir: TAdvGlowButton;
    AdvPanel4: TAdvPanel;
    txtCariHtgSupir: TAdvEdit;
    btnCariHtgSupir: TAdvGlowButton;
    AdvPanel5: TAdvPanel;
    Label2: TLabel;
    Label7: TLabel;
    dtpAwalHtgSupir: TDateTimePicker;
    dtpAkhirHtgSupir: TDateTimePicker;
    Panel2: TPanel;
    GaugeHtgSupir: TGauge;
    btnFilterHtgSupir: TAdvGlowButton;
    btnEksporHtgSupir: TAdvGlowButton;
    btnCetakHtgSupir: TAdvGlowButton;
    asgRekapHtgSupir: TAdvStringGrid;
    chbSFHtgSupir: TAdvOfficeCheckBox;
    btnDetailHtgSupir: TAdvGlowButton;
    chbTHtgSupir: TAdvOfficeCheckBox;
    btnSinkronSupir: TAdvGlowButton;
    btnSinkronSupp: TAdvGlowButton;
    btnSinkronRelasi: TAdvGlowButton;
    btnSinkronAsset: TAdvGlowButton;
    btnSinkronDpPariwisata: TAdvGlowButton;
    procedure asgRekapHtgSuppDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapHtgSupirDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapHtgPihak3DblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapHtgAssetSuppDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapHtgDPCustDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapHtgDPCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgAssetSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapHtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgAssetSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgDPCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapHtgDPCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgDPCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgAssetSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgAssetSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapHtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnCariSupplierPihak3Click(Sender: TObject);
    procedure btnCariHtgDPCustClick(Sender: TObject);
    procedure btnCariHtgSupirClick(Sender: TObject);
    procedure btnCariHtgSuppClick(Sender: TObject);
    procedure btnCariHtgAssetSuppClick(Sender: TObject);
    procedure btnFilterHtgPihak3Click(Sender: TObject);
    procedure btnEksporHtgPihak3Click(Sender: TObject);
    procedure btnCetakHtgPihak3Click(Sender: TObject);
    procedure btnDetailHtgPihak3Click(Sender: TObject);
    procedure btnFilterHtgAssetSuppClick(Sender: TObject);
    procedure btnFilterHtgDPCustClick(Sender: TObject);
    procedure btnFilterHtgSupirClick(Sender: TObject);
    procedure btnFilterHtgSuppClick(Sender: TObject);
    procedure btnEksporHtgSuppClick(Sender: TObject);
    procedure btnEksporHtgSupirClick(Sender: TObject);
    procedure btnEksporHtgAssetSuppClick(Sender: TObject);
    procedure btnEksporHtgDPCustClick(Sender: TObject);
    procedure btnCetakHtgAssetSuppClick(Sender: TObject);
    procedure btnCetakHtgDPCustClick(Sender: TObject);
    procedure btnCetakHtgSupirClick(Sender: TObject);
    procedure btnCetakHtgSuppClick(Sender: TObject);
    procedure btnResetHtgSuppClick(Sender: TObject);
    procedure btnResetHtgSupirClick(Sender: TObject);
    procedure btnResetHtgPihak3Click(Sender: TObject);
    procedure btnResetHtgAssetSuppClick(Sender: TObject);
    procedure btnResetHtgDPCustClick(Sender: TObject);
    procedure btnLoadHtgSuppClick(Sender: TObject);
    procedure btnLoadHtgSupirClick(Sender: TObject);
    procedure btnLoadHtgPihak3Click(Sender: TObject);
    procedure btnLoadHtgAssetSuppClick(Sender: TObject);
    procedure btnLoadHtgDPCustClick(Sender: TObject);
    procedure btnDetailHtgSuppClick(Sender: TObject);
    procedure btnDetailHtgSupirClick(Sender: TObject);
    procedure btnDetailHtgAssetSuppClick(Sender: TObject);
    procedure btnDetailHtgDPCustClick(Sender: TObject);
    procedure chbSFHtgSuppClick(Sender: TObject);
    procedure chbSFHtgDPCustClick(Sender: TObject);
    procedure chbSFHtgSupirClick(Sender: TObject);
    procedure chbSFHtgPihak3Click(Sender: TObject);
    procedure btnSinkronSupirClick(Sender: TObject);
    procedure btnSinkronSuppClick(Sender: TObject);
    procedure btnSinkronDpPariwisataClick(Sender: TObject);
    procedure btnSinkronRelasiClick(Sender: TObject);
    procedure btnSinkronAssetClick(Sender: TObject);
  private
    { Private declarations }
    DateNow : TDate;
    AIdMenu : integer;
    TipePihak3 : String;
    SeqSupp, SeqSupir, SeqPihak3, SeqAsset, SeqDPCust : integer;
    procedure InitForm(idx : integer);
    procedure LoadData(AIndex : integer);
    procedure SetXceling(AIndex: Integer);
    procedure SetPrinting(AIndex: Integer);
    procedure SetGrid(AGrid : TAdvStringGrid);
    procedure ArrangeColSize(AGrid : TAdvStringGrid);
  public
    { Public declarations }
    procedure Execute(AMenuId : integer);
  end;

var
  frmRekapHutang: TfrmRekapHutang;

implementation

uses MainMenu, LOV, LOVRelasi, KartuHutang, Sinkronisasi, UFinance;

{$R *.dfm}

const
  ColNo         = 0;
  ColKode       = 1;
  ColNama       = 2;
  ColAwal       = 3;
  ColBeli       = 4;
  ColBiaya      = 5;
  ColLunas      = 6;
  ColPendapatan = 7;
  ColAkhir      = 8;
  ColRetur      = 9;
  ColTotal      = 10;
  ColSeq        = 11;

  Msg = 'Apakah sudah yakin?';

procedure TfrmRekapHutang.ArrangeColSize(AGrid: TAdvStringGrid);
begin
  AGrid.ClearRows(AGrid.RowCount-1,1);
  AGrid.AutoNumberCol(ColNo);
  AGrid.Cells[ColNama, AGrid.RowCount-1]     := 'Total : ';
  AGrid.FloatFormat := '%.2n';
  AGrid.FloatingFooter.ColumnCalc[ColAwal]       := acSum;
  AGrid.FloatingFooter.ColumnCalc[COlAkhir]      := acSum;
  AGrid.FloatingFooter.ColumnCalc[ColBeli]       := acSum;
  AGrid.FloatingFooter.ColumnCalc[ColBiaya]      := acSum;
  AGrid.FloatingFooter.ColumnCalc[ColLunas]      := acSum;
  AGrid.FloatingFooter.ColumnCalc[colPendapatan] := acSum;
  AGrid.FloatingFooter.ColumnCalc[colRetur]      := acSum;
  AGrid.FloatingFooter.ColumnCalc[ColTotal]      := acSum;
  AGrid.AutoSizeColumns(True, 10);
  if agrid <> asgRekapHtgSupp then begin
    AGrid.ColWidths[ColRetur] := 0;
    AGrid.ColWidths[ColTotal] := 0;
  end;
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    AGrid.ColWidths[ColSeq] := 0;
  end;
end;

procedure TfrmRekapHutang.asgRekapHtgAssetSuppDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapHtgAssetSupp.rowcount-1) then btnDetailHtgAssetSupp.Click;
end;

procedure TfrmRekapHutang.asgRekapHtgAssetSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapHutang.asgRekapHtgAssetSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n'
  end else isFloat := False;
end;

procedure TfrmRekapHutang.asgRekapHtgAssetSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgAssetSupp);
end;

procedure TfrmRekapHutang.asgRekapHtgAssetSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgAssetSupp);
end;

procedure TfrmRekapHutang.asgRekapHtgDPCustDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapHtgDPCust.rowcount-1) then btnDetailHtgDPCust.Click;
end;

procedure TfrmRekapHutang.asgRekapHtgDPCustGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapHutang.asgRekapHtgDPCustGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapHutang.asgRekapHtgDPCustMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmRekapHutang.asgRekapHtgDPCustMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgDPCust);
end;

procedure TfrmRekapHutang.asgRekapHtgPihak3DblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapHtgPihak3.rowcount-1) then btnDetailHtgPihak3.Click;
end;

procedure TfrmRekapHutang.asgRekapHtgPihak3GetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapHutang.asgRekapHtgPihak3GetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapHutang.asgRekapHtgPihak3MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmRekapHutang.asgRekapHtgPihak3MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgPihak3);
end;

procedure TfrmRekapHutang.asgRekapHtgSupirDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapHtgSupir.rowcount-1) then btnDetailHtgSupir.Click;
end;

procedure TfrmRekapHutang.asgRekapHtgSupirGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapHutang.asgRekapHtgSupirGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapHutang.asgRekapHtgSupirMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmRekapHutang.asgRekapHtgSupirMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupir);
end;

procedure TfrmRekapHutang.asgRekapHtgSuppDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  if (ARow > 0) and (ARow <> asgRekapHtgSupp.rowcount-1) then btnDetailHtgSupp.Click;
end;

procedure TfrmRekapHutang.asgRekapHtgSuppGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur, ColTotal] then
  HAlign := taRightJustify; VAlign := vtaCenter;
end;

procedure TfrmRekapHutang.asgRekapHtgSuppGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColNo, ColAwal, ColBeli, ColBiaya, ColLunas, colPendapatan, ColAkhir, ColRetur, ColTotal] then begin
    isFloat := True;
    if Acol in [ColNo] then FloatFormat := '%.0n'
    else if ACol in [ColSeq] then FloatFormat := '%.0f'
    else FloatFormat := '%.2n';
  end else isFloat := False;
end;

procedure TfrmRekapHutang.asgRekapHtgSuppMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmRekapHutang.asgRekapHtgSuppMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize(asgRekapHtgSupp);
end;

procedure TfrmRekapHutang.btnCariHtgAssetSuppClick(Sender: TObject);
begin
  SeqAsset       := 0;
  txtCariHtgAssetSupp.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqAsset        := frmLov.ExecuteSupplier('',boNone);
  if SeqAsset <> 0 then begin
    txtCariHtgAssetSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqAsset)],'#',2);
  end;
end;

procedure TfrmRekapHutang.btnCariHtgDPCustClick(Sender: TObject);
begin
  SeqDPCust      := 0;
  txtCariHtgDPCust.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqDPCust      := frmLOV.ExecuteCustomer('',BoNone);
  if SeqDPCust <> 0 then begin
    txtCariHtgDPCust.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqDPCust)],'#',2);
  end;
end;

procedure TfrmRekapHutang.btnCariHtgSupirClick(Sender: TObject);
begin
  SeqSupir       := 0;
  txtCariHtgSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupir       := frmLov.ExecuteSupir('',boNone);
  if SeqSupir <> 0 then begin
    txtCariHtgSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(SeqSupir)],'#',2);
  end;
end;

procedure TfrmRekapHutang.btnCariHtgSuppClick(Sender: TObject);
begin
  SeqSupp        := 0;
  txtCariHtgSupp.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqSupp        := frmLov.ExecuteSupplier('',boNone);
  if SeqSupp <> 0 then begin
    txtCariHtgSupp.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqSupp)],'#',2);
  end;
end;

procedure TfrmRekapHutang.btnCariSupplierPihak3Click(Sender: TObject);
var vTemp : string;
begin
  Application.CreateForm(TfrmLovRelasi, frmLovRelasi);
  frmLovRelasi.OnClose := frmMainMenu.ChildFormSingle;
  vTemp := frmLovRelasi.ExecuteGabungan('',false,true);
  if vTemp <> '' then begin
    TipePihak3 := EkstrakString(vTemp, '#', 1);
    SeqPihak3  := StrToIntDef(EkstrakString(vTemp, '#', 2),0);
    if TipePihak3 = UConst.VC_SUPPLIER then begin
      txtCariHtgPihak3.Text := EkstrakString(ListSupplierGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end else begin
      txtCariHtgPihak3.Text := EkstrakString(ListCustomerGlobal.Values[IntToStr(SeqPihak3)],'#',2);
    end;
  end else begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    txtCariHtgPihak3.Text := '';
  end;
end;

procedure TfrmRekapHutang.btnCetakHtgAssetSuppClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnCetakHtgDPCustClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnCetakHtgPihak3Click(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnCetakHtgSupirClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnCetakHtgSuppClick(Sender: TObject);
begin
  if not BisaPrint(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetPrinting(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnDetailHtgAssetSuppClick(Sender: TObject);
begin
  if (asgRekapHtgAssetSupp.Cells[ColSeq, asgRekapHtgAssetSupp.Row] <> '') or (asgRekapHtgAssetSupp.Ints[ColSeq, asgRekapHtgAssetSupp.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuHutang, frmKartuHutang);
    frmKartuHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuHutang.Execute(506, asgRekapHtgAssetSupp.Ints[ColSeq, asgRekapHtgAssetSupp.Row],AdvOfficePager1.ActivePageIndex, dtpAwalHtgAssetSupp.Date, dtpAkhirHtgAssetSupp.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadHtgAssetSupp.SetFocus;
  end;
end;

procedure TfrmRekapHutang.btnDetailHtgDPCustClick(Sender: TObject);
begin
  if (asgRekapHtgDPCust.Cells[ColSeq, asgRekapHtgDPCust.Row] <> '') or (asgRekapHtgDPCust.Ints[ColSeq, asgRekapHtgDPCust.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuHutang, frmKartuHutang);
    frmKartuHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuHutang.Execute(506, asgRekapHtgDPCust.Ints[ColSeq, asgRekapHtgDPCust.Row],AdvOfficePager1.ActivePageIndex, dtpAwalHtgDPCust.Date, dtpAkhirHtgDPCust.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadHtgDPCust.SetFocus;
  end;
end;

procedure TfrmRekapHutang.btnDetailHtgPihak3Click(Sender: TObject);
begin
  if (asgRekapHtgPihak3.Cells[ColSeq, asgRekapHtgPihak3.Row] <> '') or (asgRekapHtgPihak3.Ints[ColSeq, asgRekapHtgPihak3.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuHutang, frmKartuHutang);
    frmKartuHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuHutang.Execute(506, StrToInt(EkstrakString(asgRekapHtgPihak3.Cells[ColSeq, asgRekapHtgPihak3.Row],'#',1)),AdvOfficePager1.ActivePageIndex, dtpAwalHtgPihak3.Date, dtpAkhirHtgPihak3.Date, EkstrakString(asgRekapHtgPihak3.Cells[ColSeq, asgRekapHtgPihak3.Row],'#',2));
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadHtgPihak3.SetFocus;
  end;
end;

procedure TfrmRekapHutang.btnDetailHtgSupirClick(Sender: TObject);
begin
  if (asgRekapHtgSupir.Cells[ColSeq, asgRekapHtgSupir.Row] <> '') or (asgRekapHtgSupir.Ints[ColSeq, asgRekapHtgSupir.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuHutang, frmKartuHutang);
    frmKartuHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuHutang.Execute(506, asgRekapHtgSupir.Ints[ColSeq, asgRekapHtgSupir.Row],AdvOfficePager1.ActivePageIndex, dtpAwalHtgSupir.Date, dtpAkhirHtgSupir.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadHtgSupir.SetFocus;
  end;
end;

procedure TfrmRekapHutang.btnDetailHtgSuppClick(Sender: TObject);
begin
  if (asgRekapHtgSupp.Cells[ColSeq, asgRekapHtgSupp.Row] <> '') or (asgRekapHtgSupp.Ints[ColSeq, asgRekapHtgSupp.Row] <> 0) then begin
    Application.CreateForm(TfrmKartuHutang, frmKartuHutang);
    frmKartuHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmKartuHutang.Execute(506, asgRekapHtgSupp.Ints[ColSeq, asgRekapHtgSupp.Row],AdvOfficePager1.ActivePageIndex, dtpAwalHtgSupp.Date, dtpAkhirHtgSupp.Date);
  end else begin
    InForm('Silahkan load data dulu'+#13+'kemudian pilih data dari tabel di bawah.');
    btnLoadHtgSupp.SetFocus;
  end;
end;

procedure TfrmRekapHutang.btnEksporHtgAssetSuppClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnEksporHtgDPCustClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnEksporHtgPihak3Click(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnEksporHtgSupirClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnEksporHtgSuppClick(Sender: TObject);
begin
  if not BisaEkspor(AIdMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetXceling(AdvOfficePager1.ActivePageIndex);
end;

procedure TfrmRekapHutang.btnFilterHtgAssetSuppClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgAssetSupp, btnFilterHtgAssetSupp, 92);
end;

procedure TfrmRekapHutang.btnFilterHtgDPCustClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgDPCust, btnFilterHtgDPCust, 92);
end;

procedure TfrmRekapHutang.btnFilterHtgSupirClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgSupir, btnFilterHtgSupir, 92);
end;

procedure TfrmRekapHutang.btnFilterHtgSuppClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgSupp, btnFilterHtgSupp, 92);
end;

procedure TfrmRekapHutang.btnLoadHtgAssetSuppClick(Sender: TObject);
begin
  if dtpAkhirHtgAssetSupp.Date >= dtpAwalHtgAssetSupp.Date then begin
    LoadData(3);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirHtgAssetSupp.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapHutang.btnLoadHtgDPCustClick(Sender: TObject);
begin
  if dtpAkhirHtgDPCust.Date >= dtpAwalHtgDPCust.Date then begin
    LoadData(4);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirHtgDPCust.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapHutang.btnLoadHtgPihak3Click(Sender: TObject);
begin
  if dtpAkhirHtgPihak3.Date >= dtpAwalHtgPihak3.Date then begin
    LoadData(2);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirHtgPihak3.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapHutang.btnLoadHtgSupirClick(Sender: TObject);
begin
  if dtpAkhirHtgSupir.Date >= dtpAwalHtgSupir.Date then begin
    LoadData(1);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirHtgSupir.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapHutang.btnLoadHtgSuppClick(Sender: TObject);
begin
  if dtpAkhirHtgSupp.Date >= dtpAwalHtgSupp.Date then begin
    LoadData(0);
  end else begin
    InForm('Periode akhir harus >= periode awal.');
    dtpAkhirHtgSupp.SetFocus;
    Exit;
  end;
end;

procedure TfrmRekapHutang.btnResetHtgAssetSuppClick(Sender: TObject);
begin
  InitForm(4);
  SetGrid(asgRekapHtgAssetSupp);
end;

procedure TfrmRekapHutang.btnResetHtgDPCustClick(Sender: TObject);
begin
  InitForm(5);
  SetGrid(asgRekapHtgDPCust);
end;

procedure TfrmRekapHutang.btnResetHtgPihak3Click(Sender: TObject);
begin
  InitForm(3);
  SetGrid(asgRekapHtgPihak3);
end;

procedure TfrmRekapHutang.btnResetHtgSupirClick(Sender: TObject);
begin
  InitForm(2);
  SetGrid(asgRekapHtgSupir);
end;

procedure TfrmRekapHutang.btnResetHtgSuppClick(Sender: TObject);
begin
  InitForm(1);
  SetGrid(asgRekapHtgSupp);
end;

procedure TfrmRekapHutang.btnSinkronAssetClick(Sender: TObject);
var aseq : integer;
begin
  aSeq := 0;
  if (asgRekapHtgAssetSupp.Cells[ColSeq, asgRekapHtgAssetSupp.Row] <> '') and (asgRekapHtgAssetSupp.Row > 0) then begin
    aSeq := asgRekapHtgAssetSupp.Ints[ColSeq, asgRekapHtgAssetSupp.Row];
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.EXecute4HtgAsset(StartOfTheMonth(dtpAwalHtgAssetSupp.Date)-1, aSeq);
end;

procedure TfrmRekapHutang.btnSinkronDpPariwisataClick(Sender: TObject);
var aseq : integer;
begin
  aSeq := 0;
  if (asgRekapHtgDPCust.Cells[ColSeq, asgRekapHtgDPCust.Row] <> '') and (asgRekapHtgDPCust.Row > 0) then begin
    aSeq := asgRekapHtgDPCust.Ints[ColSeq, asgRekapHtgDPCust.Row];
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.EXecute4HtgDPPariwisata(StartOfTheMonth(dtpAwalHtgDPCust.Date)-1, aSeq);
end;

procedure TfrmRekapHutang.btnSinkronRelasiClick(Sender: TObject);
var aseq : integer;
    Tipe : String;
begin
  aSeq := 0;
  Tipe := '';
  if (asgRekapHtgPihak3.Cells[ColSeq, asgRekapHtgPihak3.Row] <> '') and (asgRekapHtgPihak3.Row > 0) then begin
    aSeq := StrToIntDef(Ekstrakstring(asgRekapHtgPihak3.cells[ColSeq,asgRekapHtgPihak3.Row],'#',1),0);
    Tipe := Ekstrakstring(asgRekapHtgPihak3.cells[ColSeq,asgRekapHtgPihak3.Row],'#',2);
  end;
  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasi.EXecute4HtgRelasi(StartOfTheMonth(dtpAwalHtgPihak3.Date)-1, aSeq,Tipe);
end;


procedure TfrmRekapHutang.btnSinkronSupirClick(Sender: TObject);
var aseq : integer;
begin
//  aSeq := 0;
//  if (asgRekapHtgSupir.Cells[ColSeq, asgRekapHtgSupir.Row] <> '') and (asgRekapHtgSupir.Row > 0) then begin
//    aSeq := asgRekapHtgSupir.Ints[ColSeq, asgRekapHtgSupir.Row];
//  end;
//  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
//  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
//  frmSinkronisasi.EXecute4HtgSupir(StartOfTheMonth(dtpAwalHtgSupir.Date)-1, aSeq);

  try
//    myConnection.BeginSQL;
    Sinkronisasi_Saldo_Hutang_Lain(GaugeHtgSupir, SeqSupir, StartOfTheMonth(dtpAwalHtgSupir.Date), SALDO_HUTANG_JAMINAN_SUPIR, False, False, False, VC_SUPIR, True);
//    myConnection.EndSQL;
  except
//    myConnection.UndoSQL;
  end;
end;

procedure TfrmRekapHutang.btnSinkronSuppClick(Sender: TObject);
var aseq : integer;
begin  
//  aSeq := 0;
//  if (asgRekapHtgSupp.Cells[ColSeq, asgRekapHtgSupp.Row] <> '') and (asgRekapHtgSupp.Row > 0) then begin
//    aSeq := asgRekapHtgSupp.Ints[ColSeq, asgRekapHtgSupp.Row];
//  end;
//  Application.CreateForm(TfrmSinkronisasi, frmSinkronisasi);
//  frmSinkronisasi.OnClose := frmMainMenu.ChildFormSingle;
//  frmSinkronisasi.EXecute4HtgSupplier(StartOfTheMonth(dtpAwalHtgSupp.Date)-1, aSeq);
  GaugeHtgSupp.Show;
  if Sinkronisasi_Saldo_Hutang(GaugeHtgSupp, SeqSupp, StartOfTheMonth(dtpAwalHtgSupp.Date), SALDO_HUTANG_SUPP, true) then begin
    Inform('Sinkron data berhasil.');
  end else begin
    Inform('Sinkron data gagal.');
  end;
  GaugeHtgSupp.Hide
end;

procedure TfrmRekapHutang.chbSFHtgDPCustClick(Sender: TObject);
begin
  asgRekapHtgDPCust.SearchFooter.Visible := chbSFHtgDPCust.Checked;
end;

procedure TfrmRekapHutang.chbSFHtgPihak3Click(Sender: TObject);
begin
  asgRekapHtgPihak3.SearchFooter.Visible := chbSFHtgPihak3.Checked;
end;

procedure TfrmRekapHutang.chbSFHtgSupirClick(Sender: TObject);
begin
  asgRekapHtgSupir.SearchFooter.Visible := chbSFHtgSupir.Checked;
end;

procedure TfrmRekapHutang.chbSFHtgSuppClick(Sender: TObject);
begin
  asgRekapHtgSupp.SearchFooter.Visible := chbSFHtgSupp.Checked;
end;

procedure TfrmRekapHutang.btnFilterHtgPihak3Click(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilterHtgPihak3, btnFilterHtgPihak3, 92);
end;

procedure TfrmRekapHutang.Execute(AMenuId: integer);
begin
  AIdMenu := AMenuId;
  if (not BisaLihatLaporan(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  
  DateNow := ServerNow;
  SetGrid(asgRekapHtgSupp);
  SetGrid(asgRekapHtgSupir);
  SetGrid(asgRekapHtgPihak3);
  SetGrid(asgRekapHtgAssetSupp);
  SetGrid(asgRekapHtgDPCust);
  AdvOfficePager1.ActivePageIndex := 0;  
  InitForm(0);

  Run(Self);
end;

procedure TfrmRekapHutang.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuID);
end;

procedure TfrmRekapHutang.InitForm(idx: integer);
begin
  if (idx = 0) or (idx = 1) then begin
    SeqSupp := 0;
    SetGrid(asgRekapHtgSupp);
    txtCariHtgSupp.Text       := '';
    chbTHtgSupp.Checked       := False;
    chbSFHtgSupp.Checked      := False;
    dtpAwalHtgSupp.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgSupp.Date      := DateNow;
    asgRekapHtgSupp.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgSupp);
  end;
  if (idx = 0) or (idx = 2) then begin
    SeqSupir := 0;
    SetGrid(asgRekapHtgSupir);
    txtCariHtgSupir.Text        := '';
    chbTHtgSupir.Checked        := False;
    chbSFHtgSupir.Checked       := False;
    dtpAwalHtgSupir.Date        := StartOfTheMonth(DateNow);
    dtpAkhirHtgSupir.Date       := DateNow;
    asgRekapHtgSupir.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgSupir);
  end;
  if (idx = 0) or (idx = 3) then begin
    SeqPihak3  := 0;
    TipePihak3 := '';
    SetGrid(asgRekapHtgPihak3);
    txtCariHtgPihak3.Text       := '';
    chbTHtgPihak3.Checked       := False;
    chbSFHtgPihak3.Checked      := False;
    dtpAwalHtgPihak3.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgPihak3.Date      := DateNow;
    asgRekapHtgPihak3.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgPihak3);
  end;
  if (idx = 0) or (idx = 4) then begin
    SeqAsset := 0;
    SetGrid(asgRekapHtgAssetSupp);
    txtCariHtgAssetSupp.Text    := '';
    chbTHtgAssetSupp.Checked    := False;
    chbSFHtgAssetSupp.Checked   := False;
    dtpAwalHtgAssetSupp.Date    := StartOfTheMonth(DateNow);
    dtpAkhirHtgAssetSupp.Date   := DateNow;
    asgRekapHtgAssetSupp.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgAssetSupp);
  end;
  if (idx = 0) or (idx = 5) then begin
    SeqDPCust := 0;
    SetGrid(asgRekapHtgDPCust);
    txtCariHtgDPCust.Text       := '';
    chbTHtgDPCust.Checked       := False;
    chbSFHtgDPCust.Checked      := False;
    dtpAwalHtgDPCust.Date       := StartOfTheMonth(DateNow);
    dtpAkhirHtgDPCust.Date      := DateNow;
    asgRekapHtgDPCust.SearchFooter.Visible := False;
    ArrangeColSize(asgRekapHtgDPCust);
  end;
end;

procedure TfrmRekapHutang.LoadData(AIndex: integer);
begin
  case AIndex of
    0 : begin
        asgRekapHtgSupp.ClearNormalCells;
        asgRekapHtgSupp.RowCount := 3;
        asgRekapHtgSupp.ClearRows(asgRekapHtgSupp.RowCount-1, 1);

        UReport.LoadRekapHutang(asgRekapHtgSupp, GaugeHtgSupp, dtpAwalHtgSupp.date, dtpAkhirHtgSupp.Date, SeqSupp,
                  SALDO_HUTANG_SUPP, chbTHtgSupp.checked);
        ArrangeColSize(asgRekapHtgSupp);
    end;
    1 : begin
        asgRekapHtgSupir.ClearNormalCells;
        asgRekapHtgSupir.RowCount := 3;
        asgRekapHtgSupir.ClearRows(asgRekapHtgSupir.RowCount-1, 1);

        UReport.LoadRekapHutang(asgRekapHtgSupir, GaugeHtgSupir, dtpAwalHtgSupir.date, dtpAkhirHtgSupir.Date, SeqSupir,
                  SALDO_HUTANG_JAMINAN_SUPIR, chbTHtgSupir.checked);
        ArrangeColSize(asgRekapHtgSupir);
    end;
    2 : begin
        asgRekapHtgPihak3.ClearNormalCells;
        asgRekapHtgPihak3.RowCount := 3;
        asgRekapHtgPihak3.ClearRows(asgRekapHtgPihak3.RowCount-1, 1);

        if TipePihak3 = VC_SUPPLIER then begin
          UReport.LoadRekapHutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  SALDO_HUTANG_PINJAMAN_RELASI_SUPP, chbTHtgPihak3.checked);
        end else if TipePihak3 = VC_CUSTOMER then begin
          UReport.LoadRekapHutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  SALDO_HUTANG_PINJAMAN_RELASI_CUST, chbTHtgPihak3.checked);
        end else begin
          UReport.LoadRekapHutang(asgRekapHtgPihak3, GaugeHtgPihak3, dtpAwalHtgPihak3.date, dtpAkhirHtgPihak3.Date, SeqPihak3,
                  FormatSQLString(SALDO_HUTANG_PINJAMAN_RELASI_SUPP)+', '+FormatSQLString(SALDO_HUTANG_PINJAMAN_RELASI_CUST), chbTHtgPihak3.checked);
        end;
        ArrangeColSize(asgRekapHtgPihak3);
    end;
    3 : begin
        asgRekapHtgAssetSupp.ClearNormalCells;
        asgRekapHtgAssetSupp.RowCount := 3;
        asgRekapHtgAssetSupp.ClearRows(asgRekapHtgAssetSupp.RowCount-1, 1);

        UReport.LoadRekapHutang(asgRekapHtgAssetSupp, GaugeHtgAssetSupp, dtpAwalHtgAssetSupp.date, dtpAkhirHtgAssetSupp.Date, SeqAsset,
                  SALDO_HUTANG_ASSET_SUPP, chbTHtgAssetSupp.checked);
        ArrangeColSize(asgRekapHtgAssetSupp);
    end;
    4 : begin
        asgRekapHtgDPCust.ClearNormalCells;
        asgRekapHtgDPCust.RowCount := 3;
        asgRekapHtgDPCust.ClearRows(asgRekapHtgDPCust.RowCount-1, 1);

        UReport.LoadRekapHutang(asgRekapHtgDPCust, GaugeHtgDPCust, dtpAwalHtgDPCust.date, dtpAkhirHtgDPCust.Date, SeqDPCust,
                  SALDO_HUTANG_DP_PARIWISATA, chbTHtgDPCust.Checked);
        ArrangeColSize(asgRekapHtgDPCust);
    end;
  end;
end;

procedure TfrmRekapHutang.SetGrid(AGrid: TAdvStringGrid);
begin
  AGrid.ClearNormalCells;
  asgResetGrid(AGrid,3,13,1,1);
  ArrangeColSize(AGrid);
end;

procedure TfrmRekapHutang.SetPrinting(AIndex: Integer);
  procedure PreparePrint(AGrid: TAdvStringGrid);
  begin
    myConnection.advPrint.Grid:= AGrid;
    AGrid.PrintSettings.TitleLines.Clear;
    AGrid.PrintSettings.TitleLines.Add('');
    if AGrid = asgRekapHtgSupp then AGrid.PrintSettings.TitleLines.Add('Rekap Hutang Supplier')
    else if AGrid = asgRekapHtgSupir then AGrid.PrintSettings.TitleLines.Add('Rekap Hutang Supir')
    else if AGrid = asgRekapHtgPihak3 then AGrid.PrintSettings.TitleLines.Add('Rekap Hutang Relasi')
    else if AGrid = asgRekapHtgAssetSupp then AGrid.PrintSettings.TitleLines.Add('Rekap Hutang Asset')
    else if AGrid = asgRekapHtgDPCust then AGrid.PrintSettings.TitleLines.Add('Rekap Hutang DP Pariwisata');
    AGrid.PrintSettings.TitleLines.Add('');
    AGrid.ColumnSize.Stretch := False;
    AGrid.HideColumn(ColSeq);
    if agrid <> asgRekapHtgSupp then AGrid.ColCount := 9
    else AGrid.ColCount := 10;
    SetingPrint(AGrid);
    myConnection.advPrint.Execute;
    agrid.ColCount := 11;
    AGrid.UnHideColumn(ColSeq);
    AGrid.ColumnSize.Stretch := True;
    ArrangeColSize(AGrid);
  end;
begin
  case AIndex of
    0 : PreparePrint(asgRekapHtgSupp);
    1 : PreparePrint(asgRekapHtgSupir);
    2 : PreparePrint(asgRekapHtgPihak3);
    3 : PreparePrint(asgRekapHtgAssetSupp);
    4 : PreparePrint(asgRekapHtgDPCust);
  end;
end;

procedure TfrmRekapHutang.SetXceling(AIndex: Integer);
begin
  case AIndex of
    0 : asgExportToExcell(asgRekapHtgSupp, myConnection.SaveToExcell);
    1 : asgExportToExcell(asgRekapHtgSupir, myConnection.SaveToExcell);
    2 : asgExportToExcell(asgRekapHtgPihak3, myConnection.SaveToExcell);
    3 : asgExportToExcell(asgRekapHtgAssetSupp, myConnection.SaveToExcell);
    4 : asgExportToExcell(asgRekapHtgDPCust, myConnection.SaveToExcell);
  end;
end;

end.
