unit LapUangJalanUangKomisiSupir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, Gauges,
  AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, StdCtrls, Menus, AdvOfficePager,
  AdvEdit;

type
  TfrmLapUangJalanUangKomisiSupir = class(TForm)
    MainPanel: TPanel;
    pnlfilter: TPanel;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    Panel2: TPanel;
    Gauge: TGauge;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    btnFilter: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label4: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label1: TLabel;
    pmImport: TPopupMenu;
    Import1: TMenuItem;
    Import2: TMenuItem;
    AdvPanel1: TAdvPanel;
    txtArmada: TAdvEdit;
    btnlovarmada: TAdvGlowButton;
    AofUangSupir: TAdvOfficePager;
    pgData: TAdvOfficePage;
    asgRekap: TAdvStringGrid;
    pgLaporan: TAdvOfficePage;
    asgLap: TAdvStringGrid;
    txtSupir: TAdvEdit;
    btnLovSupir: TAdvGlowButton;
    Label2: TLabel;
    Label3: TLabel;
    AdvPanel3: TAdvPanel;
    AdvEdit2: TAdvEdit;
    AdvGlowButton3: TAdvGlowButton;
    AdvGlowButton4: TAdvGlowButton;
    cmbStatus: TComboBox;
    Label5: TLabel;
    Shape2: TShape;
    Shape1: TShape;
    Label6: TLabel;
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chbSFClick(Sender: TObject);
    procedure asgLapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgLapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgLapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure AofUangSupirChange(Sender: TObject);
    procedure btnlovarmadaClick(Sender: TObject);
    procedure btnLovSupirClick(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgLapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgLapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgLapGetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure asgRekapGetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
  private
    { Private declarations }
    listkotajurusan : tstringlist;
    AMenuid, SeqArmada, seqSupir : integer;
    procedure InitForm;
    procedure SetGrid(idx : integer);
    procedure ArrangeColSize(idx : integer);
    procedure LoadData;
  public
    { Public declarations }
    procedure execute(AidMenu:integer);
  end;

var
  frmLapUangJalanUangKomisiSupir: TfrmLapUangJalanUangKomisiSupir;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, MainMenu, OracleConnection, ImportDataTerminal,
  DateUtils, UConst_Temp, ADOInt, StrUtils, ImportDataTerminalWB, LOV, 
  UReport_temp;

const
	ColNo      = 0;
  ColNoOrder = 1;
  ColOs      = 2;
  ColTgl     = 3;
  colBody    = 4;
  colSupir   = 5;
  colTujuan  = 6;
  colJumlah  = 7;
  colNoLain  = 8;
  colOrder   = 9;

{$R *.dfm}

procedure TfrmLapUangJalanUangKomisiSupir.AofUangSupirChange(Sender: TObject);
begin
  chbSF.Checked := False;
  asgRekap.SearchFooter.Visible :=  false;
  asgLap.SearchFooter.Visible :=  false;
end;

procedure TfrmLapUangJalanUangKomisiSupir.ArrangeColSize(idx : integer);
begin
  if (idx = 0) or (idx = 1) then begin
//    asgRekap.AutoNumberCol(ColNo);
    asgRekap.Cells[colTujuan, asgRekap.RowCount-1] := 'Total';
    asgRekap.FloatFormat := '%.2n';
//    asgRekap.FloatingFooter.ColumnCalc[colJumlah] := acSUM;
    asgRekap.AutoSizeColumns(true);
    if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
      asgRekap.ColWidths[colNoLain] := 0;
      asgRekap.ColWidths[colOrder] := 0;
    end;
  end;
  if (idx = 0) or (idx = 2) then begin
    asgLap.AutoNumberCol(ColNo);
    asgLap.Cells[colTujuan, asgRekap.RowCount-1] := 'Total';
    asgLap.FloatFormat := '%.2n';
    asgLap.FloatingFooter.ColumnCalc[colJumlah] := acSUM;
    asgLap.AutoSizeColumns(true);
    if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
      asgLap.ColWidths[colNoLain] := 0;
      asgLap.ColWidths[colOrder] := 0;
    end;
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgLapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 1 then HAlign := taCenter
  else if acol in [colno, colJumlah]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgLapGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (arow > 0) and (acol > 0) then begin
    if asgLap.Ints[colNoLain, arow] <> 0 then ABrush.Color := $0095B8FF;
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgLapGetFloatFormat(Sender: TObject;
  ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := false;
  IsFloat := acol in [ColNo, colJumlah];
  if IsFloat then begin
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else FloatFormat := '%.2n';
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgLapGetFormat(Sender: TObject;
  ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if acol in [colJumlah] then AStyle := ssFinancial
  else if acol in [ColTgl] then AStyle := ssDate
  else AStyle := ssAlphabetic;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgLapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(2);
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgLapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    arrangecolsize(2);
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapClickCell(Sender: TObject;
  ARow, ACol: Integer);
begin
  if (ARow <= 0) and (ACol = 0) then begin
    asgRekap.Cells[0,0] := IfThen(asgRekap.Cells[0,0]='[-]','[+]','[-]');
    if asgRekap.Cells[0,0] = '[+]' then asgRekap.ContractAll
    else asgRekap.ExpandAll;
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 1 then HAlign := taCenter
  else if acol in [ColNo, colJumlah]then HAlign := taRightJustify
  else HAlign := taLeftJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (arow > 0) and (acol > 0) then begin
    if asgRekap.Ints[colNoLain, arow] <> 0 then ABrush.Color := $0095B8FF;
    if asgRekap.Cells[colOrder, arow] = 'T' then ABrush.Color := $0095B8FF;
    if asgRekap.Cells[colOrder, arow] = 'F' then ABrush.Color := $00BEFF7D;
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isfloat := false;
  IsFloat := acol in [ColNo, colJumlah];
  if IsFloat then begin
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else FloatFormat := '%.2n';
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapGetFormat(Sender: TObject;
  ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if acol in [colJumlah] then AStyle := ssFinancial
  else if acol in [ColTgl] then AStyle := ssDate
  else AStyle := ssAlphabetic;
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapUangJalanUangKomisiSupir.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if AofUangSupir.ActivePage = pgData then begin
    SetingPrint(asgRekap);
    myConnection.advPrint.Grid := asgRekap;
    asgRekap.PrintSettings.TitleLines.Clear;
    asgRekap.PrintSettings.TitleLines.Add('');
    asgRekap.PrintSettings.TitleLines.Add('Daftar Uang Jalan');
    asgRekap.PrintSettings.TitleLines.Add('');
    asgRekap.ColumnSize.Stretch := false ;
    asgRekap.ColCount := asgRekap.ColCount-3;
    SetingPrint(asgRekap);
    myConnection.advPrint.Execute ;
    asgRekap.ColCount := asgRekap.ColCount+3;
    asgRekap.ColumnSize.Stretch := true;
    ArrangeColSize(1);
  end else if AofUangSupir.ActivePage = pgLaporan then begin
    SetingPrint(asgLap);
    myConnection.advPrint.Grid := asgLap;
    asgLap.PrintSettings.TitleLines.Clear;
    asgLap.PrintSettings.TitleLines.Add('');
    asgLap.PrintSettings.TitleLines.Add('Daftar Uang Komisi');
    asgLap.PrintSettings.TitleLines.Add('');
    asgLap.ColumnSize.Stretch := false ;
    asgLap.ColCount := asgLap.ColCount-3;
    SetingPrint(asgLap);
    myConnection.advPrint.Execute ;
    asgLap.ColCount := asgLap.ColCount+3;
    asgLap.ColumnSize.Stretch := true;
    ArrangeColSize(2);
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if AofUangSupir.ActivePage = pgData then
    asgExportToExcell(asgRekap, myConnection.SaveToExcell)
  else asgExportToExcell(asgLap, myConnection.SaveToExcell);
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlfilter, btnFilter, 90);
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnLoadClick(Sender: TObject);
begin
  loaddata;
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnlovarmadaClick(Sender: TObject);
begin
  txtArmada.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada         := frmLov.ExecuteArmada('',boNone);
  if SeqArmada <> 0 then begin
    txtArmada.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',1)+' | '
    +EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnLovSupirClick(Sender: TObject);
begin
  txtSupir.Text := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  seqSupir := frmLov.ExecuteSupir('',bonone);
  if seqSupir <> 0 then begin
    txtSupir.Text := EkstrakString(ListSupirGlobal.Values[IntToStr(seqSupir)],'#',2);
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid(0);
end;

procedure TfrmLapUangJalanUangKomisiSupir.chbSFClick(Sender: TObject);
begin
  if AofUangSupir.ActivePage = pgData then
    asgRekap.SearchFooter.Visible := chbSF.Checked
  else asgLap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLapUangJalanUangKomisiSupir.execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  listkotajurusan := TStringList.Create;
  if (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
     and (not BisaNambah(AMenuId)) and (not BisaLihatRekap(AMenuId)) and (not BisaLihatLaporan(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  initform;
  Setgrid(0);
  run(self);
end;

procedure TfrmLapUangJalanUangKomisiSupir.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapUangJalanUangKomisiSupir.InitForm;
begin
  dtpAwal.date := ServerNow;
  dtpAkhir.Date := dtpAwal.date;
  chbSF.Checked := false;
  txtArmada.Clear;
  txtSupir.Clear;
  SeqArmada := 0;
  seqSupir := 0;
  cmbStatus.ItemIndex := 1;
  AofUangSupir.ActivePage := pgData;
end;

procedure TfrmLapUangJalanUangKomisiSupir.LoadData;
VAR status : tbooleanOperator;
begin
  Gauge.Show;
  status := boNone;
  if cmbStatus.ItemIndex = 0 then status := boNone
  else if cmbStatus.ItemIndex = 1 then status := boFalse
  else if cmbStatus.ItemIndex = 2 then status := boTrue;
  if AofUangSupir.ActivePage = pgData then begin
    asgRekap.ExpandAll;
    asgRekap.RowCount := 3;
    asgRekap.ClearNormalCells;
    Load_lap_uang_jalan_uang_komisi_supir(asgRekap, dtpawal.date, dtpakhir.date, gauge, seqArmada, seqSupir, status, true);
    Gauge.hide;
    ArrangeColSize(1);
//    asgRekap.Sort(ColTgl);
  end else begin
    asgLap.RowCount := 3;
    asgLap.ClearNormalCells;
    Load_lap_uang_jalan_uang_komisi_supir(asgLap, dtpawal.date, dtpakhir.date, gauge, seqArmada, seqSupir, status, false);
    Gauge.hide;
    arrangecolsize(2);
    asgLap.Sort(ColTgl);
  end;
end;

procedure TfrmLapUangJalanUangKomisiSupir.SetGrid(idx : integer);
begin
  if (idx = 0) or (idx = 1) then begin
    asgRekap.ColCount := 11;
    asgRekap.RowCount := 3;
    asgRekap.FixedCols := 1;
    asgRekap.FixedRows := 1;
    asgRekap.ClearNormalCells;
    asgRekap.RemoveNode(1);    
  end;
  if (idx = 0) or (idx = 2) then begin
    asgLap.ColCount := 11;
    asgLap.RowCount := 3;
    asgLap.FixedCols := 1;
    asgLap.FixedRows := 1;
    asgLap.ClearNormalCells;
  end;
  ArrangeColSize(idx);
end;

end.
