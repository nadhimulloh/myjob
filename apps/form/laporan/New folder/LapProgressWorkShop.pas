unit LapProgressWorkShop;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, AdvOfficeButtons, StdCtrls, AdvEdit,
  AdvCombo, AdvGlowButton, ComCtrls, AdvPanel, ExtCtrls, OracleConnection, USystemMenu,
  UGeneral, UConst, URecord, UReport, StrUtils, DateUtils, UEngine;

type
  TfrmLapProgressWorkShop = class(TForm)
    MainPanel: TPanel;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    panel: TPanel;
    btnExport: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    chbShowSF: TAdvOfficeCheckBox;
    btnEkspor: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    Label8: TLabel;
    txtMekanik: TAdvEdit;
    btnLOVPegawai: TAdvGlowButton;
    AdvPanel4: TAdvPanel;
    Label7: TLabel;
    txtNoBody: TAdvEdit;
    btnLOVArmada: TAdvGlowButton;
    Label11: TLabel;
    txtNoPolisi: TAdvEdit;
    AdvPanel2: TAdvPanel;
    Label3: TLabel;
    dtpDari: TDateTimePicker;
    Label1: TLabel;
    dtpSampai: TDateTimePicker;
    asgRekap: TAdvStringGrid;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure chbShowSFClick(Sender: TObject);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnLOVArmadaClick(Sender: TObject);
    procedure btnLOVPegawaiClick(Sender: TObject);
    procedure txtNoPolisiKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    DateNow : TDate;
    SeqPegawai, SeqArmada, IdMenu : integer;
    procedure ArrangeColSize;
    procedure Initform;
    procedure LoadData;
    procedure SetGrid;
  public
    { Public declarations }
    procedure Execute(AMenuID : integer);
  end;

var
  frmLapProgressWorkShop: TfrmLapProgressWorkShop;

implementation

uses MainMenu, LOV, LovMekanik;

{$R *.dfm}

const
  ColNo         = 0;
  ColNama       = 1;
  ColDeskripsi  = 2;
  ColTanggal    = 3;
  ColTglSelesai = 4;
  ColNoPolisi   = 5;
  ColNoBody     = 6;
  ColSeq        = 7;

procedure TfrmLapProgressWorkShop.ArrangeColSize;
begin
  asgRekap.AutoSizeColumns(True);
  if GlobalSystemUser.AccessLevel <> UConst.LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[ColSeq] := 0;
  end;
end;

procedure TfrmLapProgressWorkShop.asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if ACol in[ColNo, ColSeq] then HAlign := taRightJustify;
end;

procedure TfrmLapProgressWorkShop.asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmLapProgressWorkShop.asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmLapProgressWorkShop.btnCetakClick(Sender: TObject);
begin
  if (not BisaPrint(IdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Grid := asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Progress Work Shop');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := false;
  asgRekap.HideColumn(ColSeq);
  asgRekap.ColCount := 7;
  myConnection.advPrint.Execute;
  asgRekap.ColCount := 9;
  asgRekap.UnHideColumn(ColSeq);
  asgRekap.ColumnSize.Stretch := True;
  ArrangeColSize;
end;

procedure TfrmLapProgressWorkShop.btnEksporClick(Sender: TObject);
begin
  if  (not BisaEkspor(idMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmLapProgressWorkShop.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 87);
end;

procedure TfrmLapProgressWorkShop.btnLoadClick(Sender: TObject);
begin
  Loaddata;
end;

procedure TfrmLapProgressWorkShop.btnLOVArmadaClick(Sender: TObject);
begin
  txtNoBody.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada      := frmLov.ExecuteArmada('' , boNone, true);
  if SeqArmada <> 0 then begin
    txtNoBody.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapProgressWorkShop.btnLOVPegawaiClick(Sender: TObject);
var Mst : TR_Master_pegawai;
begin
  SeqPegawai := 0;
  txtMekanik.Text := '';
  Application.CreateForm(TfrmLovMekanik, frmLovMekanik);
  frmLovMekanik.OnClose := frmMainMenu.ChildFormSingle;
  SeqPegawai := frmLovMekanik.ExecutePegawai('');
  if SeqPegawai <> 0 then begin
    if ListGlobalPegawai.Values[IntToStr(SeqPegawai)] = '' then begin
      //jika gk ada dilist global ambil pegawai dan masukan list
      Mst := Get_Master_pegawai(SeqPegawai);
      txtMekanik.Text := Mst.nama;
      Update_List_Global_Master_Pegawai(Mst);
    end else begin
      txtMekanik.Text := EkstrakString(ListGlobalPegawai.Values[IntToStr(SeqPegawai)],'#',2);
    end;
  end;
end;

procedure TfrmLapProgressWorkShop.btnResetClick(Sender: TObject);
begin
  InitForm;
  SetGrid;
end;

procedure TfrmLapProgressWorkShop.chbShowSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbShowSF.Checked;
end;

procedure TfrmLapProgressWorkShop.Execute(AMenuID: integer);
begin
  IdMenu := AMenuID;
  if (not BisaLihatLaporan(IdMenu)) and (not BisaPrint(IdMenu)) and (not BisaEkspor(IdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  DateNow := ServerNow;
  InitForm;
  SetGrid;
  Run(Self);
end;

procedure TfrmLapProgressWorkShop.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapProgressWorkShop.Initform;
begin
  SeqPegawai         := 0;
  SeqArmada          := 0;
  dtpDari.Date       := StartOfTheMonth(DateNow);
  dtpSampai.Date     := DateNow;
  chbShowSF.Checked  := False;
  txtNoPolisi.Text   := '';
  txtNoBody.Text     := '';
  txtMekanik.Text    := '';
  asgRekap.SearchFooter.Visible := chbShowSF.Checked;
end;

procedure TfrmLapProgressWorkShop.LoadData;
begin
  asgRekap.ClearNormalCells;
  asgRekap.RowCount := 2;
  UReport.Load_Progress_WorkShop(asgRekap, dtpDari.Date, dtpSampai.Date, SeqPegawai, SeqArmada, TrimAllString(txtNoPolisi.Text));
  ArrangeColSize;
end;

procedure TfrmLapProgressWorkShop.SetGrid;
begin
  asgResetGrid(asgRekap, 2, 9, 1, 1);
  ArrangeColSize;
end;

procedure TfrmLapProgressWorkShop.txtNoPolisiKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then btnLoad.Click;
end;

end.
