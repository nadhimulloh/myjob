// + dibuat oleh christian 251017 + //
unit LapPersediaanAkhirBarang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, AdvGlowButton, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, ExtCtrls, Gauges, ComCtrls, AdvCombo, AdvEdit, AdvPanel,
   dateutils,adoint;

type
  TfrmRekapPersediaanAkhirBarang = class(TForm)               //
    Gauge: TGauge;
    shpColor1: TShape;
    Label1: TLabel;
    asgRekap: TAdvStringGrid;             //
    btnFilter: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    pnlFilter: TPanel;
    btnOk: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label7: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    txtSparepart: TAdvEdit;
    btnSparePart: TAdvGlowButton;
    txtKategori: TAdvEdit;
    btnKategori: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label2: TLabel;
    dtpTgl: TDateTimePicker;
    chbShowAll: TAdvOfficeCheckBox;
    btnUpdateHarga: TAdvGlowButton;
    cmbGudang: TComboBox;
    Label4: TLabel;
    txtMerek: TAdvEdit;
    btnmerek: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    Label5: TLabel;
    dtptglhrgrata: TDateTimePicker;
    procedure btnFilterClick(Sender: TObject);
    procedure btnKategoriClick(Sender: TObject);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnEksporClick(Sender: TObject);
    procedure btnSparePartClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFormat(Sender: TObject; ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure btnUpdateHargaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnmerekClick(Sender: TObject);
  private
    { Private declarations }
    SeqSPart, SeqKat : integer;
    Merek : string;
    AMenuID : integer;
    isNeraca : boolean;
    ListGudang : TStringList;
    procedure arrangecolsize;
    procedure setgrid;
    procedure initform;
    procedure loaddata;
  public
    { Public declarations }
    procedure Execute(AIdMenu: Integer);
    procedure Execute4Neraca(Tgl : TDate);
  end;

var
  frmRekapPersediaanAkhirBarang: TfrmRekapPersediaanAkhirBarang;

implementation
uses  UEngine, UGeneral, OracleConnection, UReport, MainMenu, 
  USystemMenu, LOV,  URecord, UConst, LovMasterSparePart, UFinance,
  SinkronisasiHargaRata;
{$R *.dfm}
const
  ColNo             = 0;
  ColKode           = 1;
  ColNama           = 2;
  colNamaDagang     = 3;
  colMerek          = 4;
  colKategori       = 5;
  ColAkhir          = 6;
  ColHarga          = 7;
  ColNilai          = 8;
  ColMinStok        = 9;
  ColSeq            = 10;

procedure TfrmRekapPersediaanAkhirBarang.btnUpdateHargaClick(Sender: TObject);
var i : integer;
begin
  Gauge.Show;
  myConnection.BeginSQL;
  for i := 1 to asgrekap.rowcount-2 do begin
    Gauge.Progress := round((i+1)/ asgrekap.rowcount * 100);
    Sinkronisasi_Harga_rata(asgRekap.Ints[ColSeq, i],dtptglhrgrata.Date);
  end;
  myConnection.EndSQL;
  Gauge.Hide;
  Inform(MSG_SUCCESS_UPDATE);
  btnOk.click;
end;

procedure TfrmRekapPersediaanAkhirBarang.arrangecolsize;
begin
  asgRekap.AutoNumberCol(ColNo);
  asgRekap.ColumnSize.StretchColumn := 0;
  asgRekap.ColumnSize.Stretch := false;
  asgRekap.UnHideColumnsAll;
  asgRekap.cells[ColNama, asgRekap.RowCount-1] := 'Total ';
  asgRekap.FloatingFooter.ColumnCalc[ColAkhir] := AcSUM;
  asgRekap.FloatingFooter.columncalc[ColNilai] := acSUM;   
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then asgRekap.HideColumn(ColSeq);
  asgRekap.AutoSizeColumns(true, 2);   
  asgRekap.ColumnSize.StretchColumn := asgRekap.ColCount-1;
  asgRekap.ColumnSize.Stretch := true;
end;

procedure TfrmRekapPersediaanAkhirBarang.asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then HAlign := taCenter
  else if acol in [ColNo, ColAkhir, ColHarga, ColNilai, ColMinStok] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmRekapPersediaanAkhirBarang.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  AFont.Color := clBlack;
  if (ARow > 0) and (ACol > 0) and (ARow < asgRekap.RowCount -1) then begin
    if asgRekap.Floats[ColAkhir,arow] < asgRekap.Floats[ColMinStok,arow]  then begin
      AFont.Color := shpColor1.Brush.Color;
    end;
  end;
end;

procedure TfrmRekapPersediaanAkhirBarang.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  AFont.Color := clBlack;
  if (ARow > 0) and (ACol > 0) and (ARow < asgRekap.RowCount -1) then begin
    if asgRekap.Floats[ColAkhir,arow] < asgRekap.Floats[ColMinStok,arow]  then begin
      AFont.Color := shpColor1.Brush.Color;
    end;
  end;
end;

procedure TfrmRekapPersediaanAkhirBarang.asgRekapGetFormat(Sender: TObject; ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if ACol in [ColAkhir,ColHarga,ColNilai,ColMinStok] then AStyle := ssFinancial
  else AStyle := ssAutomatic;
end;

procedure TfrmRekapPersediaanAkhirBarang.btnSparePartClick(Sender: TObject);
begin
  txtSparePart.Text := '';
  SeqSPart := 0;
  Application.CreateForm(TfrmLovMasterSparePart, frmLovMasterSparePart);
  frmLovMasterSparePart.OnClose := frmMainMenu.ChildFormSingle;
  SeqSPart := frmLovMasterSparePart.Execute('',bonone,BoTrue);
  if SeqSPart <> 0 then begin
    txtSparePart.Text := EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 1)+ ' | ' +
                         EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 2);
  end;
end;

procedure TfrmRekapPersediaanAkhirBarang.btnCetakClick(Sender: TObject);
begin
   if (not BisaPrint(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Grid := asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Laporan Persediaan Akhir Barang');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.UnHideColumnsAll;
  asgRekap.ColumnSize.StretchColumn := 0;
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount -2;
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount +2;
  asgRekap.ColumnSize.StretchColumn := asgRekap.ColCount-1;
  asgRekap.ColumnSize.Stretch := True;
  Arrangecolsize;
end;

procedure TfrmRekapPersediaanAkhirBarang.btnEksporClick(Sender: TObject);
begin
   if  (not BisaEkspor(amenuid))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
 end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmRekapPersediaanAkhirBarang.btnFilterClick(Sender: TObject);
begin
   SetFilterSizeAdv(pnlFilter, btnFilter, 96);
end;

procedure TfrmRekapPersediaanAkhirBarang.btnKategoriClick(Sender: TObject);
begin
  txtKategori.Text := '';
	SeqKat := 0;
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  SeqKat         := frmLOV.ExecuteKategoriSparePart('', botrue);
  if SeqKat <> 0 then begin
    txtKategori.Text := EkstrakString(ListKategoriSparPartGlobal.Values[IntToStr(SeqKat)],'#',1)+' | '+
                        EkstrakString(ListKategoriSparPartGlobal.Values[IntToStr(SeqKat)],'#',2);
  end;
end;

procedure TfrmRekapPersediaanAkhirBarang.btnmerekClick(Sender: TObject);
begin
  txtMerek.Text := '';
	Merek := '';
  Application.CreateForm(TfrmLOV, frmLOV);
  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
  Merek         := frmLOV.ExecuteMerek('');
  if Merek <> '' then begin
    txtMerek.Text := merek;
  end;
end;

procedure TfrmRekapPersediaanAkhirBarang.btnOkClick(Sender: TObject);
begin      
  asgrekap.col := ColKode;
  asgrekap.row := 1;
  loaddata;
//  asgrekap.OnClickCell(self,asgRekap.row,asgrekap.Col);
end;

procedure TfrmRekapPersediaanAkhirBarang.btnResetClick(Sender: TObject);
begin
  setgrid;
  initform;
end;

procedure TfrmRekapPersediaanAkhirBarang.chbSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmRekapPersediaanAkhirBarang.Execute(AIdMenu: Integer);
begin
  AMenuID := AIdMenu;
  if (not BisaPrint(aidMenu)) and (not BisaEkspor(aidMenu)) and (not BisaLihatRekap(aidMenu)) and (not BisaLihatLaporan(aidMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;     
  isNeraca := False;
  SetGrid;
  InitForm;
  Run(Self);
end;

procedure TfrmRekapPersediaanAkhirBarang.Execute4Neraca(Tgl: TDate);
begin
  isNeraca := True;
  InitForm;
  SetGrid;
  dtpTgl.Date := Tgl;
  btnOk.Click;
  Run(Self);
end;

procedure TfrmRekapPersediaanAkhirBarang.FormDestroy(Sender: TObject);
begin
  ListGudang.Destroy;
end;

procedure TfrmRekapPersediaanAkhirBarang.FormShow(Sender: TObject);
begin
  if not isNeraca then
    Execute(ACurrMenuId);
end;

procedure TfrmRekapPersediaanAkhirBarang.initform;
var i : integer;
begin
  txtSparePart.Text := '';
  txtKategori.Text := '';
  asgRekap.SearchFooter.Visible := false;
  chbSF.Checked := false;
  chbShowAll.Checked := false;
  dtpTgl.Date := servernow;
  dtptglhrgrata.date := servernow;
  if GlobalSystemUser.accesslevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[ColSeq] := 0;
//    btnUpdateHarga.Visible := false;
  end;
  SeqSPart := 0; SeqKat := 0; Merek := '';

  ListGudang := TStringList.Create;
  GetListMasterGudang(ListGudang,'Nama',Bonone,True);
  cmbgudang.clear;
  for i := 0 to ListGudang.Count-1 do begin
    CmbGudang.Items.Add(EkstrakString(ListGudang.Values[ListGudang.Names[i]],'=',1));
  end;
  cmbgudang.itemindex := 0;
end;

procedure TfrmRekapPersediaanAkhirBarang.loaddata;
var Tipe : string;
    IsNol : TBooleanOperator;
    SeqGudang : integer;
begin
  if chbShowAll.Checked then IsNol := boNone
  else IsNol := boTrue;
  SeqGudang := strtointdef(ListGudang.Names[cmbGudang.ItemIndex], 0);
  setgrid;
  LoadPersediaanBarang(asgRekap,Gauge,dtpTgl.Date, SeqKat, SeqSPart, SeqGudang, IsNol, merek);
  arrangeColSize;
end;

procedure TfrmRekapPersediaanAkhirBarang.setgrid;
begin
  asgRekap.UnHideColumnsAll;
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 12;
  asgRekap.RowCount := 3;
  asgRekap.FixedCols := 1;
  asgRekap.FixedRows := 1;
  arrangecolsize;
end;

end.
