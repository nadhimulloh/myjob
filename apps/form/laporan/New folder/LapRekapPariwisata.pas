unit LapRekapPariwisata;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, clisted, ComCtrls, StdCtrls, AdvEdit, AdvOfficeButtons, Grids,
  AdvObj, BaseGrid, AdvGrid, AdvGlowButton, AdvProgressBar, ExtCtrls, AdvPanel,
  Menus, AdvCombo, AdvOfficePager;

type
  TfrmLapRekapPariwisata = class(TForm)
    MainPanel: TAdvPanel;
    AdvPanel4: TAdvPanel;
    Gauge: TAdvProgressBar;
    AopPariwisata: TAdvOfficePager;
    pg1: TAdvOfficePage;
    AdvPanel1: TAdvPanel;
    AdvProgressBar1: TAdvProgressBar;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    chbShowSF: TAdvOfficeCheckBox;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label3: TLabel;
    Label4: TLabel;
    dtpPeriode: TDateTimePicker;
    txtNobody: TAdvEdit;
    btnLovArmada: TAdvGlowButton;
    pgDetail: TAdvOfficePage;
    asgRekap: TAdvStringGrid;
    pnlFilter2: TPanel;
    btnLoad2: TAdvGlowButton;
    btnReset2: TAdvGlowButton;
    AdvPanel3: TAdvPanel;
    txtNobody2: TAdvEdit;
    btnLovArmada2: TAdvGlowButton;
    AdvPanel5: TAdvPanel;
    AdvProgressBar2: TAdvProgressBar;
    btnEkspor2: TAdvGlowButton;
    btnCetak2: TAdvGlowButton;
    btntutup: TAdvGlowButton;
    chbSf2: TAdvOfficeCheckBox;
    asgRekap2: TAdvStringGrid;
    AdvPanel6: TAdvPanel;
    Label1: TLabel;
    Label2: TLabel;
    dtpDari: TDateTimePicker;
    dtpSampai: TDateTimePicker;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapGetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure chbShowSFClick(Sender: TObject);
    procedure chbShowDecimalClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnLovArmadaClick(Sender: TObject);
    procedure btnLoad2Click(Sender: TObject);
    procedure btnReset2Click(Sender: TObject);
    procedure btntutupClick(Sender: TObject);
    procedure btnEkspor2Click(Sender: TObject);
    procedure btnCetak2Click(Sender: TObject);
    procedure asgRekap2GetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekap2GetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekap2GetFormat(Sender: TObject; ACol: Integer;
      var AStyle: TSortStyle; var aPrefix, aSuffix: string);
    procedure asgRekap2MouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekap2MouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnLovArmada2Click(Sender: TObject);
  private
    SeqArmada, AIDMenu  : integer;
    procedure InitForm;
    procedure SetGrid(idx: integer);
    procedure ArrangeColSize(idx: integer);
    procedure LoadData(idx: integer);

  public
    { Public declarations }
    procedure Execute(AMenuID : integer);
  end;

var
  frmLapRekapPariwisata: TfrmLapRekapPariwisata;

implementation

uses
  StrUtils, UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy,
  UTransaksi, UFinance, UReport, MainMenu, OracleConnection, DateUtils, LOV,
  BuktiKeluarCetak, HtgGajiPegawaiPrint, HtgGajiPegawaiPrintLama;

Const
  ColNo        = 0;
  ColCust      = 1;
  ColTelp      = 2;
  ColTglB      = 3;
  ColTglP      = 4;
  ColNoBody    = 5;
  ColHarga     = 6;
  ColUjalan    = 7;
  ColUkomisi   = 8;
  colNetto     = 9;
  colTujuan    = 10;
// det
  colDTglB      = 1;
  colDTglP      = 2;
  colDCust      = 3;
  ColDNomor     = 4;
  colDNoBody    = 5;
  colDNopol     = 6;
  colDSupir     = 7;
  colDHarga     = 8;
  colDUjalan    = 9;
  colDUkomisi   = 10;
  colDNetto     = 11;
  colDOS        = 12;
  colDTujuan    = 13;



{$R *.dfm}

procedure TfrmLapRekapPariwisata.ArrangeColSize;
begin
  if (idx = 0) or (idx = 1) then begin
    asgRekap.AutoSizeColumns(True);
  end;
  if (idx = 0) or (idx = 2) then begin
    asgrekap2.AutoNumberCol(ColNo);
    asgrekap2.ClearRows(asgrekap2.RowCount-1, 1);
    asgrekap2.Cells[colDSupir, asgrekap2.RowCount-1] := 'Total : ';
    asgrekap2.FloatingFooter.ColumnCalc[colDNetto] := acSUM;
    asgrekap2.AutoSizeColumns(True);
  end;

end;

procedure TfrmLapRekapPariwisata.asgRekap2GetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if Acol in [colNo, colDUjalan, colDUkomisi, colDHarga, colDNetto] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapRekapPariwisata.asgRekap2GetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := Acol in [colNo, colDUjalan, colDUkomisi, colDHarga, colDNetto];
  if IsFloat then begin
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else FloatFormat := '%.2n'
  end;
end;

procedure TfrmLapRekapPariwisata.asgRekap2GetFormat(Sender: TObject;
  ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if Acol in [ColUjalan, ColUkomisi, ColHarga, colNetto] then
    AStyle := ssFinancial;
end;

procedure TfrmLapRekapPariwisata.asgRekap2MouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapRekapPariwisata.asgRekap2MouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapRekapPariwisata.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then HAlign := taCenter
  else if ACol in [ColNo, ColUjalan, ColUkomisi, ColHarga, colNetto] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmLapRekapPariwisata.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := ACol in [ColNo, ColUjalan, ColUkomisi, ColHarga, colNetto];
  if IsFloat then begin
    if ACol in [ColNo] then FloatFormat := '%.0n'
    else FloatFormat := '%.2n'
  end;
end;

procedure TfrmLapRekapPariwisata.asgRekapGetFormat(Sender: TObject; ACol: Integer;
  var AStyle: TSortStyle; var aPrefix, aSuffix: string);
begin
  if Acol in [ColUjalan, ColUkomisi, ColHarga, colNetto] then
    AStyle := ssFinancial;
end;

procedure TfrmLapRekapPariwisata.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapRekapPariwisata.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize(1);
end;

procedure TfrmLapRekapPariwisata.btnCetak2Click(Sender: TObject);
begin
  if not BisaPrint(AIDMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  myConnection.advPrint.Grid:= asgrekap2;
  asgrekap2.PrintSettings.TitleLines.Clear;
  asgrekap2.PrintSettings.TitleLines.Add('');
  asgrekap2.PrintSettings.TitleLines.Add('Rekap Pariwisata Detail');
  asgrekap2.PrintSettings.TitleLines.Add('');
  asgrekap2.ColumnSize.Stretch := False;
  asgrekap2.ColCount := asgrekap2.ColCount-1;
  SetingPrint(asgrekap2);
  myConnection.advPrint.Execute;
  asgrekap2.ColCount := asgrekap2.ColCount+1;
  asgrekap2.ColumnSize.Stretch := True;
  ArrangeColSize(2);
end;

procedure TfrmLapRekapPariwisata.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AIDMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Rekap Pariwisata Per Bulan');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount-1;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount+1;
  asgRekap.ColumnSize.Stretch := True;        
  ArrangeColSize(1);
end;

procedure TfrmLapRekapPariwisata.btnEkspor2Click(Sender: TObject);
begin
  if not BisaEkspor(AIDMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
    asgExportToExcell(asgRekap2, myConnection.SaveToExcell);
end;

procedure TfrmLapRekapPariwisata.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AIDMenu) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
    asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmLapRekapPariwisata.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 94);
end;

procedure TfrmLapRekapPariwisata.btnLoad2Click(Sender: TObject);
begin
  LoadData(2);
end;

procedure TfrmLapRekapPariwisata.btnLoadClick(Sender: TObject);
begin
  LoadData(1);
end;
procedure TfrmLapRekapPariwisata.btnLovArmada2Click(Sender: TObject);
begin
  txtNoBody2.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada      := frmLov.ExecuteArmada('', boNone);
  if SeqArmada <> 0 then begin
    txtNoBody2.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapRekapPariwisata.btnLovArmadaClick(Sender: TObject);
begin
  txtNoBody.Clear;
  SeqArmada := 0;
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  SeqArmada      := frmLov.ExecuteArmada('', boNone);
  if SeqArmada <> 0 then begin
    txtNoBody.Text := EkstrakString(ListArmadaGlobal.Values[IntToStr(SeqArmada)],'#',2);
  end;
end;

procedure TfrmLapRekapPariwisata.btnReset2Click(Sender: TObject);
begin
  InitForm;
  SetGrid(2);
  asgRekap2.Row := 1;
  asgRekap2.Col := colDTglB;
  asgRekap2.SetFocus;
end;

procedure TfrmLapRekapPariwisata.btnResetClick(Sender: TObject);
begin
  InitForm;
  SetGrid(1);
  asgRekap.Row := 1;
  asgRekap.Col := colCust;
  asgRekap.SetFocus;
end;

procedure TfrmLapRekapPariwisata.btntutupClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter2, btnTutup, 94);
end;

procedure TfrmLapRekapPariwisata.chbShowDecimalClick(Sender: TObject);
begin
  ArrangeColSize(1);
end;

procedure TfrmLapRekapPariwisata.chbShowSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbShowSF.Checked;
end;

procedure TfrmLapRekapPariwisata.Execute(AMenuID: integer);
begin
  aidMenu := AMenuId;
  if (not BisaPrint(aidMenu)) and (not BisaEkspor(aidMenu)) and (not BisaLihatRekap(aidMenu)) and (not BisaLihatLaporan(aidMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  InitForm;
  SetGrid(0);
  run(self);
end;

procedure TfrmLapRekapPariwisata.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLapRekapPariwisata.InitForm;
begin
  dtpPeriode.Date   := StartOfTheMonth(ServerNow);
  dtpDari.Date   := StartOfTheMonth(ServerNow);
  dtpSampai.Date   := ServerNow;
  chbShowSF.Checked := False;
  chbSF2.Checked := False;
  asgRekap.SearchFooter.Visible := False;
  asgRekap2.SearchFooter.Visible := False;
  SeqArmada := 0;
  txtNobody.ReadOnly := true;
  txtNobody.text := '';
  txtNobody2.ReadOnly := true;
  txtNobody2.text := '';

end;

procedure TfrmLapRekapPariwisata.LoadData(idx: integer);
begin
  if (idx = 0) or (idx = 1) then begin
    asgRekap.ClearNormalCells;
    asgRekap.ClearRows(asgRekap.RowCount-1, 1);
    LoadRekapPariwisataPerBulan(asgRekap,AdvProgressBar1,dtpPeriode.Date,SeqArmada);
  end;
  if (idx = 2) or (idx = 0) then begin
    asgRekap2.ClearNormalCells;
    asgRekap2.ClearRows(asgRekap2.RowCount-1, 1);
    LoadRekapPariwisataPerTgl(asgRekap2,AdvProgressBar2,dtpDari.Date,dtpSampai.Date,SeqArmada);
  end;
  ArrangeColSize(idx);
end;

procedure TfrmLapRekapPariwisata.SetGrid(idx : integer);
begin
  if (idx = 0) or (idx = 1) then begin
    asgResetGrid(asgRekap, 2, 12, 1, 1);
  end;
  if (idx = 0) or (idx = 2) then begin
    asgResetGrid(asgRekap2, 3, 15, 1, 1);
  end;
  ArrangeColSize(idx);
end;

end.
