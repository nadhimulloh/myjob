unit LaporanNeraca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Gauges, StdCtrls, Grids, BaseGrid, AdvGrid, AdvGlowButton, ComCtrls,
  AdvPanel, ExtCtrls, AdvOfficeButtons, AdvObj;

type
  TfrmLaporanNeraca = class(TForm)
    MainPanel: TPanel;
    pnlfilter: TPanel;
    Panel2: TPanel;
    pnlPeriode: TAdvPanel;
    dtpTanggal: TDateTimePicker;
    rbtBulan: TRadioButton;
    rbtTanggal: TRadioButton;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    Gauge: TGauge;
    chbSF: TAdvOfficeCheckBox;
    chbIncNol: TAdvOfficeCheckBox;
    btnFilter: TAdvGlowButton;
    chbShowDecimal: TAdvOfficeCheckBox;
    btnUpdate: TAdvGlowButton;
    cmbTipeTrans: TComboBox;
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure chbSFClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure rbtBulanClick(Sender: TObject);
    procedure rbtTanggalClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dtpTanggalChange(Sender: TObject);
    procedure dtpTanggalCloseUp(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure btnFilterClick(Sender: TObject);
    procedure chbShowDecimalClick(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure cmbTipeTransSelect(Sender: TObject);
  private
    { Private declarations }
    AMenuid : integer;
    DateAkhir : TDate;
    tipeTrans : string;
    procedure InitForm;
    procedure SetGrid;
    procedure ArrangeColSize;
    procedure LoadData;
    procedure UpdateJurnal;//290917 juli tambah untuk memperbaiki jurnal yang membuat neraca gak balance
  public
    { Public declarations }
     procedure execute(AidMenu:integer);
  end;

var
  frmLaporanNeraca: TfrmLaporanNeraca;

implementation

uses
  OracleConnection, UGeneral, MainMenu, UConst, ADOInt, UTransaksi, DateUtils,
  UReport, USystemMenu, StrUtils, UEngine, RekapLabaRugi, UFinance;

const
  colKeterangan = 0;
  ColRupiah     = 1;
  ColKeterangan2 = 3;
  ColRupiah2     = 4;
  colSeqInduk   = 5;//juli tambah untuk pembeda saat double click munculkan detail
  ColSeqInduk2  = 6;//juli tambah untuk pembeda saat double click munculkan detail
  ColBold       = 7;
  ColBold2      = 8;


{$R *.dfm}

{ TfrmLaporanNeraca }

procedure TfrmLaporanNeraca.ArrangeColSize;
begin
  asgRekap.AutoSizeColumns(True);
  asgRekap.ColWidths[colKeterangan] := 250;
  asgRekap.ColWidths[ColRupiah]     := 150;
  asgRekap.ColWidths[colKeterangan2]:= 250;
  asgRekap.ColWidths[ColRupiah2]    := 150;
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colSeqInduk] := 0;
    asgRekap.ColWidths[colSeqInduk2] := 0;
    asgRekap.ColWidths[ColBold] := 0;
    asgRekap.ColWidths[ColBold2] := 0;
  end;
end;

//procedure TfrmLaporanNeraca.asgRekapDblClickCell(Sender: TObject; ARow,
//  ACol: Integer);
//var Tgl : TDate;
//begin
//  if (ARow > 2) then begin
//    Application.CreateForm(TfrmNeracaDetail, frmNeracaDetail);
//    frmNeracaDetail.OnClose := frmMainMenu.ChildFormSingle;
////    if rbtBulan.Checked = true Then dtpTanggal.Date := (EndOfTheMonth(dtpTanggal.Date));
//    tgl := DateAkhir;
//    if (ACol in [ColKeterangan, ColRupiah]) and ((asgRekap.Ints[colSeqInduk, ARow] <> 0)) then
//      frmNeracaDetail.Execute(tgl, asgRekap.Ints[colSeqInduk, ARow])
//    else if (ACol in [ColKeterangan2, ColRupiah2]) and ((asgRekap.Cells[ColKeterangan2, ARow] = 'Laba/(Rugi) Berjalan Bulan '+FormatDateTime('MMMM',DateAkhir))
//    or (asgRekap.Cells[ColKeterangan2, ARow] = 'Laba/(Rugi) Berjalan ')) then begin
//      Application.CreateForm(TfrmRekapLabaRugi, frmRekapLabaRugi);
//      frmRekapLabaRugi.OnClose := frmMainMenu.ChildFormSingle;
//      if rbtBulan.Checked then
//        frmRekapLabaRugi.Execute4Neraca(true, false, dateAkhir,519)
//      else if rbtTanggal.Checked then
//        frmRekapLabaRugi.Execute4Neraca(false, true, dtpTanggal.Date,519);
//    end else if (ACol in [ColKeterangan2, ColRupiah2]) and (asgRekap.Ints[colSeqInduk2, ARow] <> 0) then begin
//      frmNeracaDetail.Execute(tgl, asgRekap.Ints[colSeqInduk2, ARow])
//    end;
////    if rbtBulan.Checked = true Then dtpTanggal.Date := (StartOfTheMonth(dtpTanggal.Date));
//  end;
//end;

procedure TfrmLaporanNeraca.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
//var Tgl : TDate;
begin
  if (ARow > 2) then begin
//    Application.CreateForm(TfrmNeracaDetail, frmNeracaDetail);
//    frmNeracaDetail.OnClose := frmMainMenu.ChildFormSingle;
//    if rbtBulan.Checked = true Then dtpTanggal.Date := (EndOfTheMonth(dtpTanggal.Date));
//    tgl := DateAkhir;
//    if (ACol in [ColKeterangan, ColRupiah]) and ((asgRekap.Ints[colSeqInduk, ARow] <> 0)) then
//      frmNeracaDetail.Execute(tgl, asgRekap.Ints[colSeqInduk, ARow])
//    else
    if (ACol in [ColKeterangan2, ColRupiah2]) and ((asgRekap.Cells[ColKeterangan2, ARow] = 'Laba/(Rugi) Berjalan Bulan '+FormatDateTime('MMMM',DateAkhir))
    or (asgRekap.Cells[ColKeterangan2, ARow] = 'Laba/(Rugi) Berjalan ')) then begin
      Application.CreateForm(TfrmRekapLabaRugi, frmRekapLabaRugi);
      frmRekapLabaRugi.OnClose := frmMainMenu.ChildFormSingle;
      frmRekapLabaRugi.Execute4Neraca(rbtBulan.Checked,dateAkhir,519, chbShowDecimal.Checked);
    end;
//     else if (ACol in [ColKeterangan2, ColRupiah2]) and (asgRekap.Ints[colSeqInduk2, ARow] <> 0) then begin
//      frmNeracaDetail.Execute(tgl, asgRekap.Ints[colSeqInduk2, ARow])
//    end;
//    if rbtBulan.Checked = true Then dtpTanggal.Date := (StartOfTheMonth(dtpTanggal.Date));
  end;
end;

procedure TfrmLaporanNeraca.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow < 3 then HAlign := taCenter
  else if ACol in [ColRupiah, ColRupiah2] then HAlign := taRightJustify;
  if asgRekap.cells[ColBold, ARow] = '1' then begin
    if ACol in [ColKeterangan] then HAlign := taLeftJustify;
  end;
  if asgRekap.cells[ColBold2, ARow] = '1' then begin
    if ACol in [ColKeterangan2] then HAlign := taLeftJustify;
  end;
  if (asgRekap.Cells[ColKeterangan, ARow] = 'TOTAL AKTIVA LANCAR') or (asgRekap.Cells[ColKeterangan, ARow] = 'TOTAL AKTIVA TIDAK LANCAR') OR
  (asgRekap.Cells[ColKeterangan, ARow] = 'TOTAL AKTIVA') or (asgRekap.Cells[ColKeterangan, ARow] = 'AKTIVA') then begin
    if ACol = Colketerangan then
      HAlign := taCenter;
  end;
  if (asgRekap.Cells[ColKeterangan2, ARow] = 'TOTAL KEWAJIBAN LANCAR') OR
  (asgRekap.Cells[ColKeterangan2, ARow] = 'TOTAL KEWAJIBAN TIDAK LANCAR') or (asgRekap.Cells[ColKeterangan2, ARow] = 'TOTAL KEWAJIBAN') or
  (asgRekap.Cells[ColKeterangan2, ARow] = 'TOTAL EKUITAS') or (asgRekap.Cells[ColKeterangan2, ARow] = 'TOTAL KEWAJIBAN DAN EKUITAS') then begin
    if ACol = Colketerangan2 then
      HAlign := taCenter;
  end;
  VAlign := vtaCenter;
end;

procedure TfrmLaporanNeraca.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (asgRekap.Cells[ColBold, ARow] = '1') and (asgRekap.Cells[ColBold2, ARow] = '3') then
    AFont.Style := [fsBold];
  if (asgRekap.Cells[ColBold, ARow] = '1')  then begin
    if ACol in [ColKeterangan, ColRupiah] then AFont.Style := [fsBold];
  end;
  if asgRekap.Cells[ColBold2, ARow] = '3' then begin
    if ACol in [ColKeterangan2, ColRupiah2] then AFont.Style := [fsBold];
  end;
  if asgRekap.Cells[ColBold2, ARow] = '5' then begin
    if ACol in [ColKeterangan2] then
      AFont.Style := [fsItalic]
  end;
  if asgRekap.Cells[ColBold, ARow] = '5' then begin
    if ACol in [ColKeterangan] then
      AFont.Style := [fsItalic];
  end;
  if (asgRekap.Cells[ColBold, ARow] = '5') and (asgRekap.Cells[ColBold2, ARow] = '5') then begin
    if ACol in [ColKeterangan, ColKeterangan2] then
      AFont.Style := [fsItalic];
  end;
  if asgRekap.Cells[ColBold2, ARow] = '7' then begin  
    if ACol in [ColRupiah2] then
      AFont.Color := clBlue;
  end;
//  if asgRekap.Cells[ColBold, ARow] = '6' then begin
//    if ACol in [ColRupiah] then
//      AFont.Color := clBlue;
//  end;
//  if (asgRekap.Cells[ColBold, ARow] = '6') and (asgRekap.Cells[ColBold2, ARow] = '6') then begin
//    if ACol in [ColRupiah, ColRupiah2] then
//      AFont.Color := clBlue;
//  end;

end;

procedure TfrmLaporanNeraca.asgRekapGetCellPrintColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (asgRekap.Cells[ColBold, ARow] = '1') and (asgRekap.Cells[ColBold2, ARow] = '3') then
    AFont.Style := [fsBold];
  if (asgRekap.Cells[ColBold, ARow] = '1')  then begin
    if ACol in [ColKeterangan, ColRupiah] then AFont.Style := [fsBold];
  end;
  if asgRekap.Cells[ColBold2, ARow] = '3' then begin
    if ACol in [ColKeterangan2, ColRupiah2] then AFont.Style := [fsBold];
  end;
  if asgRekap.Cells[ColBold2, ARow] = '5' then begin
    if ACol in [ColKeterangan2] then
      AFont.Style := [fsItalic]
  end;
  if asgRekap.Cells[ColBold, ARow] = '5' then begin
    if ACol in [ColKeterangan] then
      AFont.Style := [fsItalic];
  end;
  if (asgRekap.Cells[ColBold, ARow] = '5') and (asgRekap.Cells[ColBold2, ARow] = '5') then begin
    if ACol in [ColKeterangan, ColKeterangan2] then
      AFont.Style := [fsItalic];
  end;
  if asgRekap.Cells[ColBold2, ARow] = '7' then begin  
    if ACol in [ColRupiah2] then
      AFont.Color := clBlue;
  end;
//  if asgRekap.Cells[ColBold, ARow] = '6' then begin
//    if ACol in [ColRupiah] then
//      AFont.Color := clBlue;
//  end;
//  if (asgRekap.Cells[ColBold, ARow] = '6') and (asgRekap.Cells[ColBold2, ARow] = '6') then begin
//    if ACol in [ColRupiah, ColRupiah2] then
//      AFont.Color := clBlue;
//  end;
end;

procedure TfrmLaporanNeraca.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if acol in [ColRupiah, ColRupiah2] then begin
    FloatFormat := IfThen(chbShowDecimal.Checked,'%.2n','%.0n');
    IsFloat := true;
  end else IsFloat := false
end;

procedure TfrmLaporanNeraca.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize;
end;

procedure TfrmLaporanNeraca.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then
    ArrangeColSize;
end;

procedure TfrmLaporanNeraca.btnCetakClick(Sender: TObject);
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;

  SetingPrint(asgRekap);
  myConnection.advPrint.Grid := asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Laporan Neraca');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := false ;
  asgRekap.ColCount := asgRekap.ColCount-5;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute ;
  asgRekap.ColCount := asgRekap.ColCount+5;
  asgRekap.ColumnSize.Stretch := true;
  ArrangeColSize;
end;

procedure TfrmLaporanNeraca.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmLaporanNeraca.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlfilter, btnFilter, 66);
end;

procedure TfrmLaporanNeraca.btnLoadClick(Sender: TObject);
begin
  if rbtBulan.Checked then begin
    DateAkhir := EndOfTheMonth(dtpTanggal.Date);
  end else if rbtTanggal.Checked then begin
    dateAkhir := dtpTanggal.Date;
  end;     
  loaddata;
end;

procedure TfrmLaporanNeraca.btnResetClick(Sender: TObject);
begin
  initform ; setgrid;
end;

procedure TfrmLaporanNeraca.btnUpdateClick(Sender: TObject);
begin
  UpdateJurnal;
end;

procedure TfrmLaporanNeraca.chbSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanNeraca.chbShowDecimalClick(Sender: TObject);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanNeraca.cmbTipeTransSelect(Sender: TObject);
begin
  if cmbTipeTrans.ItemIndex = 0 then  tipeTrans := '';
  if cmbTipeTrans.ItemIndex = 1 then  tipeTrans := TRANS_FAKTUR_BELI;
  if cmbTipeTrans.ItemIndex = 2 then  tipeTrans := TRANS_PEMAKAIAN_SP;  
  if cmbTipeTrans.ItemIndex = 3 then  tipeTrans := TRANS_SETORAN_KS;      
  if cmbTipeTrans.ItemIndex = 4 then  tipeTrans := TRANS_PENERIMAAN_JAMINAN;       
  if cmbTipeTrans.ItemIndex = 5 then  tipeTrans := TRANS_KEMBALI_JAMINAN;  
  if cmbTipeTrans.ItemIndex = 6 then  tipeTrans := TRANS_TERIMA_DP_PARIWISATA;      
  if cmbTipeTrans.ItemIndex = 7 then  tipeTrans := TRANS_KEMBALI_DP_PARIWISATA;     
  if cmbTipeTrans.ItemIndex = 8 then  tipeTrans := TRANS_PEMBAYARAN_PARIWISATA;     
  if cmbTipeTrans.ItemIndex = 9 then  tipeTrans := TRANS_KOMISI_AGEN;              
  if cmbTipeTrans.ItemIndex = 10 then  tipeTrans := TRANS_CLAIM_PARIWISATA;        
  if cmbTipeTrans.ItemIndex = 11 then  tipeTrans := TRANS_UANG_JALAN;            
  if cmbTipeTrans.ItemIndex = 12 then  tipeTrans := TRANS_KOMISI_SUPIR_PARIWISATA;   
  if cmbTipeTrans.ItemIndex = 13 then  tipeTrans := TRANS_PELUNASAN_HUTANG;            
  if cmbTipeTrans.ItemIndex = 14 then  tipeTrans := TRANS_PEMBELIAN_ASSET;            
  if cmbTipeTrans.ItemIndex = 15 then  tipeTrans := TRANS_PELUNASAN_BELI_ASSET;
  if cmbTipeTrans.ItemIndex = 16 then  tipeTrans := TRANS_JURNAL_UMUM_ASET;           
  if cmbTipeTrans.ItemIndex = 17 then  tipeTrans := TRANS_PENJUALAN_ASSET;

end;

procedure TfrmLaporanNeraca.dtpTanggalChange(Sender: TObject);
begin
  if rbtBulan.Checked then
    dtpTanggal.Date := StartOfTheMonth(dtpTanggal.Date);
end;

procedure TfrmLaporanNeraca.dtpTanggalCloseUp(Sender: TObject);
begin
  if rbtBulan.Checked then
    dtpTanggal.Date := StartOfTheMonth(dtpTanggal.Date);
end;

procedure TfrmLaporanNeraca.execute(AidMenu: integer);
begin
  AMenuId := AIdMenu;
  if (not BisaHapus(AmenuID)) and (not BisaLihatRekap(AmenuID)) and (not BisaPrint(AmenuID))
  and (not BisaEkspor(AmenuID)) and (not BisaNambah(AmenuID)) and (not BisaEdit(AmenuID)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  InitForm;
  SetGrid;
  Run(Self, false);
end;

procedure TfrmLaporanNeraca.FormShow(Sender: TObject);
begin
  self.execute(ACurrMenuId)
end;

procedure TfrmLaporanNeraca.InitForm;
begin
  rbtBulan.Checked := true;
  rbtTanggal.Checked := False;
  dtpTanggal.Date := StartOfTheMonth(ServerNow);
  dtpTanggal.Format := 'MMMM yyyy';
  chbSF.Checked := false;
  chbShowDecimal.Checked := false;
  asgRekap.SearchFooter.Visible := chbsf.Checked;
  rbtBulan.Visible := true;
  rbtTanggal.Visible := true;
  chbIncNol.Checked := False;
  DateAkhir := EndOfTheMonth(dtpTanggal.Date);
  dtpTanggal.DateMode := dmUpDown;
  btnUpdate.Visible := GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER;     
  cmbTipeTrans.Visible := GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER;
  tipeTrans := '';
end;

procedure TfrmLaporanNeraca.LoadData;
var tgl : Tdate;
begin
  asgrekap.ClearNormalCells;
  asgRekap.RowCount := 4;
  if rbtBulan.Checked = true THEN
    dtpTanggal.Date := (EndOfTheMonth(dtpTanggal.Date));
  tgl := trunc(dtpTanggal.date);
  if rbtBulan.Checked = true THEN
    dtpTanggal.Date := StartOfTheMonth(dtpTanggal.Date);
  if rbtBulan.Checked then
    load_lap_neraca(asgRekap, tgl, Gauge, chbIncNol.Checked, true, chbShowDecimal.Checked)
  else load_lap_neraca(asgRekap, tgl, Gauge, chbIncNol.Checked, false, chbShowDecimal.Checked);
  Arrangecolsize;
end;

procedure TfrmLaporanNeraca.rbtBulanClick(Sender: TObject);
begin
  if rbtBulan.Checked then begin
    rbtTanggal.Checked := false;
    dtpTanggal.Format := 'MMMM yyyy';
    dtpTanggal.DateMode := dmUpDown;
    dtpTanggal.date := StartOfTheMonth(dtpTanggal.Date);
  end;
end;

procedure TfrmLaporanNeraca.rbtTanggalClick(Sender: TObject);
begin
  if rbtTanggal.Checked then begin
    rbtBulan.Checked := false;
    dtpTanggal.Format := 'dd MMMM yyyy';
    dtpTanggal.DateMode := dmComboBox;                  
    dtpTanggal.date := StartOfTheMonth(dtpTanggal.Date);
  end;
end;

procedure TfrmLaporanNeraca.SetGrid;
begin
  asgResetGrid(asgRekap, 4, 10, 3, 0);
  asgRekap.MergeCells(colketerangan, 0, 5, 2);
  asgRekap.MergeCells(asgRekap.ColCount-1, 0, 1, 3);
  asgRekap.MergeCells(colSeqInduk, 0, 1, 3);
  asgRekap.Cells[colKeterangan, 0]  := 'NERACA' + #13 + UpperCase(FormatDateTime('MMMM - yyyy', dtpTanggal.Date));
  asgRekap.Cells[colKeterangan, 2]  := 'KETERANGAN';
  asgRekap.Cells[colRupiah, 2]      := 'RUPIAH';
  asgRekap.Cells[colKeterangan2, 2] := 'KETERANGAN';
  asgRekap.Cells[colRupiah2, 2]     := 'RUPIAH';
  asgRekap.Cells[colSeqInduk, 0]    := 'Seq';
  asgRekap.WordWrap := true;
  ArrangeColSize
end;

procedure TfrmLaporanNeraca.UpdateJurnal;
var tgl : TDate;
i: Integer;
AListAkun: TStringList;
begin
  AListAkun:= TStringList.Create;
  if rbtTanggal.Checked then
    tgl := dtpTanggal.date
  else tgl := StartOfTheMonth(dtpTanggal.date);
//  if (TipeTrans = TRANS_PEMAKAIAN_SP) or (TipeTrans = '') then begin
//    if not Update_jurnal_pemakaian_sp(tgl, Gauge) then begin
//      Inform(MSG_UNSUCCESS_UPDATE);
//      exit;
//    end;
//  end;
  if (TipeTrans = TRANS_PEMBAYARAN_PARIWISATA) or (TipeTrans = '') then begin
    if not Update_jurnal_bayar_pariwisata(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_FAKTUR_BELI) or (TipeTrans = '') then begin
    if not Update_jurnal_faktur_beli(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_PENERIMAAN_JAMINAN) or (TipeTrans = '') then begin
    if not Update_jurnal_penerimaan_jaminan(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_KEMBALI_JAMINAN) or (TipeTrans = '') then begin
    if not Update_jurnal_pengembalian_jaminan(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_TERIMA_DP_PARIWISATA) or (TipeTrans = '') then begin
    if not Update_jurnal_terima_dp_pariwisata(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_KOMISI_AGEN) or (TipeTrans = '') then begin
    if not Update_jurnal_rafaksi(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_CLAIM_PARIWISATA) or (TipeTrans = '') then begin
    if not Update_jurnal_claim_pariwisata(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_UANG_JALAN) or (TipeTrans = '') then begin
    if not Update_jurnal_uang_jalan(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;

  if (TipeTrans = TRANS_KOMISI_SUPIR_PARIWISATA) or (TipeTrans = '') then begin
    if not Update_jurnal_komisi_supir_pariwisata(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_SETORAN_KS) or (TipeTrans = '') then begin
    if not Update_jurnal_setoran_Baru(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_PELUNASAN_HUTANG) or (TipeTrans = '') then begin
    if not Update_jurnal_bayar_hutang(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
  if (TipeTrans = TRANS_PEMBELIAN_ASSET) or (TipeTrans = '') then begin
    if not Update_jurnal_beli_asset(tgl, Gauge) then begin
      Inform(MSG_UNSUCCESS_UPDATE);
      exit;
    end;
  end;
//  SQLToStringList(AListAkun, 'SELECT seq FROM master_akun');
//  try
//    myConnection.BeginSQL;
//    for i := 0 to AListAkun.Count-1 do begin
//      Sinkronisasi_Saldo_Akun(StrToIntDef(AListAkun.Strings[i], 0), StartOfTheMonth(dtpTanggal.Date));
//    end;
//    myConnection.EndSQL;
//  except
//    myConnection.UndoSQL;
//    Inform(MSG_UNSUCCESS_UPDATE);
//    Exit;
//  end;

  AListAkun.Destroy;

  Inform(MSG_SUCCESS_UPDATE);
end;

end.
