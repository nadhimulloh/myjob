unit LapBukuBesar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, AdvOfficeButtons, Gauges, ComCtrls,
  StdCtrls, AdvEdit, AdvPanel, AdvGlowButton, ExtCtrls;

type
  TfrmLapBukuBesar = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnReset: TAdvGlowButton;
    btnLoad: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label5: TLabel;
    Label6: TLabel;
    cmbTipeAkun: TComboBox;
    txtCari: TAdvEdit;
    btnCari: TAdvGlowButton;
    AdvPanel2: TAdvPanel;
    Label3: TLabel;
    Label4: TLabel;
    dtpSampai: TDateTimePicker;
    dtpDari: TDateTimePicker;
    AdvPanel3: TAdvPanel;
    cmbTransaksi: TComboBox;
    Panel2: TPanel;
    Gauge: TGauge;
    chbSF: TAdvOfficeCheckBox;
    btnCetak: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    asgLapBB: TAdvStringGrid;
    btnSinkronisasi: TAdvGlowButton;
    procedure asgLapBBGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnFilterClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnCariClick(Sender: TObject);
    procedure cmbTipeAkunKeyPress(Sender: TObject; var Key: Char);
    procedure cmbTipeAkunSelect(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure asgLapBBGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgLapBBGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure DetailPatchSaldo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure asgLapBBDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure btnSinkronisasiClick(Sender: TObject);
    procedure cmbTransaksiSelect(Sender: TObject);
  private
    { Private declarations }
    SeqAkun,tipe_trans,AMenuId : integer;
    ListTransaksi : TStringList;
    IsDetail : boolean;
    tipe,ASeqAkun : String;
    procedure SetGrid;
    procedure initform;
    procedure ArrangeColSize;
    procedure LoadData;
  public
    { Public declarations }
    procedure execute(AIdMenu : integer);
  end;

var
  frmLapBukuBesar: TfrmLapBukuBesar;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UReport, Home, MainMenu, OracleConnection, DateUtils, LOV, LOVAkunChecked, 
  UTransaksi_temp, UReport_temp, PatchBarangInput, PatchAssetInput, PatchAkunInput,
  PatchHutangInput, PatchKasbankInput, PatchKasbonInput, PatchPiutangInput, 
  UFinance, StrUtils, ADOInt, UEngine_Temp, SinkronisasiAkun;

{$R *.dfm}
const
  ColNo         = 0;
  ColSumber     = 1;
  ColNojurnal   = 2;
  colNoSJ       = 3;
  ColSaldo      = 4;
  ColDebit      = 5;
  ColKredit     = 6;
  ColSaldoAkhir = 7;
  colselisih    = 8;
  colketerangan = 9;
  ColSeq        = 10;


procedure TfrmLapBukuBesar.ArrangeColSize;
begin
  asgLapBB.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then
    asgLapBB.ColWidths[ColSeq] := 0;
  if (tipe_trans <> TIPE_JURNAL_SETORAN_KS) and (tipe_trans <> 0) then
    asgLapBB.ColWidths[colNoSJ] := 0
  else asgLapBB.ColWidths[colNoSJ] := 400;
  asgLapBB.ColWidths[colselisih] := 0;

end;

procedure TfrmLapBukuBesar.asgLapBBDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
//var Buffer : _Recordset;
//  Master   : TR_faktur_beli_master ;
//  MasterSO : TR_stok_opname_master ;
//  MasterJS : TR_jaminan_supir_master ;
//  MasterKB : TR_keluar_biaya_master ;
//  MasterRB : TR_retur_beli_master ;
//  MasterKJ : TR_kembali_jaminan_supir_mst ;
//  MasterTD : TR_terima_dp_pariwisata_master ;
//  MasterBP : TR_bayar_pariwisata_master ;
//  MasterBH : TR_bayar_hutang_master ;
//  MasterMD : TR_mutasi_dana_master ;
//  MasterKD : TR_keluar_dp_master ;
//  MasterTP : TR_terima_pendapatan_master ;
//  MasterPR : TR_pinjaman_dr_relasi_master ;
//  MasterBR : TR_bayar_pinjam_ke_relasi_master ;
//  MasterSM : TR_setor_modal_master ;
//  MasterKM : TR_kembali_dp_master ;
//  MasterKP : TR_kembali_dp_pariwisata_master ;
//  MasterAL : TR_ambil_laba_master ;
//  MasterSD : TR_sewa_dibayar_dimuka_master ;
//  MasterPM : TR_KASBON_PEGAWAI_MASTER ;
//  MasterPK : TR_pinjaman_ke_relasi_master ;
//  MasterLK : TR_LUNAS_KASBON_PEGAWAI_Master ;
//  MasterKR : TR_kembali_dr_relasi_master ;
//  MasterBA : TR_beli_asset_master ;
//  MasterBM : TR_bayar_hutang_asset_master ;
//  MasterJA : TR_jual_asset_master ;
//  MasterPA : TR_terima_piutang_asset_master ;
//  MasterKA : TR_komisi_agen_Master ;
//  MasterCP : TR_claim_pariwisata_Master ;
//  MasterFJ : TR_faktur_jual_ks_master;
//  MasterUJ : TR_uang_jalan_master;
//  MasterKC : TR_keluar_dana_k_cbng_mst;
//  MasterTC : TR_terima_dana_dr_cbng_mst;
//  MasterS : TR_setoran_master;
//  tipe,cek1,cek2,cek3,cek4 : string;
begin
//  if (Arow > 1) and (Acol > colNo) then begin
//    cek1 := RightStr(asgLapBB.Cells[ColSumber,asgLapBB.Row],15);
//    cek1 := LeftStr(cek1,3);
//    cek2 := RightStr(asgLapBB.Cells[ColSumber,asgLapBB.Row],14);
//    cek2 := LeftStr(cek2,3);
//    cek3 := RightStr(asgLapBB.Cells[ColSumber,asgLapBB.Row],13);
//    cek3 := LeftStr(cek3,3);
//    cek4 := RightStr(asgLapBB.Cells[ColSumber,asgLapBB.Row],12);
//    cek4 := LeftStr(cek4,3);
//    if (cek1='PSB')or(cek1='PSH')or(cek1='PKB')or(cek1='PSP')or(cek1='PSA')or(cek1='PSM')or(cek1='PBK')or(cek1='PKP')or(cek1='PSN') or
//       (cek2='PSB')or(cek2='PSH')or(cek2='PKB')or(cek2='PSP')or(cek2='PSA')or(cek2='PSM')or(cek2='PBK')or(cek2='PKP')or(cek2='PSN') or
//       (cek3='PSB')or(cek3='PSH')or(cek3='PKB')or(cek3='PSP')or(cek3='PSA')or(cek3='PSM')or(cek3='PBK')or(cek3='PKP')or(cek3='PSN') or
//       (cek4='PSB')or(cek4='PSH')or(cek4='PKB')or(cek4='PSP')or(cek4='PSA')or(cek4='PSM')or(cek4='PBK')or(cek4='PKP')or(cek4='PSN') then begin
//       DetailPatchSaldo;
//    end else
//  end;

end;

procedure TfrmLapBukuBesar.asgLapBBGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if Arow < 2 then HAlign := taCenter
  else if ACol in [ColNo, ColDebit, ColKredit, ColSaldo, ColSaldoAkhir, colselisih] then HAlign := taRightJustify
  else if  (arow > 1) and (asgLapBB.Cells[ColSaldoAkhir, arow] <> '') and (acol in [ColSaldo]) then HAlign := taRightJustify;
  VAlign:=vtaCenter;
end;

procedure TfrmLapBukuBesar.asgLapBBGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  AFont.Style:=[];
  if asgLapBB.Cells[ColSaldoAkhir, arow] <> '' then ABrush.Color := frmMainMenu.AdvPanelStyler1.Settings.ColorTo;

end;

procedure TfrmLapBukuBesar.asgLapBBGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  if ACol in [ColSaldo, ColDebit, ColKredit, ColSaldoAkhir, colselisih, ColSeq] then begin
    IsFloat := true;
    if ACol in [ColSaldo, ColDebit, ColKredit, ColSaldoAkhir, Colselisih] then FloatFormat := '%.2n'
    else if ACol = ColSeq then FloatFormat := '%.0f';
  end else isfloat := false;
end;

procedure TfrmLapBukuBesar.btnCariClick(Sender: TObject);
var Akun : TR_Master_Akun;
    ArAkun : Arrstring;
    i : integer;  ATipe: String;
begin
//0 SEMUA
//1 AKTIVA
//2 PASIVA
//3 MODAL
//4 PENDAPATAN
//5 HPP
//6 BIAYA
//7 PENDAPATAN DAN BIAYA LAIN-LAIN
  case cmbTipeAkun.ItemIndex of
    0 : ATipe := '';
    1 : ATipe := TIPE_AKUN_AKTIVA;
    2 : ATipe := TIPE_AKUN_PASIVA;
    3 : ATipe := TIPE_AKUN_MODAL;
    4 : ATipe := TIPE_AKUN_PENDAPATAN;
    5 : ATipe := TIPE_AKUN_HPP;
    6 : ATipe := TIPE_AKUN_BIAYA;
    7 : ATipe := TIPE_AKUN_BIAYA_LAIN_LAIN;
  end;



  txtCari.Text:= '';
  if not IsDetail then begin
    Application.CreateForm(TfrmLOV, frmLOV);
    frmLOV.OnClose := frmMainMenu.ChildFormSingle;
    SeqAkun := frmLOV.ExecuteAkun('',botrue, ATipe,'2');
    if SeqAKun <> 0 then begin
      Akun := Get_master_akun(seqAkun);
      txtCari.Text := Akun.kode+' | '+Akun.nama;
      ASeqAkun := IntToStr(Akun.seq); 
    end else begin
      txtCari.Text:= '';
      ASeqAkun := '';
      SeqAkun := 0;
    end;
  end else begin
    ASeqAkun := '';
    Application.CreateForm(TfrmLovAkunChecked,frmLovAkunChecked);
    frmLovAkunChecked.OnClose := frmMainMenu.ChildFormSingle;
    ArAkun := frmLovAkunChecked.Execute('');
    for i := 0 to Length(ArAkun)-1 do begin
      if i = 0 then
        ASeqAkun := ArAkun[i]
      else ASeqAkun := ASeqAkun+','+ArAkun[i];
    end;
  end;
end;

procedure TfrmLapBukuBesar.btnCetakClick(Sender: TObject);
begin
 if (not BisaPrint(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
 end;
  myConnection.advPrint.Grid := asgLapBB;
  asgLapBB.PrintSettings.TitleLines.Clear;
  asgLapBB.PrintSettings.TitleLines.Add('');
  asgLapBB.PrintSettings.TitleLines.Add('Laporan Buku Besar');
  asgLapBB.PrintSettings.TitleLines.Add('');
  asgLapBB.ColumnSize.Stretch := False;
  asgLapBB.ColCount := asgLapBB.ColCount-2;
  SetingPrint(asgLapBB);
  myConnection.advPrint.Execute;
  asgLapBB.ColCount := asgLapBB.ColCount+2;
  asgLapBB.ColumnSize.Stretch := true;
  arrangecolsize;
end;

procedure TfrmLapBukuBesar.btnEksporClick(Sender: TObject);
begin
  if (not BisaEkspor(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  asgExportToExcell(asgLapBB, myConnection.SaveToExcell);
end;

procedure TfrmLapBukuBesar.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(Panel1, btnFilter, 91);
end;

procedure TfrmLapBukuBesar.btnLoadClick(Sender: TObject);
begin
  LoadData;
end;

procedure TfrmLapBukuBesar.btnResetClick(Sender: TObject);
begin
  InitForm;
  setgrid;
end;

procedure TfrmLapBukuBesar.btnSinkronisasiClick(Sender: TObject);
begin
  Application.CreateForm(TfrmSinkronisasiAkun, frmSinkronisasiAkun);
  frmSinkronisasiAkun.OnClose := frmMainMenu.ChildFormSingle;
  frmSinkronisasiAkun.Execute(dtpDari.Date);
end;

procedure TfrmLapBukuBesar.chbSFClick(Sender: TObject);
begin
  asgLapBB.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLapBukuBesar.cmbTipeAkunKeyPress(Sender: TObject; var Key: Char);
begin
  key:=#0;
end;

procedure TfrmLapBukuBesar.cmbTipeAkunSelect(Sender: TObject);
begin
  tipe := IntToStr(cmbTipeAkun.ItemIndex);
end;

procedure TfrmLapBukuBesar.cmbTransaksiSelect(Sender: TObject);
begin
  if cmbTransaksi.ItemIndex <= 0 then
    tipe_trans := 0
  else begin
    tipe_trans := StrToIntDef(EkstrakString(ListTransaksi.Values[inttostr(cmbTransaksi.ItemIndex)], '#', 1), 0);
  end;
end;

procedure TfrmLapBukuBesar.DetailPatchSaldo;
var PSMaster : TR_patch_saldo_master;
    tipe : string;
begin
  PSMaster := Get_patch_saldo_master(asgLapBB.Ints[ColSeq, asgLapBB.Row]);
  tipe := LeftStr(PSMaster.nomor,3);
  //-------------------PATCH_SALDO-------------------------
  if tipe = H_Patch_Saldo_Spare_part then begin
    Application.CreateForm(TfrmInputPatchBarang, frmInputPatchBarang);
    frmInputPatchBarang.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchBarang.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row], false);
  end else if tipe = H_Patch_Saldo_Hutang then begin
    Application.CreateForm(TfrmInputPatchHutang, frmInputPatchHutang);
    frmInputPatchHutang.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchHutang.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row], false);
  end else if tipe = H_Patch_Saldo_Asset then begin
    Application.CreateForm(TfrmInputPatchAsset, frmInputPatchAsset);
    frmInputPatchAsset.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchAsset.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row], false);
  end else if tipe = H_Patch_Saldo_Piutang then begin
    Application.CreateForm(TfrmInputPatchPiutang, frmInputPatchPiutang);
    frmInputPatchPiutang.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchPiutang.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row], false);
  end else if tipe = H_Patch_Saldo_AKUN then begin
    Application.CreateForm(TfrmInputPatchAkun, frmInputPatchAkun);
    frmInputPatchAkun.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchAkun.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row]);
  end else if tipe = H_Patch_Saldo_Kasbank then begin
    Application.CreateForm(TfrmInputPatchKasBank, frmInputPatchKasBank);
    frmInputPatchKasBank.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchKasBank.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row], false);
  end else if tipe = H_Patch_Saldo_KASBON_PGW then begin
    Application.CreateForm(TfrmInputPatchKasbon, frmInputPatchKasbon);
    frmInputPatchKasbon.OnClose := frmMainMenu.ChildFormSingle;
    frmInputPatchKasbon.Execute(AsgLapBB.Ints[ColSeq, AsgLapBB.Row], false);
  end;
end;

procedure TfrmLapBukuBesar.execute(AIdMenu: integer);
  var i : integer;
begin
  AMenuId := AIdMenu;
   if (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
     and (not BisaNambah(AMenuId)) and (not BisaLihatRekap(AMenuId)) and (not BisaLihatLaporan(AMenuId)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  ListTransaksi := TStringList.Create;
  ListTransaksi.Clear; cmbTransaksi.Items.Clear;
  Get_List_Tipe_Transaksi_Jurnal(ListTransaksi);
  for i := 0 to ListTransaksi.Count-1 do begin
    cmbTransaksi.Items.Append(EkstrakString(ListTransaksi.ValueFromIndex[i], '#', 2));
  end;
  initform;
  Setgrid;
  run(self);
end;

procedure TfrmLapBukuBesar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ListTransaksi.Destroy;
end;

procedure TfrmLapBukuBesar.FormShow(Sender: TObject);
begin
  execute(ACurrMenuId);
end;

procedure TfrmLapBukuBesar.initform;
begin
  tipe := '';
  cmbTipeAkun.ItemIndex  := 0;
  cmbTransaksi.ItemIndex := 0;
  txtCari.Text := '';
  aSeqAkun := '';
  SeqAkun  := 0;
  dtpSampai.Date := EndOfTheMonth(ServerNow);
  dtpDari.Date   := StartOfTheMonth(ServerNow);
  chbSF.Checked  := false;
  asgLapBB.SearchFooter.Visible := False;
  btnSinkronisasi.Visible := False;
end;

procedure TfrmLapBukuBesar.LoadData;
  var tipe : string; //tipe_trans : integer;
begin
  if cmbTipeAkun.ItemIndex = 0 then
    Tipe := ''
  else begin
    if cmbTipeAkun.Text = TIPE_AKUN_AKTIVA_TEXT then tipe := TIPE_AKUN_AKTIVA
    else if cmbTipeAkun.Text = TIPE_AKUN_PASIVA_TEXT then tipe := TIPE_AKUN_PASIVA
    else if cmbTipeAkun.Text = TIPE_AKUN_MODAL_TEXT then tipe  := TIPE_AKUN_MODAL
    else if cmbTipeAkun.Text = TIPE_AKUN_PENDAPATAN_TEXT then tipe := TIPE_AKUN_PENDAPATAN
    else if cmbTipeAkun.Text = TIPE_AKUN_HPP_TEXT then tipe := TIPE_AKUN_HPP
    else if cmbTipeAkun.Text = TIPE_AKUN_BIAYA_TEXT then tipe := TIPE_AKUN_BIAYA
    else if cmbTipeAkun.Text = TIPE_AKUN_BIAYA_LAIN_LAIN_TEXT then tipe := TIPE_AKUN_BIAYA_LAIN_LAIN
  end;

  asgLapBB.RowCount := 3;
  asgLapBB.ClearNormalCells;
  load_lap_buku_besar_new(asgLapBB, dtpDari.Date, dtpSampai.Date, tipe, ASeqAkun, Gauge, tipe_trans);
  ArrangeColSize;
end;

procedure TfrmLapBukuBesar.SetGrid;
begin
  asgLapBB.ColCount  := 12;
  asgLapBB.RowCount  := 3;
  asgLapBB.FixedRows := 2;

  asgLapBB.Cells[ColNo, 0] := 'No. Akun';
  asgLapBB.Cells[ColNo, 1] := 'No.';

  asgLapBB.Cells[ColSumber, 0] := 'Nama Akun';
  asgLapBB.Cells[ColSumber, 1] := 'Sumber Jurnal';

  asgLapBB.MergeCells(ColNojurnal,0,1,2);
  asgLapBB.Cells[ColNojurnal, 1] := 'No. Jurnal';

  asgLapBB.MergeCells(colNoSJ,0,1,2);
  asgLapBB.Cells[colNoSJ, 1] := 'No. SJ - Tgl. SJ';

  asgLapBB.Cells[ColSaldo, 0] := 'Akun D/K';
  asgLapBB.Cells[ColSaldo, 1] := 'Saldo Awal';

  asgLapBB.MergeCells(ColDebit,0,1,2);
  asgLapBB.Cells[ColDebit, 1] := 'Debit';

  asgLapBB.MergeCells(ColKredit,0,1,2);
  asgLapBB.Cells[ColKredit, 1] := 'Kredit';

  asgLapBB.MergeCells(ColSaldoAkhir,0,1,2);
  asgLapBB.Cells[ColSaldoAkhir,1] := 'Saldo Akhir';

  asgLapBB.MergeCells(ColSelisih,0,1,2);
  asgLapBB.Cells[ColSelisih,1] := 'Selisih';

  asgLapBB.MergeCells(ColKeterangan,0,1,2);
  asgLapBB.Cells[ColKeterangan,1] := 'Keterangan';

  asgLapBB.MergeCells(ColSeq,0,1,2);
  asgLapBB.Cells[ColSeq,1] := 'Seq';

  asgLapBB.MergeCells(asgLapBB.ColCount-1,0,1,2);

  asgLapBB.ClearNormalCells;
  ArrangeColSize;
end;

end.

