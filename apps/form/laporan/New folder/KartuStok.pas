unit KartuStok;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficeButtons, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvEdit,
  ComCtrls, AdvPanel, AdvGlowButton, ExtCtrls, OracleConnection, UEngine, UConst, UGeneral,
  USystemMenu, URecord, UReport, DateUtils;

type
  TfrmKartuStok = class(TForm)
    Panel1: TPanel;
    pnlFilter: TPanel;
    btnOk: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    Label1: TLabel;
    Label2: TLabel;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    AdvPanel2: TAdvPanel;
    Label7: TLabel;
    Label6: TLabel;
    txtSparePart: TAdvEdit;
    btnSparePart: TAdvGlowButton;
    cmbGudang: TComboBox;
    Panel2: TPanel;
    asgRekap: TAdvStringGrid;
    btnFilter: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    Label9: TLabel;
    Label4: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure chbSFClick(Sender: TObject);
    procedure btnSparePartClick(Sender: TObject);
    procedure asgRekapCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
  private
    { Private declarations }
    DateNow : TDate;
    BrowseMode : boolean;
    SeqSPart, SeqKat, idMenu : Integer;
    ListGudang : TStringList;
    procedure ArrangeColSize;
    procedure InitForm;
    procedure Setgrid;
    procedure LoadData;
  public
    { Public declarations }
    procedure Execute(AMenuId: Integer); overLoad;
    procedure Execute(AMenuId, APartSeq, AGudangSeq, AKatSeq : Integer; ADari, ASampe: TDate); overLoad;
  end;

var
  frmKartuStok: TfrmKartuStok;

implementation

uses MainMenu, LovMasterSparePart, LOV;

{$R *.dfm}

const
  ColNo        = 0;
  ColTgl       = 1;
  ColNomor     = 2;
  ColDeskripsi = 3;
  ColAwal      = 4;
  ColMasuk     = 5;
  ColKeluar     = 6;
  ColPinjam     = 7;
  ColKPinjam      = 8;
  ColMeminjamkan  = 9;
  ColKMeminjamkan = 10;
  ColAkhir        = 11;

{ TfrmKartuStok }

procedure TfrmKartuStok.ArrangeColSize;
begin
  asgRekap.Cells[ColDeskripsi, asgRekap.RowCount-1] := 'Total';
  asgRekap.AutoSizeColumns(true);
end;

procedure TfrmKartuStok.Execute(AMenuId: Integer);
var i : integer;
begin
  idMenu  := AMenuId;
  DateNow := ServerNow;
  if (not BisaPrint(idMenu)) and (not BisaEkspor(idMenu)) and (not BisaLihatRekap(idMenu)) and (not BisaLihatLaporan(idMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;

  ListGudang := TStringList.Create;
  GetListMasterGudang(ListGudang,'Nama',Bonone,False);
  for i := 0 to ListGudang.Count-1 do begin
    CmbGudang.Items.Add(EkstrakString(ListGudang.Values[ListGudang.Names[i]],'=',1));
  end;

  BrowseMode := False;
  InitForm;
  Setgrid;
  Run(Self);
end;


procedure TfrmKartuStok.asgRekapCellValidate(Sender: TObject; ACol, ARow: Integer; var Value: string; var Valid: Boolean);
begin
  arrangecolsize;
end;

procedure TfrmKartuStok.asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then
    HAlign := taCenter
  else if ACol in[ColNo, ColAwal..ColAkhir] then
    HAlign := taRightJustify;
end;

procedure TfrmKartuStok.asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  isFloat := ACol in[ColNo, ColAwal..ColAkhir];
  case ACol of
    ColNo  : FloatFormat := '%.0n';
    ColAwal, ColMasuk..ColAkhir: FloatFormat := '%.2n';
  end;
end;

procedure TfrmKartuStok.asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  if Shift <> [] then ArrangeColSize;
end;

procedure TfrmKartuStok.btnCetakClick(Sender: TObject);
begin
  if (not BisaPrint(idMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Kartu Stok Spare Part');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 1;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := asgRekap.ColCount + 1;
  asgRekap.ColumnSize.Stretch := True;
  ArrangeColSize;
end;

procedure TfrmKartuStok.btnEksporClick(Sender: TObject);
begin
  if  (not BisaEkspor(idMenu))  then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmKartuStok.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 90);
end;

procedure TfrmKartuStok.btnOkClick(Sender: TObject);
begin
  //gunawan jika gudang belum dipilih 311018
  if cmbGudang.ItemIndex = -1 then begin
    inform('Gudang belum dipilih.');
    cmbGudang.SetFocus;
    exit;
  end;
  Loaddata;
end;

procedure TfrmKartuStok.btnResetClick(Sender: TObject);
begin
  InitForm;
  SetGrid;
end;

procedure TfrmKartuStok.btnSparePartClick(Sender: TObject);
begin
  txtSparePart.Text := '';
  SeqSPart := 0;
  Application.CreateForm(TfrmLovMasterSparePart, frmLovMasterSparePart);
  frmLovMasterSparePart.OnClose := frmMainMenu.ChildFormSingle;
  SeqSPart := frmLovMasterSparePart.Execute('',bonone,BoTrue);
  if SeqSPart <> 0 then begin
    txtSparePart.Text := EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 1)+ ' | ' +
                         EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 2);
  end;
end;

procedure TfrmKartuStok.chbSFClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmKartuStok.Execute(AMenuId, APartSeq, AGudangSeq, AKatSeq: Integer; ADari, ASampe: TDate);
var i : integer;
begin
  idMenu  := AMenuId;
  DateNow := ServerNow;
  if (not BisaPrint(idMenu)) and (not BisaEkspor(idMenu)) and (not BisaLihatRekap(idMenu)) and (not BisaLihatLaporan(idMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;

  ListGudang := TStringList.Create;
  GetListMasterGudang(ListGudang,'Nama',Bonone,True);
  for i := 0 to ListGudang.Count-1 do begin
    CmbGudang.Items.Add(EkstrakString(ListGudang.Values[ListGudang.Names[i]],'=',1));
  end;

  BrowSeMode   := True;
  InitForm;
  SeqSPart     := APartSeq;
  if AGudangSeq <> 0 then cmbGudang.ItemIndex := ListGudang.IndexOfName(IntToStr(AGudangSeq));
  self.caption := 'Kartu Stok';
  if APartSeq <> 0 then begin
    txtSparePart.Text := EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 1)+ ' | ' +
                         EkstrakString(ListsparepartGlobal.Values[inttostr(SeqSPart)], '#', 2);
  end else txtSparePart.Clear;
  dtpAwal.Date  := ADari;
  dtpAkhir.Date := ASampe;
  LoadData;
  Run(Self);
end;

procedure TfrmKartuStok.FormDestroy(Sender: TObject);
begin
  ListGudang.Destroy;
end;

procedure TfrmKartuStok.FormShow(Sender: TObject);
begin
  if not BrowseMode then Execute(ACurrMenuId);
end;

procedure TfrmKartuStok.InitForm;
begin
  dtpAwal.Date        := StartOfTheMonth(DateNow);
  dtpAkhir.Date       := DateNow;
  SeqSPart            := 0;
  SeqKat              := 0;
  txtSparePart.Text   := '';
  cmbGudang.ItemIndex := -1;
  chbSF.Checked       := False;
  asgRekap.SearchFooter.Visible := False;
end;

procedure TfrmKartuStok.LoadData;
var SeqGudang : integer;
begin
  if SeqSPart = 0 then begin
    Alert('Pilih satu spare part');
    btnSparePart.SetFocus;
    Exit;
  end;

  SeqGudang := strtointdef(ListGudang.Names[cmbGudang.ItemIndex], 0);
  LoadKartuStok(asgRekap, dtpAwal.Date, dtpAkhir.Date, SeqSPart, SeqGudang, SeqKat);
  if asgRekap.RowCount = 2 then asgRekap.AddRow;
  ArrangeColSize;
end;

procedure TfrmKartuStok.Setgrid;
begin
  asgResetGrid(asgRekap, 3, 13, 1,1);
  ArrangeColSize;
end;

end.
