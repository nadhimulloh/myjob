unit ImportDataTerminalWB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvGridWorkbook, ExtCtrls, Grids, AdvObj, BaseGrid, AdvGrid, Gauges,
  AdvGlowButton, tmsAdvGridExcel, StdCtrls, ADOInt;

type
  TfrmImportDataTerminalWB = class(TForm)
    MainPanel: TPanel;
    asgWbInput: TAdvGridWorkbook;
    btnSimpan: TAdvGlowButton;
    btnImport: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    Gauge: TGauge;
    addXls: TAdvGridExcelIO;
    odLoadXls: TOpenDialog;
    asgWBtampung: TAdvGridWorkbook;
    asgInput: TAdvStringGrid;
    Label1: TLabel;
    Label2: TLabel;
    procedure btnImportClick(Sender: TObject);
    procedure asgInputCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure asgInputGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnSimpanClick(Sender: TObject);
    procedure asgInputCanDeleteRow(Sender: TObject; ARow: Integer;
      var CanDelete: Boolean);
  private
    { Private declarations }
    JumBaris,idxold: integer;
    procedure InitForm;
    procedure SetGrid(idx : integer);
    procedure ArrangeColSize;
    procedure arrangexls(idx : integer);
    function IsValid(idx:integer) : boolean;
    function IsSaved(idx : integer) : Boolean;
    procedure ChangeDate; // Oggi 161018 +
    procedure SetNoOrder; // Oggi 161018 +
  public
    { Public declarations }
    StatSimpan : Boolean;
    function Execute : boolean;
  end;

var
  frmImportDataTerminalWB: TfrmImportDataTerminalWB;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, Home, OracleConnection, MainMenu, DateUtils, StrUtils,
  UConst_Temp;

const
  colno             = 0;
  colArmada         = 1;
  colNoorder        = 2;
  colsupir          = 3;
  colJamkota1       = 4;
  coljmlPenumpang1  = 5;
  colJamkota2       = 6;
  coljmlPenumpang2  = 7;
  colket2           = 8;
  colket1           = 9;
  colKota1          = 10;
  colKota2          = 11;
  coltanggal        = 12;
  colseqSupir       = 13;
  ColKotaSumber     = 14; //iqbal 050917 tambah Kolom
  ColTglTemp        = 15; //Oggi 161018 +
  ColRit            = 16; // Oggi 181018 +
  ColSeqJurusan     = 17; // Oggi 181018 +

{$R *.dfm}

{ TfrmImportDataTerminalWB }

procedure TfrmImportDataTerminalWB.ArrangeColSize;
begin
  asgWBinput.Grid.AutoNumberCol(colno);
  asgWBinput.Grid.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgInput.ColWidths[colseqSupir]   := 0;
    asgInput.ColWidths[ColKotaSumber] := 0;
    asgInput.ColWidths[ColTglTemp]    := 0; // Oggi 161018 +
    asgInput.ColWidths[ColRit]        := 0; // Oggi 181018 +
    asgInput.ColWidths[ColSeqJurusan] := 0; // Oggi 181018 +
  end;
end;

procedure TfrmImportDataTerminalWB.arrangexls(idx: integer);
var i, row : integer;
    Tempket1,TempKota1,TempKota2, Temptanggal : string;
    tempjam1,tempjam2 : TTime;
    dtSupir : TR_master_supir;
begin
  asgWbtampung.ActiveSheet := idx;
  asgWbInput.ActiveSheet := idx;
  setgrid(idx);

  asgWBtampung.Grid.SplitAllCells();
  for i := 3 to asgWbtampung.Grid.rowcount-1 do begin
    if ((asgWbtampung.Grid.Cells[colArmada, i] = 'NO_BODY') or (
      asgWbtampung.Grid.Cells[colArmada-1, i] = 'NO'))
         or (asgWbtampung.Grid.Cells[colArmada, i] = '') then begin
      if ((asgWbtampung.Grid.Cells[colArmada, i] = 'NO_BODY')
      ) and (asgWbtampung.Grid.Cells[colArmada-1, i] = 'NO')and (asgWbtampung.Grid.Cells[colArmada-1, i-1] <> 'NO') then begin
        Tempket1    := asgWbtampung.Grid.Cells[colno, 2];
        TempKota1   := asgWbtampung.Grid.Cells[colJamkota1, i+1];
        TempKota2   := asgWbtampung.Grid.Cells[colJamkota2, i+1];
        Temptanggal := asgWbtampung.Grid.Cells[colsupir, 3];
      end;
      Continue;
    end;
    if asgWbinput.Grid.Cells[colArmada,1] <> '' then asgWbinput.Grid.AddRow;
    Row := asgWbinput.Grid.RowCount-1;

    asgWbinput.Grid.Cells[colArmada       ,Row]:= asgWbtampung.Grid.Cells[Colarmada  ,i];
    asgWbinput.Grid.Cells[colNoorder      ,Row]:= asgWbtampung.Grid.Cells[colNoorder ,i];
    asgWbinput.Grid.Cells[colsupir        ,Row]:= asgWbtampung.Grid.Cells[colsupir   ,i];
    asgWbinput.Grid.Cells[coljmlPenumpang1,Row]:= asgWbtampung.Grid.Cells[coljmlPenumpang1,i];
    asgWbinput.Grid.Cells[coljmlPenumpang2,Row]:= asgWbtampung.Grid.Cells[coljmlPenumpang2,i];
    asgWbinput.Grid.Cells[colket2         ,Row]:= asgWbtampung.Grid.Cells[colket2         ,i];
    asgWbinput.Grid.Cells[colket1         ,Row]:= Tempket1;
    asgWbinput.Grid.Cells[colKota1        ,Row]:= TempKota1;
    asgWbinput.Grid.Cells[colKota2        ,Row]:= TempKota2;
    asgWbinput.Grid.cells[coltanggal      ,Row]:= Temptanggal;

    dtSupir := Get_master_supir(0,asgInput.Cells[colsupir,Row]);
    if dtSupir.seq <> 0 then
      asgWbinput.Grid.ints[colseqSupir,Row] := dtSupir.seq
    else asgWbinput.Grid.ints[colseqSupir,Row] := 0;


    tempJam1 := strtotimedef(asgWbtampung.Grid.Cells[colJamkota1,i],0);
    if tempjam1 = 0 then begin
      tempJam1 := StrToTimeDef(ReplaceStr(asgWbtampung.Grid.Cells[colJamkota1,i],'.',':'),0);
      if tempJam1 = 0 then asgWbinput.Grid.cells[colJamkota1    ,Row]:= ''
      else asgWbinput.Grid.Times[colJamkota1   ,Row]:= tempJam1;
    end else asgWbinput.Grid.Times[colJamkota1 ,Row]:= tempJam1;

    tempJam2 := strtotimedef(asgWbtampung.Grid.Cells[colJamkota2,i],0);
    if tempjam2 = 0 then begin
      tempJam2 := StrToTimeDef(ReplaceStr(asgWbtampung.Grid.Cells[colJamkota2,i],'.',':'),0);
      if tempJam2 = 0 then asgWbinput.Grid.cells[colJamkota2    ,Row]:= ''
      else asgWbinput.Grid.Times[colJamkota2    ,Row]:= tempJam2;
    end else asgWbinput.Grid.Times[colJamkota2  ,Row]:= tempJam2;
    asgWBInput.Grid.Cells[ColKotaSumber, Row] := asgWBTampung.sheets.items[idx].name; //iqbal 050917 tambah Kolom Kota Sumber
  end;
  arrangecolsize;  
end;

procedure TfrmImportDataTerminalWB.asgInputCanDeleteRow(Sender: TObject;
  ARow: Integer; var CanDelete: Boolean);
begin
  if (arow > 0) and (asginput.cells[colarmada,arow] <> '') then CanDelete := true;
end;

procedure TfrmImportDataTerminalWB.asgInputCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  canedit := false;
  if ARow > 0 then begin
    if acol in [coljmlPenumpang2, coljmlPenumpang1] then canedit := true
    else canedit := false
  end;
end;

procedure TfrmImportDataTerminalWB.asgInputGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
   if (ARow < 1) then HAlign := taCenter
  else if ACol in [ColNo, coljmlPenumpang1, coljmlPenumpang2] then HAlign := taRightJustify;
  VAlign := vtaCenter;
end;

procedure TfrmImportDataTerminalWB.btnImportClick(Sender: TObject);
var j : integer;
//    Tempket1,TempKota1,TempKota2, Temptanggal : string;
//    tempjam1,tempjam2 : TTime;
//    tempJml1,tempJml2 : integer;
begin
 odLoadXls.InitialDir := GetCurrentDir;
  if odLoadXls.Execute then begin
    asgWBInput.Sheets.Clear;
    asgWBInput.Grid.ClearNormalCells;
    AddXls.AdvGridWorkbook := asgWBtampung;
    AddXls.GridStartRow  := 1;
    AddXls.GridStartCol  := 0;
    AddXls.XlsStartRow   := 1;
    AddXls.XlsStartCol   := 1;
    AddXls.XLSImport(odLoadXls.FileName);
    JumBaris := 0;
    for J := 0 to asgWBtampung.Sheets.Count-1 do begin
      asgWBtampung.ActiveSheet := j;
      if (asgWBtampung.Grid.Cells[colArmada,1] = '') and
        (asgWbtampung.Grid.Cells[colsupir, 1] = '') then continue
      else asgWbInput.AddSheet('Sheet'+inttostr(j+1));
      setgrid(j);
      arrangexls(j);
      arrangecolsize;
    end;
  end;
  asgInput.SortByColumn(ColArmada); // Oggi 181018 + agar berurutan coz mau menambahkan rits
  ChangeDate; // Oggi 161018 +
  SetNoOrder; // Oggi 161018 +
  arrangecolsize;
end;

procedure TfrmImportDataTerminalWB.btnSimpanClick(Sender: TObject);
var i : integer;
begin
  btnSimpan.Enabled := false;
  if isvalid(1) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    for i := 0 to asgWbInput.sheets.count-1 do begin
      if i = 4 then begin
        inform('asup');
      end;

      isvalid(i);
      if IsSaved(i) then begin
        if i = asgWbInput.sheets.count-1 then begin
          if Confirmed(MSG_ADD_DATA) then begin
            initform; Setgrid(i); statSimpan := true;
            asgWbInput.Sheets.Clear;
          end else ModalResult := mrOk;
        end;
      end else begin
        Inform(MSG_UNSUCCESS_SAVING);
        ActiveControl := btnBatal;
      end;
    end;
  end;                      
  btnSimpan.Enabled := true;
end;

procedure TfrmImportDataTerminalWB.ChangeDate;
var i : integer;
    Temp,tglTemp, BulanTemp, TahunTemp : String;
begin
  tglTemp:=''; BulanTemp:=''; TahunTemp:='';
  Temp :='';
  for i := 1 to asgInput.RowCount-1 do begin
    Temp := EkstrakString(asgInput.Cells[ColTanggal, i],',',2);
    tglTemp := EkstrakString(Temp,' ',2);
    BulanTemp := EkstrakString(Temp, ' ',3);
    if BulanTemp = 'JANUARI' then BulanTemp := '01';
    if BulanTemp = 'FEBRUARI' then BulanTemp := '02';
    if BulanTemp = 'MARET' then BulanTemp := '03';
    if BulanTemp = 'APRIL' then BulanTemp := '04';
    if BulanTemp = 'MEI' then BulanTemp := '05';
    if BulanTemp = 'JUNI' then BulanTemp := '06';
    if BulanTemp = 'JULI' then BulanTemp := '07';
    if BulanTemp = 'AGUSTUS' then BulanTemp := '08';
    if BulanTemp = 'SEPTEMBER' then BulanTemp := '09';
    if BulanTemp = 'OKTOBER' then BulanTemp := '10';
    if BulanTemp = 'NOVEMBER' then BulanTemp := '11';
    if BulanTemp = 'DESEMBER' then BulanTemp := '12';
    TahunTemp := EkstrakString(Temp, ' ',4);
    asgInput.Dates[ColTglTemp, i] := varToDateTime(tglTemp+'/'+bulanTemp+'/'+TahunTemp);
  end;
  ArrangeColSize;
end;

function TfrmImportDataTerminalWB.execute: boolean;
begin
  InitForm;
  asgWBInput.Sheets.Clear;
  Self.Caption := 'Import Data Terminal';
  asgInput.RowCount := 2;
  asgInput.ClearnormalCells;
  Setgrid(0);
  Run(self);
  Result := ModalResult = mrOk;
end;

procedure TfrmImportDataTerminalWB.InitForm;
begin
  idxold := 1;
end;

function TfrmImportDataTerminalWB.IsSaved(idx : integer): Boolean;
var dtMst : TR_MASTER_OPERASIONAL_TERMINAL;
    dtSupir : TR_master_supir;
    dtArmada : TR_master_armada;
    dtsj : TR_sj_armada_master;
    dtKota , dtKota2, dtKotaSumber: TR_master_kota;
    i : integer;
begin
  Gauge.Show;
  try
//    for j := 0 to asgWBinput.Sheets.Count-1 do begin
      asgWBinput.ActiveSheet := idx;
      for i := 1 to asgWBinput.Grid.RowCount-1 do begin
        Gauge.Progress := round((i+1)/asgWBinput.Grid.RowCount*100);
        dtMst.nobody        := Trimallstring(asgWBinput.Grid.Cells[colArmada, i]);
        dtMst.Noorder       := Trimallstring(asgWBinput.Grid.Cells[colNoorder,i]);
        dtMst.supir         := Trimallstring(asgWBinput.Grid.Cells[colsupir,  i]);

        if asgWBinput.Grid.cells[colJamkota1,i] <> '' then
          dtMst.Jamkota1    := vartodatetime(asgWBinput.Grid.cells[colJamkota1,i])
        else dtMst.Jamkota1 :=  0;

        dtMst.jmlPenumpang1 := asgWBinput.Grid.Ints[coljmlPenumpang1   , i];
        if asgWBinput.Grid.cells[colJamkota2,i] <> '' then
          dtMst.Jamkota2    := vartodatetime(asgWBinput.Grid.cells[colJamkota2,i])
        else dtMst.Jamkota2 :=  0;

        dtMst.jmlPenumpang2 := asgWBinput.Grid.Ints[coljmlPenumpang2   , i];
        dtMst.ket2          := Trimallstring(asgWBinput.Grid.Cells[colket2, i]);
        dtMst.ket1          := Trimallstring(asgWBinput.Grid.Cells[colket1, i]);
        dtMst.Kota1         := Trimallstring(asgWBinput.Grid.Cells[colKota1, i]);
        dtMst.Kota2         := Trimallstring(asgWBinput.Grid.Cells[colKota2, i]);
        dtMst.tanggal       := DateStrToDate(asgWBinput.Grid.Cells[coltanggal, i]);

//        dtSupir := Get_master_supir(0,dtMst.supir);
        dtMst.Supir_SEQ       := asgWBinput.Grid.Ints[colseqSupir, i];//dtSupir.seq;
        dtArmada              := Get_master_armada(0,dtMst.nobody);
        dtMst.Armada_SEQ      := dtArmada.seq;
        dtMst.jurusan_seq     := dtArmada.Jurusan_seq;
        dtSJ    := Get_sj_armada_master(0,dtMst.Noorder);
        dtMst.order_SEQ       := dtSJ.seq;
        dtKota  := Get_master_kota(0,dtMst.Kota1);
        dtMst.kota1_SEQ       := dtKota.seq;
        dtKota2 := Get_master_kota(0,dtMst.Kota2);
        dtMst.kota2_SEQ       := dtKota2.seq;
        //iqbal 050917 tambah save kota Sumber
        dtKotaSumber          := Get_master_kota(0,TrimAllString(asgWBInput.Grid.Cells[ColKotaSumber, i]));
        dtMst.Kota_Sumber_Seq := dtKotaSumber.Seq;
        dtMst.jurusan_seq     := asgInput.Ints[ColSeqJurusan, i]; // Oggi 181018 +
        if (dtMst.nobody <> '' ) and (dtMst.supir <> '') then begin
          Save_master_OPERASIONAL_TERMINAL(dtMst);
        end;
      end;
//    end;
    Gauge.hide;
    Result := true;
    myConnection.EndSQL;
  except
    Result := false;
    myConnection.UndoSQL;
  end;
end;

function TfrmImportDataTerminalWB.isvalid(idx:integer): boolean;
var i: integer;
    tempJml1,tempJml2 : integer;
begin
  result := true;
  asgWbInput.ActiveSheet := idx;
  for i := 1 to asgWBinput.Grid.RowCount-1 do begin
    if asgWBinput.Grid.Ints[colseqSupir,i] = 0 then begin
      inform('Supir tidak ada dalam Database.');
      asgWBinput.Grid.row := i;
      asgWBinput.Grid.Col := colsupir;
      asgWBinput.Grid.SetFocus;
      result := false;
      exit;
    end;
    if asgWBinput.Grid.Cells[coljmlPenumpang1,i] <> '' then begin
      tempJml1 := StrToIntDef(asgWBinput.Grid.Cells[coljmlPenumpang1,i],0);
      if tempJml1 = 0 then begin
        inform('Pada baris ke - '+inttostr(i)+', jumlah penumpang tidak sesuai.');
        asgWBinput.Grid.row := I;
        asgWBinput.Grid.Col := coljmlPenumpang1;
        asgWBinput.Grid.SetFocus;
        result := false;
        exit;
      end;
    end;

    if asgWBinput.Grid.Cells[coljmlPenumpang2,i] <> '' then begin
      tempJml2 := StrToIntDef(asgWBinput.Grid.Cells[coljmlPenumpang2,i],0);
      if tempJml2 = 0 then begin
        inform('Pada baris ke - '+inttostr(i)+', Jumlah penumpang tidak sesuai.');
        asgWBinput.Grid.row := I;
        asgWBinput.Grid.Col := coljmlPenumpang2;
        asgWBinput.Grid.SetFocus;
        result := false;
        exit;
      end;
    end;
  end;
  if asgWBinput.Grid.cells[colArmada,1] = '' then begin
    inform('Tidak ada data yang diimport.');
    asgWBinput.Grid.row := 1;
    asgWBinput.Grid.Col := colArmada;
    asgWBinput.Grid.SetFocus;
    result := false;
    exit;
  end;
end;

procedure TfrmImportDataTerminalWB.SetGrid(idx : integer);
begin
  asgWbInput.ActiveSheet := idx;
  asgWBinput.Grid.ClearNormalCells;
  asgWBinput.Grid.ColCount := 19{15}; //iqbal 050917 tambah Kolom
  asgWBinput.Grid.rowcount := 2;
  asgWBinput.Grid.cells[colno            , 0] := 'No.';
  asgWBinput.Grid.cells[colArmada        , 0] := 'Armada';
  asgWBinput.Grid.cells[colNoorder       , 0] := 'No. Order';
  asgWBinput.Grid.cells[colsupir         , 0] := 'Supir';
  asgWBinput.Grid.cells[colJamkota1      , 0] := 'Jam Kota 1';
  asgWBinput.Grid.cells[coljmlPenumpang1 , 0] := 'Jml. Penumpang 1';
  asgWBinput.Grid.cells[colJamkota2      , 0] := 'Jam Kota 2';
  asgWBinput.Grid.cells[coljmlPenumpang2 , 0] := 'Jml. Penumpang 2';
  asgWBinput.Grid.cells[colket2          , 0] := 'Keterangan';
  asgWBinput.Grid.cells[colket1          , 0] := 'Ket 2';
  asgWBinput.Grid.cells[colKota1         , 0] := 'Kota 1';
  asgWBinput.Grid.cells[colKota2         , 0] := 'Kota 2';
  asgWBinput.Grid.cells[coltanggal       , 0] := 'Tanggal';
  asgWBinput.Grid.cells[colseqSupir      , 0] := 'seq';
  asgWBinput.Grid.cells[ColKotaSumber    , 0] := 'Kota Sumber';
  asgWBinput.Grid.cells[ColTglTemp       , 0] := 'TempTgl';
  asgWBinput.Grid.cells[ColRit           , 0] := 'RIT'; // Oggi 181018 +
  asgWBinput.Grid.cells[ColSeqJurusan    , 0] := 'SeqJurusan'; // Oggi 181018 +
  ArrangeColSize;
end;

procedure TfrmImportDataTerminalWB.SetNoOrder;
var vBuff : _RecordSet;
    i, rit : integer;
    TempArmada : String;
begin
  {// isi rit
  TempArmada := ''; rit := 0;
  for i := 1 to asgInput.RowCount-1 do begin
    if TempArmada <> asgInput.Cells[ColArmada, i] then begin
      rit := 0;
      rit := rit+1;
      asgInput.Ints[ColRit, i] := rit;
    end else begin
      rit := rit+1;
      asgInput.Ints[ColRit, i] := rit;
    end;
    TempArmada := asgInput.Cells[ColArmada, i];
  end;}
  
  for i := 1 to asgInput.RowCount-1 do begin
    sql := 'SELECT m.nomor, j.seq '+
              ' FROM sj_armada_master m, master_armada a, master_supir s, master_jurusan j '+
              ' WHERE m.armada_seq = a.seq AND m.supir_seq = s.seq AND m.jurusan_seq = j.seq '+
              ' AND m.tanggal = '+FormatSQLDate(asgInput.Dates[ColTglTemp, i])+
              ' AND a.no_body = '+FormatSQLString(asgInput.Cells[ColArmada, i])+
              ' AND s.nama = '+FormatSQLString(asgInput.Cells[ColSupir, i])+
//              ' AND m.rit = '+FormatSQLNumber(asgInput.Ints[ColRit, i])+
              ' AND m.jurusan_seq IN (SELECT master_seq FROM detail_jurusan dj, master_kota k '+
                    ' WHERE dj.master_seq = j.seq AND dj.kota_seq = k.seq '+
                    ' AND k.nama = '+FormatSQLString(asgInput.Cells[ColKota1, i])+
                    ' AND URUTAN = 1) '+
              ' AND m.jurusan_seq IN (SELECT master_seq FROM detail_jurusan dj, master_kota k '+
                    ' WHERE dj.master_seq = j.seq AND dj.kota_seq = k.seq '+
                    ' AND k.nama = '+FormatSQLString(asgInput.Cells[ColKota2, i])+
                    ' AND URUTAN = (SELECT MAX(urutan) FROM detail_jurusan dj WHERE dj.master_seq = j.seq)) ';
    vBuff := myConnection.OpenSQL(sql);
    if vBuff.RecordCount > 0 then begin
      asgInput.Cells[ColNoOrder,    i] := BufferToString(vBuff.Fields[0].Value);
      asgInput.Ints[ColSeqJurusan,  i] := BufferToInteger(vBuff.Fields[1].Value);
    end;
    vBuff.Close;
  end;
end;

end.
