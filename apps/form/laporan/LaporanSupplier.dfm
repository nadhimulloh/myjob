object frmLaporanSupplier: TfrmLaporanSupplier
  Left = 342
  Top = 219
  Caption = 'frmLaporanSupplier'
  ClientHeight = 361
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 706
    Height = 361
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 704
      Height = 359
      Align = alClient
      TabOrder = 0
      DesignSize = (
        704
        359)
      object Shape1: TShape
        Left = 7
        Top = 340
        Width = 13
        Height = 11
        Anchors = [akLeft, akBottom]
        Brush.Color = clBlack
        ExplicitTop = 250
      end
      object Shape2: TShape
        Left = 62
        Top = 340
        Width = 13
        Height = 11
        Anchors = [akLeft, akBottom]
        Brush.Color = clRed
        ExplicitTop = 250
      end
      object Label1: TLabel
        Left = 26
        Top = 339
        Width = 22
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Aktif'
      end
      object Label2: TLabel
        Left = 81
        Top = 339
        Width = 40
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Nonaktif'
      end
      object AdvProgressBar1: TAdvProgressBar
        Left = 568
        Top = 338
        Width = 128
        Height = 15
        Anchors = [akRight, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Level0ColorTo = 14811105
        Level1ColorTo = 13303807
        Level2Color = 5483007
        Level2ColorTo = 11064319
        Level3ColorTo = 13290239
        Level1Perc = 70
        Level2Perc = 90
        Position = 50
        ShowBorder = True
        Version = '1.2.0.2'
      end
      object btnEkspor: TAdvGlowButton
        Left = 256
        Top = 6
        Width = 75
        Height = 36
        Caption = 'E&kspor'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001FC49444154484B6364A03160A4B1F90C036F815E891EF75F4E260366
          4646BDFFFF190D803E36FEFFFFDFDA2B2D17DA89F13D8A0F342A35845999390D
          41863032301A3230FED7F9CFC0A0096433A118F69FA1E152CBB946922DD0AB35
          029A4718F04BF2338030360034E0E3D6C00D023039141F50C30290C15B0237C0
          CD25CB026B5D2B061B5D6B86E5375632446A84C33D72F9CD15862B6FAE12B6C0
          5BCF934156589661D5A9350CEFBEBE633055346130563062587F7623C3CB4F2F
          19321CD318321DD3197C3704326C0E580FB760D98D15604B09FAC052D9826146
          FC5486F98717324CDB3F836173FE7A86171F5F3024CC4961F80F84F8E280E820
          9A18D5CF60A268CCB0F0E8228674875486C8E9310CB75EDE06BB96621F800C91
          1592615897B39A81099842579C5AC5D0BDBD171E1414C701C824255145869599
          CB1818191919369FDFC2D0B8A9857A16800C9D9F3C87418C579461CBC56D0CA9
          F6C90C71B393182E3FB94C9D200A330B65A8F6A960A85E5BCBB0F7FA7E866D05
          9B185E7D7ECD10353396E1EFBFBF9447728C6514033B0B3BC3BC230B18FEFFFF
          CF60AD62C5A02BABC3B015E89BC7EF9E502792F115185489647C1650251F906B
          0130443F6E0DC251D82117D7C0F4A9C3C8F0DF90AAC5353657832A9CFF9CCC7A
          408BF481F58229B0743464F8FF6FF525722A1CC23501E92A06BE4E26DDCDA83A
          00FCFBFC193DEFE1C10000000049454E44AE426082}
        TabOrder = 0
        OnClick = btnEksporClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnCetak: TAdvGlowButton
        Left = 175
        Top = 6
        Width = 75
        Height = 36
        Caption = '&Cetak'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000019249444154484B6364A03160A4B1F90C582D9874EAB7FD7FC67F0EA4
          58CEF89FE9409E19EB41743D582D9878FA67FDFFFF0C0D2459C0C8D0906FCADE
          38022C707272AA5777896E507789212584186EEE5902C44B1BF6EDDB87124CF0
          38F0F4F46497909048F8F6ED5B049F8C8683B0921E4916BCBD7789E1D3931B07
          383939577EFDFA75E1EAD5ABBF830C805B909090B096919131882453712B5E37
          7FFEFC60140B1213137F03055828B1E096310703E37F0606D5733FFE002D6045
          B70028453D00B4001C3AF02002FA80B61604060652D582F5EBD7A3FA00983CA9
          6A0130B962B7E0CF9F3F0CC06446566470737333B0B040D2094E0B4086FFD10D
          65E077CC24C9928FFBA733B05C5ECD00B2049705F0640ACCC90C64E66498A3FE
          007D809A4C1D1D1D3700339A3F4805B2059D6B6EE2F589B59630838D9608ACA8
          00ABFDFFFFFFDAFDFBF787A024535051F1E3C78F78A02592C651550ED27AB60E
          A484D1D34B870F9C5DD6760068F8F3C78F1F2FBC73E7CE4F140B900D43AE0F70
          F900E672983EC641551FD0BCCA2425EC09A9A579AB0200EA53BC1998AD60B800
          00000049454E44AE426082}
        TabOrder = 1
        OnClick = btnCetakClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object asgRekap: TAdvStringGrid
        Left = 7
        Top = 98
        Width = 690
        Height = 236
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 50
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 2
        OnMouseWheelDown = asgRekapMouseWheelDown
        OnMouseWheelUp = asgRekapMouseWheelUp
        OnGetCellColor = asgRekapGetCellColor
        OnGetCellPrintColor = asgRekapGetCellPrintColor
        OnGetAlignment = asgRekapGetAlignment
        OnGetFloatFormat = asgRekapGetFloatFormat
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = [fsBold]
        ColumnHeaders.Strings = (
          'No.'
          'Tipe'
          'Jenis'
          'Kode'
          'Nama'
          'Kode Penomoran'
          'Unit Bisnis'
          'Alamat'
          'Kota'
          'Provinsi'
          'Negara'
          'Kode Pos'
          'No. Telp 1'
          'No. Telp 2'
          'No. Fax'
          'Email'
          'Website'
          'Kontak'
          'Penanggung Jwb 1'
          'Jabatan 1'
          'Penanggung Jwb 2'
          'Jabatan 2'
          'User Id'
          'Aktif'
          'Seq')
        ColumnSize.Stretch = True
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedRowHeight = 22
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = [fsBold]
        FloatFormat = '%.2f'
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'Tahoma'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'Tahoma'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'Tahoma'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'Tahoma'
        PrintSettings.FooterFont.Style = []
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'Tahoma'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurrence'
        SearchFooter.HintFindPrev = 'Find previous occurrence'
        SearchFooter.HintHighlight = 'Highlight occurrences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SearchFooter.ShowClose = False
        SearchFooter.ShowHighLight = False
        VAlignment = vtaCenter
        Version = '6.0.4.2'
        WordWrap = False
        ColWidths = (
          64
          64
          64
          64
          64
          64
          64
          64
          69
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64
          64)
        RowHeights = (
          22
          22
          22
          22
          22
          22
          22
          22
          22
          22)
      end
      object btnLoad: TAdvGlowButton
        Left = 7
        Top = 6
        Width = 78
        Height = 36
        Caption = '&Load'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000008B49444154484B6364A03160A4B1F90CF4B7E0FCA1E7DE4CCC4C26E4
          F8ECDFDF7F670CED24B722EBC5F0C1C5C32F7E303032B2936301D0B01F7A36E2
          9CF82D38F2F23F3986C3F4E8DB88A3381AD30734B780D64174E9C82BAFFF8CFF
          4DC90926C6FF8CA7F56CC4B6E18D83D154349A8AF026AED1544454DE1B4D4594
          A722A2029A0445F4AF9349701C514A0179396619DA9374400000000049454E44
          AE426082}
        TabOrder = 3
        OnClick = btnLoadClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnReset: TAdvGlowButton
        Left = 91
        Top = 6
        Width = 78
        Height = 36
        Caption = '&Reset'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001A949444154484BC594CF4B025110C767B414CA4DA32890ACD5FA1322
          E8187429F05AA7AE950A42A78E6DB7A04B1DDCF0DA293C06D929BA17FD079A4B
          BF0E1A62AE4684F97A6B4AFEDA1D5793DEF5CD7C3F33EF3B6F10FA7CB0CFFAD0
          33602AF236F7147226F40AED19209EA87BC07046490F6F8284A566D09F009081
          C4805D16D9C75A263451A887B40262CC26660A8788B0C10347CD78C418DC592D
          B09A0C08E95A5E0B408CA8C75C3C6C46B82196C1552A242CEB036435CBA9A62A
          FF05E08D0599DFB003AFACB25A82B5E47026C29837EA4633B9E20162BC587E5F
          273DA807A4820239043F00F4A4D2B7DB202DD1535419BBEA5102C23EE585279A
          9B7DDC7225FBF60FA802C827A004A87B43C0B49CF33D045DF79488D1B3B60748
          D703DEC979998FD373273E180D460BC01D6543F6927A06887E86205100DF41D6
          C9460673B52E9B27AF01E08EE6C7ED5F7001800BD4B3B4BBE71D6795A030A6BB
          8B44391F47C0956EC4B51C0E38E2801D5D80D681AD8CE7FC672E9A816895F3F8
          5325EDD8E52BFBD3709B9AF5802AA4FD14C598557C55656EF40B657277806A16
          B5062871EDFE7F7F7227155231DF32F49D19DFE1983D0000000049454E44AE42
          6082}
        TabOrder = 4
        OnClick = btnResetClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object GroupBox1: TGroupBox
        Left = 7
        Top = 48
        Width = 690
        Height = 44
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Filter'
        TabOrder = 5
        object Label3: TLabel
          Left = 9
          Top = 18
          Width = 31
          Height = 13
          Caption = 'Status'
          Transparent = True
        end
        object xt: TLabel
          Left = 9
          Top = 48
          Width = 20
          Height = 13
          Caption = 'Tipe'
          Transparent = True
        end
        object Label4: TLabel
          Left = 210
          Top = 18
          Width = 24
          Height = 13
          Caption = 'Jenis'
          Transparent = True
        end
        object cmbStatus: TAdvComboBox
          Left = 53
          Top = 14
          Width = 145
          Height = 21
          Color = clWindow
          Version = '1.4.0.0'
          Visible = True
          Style = csDropDownList
          DropWidth = 0
          Enabled = True
          ItemIndex = -1
          ItemHeight = 13
          Items.Strings = (
            'Semua'
            'Aktif'
            'Nonaktif')
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          TabOrder = 0
        end
        object cmbTipe: TAdvComboBox
          Left = 53
          Top = 44
          Width = 145
          Height = 21
          Color = clWindow
          Version = '1.4.0.0'
          Visible = True
          Style = csDropDownList
          DropWidth = 0
          Enabled = True
          ItemIndex = -1
          ItemHeight = 13
          Items.Strings = (
            'Semua'
            'Supplier'
            'Vendor')
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          TabOrder = 1
        end
        object cmbDes: TComboBox
          Left = 241
          Top = 14
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          Items.Strings = (
            'Semua'
            'Company'
            'Individual')
        end
        object chbSF: TAdvOfficeCheckBox
          Left = 392
          Top = 14
          Width = 171
          Height = 20
          TabOrder = 3
          OnClick = chbSFClick
          Alignment = taLeftJustify
          Caption = 'Tampilkan Kolom Pencarian'
          ReturnIsTab = False
          Version = '1.3.4.1'
        end
      end
      object btnAtur: TAdvGlowButton
        Left = 337
        Top = 6
        Width = 75
        Height = 36
        Caption = '&Atur Kolom'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000008249444154484B6364A03160A4B1F90CF4B5C07981FB7F627CE4ABB5
          09AFB2023376B8C3517C40370BF626ECC41A743007E0929F70EA27380408FA60
          E85BE0A7BDA9015B2C6EBAEA0716C725FFFF3F03589E6010E14A259BAFF981ED
          A53815D1DC07A3913C1AC90CA34505C12A81E29C4CC806B22D20643039F2F4AD
          93C97121213D00B13EAA1963B9837C0000000049454E44AE426082}
        TabOrder = 6
        OnClick = btnAturClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
    end
  end
end
