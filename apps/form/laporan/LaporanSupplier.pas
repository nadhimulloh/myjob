unit LaporanSupplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo;

type
  TfrmLaporanSupplier = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    xt: TLabel;
    Label4: TLabel;
    cmbStatus: TAdvComboBox;
    cmbTipe: TAdvComboBox;
    cmbDes: TComboBox;
    chbSF: TAdvOfficeCheckBox;
    AdvProgressBar1: TAdvProgressBar;
    btnAtur: TAdvGlowButton;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnAturClick(Sender: TObject);
  private
    AMenuid : integer;
    listHideCol : Tstringlist;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmLaporanSupplier: TfrmLaporanSupplier;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, MasterCardInput, StrUtils, AturKolom;

{$R *.dfm}

const
  ColNo         = 0;
  colTypeCard   = 1;
  colDes        = 2;
  ColKode       = 3;
  ColNama       = 4;
  colKodeNmr    = 5;
  colUnit       = 6;
  ColAlamat     = 7;
  colKota       = 8;
  colProv       = 9;
  colNegara     = 10;
  colPost       = 11;
  ColNoTelp1    = 12;
  ColNoTelp2    = 13;
  ColNoFax      = 14;
  ColEmail      = 15;
  ColWebsite    = 16;
  ColContact    = 17;
  Colpng_jwb1   = 18;
  Coljabatan1   = 19;
  Colpng_jwb2   = 20;
  Coljabatan2   = 21;
  ColUserId     = 22;
  ColAktif      = 23;
  ColSeq        = 24;

procedure TfrmLaporanSupplier.arrangecolsize;
var col, i : integer;
begin
  asgRekap.AutoNumberCol(colno);
  asgRekap.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
    asgRekap.ColWidths[colaktif] := 0;
  end;

  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.ColWidths[col]:= 0;
    end;
  end;                         
end;

procedure TfrmLaporanSupplier.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [colNo] then HAlign := taRightJustify;
end;

procedure TfrmLaporanSupplier.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmLaporanSupplier.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
    AFont.Color := clRed;
end;

procedure TfrmLaporanSupplier.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol = colseq then FloatFormat    := '%.0f'
  end;
end;

procedure TfrmLaporanSupplier.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanSupplier.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanSupplier.btnAktivasiClick(Sender: TObject);
var Tanggal : tdate;
begin
  if not BisaHapus(AMenuid) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Cells[ColAktif, asgRekap.Row]= '' then  begin
    Tanggal := 0;
  end else
    Tanggal := VarToDateTime(asgRekap.Cells[ColAktif, asgRekap.Row]);
  if Confirmed(MSG_CONFIRMED_AKTIVASI) then begin
    UEngine.Aktivasi_Master(asgRekap.Ints[ColSeq, asgRekap.Row], Tanggal, 'master_Card', true, 'tgl_hapus');
    loaddata;
  end;
end;

procedure TfrmLaporanSupplier.btnAturClick(Sender: TObject);
begin
  listHideCol.clear;
  arrangecolsize;
  Application.CreateForm(TfrmAturKolom, frmAturKolom);
  frmAturKolom.OnClose := frmMainMenu.ChildFormSingle;
  frmAturKolom.execute(listHideCol, asgRekap);
  arrangecolsize;
end;

procedure TfrmLaporanSupplier.btnCetakClick(Sender: TObject);
var col, I : integer;

begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Laporan Supplier/Vendor');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.HideColumn(col);
    end;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.UnHideColumnsAll;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 3;
  arrangecolsize;
end;

procedure TfrmLaporanSupplier.btnLoadClick(Sender: TObject);
begin
  loaddata;
end;

procedure TfrmLaporanSupplier.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
end;

procedure TfrmLaporanSupplier.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanSupplier.Execute(AidMenu: integer);
begin
  listHideCol := Tstringlist.create;
  AMenuId := AIdMenu;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmLaporanSupplier.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLaporanSupplier.initform;
begin
  listHideCol.clear;  
  cmbStatus.ItemIndex           := 1;
  cmbTipe.ItemIndex             := 0;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanSupplier.loaddata;
var i,Row       : Integer;
    Astatus     : TBooleanOperator;
    Filter, Atipe, ADes : string;
    ArData      : AR_master_card;
begin
  asgRekap.RowCount := 2;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  AStatus := boNone;
  case cmbStatus.ItemIndex of
    1 : Filter := ' and tgl_hapus is null ';
    2 : Filter := ' and tgl_hapus is not null ';
  end;
//  Atipe := '';
//  case cmbTipe.ItemIndex of
//    1 :
//    Atipe := TIPE_Supplier;
    Atipe := TIPE_VENDOR;
//  end;

  Ades := '';
  case cmbDes.ItemIndex of
    1 : ADes := TIPE_COMPANY;
    2 : ADes := TIPE_INDIE;
  end;  

  filter := filter + ifthen(Atipe<>'',' and tipe = '+FormatSQLString(Atipe));
  filter := filter + ifthen(ADes<>'',' and jenis = '+FormatSQLString(ADes));

  ArData := Get_Arr_master_card('',Filter);
  for i := 0 to Length(ArData) - 1 do begin
    AdvProgressBar1.Position := round((i+1)/Length(ArData)*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -1;
    Asgrekap.cells[colTypeCard ,Row] := ifthen(arData[i].tipe=TIPE_CUSTOMER,TIPE_CUSTOMER_TEXT,
      TIPE_VENDOR_TEXT);
    Asgrekap.cells[colDes      ,Row] := ifthen(arData[i].jenis=TIPE_COMPANY,TIPE_COMPANY_TEXT,
      TIPE_indie_TEXT);
    Asgrekap.cells[colKode     ,Row] := arData[i].kode;
    Asgrekap.cells[colNama     ,Row] := arData[i].nama;
    Asgrekap.cells[colAlamat   ,Row] := arData[i].alamat;
    Asgrekap.cells[colKota     ,Row] := arData[i].kota;
    Asgrekap.cells[colProv     ,Row] := arData[i].provinsi;
    Asgrekap.cells[colNegara  ,Row] := arData[i].negara;
    Asgrekap.cells[colPost     ,Row] := arData[i].kodepos;
    Asgrekap.cells[colNoTelp1  ,Row] := arData[i].no_telp1;
    Asgrekap.cells[colNoTelp2  ,Row] := arData[i].no_telp2;
    Asgrekap.cells[colNoFax    ,Row] := arData[i].no_fax;
    Asgrekap.cells[colEmail    ,Row] := arData[i].email;
    Asgrekap.cells[colWebsite  ,Row] := arData[i].website;
    Asgrekap.cells[colContact  ,Row] := arData[i].kontak;
    Asgrekap.cells[colUserId   ,Row] := arData[i].user_id;

    if arData[i].tgl_hapus <> 0 then      
      Asgrekap.Dates[colAktif    ,Row] := arData[i].tgl_hapus;
    Asgrekap.ints[colSeq       ,Row] := arData[i].seq;
    Asgrekap.cells[colKodeNmr  ,Row] := arData[i].kode_nomor;
    if arData[i].unit_bisnis = TIPE_DIVISI_LOGISTIC then
      Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_LOGISTIC_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_TRADING then
      Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_TRADING_TEXT
    else if arData[i].unit_bisnis = TIPE_DIVISI_CONSTRUCTION then
      Asgrekap.cells[colUnit      ,Row] := TIPE_DIVISI_CONSTRUCTION_TEXT;

    Asgrekap.cells[Colpng_jwb1 ,Row] := arData[i].png_jwb1;
    Asgrekap.cells[Coljabatan1 ,Row] := arData[i].jabatan1;
    Asgrekap.cells[Colpng_jwb2 ,Row] := arData[i].png_jwb2;
    Asgrekap.cells[Coljabatan2 ,Row] := arData[i].jabatan2;
  end;
  AdvProgressBar1.Hide;
  arrangecolsize;
end;

procedure TfrmLaporanSupplier.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 26;
  asgRekap.RowCount := 2;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;
  arrangecolsize;
end;

procedure TfrmLaporanSupplier.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

end.
