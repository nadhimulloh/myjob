object frmLampiranInvoice: TfrmLampiranInvoice
  Left = 337
  Top = 223
  BorderStyle = bsDialog
  Caption = 'Tambah Invoice'
  ClientHeight = 447
  ClientWidth = 1042
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 172
    Width = 27
    Height = 13
    Caption = 'Nama'
  end
  object Label22: TLabel
    Left = 16
    Top = 201
    Width = 26
    Height = 13
    Caption = 'State'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1042
    Height = 447
    Align = alClient
    TabOrder = 0
    DesignSize = (
      1042
      447)
    object Label2: TLabel
      Left = 8
      Top = 416
      Width = 25
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Judul'
    end
    object btnSimpan: TAdvGlowButton
      Left = 760
      Top = 408
      Width = 92
      Height = 30
      Anchors = [akRight, akBottom]
      Caption = '&Cetak Format 1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
        F80000019249444154484B6364A03160A4B1F90C582D9874EAB7FD7FC67F0EA4
        58CEF89FE9409E19EB41743D582D9878FA67FDFFFF0C0D2459C0C8D0906FCADE
        38022C707272AA5777896E507789212584186EEE5902C44B1BF6EDDB87124CF0
        38F0F4F46497909048F8F6ED5B049F8C8683B0921E4916BCBD7789E1D3931B07
        383939577EFDFA75E1EAD5ABBF830C805B909090B096919131882453712B5E37
        7FFEFC60140B1213137F03055828B1E096310703E37F0606D5733FFE002D6045
        B70028453D00B4001C3AF02002FA80B61604060652D582F5EBD7A3FA00983CA9
        6A0130B962B7E0CF9F3F0CC06446566470737333B0B040D2094E0B4086FFD10D
        65E077CC24C9928FFBA733B05C5ECD00B2049705F0640ACCC90C64E66498A3FE
        007D809A4C1D1D1D3700339A3F4805B2059D6B6EE2F589B59630838D9608ACA8
        00ABFDFFFFFFDAFDFBF787A024535051F1E3C78F78A02592C651550ED27AB60E
        A484D1D34B870F9C5DD6760068F8F3C78F1F2FBC73E7CE4F140B900D43AE0F70
        F900E672983EC641551FD0BCCA2425EC09A9A579AB0200EA53BC1998AD60B800
        00000049454E44AE426082}
      TabOrder = 0
      TabStop = True
      OnClick = btnSimpanClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object btnBatal: TAdvGlowButton
      Left = 961
      Top = 408
      Width = 73
      Height = 30
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = '&Batal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000024A49444154384FAD94C16BD3501CC7BF2F699BAD8975154477503CB8
        3127C8305B87C2C4D293FFC58E524174C80E0A7A109987E1D845D8C9B3FF839B
        456160BBA06C6363B307A5A03BE8DCEC9226B6C9F3BDCC84A48D8D03DF2590F7
        7E9FDFF7F7FBFEDE23F8CF8B74E351807C55D5DE1E20C9CF65812634ADC182D8
        56F48A047EBC715EEAB74ECC80D022634AE1506AC11116E4DAF76952AD5AEDD8
        0EE0FEB5910131915A050113D655BF456D3A7AACB4B21E3C1502BAB0646AFB28
        6D751C6728F37A65CB8BF181747838659C56F62841EF518084A299DE3950C8C6
        C62F1EE7037FE673CF04017743F2CF9C03AD7D0AF149C43F66D19CB2549EF281
        DC4DBD30D6081AC003D32F5EA2555E8675FF308FF4E829121379348A9370B637
        0389A8252F5664A6CE76157E51D574A64FD4DB4B951ECF22716502F6DA07C030
        208E5F3D4CF08089A1E1C9716CFB64A6A47D7381BBAA7A3CD527EE45F5AE67F6
        39C411D5DDB2B73661DE9A8C6CB1D0A467D36F2AB578E0933957990B5C7D0F73
        EA663C90B292F5A892FFF48C2B4343674A47D15A7E0BEBE1BD0EA8CC4A265EC9
        DC14A39033D8D71F66DF9477BC6771A6C09417CB8A6F0A4F77901F9F87406F07
        530B034370AA6C660306088317DA1C661194CE2B4B953BA1397407BB5FD90FAA
        FCC7016FC99F7715EF5E87AE5E3D3F769108247437E3A049625F925E696BDEB9
        8EC7C12CE4066D8A757605DD27ABCB6A31D8E5202C54723090975F3FA5CC8802
        8AED2D600A4C4AE982BCA34F7BF737181BF7C08AF5EB6AB6D5449A0765933050
        D27E7037FFA6BC2B30AE7F51FBBF01AD04E81511DF13380000000049454E44AE
        426082}
      TabOrder = 1
      TabStop = True
      OnClick = btnBatalClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 0
      Width = 1026
      Height = 402
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 2
      DesignSize = (
        1026
        402)
      object asgInput: TAdvStringGrid
        Left = 9
        Top = 12
        Width = 1014
        Height = 384
        Cursor = crDefault
        TabStop = False
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 17
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goTabs]
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
        OnMouseWheelDown = asgInputMouseWheelDown
        OnMouseWheelUp = asgInputMouseWheelUp
        OnGetAlignment = asgInputGetAlignment
        OnCanAddRow = asgInputCanAddRow
        OnCanDeleteRow = asgInputCanDeleteRow
        OnCanEditCell = asgInputCanEditCell
        OnGetFloatFormat = asgInputGetFloatFormat
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = []
        Bands.Active = True
        ColumnSize.Stretch = True
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        ControlLook.NoDisabledButtonLook = True
        ExcelStyleDecimalSeparator = True
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedFooters = 1
        FixedRowHeight = 22
        FixedColAlways = True
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = []
        FloatFormat = '%.2n'
        FloatingFooter.Visible = True
        Navigation.AllowDeleteRow = True
        Navigation.AdvanceOnEnter = True
        Navigation.AdvanceInsert = True
        Navigation.AutoComboDropSize = True
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'Tahoma'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'Tahoma'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'Tahoma'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'Tahoma'
        PrintSettings.FooterFont.Style = []
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'Tahoma'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurrence'
        SearchFooter.HintFindPrev = 'Find previous occurrence'
        SearchFooter.HintHighlight = 'Highlight occurrences'
        SearchFooter.MatchCaseCaption = 'Match case'
        ShowDesignHelper = False
        SizeWhileTyping.Width = True
        SortSettings.Column = 1
        SortSettings.NormalCellsOnly = True
        SortSettings.Row = -1
        VAlignment = vtaCenter
        Version = '6.0.4.2'
        WordWrap = False
        ColWidths = (
          64
          36
          40
          27
          50
          64
          46
          32
          30
          23
          64
          64
          162
          64
          464
          103
          234)
        RowHeights = (
          22
          22
          22
          22
          22
          22
          22
          22
          22
          22)
      end
    end
    object btnEkspor: TAdvGlowButton
      Left = 311
      Top = 408
      Width = 75
      Height = 30
      Anchors = [akLeft, akBottom]
      Caption = 'E&kspor'
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      Picture.Data = {
        89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
        F8000001FC49444154484B6364A03160A4B1F90C036F815E891EF75F4E260366
        4646BDFFFF190D803E36FEFFFFDFDA2B2D17DA89F13D8A0F342A35845999390D
        41863032301A3230FED7F9CFC0A0096433A118F69FA1E152CBB946922DD0AB35
        029A4718F04BF2338030360034E0E3D6C00D023039141F50C30290C15B0237C0
        CD25CB026B5D2B061B5D6B86E5375632446A84C33D72F9CD15862B6FAE12B6C0
        5BCF934156589661D5A9350CEFBEBE633055346130563062587F7623C3CB4F2F
        19321CD318321DD3197C3704326C0E580FB760D98D15604B09FAC052D9826146
        FC5486F98717324CDB3F836173FE7A86171F5F3024CC4961F80F84F8E280E820
        9A18D5CF60A268CCB0F0E8228674875486C8E9310CB75EDE06BB96621F800C91
        1592615897B39A81099842579C5AC5D0BDBD171E1414C701C824255145869599
        CB1818191919369FDFC2D0B8A9857A16800C9D9F3C87418C579461CBC56D0CA9
        F6C90C71B393182E3FB94C9D200A330B65A8F6A960A85E5BCBB0F7FA7E866D05
        9B185E7D7ECD10353396E1EFBFBF9447728C6514033B0B3BC3BC230B18FEFFFF
        CF60AD62C5A02BABC3B015E89BC7EF9E502792F115185489647C1650251F906B
        0130443F6E0DC251D82117D7C0F4A9C3C8F0DF90AAC5353657832A9CFF9CCC7A
        408BF481F58229B0743464F8FF6FF525722A1CC23501E92A06BE4E26DDCDA83A
        00FCFBFC193DEFE1C10000000049454E44AE426082}
      TabOrder = 3
      OnClick = btnEksporClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object txtJudul: TAdvEdit
      Left = 38
      Top = 412
      Width = 267
      Height = 21
      LabelFont.Charset = DEFAULT_CHARSET
      LabelFont.Color = clWindowText
      LabelFont.Height = -11
      LabelFont.Name = 'Tahoma'
      LabelFont.Style = []
      Lookup.Separator = ';'
      Anchors = [akLeft, akBottom]
      Color = clWindow
      TabOrder = 4
      Visible = True
      Version = '2.9.2.1'
    end
    object btnCetak2: TAdvGlowButton
      Left = 858
      Top = 408
      Width = 97
      Height = 30
      Anchors = [akRight, akBottom]
      Caption = 'Cetak &Format Trucking'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
        F80000019249444154484B6364A03160A4B1F90C582D9874EAB7FD7FC67F0EA4
        58CEF89FE9409E19EB41743D582D9878FA67FDFFFF0C0D2459C0C8D0906FCADE
        38022C707272AA5777896E507789212584186EEE5902C44B1BF6EDDB87124CF0
        38F0F4F46497909048F8F6ED5B049F8C8683B0921E4916BCBD7789E1D3931B07
        383939577EFDFA75E1EAD5ABBF830C805B909090B096919131882453712B5E37
        7FFEFC60140B1213137F03055828B1E096310703E37F0606D5733FFE002D6045
        B70028453D00B4001C3AF02002FA80B61604060652D582F5EBD7A3FA00983CA9
        6A0130B962B7E0CF9F3F0CC06446566470737333B0B040D2094E0B4086FFD10D
        65E077CC24C9928FFBA733B05C5ECD00B2049705F0640ACCC90C64E66498A3FE
        007D809A4C1D1D1D3700339A3F4805B2059D6B6EE2F589B59630838D9608ACA8
        00ABFDFFFFFFDAFDFBF787A024535051F1E3C78F78A02592C651550ED27AB60E
        A484D1D34B870F9C5DD6760068F8F3C78F1F2FBC73E7CE4F140B900D43AE0F70
        F900E672983EC641551FD0BCCA2425EC09A9A579AB0200EA53BC1998AD60B800
        00000049454E44AE426082}
      TabOrder = 5
      TabStop = True
      OnClick = btnCetak2Click
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
  end
end
