object frmAturKolom: TfrmAturKolom
  Left = 0
  Top = 0
  Caption = 'Atur Kolom'
  ClientHeight = 324
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 324
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = -8
    ExplicitTop = -8
    ExplicitWidth = 447
    ExplicitHeight = 202
    DesignSize = (
      514
      324)
    object btnSimpan: TAdvGlowButton
      Left = 8
      Top = 282
      Width = 65
      Height = 34
      Anchors = [akLeft, akBottom]
      Caption = '&Simpan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000013E49444154384F6364A03260A4B2790C70034D67DDB6FFFF9FC1E1CB
        8B7B603B3AED78F0DAD57C4B8CE1DF3F86EBE7325557212B841B6832F3763DC3
        7F8686AF5003DBB118D8DEDAC260636BC7606B67C7D002341004FE33FC2F389B
        A136116628490696951433B8BAB901B13BDC4090418C0C4CC1A73394D741D850
        408C0B71190834A5E14CBA6A235E03DB6CB919181951E3ACAEA69AC1D1C90988
        9D515C88D7C05F5F3F0003E63F83182723839E182BD688F9F99791E1E4072E84
        1C3E1792958CE86AE0E97415921C693AF30E286A71470AC8C0FB4F9F337CFCF2
        0DAFC13C9C1C0C2A72D20C4419488A13893290EA2EFCFEF327C3DFBFFFF03A94
        9989898193839D382F7FFDFE83E1CF9FBFF80D646662E0E1E2246CE0BE044552
        8290C169C17DECB10C2BBE48320DAA1898430F9C4E533D889297C931089B1EAA
        97D800DC86C11578169C520000000049454E44AE426082}
      TabOrder = 0
      TabStop = True
      OnClick = btnSimpanClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
      ExplicitTop = 160
    end
    object btnBatal: TAdvGlowButton
      Left = 79
      Top = 282
      Width = 65
      Height = 34
      Anchors = [akLeft, akBottom]
      Cancel = True
      Caption = '&Batal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      Picture.Data = {
        89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
        0D0000024A49444154384FAD94C16BD3501CC7BF2F699BAD8975154477503CB8
        3127C8305B87C2C4D293FFC58E524174C80E0A7A109987E1D845D8C9B3FF839B
        456160BBA06C6363B307A5A03BE8DCEC9226B6C9F3BDCC84A48D8D03DF2590F7
        7E9FDFF7F7FBFEDE23F8CF8B74E351807C55D5DE1E20C9CF65812634ADC182D8
        56F48A047EBC715EEAB74ECC80D022634AE1506AC11116E4DAF76952AD5AEDD8
        0EE0FEB5910131915A050113D655BF456D3A7AACB4B21E3C1502BAB0646AFB28
        6D751C6728F37A65CB8BF181747838659C56F62841EF518084A299DE3950C8C6
        C62F1EE7037FE673CF04017743F2CF9C03AD7D0AF149C43F66D19CB2549EF281
        DC4DBD30D6081AC003D32F5EA2555E8675FF308FF4E829121379348A9370B637
        0389A8252F5664A6CE76157E51D574A64FD4DB4B951ECF22716502F6DA07C030
        208E5F3D4CF08089A1E1C9716CFB64A6A47D7381BBAA7A3CD527EE45F5AE67F6
        39C411D5DDB2B73661DE9A8C6CB1D0A467D36F2AB578E0933957990B5C7D0F73
        EA663C90B292F5A892FFF48C2B4343674A47D15A7E0BEBE1BD0EA8CC4A265EC9
        DC14A39033D8D71F66DF9477BC6771A6C09417CB8A6F0A4F77901F9F87406F07
        530B034370AA6C660306088317DA1C661194CE2B4B953BA1397407BB5FD90FAA
        FCC7016FC99F7715EF5E87AE5E3D3F769108247437E3A049625F925E696BDEB9
        8EC7C12CE4066D8A757605DD27ABCB6A31D8E5202C54723090975F3FA5CC8802
        8AED2D600A4C4AE982BCA34F7BF737181BF7C08AF5EB6AB6D5449A0765933050
        D27E7037FFA6BC2B30AE7F51FBBF01AD04E81511DF13380000000049454E44AE
        426082}
      TabOrder = 1
      TabStop = True
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
      ExplicitTop = 160
    end
    object asgInput: TAdvStringGrid
      Left = 8
      Top = 7
      Width = 500
      Height = 269
      Cursor = crDefault
      Anchors = [akLeft, akTop, akRight, akBottom]
      ColCount = 50
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 2
      OnGetAlignment = asgInputGetAlignment
      OnCanAddRow = asgInputCanAddRow
      OnCanDeleteRow = asgInputCanDeleteRow
      OnCanSort = asgInputCanSort
      OnCanEditCell = asgInputCanEditCell
      OnGetEditorType = asgInputGetEditorType
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Tahoma'
      ActiveCellFont.Style = [fsBold]
      ColumnHeaders.Strings = (
        'Kolom'
        'Muncul/Tidak')
      ColumnSize.Stretch = True
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDownClear = '(All)'
      FixedRowHeight = 22
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = [fsBold]
      FloatFormat = '%.2f'
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Tahoma'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Tahoma'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Tahoma'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Tahoma'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollWidth = 16
      SearchFooter.FindNextCaption = 'Find &next'
      SearchFooter.FindPrevCaption = 'Find &previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Tahoma'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurrence'
      SearchFooter.HintFindPrev = 'Find previous occurrence'
      SearchFooter.HintHighlight = 'Highlight occurrences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SearchFooter.ShowClose = False
      SearchFooter.ShowHighLight = False
      VAlignment = vtaCenter
      Version = '6.0.4.2'
      WordWrap = False
      ExplicitWidth = 433
      ExplicitHeight = 147
      ColWidths = (
        64
        64
        64
        64
        64
        64
        64
        64
        69
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64
        64)
      RowHeights = (
        22
        4
        22
        22
        22
        22
        22
        22
        22
        22)
    end
  end
end
