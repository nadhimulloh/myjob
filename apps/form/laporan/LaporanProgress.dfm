object frmLaporanProgress: TfrmLaporanProgress
  Left = 342
  Top = 219
  Caption = 'frmLaporanProgress'
  ClientHeight = 427
  ClientWidth = 923
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 923
    Height = 427
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 921
      Height = 425
      Align = alClient
      TabOrder = 0
      DesignSize = (
        921
        425)
      object AdvProgressBar1: TAdvProgressBar
        Left = 786
        Top = 404
        Width = 128
        Height = 15
        Anchors = [akRight, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Verdana'
        Font.Style = []
        Level0ColorTo = 14811105
        Level1ColorTo = 13303807
        Level2Color = 5483007
        Level2ColorTo = 11064319
        Level3ColorTo = 13290239
        Level1Perc = 70
        Level2Perc = 90
        Position = 50
        ShowBorder = True
        Version = '1.2.0.2'
        ExplicitLeft = 569
        ExplicitTop = 248
      end
      object Shape1: TShape
        Left = 7
        Top = 382
        Width = 13
        Height = 11
        Anchors = [akLeft, akBottom]
        Brush.Color = clBlack
        ExplicitTop = 316
      end
      object Shape2: TShape
        Left = 62
        Top = 382
        Width = 13
        Height = 11
        Anchors = [akLeft, akBottom]
        Brush.Color = clRed
        ExplicitTop = 316
      end
      object Label1: TLabel
        Left = 26
        Top = 381
        Width = 30
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Active'
        ExplicitTop = 315
      end
      object Label2: TLabel
        Left = 81
        Top = 381
        Width = 39
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'Inactive'
        ExplicitTop = 315
      end
      object btnEkspor: TAdvGlowButton
        Left = 168
        Top = 7
        Width = 75
        Height = 36
        Caption = 'E&kspor'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001FC49444154484B6364A03160A4B1F90C036F815E891EF75F4E260366
          4646BDFFFF190D803E36FEFFFFDFDA2B2D17DA89F13D8A0F342A35845999390D
          41863032301A3230FED7F9CFC0A0096433A118F69FA1E152CBB946922DD0AB35
          029A4718F04BF2338030360034E0E3D6C00D023039141F50C30290C15B0237C0
          CD25CB026B5D2B061B5D6B86E5375632446A84C33D72F9CD15862B6FAE12B6C0
          5BCF934156589661D5A9350CEFBEBE633055346130563062587F7623C3CB4F2F
          19321CD318321DD3197C3704326C0E580FB760D98D15604B09FAC052D9826146
          FC5486F98717324CDB3F836173FE7A86171F5F3024CC4961F80F84F8E280E820
          9A18D5CF60A268CCB0F0E8228674875486C8E9310CB75EDE06BB96621F800C91
          1592615897B39A81099842579C5AC5D0BDBD171E1414C701C824255145869599
          CB1818191919369FDFC2D0B8A9857A16800C9D9F3C87418C579461CBC56D0CA9
          F6C90C71B393182E3FB94C9D200A330B65A8F6A960A85E5BCBB0F7FA7E866D05
          9B185E7D7ECD10353396E1EFBFBF9447728C6514033B0B3BC3BC230B18FEFFFF
          CF60AD62C5A02BABC3B015E89BC7EF9E502792F115185489647C1650251F906B
          0130443F6E0DC251D82117D7C0F4A9C3C8F0DF90AAC5353657832A9CFF9CCC7A
          408BF481F58229B0743464F8FF6FF525722A1CC23501E92A06BE4E26DDCDA83A
          00FCFBFC193DEFE1C10000000049454E44AE426082}
        TabOrder = 0
        OnClick = btnEksporClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnCetak: TAdvGlowButton
        Left = 249
        Top = 7
        Width = 75
        Height = 36
        Caption = '&Cetak'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000019249444154484B6364A03160A4B1F90C582D9874EAB7FD7FC67F0EA4
          58CEF89FE9409E19EB41743D582D9878FA67FDFFFF0C0D2459C0C8D0906FCADE
          38022C707272AA5777896E507789212584186EEE5902C44B1BF6EDDB87124CF0
          38F0F4F46497909048F8F6ED5B049F8C8683B0921E4916BCBD7789E1D3931B07
          383939577EFDFA75E1EAD5ABBF830C805B909090B096919131882453712B5E37
          7FFEFC60140B1213137F03055828B1E096310703E37F0606D5733FFE002D6045
          B70028453D00B4001C3AF02002FA80B61604060652D582F5EBD7A3FA00983CA9
          6A0130B962B7E0CF9F3F0CC06446566470737333B0B040D2094E0B4086FFD10D
          65E077CC24C9928FFBA733B05C5ECD00B2049705F0640ACCC90C64E66498A3FE
          007D809A4C1D1D1D3700339A3F4805B2059D6B6EE2F589B59630838D9608ACA8
          00ABFDFFFFFFDAFDFBF787A024535051F1E3C78F78A02592C651550ED27AB60E
          A484D1D34B870F9C5DD6760068F8F3C78F1F2FBC73E7CE4F140B900D43AE0F70
          F900E672983EC641551FD0BCCA2425EC09A9A579AB0200EA53BC1998AD60B800
          00000049454E44AE426082}
        TabOrder = 1
        OnClick = btnCetakClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object asgRekap: TAdvStringGrid
        Left = 7
        Top = 123
        Width = 907
        Height = 277
        Cursor = crDefault
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColCount = 17
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 2
        OnMouseWheelDown = asgRekapMouseWheelDown
        OnMouseWheelUp = asgRekapMouseWheelUp
        OnGetCellColor = asgRekapGetCellColor
        OnGetCellPrintColor = asgRekapGetCellPrintColor
        OnGetAlignment = asgRekapGetAlignment
        OnClickCell = asgRekapClickCell
        OnDblClickCell = asgRekapDblClickCell
        OnGetFloatFormat = asgRekapGetFloatFormat
        OnSelectionChanged = asgRekapSelectionChanged
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = []
        ColumnHeaders.Strings = (
          'No.'
          'Customer'
          'Nama Pekerjaan'
          'Deskripsi'
          'Tgl. Mulai'
          'Tgl. Target'
          'Tgl. Jual'
          'Total Jual'
          'Tgl. Beli'
          'Total Beli'
          'Tgl. Invoice'
          'No. Invoice'
          'Tgl. Bayar Cust.'
          'Vendor'
          'Tgl. Bayar Vendor'
          'Seq')
        ColumnSize.Stretch = True
        ControlLook.FixedGradientHoverFrom = clGray
        ControlLook.FixedGradientHoverTo = clWhite
        ControlLook.FixedGradientDownFrom = clGray
        ControlLook.FixedGradientDownTo = clSilver
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'Tahoma'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'Tahoma'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedFooters = 1
        FixedRowHeight = 22
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'Tahoma'
        FixedFont.Style = []
        FloatFormat = '%.2f'
        FloatingFooter.Visible = True
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'Tahoma'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'Tahoma'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'Tahoma'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'Tahoma'
        PrintSettings.FooterFont.Style = []
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'Tahoma'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurrence'
        SearchFooter.HintFindPrev = 'Find previous occurrence'
        SearchFooter.HintHighlight = 'Highlight occurrences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SearchFooter.ShowClose = False
        SearchFooter.ShowHighLight = False
        VAlignment = vtaCenter
        Version = '6.0.4.2'
        WordWrap = False
        ColWidths = (
          64
          64
          64
          64
          64
          64
          64
          64
          69
          64
          64
          64
          64
          64
          64
          64
          64)
        RowHeights = (
          22
          22
          22
          22
          22
          22
          22
          22
          22
          22)
      end
      object btnLoad: TAdvGlowButton
        Left = 6
        Top = 7
        Width = 75
        Height = 36
        Caption = '&Load'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000008B49444154484B6364A03160A4B1F90CF4B7E0FCA1E7DE4CCC4C26E4
          F8ECDFDF7F670CED24B722EBC5F0C1C5C32F7E303032B2936301D0B01F7A36E2
          9CF82D38F2F23F3986C3F4E8DB88A3381AD30734B780D64174E9C82BAFFF8CFF
          4DC90926C6FF8CA7F56CC4B6E18D83D154349A8AF026AED1544454DE1B4D4594
          A722A2029A0445F4AF9349701C514A0179396619DA9374400000000049454E44
          AE426082}
        TabOrder = 3
        OnClick = btnLoadClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnReset: TAdvGlowButton
        Left = 87
        Top = 7
        Width = 75
        Height = 36
        Caption = '&Reset'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000001A949444154484BC594CF4B025110C767B414CA4DA32890ACD5FA1322
          E8187429F05AA7AE950A42A78E6DB7A04B1DDCF0DA293C06D929BA17FD079A4B
          BF0E1A62AE4684F97A6B4AFEDA1D5793DEF5CD7C3F33EF3B6F10FA7CB0CFFAD0
          33602AF236F7147226F40AED19209EA87BC07046490F6F8284A566D09F009081
          C4805D16D9C75A263451A887B40262CC26660A8788B0C10347CD78C418DC592D
          B09A0C08E95A5E0B408CA8C75C3C6C46B82196C1552A242CEB036435CBA9A62A
          FF05E08D0599DFB003AFACB25A82B5E47026C29837EA4633B9E20162BC587E5F
          273DA807A4820239043F00F4A4D2B7DB202DD1535419BBEA5102C23EE585279A
          9B7DDC7225FBF60FA802C827A004A87B43C0B49CF33D045DF79488D1B3B60748
          D703DEC979998FD373273E180D460BC01D6543F6927A06887E86205100DF41D6
          C9460673B52E9B27AF01E08EE6C7ED5F7001800BD4B3B4BBE71D6795A030A6BB
          8B44391F47C0956EC4B51C0E38E2801D5D80D681AD8CE7FC672E9A816895F3F8
          5325EDD8E52BFBD3709B9AF5802AA4FD14C598557C55656EF40B657277806A16
          B5062871EDFE7F7F7227155231DF32F49D19DFE1983D0000000049454E44AE42
          6082}
        TabOrder = 4
        OnClick = btnResetClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object chbSF: TAdvOfficeCheckBox
        Left = 6
        Top = 401
        Width = 157
        Height = 20
        Anchors = [akLeft, akBottom]
        TabOrder = 5
        OnClick = chbSFClick
        Alignment = taLeftJustify
        Caption = 'Tampilkan Kolom Pencarian'
        ReturnIsTab = False
        Version = '1.3.4.1'
      end
      object GroupBox1: TGroupBox
        Left = 7
        Top = 45
        Width = 907
        Height = 73
        Anchors = [akLeft, akTop, akRight]
        Caption = 'Filter'
        TabOrder = 6
        object Label4: TLabel
          Left = 392
          Top = 47
          Width = 48
          Height = 13
          Caption = 'Unit Bisnis'
          Transparent = True
          Visible = False
        end
        object Label19: TLabel
          Left = 160
          Top = 20
          Width = 46
          Height = 13
          Caption = 'Customer'
        end
        object Label5: TLabel
          Left = 8
          Top = 47
          Width = 34
          Height = 13
          Caption = 'Sampai'
        end
        object Label6: TLabel
          Left = 8
          Top = 20
          Width = 19
          Height = 13
          Caption = 'Dari'
        end
        object Label3: TLabel
          Left = 405
          Top = 20
          Width = 34
          Height = 13
          Caption = 'Vendor'
          Visible = False
        end
        object Label7: TLabel
          Left = 160
          Top = 47
          Width = 55
          Height = 13
          Caption = 'Stat. Bayar'
          Transparent = True
        end
        object cmbDes: TComboBox
          Left = 446
          Top = 43
          Width = 143
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          Visible = False
          Items.Strings = (
            'Semua'
            'Logistic'
            'General Services'
            'Construction')
        end
        object btnCust: TAdvGlowButton
          Left = 370
          Top = 16
          Width = 21
          Height = 20
          Caption = '...'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          TabOrder = 1
          OnClick = btnCustClick
          Appearance.ColorChecked = 16111818
          Appearance.ColorCheckedTo = 16367008
          Appearance.ColorDisabled = 15921906
          Appearance.ColorDisabledTo = 15921906
          Appearance.ColorDown = 16111818
          Appearance.ColorDownTo = 16367008
          Appearance.ColorHot = 16117985
          Appearance.ColorHotTo = 16372402
          Appearance.ColorMirrorHot = 16107693
          Appearance.ColorMirrorHotTo = 16775412
          Appearance.ColorMirrorDown = 16102556
          Appearance.ColorMirrorDownTo = 16768988
          Appearance.ColorMirrorChecked = 16102556
          Appearance.ColorMirrorCheckedTo = 16768988
          Appearance.ColorMirrorDisabled = 11974326
          Appearance.ColorMirrorDisabledTo = 15921906
        end
        object txtCust: TAdvEdit
          Left = 221
          Top = 15
          Width = 143
          Height = 21
          EditType = etUppercase
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          MaxLength = 50
          ReadOnly = True
          TabOrder = 2
          Visible = True
          Version = '2.9.2.1'
        end
        object dtpAwal: TDateTimePicker
          Left = 55
          Top = 16
          Width = 89
          Height = 21
          Date = 41127.626447476840000000
          Time = 41127.626447476840000000
          TabOrder = 3
        end
        object dtpAkhir: TDateTimePicker
          Left = 55
          Top = 43
          Width = 89
          Height = 21
          Date = 41127.626776620360000000
          Time = 41127.626776620360000000
          TabOrder = 4
        end
        object txtVendor: TAdvEdit
          Left = 445
          Top = 16
          Width = 143
          Height = 21
          EditType = etUppercase
          ReturnIsTab = True
          LabelFont.Charset = DEFAULT_CHARSET
          LabelFont.Color = clWindowText
          LabelFont.Height = -11
          LabelFont.Name = 'Tahoma'
          LabelFont.Style = []
          Lookup.Separator = ';'
          Color = clWindow
          MaxLength = 50
          ReadOnly = True
          TabOrder = 5
          Visible = False
          Version = '2.9.2.1'
        end
        object btnVendor: TAdvGlowButton
          Left = 594
          Top = 16
          Width = 21
          Height = 20
          Caption = '...'
          NotesFont.Charset = DEFAULT_CHARSET
          NotesFont.Color = clWindowText
          NotesFont.Height = -11
          NotesFont.Name = 'Tahoma'
          NotesFont.Style = []
          TabOrder = 6
          Visible = False
          OnClick = btnVendorClick
          Appearance.ColorChecked = 16111818
          Appearance.ColorCheckedTo = 16367008
          Appearance.ColorDisabled = 15921906
          Appearance.ColorDisabledTo = 15921906
          Appearance.ColorDown = 16111818
          Appearance.ColorDownTo = 16367008
          Appearance.ColorHot = 16117985
          Appearance.ColorHotTo = 16372402
          Appearance.ColorMirrorHot = 16107693
          Appearance.ColorMirrorHotTo = 16775412
          Appearance.ColorMirrorDown = 16102556
          Appearance.ColorMirrorDownTo = 16768988
          Appearance.ColorMirrorChecked = 16102556
          Appearance.ColorMirrorCheckedTo = 16768988
          Appearance.ColorMirrorDisabled = 11974326
          Appearance.ColorMirrorDisabledTo = 15921906
        end
        object cmbStat: TComboBox
          Left = 221
          Top = 42
          Width = 143
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 7
          Items.Strings = (
            'Semua'
            'Open'
            'Close')
        end
      end
      object btnCetakInv: TAdvGlowButton
        Left = 846
        Top = 7
        Width = 75
        Height = 36
        Caption = 'Cet&ak Progress'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000019249444154484B6364A03160A4B1F90C582D9874EAB7FD7FC67F0EA4
          58CEF89FE9409E19EB41743D582D9878FA67FDFFFF0C0D2459C0C8D0906FCADE
          38022C707272AA5777896E507789212584186EEE5902C44B1BF6EDDB87124CF0
          38F0F4F46497909048F8F6ED5B049F8C8683B0921E4916BCBD7789E1D3931B07
          383939577EFDFA75E1EAD5ABBF830C805B909090B096919131882453712B5E37
          7FFEFC60140B1213137F03055828B1E096310703E37F0606D5733FFE002D6045
          B70028453D00B4001C3AF02002FA80B61604060652D582F5EBD7A3FA00983CA9
          6A0130B962B7E0CF9F3F0CC06446566470737333B0B040D2094E0B4086FFD10D
          65E077CC24C9928FFBA733B05C5ECD00B2049705F0640ACCC90C64E66498A3FE
          007D809A4C1D1D1D3700339A3F4805B2059D6B6EE2F589B59630838D9608ACA8
          00ABFDFFFFFFDAFDFBF787A024535051F1E3C78F78A02592C651550ED27AB60E
          A484D1D34B870F9C5DD6760068F8F3C78F1F2FBC73E7CE4F140B900D43AE0F70
          F900E672983EC641551FD0BCCA2425EC09A9A579AB0200EA53BC1998AD60B800
          00000049454E44AE426082}
        TabOrder = 7
        Visible = False
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnAtur: TAdvGlowButton
        Left = 330
        Top = 7
        Width = 75
        Height = 36
        Caption = '&Atur Kolom'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000008249444154484B6364A03160A4B1F90CF4B5C07981FB7F627CE4ABB5
          09AFB2023376B8C3517C40370BF626ECC41A743007E0929F70EA27380408FA60
          E85BE0A7BDA9015B2C6EBAEA0716C725FFFF3F03589E6010E14A259BAFF981ED
          A53815D1DC07A3913C1AC90CA34505C12A81E29C4CC806B22D20643039F2F4AD
          93C97121213D00B13EAA1963B9837C0000000049454E44AE426082}
        TabOrder = 8
        OnClick = btnAturClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnUpd: TAdvGlowButton
        Left = 411
        Top = 7
        Width = 94
        Height = 36
        Caption = 'Update Pembayaran Customer'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000003FE49444154484BED555D4C5C4514FEE6CECCBD77772FBBC5854DF95B
          2A58623784624585A64002368D4D4335364D1A6D62AA6D4D9F34261A4DB40F8D
          BEF4C198106B6CE293011B0D3131228556AC98A2264516C1501453C44AD805BA
          B20BFB77F75E676E4B4D0B411FDA0713CF64F69E3DF7CCF9CE7CF3CD2EC15D36
          7297EBE37F807F64D8A14861EC25CB323DD213435138E58432AE6854239CE954
          652EC2A54F5D0AA33A615403555C8429AA4214E96B4421DC56A0124238210A4B
          4E47138989998004D00B77D5260A5BAB295114A723DBB661A5B2C8A532B0D226
          ECAC092B9B034C0B76CE0211AB6CE18B44275FBE1385A107FDE0FE3C2716E90D
          E7A2BD6126014A8BF7D5FF9C5F5FA52F5DFE03DE580E1E9F01EED2A01A2EA86E
          1D5C53C175156227A09C3905E453740D89C66EC4863EBD80799281515D8AB9FE
          315B00C80C84CA0E360D31AF5B0B322FB6ED6F59C5AB99C9A2FF5417E6166360
          26D0FCD46E04AACAD6E47F767C0AE73B3F87CD142BD233CC25405DF9A1968BB9
          788A3FF6F45E18053E9C7DBB03D7627FA2F1C99D28A9B90F63DD83F8351B8316
          F039F4C5FBC6B0EFF81107E0BB0F7B30393EE9E4963D50E5C4CEBC791AA66DE5
          225FFCE096000DE547760E206DD2A6D61D28A82C41E7EBEFC2531BC426EE434D
          5B2386BBBEC255B709EE733B05E67B4770E0F8F38EFFD11BA7E0DE1A44B96260
          EB13CDD76327DE87A5203BDB3DB44102D4070FB70E909CCD1A9B1A50B8B90C72
          9B91896984763780328AE585457CF64E078CFA4A64228BF0A7289A8E3EEE14BB
          1AFE05F3576650BD673B144A9D58E789F760117273070F0A8A06CD7892B7B43D
          8A828A9235B94DCCC530F9CD087C457E943F1C7254B3964D5FBA8C8B5F7F0B33
          BE6C45CE86A9CCAA2C39B063D45B13D4130313280E041C65D8625896FC14C312
          F2148AB4E4530C6912800A5933CA206E0E2C21DF743AE5A8C8B7AD02D1DEE14C
          A46F4493001B36B6D5CDFA9B42AA5C985D482039F61B68BE076A59A1D0BBD0FF
          0D2354C1CABCBD7B71F99CC6562CDA175E9AFB72D49011A215E49D1607183293
          99D0D1CDF0BEB8DD20E35113CFF6678087EEFF7BA1E8525E3439D7B3F84FBFE7
          96A7A22F58E96CFB2D44EA4C7975EAB5D2B70CF5FA8D7EA57BC16AFF3E715280
          7BB4026F19CF372AA84B2D55349EC7F2F40CF77918F3BA5431413DDACD7311FA
          4F46CF8D3892BB05C0D0946383C78ADA2BFC9C5882F497BBAFD90357D21F8CCE
          A49FBBAD632EBE1789594C5DAE4DE2B48A90CDE6739F710FF77B4A72F1747172
          66E191550022A0D714693DBBAAF4E69469E3505D1E3A8697E2272FC4BCEB72B2
          CECB555A73ABD8DB7570E3278DF7EACE8FCE3367A2E18F7F5CAABD630092B62D
          01DEBE678BFB702C65C53B2EC5F72F99387F2701566AC9935E5F2EFF02F5BFFF
          9FFC17DF2F6E40B95516600000000049454E44AE426082}
        TabOrder = 9
        OnClick = btnUpdClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnBatal: TAdvGlowButton
        Left = 511
        Top = 7
        Width = 94
        Height = 36
        Caption = 'Batal Pembayaran Customer'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000023E49444154484B6364A03160C466FEA453BFEDFF33FE7320C7EE7C53
          F646647D582D9878FA67FDFFFF0C0DE4585060C68E6226410B1EBDFEC600C2F8
          809C281703088300C9161CB9F686E1E8B5B7782DB0D61266B0D11221CF029AFB
          80D478A0388840C10102C8C146541049945DAE6764F80F4E39C81AA8E6035C16
          A047F2F0F001331323C3D71F7F18B4E5F81944F9D918589899E021893392498D
          03D9EBC718748FAC64107D72838101985D5F4B6B305CB60967088E77C49E9309
          C5010BD0D52097473BCA31789C5CC460B87F11D6F8FFFFFF7F0BEFBED3B53049
          B86DC4FA00E472F745E50C0CACAC0C6CA9B90C2CAE5E0CFF3FBE67F89E97CCC0
          F0E913C85C6039C9E0CBBDFFD4561087A005C8A92806E8FAD475150C52F7CE31
          B0651531B0068533FCFF0034BC249BE1FF83BB0C0CBC7C0C0C9F8196FC67D8C7
          B3EF94335116A087437C831B03EBCFEF0C5CEBF73030FCFD03379C515E9181A3
          BE83E17B5238C882CF400B80B691E883C2005586D4164FA005DF1838E7AF66F8
          D158017639C870CE9E690C0C2C2C0CDF025D41E67EE2D97B8A1FC502C9D28B55
          0C8C8CAD20413B6D11064B4D4891800EBC66E7838388810FA8FFD347B8E18C82
          420CBFD72E67F8357D02F620922ABEA4F19F9961172F278B6CB8AD2C83301F1B
          560BE0910C92058639E7C4D90C20C3FFECDACAF06BF65406863FBFB147322965
          CE6727D3664646C66AE42086EA07A6D2FFAD589329291680D47E7534F3FECFC8
          50F49F81C19411584A02E119C6FF0C7DB0E489910F48B58058F558EB64623513
          A30E0077BB2428BF6E4A580000000049454E44AE426082}
        TabOrder = 10
        OnClick = btnBatalClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnBatalV: TAdvGlowButton
        Left = 711
        Top = 7
        Width = 94
        Height = 36
        Caption = 'Batal Pembayaran Vendor'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F80000023E49444154484B6364A03160C466FEA453BFEDFF33FE7320C7EE7C53
          F646647D582D9878FA67FDFFFF0C0DE4585060C68E6226410B1EBDFEC600C2F8
          809C281703088300C9161CB9F686E1E8B5B7782DB0D61266B0D11221CF029AFB
          80D478A0388840C10102C8C146541049945DAE6764F80F4E39C81AA8E6035C16
          A047F2F0F001331323C3D71F7F18B4E5F81944F9D918589899E021893392498D
          03D9EBC718748FAC64107D72838101985D5F4B6B305CB60967088E77C49E9309
          C5010BD0D52097473BCA31789C5CC460B87F11D6F8FFFFFF7F0BEFBED3B53049
          B86DC4FA00E472F745E50C0CACAC0C6CA9B90C2CAE5E0CFF3FBE67F89E97CCC0
          F0E913C85C6039C9E0CBBDFFD4561087A005C8A92806E8FAD475150C52F7CE31
          B0651531B0068533FCFF0034BC249BE1FF83BB0C0CBC7C0C0C9F8196FC67D8C7
          B3EF94335116A087437C831B03EBCFEF0C5CEBF73030FCFD03379C515E9181A3
          BE83E17B5238C882CF400B80B691E883C2005586D4164FA005DF1838E7AF66F8
          D158017639C870CE9E690C0C2C2C0CDF025D41E67EE2D97B8A1FC502C9D28B55
          0C8C8CAD20413B6D11064B4D4891800EBC66E7838388810FA8FFD347B8E18C82
          420CBFD72E67F8357D02F620922ABEA4F19F9961172F278B6CB8AD2C83301F1B
          560BE0910C92058639E7C4D90C20C3FFECDACAF06BF65406863FBFB147322965
          CE6727D3664646C66AE42086EA07A6D2FFAD589329291680D47E7534F3FECFC8
          50F49F81C19411584A02E119C6FF0C7DB0E489910F48B58058F558EB64623513
          A30E0077BB2428BF6E4A580000000049454E44AE426082}
        TabOrder = 11
        OnClick = btnBatalVClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
      object btnUpdV: TAdvGlowButton
        Left = 611
        Top = 7
        Width = 94
        Height = 36
        Caption = 'Update Pembayaran Vendor'
        NotesFont.Charset = DEFAULT_CHARSET
        NotesFont.Color = clWindowText
        NotesFont.Height = -11
        NotesFont.Name = 'Tahoma'
        NotesFont.Style = []
        Picture.Data = {
          89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
          F8000003FE49444154484BED555D4C5C4514FEE6CECCBD77772FBBC5854DF95B
          2A58623784624585A64002368D4D4335364D1A6D62AA6D4D9F34261A4DB40F8D
          BEF4C198106B6CE293011B0D3131228556AC98A2264516C1501453C44AD805BA
          B20BFB77F75E676E4B4D0B411FDA0713CF64F69E3DF7CCF9CE7CF3CD2EC15D36
          7297EBE37F807F64D8A14861EC25CB323DD213435138E58432AE6854239CE954
          652EC2A54F5D0AA33A615403555C8429AA4214E96B4421DC56A0124238210A4B
          4E47138989998004D00B77D5260A5BAB295114A723DBB661A5B2C8A532B0D226
          ECAC092B9B034C0B76CE0211AB6CE18B44275FBE1385A107FDE0FE3C2716E90D
          E7A2BD6126014A8BF7D5FF9C5F5FA52F5DFE03DE580E1E9F01EED2A01A2EA86E
          1D5C53C175156227A09C3905E453740D89C66EC4863EBD80799281515D8AB9FE
          315B00C80C84CA0E360D31AF5B0B322FB6ED6F59C5AB99C9A2FF5417E6166360
          26D0FCD46E04AACAD6E47F767C0AE73B3F87CD142BD233CC25405DF9A1968BB9
          788A3FF6F45E18053E9C7DBB03D7627FA2F1C99D28A9B90F63DD83F8351B8316
          F039F4C5FBC6B0EFF81107E0BB0F7B30393EE9E4963D50E5C4CEBC791AA66DE5
          225FFCE096000DE547760E206DD2A6D61D28A82C41E7EBEFC2531BC426EE434D
          5B2386BBBEC255B709EE733B05E67B4770E0F8F38EFFD11BA7E0DE1A44B96260
          EB13CDD76327DE87A5203BDB3DB44102D4070FB70E909CCD1A9B1A50B8B90C72
          9B91896984763780328AE585457CF64E078CFA4A64228BF0A7289A8E3EEE14BB
          1AFE05F3576650BD673B144A9D58E789F760117273070F0A8A06CD7892B7B43D
          8A828A9235B94DCCC530F9CD087C457E943F1C7254B3964D5FBA8C8B5F7F0B33
          BE6C45CE86A9CCAA2C39B063D45B13D4130313280E041C65D8625896FC14C312
          F2148AB4E4530C6912800A5933CA206E0E2C21DF743AE5A8C8B7AD02D1DEE14C
          A46F4493001B36B6D5CDFA9B42AA5C985D482039F61B68BE076A59A1D0BBD0FF
          0D2354C1CABCBD7B71F99CC6562CDA175E9AFB72D49011A215E49D1607183293
          99D0D1CDF0BEB8DD20E35113CFF6678087EEFF7BA1E8525E3439D7B3F84FBFE7
          96A7A22F58E96CFB2D44EA4C7975EAB5D2B70CF5FA8D7EA57BC16AFF3E715280
          7BB4026F19CF372AA84B2D55349EC7F2F40CF77918F3BA5431413DDACD7311FA
          4F46CF8D3892BB05C0D0946383C78ADA2BFC9C5882F497BBAFD90357D21F8CCE
          A49FBBAD632EBE1789594C5DAE4DE2B48A90CDE6739F710FF77B4A72F1747172
          66E191550022A0D714693DBBAAF4E69469E3505D1E3A8697E2272FC4BCEB72B2
          CECB555A73ABD8DB7570E3278DF7EACE8FCE3367A2E18F7F5CAABD630092B62D
          01DEBE678BFB702C65C53B2EC5F72F99387F2701566AC9935E5F2EFF02F5BFFF
          9FFC17DF2F6E40B95516600000000049454E44AE426082}
        TabOrder = 12
        OnClick = btnUpdVClick
        Appearance.ColorChecked = 16111818
        Appearance.ColorCheckedTo = 16367008
        Appearance.ColorDisabled = 15921906
        Appearance.ColorDisabledTo = 15921906
        Appearance.ColorDown = 16111818
        Appearance.ColorDownTo = 16367008
        Appearance.ColorHot = 16117985
        Appearance.ColorHotTo = 16372402
        Appearance.ColorMirrorHot = 16107693
        Appearance.ColorMirrorHotTo = 16775412
        Appearance.ColorMirrorDown = 16102556
        Appearance.ColorMirrorDownTo = 16768988
        Appearance.ColorMirrorChecked = 16102556
        Appearance.ColorMirrorCheckedTo = 16768988
        Appearance.ColorMirrorDisabled = 11974326
        Appearance.ColorMirrorDisabledTo = 15921906
      end
    end
  end
end
