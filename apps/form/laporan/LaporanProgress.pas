unit LaporanProgress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls;

type
  TfrmLaporanProgress = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label19: TLabel;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    txtVendor: TAdvEdit;
    btnVendor: TAdvGlowButton;
    btnCetakInv: TAdvGlowButton;
    btnAtur: TAdvGlowButton;
    btnUpd: TAdvGlowButton;
    cmbStat: TComboBox;
    Label7: TLabel;
    btnBatal: TAdvGlowButton;
    btnBatalV: TAdvGlowButton;
    btnUpdV: TAdvGlowButton;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnVendorClick(Sender: TObject);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure btnAturClick(Sender: TObject);
    procedure btnUpdClick(Sender: TObject);
    procedure btnBatalClick(Sender: TObject);
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapSelectionChanged(Sender: TObject; ALeft, ATop,
      ARight, ABottom: Integer);
    procedure btnUpdVClick(Sender: TObject);
    procedure btnBatalVClick(Sender: TObject);
  private
    seqCard,seqVendor, AMenuid : integer;
    datenow: TDate;
    listHideCol : Tstringlist;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
    function isbatal : boolean;
    function isbatalVendor : boolean;

  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmLaporanProgress: TfrmLaporanProgress;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, StrUtils, LOV, AturKolom, BAST, BAUT,
  sUpDown, UpdateTglByr;

{$R *.dfm}

const
  ColNo          = 0;
  colCust        = 1;
  colNama        = 2;
  colDesk        = 3;
  ColtglMulai    = 4;
  colTglTarget   = 5;
  colTglJual     = 6;
  colTotJual     = 7;
  colTglBeli     = 8;
  colTotBeli     = 9;
  colTglInvoice  = 10;
  colNoInvoice    = 11;
  colTglBayar     = 12;
  colVendor       = 13;
  colTglByrVendor = 14;
  ColSeq          = 15;
  colseqBeli      = 16;


procedure TfrmLaporanProgress.btnVendorClick(Sender: TObject);
begin
  seqVendor := 0;
  txtVendor.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqVendor := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_VENDOR));
  if seqVendor <> 0 then begin
    txtVendor.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqVendor)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqVendor)],'#',2);
  end;
end;

procedure TfrmLaporanProgress.arrangecolsize;
var col, i : integer;
begin
  asgRekap.AutoNumberCol(colno);
  asgrekap.cells[colTglJual, asgrekap.rowcount-1]:= 'Total';
  asgrekap.floatingfooter.columncalc[colTotJual]    :=  acsum;
  asgrekap.floatingfooter.columncalc[colTotBeli]    :=  acsum;
  asgRekap.AutoSizeColumns(true);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;      
    asgRekap.ColWidths[colseqBeli]   := 0;
  end;

  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.ColWidths[col]:= 0;
    end;
  end;
end;

procedure TfrmLaporanProgress.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if asgRekap.Cells[colTglBayar, arow] = '' then begin
    btnUpd.Enabled := True;
    btnBatal.Enabled:= False;
  end else begin
    btnUpd.Enabled := false;
    btnBatal.Enabled:= true;
  end;
  if asgRekap.Cells[colTglByrVendor, arow] = '' then begin
    btnUpdV.Enabled := True;
    btnBatalV.Enabled:= False;
  end else begin
    btnUpdV.Enabled := false;
    btnBatalV.Enabled:= true;
  end;
end;

procedure TfrmLaporanProgress.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
//  if (arow > 0) and (arow< asgrekap.rowcount-1) then btndetail.click;
end;

procedure TfrmLaporanProgress.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [{ColTotal,}colSeq, colTotJual, colTotbeli, colNo] then HAlign := taRightJustify;
end;

procedure TfrmLaporanProgress.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
//  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
//    AFont.Color := clRed;
end;

procedure TfrmLaporanProgress.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
//  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
//    AFont.Color := clRed;
end;

procedure TfrmLaporanProgress.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, colseqBeli,colSeq, colTotJual,colTotBeli] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol in [colseqBeli, colseq] then FloatFormat    := '%.0f'
    else FloatFormat := '%.2n'
  end;
end;

procedure TfrmLaporanProgress.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanProgress.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanProgress.asgRekapSelectionChanged(Sender: TObject;
  ALeft, ATop, ARight, ABottom: Integer);
begin
  asgRekap.OnClickCell(self,asgRekap.Row,asgRekap.Col);
end;

procedure TfrmLaporanProgress.btnAturClick(Sender: TObject);
begin
  listHideCol.clear;
  arrangecolsize;
  Application.CreateForm(TfrmAturKolom, frmAturKolom);
  frmAturKolom.OnClose := frmMainMenu.ChildFormSingle;
  frmAturKolom.execute(listHideCol, asgRekap);
  arrangecolsize;
end;

procedure TfrmLaporanProgress.btnBatalClick(Sender: TObject);
begin
  if Confirmed('Pembayaran apakah akan dibatalkan?') then begin
    if isbatal then begin
      Inform('Berhasil dibatalkan');
      btnload.click;
    end else Inform('Gagal dibatalkan');

  end;

end;

procedure TfrmLaporanProgress.btnBatalVClick(Sender: TObject);
begin
  if Confirmed('Pembayaran apakah akan dibatalkan?') then begin
    if isbatalVendor then begin
      Inform('Berhasil dibatalkan');
      btnload.click;
    end else Inform('Gagal dibatalkan');
  end;
end;

procedure TfrmLaporanProgress.btnCetakClick(Sender: TObject);
var col, I : integer;
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Lapoeran Progress');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 3;
  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.HideColumn(col);
    end;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;         
  asgRekap.UnHideColumnsAll;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 3;
  arrangecolsize;
end;

procedure TfrmLaporanProgress.btnCustClick(Sender: TObject);
begin
  seqCard := 0;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmLaporanProgress.btnLoadClick(Sender: TObject);
begin
  loaddata;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmLaporanProgress.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
  asgRekap.OnClickCell(self,1,1);
end;

procedure TfrmLaporanProgress.btnUpdClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    if (asgRekap.cells[Coltglinvoice, asgRekap.Row]='') then begin
      inform('Data pekerjaan belum ada Invoicenya');
      exit;      
    end;

    Application.CreateForm(TfrmUpdateTglByr, frmUpdateTglByr);
    frmUpdateTglByr.OnClose := frmMainMenu.ChildFormSingle;
    if frmUpdateTglByr.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then btnLoad.Click;
  end;
end;

procedure TfrmLaporanProgress.btnUpdVClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    if (asgRekap.cells[colTglBeli, asgRekap.Row]='') then begin
      inform('Data pekerjaan belum ada Data Belinya');
      exit;
    end;

    Application.CreateForm(TfrmUpdateTglByr, frmUpdateTglByr);
    frmUpdateTglByr.OnClose := frmMainMenu.ChildFormSingle;
    if frmUpdateTglByr.execute(asgRekap.Ints[colseq, asgRekap.Row], true, asgRekap.Ints[colseqBeli, asgRekap.Row]) then btnLoad.Click;
  end;
end;

procedure TfrmLaporanProgress.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanProgress.Execute(AidMenu: integer);
begin      
  listHideCol := Tstringlist.create;
  AMenuId := AIdMenu;
  datenow  :=ServerNow;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;

  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmLaporanProgress.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLaporanProgress.initform;
begin
  listHideCol.clear;  
  dtpAwal.Date    := datenow;
  dtpAkhir.Date    := datenow;
  seqVendor       := 0;
  txtCust.Text    := '';
  seqCard         := 0;
  cmbDes.ItemIndex              := 0;    
  cmbStat.ItemIndex             := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

function TfrmLaporanProgress.isbatal: boolean;
var dtJob : TR_master_job;
begin
  try
    myConnection.BeginSQL;
    dtJob := Get_master_job(asgRekap.Ints[ColSeq, asgrekap.row]);
    if dtJob.seq <> 0 then begin
      dtJob.tgl_Bayar :=0;
      Update_master_job(dtJob, true);
    end;
    myConnection.EndSQL;
    result :=true;
  except
    myConnection.UndoSQL;
    result :=false;
  end;

end;

function TfrmLaporanProgress.isbatalVendor: boolean;
var dtbeli : tr_data_beli_master;
begin
  try
    myConnection.BeginSQL;
    dtbeli := Get_data_beli_master(asgRekap.Ints[colseqBeli, asgRekap.Row]);
    if dtbeli.seq <> 0 then begin
      dtbeli.tgl_byr := 0;
      update_data_beli_master(dtbeli, true);
    end;
    
    myConnection.EndSQL;
    result :=true;
  except
    myConnection.UndoSQL;
    result :=false;
  end;


end;

procedure TfrmLaporanProgress.loaddata;
var buffer      : _recordset;
    tmp,i,Row       : Integer;
    vendor, Stat, Filter, unitTmp,Divisi : string;
    Periode : TR_FilterPeriode;
    listVendor : TStringList;
begin
  listVendor:= TStringList.Create;
  asgRekap.RowCount := 3;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  periode.FPeriodeAwal      := dtpAwal.Date;
  periode.FPeriodeAkhir     := dtpAkhir.Date;
  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;

  Divisi := '';
  case cmbDes.ItemIndex of
    1 : Divisi := TIPE_DIVISI_LOGISTIC;
    2 : Divisi := TIPE_DIVISI_TRADING;
    3 : Divisi := TIPE_DIVISI_CONSTRUCTION;
  end;

  Stat := '';
  case cmbStat.ItemIndex of
    1 : stat := ' and j.tgl_bayar is null ';
    2 : stat := ' and j.tgl_bayar is not null ';
  end;

  filter := filter +ifthen(seqCard<>0,' and j.card_seq = '+formatsqlnumber(seqCard));
  filter := filter +Stat;

//  Load_Progress_master(buffer, periode, seqCard, Filter);
  if Periode.FPeriodeAwal <> 0 then filter := filter + ' and trunc(j.tgl_mulai) '+FormatSQLOperator(Periode.FOprPeriodeAwal)+FormatSQLDate(Periode.FPeriodeAwal);
  if Periode.FPeriodeAkhir <> 0 then filter := filter + ' and trunc(j.tgl_mulai) '+FormatSQLOperator(Periode.FOprPeriodeAkhir)+FormatSQLDate(Periode.FPeriodeAkhir);
//  if card_seq <> 0 then filter := filter + ' and card_seq = ' + FormatSqlNumber(card_seq);
//    filter := filter + addpar;
  sql := ' select distinct d.card_seq, d.master_seq from data_beli_detail d where exists (select * from data_beli_master m where m.seq = d.master_seq) ';
//         ' and m.tanggal between '+FormatSQLDate(Periode.FPeriodeAwal)+' and  '+FormatSQLDate(Periode.FPeriodeAkhir)+' ) ';
  buffer :=myConnection.OpenSQL(sql);
  tmp := 0;
  for i := 0 to buffer.RecordCount-1 do begin
    if (i<>0)and(tmp <> buffertointeger(buffer.Fields[1].value)) then begin
      listVendor.Add(inttostr(tmp)+'='+vendor);
      vendor:= '';
    end;
    vendor := vendor+ifthen(vendor<>'',', ')+EkstrakString(ListCardGlobal.Values[BufferToString(buffer.Fields[0].value)],'#',2);
    tmp := buffertointeger(buffer.Fields[1].value);

    buffer.MoveNext;
  end;
  if vendor <> '' then begin
    listVendor.Add(inttostr(tmp)+'='+vendor);
    vendor:= '';
  end;
  buffer.Close;


  sql := ' SELECT c.nama, j.nama, j.deskripsi, j.tgl_mulai, j.tgl_target, null as tgljual, ' +
          ' null as  tgl_beli, null as tglinv, null as noinv, j.seq, j.tgl_bayar, null  as seqbeli, null as tglbyrvendor, 0 as totjual, 0 as totbeli '+
          ' From master_card c, master_job j where j.card_seq = c.seq  and '+
          ' j.seq not in (select m.job_seq from data_jual_master m union all select m.job_seq ' +
          ' from  data_beli_master m  ) '+filter+

          ' union all '+

          ' SELECT c.nama, j.nama, j.deskripsi, j.tgl_mulai, j.tgl_target, m.tanggal as tgljual, ' +
           'null as  tgl_beli, null as tglinv, null as noinv, j.seq, j.tgl_bayar , null  as seqbeli, null as tglbyrvendor, m.subtotal as totjual, 0 as totbeli '+
          ' From master_card c, master_job j, data_jual_master m where j.card_seq = c.seq  and '+
          ' j.seq = m.job_seq and m.job_seq not in (select mm.job_seq from  data_beli_master mm where  m.job_seq =  m.job_seq) '+filter+
          ' and m.seq not in (select i.jual_seq from invoice_detail i where i.jual_seq = m.seq ) '+

          ' union all '+

          ' SELECT c.nama, j.nama, j.deskripsi, j.tgl_mulai, j.tgl_target, m.tanggal  as tgljual, ' +
          ' mm.tanggal  as  tgl_beli, null as tglinv, null as noinv, j.seq, j.tgl_bayar, mm.seq as seqbeli, mm.tgl_byr as tglbyrvendor, m.subtotal as totjual, mm.subtotal as totbeli '+
          ' From master_card c, master_job j, data_jual_master m, data_beli_master mm where j.card_seq = c.seq  and '+
          ' j.seq = m.job_seq and j.seq = mm.job_seq and m.job_seq = mm.job_seq '+
          ' and m.seq not in (select i.jual_seq from invoice_detail i where i.jual_seq = m.seq ) '+filter+

          ' union all '+

          ' SELECT c.nama, j.nama, j.deskripsi, j.tgl_mulai, j.tgl_target, m.tanggal  as tgljual, mm.tanggal  as  tgl_beli, '+
          ' (select im.tanggal from invoice_master im where im.seq = id.master_seq and im.card_seq = c.seq) as tglinv, '+
          ' (select im.nomor  from invoice_master im where im.seq = id.master_seq and im.card_seq = c.seq) as tglinv, j.seq, j.tgl_bayar, mm.seq as seqbeli, mm.tgl_byr as tglbyrvendor, m.subtotal as totjual, mm.subtotal as totbeli '+
          ' From master_card c, master_job j, data_jual_master m, data_beli_master mm, invoice_detail id '+
          ' where j.card_seq = c.seq and id.jual_seq = m.seq and '+
          ' j.seq = m.job_seq and j.seq = mm.job_seq and m.job_seq = mm.job_seq '+filter+

          ' union all '+

          ' SELECT c.nama, j.nama, j.deskripsi, j.tgl_mulai, j.tgl_target, m.tanggal  as tgljual, null  as  tgl_beli, '+
          ' (select im.tanggal from invoice_master im where im.seq = id.master_seq and im.card_seq = c.seq) as tglinv, '+
          ' (select im.nomor  from invoice_master im where im.seq = id.master_seq and im.card_seq = c.seq) as tglinv, j.seq, j.tgl_bayar , null  as seqbeli, null as tglbyrvendor, m.subtotal as totjual, 0 as totbeli '+
          ' From master_card c, master_job j, data_jual_master m, invoice_detail id '+
          ' where j.card_seq = c.seq and id.jual_seq = m.seq and '+
          ' j.seq = m.job_seq and not exists (select * from data_beli_master mm where j.seq = mm.job_seq and m.job_seq = mm.job_seq) '+filter+

          ' order by tgl_mulai ';

          //and m.job_seq not in (select m.job_seq from   m  ) '+

//         ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter))) + ' order by tanggal, nomor ';
  buffer := myConnection.OpenSQL(sql);

  for i := 0 to buffer.RecordCount - 1 do begin
    AdvProgressBar1.Position := round((i+1)/buffer.RecordCount*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -2;

    Asgrekap.cells[colCust       , row] := BufferToString(buffer.Fields[0].Value)  ;
    Asgrekap.cells[colNama       , row] := BufferToString(buffer.Fields[1].Value)  ;
    Asgrekap.cells[colDesk       , row] := BufferToString(buffer.Fields[2].Value)  ;


    if BufferToDateTime(buffer.Fields[3].Value)  > 0 then
      Asgrekap.dates[ColtglMulai   , row] := BufferToDateTime(buffer.Fields[3].Value)  ;

    if BufferToDateTime(buffer.Fields[4].Value)  > 0 then
      Asgrekap.dates[colTglTarget  , row] := BufferToDateTime(buffer.Fields[4].Value)  ;

    if BufferToDateTime(buffer.Fields[5].Value)  > 0 then
      Asgrekap.dates[colTglJual    , row] := BufferToDateTime(buffer.Fields[5].Value)  ;

    if BufferToDateTime(buffer.Fields[6].Value)  > 0 then
      Asgrekap.dates[colTglBeli    , row] := BufferToDateTime(buffer.Fields[6].Value)  ;

    if BufferToDateTime(buffer.Fields[7].Value)  > 0 then
      Asgrekap.Dates[colTglInvoice , row] := BufferToDateTime(buffer.Fields[7].Value)  ;

    if BufferToDateTime(buffer.Fields[10].Value)  > 0 then
      Asgrekap.Dates[colTglBayar , row] := BufferToDateTime(buffer.Fields[10].Value)  ;

    Asgrekap.cells[colNoInvoice  , row] := BufferToString(buffer.Fields[8].Value);
    Asgrekap.ints[ColSeq         , row] := BufferToInteger(buffer.Fields[9].Value);

    Asgrekap.ints[colseqBeli, row] := BufferToInteger(buffer.Fields[11].Value);
    Asgrekap.cells[colVendor  , row] := listVendor.Values[BufferToString(buffer.Fields[11].Value)];
    if BufferToDateTime(buffer.Fields[12].Value)  > 0 then
      Asgrekap.Dates[colTglByrVendor , row] := BufferToDateTime(buffer.Fields[12].Value)  ;

    Asgrekap.Floats[colTotJual  , row] := BufferToFloat(buffer.Fields[13].Value);
    Asgrekap.Floats[colTotBeli  , row] := BufferToFloat(buffer.Fields[14].Value);

//    Asgrekap.dates[ColTanggal  , row] := BufferToDateTime(buffer.Fields[2].Value)  ;
//    Asgrekap.cells[ColNomor    , row] := BufferToString(buffer.Fields[1].Value)  ;
//    Asgrekap.cells[ColNomorBaut, row] := BufferToString(buffer.Fields[10].Value)  ;
//    Asgrekap.cells[ColNomorBast, row] := BufferToString(buffer.Fields[11].Value)  ;
//    Asgrekap.cells[colcust     , row] := EkstrakString(ListCardGlobal.Values[buffertoString(
//      buffer.Fields[3].Value)],'#',2);
//    Asgrekap.Floats[colTotal   , row] := BufferToFloat(buffer.Fields[4].Value);
//    Asgrekap.cells[colKet      , row] := BufferToString(buffer.Fields[5].Value);
//    Asgrekap.cells[colUserId   , row] := BufferToString(buffer.Fields[6].Value);
//    Asgrekap.dates[colTglInput e, row] := BufferToDateTime(buffer.Fields[7].Value);
//    Asgrekap.ints[colSeq       , row] := BufferToINteger(buffer.Fields[0].Value);

    buffer.MoveNext;
  end;
  buffer.Close;
  AdvProgressBar1.Hide;
  arrangecolsize;
end;

procedure TfrmLaporanProgress.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 18;
  asgRekap.RowCount := 3;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;

  arrangecolsize;
end;

procedure TfrmLaporanProgress.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

end.
