unit LaporanInvoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, AdvGlowButton, AdvPanel,
  ExtCtrls, Gauges, AdvProgressBar, AdvOfficeButtons, AdvSmoothPanel,
  AdvCombo, AdvEdit, ComCtrls, Menus;

type
  TfrmLaporanInvoice = class(TForm)
    MainPanel: TPanel;
    Panel1: TPanel;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    AdvProgressBar1: TAdvProgressBar;
    Shape1: TShape;
    Shape2: TShape;
    Label1: TLabel;
    Label2: TLabel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    chbSF: TAdvOfficeCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label19: TLabel;
    cmbDes: TComboBox;
    btnCust: TAdvGlowButton;
    txtCust: TAdvEdit;
    dtpAwal: TDateTimePicker;
    dtpAkhir: TDateTimePicker;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    txtVendor: TAdvEdit;
    btnVendor: TAdvGlowButton;
    btnCetakInv: TAdvGlowButton;
    btnKwitansi: TAdvGlowButton;
    btnAtur: TAdvGlowButton;
    btnDetail: TAdvGlowButton;
    btnBast: TAdvGlowButton;
    btnBaut: TAdvGlowButton;
    btnLamp: TAdvGlowButton;
    btnLampBeli: TAdvGlowButton;
    pmCetak: TPopupMenu;
    Print1: TMenuItem;
    Print2: TMenuItem;
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetFloatFormat(Sender: TObject; ACol, ARow: Integer;
      var IsFloat: Boolean; var FloatFormat: string);
    procedure asgRekapMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgRekapMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnResetClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure chbSFClick(Sender: TObject);
    procedure btnCustClick(Sender: TObject);
    procedure btnDetailClick(Sender: TObject);
    procedure btnVendorClick(Sender: TObject);
    procedure btnCetakInvClick(Sender: TObject);
    procedure btnKwitansiClick(Sender: TObject);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure btnAturClick(Sender: TObject);
    procedure btnBautClick(Sender: TObject);
    procedure btnBastClick(Sender: TObject);
    procedure btnLampClick(Sender: TObject);
    procedure btnLampBeliClick(Sender: TObject);
    procedure Print1Click(Sender: TObject);
    procedure Print2Click(Sender: TObject);
  private
    seqCard,seqVendor, AMenuid : integer;
    datenow: TDate;
    listHideCol : Tstringlist;
    procedure initform;
    procedure setgrid;
    procedure arrangecolsize;
    procedure loaddata;
  public
    { Public declarations }
    procedure Execute(AidMenu : integer);
  end;

var
  frmLaporanInvoice: TfrmLaporanInvoice;

implementation

uses
  UEngine, URecord, USystemMenu, UConst, UGeneral, UCreateForm, UDummy, UTransaksi,
  UFinance, UReport, MainMenu, OracleConnection, ADOInt, StrUtils, LOV, InvoiceInput, InvoicePrint, 
  Invoice2print, AturKolom, BAST, BAUT, LampiranInvoice, Invoice2printFmt2;

{$R *.dfm}

const
  ColNo         = 0;
  ColTanggal    = 1;
  ColNomor      = 2;      
  ColNomorBaut  = 3;
  ColNomorBast  = 4;
  colcust       = 5;
  ColTotal      = 6;
  ColKet        = 7;
  colUserId     = 8;
  colTglInput   = 9;
  ColSeq        = 10;


procedure TfrmLaporanInvoice.btnVendorClick(Sender: TObject);
begin
  seqVendor := 0;
  txtVendor.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqVendor := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_VENDOR));
  if seqVendor <> 0 then begin
    txtVendor.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqVendor)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqVendor)],'#',2);
  end;
end;

procedure TfrmLaporanInvoice.btnLampBeliClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    Application.CreateForm(TfrmLampiranInvoice, frmLampiranInvoice);
    frmLampiranInvoice.OnClose := frmMainMenu.ChildFormSingle;
    if frmLampiranInvoice.execute(asgRekap.Ints[colseq, asgRekap.Row], true) then btnLoad.Click;
  end;
end;

procedure TfrmLaporanInvoice.arrangecolsize;
var col, i : integer;
begin
  asgRekap.AutoNumberCol(colno);
  asgrekap.cells[colcust, asgrekap.rowcount-1]:= 'Total';
  asgrekap.floatingfooter.columncalc[coltotal]    :=  acsum;
  asgRekap.AutoSizeColumns(true);
   if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colseq]   := 0;
  end;

  if (Amenuid = 504) or (amenuid = 507) then begin
    asgRekap.ColWidths[colNomorbaut]   := 0;
    asgRekap.ColWidths[colNomorbast]   := 0;
  end else if Amenuid = 506 then begin
    asgRekap.ColWidths[colNomorbast]   := 0;
  end;

  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.ColWidths[col]:= 0;
    end;
  end;
end;

procedure TfrmLaporanInvoice.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (arow > 0) and (arow< asgrekap.rowcount-1) then btndetail.click;
end;

procedure TfrmLaporanInvoice.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [ColTotal,colSeq, colNo] then HAlign := taRightJustify;
end;

procedure TfrmLaporanInvoice.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
//  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
//    AFont.Color := clRed;
end;

procedure TfrmLaporanInvoice.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
//  if (ARow > 0) and (ACol > 0)and (asgRekap.cells[colAktif, ARow] <> '') then
//    AFont.Color := clRed;
end;

procedure TfrmLaporanInvoice.asgRekapGetFloatFormat(Sender: TObject; ACol,
  ARow: Integer; var IsFloat: Boolean; var FloatFormat: string);
begin
  IsFloat := false;
  if acol in [colNo, ColTotal,colSeq] then begin
    IsFloat := true;
    if acol in [colNo] then FloatFormat       := '%.0n'
    else if acol in [colseq] then FloatFormat    := '%.0f'
    else FloatFormat := '%.2n'
  end;
end;

procedure TfrmLaporanInvoice.asgRekapMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanInvoice.asgRekapMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColSize;
end;

procedure TfrmLaporanInvoice.btnKwitansiClick(Sender: TObject);
var data : TR_invoice_master;
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.row] <> 0 then begin
    Data := Get_invoice_master(asgRekap.Ints[ColSeq, asgRekap.Row]);
    btnKwitansi.Enabled := False;
    Application.CreateForm(TfrmPrintInvoice, frmPrintInvoice);
    frmPrintInvoice.OnClose := frmMainMenu.ChildFormSingle;
    frmPrintInvoice.Execute(Data);
    btnKwitansi.Enabled := True;
  end;
end;

procedure TfrmLaporanInvoice.btnAturClick(Sender: TObject);
begin
  listHideCol.clear;
  arrangecolsize;
  Application.CreateForm(TfrmAturKolom, frmAturKolom);
  frmAturKolom.OnClose := frmMainMenu.ChildFormSingle;
  frmAturKolom.execute(listHideCol, asgRekap);
  arrangecolsize;
end;

procedure TfrmLaporanInvoice.btnBastClick(Sender: TObject);
var data : TR_invoice_master;
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.row] <> 0 then begin
    Data := Get_invoice_master(asgRekap.Ints[ColSeq, asgRekap.Row]);
    btnBast.Enabled := False;
    Application.CreateForm(TfrmBast, frmBast);
    frmBast.OnClose := frmMainMenu.ChildFormSingle;
    frmBast.Execute(Data);
    btnBast.Enabled := True;
  end;
end;

procedure TfrmLaporanInvoice.btnBautClick(Sender: TObject);
var data : TR_invoice_master;
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  if asgRekap.Ints[ColSeq, asgRekap.row] <> 0 then begin
    Data := Get_invoice_master(asgRekap.Ints[ColSeq, asgRekap.Row]);
    btnBaut.Enabled := False;
    Application.CreateForm(TfrmBaut, frmBaut);
    frmBaut.OnClose := frmMainMenu.ChildFormSingle;
    frmBaut.Execute(Data);
    btnbaut.Enabled := True;
  end;
end;

procedure TfrmLaporanInvoice.btnCetakClick(Sender: TObject);
var col, I : integer;
begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Lapoeran Invoice');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := asgRekap.ColCount - 2;
  for i := 0 to listHideCol.count-1 do begin
    if listHideCol.ValueFromIndex[i] = FALSE_STRING then begin
      col := strtointdef(listHideCol.Names[i],0);
      if col > 0 then
        asgRekap.HideColumn(col);
    end;
  end;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;         
  asgRekap.UnHideColumnsAll;
  asgRekap.ColumnSize.Stretch := True;
  asgRekap.ColCount := asgRekap.ColCount + 2;
  arrangecolsize;
end;

procedure TfrmLaporanInvoice.btnCetakInvClick(Sender: TObject);

begin
  if not BisaPrint(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  
  PopupMenu := pmCetak;
  pmCetak.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  PopupMenu := nil;

end;

procedure TfrmLaporanInvoice.btnCustClick(Sender: TObject);
begin
  seqCard := 0;
  txtCust.text := '';
  Application.CreateForm(TfrmLov, frmLov);
  frmLov.OnClose := frmMainMenu.ChildFormSingle;
  seqCard := frmLov.executeCard('', boTrue, ' and tipe = '+formatsqlstring(TIPE_CUSTOMER));
  if seqCard <> 0 then begin
    txtCust.text := EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',1) + ' - ' +
                    EkstrakString(ListCardGlobal.Values[IntToStr(seqCard)],'#',2);
  end;
end;

procedure TfrmLaporanInvoice.btnDetailClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    Application.CreateForm(TfrmInputInvoice, frmInputInvoice);
    frmInputInvoice.OnClose := frmMainMenu.ChildFormSingle;
    frmInputInvoice.execute(asgRekap.Ints[colseq, asgRekap.Row], false);
  end;
end;

procedure TfrmLaporanInvoice.btnLampClick(Sender: TObject);
begin
  if (asgRekap.Ints[ColSeq, asgRekap.Row]<>0) then begin
    Application.CreateForm(TfrmLampiranInvoice, frmLampiranInvoice);
    frmLampiranInvoice.OnClose := frmMainMenu.ChildFormSingle;
    if frmLampiranInvoice.execute(asgRekap.Ints[colseq, asgRekap.Row], false) then btnLoad.Click;
  end;
end;

procedure TfrmLaporanInvoice.btnLoadClick(Sender: TObject);
begin
  loaddata;
end;

procedure TfrmLaporanInvoice.btnResetClick(Sender: TObject);
begin
  initform;
  setgrid;
end;

procedure TfrmLaporanInvoice.chbSFClick(Sender: TObject);
begin
   asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanInvoice.Execute(AidMenu: integer);
begin      
  listHideCol := Tstringlist.create;
  AMenuId := AIdMenu;
  datenow  :=ServerNow;
  if (not BisaLihatRekap(AIdMenu)) and (not BisaNambah(AIdMenu)) and (not BisaEdit(AIdMenu)) and
     (not BisaHapus(AIdMenu)) and (not BisaPrint(AIdMenu)) and (not BisaEkspor(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  if (Amenuid = 504) or (amenuid = 507) then begin
    btnBaut.Visible := false;
    btnBast.Visible := false;
    btnLamp.Left    := btnBaut.Left;
    btnLampBeli.Left := btnBast.Left;
  end else if Amenuid = 505 then begin
    btnBaut.Visible := false;
    btnKwitansi.Visible := false;
    btnCetakInv.Visible := false;
    btnBast.Left := btnCetakInv.Left;
    btnLamp.Visible := false;
    btnLampBeli.Visible := false;
  end else begin
    btnBast.Visible := false;
    btnKwitansi.Visible := false;
    btnCetakInv.Visible := false;
    btnBaut.Left := btnCetakInv.Left;
    btnLamp.Visible := false;
    btnLampBeli.Visible := false;
  end;

  initForm;
  setGrid;
  btnLoad.Click;
  Run(self);
end;

procedure TfrmLaporanInvoice.FormShow(Sender: TObject);
begin
  Execute(ACurrMenuId);
end;

procedure TfrmLaporanInvoice.initform;
begin                      
  listHideCol.clear;  
  dtpAwal.Date    := datenow;
  dtpAkhir.Date    := datenow;
  seqVendor       := 0;
  txtCust.Text    := '';
  seqCard         := 0;
  cmbDes.ItemIndex              := 0;
  chbSF.Checked                 := false;
  asgRekap.SearchFooter.Visible := chbSF.Checked;
end;

procedure TfrmLaporanInvoice.loaddata;
var buffer      : _recordset;
    i,Row       : Integer;
    Filter, unitTmp,Divisi : string;
    Periode : TR_FilterPeriode;
begin
  asgRekap.RowCount := 3;
  asgRekap.ClearNormalCells;
  asgRekap.ClearRows(asgRekap.RowCount-1,1);
  asgRekap.Row := 1;
  Filter :='';
  periode.FPeriodeAwal      := dtpAwal.Date;
  periode.FPeriodeAkhir     := dtpAkhir.Date;
  periode.FOprPeriodeAwal   := soGreaterThanEqualsTo;
  periode.FOprPeriodeAkhir  := soLessThanEqualsTo;

  Divisi := '';
  case cmbDes.ItemIndex of
    1 : Divisi := TIPE_DIVISI_LOGISTIC;
    2 : Divisi := TIPE_DIVISI_TRADING;
    3 : Divisi := TIPE_DIVISI_CONSTRUCTION;
  end;

  filter := filter +ifthen(seqCard<>0,' and card_seq = '+formatsqlnumber(seqCard));
  if (AMenuid = 505) or (amenuid = 506) then filter := filter +' and card_Seq in (select seq from master_card where ' +
    'isBautBast = ''T'')';

//  (seq = 0)  (nomor = 1)  (tanggal = 2)  (card_seq = 3)  (total = 4)
//(keterangan = 5)  (user_id = 6)  (tgl_input = 7)  (user_edit = 8)  (tgl_edit = 9)
//baut 10 bast 11

  Load_invoice_master(buffer, periode, seqCard, Filter);
  for i := 0 to buffer.RecordCount - 1 do begin
    AdvProgressBar1.Position := round((i+1)/buffer.RecordCount*100);
    if i > 0 then asgRekap.AddRow;
    Row := AsgRekap.RowCount -2;
    Asgrekap.dates[ColTanggal  , row] := BufferToDateTime(buffer.Fields[2].Value)  ;
    Asgrekap.cells[ColNomor    , row] := BufferToString(buffer.Fields[1].Value)  ;
    Asgrekap.cells[ColNomorBaut, row] := BufferToString(buffer.Fields[10].Value)  ;
    Asgrekap.cells[ColNomorBast, row] := BufferToString(buffer.Fields[11].Value)  ;
    Asgrekap.cells[colcust     , row] := EkstrakString(ListCardGlobal.Values[buffertoString(
      buffer.Fields[3].Value)],'#',2);
    Asgrekap.Floats[colTotal   , row] := BufferToFloat(buffer.Fields[4].Value);
    Asgrekap.cells[colKet      , row] := BufferToString(buffer.Fields[5].Value);
    Asgrekap.cells[colUserId   , row] := BufferToString(buffer.Fields[6].Value);
    Asgrekap.dates[colTglInput , row] := BufferToDateTime(buffer.Fields[7].Value);
    Asgrekap.ints[colSeq       , row] := BufferToINteger(buffer.Fields[0].Value);

    buffer.MoveNext;
  end;
  buffer.Close;
  AdvProgressBar1.Hide;
  arrangecolsize;
end;

procedure TfrmLaporanInvoice.Print2Click(Sender: TObject);
var data   : TR_invoice_master;
    isShow : boolean;
begin
  if asgRekap.Ints[ColSeq, asgRekap.row] <> 0 then begin
    Data := Get_invoice_master(asgRekap.Ints[ColSeq, asgRekap.Row]);
    btnCetakInv.Enabled := False;
    if Confirmed('Tampilkan rincian PO/SPK ?') then isShow := true
    else isShow := false;

    Application.CreateForm(TfrmInvoice2printFmt2, frmInvoice2printFmt2);
    frmInvoice2printFmt2.OnClose := frmMainMenu.ChildFormSingle;
    frmInvoice2printFmt2.Execute(Data, isShow);
    btnCetakInv.Enabled := True;
  end;
end;

procedure TfrmLaporanInvoice.Print1Click(Sender: TObject);
var data : TR_invoice_master;
    isShow : boolean;
begin
  if asgRekap.Ints[ColSeq, asgRekap.row] <> 0 then begin
    Data := Get_invoice_master(asgRekap.Ints[ColSeq, asgRekap.Row]);
    btnCetakInv.Enabled := False;
    if Confirmed('Tampilkan rincian PO/SPK ?') then isShow := true
    else isShow := false;
    Application.CreateForm(TfrmInvoice2print, frmInvoice2print);
    frmInvoice2print.OnClose := frmMainMenu.ChildFormSingle;
    frmInvoice2print.Execute(Data, isShow);
    btnCetakInv.Enabled := True;
  end;
end;

procedure TfrmLaporanInvoice.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.ColCount := 12;
  asgRekap.RowCount := 3;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;

  arrangecolsize;
end;

procedure TfrmLaporanInvoice.btnEksporClick(Sender: TObject);
begin
  if not BisaEkspor(AMenuId) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    exit;
  end;
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

end.
