object frmInputSettingTampilan: TfrmInputSettingTampilan
  Left = 424
  Top = 213
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Setting Tampilan'
  ClientHeight = 254
  ClientWidth = 437
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 437
    Height = 254
    Align = alClient
    TabOrder = 0
    ExplicitHeight = 255
    DesignSize = (
      437
      254)
    object Label1: TLabel
      Left = 8
      Top = 12
      Width = 110
      Height = 13
      Caption = 'Validasi stok bisa minus'
    end
    object Label2: TLabel
      Left = 8
      Top = 38
      Width = 116
      Height = 13
      Caption = 'Tanggal Batas Transaksi'
    end
    object Label3: TLabel
      Left = 151
      Top = 12
      Width = 141
      Height = 13
      Caption = '(centang jika stok bisa minus)'
    end
    object btnSimpan: TAdvGlowButton
      Left = 272
      Top = 223
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Simpan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      OnClick = btnSimpanClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object btnCancel: TAdvGlowButton
      Left = 353
      Top = 223
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Batal'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      NotesFont.Charset = DEFAULT_CHARSET
      NotesFont.Color = clWindowText
      NotesFont.Height = -11
      NotesFont.Name = 'Tahoma'
      NotesFont.Style = []
      ParentFont = False
      TabOrder = 1
      TabStop = True
      OnClick = btnCancelClick
      Appearance.ColorChecked = 16111818
      Appearance.ColorCheckedTo = 16367008
      Appearance.ColorDisabled = 15921906
      Appearance.ColorDisabledTo = 15921906
      Appearance.ColorDown = 16111818
      Appearance.ColorDownTo = 16367008
      Appearance.ColorHot = 16117985
      Appearance.ColorHotTo = 16372402
      Appearance.ColorMirrorHot = 16107693
      Appearance.ColorMirrorHotTo = 16775412
      Appearance.ColorMirrorDown = 16102556
      Appearance.ColorMirrorDownTo = 16768988
      Appearance.ColorMirrorChecked = 16102556
      Appearance.ColorMirrorCheckedTo = 16768988
      Appearance.ColorMirrorDisabled = 11974326
      Appearance.ColorMirrorDisabledTo = 15921906
    end
    object chbstok: TCheckBox
      Left = 130
      Top = 11
      Width = 15
      Height = 17
      TabOrder = 2
    end
    object dtptanggal: TDateTimePicker
      Left = 130
      Top = 34
      Width = 95
      Height = 21
      Date = 41127.632414814810000000
      Time = 41127.632414814810000000
      TabOrder = 3
    end
    object Memo1: TMemo
      Left = 8
      Top = 61
      Width = 420
      Height = 156
      Anchors = [akLeft, akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Lines.Strings = (
        'Catatan :'
        
          ' a) Untuk Tanggal Batas transaksi, atur tanggal terakhir transak' +
          'si'
        '     yang dianggap sudah selesai pencatatannya.'
        '     Contoh :'
        
          '     Semua transaksi sampai tanggal 31 Desember 2013 sudah terca' +
          'tat'
        '     semua, maka set tanggalnya menjadi 01/01/2014.'
        
          '     Dengan disetnya batas transaksi per tanggal 01/01/2014, art' +
          'inya'
        
          '     transaksi tidak dapat dilakukan jika tanggal kurang dari 01' +
          '/01/2014.'
        
          ' b) Jika validasi stok bisa minus tidak dicentang semua transaks' +
          'i yang'
        
          '     mengurangi stok, saat jumlah barang yang dikeluarkan melebi' +
          'hi stok'
        '     akan muncul peringatan.')
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
    end
  end
end
