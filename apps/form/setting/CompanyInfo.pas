unit CompanyInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ExtDlgs, AdvGlowButton, StdCtrls, AdvEdit, ugeneral,
  urecord, Buttons, USystemMenu, AdvGroupBox;

type
  TfrmCompanyInfo = class(TForm)
    Panel1: TPanel;
    opdLinkFoto: TOpenPictureDialog;
    Panel2: TPanel;
    AdvGroupBox1: TAdvGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label16: TLabel;
    txtNama: TAdvEdit;
    mmALamat: TMemo;
    txtTelp1: TAdvEdit;
    txtFax: TAdvEdit;
    GroupBox1: TGroupBox;
    imgLogo: TImage;
    btnLoadFoto: TAdvGlowButton;
    txtNpwp: TAdvEdit;
    txtPng1: TAdvEdit;
    txtEmail: TAdvEdit;
    Label3: TLabel;
    txtJab1: TAdvEdit;
    Label7: TLabel;
    txtPng2: TAdvEdit;
    Label13: TLabel;
    txtJab2: TAdvEdit;
    Label14: TLabel;
    btnSimpan: TAdvGlowButton;
    btnBatal: TAdvGlowButton;
    procedure btnBatalClick(Sender: TObject);
    procedure cmbKotaKeyPress(Sender: TObject; var Key: Char);
    procedure btnLoadFotoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure txtIzinPbfKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    dataComp : TR_company_info ;
    file_foto : String;
    procedure initform;
    function isvalid : boolean;
    function issaved : boolean;
    procedure load;
  public
    { Public declarations }
    procedure execute;
  end;

var
  frmCompanyInfo: TfrmCompanyInfo;

implementation

uses uconst, uengine, oracleConnection, MainMenu;

{$R *.dfm}

procedure TfrmCompanyInfo.load;
begin
  dataComp := Get_company_info;
  txtNama.Text          := dataComp.Nama;
  mmALamat.Text         := dataComp.alamat;
  txtTelp1.Text         := dataComp.no_telp ;
  txtFax.Text           := dataComp.no_fax ;
  txtNpwp.Text          := dataComp.no_npwp;
  txtPng1.Text          := dataComp.png_jwb1 ;
  txtJab1.Text          := dataComp.jabatan1 ;
  txtPng2.Text          := dataComp.png_jwb2 ;
  txtJab2.Text          := dataComp.jabatan2 ;
  txtEmail.Text         := dataComp.email;

  file_foto := dataComp.logo;
  imgLogo.Visible := True; imgLogo.Picture.Bitmap.FreeImage;
  file_foto := GetAppPath + 'Images\' + dataComp.logo;
  imgLogo.Visible := True;
  if FileExists(file_foto) then
    imgLogo.Picture.LoadFromFile(file_foto)
  else imgLogo.Visible := False;
end;

procedure TfrmCompanyInfo.txtIzinPbfKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
    btnLoadFoto.SetFocus;
end;

function TfrmCompanyInfo.isvalid:boolean;
begin
  result := false;
  if TrimAllString(txtNama.Text) = '' then begin
     Inform('Nama belum diisi.');
     txtNama.SetFocus;
     exit;
  end else if TrimAllString(mmALamat.Text) = '' then  begin
     Inform('Alamat belum diisi.');
     mmALamat.SetFocus;
     exit;
  end else if TrimAllString(txtTelp1.Text) = '' then  begin
     Inform('No. Telp belum diisi.');
     txtTelp1.SetFocus;
     exit;
  end else result := true;
end;

function TfrmCompanyInfo.issaved:boolean;
begin
  try
    myConnection.BeginSQL;
    dataComp.Nama := TrimAllString(txtNama.Text);
    dataComp.alamat  := TrimAllString(mmALamat.Text);
    dataComp.no_telp := TrimAllString(txtTelp1.Text);
    dataComp.no_fax  := TrimAllString(txtFax.Text);
    dataComp.email   := TrimAllString(txtEmail.Text);
    dataComp.no_npwp      := TrimAllString(txtNpwp.Text);
    dataComp.png_jwb1     := TrimAllString(txtPng1.Text);
    dataComp.jabatan1     := TrimAllString(txtJab1.Text);
    dataComp.png_jwb2     := TrimAllString(txtPng2.Text);
    dataComp.jabatan2     := TrimAllString(txtJab2.Text);

    if file_foto <> '' then begin
      file_foto := ExtractFileName(opdLinkFoto.FileName);
      dataComp.logo := file_foto
    end else dataComp.logo := '';

    Delete_company_info;
    Save_company_info (dataComp);

    myConnection.EndSQL;
    result := true; //USystemMenu.GlobalSetPerusahaan := UEngine.Get_company_info_Global;
  except
    myConnection.UndoSQL;
    Inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

procedure TfrmCompanyInfo.initform;
begin
  txtSetAllEmpty(self);
  txtNama.Clear;
  mmALamat.Clear;
  txtTelp1.Clear;
  txtFax.Clear;
  btnLoadFoto.Enabled := true;
  btnSimpan.Enabled :=  true;
  imgLogo.Visible := False;
end;

procedure TfrmCompanyInfo.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmCompanyInfo.btnLoadFotoClick(Sender: TObject);
begin
  file_foto := '';
  opdLinkFoto.InitialDir := GetCurrentDir + '\Images';
//  if opdLinkFoto.Execute then begin
  opdLinkFoto.Execute;
  imgLogo.Visible := True;
  imgLogo.Picture.LoadFromFile(opdLinkFoto.FileName); 
  file_foto := ExtractFileName(opdLinkFoto.FileName);
//  end ;
end;

procedure TfrmCompanyInfo.btnSimpanClick(Sender: TObject);
begin
  if (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if (isSaved) then begin
      ModalResult := mrOk ;
    end;
  end;
end;

procedure TfrmCompanyInfo.cmbKotaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
    txtTelp1.SetFocus;
end;

procedure TfrmCompanyInfo.execute;
begin
  initform;
  load;
  Self.Caption := 'Profil Perusahaan';
  Run(Self, True);
end;

procedure TfrmCompanyInfo.FormShow(Sender: TObject);
begin
  txtNama.SetFocus;
end;

end.
