//--------------------gunawan 240816-----------------------------------
unit SettingTampilanInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvGlowButton, ExtCtrls, Urecord, OracleConnection,
  Ugeneral, Uconst, UsystemMenu, MainMenu, Uengine_temp;

type
  TfrmInputSettingTampilan = class(TForm)
    MainPanel: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btnSimpan: TAdvGlowButton;
    btnCancel: TAdvGlowButton;
    chbstok: TCheckBox;
    dtptanggal: TDateTimePicker;
    Memo1: TMemo;
    procedure btnSimpanClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    AMenuId    : integer;
    Data : TR_setting_tampilan ;
    procedure initform;
    function issaved : boolean;
    procedure load;
    { Private declarations }
  public
    { Public declarations }
    procedure execute(Aidmenu: Integer);
  end;

var
  frmInputSettingTampilan: TfrmInputSettingTampilan;

implementation

uses
  UEngine;


{$R *.dfm}

{ TfrmInputSettingTampilan }

procedure TfrmInputSettingTampilan.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmInputSettingTampilan.btnSimpanClick(Sender: TObject);
begin
  if Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if (isSaved) then begin
      ModalResult := mrOk ;
      Inform(MSG_SUCCESS_SAVING);
    end;
  end;
end;

procedure TfrmInputSettingTampilan.execute(Aidmenu: Integer);
begin
  AMenuId := AIdMenu;
  if (Not BisaNambah(AMenuId)) then begin
   Alert(MSG_UNAUTHORISED_ACCESS);
   Exit;
  end;
  initform;
  load;                                                                   
  Run(Self, True);
end;

procedure TfrmInputSettingTampilan.initform;
begin
  chbstok.Checked := false;
  dtptanggal.Date := ServerNow;
end;

function TfrmInputSettingTampilan.issaved: boolean;
begin
  try
    myConnection.BeginSQL;
    Data.tanggal:= dtptanggal.Date;
    if chbstok.Checked  = True then begin
      Data.is_stok_boleh_minus := STATUS_TRUE
    end else if chbstok.Checked  = False then begin
      Data.is_stok_boleh_minus := STATUS_FALSE
    end;
    Data.user_id := GlobalSystemUser.UserId;

    delete_setting_tampilan(data);
    Save_setting_tampilan(data);

    myConnection.EndSQL;
    result := true;
  except
    myConnection.UndoSQL;
    Inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

procedure TfrmInputSettingTampilan.load;
begin
  data := get_setting_tampilan;
  if Data.tanggal <> 0 then begin
    dtptanggal.Date := Data.tanggal;
    if Data.is_stok_boleh_minus = STATUS_TRUE then begin
      chbstok.Checked := true;
    end else if  Data.is_stok_boleh_minus = STATUS_TRUE then begin
      chbstok.Checked := false;
    end;
  end;
end;

end.
