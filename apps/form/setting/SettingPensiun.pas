unit SettingPensiun;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvGlowButton, ExtCtrls, Urecord, OracleConnection,
  Ugeneral, Uconst, UsystemMenu, MainMenu, Uengine_temp, AdvEdit;

type
  TfrmSettingPensiun = class(TForm)
    Panel1: TPanel;
    txtPrcnt: TAdvEdit;
    txtNominal: TAdvEdit;
    Label1: TLabel;
    btnbatal: TAdvGlowButton;
    btnSimpan: TAdvGlowButton;
    procedure btnSimpanClick(Sender: TObject);
    procedure btnbatalClick(Sender: TObject);
  private
    { Private declarations }
    AMenuId    : integer;
    data : TR_setting_pensiun;
    procedure initform;
    function issaved : boolean;
    function isValid : Boolean;
    procedure load;
  public
    { Public declarations }
    procedure execute(Aidmenu: Integer);
  end;

var
  frmSettingPensiun: TfrmSettingPensiun;

implementation

uses
  UEngine;

{$R *.dfm}

procedure TfrmSettingPensiun.btnbatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmSettingPensiun.btnSimpanClick(Sender: TObject);
begin
  if (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if (isSaved) then begin
      ModalResult := mrOk ;
      Inform(MSG_SUCCESS_SAVING);
    end;
  end;
end;

procedure TfrmSettingPensiun.execute(Aidmenu: Integer);
begin
  AMenuId := AIdMenu;
  if (Not BisaNambah(AMenuId)) then begin
   Alert(MSG_UNAUTHORISED_ACCESS);
   Exit;
  end;
  initform;
  load;
  Run(Self, True);
end;

procedure TfrmSettingPensiun.initform;
begin
 txtPrcnt.FloatValue := 0;
 txtNominal.FloatValue := 0;
end;

function TfrmSettingPensiun.issaved: boolean;
begin
  try
    myConnection.BeginSQL;
    Delete_setting_pensiun_4save(data);
    Data.persen := txtPrcnt.FloatValue;
    Data.nominal  := txtNominal.FloatValue;

    Save_setting_pensiun(data);

    myConnection.EndSQL;
    result := true;
  except
    myConnection.UndoSQL;
    Inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;

function TfrmSettingPensiun.isValid: Boolean;
begin
  result := true;
  if txtPrcnt.FloatValue < 0 then begin
    Inform('Persen tidak boleh minus');
    Result := false;
    txtPrcnt.SetFocus;
    exit;
  end;
  if txtNominal.FloatValue < 0 then begin
    Inform('Nominal tidak boleh minus');
    Result := false;
    txtNominal.SetFocus;
    exit;
  end;  
end;

procedure TfrmSettingPensiun.load;
begin
  data := Get_setting_pensiun(botrue);
  txtPrcnt.FloatValue := data.persen;
  txtNominal.FloatValue := data.nominal;
end;

end.
