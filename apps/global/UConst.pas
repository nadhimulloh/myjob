unit UConst;
{$DEFINE ORCL}
{DEFINE POSTGRE}
interface

uses Graphics, AdvStyleIF;

const
  {EMAIL}
  Setting_EASend = 'ES-AA1141023508-00959-1856F4BBF62E35C2338720D2259DC407';
  Setting_EAGet  = 'EG-AA1150422356-00734-CF213AB1F2B11FC81A0135087066E04D';

  Setting_From   = '';
  Setting_Email_Password = '';
  Setting_Email_SMTP = 'smtp.gmail.com';
  Setting_Email_POP  = 'imap.gmail.com';
  Setting_Email_Port_Kirim = 465;
  Setting_Email_Port_Ambil = 993;

  MailServerImap4        = 1;

  EMAIL_FILE = 'Email.dll';

  {System Messages}
  MSG_UNAUTHORISED_ACCESS       = 'Anda tidak memiliki autorisasi untuk fungsi ini';//'You don`t have an access to this menu.';//
  MSG_SAVE_CONFIRMATION         = 'Apakah data sudah benar?';//'Is the data correct?';//
  MSG_DELETE_CONFIRMATION       = 'Data ini akan dihapus?';//'This data will be deleted,
  //proceed?';
  MSG_SUCCESS_SAVING            = 'Penyimpanan berhasil.';//'Saving successfully.';//
  MSG_UNSUCCESS_SAVING          = 'Penyimpanan tidak berhasil.'; //'Saving failed.';//
  MSG_SUCCESS_UPDATE            = 'Update data berhasil.';//'Data edit successfully.';//
  MSG_UNSUCCESS_UPDATE          = 'Update data gagal.'; //Data edit failed.';//'
  MSG_DUPLICATE                 = 'Duplikasi data.';//'Duplicate data';//
  MSG_NO_DATA_FOUND             = 'Data tidak ada atau tidak ditemukan.';//'Data not found.';//
  MSG_ADD_DATA                  = 'Tambah data lagi ?';//'Add more data?';//
  MSG_SAVE_ADD_DATA             = 'Penyimpanan berhasil, '+#13+'tambah data lagi ?';
  MSG_NO_NUMBER                 = '[Empty]';
  MSG_UNDERCONSTRUCTION         = 'Under Construction.';
  MSG_CONFIRMED_AKTIVASI        = 'Data akan dinonaktifkan / diaktifkan ? ';
  MSG_SUCCESS_DELETING          = 'Data berhasil dihapus.';//'Data has been deleted.';//
  MSG_UNSUCCESS_DELETING        = 'Data tidak berhasil dihapus.';//'Data delete failed.';//
  MSG_SUCCESS_AKTIVASI          = 'Data berhasil diaktifkan.';//'Data has been activated.';//
  MSG_UNSUCCESS_AKTIVASI        = 'Data tidak berhasil diaktifkan.';//'Activating data failed.';//
  MSG_SUCCESS_NOT_AKTIVASI      = 'Data berhasil dinonaktifkan.';//'Data has been deactivated.';//
  MSG_UNSUCCESS_NOT_AKTIVASI    = 'Data tidak berhasil dinonaktifkankan.';//'Deactivating data 
  //failed.';//
  MSG_CAN_NOT_DELETE            = 'Transaksi ini tidak bisa dihapus, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_ROW_DELETE_CONFIRMATION   = 'Baris ini akan dihapus?';
  MSG_SERVER_BUSY               = 'Komputer server sedang sibuk.'+#13+ 'Silahkan coba diulangi lagi.';
  MSG_CAN_NOT_UPDATE            = 'Transaksi ini tidak bisa diedit, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_PRINT                     = 'Cetak Transaksi ini?';//'Are you sure print this data ?';//

  MSG_BRG_SUDAH_DIOPNAME_EDIT   = 'Data tidak dapat diedit karena ada barang yang sudah diopname.';
  MSG_BRG_SUDAH_DIOPNAME_HAPUS  = 'Data tidak dapat dihapus karena ada barang yang sudah diopname.';
  MSG_SUDAH_JURNAL_EDIT         = 'Data tidak dapat diedit karena sudah dijurnal.';
  MSG_SUDAH_JURNAL_HAPUS        = 'Data tidak dapat dihapus karena sudah dijurnal.';

  MSG_CAN_NOT_DELETE_PGW        = 'Pegawai dari transaksi ini tidak bisa dihapus, '+#13+ //100816 ridwan
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_CAN_NOT_EDIT              = 'Transaksi ini tidak bisa diedit, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_CAN_NOT_EDIT_PGW          = 'Pegawai dari transaksi ini tidak bisa diedit, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';

  {Detail pembayaran massage}
  MSG_JENIS_BAYAR               = 'Tipe bayar harus dipilih.';
  MSG_ALAT_BAYAR                = 'Kas / Bank harus dipilih.';
  MSG_JUMLAH                    = 'Kolom jumlah harus diisi.';
  MSG_NO_DOKUMEN                = 'No dokumen harus diisi.';
  MSG_TGL_CAIR                  = 'Tgl. cair harus diisi.';
  MSG_TGL_JT                    = 'Tgl. jatuh tempo harus diisi.';
  MSG_DUPLICATE_DOKUMEN         = 'No. dokumen sudah ada.';
  MSG_TGL_JTH_TEMPO             = 'Tgl. jatuh tempo harus >= tgl transaksi.';
  MSG_TGL_CAIR_TRANS            = 'Tgl. cair harus >= tgl transaksi.';
  MSG_CEK_SALDO                 = 'Saldo tidak mencukupi.';
  MSG_TGL_TRANS                 = 'Tanggal transaksi tidak boleh > dari akhir bulan ini.';

  MSG_SUPPLIER                  = 'Supplier harus dipilih.';
  MSG_CUSTOMER                  = 'Customer harus dipilih.';
  MSG_RELASI                    = 'Relasi harus dipilih.';
  MSG_NO_VOUCHER                = 'No. Voucher harus diisi.';

  BTN_CAP_OK                    = '&OK';
  BTN_CAP_SAVE                  = '&Simpan';
  BTN_CAP_CANCEL                = '&Batal';
  BTN_CAP_DELETE                = '&Hapus';
  BTN_CAP_CLOSE                 = 'Close';
  BTN_CAP_PRINT                 = '&Cetak';
  BTN_CAP_RESET                 = '&Reset';
  BTN_CAP_ADD                   = '&Tambah';
  BTN_CAP_EDIT                  = '&Ubah';
  BTN_CAP_LIST                  = 'List All';
  BTN_CAP_FIND                  = 'Ca&ri';
  BTN_CAP_DETAIL                = '&Detail';
  BTN_CAP_SHOW_FILTER           = '&Lihat Filter';
  BTN_CAP_HIDE_FILTER           = 'Tutup &Filter';

  {table field constants}

  {Menu Groups}
  MENU_GROUP_SYSTEM    = 'System';
  MENU_GROUP_FINANCE   = 'Finance';
  MENU_GROUP_INVENTORY = 'Inventory';
  MENU_GROUP_SALES     = 'Sales';
  MENU_GROUP_PURCHASES = 'Purchases';

    {Form_Type}
  Form_type_setting        = 1;
  Form_type_master         = 2;
  Form_type_transaksi      = 3;
  Form_type_keuangan       = 4;
  Form_type_laporan        = 5;  
  Form_type_administratif  = 6;

  STYLE_OFFICE_2003_BLUE     = 'S1';
  STYLE_OFFICE_2003_CLASSIC  = 'S2';
  STYLE_OFFICE_2003_OLIVE    = 'S3';
  STYLE_OFFICE_2003_SILVER   = 'S4';
  STYLE_OFFICE_2007_LUNA     = 'S5';
  STYLE_OFFICE_2007_OBSIDIAN = 'S6';
  STYLE_OFFICE_2007_SILVER   = 'S7';
  STYLE_OFFICE_2010_BLACK    = 'S8';
  STYLE_OFFICE_2010_BLUE     = 'S9';
  STYLE_OFFICE_2010_SILVER   = 'S10';
  STYLE_TERMINAL             = 'S11';
  STYLE_WHIDBEY              = 'S12';
  STYLE_WINDOWS_7            = 'S13';
  STYLE_WINDOWS_VISTA        = 'S14';
  STYLE_WINDOWS_XP           = 'S15';

  STATUS_TRUE  = 'T';
  STATUS_FALSE = 'F';

  {Tipe Harga}
  HRG_MRPKAN_DPP = '1';
  HRG_INC_PPN = '2';
  HRG_NON_PPN = '3';

  {dokumen pembayaran tipe}
  DOC_TYPE_CASH                   = 'C';
  DOC_TYPE_CHEQUE                 = 'Q';
  DOC_TYPE_GIRO                   = 'G';
  DOC_TYPE_TRANSFER               = 'T';

  DOC_TYPE_CASH_TEXT              = 'Cash';
  DOC_TYPE_CHEQUE_TEXT            = 'Cheque';
  DOC_TYPE_GIRO_TEXT              = 'Bilyet Giro';
  DOC_TYPE_TRANSFER_TEXT          = 'Transfer';

  //tipe
  TIPE_CUSTOMER = 'C';
  TIPE_VENDOR   = 'V';

  TIPE_CUSTOMER_TEXT = 'Customer';
  TIPE_VENDOR_TEXT   = 'Vendor';

  TIPE_COMPANY = 'C';
  TIPE_INDIE   = 'I';

  TIPE_COMPANY_TEXT = 'Company';
  TIPE_INDIE_TEXT   = 'Individual';

  TIPE_DIVISI_LOGISTIC    = 'L';
  TIPE_DIVISI_TRADING     = 'T';
  TIPE_DIVISI_CONSTRUCTION = 'C';

  TIPE_DIVISI_LOGISTIC_TEXT    = 'Logistic';
  TIPE_DIVISI_TRADING_TEXT     = 'General Services';
  TIPE_DIVISI_CONSTRUCTION_TEXT = 'Construction';

  //setting bulan tahun
  GLOBAL_ED_INPUT = 'MMM yyyy';

  {}
  MASTER_CARD = 'LC';
  MASTER_JOB  = 'LJ';    
  MASTER_BANK = 'LB';

  {Daftar Transaksi}
  TRANS_PO           = 'PO';
  TRANS_BAST         = 'BAST';
  TRANS_INVOICE      = 'INV';   
  TRANS_BAUT         = 'BAUT';
  TRANS_KWITANSI     = 'KWT'; 

  TIPE_PEMBAYARAN_PIUTANG = 'PIUTANG';
  TIPE_PEMBAYARAN_TUNAI   = 'TUNAI';

  KONDISI_OK = 'OK';
  KONDISI_RUSAK = 'RUSAK';

  {tipe pinjam minjam}
  TIPE_PINJAMKAN_BARANG_TEXT           = 'Pinjamkan Barang';
  TIPE_MEMINJAM_BARANG_TEXT            = 'Meminjam Barang';
  TIPE_BARANG_DILUAR_DIKEMBALIKAN_TEXT = 'Barang Diluar Dikembalikan';
  TIPE_KEMBALIKAN_BARANG_MASUK_TEXT    = 'Kembalikan Barang Masuk';

  TIPE_PINJAMKAN_BARANG           = 1;
  TIPE_MEMINJAM_BARANG            = 2;
  TIPE_BARANG_DILUAR_DIKEMBALIKAN = 3;
  TIPE_KEMBALIKAN_BARANG_MASUK    = 4;

  ERR_LOCKED_TABLE        = 'ORA-00060';
  MSG_SERVER_BUSY_INFORM  = 'Penyimpanan tidak berhasil. Server sedang sibuk.';

  //laba Rugi
  SALDO_RL_CLOSING    = 'T';
  SALDO_RL_AMBIL_LABA = 'A';

  TIPE_SPA = 'SPA';
  TIPE_SPM = 'SPM';

  {max level constraint}

  MAX_PROG_LEVEL                = 3;

  {document numbering schema}

  DISABLED_BG_COLOR = clBtnFace;
  DISABLED_FG_COLOR = clWindowText;

  {user management}
  LEVEL_OPERATOR    = 1;
  LEVEL_SUPERVISOR  = 2;
  LEVEL_MANAGER     = 3;
  LEVEL_ADMIN       = 4;
  LEVEL_OWNER       = 5;
  LEVEL_DEFAULT     = 9;
  LEVEL_DEVELOPER   = 10;
  PWD_LENGTH        = 8;
  DEVELOPER_NIK     = '~';
  OWNER_NIK         = '0';

  TEXT_LEVEL_OPERATOR    = 'Operator';
  TEXT_LEVEL_SUPERVISOR  = 'Supervisor';
  TEXT_LEVEL_MANAGER     = 'Manager';
  TEXT_LEVEL_ADMIN       = 'Admin';
  TEXT_LEVEL_OWNER       = 'Direksi';
  TEXT_LEVEL_DEVELOPER   = 'Developer';

   // table master
  TABLE_BARANG   = 'B';
  TABLE_MST_CUST = 'C';
  TABLE_MST_SUPP = 'S';
  TABLE_KATEGORI = 'K';
  TABLE_UKURAN   = 'U';
  TABLE_CLASS    = 'CL';
  TABLE_SATUAN   = 'SU';
  TABLE_GUDANG   = 'G';
  LAP_NERACA     = 'N';


  {Transaksi} // untuk di master dokumen
  TIPE_JURNAL_FAKTUR_BELI               = 1;
  TIPE_JURNAL_STOK_OPNAME               = 2;
  TIPE_JURNAL_RETUR_BELI                = 3;
  TIPE_JURNAL_PEMAKAIAN_SP              = 4;
  TIPE_JURNAL_WORK_SHOP                 = 5;
  TIPE_JURNAL_PENJUALAN_KS              = 6;
  TIPE_JURNAL_SETORAN_KS                = 7;
  TIPE_JURNAL_PENERIMAAN_JAMINAN        = 8;
  TIPE_JURNAL_KEMBALI_JAMINAN           = 9;
  TIPE_JURNAL_TERIMA_DP_PARIWISATA      = 10;
  TIPE_JURNAL_KEMBALI_DP_PARIWISATA     = 11;
  TIPE_JURNAL_PEMBAYARAN_PARIWISATA     = 12;
  TIPE_JURNAL_KOMISI_AGEN               = 13;
  TIPE_JURNAL_CLAIM_PARIWISATA          = 14;
  TIPE_JURNAL_UANG_JALAN                = 15;
  TIPE_JURNAL_MUTASI_DANA               = 16;
  TIPE_JURNAL_PENGELUARAN_DANA_CABANG		= 17;
  TIPE_JURNAL_PENERIMAAN_DANA_CABANG    = 18;
  TIPE_JURNAL_KELUAR_DP_SUPPLIER        = 19;
  TIPE_JURNAL_KEMBALI_DP_SUPPLIER       = 20;
  TIPE_JURNAL_PELUNASAN_HUTANG          = 21;
  TIPE_JURNAL_PENGELUARAN_BIAYA         = 22;
  TIPE_JURNAL_PENERIMAAN_LAIN_LAIN      = 23;
  TIPE_JURNAL_PEMBELIAN_ASSET           = 24;
  TIPE_JURNAL_PELUNASAN_BELI_ASSET      = 25;
  TIPE_JURNAL_PENJUALAN_ASSET           = 26;
  TIPE_JURNAL_PELUNASAN_PIUTANG_ASSET   = 27;
  TIPE_JURNAL_PINJAMAN_DR_RELASI        = 28;
  TIPE_JURNAL_BAYAR_PINJAM_KE_RELASI    = 29;
  TIPE_JURNAL_PINJAMAN_KE_RELASI        = 30;
  TIPE_JURNAL_KEMBALI_DR_RELASI         = 31;
  TIPE_JURNAL_KASBON_PEGAWAI            = 32;
  TIPE_JURNAL_LUNAS_KASBON_PEGAWAI      = 33;
  TIPE_JURNAL_GAJI_PEGAWAI              = 34;
  TIPE_JURNAL_SETOR_MODAL               = 35;
  TIPE_JURNAL_AMBIL_LABA                = 36;
  TIPE_JURNAL_SEWA_DIBAYAR_DIMUKA       = 37;
  TIPE_JURNAL_UMUM                      = 38;
  TIPE_JURNAL_PATCH_SALDO               = 39;
  TIPE_JURNAL_BAYAR_GAJI_PEGAWAI        = 40;
  TIPE_JURNAL_KOMISI_SUPIR_PARIWISATA   = 41;
  TIPE_JURNAL_UMUM_ASET                 = 42;
  TIPE_JURNAL_PENYELESAIAN_KASBON       = 43;
  TIPE_JURNAL_PENCAIRAN_PENSIUN         = 44;
  TIPE_JURNAL_PINJAM_MINJAM             = 45;
  TIPE_JURNAL_PENGEMBALIAN_SP           = 46;
  TIPE_JURNAL_HUTANG_UANG_JALAN         = 47;
  TIPE_JURNAL_PENERIMAAN_PENDING        = 48;
  TIPE_JURNAL_HUTANG_KOMISI_AGEN        = 49;


  TEXT_TIPE_JURNAL_Faktur_Beli  = 'Faktur Beli';
  TEXT_TIPE_JURNAL_Retur_Beli   = 'Retur Beli';
  TEXT_TIPE_JURNAL_STOK_OPNAME  = 'Stok Opname';

  //saldo hutang piutang   FINANCE
  SALDO_HUTANG_SUPP                 = 'H0';
  SALDO_HUTANG_JAMINAN_SUPIR        = 'H1';
  SALDO_HUTANG_ASSET_SUPP           = 'H2';
  SALDO_HUTANG_PINJAMAN_RELASI_SUPP = 'H3';
  SALDO_HUTANG_PINJAMAN_RELASI_CUST = 'H4';


  SALDO_PENERIMAAN_CREDIT_NOTE  = 'V0';

  SALDO_PIUTANG_RETUR_SUPP      = 'R0';

  SALDO_PIUTANG_DP_SUPP         = 'D0';
  SALDO_PIUTANG_DP_ASSET        = 'D1';
  SALDO_HUTANG_DP_PARIWISATA    = 'D2';
  SALDO_HUTANG_DP_ASSET         = 'D3';

  SALDO_PIUTANG_PARIWISATA      = 'P0';
  SALDO_PIUTANG_PINJAMAN_RELASI = 'P1';
  SALDO_PIUTANG_ASSET_SUPP      = 'P2';
  SALDO_PIUTANG_ASSET_CUST      = 'P3';
  SALDO_PIUTANG_PINJAMAN_SUPP   = 'P4';
  SALDO_PIUTANG_PINJAMAN_CUST   = 'P5';
  SALDO_PIUTANG_KS              = 'P6';
  SALDO_PIUTANG_SETORAN         = 'P7';
  SALDO_PIUTANG_SISA_SETORAN    = 'P8';
  SALDO_PIUTANG_KASBON_OPERASIONAL = 'P9';   //dzkri 19102017

  SALDO_PIUTANG_LEBIH_BYR_SUPP  = 'L0';
  SALDO_HUTANG_LEBIH_BAYAR_CUST = 'L1';

    //tipe patch saldo
  PATCH_SALDO_BARANG     = 'CB';
  PATCH_SALDO_KASBANK    = 'CK';
  PATCH_SALDO_ASSET      = 'CA';
  PATCH_SALDO_MODAL      = 'CM';
  PATCH_SALDO_KONSINYASI = 'CS';
  PATCH_SALDO_KASBON_PGW = 'CP';
  PATCH_SALDO_AKUN       = 'CN';



  //patch saldo hutang
  PATCH_SALDO_HUTANG_SUPPLIER         = 'C0';
  PATCH_SALDO_HUTANG_PINJAMAN_RELASI  = 'C1';
  PATCH_SALDO_HUTANG_DP_PARIWISATA    = 'C2';
  PATCH_SALDO_HUTANG_ASSET_SUPPLIER   = 'C3';
  PATCH_SALDO_HUTANG_JAMINAN_SUPIR    = 'C4';

  //patch saldo piutang
  PATCH_SALDO_PIUTANG_CUST            = 'T0';
  PATCH_SALDO_PIUTANG_ASSET_CUST      = 'T1';
  PATCH_SALDO_PIUTANG_PINJAMAN_RELASI = 'T2';
  PATCH_SALDO_PIUTANG_DP_SUPP         = 'T3';
  PATCH_SALDO_PIUTANG_KS              = 'T4';

  //nomor patch saldo
  H_Patch_Saldo_Spare_part = 'PSB';
  H_Patch_Saldo_Hutang     = 'PSH';
  H_Patch_Saldo_Kasbank    = 'PKB';
  H_Patch_Saldo_Piutang    = 'PSP';
  H_Patch_Saldo_Asset      = 'PSA';
  H_Patch_Saldo_KASBON_PGW = 'PKP';
  H_Patch_Saldo_AKUN       = 'PSN';

  SALDO_KASBON = 'K0';

  TYPE_TRANS_PENDAPATAN = 'P';
  TYPE_TRANS_BIAYA      = 'B';
  //TIPE DEBET / CREDIT
  DB_CR_TYPE_DEBIT      = 'D';
  DB_CR_TYPE_CREDIT     = 'C';

  NR_LR_TYPE_NR         = 'N';
  NR_LR_TYPE_LR         = 'L';

  TIPE_BAYAR_UMUM      = 0;
  TIPE_BAYAR_SERVICE   = 1;
  TIPE_BAYAR_WORK_SHOP = 2;

  {untuk tabel akun_transaksi}
  AKUN_BIAYA            = 'B';
  AKUN_PENDAPATAN       = 'P';

    //Setting TUnj & Potongan
  TIPE_TUNJANGAN = 'TUNJANGAN';
  TIPE_POTONGAN  = 'POTONGAN';

  PENGALI_TP_1 = 'PTP1';
  PENGALI_TP_2 = 'PTP2';
  PENGALI_TP_3 = 'PTP3';
  PENGALI_TP_4 = 'PTP4';
  PENGALI_TP_5 = 'PTP5';


  TIPE_IMPORT_GAJI_KOPERASI    = 1;
  TIPE_IMPORT_GAJI_ADM         = 2;
  TIPE_IMPORT_GAJI_LEMBUR      = 3;
  TIPE_IMPORT_GAJI_EXTRA_TUNAI = 4;
  TIPE_IMPORT_GAJI_PESANGON    = 5; //+ fahmi 260517, Pesangon & THR
  TIPE_IMPORT_GAJI_THR         = 6; //+ fahmi 260517, Pesangon & THR

  //+ fahmi 260517, Pesangon & THR
  TIPE_GAJI_PESANGON_TXT = 'PESANGON';
  TIPE_GAJI_THR_TXT      = 'THR';

  {tipe akun}//ridwan 050816

  TIPE_AKUN_AKTIVA          = '1';
  TIPE_AKUN_PASIVA          = '4';
  TIPE_AKUN_MODAL           = '5';
  TIPE_AKUN_PENDAPATAN      = '6';
  TIPE_AKUN_HPP             = '7';
  TIPE_AKUN_BIAYA           = '8';
  TIPE_AKUN_BIAYA_LAIN_LAIN = '9'; //(PENDAPATAN DAN BIAYA LIAN LAIN)

  TIPE_AKUN_AKTIVA_TEXT          = 'AKTIVA';
  TIPE_AKUN_PASIVA_TEXT          = 'PASIVA';
  TIPE_AKUN_MODAL_TEXT           = 'MODAL';
  TIPE_AKUN_PENDAPATAN_TEXT      = 'PENDAPATAN';
  TIPE_AKUN_HPP_TEXT             = 'HPP';
  TIPE_AKUN_BIAYA_TEXT           = 'BIAYA';
  TIPE_AKUN_BIAYA_LAIN_LAIN_TEXT = 'PENDAPATAN & BIAYA LAIN-LAIN';





  NILAI_RUGI                 = 'R';
  NILAI_LABA                 = 'L';

  KASBANK_KAS                = 'K';
  KASBANK_BANK               = 'B';

  //Gunawan +231018
  SEQ_GUDANG_SPM  = 3; //Gunawan +231018


    //u/ keperluan penomoran voucher
  TIPE_BAYAR_IN              = 'I';
  TIPE_BAYAR_OUT             = 'O';
{global variable}
var                                                 
  FOTOPATH: string;
  LOGPATH: string = 'Log\';
  SKINPATH: string = '???\Skin\';
  GLOBAL_REPORT_ID : integer;
  GLOBAL_THEMES : TTMSStyle = tsOffice2003Blue;

{cashier variable}
var
  TAX_RATE : integer = 10; {percent}
  BANK_CODE: string;
  BANK_DAY : integer =  3; {day}
  CANCEL_TOLERANCE: integer = 2; {dlm menit}

  isPOByPass : Boolean;

  SeqSalesIntern, SeqCustIntern : integer;
const
  {$IFDEF ORCL}
    DBisORCL = True;
  {$ENDIF}

  {$IFDEF POSTGRE}
    DBisORCL = False;
  {$ENDIF}
implementation

end.



