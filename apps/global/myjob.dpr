program myjob;

uses
  Forms,
  UEngine in '..\engine\UEngine.pas',
  URecord in '..\engine\URecord.pas',
  USystemMenu in '..\engine\USystemMenu.pas',
  UConst in 'UConst.pas',
  UCreateForm in '..\engine\UCreateForm.pas',
  UDummy in '..\engine\UDummy.pas',
  UTransaksi in '..\engine\UTransaksi.pas',
  UFinance in '..\engine\UFinance.pas',
  UReport in 'UReport.pas',
  Home in 'Home.pas' {frmHome},
  HtmlHlp in 'HtmlHlp.pas',
  MainMenu in 'MainMenu.pas' {frmMainMenu},
  EAGetMailObjLib_TLB in '..\engine\EAGetMailObjLib_TLB.pas',
  EASendMailObjLib_TLB in '..\engine\EASendMailObjLib_TLB.pas',
  OracleConnection in 'OracleConnection.pas' {myConnection: TDataModule},
  Login in 'Login.pas' {frmLogin},
  AddUser in '..\users\AddUser.pas' {frmAddUser},
  ChangePassword in '..\users\ChangePassword.pas' {frmChangePassword},
  KelolaUser in '..\users\KelolaUser.pas' {frmKelolaUser},
  Loading in 'Loading.pas' {frmLoading},
  UGeneral in 'UGeneral.pas',
  MasterCardInput in '..\form\master\MasterCardInput.pas' {frmInputMasterCard},
  MasterCardRekap in '..\form\master\MasterCardRekap.pas' {frmRekapMasterCard},
  MasterJobInput in '..\form\master\MasterJobInput.pas' {frmInputMasterJob},
  MasterJobRekap in '..\form\master\MasterJobRekap.pas' {frmRekapMasterJob},
  LOV in '..\form\master\LOV.pas' {frmLOV},
  DataJualInput in '..\form\transaksi\DataJualInput.pas' {frmInputDataJual},
  DataJualRekap in '..\form\transaksi\DataJualRekap.pas' {frmRekapDataJual},
  DataBeliInput in '..\form\transaksi\DataBeliInput.pas' {frmInputDataBeli},
  DataBeliRekap in '..\form\transaksi\DataBeliRekap.pas' {frmRekapDataBeli},
  CompanyInfo in '..\form\setting\CompanyInfo.pas' {frmCompanyInfo},
  InvoiceInput in '..\form\transaksi\InvoiceInput.pas' {frmInputInvoice},
  InvoiceRekap in '..\form\transaksi\InvoiceRekap.pas' {frmRekapInvoice},
  InvoicePrint in '..\form\print\InvoicePrint.pas' {frmPrintInvoice},
  Invoice2print in '..\form\print\Invoice2print.pas' {frmInvoice2print},
  LaporanCustomer in '..\form\laporan\LaporanCustomer.pas' {frmLaporanCustomer},
  LaporanInvoice in '..\form\laporan\LaporanInvoice.pas' {frmLaporanInvoice},
  LaporanPekerjaan in '..\form\laporan\LaporanPekerjaan.pas' {frmLaporanPekerjaan},
  LaporanSupplier in '..\form\laporan\LaporanSupplier.pas' {frmLaporanSupplier},
  AturKolom in '..\form\laporan\AturKolom.pas' {frmAturKolom},
  BAST in '..\form\print\BAST.pas' {frmBAST},
  BAUT in '..\form\print\BAUT.pas' {frmBAUT},
  LampiranPrint in '..\form\print\LampiranPrint.pas' {frmLampiranPrint},
  LampiranInvoice in '..\form\laporan\LampiranInvoice.pas' {frmLampiranInvoice},
  LaporanProgress in '..\form\laporan\LaporanProgress.pas' {frmLaporanProgress},
  Invoice2printFmt2 in '..\form\print\Invoice2printFmt2.pas' {frmInvoice2printFmt2},
  LampiranPrintJual in '..\form\print\LampiranPrintJual.pas' {frmLampiranPrintJual},
  UpdateTglByr in '..\form\master\UpdateTglByr.pas' {frmUpdateTglByr},
  LampiranTruckingPrint in '..\form\print\LampiranTruckingPrint.pas' {frmLampiranTruckingPrint},
  LampiranTruckingPrintJual in '..\form\print\LampiranTruckingPrintJual.pas' {frmLampiranTruckingPrintJual},
  LampiranGSPrint in '..\form\print\LampiranGSPrint.pas' {frmLampiranGSPrint},
  LampiranGSPrintJual in '..\form\print\LampiranGSPrintJual.pas' {frmLampiranGSPrintJual},
  MasterBankInput in '..\form\master\MasterBankInput.pas' {frmInputMasterBank},
  MasterBankRekap in '..\form\master\MasterBankRekap.pas' {frmRekapMasterBank},
  MasterPPNInput in '..\form\master\MasterPPNInput.pas' {frmInputMasterPPN},
  MasterPPNRekap in '..\form\master\MasterPPNRekap.pas' {frmRekapMasterPPN};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'MyJob';
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(TfrmMainMenu, frmMainMenu);
  Application.CreateForm(TfrmHome, frmHome);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmLoading, frmLoading);
  Application.CreateForm(TfrmInputMasterBank, frmInputMasterBank);
  Application.CreateForm(TfrmRekapMasterBank, frmRekapMasterBank);
  Application.CreateForm(TfrmInputMasterPPN, frmInputMasterPPN);
  Application.CreateForm(TfrmRekapMasterPPN, frmRekapMasterPPN);
  frmLogin.Status('Connecting to Database...');
  frmLogin.Status;
  RearrangeForms;
  Application.Run;
end.
