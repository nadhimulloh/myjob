object frmLoading: TfrmLoading
  Left = 412
  Top = 368
  Cursor = crHourGlass
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Loading'
  ClientHeight = 53
  ClientWidth = 255
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 255
    Height = 53
    Cursor = crHourGlass
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 263
    ExplicitHeight = 63
    object Gauge: TGauge
      Left = 1
      Top = 30
      Width = 253
      Height = 22
      Cursor = crHourGlass
      Align = alBottom
      ForeColor = clSkyBlue
      Progress = 0
      ExplicitTop = 58
      ExplicitWidth = 261
    end
    object sttNamaTrans: TStaticText
      Left = 1
      Top = 11
      Width = 254
      Height = 17
      Cursor = crHourGlass
      Alignment = taCenter
      AutoSize = False
      Caption = 'sttNamaTrans'
      TabOrder = 0
    end
  end
end
