﻿unit Home;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, AdvPanel, AdvSmoothLabel, StdCtrls,MainMenu, AdvSplitter,
  AdvGroupBox, AdvScrollBox, AdvSmoothScrollBar, AdvGlassButton,
  AdvSmoothTabPager, AdvOfficeImage, ImgList, AdvPicture, AdvSmoothImageListBox, AdvAppStyler,
  AdvGlowButton, AdvOfficePager, AdvOfficePagerStylers, Menus, AdvMenus, AdvSmoothPopup,
  AdvSmoothButton, jpeg, AdvMenuStylers;

type
 TMenuGroup = class(TObject)
  type
    TR_Menu = Record
      Menu : TAdvGlowButton;
      SubMenu : TAdvPopupMenu;
    end;
    Ar_Menu = array of TR_Menu;

  var
    Col : integer;
    Row: integer;
    Picture : TAdvOfficeImage;
    Panel : TAdvGroupBox;
    Title : TAdvSmoothLabel;
    ArMenu : ar_menu;

    constructor Create; virtual;
    procedure ConfigureMenu(Caption : string; Index: integer; Top : integer; IsSubMenu : boolean);
    procedure ArrangeMenu;
    destructor Destroy; override;
  end;
  TMenuGroupClass = class of TMenuGroup;

type
  TfrmHome = class(TForm)
    PanelTitle: TAdvPanel;
    lblFontTitle: TLabel;
    lblFontMenu: TLabel;
    MenuPanel: TAdvPanel;
    MenuBox: TAdvScrollBox;
    btnMenu: TAdvGlowButton;
    imgSetting: TAdvOfficeImage;
    imgData: TAdvOfficeImage;
    imgTransaction: TAdvOfficeImage;
    imgReport: TAdvOfficeImage;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    ImageList1: TImageList;
    ImgTransKeuangan: TAdvOfficeImage;
    AdvFormStyler2: TAdvFormStyler;
    AdvPanelStyler2: TAdvPanelStyler;
    AdvPanelStyler3: TAdvPanelStyler;
    lblNoAccess: TAdvSmoothLabel;
    AdvSmoothLabel3: TAdvSmoothLabel;
    AdvOfficePager1: TAdvOfficePager;
    AdvOfficePager11: TAdvOfficePage;
    AdvOfficePager12: TAdvOfficePage;
    AdvOfficePager13: TAdvOfficePage;
    AdvOfficePage1: TAdvOfficePage;
    AdvOfficePage2: TAdvOfficePage;
    AdvSmoothLabel1: TAdvSmoothLabel;
    AdvSmoothLabel2: TAdvSmoothLabel;
    AdvSmoothLabel5: TAdvSmoothLabel;
    AdvSmoothLabel4: TAdvSmoothLabel;
    AdvSmoothLabel7: TAdvSmoothLabel;
    AdvSmoothLabel6: TAdvSmoothLabel;
    AdvSmoothLabel8: TAdvSmoothLabel;
    AdvSmoothLabel9: TAdvSmoothLabel;
    AdvSmoothLabel10: TAdvSmoothLabel;
    AdvSmoothLabel11: TAdvSmoothLabel;
    AdvSmoothLabel12: TAdvSmoothLabel;
    AdvSmoothLabel13: TAdvSmoothLabel;
    AdvSmoothLabel14: TAdvSmoothLabel;
    AdvSmoothLabel15: TAdvSmoothLabel;
    AdvSmoothLabel16: TAdvSmoothLabel;
    AdvSmoothLabel17: TAdvSmoothLabel;
    AdvSmoothLabel18: TAdvSmoothLabel;
    procedure OnMenuMouseEnter(Sender : TObject);
    procedure OnMenuMouseLeave(Sender : TObject);
    procedure OnMenuClick(Sender : TObject);
    procedure lblLogOffClick(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblKirimEmailClick(Sender: TObject);
    procedure AdvPanelGroup1Click(Sender: TObject);
    procedure AdvSmoothLabel1MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel1MouseLeave(Sender: TObject);
    procedure AdvOfficeImage1Click(Sender: TObject);
    procedure AdvOfficeImage2Click(Sender: TObject);
    procedure AdvOfficeImage3Click(Sender: TObject);
    procedure AdvPanelGroup5Click(Sender: TObject);
    procedure AdvOfficeImage6Click(Sender: TObject);
    procedure AdvSmoothLabel2MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel2MouseLeave(Sender: TObject);
    procedure AdvOfficeImage5MouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure AdvSmoothLabel6MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel5MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel5MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel4MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel4MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel8MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel8MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel7MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel7MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel7Click(Sender: TObject);
    procedure AdvSmoothLabel6MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel10Click(Sender: TObject);
    procedure AdvSmoothLabel11Click(Sender: TObject);
    procedure AdvSmoothLabel9Click(Sender: TObject);
    procedure AdvSmoothLabel12Click(Sender: TObject);
    procedure AdvSmoothLabel13Click(Sender: TObject);
    procedure AdvSmoothLabel8Click(Sender: TObject);
    procedure AdvSmoothLabel14Click(Sender: TObject);
    procedure AdvSmoothLabel16Click(Sender: TObject);
    procedure AdvSmoothLabel15Click(Sender: TObject);
    procedure AdvSmoothLabel14MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel16MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel15MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel14MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel16MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel15MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel10MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel10MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel9MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel9MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel13MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel13MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel12MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel11MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel11MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel12MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel17Click(Sender: TObject);
    procedure AdvSmoothLabel17MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel17MouseLeave(Sender: TObject);
    procedure AdvSmoothLabel18Click(Sender: TObject);
    procedure AdvSmoothLabel18MouseEnter(Sender: TObject);
    procedure AdvSmoothLabel18MouseLeave(Sender: TObject);
  private
    { Private declarations }
    MenuList : TList;
    procedure CreateMenuGroup(ATitle : String);
    procedure CreateMenuName(ATitle: string; AMenuId : Integer);
    procedure CreateSubMenu(ATitle : string; AMenuId : integer);
    procedure RearrangePanel;
    procedure EnableComposited(WinControl: TWinControl);
  public
    { Public declarations }
    procedure LoadAllMenu;
  end;

var
  frmHome: TfrmHome;

implementation

uses
  USystemMenu, AdvGDIP, UConst, OracleConnection, UGeneral, KelolaUser;

{$R *.dfm}

const
  MARGIN_LEFT   = 15;
  MARGIN_TOP    = 15;
  MARGIN_RIGHT  = 25;
  MARGIN_BOTTOM = 0;

  TITLE_MARGIN_LEFT = 30;

  MENU_MARGIN_TOP = 5;
  MENU_MARGIN_LEFT = 0;
  MENU_MARGIN_RIGHT = 30;
  MENU_MARGIN_BOTTOM = 15;



{ TfrmHome }

procedure TfrmHome.AdvOfficeImage1Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(2);
end;

procedure TfrmHome.AdvOfficeImage2Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(201);
end;

procedure TfrmHome.AdvOfficeImage3Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(202);
end;

procedure TfrmHome.AdvOfficeImage5MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  self.cursor := crHandPoint;
end;

procedure TfrmHome.AdvOfficeImage6Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(301);
end;

procedure TfrmHome.AdvPanelGroup1Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(1);
end;

procedure TfrmHome.AdvPanelGroup5Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(301);
end;

procedure TfrmHome.AdvSmoothLabel10Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(501);
end;

procedure TfrmHome.AdvSmoothLabel10MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel10.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel10MouseLeave(Sender: TObject);
begin

  AdvSmoothLabel10.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel11Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(503);
end;

procedure TfrmHome.AdvSmoothLabel11MouseEnter(Sender: TObject);
begin

  AdvSmoothLabel11.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel11MouseLeave(Sender: TObject);
begin              
  AdvSmoothLabel11.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel12Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(504);
end;

procedure TfrmHome.AdvSmoothLabel12MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel12.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel12MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel12.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel13Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(508);
end;

procedure TfrmHome.AdvSmoothLabel13MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel13.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel13MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel14.Cursor := crdefault;
end;

procedure TfrmHome.AdvSmoothLabel14Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(505);
end;

procedure TfrmHome.AdvSmoothLabel14MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel14.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel14MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel14.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel15Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(506);
end;

procedure TfrmHome.AdvSmoothLabel15MouseEnter(Sender: TObject);
begin       
  AdvSmoothLabel15.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel15MouseLeave(Sender: TObject);
begin                                  
  AdvSmoothLabel15.Cursor := crdefault;
end;

procedure TfrmHome.AdvSmoothLabel16Click(Sender: TObject);
begin         
  frmMainMenu.OpenForm(507);
end;

procedure TfrmHome.AdvSmoothLabel16MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel16.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel16MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel16.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel17Click(Sender: TObject);
begin
   frmMainMenu.OpenForm(203);
end;

procedure TfrmHome.AdvSmoothLabel17MouseEnter(Sender: TObject);
begin
                
  AdvSmoothLabel17.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel17MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel17.Cursor := crDefault;

end;

procedure TfrmHome.AdvSmoothLabel18Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(3);
end;

procedure TfrmHome.AdvSmoothLabel18MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel18.Cursor := crHandPoint;

end;

procedure TfrmHome.AdvSmoothLabel18MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel18.Cursor := crDefault;

end;

procedure TfrmHome.AdvSmoothLabel1MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel1.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel1MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel1.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel2MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel2.cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel2MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel2.cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel4MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel4.cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel4MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel4.cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel5MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel5.cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel5MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel5.cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel6MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel6.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel6MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel6.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel7Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(302);
end;

procedure TfrmHome.AdvSmoothLabel7MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel7.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel7MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel7.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel8Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(303);
end;

procedure TfrmHome.AdvSmoothLabel8MouseEnter(Sender: TObject);
begin
  AdvSmoothLabel8.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel8MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel8.Cursor := crDefault;
end;

procedure TfrmHome.AdvSmoothLabel9Click(Sender: TObject);
begin
  frmMainMenu.OpenForm(502);
end;

procedure TfrmHome.AdvSmoothLabel9MouseEnter(Sender: TObject);
begin

  AdvSmoothLabel9.Cursor := crHandPoint;
end;

procedure TfrmHome.AdvSmoothLabel9MouseLeave(Sender: TObject);
begin
  AdvSmoothLabel9.Cursor := crDefault;
end;

procedure TfrmHome.CreateMenuGroup(ATitle : String);
var MenuGroup : TMenuGroup;
    index : integer;
begin
  index := 0;
  MenuGroup := TMenuGroup.Create;
  with MenuGroup do
  begin
    Col := index+1;
    Row := 1;

    ATitle := UpperCase(ATitle);

    Title.Caption.Text := ATitle;
    Title.CaptionShadow.Text := ATitle;

    lblFontTitle.Caption := ATitle;
    lblFontTitle.AutoSize := true;
    panel.Width := TITLE_MARGIN_LEFT+lblFontTitle.Width+25;

    Panel.Parent := MenuBox;

    if MenuList.Count = 0 then begin
      Panel.Left := MARGIN_LEFT;
      Panel.Top := MARGIN_TOP;
    end else begin
      Panel.Left := TMenuGroup(MenuList[MenuList.Count-1]).panel.left +
                    TMenuGroup(MenuList[MenuList.Count-1]).panel.width + MARGIN_RIGHT;
      Panel.Top := MARGIN_TOP;
    end;

    Panel.Tag := 0;

    if ATitle = 'SETTING' then
      Picture.Picture := imgSetting.Picture
    else if ATitle = 'DATA' then
      Picture.Picture := imgData.Picture
    else if (ATitle = 'ARMADA') or (ATitle = 'INVENTORY') then
      Picture.Picture := imgTransaction.Picture
    else if ATitle = 'KEUANGAN' then
      Picture.Picture := ImgTransKeuangan.Picture
    else if ATitle = 'LAPORAN' then
      Picture.Picture := imgReport.Picture;


    MenuList.Add(MenuGroup);
  end;
end;

procedure TfrmHome.CreateMenuName(ATitle: string; AMenuId : Integer);
var  Index,Index2,NTop : integer;
begin
  Index := MenuList.Count-1;
  with TMenuGroup(MenuList[index]) do
  begin
    if Length(ArMenu) = 0 then begin
      NTop := Title.Top+Title.Height+MENU_MARGIN_TOP+10;
    end else begin
      NTop := armenu[Length(ArMenu)-1].Menu.top+armenu[Length(ArMenu)-1].Menu.height+MENU_MARGIN_TOP;
    end;

    SetLength(ArMenu,length(ArMenu)+1);

    Index2 := length(ArMenu)-1;

    ConfigureMenu(ATitle, Index2, NTop, AMenuId = -1);

    lblFontMenu.Caption := ATitle;
    lblFontMenu.AutoSize := true;
    if AMenuId = -1 then begin
      ArMenu[Index2].Menu.Width := lblFontMenu.Width + 45
    end else begin
      ArMenu[Index2].Menu.Width := lblFontMenu.Width + 35
    end;

    if ArMenu[Index2].Menu.Width > Panel.Width then begin
      Panel.Width :=  ArMenu[Index2].Menu.Width;
    end;

    Panel.Height := Panel.Height + (MENU_MARGIN_TOP+ArMenu[Index2].Menu.Height);

    ArMenu[Index2].menu.Tag := AMenuId;
    ArMenu[Index2].Menu.OnMouseEnter := OnMenuMouseEnter;
    ArMenu[Index2].Menu.OnMouseLeave := OnMenuMouseLeave;
    ArMenu[Index2].Menu.OnClick := OnMenuClick;

    Panel.Tag := Panel.Tag + 1;
  end;
end;

procedure TfrmHome.CreateSubMenu(ATitle: string; AMenuId: integer);
var index,index2 : integer;
    Item : TMenuItem;
begin
  Index := MenuList.Count-1;
  with TMenuGroup(MenuList[index]) do
  begin
    if Length(ArMenu) <> 0 then begin

      index2 := Length(ArMenu)-1;
      with ArMenu[index2] do
      begin
        Item := TMenuItem.Create(SubMenu);
        Item.Caption := ATitle;
        item.Tag := AMenuId;
        item.OnClick := OnMenuClick;
        Item.ImageIndex := 0;
        SubMenu.Items.Add(Item);
      end;
    end;
  end;
end;

procedure TfrmHome.EnableComposited(WinControl: TWinControl);
var
  i: Integer;
  NewExStyle: DWORD;
begin
  NewExStyle := GetWindowLong(WinControl.Handle, GWL_EXSTYLE) or WS_EX_COMPOSITED;
  SetWindowLong(WinControl.Handle, GWL_EXSTYLE, NewExStyle);

  for i := 0 to WinControl.ControlCount-1 do
    if WinControl.Controls[i] is TWinControl then
      EnableComposited(TWinControl(WinControl.Controls[i]));
end;

procedure TfrmHome.FormCreate(Sender: TObject);
begin
  MenuList := TList.Create;
  AdvFormStyler2.ApplyStyle;
end;

procedure TfrmHome.FormShow(Sender: TObject);
begin
  EnableComposited(Self);
  PanelTitle.visible:=false;
end;

procedure TfrmHome.lblExitClick(Sender: TObject);
begin
  frmMainMenu.lblKeluarClick(Self);
end;

procedure TfrmHome.lblKirimEmailClick(Sender: TObject);
begin
//  Application.CreateForm(TfrmRekapKirimEmail, frmRekapKirimEmail);
//  frmRekapKirimEmail.OnClose := frmMainMenu.ChildFormSingle;
//  frmRekapKirimEmail.Execute;
end;

procedure TfrmHome.lblLogOffClick(Sender: TObject);
begin
  frmMainMenu.lblLogOffClick(Self);
end;

procedure TfrmHome.LoadAllMenu;
var i,AMenuId,LastFormTipe : integer;
    LastMenuGroup : string;
    LastSubMenu : string;
begin
  advofficepager1.activepage := AdvOfficePager11;
  try
    frmHome.WindowState := wsMaximized;
    LastSubMenu := '';
    LastFormTipe := 0;
    for i := 0 to MenuList.Count-1 do TMenuGroup(MenuList[i]).Destroy;
    MenuList.Clear;
    //--------
//    for i:=0 to GlobalSystemMenu_Arr.Count-1 do begin
//      if LastFormTipe <> GlobalSystemMenu_Arr[i].FormTypeNew then LastSubMenu := '';
//
//      AMenuId := GlobalSystemMenu_Arr[i].MenuId;
//
//      if (BisaHapus(AMenuID)) or (BisaLihatRekap(AMenuID)) or (BisaPrint(AMenuID)) or (BisaLihatLaporan(AMenuID))
//        or (BisaEkspor(AMenuID)) or (BisaNambah(AMenuID)) or (BisaEdit(AMenuID)) or (GlobalSystemUser.AccessLevel = LEVEL_OWNER) then begin
//        if LastMenuGroup <> GlobalSystemMenu_Arr[i].MenuGroup then begin
//          if uppercase(GlobalSystemMenu_Arr[i].MenuGroup) = 'MASTER' then CreateMenuGroup('Data')
//          else CreateMenuGroup(GlobalSystemMenu_Arr[i].MenuGroup);
//        end;
//        LastMenuGroup := GlobalSystemMenu_Arr[i].MenuGroup;
//      end;
//
//      if GlobalSystemMenu_Arr[i].SubMenuName <> '' then begin
//        AMenuId := GlobalSystemMenu_Arr[i].MenuId;
//        if (BisaHapus(AMenuID)) or (BisaLihatRekap(AMenuID)) or (BisaPrint(AMenuID)) or (BisaLihatLaporan(AMenuID))
//          or (BisaEkspor(AMenuID)) or (BisaNambah(AMenuID)) or (BisaEdit(AMenuID)) or (GlobalSystemUser.AccessLevel = LEVEL_OWNER) then begin
//          if (LastSubMenu <> GlobalSystemMenu_Arr[i].SubMenuName) then begin
//            CreateMenuName(GlobalSystemMenu_Arr[i].SubMenuName,-1);
//          end;
//          LastSubMenu := GlobalSystemMenu_Arr[i].SubMenuName;
//          LastFormTipe := GlobalSystemMenu_Arr[i].FormTypeNew;
//          CreateSubMenu(GlobalSystemMenu_Arr[i].MenuName,GlobalSystemMenu_Arr[i].MenuId);
//        end;
//      end else begin
//        AMenuId := GlobalSystemMenu_Arr[i].MenuId;
//        if (BisaHapus(AMenuID)) or (BisaLihatRekap(AMenuID)) or (BisaPrint(AMenuID)) or (BisaLihatLaporan(AMenuID))
//          or (BisaEkspor(AMenuID)) or (BisaNambah(AMenuID)) or (BisaEdit(AMenuID)) or (GlobalSystemUser.AccessLevel = LEVEL_OWNER) then begin
//          CreateMenuName(GlobalSystemMenu_Arr[i].MenuName,GlobalSystemMenu_Arr[i].MenuId);
//        end;
//      end;
//
//    end;
    RearrangePanel;
  except
    Inform('Menu error, Silahkan cek data system_menu');
    Application.Terminate;
  end;
end;

procedure TfrmHome.OnMenuClick(Sender: TObject);
var ALabel : TAdvGlowButton;
    AMenu : TMenuItem;
begin
  if Sender is TAdvGlowButton then begin
    ALabel:= (Sender as TAdvGlowButton);
    if (ALabel.Tag > 0) then frmMainMenu.OpenForm(ALabel.Tag);
  end else begin
    AMenu:= (Sender as TMenuItem);
    if (AMenu.Tag > 0) then frmMainMenu.OpenForm(AMenu.Tag);
  end;
end;

procedure TfrmHome.OnMenuMouseEnter(Sender: TObject);
begin
  Screen.Cursor := crHandPoint;
end;

procedure TfrmHome.OnMenuMouseLeave(Sender: TObject);
begin
  Screen.Cursor := crDefault;
end;



procedure TfrmHome.RearrangePanel;
var i : integer;
    IsNoAccess : boolean;
begin
  IsNoAccess := true;
  for i := 0 to MenuList.Count-1 do begin
    TMenuGroup(MenuList[i]).ArrangeMenu;
    if TMenuGroup(MenuList[i]).Panel.Tag > 0 then IsNoAccess := false;
  end;
  lblNoAccess.Visible := IsNoAccess;
end;

procedure TfrmHome.Timer2Timer(Sender: TObject);
begin

end;

{ TMenuGroup }


procedure TMenuGroup.ArrangeMenu;
var i : integer;
begin
  for i := 0 to Length(ArMenu)-1 do begin
    ArMenu[i].Menu.Width := Panel.Width;
  end;
end;

procedure TMenuGroup.ConfigureMenu(Caption : string; Index: integer; Top : integer; IsSubMenu :
  boolean);
begin
  with ArMenu[Index] do
  begin
    Menu := TAdvGlowButton.Create(frmHome);
    Menu.Visible := true;
    Menu.WordWrap := false;
    Menu.Caption := Caption;
    if IsSubMenu then begin
      Menu.Font.Name := 'TAHOMA';
      Menu.Font.Size := 8;
      Menu.DropDownButton := true;
      Menu.DropDownSplit := false;
      Menu.DropDownDirection := ddRight;
    end else begin
      Menu.Font.Name := 'TAHOMA';
      Menu.Font.Size := 8;
    end;
    Menu.Transparent := true;
    Menu.Left := MENU_MARGIN_LEFT;
    Menu.AlignWithMargins := true;
    Menu.Height := 20;
    Menu.Width := 0;
    Menu.Parent := Panel;
    Menu.Top := Top;
    Menu.Picture := frmHome.btnMenu.Picture;
    Menu.Layout := blGlyphLeftAdjusted;
    Menu.BorderStyle := Forms.bsNone;
    menu.AutoSize := false;
    Menu.ControlStyle := menu.controlstyle + [csOpaque];
    Menu.DoubleBuffered := true;

    SubMenu := TAdvPopupMenu.Create(frmHome);
    SubMenu.AutoPopup := true;
    submenu.MenuAnimation := [maLeftToRight];
    SubMenu.MenuStyler := frmHome.AdvMenuOfficeStyler1;
    SubMenu.Images := frmHome.ImageList1;

    Menu.DropDownMenu := SubMenu;
  end;
end;

constructor TMenuGroup.Create;
begin
  Panel := TAdvGroupBox.Create(Panel);
  Panel.DoubleBuffered := true;
  Panel.Width := 20;
  Panel.Visible := true;
  Panel.BorderStyle := AdvGroupBox.bsNone;
  Panel.Color := frmHome.MenuPanel.Color;

  Title := TAdvSmoothLabel.Create(Title);
  Title.Visible := true;
  Title.Caption.ColorStart := clBlack;
  Title.Caption.ColorEnd := clGrayText;
  Title.Caption.GradientType := gtVertical;
  Title.Caption.Font.Size := 13;
  Title.Left := TITLE_MARGIN_LEFT;
  Title.Caption.Font.Style := [fsBold];
  Title.Caption.Location := plTopLeft;
  Title.AlignWithMargins := true;
  Title.Height := 20;
  Title.Top := 5;
  Title.TextRendering := AdvSmoothLabel.tClearType;

  Panel.Height := Title.Top+Title.Height+MENU_MARGIN_BOTTOM;

  Title.Parent := Panel;

  Picture := TAdvOfficeImage.Create(Picture);
  Picture.Left := 0;
  Picture.Top := 0;
  Picture.Width := 30;
  Picture.Height := 30;
  Picture.Visible := true;
  Picture.Parent := Panel;
  Picture.Stretch := true;

  SetLength(ArMenu,0);
end;

destructor TMenuGroup.Destroy;
var i : integer;
begin
  Picture.Free;
  for i := 0 to Length(ArMenu)-1 do begin
    ArMenu[i].SubMenu.Free;
    ArMenu[i].Menu.Free;
  end;
  Panel.Free;
  ArMenu := nil;
  inherited;
end;

end.




