unit Login;
    {DEFINE DEBUG}
interface

uses
  OracleConnection, UConst, 
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DB, ADODB, AdvEdit, ExtCtrls, Buttons, jpeg, AdvSmoothLabel, AdvPanel,
  AdvGlowButton;

type
  TfrmLogin = class(TForm)
    pnlInput: TAdvPanel;
    lblLogin: TLabel;
    lblPassword: TLabel;
    AdvSmoothLabel1: TAdvSmoothLabel;
    txtPass: TAdvEdit;
    txtLoginID: TAdvEdit;
    btnOK: TAdvGlowButton;
    btnCancel: TAdvGlowButton;
    procedure btnOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure txtPassKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    LoginMode: boolean;
    LocalLoginID, LocalLoginName: string;
    LocalAccessLevel, LocalNeedAccessLevel: integer;
  public
    { Public declarations }
    function LogOn: boolean;
    function Authorize(var ALoginID: string; ANeedAccessLevel: integer): boolean;
    procedure Status(const MessageStatus: string = '');

  end;

var
  frmLogin: TfrmLogin;
  NeedAccessLevel: integer = 0;
  myMainForm: TForm = nil;

implementation

uses USystemMenu, UGeneral, MainMenu, UEngine;

{$R *.dfm}

procedure TfrmLogin.btnOKClick(Sender: TObject);
begin
  btnOK.Enabled := false;
  if ((UpperCase(txtLoginID.Text) = 'SYSTEM') and (UpperCase(txtPass.Text) =
    'USER')) then begin
    GlobalSystemUser.Reset;
    LocalLoginID := txtLoginID.Text;
    LocalLoginName := 'Default User';//txtLoginPwd.Text;
    LocalAccessLevel := LEVEL_DEFAULT;
    ModalResult := mrOK;
  end else if (UpperCase(txtLoginID.Text) = 'DEV') and (UpperCase(txtPass.Text)
    = 'DEV') then begin
    GlobalSystemUser.Reset;
    LocalLoginID := txtLoginID.Text;
    LocalLoginName := 'Developer';
    LocalAccessLevel := LEVEL_DEVELOPER;
    ModalResult := mrOK;
  end else
  if (GlobalSystemUser.SelectInDB(txtLoginID.Text, '') = false) then begin
    Alert('User Id/Password salah.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
  end else if (GlobalSystemUser.Passwd <> txtPass.Text)    then begin
    Alert('User Id/Password salah.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
  end else if (GlobalSystemUser.DisableDate<>0) then begin
    Alert('Login disabled.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
  end else begin
    LocalLoginID := txtLoginID.Text;
    LocalLoginName := GlobalSystemUser.UserName;
    LocalAccessLevel := GlobalSystemUser.AccessLevel;
    if (LocalAccessLevel<LocalNeedAccessLevel) then begin
      if (LoginMode) then begin
        Alert('Logon need access level '+IntToStr(LocalNeedAccessLevel)+'.');
      end else
        Alert('Authentication need access level '+IntToStr(LocalNeedAccessLevel)+'.');
      txtLoginID.SetFocus;
    end else begin
      ModalResult := mrOK;
    end;
  end;
  btnOK.Enabled := true;
end;

procedure TfrmLogin.btnCancelClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if LocalAccessLevel = 0 then Application.Terminate;
end;

procedure TfrmLogin.FormCreate(Sender: TObject);
begin
  LoginMode := true;
  try
//    if GlobalSetPerusahaan.logo <> '' then begin
//      path := GetAppPath+'Images\'+GlobalSetPerusahaan.logo;
//      if FileExists(path) then
//        imgLogo.Picture.LoadFromFile(path);
//    end;
  finally
  end;
//  Image2.Visible := false;
//  {$IFDEF PENDING}
//  Image1.Visible := False;
//  Image2.Visible := TRUE;
//  Image2.Left := Image1.Left;
//  AdvSmoothLabel1.Caption.Text := 'PENDING SPA';
//  AdvSmoothLabel1.CaptionShadow.Text := 'PENDING SPA';
//  pnlInput.ColorTo := clGray;
//  lblAll.Caption := '2017';
//  {$ENDIF}
//  Caption := ReplaceSubStr(Caption,'ApplicationTitle',Application.Title);
//{$IFDEF POLITIX}
//  lblCopyright.Visible := false;
//{$ENDIF}
end;

procedure TfrmLogin.FormActivate(Sender: TObject);
begin
//  pnlLogo.Visible := (LoginMode);
  txtPass.MaxLength := 50;
//  lblCopyRight.Visible := (LoginMode);
//  if (LoginMode) then Self.ClientHeight := pnlLogo.Height + pnlInput.Height
//  else Self.ClientHeight := pnlInput.Height;
  txtLoginID.Clear;
  txtPass.Clear;
  txtLoginID.SetFocus;
//  pnlInput.Color := $00FFDEC5;
//  pnlLogo.Color  := $00FFFFFF; //$00F6E9D9
end;

function TfrmLogin.LogOn: boolean;
begin
  Self.Caption := Application.Title+' - Login';
  LoginMode := true;
  LocalNeedAccessLevel := NeedAccessLevel;


  if Assigned(myMainForm) then myMainForm.Hide;
  Result := (ShowModal=mrOK);
  if (Result) then begin
    GlobalSystemUser.UserId := LocalLoginID;
    GlobalSystemUser.UserName := LocalLoginName;
    GlobalSystemUser.AccessLevel := LocalAccessLevel;
//    Copy_All_Saldo(GlobalSystemUser.UserId);
    if Assigned(myMainForm) then myMainForm.Show;
  end else begin
    //if Assigned(myMainForm) then myMainForm.Close;
  end;

  Get_List_master_card(ListCardGlobal);
  Get_list_master_job(ListJobGlobal);
  Get_List_master_Bank(ListBankGlobal);

end;

function TfrmLogin.Authorize(var ALoginID: string; ANeedAccessLevel: integer): boolean;
var LastBorderStyle: TFormBorderStyle; LastCaption: string;
begin
  Self.Caption := Application.Title+' - Authorization';
  LastBorderStyle := BorderStyle; BorderStyle := bsToolWindow;
  LastCaption := Caption; Caption := Application.Title+' - Otorisasi';
  LoginMode := false;
  LocalNeedAccessLevel := ANeedAccessLevel;

  if (ShowModal=mrOK) then ALoginID := LocalLoginID;
  Result := (ModalResult = mrOK);
  BorderStyle := LastBorderStyle;
  Caption := LastCaption;
end;

procedure TfrmLogin.Status(const MessageStatus: string);
begin
  Self.Caption := Application.Title;
  lblLogin.Visible := (MessageStatus='');
  lblPassword.Visible := (MessageStatus='');
  txtLoginID.Visible := (MessageStatus='');
  txtPass.Visible := (MessageStatus='');
  btnOK.Visible := (MessageStatus='');
  btnCancel.Visible := (MessageStatus='');
  pnlInput.Caption.Text := MessageStatus;

  try
    if (MessageStatus<>'') then begin
      if (not Self.Visible) then Self.Show;
      //if (not Self.Visible) then Self.Visible := true;
    end else begin
      if (Self.Visible) then Self.Hide;
      //if (Self.Visible) then Self.Visible := false;
    end;
  except
  end;
  Application.ProcessMessages;
end;

procedure TfrmLogin.txtPassKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then  btnOK.SetFocus;
end;

end.
