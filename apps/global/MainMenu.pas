unit MainMenu;

interface

                                                      //
uses
  OracleConnection, UConst, strUtils, BaseGrid, Grids,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, Menus, FthCtrls, FthSBar, ExtCtrls, ComCtrls,
  ToolWin, AdvGrid, AdvEdit, Gauges, sPanel, ColCombo,  IdTimeServer,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdDayTimeServer,
  AdvOfficeTabSet, AdvOfficeTabSetStylers, AdvMenus, AdvMenuStylers, AdvToolBar,
  AdvToolBarStylers, AdvAppStyler, ActnList, AdvPanel, AdvGlowButton,
  AdvNavBar, GradientLabel, TlHelp32, AdvDateTimePicker, AdvSplitter, jpeg,
  AdvOfficePager, AdvOfficePagerStylers, AdvGlassButton, AdvSmoothMenu, AdvSmoothMegaMenu,
  AdvOfficeStatusBar, AdvOfficeStatusBarStylers, CPort, DB, DBTables;

{Personalized Windows Message for StatusBar}
const
  WM_UpdateStatusBarMsg = WM_User + 8;
  WM_ResizeScrollBoxMsg = WM_User + 16;

type
  TfrmMainMenu = class(TForm)
    ImageList: TImageList;
    Timer1: TTimer;
    ProgressBar: TProgressBar;
    ShortcutBarPanelPopupMenu: TPopupMenu;
    LargeIcons1: TMenuItem;
    SmallIcons1: TMenuItem;
    AddGroup1: TMenuItem;
    RemoveGroup1: TMenuItem;
    RenameGroup1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    HideShortcutBar1: TMenuItem;
    PrinterSetupDialog1: TPrinterSetupDialog;
    ShortcutBarButtonPopupMenu: TPopupMenu;
    N14: TMenuItem;
    OutlookBarShortcut1: TMenuItem;
    Open1: TMenuItem;
    OpeninNewWindow1: TMenuItem;
    N15: TMenuItem;
    RemovefromOutlookBar1: TMenuItem;
    RenameShortcut1: TMenuItem;
    Panel2: TPanel;
    Gauge: TGauge;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    MainMenu1: TAdvMainMenu;
    AdvPopupMenu1: TAdvPopupMenu;
    View1: TMenuItem;
    ViewGoToMenu1: TMenuItem;
    ViewData1: TMenuItem;
    ViewTransaction1: TMenuItem;
    N9: TMenuItem;
    ViewReport1: TMenuItem;
    N10: TMenuItem;
    ViewSettings1: TMenuItem;
    N12: TMenuItem;
    FileLogoff1: TMenuItem;
    FileExit1: TMenuItem;
    N16: TMenuItem;
    PopupMenu1: TPopupMenu;
    F21: TMenuItem;
    dtpSystem: TAdvDateTimePicker;
    AdvOfficeMDITabSet1: TAdvOfficeMDITabSet;
    AdvDockPanel1: TAdvDockPanel;
    AdvToolBar1: TAdvToolBar;
    AdvMenuStyler1: TAdvMenuStyler;
    AdvMenuOfficeStyler2: TAdvMenuOfficeStyler;
    AdvOfficeTabSetOfficeStyler1: TAdvOfficeTabSetOfficeStyler;
    AdvToolBarButton2: TAdvToolBarButton;
    SmallImageList: TImageList;
    objectGradientLabel23TGradientLabel1: TMenuItem;
    advTabStyler: TAdvOfficePagerOfficeStyler;
    NotificationImage: TImageList;
    AdvOfficeStatusBarOfficeStyler1: TAdvOfficeStatusBarOfficeStyler;
    sttWaktu: TStaticText;
    StatusNightAudit: TStaticText;
    Styler1: TMenuItem;
    Office2003Blue1: TMenuItem;
    Office2003Classic1: TMenuItem;
    Office2003Olive1: TMenuItem;
    Office2003Silver1: TMenuItem;
    Office2007Luna1: TMenuItem;
    Office2007Obsidian1: TMenuItem;
    Office2007Silver1: TMenuItem;
    Office2010Black1: TMenuItem;
    Office2010Blue1: TMenuItem;
    Office2010Silver1: TMenuItem;
    erminal1: TMenuItem;
    Whidbey1: TMenuItem;
    Windows71: TMenuItem;
    WindowsVista1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvFormStyler1: TAdvFormStyler;
    AdvPanelStyler1: TAdvPanelStyler;
    AdvOfficePagerOfficeStyler1: TAdvOfficePagerOfficeStyler;
    btnHome: TAdvSplitter;
    StoredProc1: TStoredProc;
    StatusBar1: TAdvOfficeStatusBar;
    UbahPassword1: TMenuItem;
    Setup1: TMenuItem;
    Administratif1: TMenuItem;
    AdvFormStyler2: TAdvFormStyler;

    procedure UpdateStatusBar(var AMessage: TMessage); message WM_UpdateStatusBarMsg;
    procedure ResizeScrollBox(var AMessage: TMessage); message WM_ResizeScrollBoxMsg;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    { MainMenu events }
    procedure FilePrintSetup1Click(Sender: TObject);
    procedure FileLogoff1Click(Sender: TObject);
    procedure FileExit1Click(Sender: TObject);
    procedure ViewGoToMenu1Click(Sender: TObject);
    procedure ViewStatusBar1Click(Sender: TObject);
    procedure HideShortcutBar1Click(Sender: TObject);
    procedure MenuClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure GetPanel(AMenuID: integer);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure AdvToolBar1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure OnLabelClick(Sender: TObject);
    procedure Houver(Sender: TObject);
    procedure UnHouver(Sender: TObject);
    procedure lblKeluarClick(Sender: TObject);
    procedure lblLogOffClick(Sender: TObject);
//    procedure lblGantiPassWordClick(Sender: TObject);
    procedure AdvOfficeMDITabSet1TabClose(Sender: TObject;
      TabIndex: Integer; var Allow: Boolean);
    procedure GradientLabel14Click(Sender: TObject);
//    procedure lblHelpClick(Sender: TObject);
    procedure GradientLabel38Click(Sender: TObject);
    procedure GradientLabel38MouseEnter(Sender: TObject);
    procedure GradientLabel38MouseLeave(Sender: TObject);
    procedure objectGradientLabel23TGradientLabel1Click(Sender: TObject);
    procedure RefreshAll1Click(Sender: TObject);
    procedure Office2003Blue1Click(Sender: TObject);
    procedure Office2003Classic1Click(Sender: TObject);
    procedure Office2003Olive1Click(Sender: TObject);
    procedure Office2003Silver1Click(Sender: TObject);
    procedure Office2007Luna1Click(Sender: TObject);
    procedure Office2007Obsidian1Click(Sender: TObject);
    procedure Office2007Silver1Click(Sender: TObject);
    procedure Office2010Black1Click(Sender: TObject);
    procedure Office2010Blue1Click(Sender: TObject);
    procedure Office2010Silver1Click(Sender: TObject);
    procedure erminal1Click(Sender: TObject);
    procedure Whidbey1Click(Sender: TObject);
    procedure Windows71Click(Sender: TObject);
    procedure WindowsVista1Click(Sender: TObject);
    procedure WindowsXP1Click(Sender: TObject);
    procedure btnHomeClick(Sender: TObject);
    procedure AdvOfficeMDITabSet1Change(Sender: TObject);
    procedure AdvOfficeMDITabSet1TabClick(Sender: TObject;
      TabIndex: Integer);
    procedure UbahPassword1Click(Sender: TObject);

  protected
    { Private declarations }
    LastPanel: TPanel; LastPanelParent: TWinControl;
    LastPanelWidth, LastPanelHeight : integer;
    CurrentTopic: string;
    ListCurrentMenuId: TStringList;
    ListForm : TStringList; //faisal
    SudahBackup : Boolean; ListTheme : TStringList;
    function AddSubMenuItem(aMenuItem:TMenuItem; aCaption:string;AImageIndex: integer; AShortcut: TShortcut; ATag: integer; AEnabled: boolean):TMenuItem;
    procedure AddMainMenuItem(AMenuItem: TMenuItem; ACaption: string; AImageIndex: integer; AShortcut: TShortcut; ATag: integer; AEnabled: boolean);
    procedure AddShortcutBarButton(APanelName: string; AButtonName: string; AImageIndex: integer; ATag: integer);
    procedure ChangeDisplay;
    procedure RefreshFavourite;
    //Cek Apakah Menu ini sudah show belum/
    function FormIsShow(AMenuId : Integer): Boolean;
    procedure ChildFormClose(Sender: TObject; var Action: TCloseAction);

    procedure ChildKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CloseAllChild;
    function Str2PChar(var S: String): PChar;
    function FindProcess(ProcName: String): boolean;
  public
    { Public declarations }
    SudahBukaMenu : Boolean;
    procedure ChildFormSingle(Sender: TObject; var Action: TCloseAction);
    procedure OpenForm(AMenuID: integer; AShowModal: boolean = false);
  end;

var
  frmMainMenu: TfrmMainMenu;
  StatusBarText: string = '';
  ACurrMenuId: Integer;
  MasterDeptId : string = '1';

procedure RearrangeForms;
procedure InitializeApplication;
procedure FinalizeApplication;

function Run(Sender: TForm; AIsDialog: Boolean=False; AIsIconMax : boolean =
  true; AIsWSMax : Boolean = False): TModalResult;
function BisaNambah(AMenuId: Integer): boolean;
function BisaHapus(AMenuId: Integer): boolean;
function BisaEdit(AMenuId: Integer): boolean;
function BisaLihatRekap(AMenuId: Integer): boolean;
function BisaPrint(AMenuId: Integer): boolean;
function BisaLihatLaporan(AMenuId: Integer): boolean;
function BisaEkspor(AMenuId: Integer): boolean;
function BisaTutup(AMenuId: Integer): boolean;
function BisaPosting(AMenuId: Integer): boolean;

procedure StatusBar(Text: string); overload; // set Text='' to clear
procedure StatusBar(Position: integer); overload; // set Position=-1 to hide
procedure ScrollBox(Width, Height: integer);

implementation

uses USystemMenu, AdvStyleIF, UCreateForm, Login, Home
  {$IFDEF PENDING}
  ;
  {$ELSE}
  ,ChangePassword, KelolaUser, UGeneral, MasterppnRekap, MasterCardRekap, MasterJobRekap, DataJualRekap, 
  DataBeliRekap, CompanyInfo, InvoiceRekap, LaporanCustomer, LaporanInvoice, LaporanPekerjaan, LaporanSupplier, 
  LaporanProgress, MasterBankRekap;
  {$ENDIF}


{$R *.dfm}

procedure RearrangeForms;

var i, j: integer; Form: TForm;
  Panel: TPanel; Control: TControl; MainPanelFound: boolean; NeedUpdate: boolean;
  AdvStringGrid: TAdvStringGrid; AdvEdit: TAdvEdit; Button: TButton; DateTimePicker: TDateTimePicker;
  ALabel : TLabel;
  //vComboJurnal: TColumnComboBox;
begin
  frmMainMenu.Caption := Application.Title;
//  GlobalSystemMenu_Arr.LoadFromDB;

  for i:=0 to Application.ComponentCount-1 do
  if (Application.Components[i] is TForm) then begin
    Form := (Application.Components[i] as TForm);
    if (Form.Name='frmMainMenu') or (Form.Name='frmAbout') or (Form.Name=
      'frmLogin') then continue;

    Form.BorderStyle := bsDialog;
    Form.Position := poDesktopCenter;
    Form.WindowState := wsNormal;
    //Form.Color := FORM_BG_COLOR;
    Form.Color  := clpurple;//clBtnFace; //$cccccc;
    MainPanelFound := false;

    for j:=0 to Form.ComponentCount-1 do
    if (Form.Components[j] is TAdvStringGrid) then begin
      AdvStringGrid := (Form.Components[j] as TAdvStringGrid);
      with AdvStringGrid do begin
        Color:=clWindow;
        HintColor:=clInfoBk;
        SelectionColor:=$0094E6FB;
        SelectionColorTo:=$001595EE;
        SelectionTextColor:=clBlack;
        URLColor:=clOlive;
        URLShow:=false;
        ScrollColor:=clNone;
        ActiveRowColor:=clInfoBk;
        ActiveRowShow:=false;
        ActiveCellShow:=false;
        Font.Color:=clWindowText;
        Font.Style:=[];
        Font.Size:=8;
        Font.Name:='Tahoma';
        ActiveCellColor:=$0094E6FB;
        ActiveCellColorTo:=$001595EE;
        ActiveCellFont.Color:=clWindowText;
        ActiveCellFont.Style:=[fsBold];
        ActiveCellFont.Size:=8;
        ActiveCellFont.Name:='Tahoma';
        FixedFont.Color:=clWindowText;
        FixedFont.Style:=[fsBold];
        FixedFont.Size:=8;
        FixedFont.Name:='Tahoma';
        Balloon.BackgroundColor:=clNone;
        Balloon.TextColor:=clNone;
        Balloon.Transparency:=0;
        Bands.PrimaryColor:=clInfoBk;
        Bands.SecondaryColor:=clWindow;
        Bands.Active:=false;
        SortSettings.IndexColor:=clYellow;
        FloatingFooter.Color:=clBtnFace;
        ControlLook.CheckSize:=15;
        ControlLook.Color:=clBlack;
        ControlLook.CommentColor:=clRed;
        ControlLook.ControlStyle:=csWinXP;
        ControlLook.FixedGradientFrom:=$00FCE1CB;
        ControlLook.FixedGradientTo:=$00E0A57D;
        ControlLook.RadioSize:=10;
        ControlLook.FlatButton:=false;
        ControlLook.ProgressBorderColor:=clGray;
        ControlLook.ProgressXP:=false;
        ControlLook.NoDisabledCheckRadioLook := True;
        ControlLook.NoDisabledButtonLook := True;
        Look:=glXP;
        SearchFooter.Color:=$00FCE1CB;
        SearchFooter.ColorTo:=$00E0A57D;
        GridLineColor:=clSilver;
        Grouping.HeaderColor:=$00D68759;
        Grouping.HeaderColorTo:=$00933803;
        Grouping.HeaderTextColor:=clWhite;
        Grouping.HeaderUnderline:=true;
        Grouping.HeaderLineColor:=clOlive;
        Grouping.HeaderLineWidth:=2;
        Grouping.SummaryColor:=$00FADAC4;
        Grouping.SummaryColorTo:=$00F5BFA0;
        Grouping.SummaryTextColor:=clNone;
        Grouping.SummaryLine:=false;
        Grouping.SummaryLineColor:=clOlive;
        Grouping.SummaryLineWidth:=2;
        BackGround.Top:=0;
        BackGround.Left:=0;
//        BackGround.Display:=[bdTile];
        BackGround.Cells:=bcNormal;
        BackGround.Color:=clWindow;
        BackGround.ColorTo:=clBtnFace;
        Flat := False;
        WordWrap := False;
        SearchFooter.ShowClose := false;
        Navigation.AllowClipboardShortCuts := true;
//        con;
      end;
      if AdvStringGrid.Tag = -1 then begin
        AdvStringGrid.Bands.Active          := False;
        AdvStringGrid.SortSettings.Show     := False;
      end else begin
        AdvStringGrid.SortSettings.Show     := True;
        AdvStringGrid.SortSettings.Column   := AdvStringGrid.ColCount-1;
        AdvStringGrid.Bands.Active          := True;
        AdvStringGrid.SortSettings.AutoFormat:= True;
        AdvStringGrid.SortSettings.NormalCellsOnly := True;
      end;
      AdvStringGrid.Invalidate;

    end else if (Form.Components[j] is TAdvEdit) then begin
      AdvEdit := (Form.Components[j] as TAdvEdit);
      AdvEdit.DisabledColor := AdvEdit.Color;
      AdvEdit.Font.Color := clBlack;
      //case AdvEdit.EditType of
        //etMoney:
        //etFloat:
        //etNumeric:
      //end;
    end else if (Form.Components[j] is TDateTimePicker) then begin
      DateTimePicker := (Form.Components[j] as TDateTimePicker);
      DateTimePicker.Format := '';
      if DateTimePicker.Kind=dtkDate then begin
        DateTimePicker.DateMode := dmComboBox;
        DateTimePicker.DateFormat := dfShort;
        DateTimePicker.Format := 'dd/MM/yyyy';
      end;
//    end else if (Form.Components[j] is TColumnComboBox) then begin
//      vComboJurnal := (Form.Components[j] as TColumnComboBox);
////      vComboJurnal.Visible := JENIS_JURNAL <> UConst.IS_JURNAL_BULANAN;
    end else if (Form.Components[j] is TButton) then begin
      Button := (Form.Components[j] as TButton);
      if (Button.Name='btnCancel') then
        Button.Cancel := true
      // handy
      else if (Button.Name='btnFilter') then
        Button.Visible  := False
      else if (UpperCase(Button.Name)='BTNPRINT') or (UpperCase(Button.Name)='BTNCETAK') or (UpperCase(Button.Name)='BTNPRIN')then
        Button.Caption  := '&Print'
      else if (UpperCase(Button.Name)='BTNEKSPOR') or (UpperCase(Button.Name)='BTNEKSPORT') or (UpperCase(Button.Name)='BTNEXCEL')then
        Button.Caption  := '&Export';

    end else if (Form.Components[j] is TPanel) then begin
      Panel := (Form.Components[j] as TPanel);

      if (UpperCase(Panel.Name)='MAINPANEL') and (Panel.Name<>'MainPanel') then begin
        Panel.Name := 'MainPanel';
        //Panel.Update;
        //Panel.Caption := 'Silahkan tunggu sebentar....';


      end;
      Panel.ParentBackground := True;
      Panel.ParentColor := True;
      panel.Color := $00BDA4A5;
      if (Panel.Name='MainPanel') then
        MainPanelFound := true
      else Panel.Caption := '';
        //
    end else if (Form.Components[j] is TLabel) then begin
      ALabel := (Form.Components[j] as TLabel);
    	if ALabel.Tag = 0 then
        ALabel.Transparent := True
      else ALabel.Transparent:= False;
      if UpperCase(ALabel.Name) = 'LBLJURNAL' then
//        ALabel.Visible := JENIS_JURNAL <> UConst.IS_JURNAL_BULANAN;
      ALabel.ParentColor := True;
      ALabel.Font.Color := clBlack;
    end;

    NeedUpdate := false;
    for j:=0 to GlobalSystemMenu_Arr.Count-1 do
      if (UpperCase(GlobalSystemMenu_Arr[j].FormName)=UpperCase(Form.Name)) and
        (GlobalSystemMenu_Arr[j].FormType in [ftLookup,ftBrowse]) then
        NeedUpdate := true;

    if (NeedUpdate) and (not MainPanelFound) then begin
      Panel := TPanel.Create(Form);
      Panel.Name := 'MainPanel';
      Panel.Width := Form.ClientWidth;
      Panel.Height := Form.ClientHeight;
      Panel.Caption := '';
      //Panel.Color := FORM_BG_COLOR;
      Panel.BevelOuter := bvNone;
      for j:=0 to Form.ComponentCount-1 do
      if (Form.Components[j] is TControl) then begin
        Control := (Form.Components[j] as TControl);
        if (Control<>Panel) and (Control.Parent=Form) then
          Control.Parent := Panel;
        if (Control is TAdvStringGrid) then begin
          AdvStringGrid := (Control as TAdvStringGrid);
          AdvStringGrid.Anchors := [akLeft,akTop,akRight,akBottom];
        end else begin
          if (Control.Left>Panel.Width div 2) then
            Control.Anchors := Control.Anchors - [akLeft] + [akRight];
          if (Control.Top>Panel.Height div 2) then
            Control.Anchors := Control.Anchors - [akTop] + [akBottom];
        end;
      end;
      Panel.Parent := Form;
      Panel.Align := alClient;
    end;
  end;
end;

procedure InitializeApplication;
begin
end;

procedure FinalizeApplication;
begin
  GlobalSystemUser.SystemAccess_Arr.SaveUsageCountOnDB;
  GlobalSystemUser.SystemAccess_Arr.Clear;
  //Application.Terminate;
end;

function Run(Sender: TForm; AIsDialog: Boolean=False; AIsIconMax : boolean = true; AIsWSMax : Boolean = False): TModalResult;
var X: TControl;
begin
  Result := 0;
  RearrangeFormChild(Sender, AIsIconMax);
  if Sender.FormStyle = fsMDIChild then begin
    Sender.WindowState := wsMaximized;
  end else begin
    if AIsWSMax = True then Sender.WindowState := wsMaximized
    else Sender.WindowState := wsNormal;
    X := TControl(Sender.FindComponent('MainPanel'));
    if (X<>nil) and (X.Parent<>Sender) then
      Result := mrOK
    else begin
      Sender.Position := poDesktopCenter;
      if AIsDialog then Sender.BorderStyle := bsDialog
      else Sender.BorderStyle := bsSizeable;
      Result := Sender.ShowModal;
    end;
  end;
end;

function BisaAccess(AMenuId: Integer; Access: TAccessList): boolean;
var X: TSystemAccess;
begin
  X := GlobalSystemUser.SystemAccess_Arr.GetByMenuID(AMenuId);
  Result := (X<>nil) and (Access in X.AccessList);
  if (GlobalSystemUser.AccessLevel>=LEVEL_OWNER ) then Result := true;
end;
//, , , , , ,
function BisaNambah(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksInput);
end;

function BisaEdit(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksEdit);
end;

function BisaHapus(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksHapus);
end;

function BisaLihatRekap(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksRekap);
end;

function BisaLihatLaporan(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksReport);
end;

function BisaPrint(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksCetak);
end;

function BisaEkspor(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksExport);
end;

function BisaTutup(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksTutup);
end;

function BisaPosting(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksPosting);
end;

procedure ScrollBox;
begin
  PostMessage(Application.MainForm.Handle,WM_ResizeScrollBoxMsg,Width,Height);
  Application.ProcessMessages;
end;

procedure StatusBar(Text: string);
begin
  StatusBarText := Text;
  PostMessage(Application.MainForm.Handle,wm_UpdateStatusBarMsg,0,0);
  Application.ProcessMessages;
end;

procedure StatusBar(Position: integer);
begin
  if (Position=-1) then
    PostMessage(Application.MainForm.Handle,wm_UpdateStatusBarMsg,2,0)
  else
    PostMessage(Application.MainForm.Handle,wm_UpdateStatusBarMsg,1,Position);
  Application.ProcessMessages;
end;

procedure TfrmMainMenu.FormCreate(Sender: TObject);
var path : string;
begin
  ListCurrentMenuId := TStringList.Create;
  Self.DoubleBuffered := true;
  ListTheme := TStringList.Create;
  path := vApplikasiPath+'\theme.dll';
  if FileExists(path) then begin
    ListTheme.LoadFromFile(path);
  end else ListTheme.Add('');

  LastPanel := nil;  LastPanelParent := nil;
  LastPanelWidth := 0; LastPanelHeight := 0;

end;

procedure TfrmMainMenu.FormDestroy(Sender: TObject);
begin
  ListCurrentMenuId.Free;
  ListTheme.Free;
end;

function TfrmMainMenu.FormIsShow(AMenuId: Integer): Boolean;
var
  i : Integer;
begin
  Result := False;
  for i := 0 to ListCurrentMenuId.Count-1 do begin
    if ListCurrentMenuId.Strings[i] = IntToStr(AMenuId) then begin
      AdvOfficeMDITabSet1.ActiveTabIndex := i;
      Result := True;
      Break;
    end;
  end;
end;

procedure TfrmMainMenu.FormShow(Sender: TObject);
begin
  SudahBackup := FALSE;
  if (GlobalSystemUser.UserID='') then FileLogoff1Click(nil);
  OpenForm(0);
end;

procedure TfrmMainMenu.GradientLabel14Click(Sender: TObject);
var cmd, path, AUser, APaswd, AServis : string;
   ada : boolean; AList : TStringList;
begin
  if SudahBackup = false then begin
    if Confirmed('Database hendak di-backup ?') then begin
      AList := TStringList.Create;

      vApplikasiPath := GetCurrentDir;
      path := vApplikasiPath+'\profile.dll';
      if FileExists(path) then
      AList.LoadFromFile(path);

      if AList.Count <> 0 then begin
      //inv_sinar_surya/inv_sinar_surya/10.100.100.113/xe

        AUser := EkstrakString(AList.Strings[0], '/', 1);
        APaswd := EkstrakString(AList.Strings[0], '/', 2);
        AServis := EkstrakString(APaswd, '@', 2);
        APaswd := EkstrakString(APaswd, '@', 1);

        path := 'Provider=MSDAORA.1;Password='+APaswd+';User ID='+AUser+';Data Source='+AServis;
        cmd := 'exp '+AUser+'/'+AUser+'@'+AServis+' file=Grafika_Wisata'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
      end else begin
        cmd := 'exp gracia_finance/Grafika_Wisata@xe file=Grafika_Wisata'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
      end;
      if WinExec(Str2PChar(cmd),SW_SHOWDEFAULT) < 31 then
        showmessage('Gagal backup data.')
      else begin
        repeat
          ada := FindProcess('exp.exe')
        until ada = false;
        showmessage('Data berhasil dibackup.')
      end;
      AList.Destroy;
    end;
  end;
end;

procedure TfrmMainMenu.GradientLabel38Click(Sender: TObject);
var ALabel : TLabel;
begin
  ALabel:= (Sender as TLabel);
  if (ALabel.Tag > 0) then OpenForm(ALabel.Tag);
end;

procedure TfrmMainMenu.GradientLabel38MouseEnter(Sender: TObject);
begin
  (Sender as TLabel).Font.Style:= [fsUnderLine];
  (Sender as TLabel).Font.Color:= clOlive;
end;

procedure TfrmMainMenu.GradientLabel38MouseLeave(Sender: TObject);
begin
  (Sender as TLabel).Font.Style := [];
  (Sender as TLabel).Font.Color := $00423740;
end;

procedure TfrmMainMenu.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Resize := (NewWidth>400) and (NewHeight>300);
end;

procedure TfrmMainMenu.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if confirmed('Program akan ditutup. Anda yakin?') then begin
    FinalizeApplication;
  end else Action := caNone;
end;

procedure TfrmMainMenu.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  CASE key of
//F1 Pembelian                      F2 Retur Beli                    F3 Penjualan                         F4 Retur Jual                      F5 Stok Opname                     F6 Mutasi Gudang
//F7 Mutasi Barang               F8 Barang                         F9 Customer                          F10 Supplier                       F11 Kategori                            F12 Satuan                     ESC Close All
    VK_F1 : OpenForm(301);
    VK_F2 : OpenForm(302);
    VK_F3 : OpenForm(303);
    VK_F4 : OpenForm(304);
    VK_F5 : OpenForm(305);
    VK_F6 : OpenForm(306);
    VK_F7 : OpenForm(307);
    VK_F8 : OpenForm(308);
    VK_ESCAPE : CloseAllChild;
  END;
end;

procedure TfrmMainMenu.ChangeDisplay;
var i: integer; Menu,subMenu : TMenuItem; LastSubmenuName,
LastMenuGroup, LastSubMenu: string;
//AformType : TFormType;
begin
//  RearrangeForms()
  ListCurrentMenuId.Clear;
  ListCurrentMenuId.Add('0');
  LastSubmenuName := '';subMenu := nil;

  StatusBar1.Panels[0].Text := 'Login : '+GlobalSystemUser.UserId;
//    sttLogin.Caption := 'Login : '+GlobalSystemUser.UserId;
//    sttLogin.Height := 14;

//  StatusBar.Panels[0].Text := '  Login : '+GlobalSystemUser.UserId;
//  lblUserLocation.Caption := GlobalSystemUser.UserName;
  tag := 0;
//    frmHome.execute;

  if (GlobalSystemUser.UserId<>'') then begin
    // Outlook Bar
//    ShortcutBar.Panels.Clear;
//    ShortcutBar.Panels.Add.Text := 'Favourite'; // favourite panel
    if GlobalSystemUser.AccessLevel > LEVEL_OWNER then
      GlobalSystemUser.SystemAccess_Arr.LoadFromDB_All(GlobalSystemUser.UserId,MasterDeptId)
    else GlobalSystemUser.SystemAccess_Arr.LoadFromDB(GlobalSystemUser.UserId,0, MasterDeptId);

    GlobalSystemMenu_Arr.LoadFromDB(MasterDeptId);
//    for i:=0 to GlobalSystemUser.SystemAccess_Arr.Count-1 do
//    with GlobalSystemUser.SystemAccess_Arr[i] do begin
//      if (Trim(OutlookPanel)='') or (Trim(OutlookButton)='') then continue;
//      Self.AddShortcutBarButton(OutlookPanel,OutlookButton,SystemMenu.ImageIndex,SystemMenu.MenuId);
//    end;
//    ShortcutBar.PanelIndex := 0;
    GlobalSystemUser.SystemAccess_Arr.RecalculateUsage;
    Self.RefreshFavourite;

//    ViewOutlookBar1.Checked := false;
  //  ViewOutlookBar1Click(nil);
    //ViewOutlookBar1.Visible := true;
    FileLogoff1.Caption := 'Log Off';
    //USaldo.CopySaldoAwal(GlobalSystemUser.UserId);
  end else begin
//    ViewOutlookBar1.Checked := true;
//    ViewOutlookBar1Click(nil);
  //  ViewOutlookBar1.Visible := false;
    FileLogoff1.Caption := 'Log On';
  end;

  // Main Menu
  //FileNewData1.Clear;
  //FileNewTransaction1.Clear;  
//  Developer1.visible := GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER;
  ViewData1.Clear;
  ViewTransaction1.Clear;
  ViewReport1.Clear;
  ViewSettings1.Clear;
  Administratif1.Clear;

  LastSubMenu := '';
//  submenuform := nil;
  for i:=0 to GlobalSystemMenu_Arr.Count-1 do begin
//    AMenuID := GlobalSystemMenu_Arr[i].MenuId;
//    if (BisaHapus(AMenuID)) or (BisaLihatRekap(AMenuID)) or (BisaPrint(AMenuID)) or (BisaLihatLaporan(AMenuID))
//      or (BisaEkspor(AMenuID)) or (BisaNambah(AMenuID)) or (BisaEdit(AMenuID)) or (GlobalSystemUser.AccessLevel = LEVEL_OWNER) then begin
      case GlobalSystemMenu_Arr[i].FormTypeNew of
//        ftLookup: Menu := ViewData1;
//        ftBrowse: Menu := ViewTransaction1;
//        ftReport: Menu := ViewReport1;
//        ftSetting: Menu := ViewSettings1;
        Form_type_master : Menu := ViewData1;
        Form_type_transaksi : Menu := ViewTransaction1;
        Form_type_laporan : Menu := ViewReport1;
        Form_type_setting : Menu := ViewSettings1;
        Form_type_keuangan : Menu := ViewTransaction1;
        Form_type_administratif : Menu := Administratif1;
        else Menu := nil;
      end;
      if (Menu=nil) then continue;
      if (GlobalSystemMenu_Arr[i].MenuGroup<>LastMenuGroup) {and (AformType <> FormType)} then begin
        LastMenuGroup := GlobalSystemMenu_Arr[i].MenuGroup;
        if (i>0) then    Self.AddMainMenuItem(Menu, '-', -1, 0, 0, false);
        //Self.AddMainMenuItem(Menu,'******* '+GlobalSystemMenu_Arr[i].MenuGroup+' *******', -1, 0, 0, false);
        //AformType := FormType;
      end;
      if copy(GlobalSystemMenu_Arr[i].MenuName,0,1) <> '*' then begin
       //&&&&&&&&&&&&& add by chan &&&&&&&&&&&&&&
//        if (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'REKAP') or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'PIUTANG') or
//             (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'RELASI') or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'PURCHASE ORDER') or
//             (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'MUTASI UANG') or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'PERSEDIAAN') or
//             (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'PENGELUARAN DANA')  or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'PENERIMAAN DANA') or
//             (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'BARANG')  or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'BENGKEL  ') or
//              (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'SHOW ROOM  ')  or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'HUTANG') or
//              (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'PENERIMAAN BARANG')  or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'BENGKEL') or
//              (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'ACCOUNTING')  or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'RAPORT/LAPORAN') or
//              (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'KEUANGAN') or (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) = 'SHOW ROOM') then begin
        if (UpperCase(GlobalSystemMenu_Arr[i].SubMenuName) <> '') then begin
           if LastSubmenuName <> GlobalSystemMenu_Arr[i].SubMenuName then begin
             LastSubmenuName := GlobalSystemMenu_Arr[i].SubMenuName;
             subMenu := Self.AddSubMenuItem(Menu,GlobalSystemMenu_Arr[i].SubMenuName,0,0,0,True);
           end;

           Self.AddMainMenuItem(subMenu,GlobalSystemMenu_Arr[i].MenuName+' ('+IntToStr(GlobalSystemMenu_Arr[i].MenuID)+')', GlobalSystemMenu_Arr[i].ImageIndex, GlobalSystemMenu_Arr[i].Shortcut, GlobalSystemMenu_Arr[i].MenuId,
          (GlobalSystemUser.AccessLevel>=LEVEL_DEFAULT) or
          (GlobalSystemUser.SystemAccess_Arr.IndexOf(GlobalSystemMenu_Arr[i].MenuId)<>-1));

        end else  begin
          LastSubmenuName := '';
          if (GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER) or
             ((UpperCase(GlobalSystemMenu_Arr[i].MenuName) <> 'MASUKAN MENU DISINI JIKA ADA PENGECUALIAN')) then begin
            Self.AddMainMenuItem(Menu, GlobalSystemMenu_Arr[i].MenuName+' ('+IntToStr(GlobalSystemMenu_Arr[i].MenuID)+')', GlobalSystemMenu_Arr[i].ImageIndex, GlobalSystemMenu_Arr[i].Shortcut, GlobalSystemMenu_Arr[i].MenuId,
            (GlobalSystemUser.AccessLevel>=LEVEL_DEFAULT) or
            (GlobalSystemUser.SystemAccess_Arr.IndexOf(GlobalSystemMenu_Arr[i].MenuId)<>-1));
          end;
        end;
      end;
//    end;
  end;

  frmHome.LoadAllMenu;
end;

procedure TfrmMainMenu.ChildFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  AdvToolBar1.RemoveMDIChildMenu(TForm(Sender));
  Action   := caFree;
end;

procedure TfrmMainMenu.ChildFormSingle(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmMainMenu.ChildKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  if (ssCtrl in Shift) and (Key = VK_F2) then
//    Button1.Click;
end;

procedure TfrmMainMenu.CloseAllChild;
var i : Integer;
begin
  for i := 1 to MDIChildCount-1 do begin
    MDIChildren[i].Close;
  end;
end;

procedure TfrmMainMenu.erminal1Click(Sender: TObject);
begin
//  myConnection.AdvAppStyler1.Style := tsTerminal;
//  ListTheme.Strings[0] := STYLE_TERMINAL;
//  ListTheme.SaveToFile('theme.dll');
end;

function TfrmMainMenu.Str2PChar(var S: String): PChar;
begin
  IF (S <> '')
  THEN BEGIN
         IF (S[Length(S)]<>#0) { It has to end with a zero byte... }
         THEN S := S + #0;
       END;
  Str2PChar := @S[1];
end;

function TfrmMainMenu.FindProcess(ProcName: String): boolean;
var ExeName : String;
    List    : TStringList;
    Found   : Boolean;
    I       : Integer;
    proc : PROCESSENTRY32;
    hSnap : HWND;
    Looper : BOOL;
begin
    List := TStringList.Create;
    proc.dwSize := SizeOf(Proc);
    hSnap := CreateToolhelp32Snapshot(TH32CS_SNAPALL,0);
    Looper := Process32First(hSnap,proc);
    Found := False;
    while Integer(Looper) <> 0 do
    begin
      ExeName := ExtractFileName(proc.szExeFile);
      List.Add(ExeName);
      Looper := Process32Next(hSnap,proc);
    end;
    For I := 0 to List.Count - 1 do
    begin
      If List.Strings[I] = ProcName then
        Found := true
    end;
    List.Free;
    result := Found;
    CloseHandle(hSnap);
end;

procedure TfrmMainMenu.RefreshAll1Click(Sender: TObject);
begin
   // UNTUK DISABLE DATE KALAU DATA AKTIF DI ISI STRING KOSONG BUKAN FORMATDATETIME

//  Get_List_Master_Kategori_1(ListKategori1Global, boNone);
//
//  Get_List_Master_Kategori_2(ListKategori2Global, boNone);
//
//  Get_List_Master_Kategori_3(ListKategori3Global, boNone);
//
//  Get_List_master_Satuan(ListSatuanGlobal, boNone);
//
//  Get_List_Master_akun(ListAkunGlobal, boNone);
//
//  Get_List_master_supplier(ListSupplierGlobal, boNone);
//
//  Get_List_master_Pegawai(ListPegawaiGlobal, boNone);
//
//  Get_List_Master_Barang(ListBarangGlobal,boNone);
//
//  Get_List_Master_Customer(ListCustomerGlobal, boNone);
//
//  Get_List_Master_sales_Area(ListAreaGlobal,boNone);
//
//  get_list_master_kas_bank(ListKasBankGlobal);
//
//  Get_List_Master_Relasi(ListRelasiGlobal, boNone);
//
//  Get_List_master_penyimpanan(ListPenyimpananGlobal,boNone);

//  Get_list_master_batch(ListBatchGlobal);
//
//  Get_list_master_batch(ListBatchPerKodeDanBarangGlobal,0,'nomor||''#''||barang_seq');
  Inform('Refresh data berhasil.');
end;

procedure TfrmMainMenu.RefreshFavourite;
var i: integer; Prefix: string;
begin
//  ShortcutBar.Panels[0].Buttons.Clear;
  for i:=0 to 9 do
    with GlobalSystemUser.SystemAccess_Arr.TopUsage[i] do
      if Assigned(GlobalSystemUser.SystemAccess_Arr.TopUsage[i]) and
        (GlobalSystemUser.SystemAccess_Arr.TopUsage[i].UsageCount>0) then begin
        case SystemMenu.FormType of
          ftDetail: Prefix := 'New';
          ftLookup: Prefix := 'Data';
          ftReview: Prefix := 'New';
          ftBrowse: Prefix := 'Transaction';
          ftReport: Prefix := 'Report';
          ftSetting: Prefix := 'Setting';
        end;

        Self.AddShortcutBarButton('Favourite',Prefix+' '+SystemMenu.MenuName,SystemMenu.ImageIndex,SystemMenu.MenuId);
      end;
//  ShortCutBar.Invalidate;
end;


procedure TfrmMainMenu.OpenForm(AMenuId: integer; AShowModal: boolean);
var Form2: TForm; SystemMenu: TSystemMenu; SystemAccess: TSystemAccess;
begin
  if (AMenuID <> 0) then begin
    if FormIsShow(AMenuID) = True then Exit;
    ListCurrentMenuId.Add(IntToStr(AMenuID));

    SystemMenu := GlobalSystemMenu_Arr.GetByMenuID(AMenuId);
    if (SystemMenu=nil) then
      raise EAbort.Create('SystemMenu not found.')
    else begin
      Form2 := TForm(Application.FindComponent(SystemMenu.FormName));
    end;
    SystemAccess := GlobalSystemUser.SystemAccess_Arr.GetByMenuID(AMenuID);
    if (SystemAccess<>nil) then begin
      SystemAccess.IncUsageCount;
      GlobalSystemUser.SystemAccess_Arr.RecalculateUsage;
      Self.RefreshFavourite;
    end;

    ACurrMenuId := AMenuID;
    if (not BisaHapus(AMenuID)) and (not BisaLihatRekap(AMenuID)) and (not BisaPrint(AMenuID)) and (not BisaLihatLaporan(AMenuID))
        and (not BisaEkspor(AMenuID)) and (not BisaNambah(AMenuID)) and (not BisaEdit(AMenuID)) then begin
      Alert(MSG_UNAUTHORISED_ACCESS);
      ListCurrentMenuId.Delete(ListCurrentMenuId.Count-1);
      Exit;
    end;
  end else begin
    if AdvOfficeMDITabSet1.AdvOfficeTabCount > 0 then begin
      AdvOfficeMDITabSet1.ActiveTabIndex := 0;
    end else begin
      frmHome.OnClose := frmMainMenu.ChildFormSingle;
      frmMainMenu.AdvToolBar1.AddMDIChildMenu(frmHome);
      frmHome.FormStyle := fsMDIChild;
      frmMainMenu.AdvOfficeMDITabSet1.AddTab(frmHome);
      frmMainMenu.AdvOfficeMDITabSet1.AdvOfficeTabs[0].ShowClose := false;
      ListCurrentMenuId.Clear;
      ListCurrentMenuId.Add('0');
    end;
    exit;
  end;
  case AMenuID of
      //seting
      1: begin
            Application.CreateForm(TfrmKelolaUser, frmKelolaUser);
            frmKelolaUser.OnClose := ChildFormSingle;
            frmKelolaUser.Execute(AMenuID);
            ListCurrentMenuId.Delete(ListCurrentMenuId.Count-1);
         end;
      2: begin
            Application.CreateForm(TfrmCompanyInfo, frmCompanyInfo);
            frmCompanyInfo.OnClose := ChildFormSingle;
            frmCompanyInfo.Execute;
            ListCurrentMenuId.Delete(ListCurrentMenuId.Count-1);
         end;
      3 : Form2 := TfrmRekapMasterPPN.Create(Self);

      // master
      201 : Form2 := TfrmRekapMasterCard.Create(Self);
      202 : Form2 := TfrmRekapMasterJob.Create(Self);
      203 : Form2 := TfrmRekapMasterBank.Create(Self);

//       // Transaksi barang
      301 : Form2 := TfrmRekapDataJual.Create(Self);
      302 : Form2 := TfrmRekapDataBeli.Create(Self);
      303 : Form2 := TfrmRekapInvoice.Create(Self);

//    // Laporan
      501 : Form2 := TfrmLaporanCustomer.Create(Self);
      502 : Form2 := TfrmLaporanSupplier.Create(Self);
      503 : Form2 := TfrmLaporanPekerjaan.Create(Self);
      504, 505, 506, 507 : Form2 := TfrmLaporanInvoice.Create(Self);
      508 : Form2 := TfrmLaporanProgress.Create(Self);


      else raise EAbort.Create('MenuID Not Recognize');
  end;
  if (AMenuID > 2) then begin
      Form2.Caption := SystemMenu.MenuName;
      Form2.OnClose := ChildFormClose;
      AdvToolBar1.AddMDIChildMenu(Form2);
      Form2.OnKeyDown := ChildKeyDown;
//      RearrangeFormChild(Form2);
      Form2.FormStyle := fsMDIChild;
//      Form2.Show;
      AdvOfficeMDITabSet1.AddTab(Form2);
  end;
end;

procedure TfrmMainMenu.UpdateStatusBar(var AMessage: TMessage);
begin
  case AMessage.WParam of
    0: StatusBar1.Panels[0].Text := StatusBarText;
    1: begin
      if (AMessage.LParam <> ProgressBar.Position) then
        ProgressBar.Position := AMessage.LParam ;
      ProgressBar.Show;
    end;
    2: ProgressBar.Hide;
  end;
end;

procedure TfrmMainMenu.ResizeScrollBox(var AMessage: TMessage);
begin
  LastPanel.Align := alNone;
  if (AMessage.WParam>=0) then LastPanelWidth := AMessage.WParam;
  if (AMessage.LParam>=0) then LastPanelHeight := AMessage.LParam;
//  ScrollBox1Resize(nil);
end;

procedure TfrmMainMenu.HideShortcutBar1Click(Sender: TObject);
begin
  //ViewOutlookBar1.Checked := true;
//  ViewOutlookBar1Click(nil);
end;

procedure TfrmMainMenu.FilePrintSetup1Click(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
end;

procedure TfrmMainMenu.FileLogoff1Click(Sender: TObject);

begin
  if AdvOfficeMDITabSet1.AdvOfficeTabCount > 0 then
     AdvOfficeMDITabSet1.ActiveTabIndex := 0;
  SudahBukaMenu := false;
  ListCurrentMenuId.Clear;
  GlobalSystemUser.Reset;
  CloseAllChild;
  frmLogin.LogOn;  
  ChangeDisplay;
  ListCurrentMenuId.Clear;
  ListCurrentMenuId.Add('0');
//  btnHome.Click;
//  AdvOfficeMDITabSet1.AdvOfficeTabs[0].ShowClose := false;
end;

procedure TfrmMainMenu.FileExit1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainMenu.MenuClick(Sender: TObject);
begin
  if (Sender is TMenuItem) then
    OpenForm(TMenuItem(Sender).Tag)
  else if (Sender is TAdvGlowButton) then
    OpenForm(TButton(Sender).Tag)
  else if (Sender is TFourthShortcutBarButton) then
    OpenForm(TFourthShortcutBarButton(Sender).Tag);
end;

procedure TfrmMainMenu.objectGradientLabel23TGradientLabel1Click(
  Sender: TObject);
var i : integer ;
begin
  ListCurrentMenuId.Clear;
  ListCurrentMenuId.Add('0');
  AdvOfficeMDITabSet1.ActiveTabIndex := 0;
  for i := 1 to MDIChildCount-1 do begin
    MDIChildren[i].Close;
  end;
  SudahBukaMenu := false;
  AdvOfficeMDITabSet1.AdvOfficeTabs[0].ShowClose := false;

end;

procedure TfrmMainMenu.ViewGoToMenu1Click(Sender: TObject);
var MenuID: string;tmp : integer;
begin
//  GetPanel(0);
  if InputQuery('Pilih Menu','Nomor Menu', MenuID) then  begin
    if UpperCase(copy(MenuID,1,1)) = 'P' then
      tmp := StrToIntDef('6'+copy(MenuId,2,3),0)
    else
      tmp := StrToIntDef(MenuID,0);
    OpenForm(tmp);
  end;
end;

procedure TfrmMainMenu.ViewStatusBar1Click(Sender: TObject);
begin
//  ViewStatusBar1.Checked := not ViewStatusBar1.Checked;
//  StatusBar1.Visible := ViewStatusBar1.Checked;
end;

procedure TfrmMainMenu.Whidbey1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsWhidbey;
  ListTheme.Strings[0] := STYLE_WHIDBEY;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Windows71Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsWindows7;
  ListTheme.Strings[0] := STYLE_WINDOWS_7;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.WindowsVista1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsWindowsVista;
  ListTheme.Strings[0] := STYLE_WINDOWS_VISTA;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.WindowsXP1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsWindowsXP;
  ListTheme.Strings[0] := STYLE_WINDOWS_XP;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.AddMainMenuItem(AMenuItem: TMenuItem; ACaption: string;
  AImageIndex: integer; AShortcut: TShortcut; ATag: integer; AEnabled: boolean);
var NewMenuItem: TMenuItem;
begin
  NewMenuItem := TMenuItem.Create(AMenuItem);
  NewMenuItem.Caption := ACaption;
  NewMenuItem.ImageIndex := AImageIndex;
  NewMenuItem.Shortcut := AShortcut;
  NewMenuItem.Tag := ATag;
  NewMenuItem.Enabled := AEnabled;
  NewMenuItem.OnClick := MenuClick;
  AMenuItem.Add(NewMenuItem);
end;

procedure TfrmMainMenu.AddShortcutBarButton(APanelName: string; AButtonName: string;
  AImageIndex: integer; ATag: integer);
begin
end;

function TfrmMainMenu.AddSubMenuItem(aMenuItem: TMenuItem;
  aCaption: string; AImageIndex: integer; AShortcut: TShortcut;
  ATag: integer; AEnabled: boolean):TMenuITem;
var subMenu : TMenuItem;
begin
  subMenu := TMenuItem.Create(aMenuItem);
  subMenu.Caption := aCaption;
  subMenu.ImageIndex := AImageIndex;
  subMenu.ShortCut := AShortcut;
  subMenu.Tag := ATag;
  subMenu.Enabled := AEnabled;
  aMenuItem.Add(subMenu);
  Result := subMenu;
end;

procedure TfrmMainMenu.AdvOfficeMDITabSet1Change(Sender: TObject);
begin
  btnHome.Visible := AdvOfficeMDITabSet1.ActiveTabIndex <> 0;
end;

procedure TfrmMainMenu.AdvOfficeMDITabSet1TabClick(Sender: TObject;
  TabIndex: Integer);
begin
  btnHome.Visible := TabIndex <> 0;
end;

procedure TfrmMainMenu.AdvOfficeMDITabSet1TabClose(Sender: TObject;
  TabIndex: Integer; var Allow: Boolean);
begin
  ListCurrentMenuId.Delete(TabIndex);
end;

procedure TfrmMainMenu.AdvToolBar1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CASE key of
//F1 Pembelian                      F2 Retur Beli                    F3 Penjualan                         F4 Retur Jual                      F5 Stok Opname                     F6 Mutasi Gudang
//F7 Mutasi Barang               F8 Barang                         F9 Customer                          F10 Supplier                       F11 Kategori                            F12 Satuan                     ESC Close All

    VK_F1 : OpenForm(301);
    VK_F2 : OpenForm(302);
    VK_F3 : OpenForm(303);
    VK_F4 : OpenForm(304);
    VK_F5 : OpenForm(305);
    VK_F6 : OpenForm(306);
    VK_F7 : OpenForm(307);
    VK_F8 : OpenForm(308);
    VK_ESCAPE : CloseAllChild;
  END;
end;

procedure TfrmMainMenu.btnHomeClick(Sender: TObject);
begin
  AdvOfficeMDITabSet1.ActiveTabIndex := 0;
  btnHome.Visible := false;
end;

procedure TfrmMainMenu.Office2003Blue1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2003Blue;
  ListTheme.Strings[0] := STYLE_OFFICE_2003_BLUE;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2003Classic1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2003classic;
  ListTheme.Strings[0] := STYLE_OFFICE_2003_CLASSIC;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2003Olive1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2003Olive;
  ListTheme.Strings[0] := STYLE_OFFICE_2003_OLIVE;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2003Silver1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2003Silver;
  ListTheme.Strings[0] := STYLE_OFFICE_2003_SILVER;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2007Luna1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2007Luna;
  ListTheme.Strings[0] := STYLE_OFFICE_2007_LUNA;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2007Obsidian1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2007Obsidian;
  ListTheme.Strings[0] := STYLE_OFFICE_2007_OBSIDIAN;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2007Silver1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2007Silver;
  ListTheme.Strings[0] := STYLE_OFFICE_2007_SILVER;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2010Black1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2010Black;
  ListTheme.Strings[0] := STYLE_OFFICE_2010_BLACK;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2010Blue1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2010Blue;
  ListTheme.Strings[0] := STYLE_OFFICE_2010_BLUE;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.Office2010Silver1Click(Sender: TObject);
begin
  myConnection.AdvAppStyler1.Style := tsOffice2010Silver;
  ListTheme.Strings[0] := STYLE_OFFICE_2010_SILVER;
  ListTheme.SaveToFile('theme.dll');
end;

procedure TfrmMainMenu.OnLabelClick(Sender: TObject);
var ALabel : TLabel;
begin
  ALabel:= (Sender as TLabel);
  if (ALabel.Tag > 0) then OpenForm(ALabel.Tag);
end;

procedure TfrmMainMenu.Houver(Sender: TObject);
begin
  (Sender as TLabel).Font.Style:= [fsUnderLine];
  (Sender as TLabel).Font.Color:= clOlive;
end;

procedure TfrmMainMenu.UbahPassword1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmChangePassword, frmChangePassword);
  frmChangePassword.OnClose := frmMainMenu.ChildFormSingle;
  frmChangePassword.Execute(GlobalSystemUser.UserId);
end;

procedure TfrmMainMenu.UnHouver(Sender: TObject);
begin
  (Sender as TLabel).Font.Style := [];
  (Sender as TLabel).Font.Color := $00423740;
end;

procedure TfrmMainMenu.lblKeluarClick(Sender: TObject);
begin
  Close;                                                         
end;

procedure TfrmMainMenu.lblLogOffClick(Sender: TObject);
begin
  SudahBukaMenu := false;
  ListCurrentMenuId.Clear;
  GlobalSystemUser.Reset;
  CloseAllChild;  
  frmLogin.LogOn;
  ChangeDisplay;
end;

end.

