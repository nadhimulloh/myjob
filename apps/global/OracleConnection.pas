unit OracleConnection;
{$DEFINE DEBUG}
{$IFDEF DEBUG}
  {$DEFINE SQL_DEBUG}
{$ENDIF}

interface

uses
  ActiveX, ADOConst, ComObj, ADOInt,
  SysUtils, Classes, DB, ADODB, Variants, Controls,
  Windows, Messages, Graphics, Dialogs, Forms, DBTables,DateUtils,
  BaseGrid, AdvGrid, asgprev, AsgFindDialog, TmsAdvGridExcel, AsgLinks, strUtils, AdvAppStyler,
  UHDD_SN;

type
  TSQLOperator = (soGreaterThan, soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);
  TBooleanOperator = (boTrue,boFalse,boNone);

  //add by j@idol
  TCompareOperator = (coEquals, coInEquals, coInclude);

  arrString = array of string;
  arInteger = array of integer;
  arDate    = array of TDate;

  TmyConnection = class(TDataModule)
    ADOConnection: TADOConnection;
    ADOCommand: TADOCommand;
    ADODataSet: TADODataSet;
    advPrint: TAdvPreviewDialog;
    SaveToExcell: TSaveDialog;
    AdvFind: TAdvGridFindDialog;
    AdvExcel: TAdvGridExcelIO;
    advEditTipe: TAdvEditEditLink;
    AdvAppStyler1: TAdvAppStyler;
    ADOConnectionPostgree: TADOConnection;
    HDD_SN1: THDD_SN;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure SetConnection(Sender: TObject);
  protected
    { Private declarations }
    Level: integer;
    //Log: array of TStringList;


  public
    { Public declarations }
    //ListKategori: TStringList;

    function StoredProc(AName: string): TParameters;
    procedure ExecStoredProc;
    function AddParamString(AName: string; AString: string; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamNull(AName: string): TParameter;
    function AddParamInteger(AName: string; AInteger: integer; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamFloat(AName: string; AFloat: real; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamDateTime(AName: string; ADateTime: TDateTime; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamDateTime2(AName: string; ADateTime: TDateTime; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamBoolean(AName: string; ABoolean: boolean; ADirection: TParameterDirection = pdInput): TParameter;
    function GetParamOutput(AName: string): variant;

    procedure DumpSQL(ASQL: string);
    function ExecSQL(ASQL: string): integer;
    function OpenSQL(ASQL: string; ado : integer = 1): _RecordSet; //

    function BeginSQL(ado : integer = 1): integer;                 //
    procedure EndSQL(ado : integer = 1);                           //
    procedure UndoSQL(ado : integer = 1);                          //
    procedure Set_Kunci_Table_Transaksi(ANamaTabel: String);
    procedure Tunggu_Transaksi_Lain_Beres(ANamaTabel: String);
    procedure connectHRD;

    class function OpenRecordSet(ASQL: string): _RecordSet;


  end;

var
  myConnection: TmyConnection;
  //DQLFile   : string = {$IFDEF SQL_DEBUG}'dql.sql'{$ELSE}''{$ENDIF};
//  DMLFile   : string = {$IFDEF SQL_DEBUG}'dml.sql'{$ELSE}''{$ENDIF};
  ErrorFile : string = 'error.log';
  ErrorSave : string;
//  DumpFile  : string = 'dump.sql';
  DelFile   : string = 'delete.log';
  path2 : string;
  AMinDate: TDate;

  ListCardGlobal          : TStringList;
  ListJobGlobal           : TStringList;
  ListBankGlobal          : TStringList;

const
  TRUE_STRING = 'T';
  FALSE_STRING = 'F';
  TRUE_VALUE = -1;
  FALSE_VALUE = 0;

  procedure GetListGlobal;
  function ServerNow(IncTime : Boolean = False): TDateTime;
  function FormatSQLDateTimeNow: string;
  function FormatSQLDateNow: string;

  function FormatSQLBoolean(ABoolean: boolean): string;
  function FormatSQLDateTime(ADateTime: TDateTime): string;
  function FormatSQLDate(ADate: TDate): string;
  function FormatSQLTime(ATime: TTime): string;
  function FormatSQLDateTime2(ADateTime: TDateTime):string;
  function FormatSQLString(AString: string): string; overload;
  function FormatSQLString(AChar: char): string; overload;
  function FormatSQLNumber(AInteger: integer): string; overload;
  function FormatSQLNumber(AFloat: real): string; overload;
  function FormatSQLNULL: string;
  function FormatSQLNULL2: string;
  function FormatSQLOperator(operator:TSQLOperator):string;
  function FormatSQLBooleanOperator(operator:TBooleanOperator):string;
  function FormatSQLCompareOperator(operator:TCompareOperator):string;
  //TSetOperator = (soEquals, soInEquals, soInclude);

  function BufferToBooleanDef(ABuffer: variant; ADefault: boolean): boolean;
  function BufferToBoolean(ABuffer: variant): boolean;
  function BufferToStringDef(ABuffer: variant; ADefault: string): string;
  function BufferToString(ABuffer: variant): string;
  function BufferToIntegerDef(ABuffer: variant; ADefault: integer): integer;
  function BufferToInteger(ABuffer: variant): integer;
  function BufferToFloatDef(ABuffer: variant; ADefault: real): real;
  function BufferToFloat(ABuffer: variant): real;
  function BufferToDateTimeDef(ABuffer: variant; ADefault: TDateTime): TDateTime;
  function BufferToDateTime(ABuffer: variant): TDateTime;

  procedure SetingPrint(Asg: TAdvStringGrid);
  function CreateNewSeq(NamaSeq : String; Table : string) : integer;
  function CreateNewSeqMaster(ASeqName: string) :integer;
  function GetCurrentSeq(NamaSeq, Table : string; ANomor : string = '') : integer;

  function Get_NVL(Val1, Val2 : String) : String;
  function Get_Null(Field : String; Stat : Boolean) : String;

var
  vApplikasiPath : string;

implementation

uses  UConst, USystemMenu, UGeneral, UEngine, AdvStyleIF;
{$R *.dfm}

  procedure GetListGlobal;
  begin
//    ListGlobalWilayah := TStringList.Create;
//    Get_List_All_Wilayah_umr(ListGlobalWilayah);

//    ListGlobalAgama := TStringList.Create;
//    ListGlobalAgama.Clear;
//    ListGlobalAgama.Add('ISLAM');
//    ListGlobalAgama.Add('KRISTEN KATOLIK');
//    ListGlobalAgama.Add('KRISTEN PROTESTAN');
//    ListGlobalAgama.Add('BUDDHA');
//    ListGlobalAgama.Add('HINDU');
//    ListGlobalAgama.Add('LAIN - LAIN');
  end;

  function ServerNow(IncTime : Boolean = False): TDateTime;
  var sql: string;
    buffer: _RecordSet;
  begin
    sql := 'SELECT '+IfThen(IncTime, FormatSQLDateTimeNow, FormatSQLDateNow)+' FROM DUAL';
    buffer := myConnection.ADOConnection.Execute(sql);
    Result := BufferToDateTime(buffer.Fields[0].Value);
    buffer.Close;
  end;

  procedure WriteLog(AFilename, AString: string);
  var FP: Text;
  begin
    if (AFileName='') then Exit;

    AssignFile(FP, AFilename);
    {$I-} Append(FP); {$I+}
    if (IOResult<>0) then Rewrite(FP);
    Writeln(FP, AString);
    CloseFile(FP);
  end;

{ ---------------------------------------------------------------------------- }

procedure TmyConnection.DataModuleCreate(Sender: TObject);
var path:string;
AList: TStringList;
  Style : string;
begin
  AList := TStringList.Create;
  AList.clear;
  path := vApplikasiPath+'\theme.dll';
  if FileExists(path) then begin
    AList.LoadFromFile(path);
    if AList.Count > 0 then begin
      Style := AList.Strings[0];
    end else Style := STYLE_OFFICE_2003_SILVER;
  end else Style := STYLE_OFFICE_2003_SILVER;
  if style = STYLE_OFFICE_2003_BLUE then myConnection.AdvAppStyler1.Style := tsOffice2003Blue
  else if style = STYLE_OFFICE_2003_CLASSIC then myConnection.AdvAppStyler1.Style := tsOffice2003Classic
  else if style = STYLE_OFFICE_2003_OLIVE then myConnection.AdvAppStyler1.Style := tsOffice2003Olive
  else if style = STYLE_OFFICE_2003_SILVER then myConnection.AdvAppStyler1.Style := tsOffice2003Silver
  else if style = STYLE_OFFICE_2007_LUNA then myConnection.AdvAppStyler1.Style := tsOffice2007Luna
  else if style = STYLE_OFFICE_2007_OBSIDIAN then myConnection.AdvAppStyler1.Style := tsOffice2007Obsidian
  else if style = STYLE_OFFICE_2007_SILVER then myConnection.AdvAppStyler1.Style := tsOffice2007Silver
  else if style = STYLE_OFFICE_2010_BLACK then myConnection.AdvAppStyler1.Style := tsOffice2010Black
  else if style = STYLE_OFFICE_2010_BLUE then myConnection.AdvAppStyler1.Style := tsOffice2010Blue
  else if style = STYLE_OFFICE_2010_SILVER then myConnection.AdvAppStyler1.Style := tsOffice2010Silver
  else if style = STYLE_TERMINAL then myConnection.AdvAppStyler1.Style := tsTerminal
  else if style = STYLE_WHIDBEY then myConnection.AdvAppStyler1.Style := tsWhidbey
  else if style = STYLE_WINDOWS_7 then myConnection.AdvAppStyler1.Style := tsWindows7
  else if style = STYLE_WINDOWS_VISTA then myConnection.AdvAppStyler1.Style := tsWindowsVista
  else if style = STYLE_WINDOWS_XP then myConnection.AdvAppStyler1.Style := tsWindowsXP;
  path2 := '';
  SetConnection(Self);


  SaveToExcell.InitialDir := GetCurrentDir;

  ListCardGlobal           := TStringList.Create;
  ListJobGlobal            := TStringList.Create;   
  ListBankGlobal           := TStringList.Create;

  GlobalListDocType.Clear;
  GlobalListDocType.Add(DOC_TYPE_CASH+'='+DOC_TYPE_CASH_TEXT);
  GlobalListDocType.Add(DOC_TYPE_CHEQUE+'='+DOC_TYPE_CHEQUE_TEXT);
  GlobalListDocType.Add(DOC_TYPE_GIRO+'='+DOC_TYPE_GIRO_TEXT);
  GlobalListDocType.Add(DOC_TYPE_TRANSFER+'='+DOC_TYPE_TRANSFER_TEXT);

  getListGlobal;
  AList.Destroy;
end;

procedure TmyConnection.DataModuleDestroy(Sender: TObject);
begin
  //writelog(DQLFile,'-- Stopped on '+DateTimeToStr(Now));
//  writelog(DMLFile,'-- Stopped on '+DateTimeToStr(Now));

//  if (DMLFile<>'') then
//    for i:=0 to high(Log) do Log[i].Destroy;

  if (ADOConnection.Connected) then begin
    ADOConnection.Close;
    repeat Application.ProcessMessages; until not ADOConnection.Connected;
  end;
end;

procedure TmyConnection.SetConnection(Sender: TObject);
var path:string;
AList: TStringList;
AUser, APaswd, AServis, ADataSourceHrd: String;
  AHasil : string;
  i : integer;
begin
  AList := TStringList.Create;
  AList.clear;
  vApplikasiPath := GetCurrentDir;
  path := vApplikasiPath+'\profile.dll';
  if FileExists(path) then
    AList.LoadFromFile(path);
  ADataSourceHrd := '';
  try
    if AList.Count <> 0 then begin
      for i := 0 to AList.count-1 do begin
        AHasil := HDD_SN1.ReadSerialHDDEnkripsi1(AList.Strings[i]);
        case i of
          0 : AUser := AHasil;
          1 : APaswd := AHasil;
          2 : AServis := AHasil;
          3 : ADataSourceHrd := AHasil;
        end;
      end;
//      path := 'Provider=MSDAORA.1;Password='+APaswd+';User ID='+AUser+';Data Source='+AServis;
      path := 'Provider=MSDAORA.1;Password='+APaswd+';User ID='+AUser+';Data ' +
        'Source='+AServis;
    end else begin

    end;

    if ADOConnection.Connected then
      ADOConnection.Connected := False;
    ADOConnection.ConnectionString := path;
    ADOConnection.Open;
    repeat Application.ProcessMessages; until ADOConnection.Connected;
  except
    Application.Terminate;
    repeat Application.ProcessMessages; until Application.Terminated;
    raise;
  end;
  AList.Destroy;
end;

function TmyConnection.BeginSQL(ado : integer = 1): integer;
begin
  if ado = 1 then begin
    Level := ADOConnection.BeginTrans;
    Result := Level;
  end else begin
    Level := ADOConnectionPostgree.BeginTrans;
    Result := Level;
  end;
end;

procedure TmyConnection.EndSQL(ado : integer = 1);
begin
  if (Level=0) then Exit;
  dec(Level);
  if ado = 1 then ADOConnection.CommitTrans
  else ADOConnectionPostgree.CommitTrans;
end;

procedure TmyConnection.UndoSQL(ado : integer = 1);
begin
  if (Level=0) then Exit;
  dec(Level);
  if ado = 1 then ADOConnection.RollbackTrans
  else ADOConnectionPostgree.RollbackTrans;
end;


procedure TmyConnection.Set_Kunci_Table_Transaksi(ANamaTabel: String);
begin
  myConnection.ExecSQL('LOCK TABLE '+ANamaTabel+' IN SHARE MODE');
end;

procedure TmyConnection.Tunggu_Transaksi_Lain_Beres(ANamaTabel: String);
var i: Integer;
sqL: string;
begin
  sql := 'SELECT COUNT(*) FROM v$locked_table WHERE NAMA_TABEL = '+Formatsqlstring(UpperCase(ANamaTabel))+
         ' AND PEMILIK_TABEL = '+FormatSQLString('my_job');
  i := 0;
  if getIntegerFromSQL(sqL) > 0 then begin
    repeat i := i + 1;
    until getIntegerFromSQL(sqL) = 0;
  end;
end;


procedure TmyConnection.connectHRD;
begin
  if path2 <> ''  then begin
    if ADOConnectionPostgree.Connected then
      exit;
    ADOConnectionPostgree.ConnectionString := path2;
    ADOConnectionPostgree.Open;
    repeat Application.ProcessMessages; until ADOConnectionPostgree.Connected;
  end;
end;

function TmyConnection.ExecSQL(ASQL: string): integer;
var
  vTemp: String;
begin
  ErrorSave := '';
  try
    ADOConnection.Execute(ASQL,Result);

    vTemp := Copy(ASQL, 1, 6);
//    if UpperCase(vTemp) = 'DELETE' then
//      WriteLog(DelFile, FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' : '+ASQL);
  except
    on E: Exception do begin
      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      ErrorSave := E.Message;
      //InsertDumpSQL(ASQL);
      raise;
    end;
  end;
end;

function TmyConnection.OpenSQL(ASQL: string; ado : integer = 1): _RecordSet;
begin
  try
    if ado = 1 then
      Result := ADOConnection.Execute(ASQL)
    else
      Result := ADOConnectionPostgree.Execute(ASQL)
        //writelog(DQLFile,ASQL);
  except
    on E: Exception do begin
      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      raise;
    end;
  end;
end;

procedure TmyConnection.DumpSQL(ASQL: string);
begin
//  WriteLog(DumpFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+ ' - '+ASQL);
end;

procedure TmyConnection.ExecStoredProc;
var i: integer; ASQL, DirectionStr, ValueStr: string;
begin
  ASQL := '';
  for i:=0 to ADOCommand.Parameters.Count-1 do
    with ADOCommand.Parameters[i] do begin
      {case Direction of
        pdInput: DirectionStr:='<=';
        pdOutput: DirectionStr:='=>';
        pdInputOutput: DirectionStr:='<=>';
        pdReturnValue: DirectionStr:='=';
      end;}
      DirectionStr:=' => ';
      if (Direction in [pdInput,pdInputOutput]) then begin
        case DataType of
          ftDateTime: ValueStr := FormatSQLDate(Value);
          ftFloat, ftInteger: ValueStr := FormatSQLNumber(Value);
          ftString: ValueStr := FormatSQLString(Value);
          
        end;
        ASQL := ASQL + ', '+Name+DirectionStr+ValueStr+' '
      end{ else
        ASQL := ASQL + ', '+Name+DirectionStr+' '};
    end;
  if (length(ASQL)>0) then delete(ASQL,1,1);

  ASQL := 'EXEC '+ADOCommand.CommandText+'('+ASQL+')';

  try
    ADOCommand.Execute;
//    if (DMLFile<>'') then begin
//      if (Level = 0) then
////        writeLog(DMLFile,ASQL+';')
//      else
//        Log[high(Log)].Append(ASQL+';');
//    end;
  except
    on E: Exception do begin
      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      raise;
    end;
  end;
end;

function TmyConnection.StoredProc(AName: string): TParameters;
begin
  ADOCommand.CommandType := cmdStoredProc;
  ADOCommand.CommandText := AName;
  ADOCommand.Parameters.Clear;
  Result := ADOCommand.Parameters;
end;

function TmyConnection.AddParamDateTime(AName: string; ADateTime: TDateTime;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftDateTime, ADirection, 0, ADateTime);
end;

function TmyConnection.AddParamFloat(AName: string; AFloat: real;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftFloat, ADirection, 0, AFloat);
end;

function TmyConnection.AddParamInteger(AName: string; AInteger: integer;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftInteger, ADirection, 0, AInteger);
end;

function TmyConnection.AddParamString(AName: string; AString: string;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftString, ADirection, length(AString)+1, AString);
end;

function TmyConnection.AddParamNull(AName: string): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftUnknown, pdInput, 0, varNull);
end;

function TmyConnection.AddParamBoolean(AName: string; ABoolean: boolean;
  ADirection: TParameterDirection): TParameter;
begin
  if (ABoolean) then
    Result := AddParamInteger(AName,TRUE_VALUE,ADirection)
  else
    Result := AddParamInteger(AName,FALSE_VALUE,ADirection);
end;

function TmyConnection.GetParamOutput(AName: string): variant;
begin
  Result := ADOCommand.Parameters.ParamValues[AName];
end;

{ ---------------------------------------------------------------------------- }

  function FormatSQLDateTimeNow: string;
  begin
    Result := 'SYSDATE';
  end;

  function FormatSQLDateNow: string;
  begin
    Result := 'TRUNC(SYSDATE)';
  end;

  function FormatSQLNULL: string;
  begin
    Result := '';
  end;

  function FormatSQLNULL2: string;
  begin
    Result := 'NULL';
  end;

  function FormatSQLOperator(operator:TSQLOperator):string;
  begin
    case operator of
      soGreaterThan : result := '> ';
      soGreaterThanEqualsTo : result:= '>= ';
      soEquals : result := '= ';
      soLessThan : result:= '< ';
      soLessThanEqualsTo : result:= '<=';
      else result := '= ';
    end;
  end;

  function FormatSQLBooleanOperator(operator:TBooleanOperator):string;
  begin
    case operator of
      boNone : result := '';
      boTrue : Result := 'True';
      boFalse: Result := 'False';
      else result := '= ';
    end;
  end;

  function FormatSQLNumber(AInteger: integer): string; overload;
  begin
    Result := IntToStr(AInteger);
  end;

  function FormatSQLNumber(AFloat: real): string; overload;
  begin
    if DecimalSeparator = '.' then
      Result := FloatToStr(AFloat)
    else
      Result := ReplaceSubStr(FloatToStr(AFloat),DecimalSeparator,'.');
  end;

  function FormatSQLNumber(ANumber: string): string; overload;
  begin
    Result := ReplaceSubStr(ReplaceSubStr(ANumber,ThousandSeparator,''),DecimalSeparator,'.');
  end;

  function FormatSQLString(AString: string): string;
  begin
    Result := QuotedStr(AString);
  end;

  function FormatSQLString(AChar: char): string; overload;
  begin
    Result := QuotedStr(AChar);
  end;

  function FormatSQLDate(ADate: TDate): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"dd/mm/yyyy"'', ''DD/MM/YYYY'')"', ADate);
  end;

  function FormatSQLTime(ATime: TTime): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"hh:nn:ss"'', ''HH24:MI:SS'')"', ATime);
  end;

  function FormatSQLDateTime(ADateTime: TDateTime): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"', ADateTime);
  end;

  function FormatSQLBoolean(ABoolean: boolean): string;
  begin
    if (ABoolean) then Result := FormatSQLNumber(TRUE_VALUE) else Result := FormatSQLNumber(FALSE_VALUE);
  end;
   // add by chan********
  function FormatSQLDateTime2(ADateTime: TDateTime):string;
  var y,m,d,hh,nn,ss,ms:word;
  begin
    DecodeTime(Now,hh,nn,ss,ms);
    DecodeDate(ADateTime,y,m,d);
    result :=  FormatDateTime('"TO_DATE(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"',EncodeDateTime(y,m,d,hh,nn,ss,ms));
  end;
{ ---------------------------------------------------------------------------- }

  function BufferToBoolean(ABuffer: variant): boolean;
  begin
    Result := BufferToBooleanDef(ABuffer,false);
  end;

  function BufferToBooleanDef(ABuffer: variant; ADefault: boolean): boolean;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := (BufferToInteger(ABuffer)=TRUE_VALUE)
    else
      Result := ADefault;
  end;

  function BufferToString(ABuffer: variant): string;
  begin
    Result := BufferToStringDef(ABuffer,'');
  end;

  function BufferToStringDef(ABuffer: variant; ADefault: string): string;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToInteger(ABuffer: variant): integer;
  begin
    Result := BufferToIntegerDef(ABuffer,0);
  end;

  function BufferToIntegerDef(ABuffer: variant; ADefault: integer): integer;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToFloat(ABuffer: variant): real;
  begin
    Result := BufferToFloatDef(ABuffer,0);
  end;

  function BufferToFloatDef(ABuffer: variant; ADefault: real): real;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToDateTime(ABuffer: variant): TDateTime;
  begin
    Result := BufferToDateTimeDef(ABuffer,0);
  end;

  function BufferToDateTimeDef(ABuffer: variant; ADefault:TDateTime): TDateTime;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function CreateADOObject(const ClassID: TGUID): IUnknown;
  var
    Status: HResult;
    FPUControlWord: Word;
  begin
    asm
      FNSTCW  FPUControlWord
    end;
    Status := CoCreateInstance(ClassID, nil, CLSCTX_INPROC_SERVER or
      CLSCTX_LOCAL_SERVER, IUnknown, Result);
    asm
      FNCLEX
      FLDCW FPUControlWord
    end;
    if (Status = REGDB_E_CLASSNOTREG) then
      raise Exception.CreateRes(@SADOCreateError) else
      OleCheck(Status);
  end;

class function TmyConnection.OpenRecordSet(ASQL: string): _RecordSet;
begin
  Result := CreateADOObject(CLASS_Recordset) as _Recordset;
  Result.Open(ASQL,myConnection.ADOConnection.ConnectionObject,
    adOpenForwardOnly,adLockPessimistic,adCmdText);
end;

function TmyConnection.AddParamDateTime2(AName: string;
  ADateTime: TDateTime; ADirection: TParameterDirection): TParameter;
var y,m,d,hh,nn,ss,ms:word;
begin
  DecodeTime(Now,hh,nn,ss,ms);
  DecodeDate(ADateTime,y,m,d);
  ADateTime := EncodeDateTime(y,m,d,hh,nn,ss,ms);
  Result := ADOCommand.Parameters.CreateParameter(AName, ftDateTime, ADirection, 0, ADateTime);
end;


function FormatSQLCompareOperator(operator:TCompareOperator):string;
begin
  case operator of
    coEquals  : Result := '=';
    coInEquals: Result := '<>';
    coInclude : Result := ' in ';
    else result := '= ';
  end;
end;

procedure SetingPrint(Asg: TAdvStringGrid);
begin
  with Asg.PrintSettings do begin
    Borders           := pbSingle;
    BorderStyle       := psSolid;
    FitToPage         := fpNever;
    Centered          := True;
    HeaderFont.Name   := 'Arial';
    HeaderFont.Size   := 10;
    HeaderFont.Style  := [fsBold];
    Font.Name         := 'Tahoma';
    Font.Size         := 8;

    FooterFont.Color  := clWhite;
    Date              := ppTopRight;
    DateFormat        := 'dd/mmm/yyyy';
    Title             := ppTopLeft;
    PagePrefix        := 'Halaman : ';
    HeaderSize        := 200;
    FooterSize        := 200;
    LeftSize          := 75;
    RightSize         := 75;
    ColumnSpacing     := 5;
    RowSpacing        := 5;
    TitleSpacing      := 5;
    //Orientation       :=
    //fo
    //NoAutoSize        := True;
    RepeatFixedRows   := True;
    //FitToPage         := fpAlways;
    Centered          := True;
  end;

end;




function CreateNewSeq(NamaSeq : String; Table : string) : integer;
var sql : string;
begin
  sql := 'SELECT MAX('+NamaSeq+') FROM ' + Table;
  Result := getIntegerFromSQL(sql)+1;
end;

function CreateNewSeqMaster(ASeqName: string) :integer;
var sql : string;
begin
  sql := 'select '+ASeqName+'.nextval from dual';
  Result := GetIntegerFromSQL(sql);
end;

function GetCurrentSeq(NamaSeq, Table : string; ANomor : string = '') : integer;
var sql : string;
begin
  sql := 'SELECT MAX('+NamaSeq+') FROM ' + Table + IfThen(ANomor <> '', ' WHERE nomor = '+FormatSQLString(ANomor));
  Result := getIntegerFromSQL(sql);
end;

function Get_NVL(Val1, Val2 : String) : String;
begin
  if DBisORCL then Result := ' NVL('+Val1+', '+Val2+') '
  else Result := ' coalesce('+Val1+', '+Val2+') ';
end;


function Get_Null(Field : String; Stat : Boolean) : String;
begin
  if DBisORCL then Result := ' '+Field+' IS '+IfThen(not Stat, 'NOT ')+'NULL ';
end;



//function Get_Dokumen_Global: TR_setting_no_dok;
//var buffer : _Recordset;
//sql: String;
//begin
//  sql := 'SELECT seq, jenis_trans, jenis_dok, is_ket_lokasi, ket_lokasi, is_bln_thn, tipe_reset, digit_no_urut, judul_print, disable_date '+
//         'FROM setting_no_dok WHERE seq = (SELECT MAX(SEQ) FROM setting_no_dok) ';
//  buffer := myConnection.OpenSQL(sql);
//  if buffer.RecordCount > 0 then begin
//    Result.seq := BufferToInteger(buffer.Fields[0].Value);
//    Result.jenis_trans := BufferToString(buffer.Fields[1].Value);
//    Result.jenis_dok := BufferToString(buffer.Fields[2].Value);
//    Result.is_ket_lokasi := BufferToString(buffer.Fields[3].Value);
//    Result.ket_lokasi := BufferToString(buffer.Fields[4].Value);
//    Result.is_bln_thn := BufferToString(buffer.Fields[5].Value);
//    Result.tipe_reset := BufferToString(buffer.Fields[6].Value);
//    Result.digit_no_urut := BufferToInteger(buffer.Fields[7].Value);
//    Result.judul_print := BufferToString(buffer.Fields[8].Value);
//    Result.disable_date := BufferToDateTime(buffer.Fields[9].Value);
//  end else begin
//    Result.seq := 0;
//    Result.jenis_trans := '';
//    Result.jenis_dok := '';
//    Result.is_ket_lokasi := '';
//    Result.ket_lokasi := '';
//    Result.is_bln_thn := '';
//    Result.tipe_reset := '';
//    Result.digit_no_urut := 0;
//    Result.judul_print := '';
//    Result.disable_date := 0;
//  end;


//end;

end.

