unit Loading;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Gauges, ExtCtrls;

type
  TfrmLoading = class(TForm)
    MainPanel: TPanel;
    Gauge: TGauge;
    sttNamaTrans: TStaticText;
  private
    { Private declarations }
  public
    { Public declarations }
     procedure Execute(Ket1 : string; value1, value2 : real);
  end;

var
  frmLoading: TfrmLoading;

implementation

{$R *.dfm}

{ TfrmLoading }

procedure TfrmLoading.Execute(Ket1: string; value1, value2: real);
begin
  sttNamatrans.Caption := Ket1;
  if (Value1 <> 0) and (value2 <> 0) then begin
    Gauge.Show;
    Gauge.Progress := Round((value2+1)/value1*100);
    Self.Height := 115;
  end else begin
    Gauge.Hide;
    Self.Height := 93;
  end;
  show;
  Update;
end;

end.
