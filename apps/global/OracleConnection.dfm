object myConnection: TmyConnection
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 332
  Width = 398
  object ADOConnection: TADOConnection
    ConnectOptions = coAsyncConnect
    LoginPrompt = False
    Provider = 'OraOLEDB.Oracle.1'
    Left = 32
    Top = 24
  end
  object ADOCommand: TADOCommand
    CommandType = cmdStoredProc
    Connection = ADOConnection
    Prepared = True
    Parameters = <>
    Left = 32
    Top = 88
  end
  object ADODataSet: TADODataSet
    Connection = ADOConnection
    Parameters = <>
    Left = 112
    Top = 24
  end
  object advPrint: TAdvPreviewDialog
    CloseAfterPrint = False
    DialogCaption = 'Preview'
    DialogPrevBtn = 'Previous'
    DialogNextBtn = 'Next'
    DialogPrintBtn = 'Print'
    DialogCloseBtn = 'Close'
    PreviewFast = False
    PreviewWidth = 800
    PreviewHeight = 600
    PreviewLeft = 112
    PreviewTop = 84
    PreviewCenter = True
    PrinterSetupDialog = True
    Left = 112
    Top = 92
  end
  object SaveToExcell: TSaveDialog
    DefaultExt = '*.xls'
    Filter = 'Excel Files|*.xls'
    Left = 200
    Top = 24
  end
  object AdvFind: TAdvGridFindDialog
    AutoPosition = False
    MsgNotFound = 'Could not find text'
    MsgNoMoreFound = 'No more occurences of text '
    TxtCaption = 'Find text'
    TxtTextToFind = 'Text to find'
    TxtDirection = 'Direction'
    TTxtDirForward1 = 'Forward (top to bottom)'
    TTxtDirForward2 = 'Forward (left to right)'
    TTxtDirBackward1 = 'Backward (bottom to top)'
    TTxtDirBackward2 = 'Backward (right to left)'
    TxtScope = 'Scope'
    TxtScopeAllCells = 'All cells'
    TxtScopeCurrRow = 'Current row only'
    TxtScopeCurrCol = 'Current column only'
    TxtScopeSelectedCells = 'Selected cells'
    TxtOptions = 'Options'
    TxtOptionsCase = '&Case sensitive'
    TxtOptionsWholeWords = '&Whole words only'
    TxtOptionsMatchFirst = '&Match from first char'
    TxtOptionsIgnoreHTML = '&Ignore HTML tags'
    TxtOptionsFixedCells = '&Find in fixed cells'
    TxtOptionsWildcards = 'Match with &wildcards'
    TxtBtnOk = 'Ok'
    TxtBtnCancel = 'Cancel'
    Left = 200
    Top = 88
  end
  object AdvExcel: TAdvGridExcelIO
    Options.ExportOverwrite = omAlways
    Options.ExportOverwriteMessage = 'File %s already exists'#13'Ok to overwrite ?'
    Options.ExportHiddenColumns = True
    Options.ExportRawRTF = False
    Options.ExportHardBorders = True
    Options.UseExcelStandardColorPalette = False
    UseUnicode = False
    GridStartRow = 0
    GridStartCol = 0
    Version = '3.4.1'
    Left = 168
    Top = 128
  end
  object advEditTipe: TAdvEditEditLink
    Tag = 0
    AutoPopupWidth = False
    EditStyle = esInplace
    PopupWidth = 0
    PopupHeight = 0
    WantKeyLeftRight = True
    WantKeyUpDown = False
    WantKeyHomeEnd = True
    WantKeyPriorNext = False
    WantKeyReturn = False
    WantKeyEscape = False
    EditAlign = eaRight
    EditColor = clWindow
    MaxLength = 0
    ModifiedColor = clRed
    EditType = etMoney
    ShowModified = False
    Precision = 2
    Signed = False
    ExcelStyleDecimalSeparator = True
    Left = 152
    Top = 21
  end
  object AdvAppStyler1: TAdvAppStyler
    Style = tsOffice2007Luna
    Left = 120
    Top = 184
  end
  object ADOConnectionPostgree: TADOConnection
    Left = 296
    Top = 184
  end
  object HDD_SN1: THDD_SN
    Left = 912
    Top = 104
  end
end
