unit UCreateForm;

interface

uses

  MainMenu,
  OracleConnection, UConst, strUtils, BaseGrid, Grids,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, Menus, FthCtrls, FthSBar, ExtCtrls, ComCtrls,
  ToolWin, AdvGrid, AdvEdit, Gauges, sPanel, ColCombo, IdTimeServer,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdDayTimeServer,
  AdvOfficeTabSet, AdvOfficeTabSetStylers, AdvMenus, AdvMenuStylers, AdvToolBar,
  AdvToolBarStylers, AdvAppStyler;

  procedure CreateFormApp(AMenuId: Integer);
  procedure RearrangeFormChild(Form: TForm; AIsIconMax : boolean = true);

implementation

uses
  USystemMenu,Login,
  ugeneral, AdvPanel, AdvGlowButton, AdvOfficePager,
  AdvOfficeButtons, AdvStyleIF, Home;

procedure RearrangeFormChild(Form: TForm; AIsIconMax : boolean = true);
var  j: integer;
  Panel: TPanel;
  //Control: TControl;
//  MainPanelFound: boolean; //NeedUpdate: boolean;
  AdvStringGrid: TAdvStringGrid; AdvEdit: TAdvEdit; Button: TButton; //DateTimePicker: TDateTimePicker;
  ALabel : TLabel;
  ASttText: TStaticText;
  AMemo : TMemo;
  APanelAdv : TAdvPanel;
  AGlowButton : TAdvGlowButton;
  AGroupBox : TGroupBox;
  APager : TAdvOfficePager;
  ADate : TDateTimePicker;
  AdvCheckBox : TAdvOfficeCheckBox;
  Acombo : TComboBox;
  Caption : string;
  AStyleForm : TTMSStyle;  
  AFormStyle  : TAdvFormStyler;
begin
//+juli AIsIconMax untuk form yang ada sortcut tapi ga usah maximize
  if AIsIconMax = true then
    Form.BorderIcons := [biSystemMenu, biMinimize, biMaximize]
  else Form.BorderIcons := [biSystemMenu, biMinimize];
//    Form.WindowState := wsMaximized;
//    Form.Color := $00E0E0E0;

	AStyleForm := tsOffice2003Blue;
  AFormStyle := TAdvFormStyler.Create(Application);
  AFormStyle.ApplyStyleToForm(Form, AStyleForm);
for j:=0 to Form.ComponentCount-1 do
    if (Form.Components[j] is TAdvStringGrid) then begin
      AdvStringGrid := (Form.Components[j] as TAdvStringGrid);
      with AdvStringGrid do begin
//        Color:=$00ACC3C3;
        HintColor:=clInfoBk;
//        SelectionColor:=$00ACC3C3;
//        SelectionColorTo:=$00ACC3C3;
        SelectionTextColor:=clBlack;
        URLColor:=clBlue;
        URLShow:=false;
        ScrollColor:=clNone;
        ActiveRowColor:=clInfoBk;
        ActiveRowShow:=false;
        ActiveCellShow:=false;
        Font.Color:=clWindowText;
        Font.Style:=[];
        Font.Size:=8;
        Font.Name:='Tahoma';
//        ActiveCellColor:=$00ACC3C3;
//        ActiveCellColorTo:=$00ACC3C3;
//        ActiveRowColor := $00ACC3C3;
//        ActiveRowColorTo := $00ACC3C3;
        ActiveCellFont.Color:=clWindowText;
        ActiveCellFont.Style:=[];
        ActiveCellFont.Size:=8;
        ActiveCellFont.Name:='Tahoma';
        FixedFont.Color:=clWindowText;
        FixedColor := frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
//        FixedFont.Style:=[fsBold];
        FixedFont.Style:=[];
        FixedFont.Size:=8;
        FixedFont.Name:='Tahoma';
        Balloon.BackgroundColor:=clNone;
        Balloon.TextColor:=clNone;
        Balloon.Transparency:=0;
        Bands.PrimaryColor:=clInfoBk;
        Bands.SecondaryColor:=clWindow;
        Bands.Active:=false;
        SortSettings.IndexColor:=clYellow;
        FloatingFooter.Color:=clBtnFace;
        ControlLook.CheckSize:=15;
        ControlLook.Color:=clBlack;
        ControlLook.CommentColor:=clCream;
        ControlLook.ControlStyle:=csWinXP;
        ControlLook.FixedGradientFrom:=frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
        ControlLook.FixedGradientTo:=frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
//        ControlLook.FixedGradientMirrorFrom := $003B8BD5;
//        ControlLook.FixedGradientMirrorTo := $00477CC7;
        ControlLook.RadioSize:=10;
        ControlLook.FlatButton:=false;
        ControlLook.ProgressBorderColor:=clGray;
        ControlLook.ProgressXP:=false;
        Look:=glXP;
        SearchFooter.Color:=frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
        SearchFooter.ColorTo:=frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
        GridLineColor:=clSilver;
        Grouping.HeaderColor:=frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
        Grouping.HeaderColorTo:=frmMainMenu.AdvPanelStyler1.Settings.ColorTo;
        Grouping.HeaderUnderline:=true;
        Grouping.HeaderTextColor:=clWhite;
        Grouping.HeaderLineColor:=$00215F99;
        Grouping.HeaderLineWidth:=1;
        Grouping.SummaryColor:=$00F3F7F8;
        Grouping.SummaryColorTo:=$00ACC3C3;
        Grouping.SummaryTextColor:=clNone;
        Grouping.SummaryLine:=false;
        Grouping.SummaryLineColor:=clBlue;
        Grouping.SummaryLineWidth:=1;
        BackGround.Top:=0;
        BackGround.Left:=0;
        BackGround.Cells:=bcNormal;
//        BackGround.Color:=$001595EE;
//        BackGround.ColorTo:=$001595EE;

        Flat := False;
        WordWrap := False;
        Navigation.AllowClipboardShortCuts := True;
        if AdvStringGrid.Tag = 2 then begin
          Grouping.HeaderUnderline:=false;
        end;
      end;

      if AdvStringGrid.Tag = -1 then begin
        AdvStringGrid.Bands.Active          := False;
        AdvStringGrid.SortSettings.Show     := False;
      end else begin
        AdvStringGrid.SortSettings.Show     := True;
        AdvStringGrid.SortSettings.Column   := AdvStringGrid.ColCount-1;
        AdvStringGrid.Bands.Active          := True;
        AdvStringGrid.SortSettings.AutoFormat:= True;
        AdvStringGrid.SortSettings.NormalCellsOnly := true;
        AdvStringGrid.SearchFooter.Visible := False;
        AdvStringGrid.SearchFooter.AutoSearch := True;
        AdvStringGrid.SearchFooter.ShowClose := False;
        AdvStringGrid.SearchFooter.ShowMatchCase := False;
      end;
      if AdvStringGrid.Tag = 2 then begin
        AdvStringGrid.Bands.Active := False;
      end;
//      if AdvStringGrid.Tag = -100 then   handi041019
//        advStringGrid.FloatFormat := '%.7n';
      // + fahmi 260514, semua font tahoma
      AdvStringGrid.Font.Name := 'Tahoma';
      AdvStringGrid.DefaultRowHeight := 22;
//      AdvStringGrid.Color := $00CDF1F9;
      AdvStringGrid.MouseActions.AutoSizeColOnDblClick := False;
      AdvStringGrid.Invalidate;
    end else if (Form.Components[j] is TAdvEdit) then begin
      AdvEdit := (Form.Components[j] as TAdvEdit);
      AdvEdit.DisabledColor := AdvEdit.Color;
      // + fahmi 260514, semua font tahoma
      AdvEdit.Font.Name := 'Tahoma';
      advedit.BevelKind := bkSoft;
      AdvEdit.BorderStyle := bsNone;
    end else if (Form.Components[j] is TButton) then begin
      Button := (Form.Components[j] as TButton);
      if (Button.Name='btnCancel') then
        Button.Cancel := true
      // handy
      else if (Button.Name='btnFilter') then
        Button.Visible  := False
      else if (UpperCase(Button.Name)='BTNPRINT') or (UpperCase(Button.Name)='BTNCETAK') or (UpperCase(Button.Name)='BTNPRIN')then
        Button.Caption  := '&Print'
      else if (UpperCase(Button.Name)='BTNEKSPOR') or (UpperCase(Button.Name)='BTNEKSPORT') or (UpperCase(Button.Name)='BTNEXCEL')then
        Button.Caption  := '&Export';
      // + fahmi 260514, semua font tahoma
      Button.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TPanel) then begin
      Panel := (Form.Components[j] as TPanel);
//      Panel.ParentBackground := True;
//      Panel.ParentColor := True;
      // + fahmi 260514, semua font tahoma
      Panel.Font.Name := 'Tahoma';
      panel.Color := frmHome.AdvPanelStyler2.Settings.Color;
//      panel.Color := $00E3F0F2;
    end else if (Form.Components[j] is TLabel) then begin
      ALabel := (Form.Components[j] as TLabel);
    	if ALabel.Tag = 0 then
        ALabel.Transparent := True
      else ALabel.Transparent:= False;
      if UpperCase(ALabel.Name) = 'LBLJURNAL' then
      ALabel.ParentColor := True;
      ALabel.Font.Color := clBlack;
      // + fahmi 260514, semua font tahoma
      ALabel.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TStaticText) then begin
      ASttText := (Form.Components[j] as TStaticText);
      ASttText.Transparent:= True;
      ASttText.ParentColor := True;
      // + fahmi 260514, semua font tahoma
      ASttText.Font.Name := 'Tahoma';
//      ASttText.Color := $00AFEFFF;
    end else if (Form.Components[j] is TGroupBox) then begin
      AGroupBox := (Form.Components[j] as TGroupBox);
      AGroupBox.Font.Name := 'Tahoma';
//      AGroupBox.ParentColor := True;
//      AGroupBox.Color := $00AFEFFF;
    end else if (Form.Components[j] is TAdvOfficePager) then begin
      APager := (Form.Components[j] as TAdvOfficePager);
      APager.AdvOfficePagerStyler := frmMainMenu.AdvOfficePagerOfficeStyler1;
    end else if (Form.Components[j] is TMemo) then begin
      AMemo := (Form.Components[j] as TMemo);
      AMemo.Font.Name := 'Tahoma';
      AMemo.BevelKind := bkSoft;
      AMemo.BorderStyle := bsNone;
    end  else  if (form.Components[j] is TAdvPanel) then begin
      APanelAdv := (form.Components[j] as TAdvPanel);
//      APanelAdv.Caption.Color := $003B8BD5;
//      APanelAdv.Caption.ColorTo := $00559ADB;
//      APanelAdv.Caption.Font.Color := clBlack;
      APanelAdv.Font.Name := 'Tahoma';
      APanelAdv.Caption.Font.Name := 'Tahoma';
      APanelAdv.Styler := frmMainMenu.AdvPanelStyler1;
//      APanelAdv.Caption.GradientDirection := AdvPanel.gdHorizontal;
//      APanelAdv.Color := $00AFEFFF;
    end else if (Form.Components[j] is TMemo) then begin
      AMemo := (Form.Components[j] as TMemo);
      AMemo.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TComboBox) then begin
      Acombo := (Form.Components[j] as TComboBox);
      Acombo.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TAdvOfficeCheckBox) then begin
      AdvCheckBox := (Form.Components[j] as TAdvOfficeCheckBox);
      AdvCheckBox.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TDateTimePicker) then begin
      ADate := (Form.Components[j] as TDateTimePicker);
      ADate.Font.Name := 'Tahoma';
    end  else  if (form.Components[j] is TAdvGlowButton) then begin
      AGlowButton := (form.Components[j] as TAdvGlowButton);
      if (ContainsStr(UpperCase(AGlowButton.Caption),'BATAL'))or
         (ContainsStr(UpperCase(AGlowButton.Caption),'TUTUP')) then begin
        AGlowButton.Cancel := true;
        AGlowButton.ModalResult := mrCancel;
      end;
      AGlowButton.Font.Style := [];
    end;



//  end;
end;
procedure CreateFormApp(AMenuId: Integer);
begin
//  case AMenuID of
//    1: Application.CreateForm(TfrmKelolaUser, frmKelolaUser);
//    2: Application.CreateForm(TfrmClosing, frmClosing);
//
//    201: Application.CreateForm(TfrmKasBankKelola, frmKasBankKelola);
//    202: Application.CreateForm(TfrmAkunkelola, frmAkunkelola);
//    203: Application.CreateForm(TfrmKelolaAccSetup, frmKelolaAccSetup);
//    204: Application.CreateForm(TfrmLihatPegawai, frmLihatPegawai);
//    205: Application.CreateForm(TfrmLihatRelasi, frmLihatRelasi);
//    206: Application.CreateForm(TfrmKelolaMasterSupplier, frmKelolaMasterSupplier);
//    207: Application.CreateForm(TfrmBarangPenyerta, frmBarangPenyerta);
//    208: Application.CreateForm(TfrmKelolaCabang, frmKelolaCabang);
//      209 : Application.CreateForm(TfrmRekapMasterKasBank, frmRekapMasterKasBank);
//    211: Application.CreateForm(TfrmKelolaMasterMotor, frmKelolaMasterMotor);
//    212: Application.CreateForm(TfrmKelolaMasterLeasing, frmKelolaMasterLeasing);
//    213: Application.CreateForm(TfrmKelolaMasterWarna, frmKelolaMasterWarna);
//    214: Application.CreateForm(TfrmKelolaMasterCustMotor, frmKelolaMasterCustMotor);
//    215: Application.CreateForm(TfrmKelolaSetingSubsidiLeasing, frmKelolaSetingSubsidiLeasing);
//    221: Application.CreateForm(TfrmKelolaMasterSparepart, frmKelolaMasterSparepart);
//    222: Application.CreateForm(TfrmKelolaSetingKSG, frmKelolaSetingKSG);
//    223: Application.CreateForm(TfrmKelolaMasterCustBengkel, frmKelolaMasterCustBengkel);
//
//    2001: Application.CreateForm(TfrmAlokasiRekap, frmAlokasiRekap);
//    2002: Application.CreateForm(TfrmMotorTrxPembelian, frmMotorTrxPembelian);
//    2003: Application.CreateForm(TfrmReturMotor, frmReturMotor);
//    2004: Application.CreateForm(TfrmMotorTrxPO, frmMotorTrxPO);
//    2005: Application.CreateForm(TfrmTerimaTolakRekap, frmTerimaTolakRekap);
//    2006: Application.CreateForm(TfrmMotorTrxKwitansi, frmMotorTrxKwitansi);
//    2007: Application.CreateForm(TfrmMotorTrxSJ, frmMotorTrxSJ);
//    2010: Application.CreateForm(TfrmMutasiMotorRekap, frmMutasiMotorRekap);
//    2011: Application.CreateForm(TfrmPOPart, frmPOPart);
//
//    2012: Application.CreateForm(TfrmBengkelTerimaPO, frmBengkelTerimaPO);
//    2013: Application.CreateForm(TfrmPenjualanHadiahRekap, frmPenjualanHadiahRekap);
//    2014, 2015, 2016, 2017,2018, 2019: Application.CreateForm(TfrmSuratMotorSamsat, frmSuratMotorSamsat);
//    2020, 2021: Application.CreateForm(TfrmMotorTrxChanelRekap, frmMotorTrxChanelRekap);
//    2101, 2102: Application.CreateForm(TfrmPOPart, frmPOPart);
//    2103, 2104: Application.CreateForm(TfrmBengkelTerimaPO, frmBengkelTerimaPO);
//    2105: Application.CreateForm(TfrmBengkelTrxRetur, frmBengkelTrxRetur);
//    2106: Application.CreateForm(TfrmBengkelTrxPenjualan, frmBengkelTrxPenjualan);
//    2107: Application.CreateForm(TfrmBengkelTrxSPK, frmBengkelTrxSPK);
//    2108: Application.CreateForm(TfrmBengkelTrxPenjualan, frmBengkelTrxSPK);
//    2109: Application.CreateForm(TfrmPOSparePartOli, frmPOSparePartOli);
//    2110: Application.CreateForm(TfrmBengkelTrxReturPenjualan, frmBengkelTrxReturPenjualan);
//    //persediaan
//    2201: Application.CreateForm(TfrmBengkelStokAdjusment, frmBengkelStokAdjusment);
//
//    //keuangan
//    3001: Application.CreateForm(TfrmRekapTerimaTandajadi, frmRekapTerimaTandajadi);
//    3002: Application.CreateForm(TfrmRekapTerimaCicilan, frmRekapTerimaCicilan);
//    3003: Application.CreateForm(TfrmPenerimaanLeasingRekap, frmPenerimaanLeasingRekap);
//    3004: Application.CreateForm(TfrmPenerimaanServiceRekap, frmPenerimaanServiceRekap);
//    3005, 3006: Application.CreateForm(TfrmPenerimaanPenjualanPartNonPartRekap, frmPenerimaanPenjualanPartNonPartRekap);
//    3007: Application.CreateForm(TfrmTerimaCicilanPegawaiRekap, frmTerimaCicilanPegawaiRekap);
//    3008: Application.CreateForm(TfrmBayarPinjamanRekap, frmBayarPinjamanRekap);
//    3009: Application.CreateForm(TfrmPinjamanRelasiRekap, frmPinjamanRelasiRekap);
//    3010: Application.CreateForm(TfrmPenerimaanLainRekap, frmPenerimaanLainRekap);
//    3101: Application.CreateForm(TfrmBayarUtangMotorRekap, frmBayarUtangMotorRekap);
//    3102: Application.CreateForm(TfrmBayarUtangSparePartRekap, frmBayarUtangSparePartRekap);
//    3103: Application.CreateForm(TfrmBayarBatalSPKRekap, frmBayarBatalSPKRekap);
//    3104: Application.CreateForm(TfrmBayarSTNKRekap, frmBayarSTNKRekap);
//    3105: Application.CreateForm(TfrmBayarBiayaRekap, frmBayarBiayaRekap);
//    3106: Application.CreateForm(TfrmKeluarKasbonRekap, frmKeluarKasbonRekap);
//    3107: Application.CreateForm(TfrmPinjamanRelasiRekap, frmPinjamanRelasiRekap);
//    3108: Application.CreateForm(TfrmBayarPinjamanRekap, frmBayarPinjamanRekap);
//    3201: Application.CreateForm(TfrmMutasiRekap, frmMutasiRekap);
//    3301: Application.CreateForm(TfrmJurnalRekap, frmJurnalRekap);
//    //piutang leasing
//    5001, 5002: Application.CreateForm(TfrmLaporanPiutangLeasing, frmLaporanPiutangLeasing);
//    //piutang relasi
//    5003, 5004,
//    5005, 5006 : Application.CreateForm(TfrmLaporanPiutangLeasing, frmLaporanPiutangLeasing);
//    //hutang motor
//    5101, 5102,
//    5105, 5106: Application.CreateForm(TfrmLaporanHutangMotor, frmLaporanHutangMotor);
//    //hutang relasi
//    5103, 5104: Application.CreateForm(TfrmLaporanHutangRelasi, frmLaporanHutangRelasi);
//    5211: Application.CreateForm(TfrmLaporanMutasiKasBank, frmLaporanMutasiKasBank);
//    //5212: Application.CreateForm(TfrmCashFlow, frmCashFlow);
//    //5301: frmWorkSheet.Execute(AMenuID, WORK_SHEET_TYPE);
////    5302: Application.CreateForm(TfrmWorkSheet, frmWorkSheet);
//    5303: Application.CreateForm(TfrmLabaRugiBaru, frmLabaRugiBaru);
//
//    5401 : Application.CreateForm(TfrmLaporanPembelian, frmLaporanPembelian);
//    5402 : Application.CreateForm(TfrmLaporanPajakPenjualamMotor, frmLaporanPajakPenjualamMotor);
//    5403 : Application.CreateForm(TfrmSumaryPenjualan, frmSumaryPenjualan);
//    5404 : Application.CreateForm(TfrmMotorLapLabaRugi, frmMotorLapLabaRugi);
//    5405 : Application.CreateForm(TfrmLaporanKurangBayar, frmLaporanKurangBayar);
//    5406 : Application.CreateForm(TfrmPenjualanHadiahRekap, frmPenjualanHadiahRekap);
//    5407 : Application.CreateForm(TfrmMotorInvStokRerkap, frmMotorInvStokRerkap);
//    5408 : Application.CreateForm(TfrmMotorInvStokAkhir, frmMotorInvStokAkhir);
//    5409 : Application.CreateForm(TfrmMotorInvStok, frmMotorInvStok);
//
//    5410, 5411 : Application.CreateForm(TfrmStokHadiah, frmStokHadiah);
//    //5411 : Application.CreateForm(TfrmStokHadiah.Execute(0, AMenuID);
//    5412 : Application.CreateForm(TfrmKartuStokHadiah, frmKartuStokHadiah);
//    5413 : Application.CreateForm(TfrmSuratMotor, frmSuratMotor);
//    5501 : Application.CreateForm(TfrmLapPembelianSparepart, frmLapPembelianSparepart);
//    5503 : Application.CreateForm(TfrmBengkelLapLabaRugi, frmBengkelLapLabaRugi);
//    5504 : Application.CreateForm(TfrmLapHarianRekap, frmLapHarianRekap);
//    5505, 5506 : Application.CreateForm(TfrmBengkelInvStok, frmBengkelInvStok);
//    //5506 : Application.CreateForm(TfrmBengkelInvStok.Execute(0, TRANS_TYPE_SPAREPART, AMenuID);
//    5507 : Application.CreateForm(TfrmKartuStokHadiah, frmKartuStokHadiah);
//    5508 : Application.CreateForm(TfrmPersediaanAkhirBaru, frmPersediaanAkhirBaru);
//    5509 : Application.CreateForm(TfrmLaporanKSG, frmLaporanKSG);
//    5510 : Application.CreateForm(TfrmLapMarginService, frmLapMarginService);
//    5601 : Application.CreateForm(TfrmLaporanHapusTransaksi,frmLaporanHapusTransaksi) ;
//    else raise EAbort.Create('MenuID not recognize');
//  end;
end;

end.

