unit UTransaksi;

interface
uses ADOInt, controls, OracleConnection, Urecord, UGeneral, DateUtils,classes, StrUtils, USystemMenu, UConst, SysUtils, UEngine, Gauges;


{data_jual_master}
procedure Reset_Record_data_jual_master (var Data : TR_data_jual_master);
function Get_data_jual_master (VKey : integer; seqJob : integer = 0) : TR_data_jual_master;
function Get_arr_data_jual_master (VKey : integer; seqJob : integer = 0; seqIn : string = '') : AR_data_jual_master;
function Save_data_jual_master (var R_data_jual_master : TR_data_jual_master) : integer;
procedure Delete_data_jual_master (VKey : integer);
procedure Update_data_jual_master (R_data_jual_master : TR_data_jual_master);
procedure Load_data_jual_master (var Buffer : _Recordset; Periode : TR_FilterPeriode; job_seq :
  integer = 0; card_seq : integer = 0; AtipePek : string = ''; addpar : string = '' );
procedure Load_data_jual_invoice_master(var Buffer : _Recordset; Periode : TR_FilterPeriode; job_seq :
  integer = 0; card_seq : integer = 0; AtipePek : string = ''; addpar : string = '' );
procedure update_terbayar(mengurangi : boolean; seqJual: integer; jumlah : real);

{data_jual_detail}
procedure Reset_Record_data_jual_detail (var Data : TR_data_jual_detail);
function Get_data_jual_detail (VKey : integer) : TR_data_jual_detail;
procedure Save_data_jual_detail (R_data_jual_detail : TR_data_jual_detail);
procedure Delete_data_jual_detail (MstSeq : integer);
procedure Update_data_jual_detail (R_data_jual_detail : TR_data_jual_detail);
function Get_Arr_data_jual_detail (MstSeq : integer; seqIn : string = '') : AR_data_jual_detail;


{data_beli_master}
procedure Reset_Record_data_beli_master (var Data : TR_data_beli_master);
function Get_data_beli_master (VKey : integer) : TR_data_beli_master;
function Save_data_beli_master (var R_data_beli_master : TR_data_beli_master) : integer;
procedure Delete_data_beli_master (VKey : integer);
procedure Update_data_beli_master (R_data_beli_master : TR_data_beli_master; istglbeli : boolean=false);
procedure Load_data_beli_master (var Buffer : _Recordset; Periode : TR_FilterPeriode; job_seq :
  integer = 0; card_seq : integer = 0; AtipePek : string = ''; addpar : string = '' );

{data_beli_detail}
procedure Reset_Record_data_beli_detail (var Data : TR_data_beli_detail);
function Get_data_beli_detail (VKey : integer) : TR_data_beli_detail;
procedure Save_data_beli_detail (R_data_beli_detail : TR_data_beli_detail);
procedure Delete_data_beli_detail (MstSeq : integer);
procedure Update_data_beli_detail (R_data_beli_detail : TR_data_beli_detail);
function Get_Arr_data_beli_detail (MstSeq : integer) : AR_data_beli_detail;


{invoice_master}
procedure Reset_Record_invoice_master (var Data : TR_invoice_master);
function Get_invoice_master (VKey : integer) : TR_invoice_master;
function Save_invoice_master (var R_invoice_master : TR_invoice_master) : integer;
procedure Delete_invoice_master (VKey : integer);
procedure Update_invoice_master (R_invoice_master : TR_invoice_master);
procedure Load_invoice_master (var Buffer : _Recordset; Periode : TR_FilterPeriode; card_seq : integer = 0; addpar : string = '' );

{invoice_detail}
procedure Reset_Record_invoice_detail (var Data : TR_invoice_detail);
function Get_invoice_detail (VKey : integer) : TR_invoice_detail;
procedure Save_invoice_detail (R_invoice_detail : TR_invoice_detail);
procedure Delete_invoice_detail (MstSeq : integer);
procedure Update_invoice_detail (R_invoice_detail : TR_invoice_detail);
function Get_Arr_invoice_detail (MstSeq : integer) : AR_invoice_detail;


implementation

uses
  UFinance;
var Sql : string;

{data_jual_master}
procedure Reset_Record_data_jual_master (var Data : TR_data_jual_master);
begin
  Data.seq := 0;
  Data.tanggal := 0;
  Data.nomor_po := '';
  Data.job_seq := 0;
  Data.card_seq := 0;
  Data.subtotal := 0;
  Data.ppn := 0;
  Data.total := 0;
  Data.keterangan := '';
  Data.tempo_byr := 0;
  Data.user_id := '';
  Data.tgl_input := 0;
  Data.user_edit := '';
  Data.tgl_edit := 0;
  data.terbayar :=0;
  data.sisa := 0;
end;

function Get_data_jual_master (VKey : integer; seqJob : integer = 0) : TR_data_jual_master;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, tanggal, nomor_po, job_seq, card_seq, subtotal, ppn, total, keterangan, ' +
    'tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa FROM data_jual_master '+
         ifthen(seqJob=0,'WHERE seq = '+FormatSQLNumber(VKey),
         'WHERE job_seq = '+FormatSQLNumber(seqJob));
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.tanggal := BufferToDateTime(buffer.Fields[1].Value);
    Result.nomor_po := BufferToString(buffer.Fields[2].Value);
    Result.job_seq := BufferToInteger(buffer.Fields[3].Value);
    Result.card_seq := BufferToInteger(buffer.Fields[4].Value);
    Result.subtotal := BufferToFloat(buffer.Fields[5].Value);
    Result.ppn := BufferToFloat(buffer.Fields[6].Value);
    Result.total := BufferToFloat(buffer.Fields[7].Value);
    Result.keterangan := BufferToString(buffer.Fields[8].Value);
    Result.tempo_byr := BufferToINteger(buffer.Fields[9].Value);
    Result.user_id := BufferToString(buffer.Fields[10].Value);
    Result.tgl_input := BufferToDateTime(buffer.Fields[11].Value);
    Result.user_edit := BufferToString(buffer.Fields[12].Value);
    Result.tgl_edit := BufferToDateTime(buffer.Fields[13].Value);
    Result.terbayar := BufferToFloat(buffer.Fields[14].Value);
    Result.sisa := BufferToFloat(buffer.Fields[15].Value);
  end else Reset_Record_data_jual_master(Result);
  buffer.Close;
end;


function Get_arr_data_jual_master (VKey : integer; seqJob : integer = 0; seqIn : string = '') : AR_data_jual_master;
var buffer : _Recordset;
    i : integer;
begin
  sql := 'SELECT seq, tanggal, nomor_po, job_seq, card_seq, subtotal, ppn, total, keterangan, ' +
    'tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa FROM data_jual_master '+
    ' where seq <> 0 '+
    ifthen(vkey <>0,' and seq = '+FormatSQLNumber(VKey))+
    ifthen(seqJob<>0, ' and job_seq = '+FormatSQLNumber(seqJob))+
    ifthen(seqin <> '' , ' and seq in ('+seqin+')');
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  if buffer.RecordCount > 0 then begin
    for i := 0 to buffer.RecordCount-1 do begin
      result[i].seq := BufferToInteger(buffer.Fields[0].Value);
      result[i].tanggal := BufferToDateTime(buffer.Fields[1].Value);
      result[i].nomor_po := BufferToString(buffer.Fields[2].Value);
      result[i].job_seq := BufferToInteger(buffer.Fields[3].Value);
      result[i].card_seq := BufferToInteger(buffer.Fields[4].Value);
      result[i].subtotal := BufferToFloat(buffer.Fields[5].Value);
      result[i].ppn := BufferToFloat(buffer.Fields[6].Value);
      result[i].total := BufferToFloat(buffer.Fields[7].Value);
      result[i].keterangan := BufferToString(buffer.Fields[8].Value);
      result[i].tempo_byr := BufferToINteger(buffer.Fields[9].Value);
      result[i].user_id := BufferToString(buffer.Fields[10].Value);
      result[i].tgl_input := BufferToDateTime(buffer.Fields[11].Value);
      result[i].user_edit := BufferToString(buffer.Fields[12].Value);
      result[i].tgl_edit := BufferToDateTime(buffer.Fields[13].Value);
      result[i].terbayar := BufferToFloat(buffer.Fields[14].Value);
      result[i].sisa := BufferToFloat(buffer.Fields[15].Value);
      buffer.MoveNext;
    end;
  end;
  //else Reset_Record_data_jual_master(Result);
  buffer.Close;
end;



function Save_data_jual_master (var R_data_jual_master : TR_data_jual_master) : integer;
begin
  myConnection.Tunggu_Transaksi_Lain_Beres('data_jual_master');
  try
    myConnection.BeginSQL;
    myConnection.Set_Kunci_Table_Transaksi('data_jual_master');
//    R_data_jual_master.nomor := GetNextNumber(R_data_jual_master.tanggal, TRANS_DATA_JUAL);
    R_data_jual_master.seq := CreateNewSeqMaster('data_jual_seq');
    sql := 'INSERT INTO data_jual_master (seq, tanggal, nomor_po, job_seq, card_seq, subtotal, ppn, ' +
      'total, keterangan, tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa)  '+
         'VALUES ('+FormatSQLNumber(R_data_jual_master.seq)+', '+
                    FormatSQLDate(R_data_jual_master.tanggal)+', '+
                    FormatSQLString(R_data_jual_master.nomor_po)+', '+
                    FormatSQLNumber(R_data_jual_master.job_seq)+', '+
                    FormatSQLNumber(R_data_jual_master.card_seq)+', '+
                    FormatSQLNumber(R_data_jual_master.subtotal)+', '+
                    FormatSQLNumber(R_data_jual_master.ppn)+', '+
                    FormatSQLNumber(R_data_jual_master.total)+', '+
                    FormatSQLString(R_data_jual_master.keterangan)+', '+
                    FormatSQLNumber(R_data_jual_master.tempo_byr)+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLDateTimeNow +', '+
                    FormatSQLString('')+', '+
                    FormatSQLNull2 +', '+
                    FormatSQLNumber(0)+', '+
                    FormatSQLNumber(R_data_jual_master.total)+')';
    ExecTransaction3(sql);
    Result := R_data_jual_master.seq;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := 0;
  end;
end;

procedure Delete_data_jual_master (VKey : integer);
begin
  sql := 'DELETE FROM data_jual_master '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;

procedure Update_data_jual_master (R_data_jual_master : TR_data_jual_master);
begin
  sql := 'UPDATE data_jual_master SET '+
                'tanggal = '+FormatSQLDate(R_data_jual_master.tanggal)+', '+
                'nomor_po = '+FormatSQLString(R_data_jual_master.nomor_po)+', '+
                'job_seq = '+FormatSQLNumber(R_data_jual_master.job_seq)+', '+
                'card_seq = '+FormatSQLNumber(R_data_jual_master.card_seq)+', '+    
                'sisa = '+FormatSQLNumber(R_data_jual_master.sisa)+', '+
                'subtotal = '+FormatSQLNumber(R_data_jual_master.subtotal)+', '+
                'ppn = '+FormatSQLNumber(R_data_jual_master.ppn)+', '+
                'total = '+FormatSQLNumber(R_data_jual_master.total)+', '+
                'keterangan = '+FormatSQLString(R_data_jual_master.keterangan)+', '+
                'tempo_byr = '+FormatSQLNumber(R_data_jual_master.tempo_byr)+', '+
                'user_edit = '+FormatSQLString(GlobalSystemUser.UserId)+', '+
                'tgl_edit = '+FormatSQLDateTimeNow+'  '+
         'WHERE seq = '+FormatSQLNumber(R_data_jual_master.seq);
 ExecTransaction3(sql);
end;

procedure Load_data_jual_master (var Buffer : _Recordset; Periode : TR_FilterPeriode; job_seq :
  integer = 0; card_seq : integer = 0; AtipePek : string = ''; addpar : string = '' );
var Filter : string;
begin
  filter := '';
  if Periode.FPeriodeAwal <> 0 then filter := filter + ' and trunc(tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAwal)+FormatSQLDate(Periode.FPeriodeAwal);
  if Periode.FPeriodeAkhir <> 0 then filter := filter + ' and trunc(tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAkhir)+FormatSQLDate(Periode.FPeriodeAkhir);
  if job_seq <> 0 then filter := filter + ' and job_seq = ' + FormatSqlNumber(job_seq);
  if card_seq <> 0 then filter := filter + ' and card_seq = ' + FormatSqlNumber(card_seq);
  filter := filter + ifthen(AtipePek<>'',' and job_seq in (select seq from master_job where ' +
    'unit_bisnis = '+FormatSQLString(AtipePek)+')');
  filter := filter + addpar;

//  (seq = 0)  (tanggal = 1)  (nomor_po = 2)  (job_seq = 3)  (card_seq = 4)  (subtotal = 5)  (ppn = 6)
// (total = 7)  (keterangan = 8)  (tempo_byr = 9)  (user_id = 10)  (tgl_input = 11)  (user_edit = 12)  (tgl_edit = 13)
  sql := 'SELECT seq, tanggal, nomor_po, job_seq, card_seq, subtotal, ppn, total, keterangan, ' +
    'tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa FROM data_jual_master '+
         ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter))) + ' order by tanggal, ' +
         'nomor_po ';
  buffer := myConnection.OpenSQL(sql);
end;

procedure Load_data_jual_invoice_master(var Buffer : _Recordset; Periode : TR_FilterPeriode; job_seq :
  integer = 0; card_seq : integer = 0; AtipePek : string = ''; addpar : string = '' );
var Filter : string;
begin
  filter := '';
  if Periode.FPeriodeAwal <> 0 then filter := filter + ' and trunc(tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAwal)+FormatSQLDate(Periode.FPeriodeAwal);
  if Periode.FPeriodeAkhir <> 0 then filter := filter + ' and trunc(tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAkhir)+FormatSQLDate(Periode.FPeriodeAkhir);
  if job_seq <> 0 then filter := filter + ' and job_seq = ' + FormatSqlNumber(job_seq);
  if card_seq <> 0 then filter := filter + ' and card_seq = ' + FormatSqlNumber(card_seq);
  filter := filter + ifthen(AtipePek<>'',' and job_seq in (select seq from master_job where ' +
    'unit_bisnis = '+FormatSQLString(AtipePek)+')');
  filter := filter + addpar;

//  (seq = 0)  (tanggal = 1)  (nomor_po = 2)  (job_seq = 3)  (card_seq = 4)  (subtotal = 5)  (ppn = 6)  (total = 7)  (keterangan = 8)  (tempo_byr = 9)  (user_id = 10)  (tgl_input = 11)  (user_edit = 12)  (tgl_edit = 13)
  sql := 'SELECT seq, tanggal, nomor_po, job_seq, card_seq, subtotal, ppn, total, keterangan, ' +
            ' tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa FROM ' +
            ' data_jual_master Where seq not in (select jual_seq from invoice_detail  '+filter +
          ' union all '+
          'SELECT seq, tanggal, nomor_po, job_seq, card_seq, subtotal, ppn, total, keterangan, ' +
            ' tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa FROM ' +
            'data_jual_master Where seq <> 0'+filter+

         ' order by tanggal, nomor_po ';
  buffer := myConnection.OpenSQL(sql);
end;

procedure update_terbayar(mengurangi : boolean; seqJual: integer; jumlah : real);
begin
  sql := ' update data_jual_master set terbayar = terbayar'+IfThen(mengurangi,'-','+')+
         FormatSQLNumber(jumlah)+' where seq = '+FormatSQLNumber(seqJual);
  myconnection.execsql(sql);
end;


{data_beli_detail}
procedure Reset_Record_data_beli_detail (var Data : TR_data_beli_detail);
begin
  Data.seq := 0;
  Data.master_seq := 0;
  Data.spesifikasi := '';
  Data.harga := 0;
  Data.ppn := 0;
  Data.qty := 0;
  Data.keterangan := '';
  Data.card_seq := 0;
  Data.satuan := '';
  Data.origin := '';
  Data.clust := '';
  Data.destination := '';
  Data.bpid := '';
  Data.site_name := '';
  Data.seq_detail_jual := 0;
  Data.serial := '';
end;

function Get_data_beli_detail (VKey : integer) : TR_data_beli_detail;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, master_seq, spesifikasi, harga, ppn, qty, keterangan, card_seq, satuan, origin, clust, destination, bpid, site_name, seq_detail_jual, serial FROM data_beli_detail '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.master_seq := BufferToInteger(buffer.Fields[1].Value);
    Result.spesifikasi := BufferToString(buffer.Fields[2].Value);
    Result.harga := BufferToFloat(buffer.Fields[3].Value);
    Result.ppn := BufferToFloat(buffer.Fields[4].Value);
    Result.qty := BufferToFloat(buffer.Fields[5].Value);
    Result.keterangan := BufferToString(buffer.Fields[6].Value);
    Result.card_seq := BufferToInteger(buffer.Fields[7].Value);
    Result.satuan := BufferToString(buffer.Fields[8].Value);
    Result.origin := BufferToString(buffer.Fields[9].Value);
    Result.clust := BufferToString(buffer.Fields[10].Value);
    Result.destination := BufferToString(buffer.Fields[11].Value);
    Result.bpid := BufferToString(buffer.Fields[12].Value);
    Result.site_name := BufferToString(buffer.Fields[13].Value);
    Result.seq_detail_jual := BufferToInteger(buffer.Fields[14].Value);
    Result.serial := BufferToString(buffer.Fields[13].Value);

  end else Reset_Record_data_beli_detail(Result);
  buffer.Close;
end;

procedure Save_data_beli_detail (R_data_beli_detail : TR_data_beli_detail);
begin
    sql := 'INSERT INTO data_beli_detail (seq, master_seq, spesifikasi, harga, ppn, qty, keterangan, card_seq, satuan, origin, clust, destination, bpid, site_name, seq_detail_jual, serial)  '+
         'VALUES ('+FormatSQLNumber(R_data_beli_detail.seq)+', '+
                    FormatSQLNumber(R_data_beli_detail.master_seq)+', '+
                    FormatSQLString(R_data_beli_detail.spesifikasi)+', '+
                    FormatSQLNumber(R_data_beli_detail.harga)+', '+
                    FormatSQLNumber(R_data_beli_detail.ppn)+', '+
                    FormatSQLNumber(R_data_beli_detail.qty)+', '+
                    FormatSQLString(R_data_beli_detail.keterangan)+', '+
                    FormatSQLNumber(R_data_beli_detail.card_seq)+', '+
                    FormatSQLString(R_data_beli_detail.satuan)+', '+
                    FormatSQLString(R_data_beli_detail.origin)+', '+
                    FormatSQLString(R_data_beli_detail.clust)+', '+
                    FormatSQLString(R_data_beli_detail.destination)+', '+
                    FormatSQLString(R_data_beli_detail.bpid)+', '+
                    FormatSQLString(R_data_beli_detail.site_name)+', '+
                    FormatSQLNumber(R_data_beli_detail.seq_detail_jual)+', '+
                    FormatSQLString(R_data_beli_detail.serial)+')';
  ExecTransaction3(sql);
end;
 
procedure Delete_data_beli_detail (MstSeq : integer);
begin
  sql := 'DELETE FROM data_beli_detail '+
         'WHERE master_seq = '+FormatSQLNumber(MstSeq);
  ExecTransaction3(sql);
end;
 
procedure Update_data_beli_detail (R_data_beli_detail : TR_data_beli_detail);
begin
  sql := 'UPDATE data_beli_detail SET '+
                'master_seq = '+FormatSQLNumber(R_data_beli_detail.master_seq)+', '+
                'spesifikasi = '+FormatSQLString(R_data_beli_detail.spesifikasi)+', '+
                'harga = '+FormatSQLNumber(R_data_beli_detail.harga)+', '+
                'ppn = '+FormatSQLNumber(R_data_beli_detail.ppn)+', '+
                'qty = '+FormatSQLNumber(R_data_beli_detail.qty)+', '+
                'keterangan = '+FormatSQLString(R_data_beli_detail.keterangan)+', '+
                'card_seq = '+FormatSQLNumber(R_data_beli_detail.card_seq)+', '+
                'satuan = '+FormatSQLString(R_data_beli_detail.satuan)+', '+
                'origin = '+FormatSQLString(R_data_beli_detail.origin)+', '+
                'clust = '+FormatSQLString(R_data_beli_detail.clust)+', '+
                'destination = '+FormatSQLString(R_data_beli_detail.destination)+', '+
                'bpid = '+FormatSQLString(R_data_beli_detail.bpid)+', '+
                'site_name = '+FormatSQLString(R_data_beli_detail.site_name)+', '+
                'serial = '+FormatSQLString(R_data_beli_detail.serial)+', '+
                'seq_detail_jual = '+FormatSQLNumber(R_data_beli_detail.seq_detail_jual)+'  '+
         'WHERE seq = '+FormatSQLNumber(R_data_beli_detail.seq);
 ExecTransaction3(sql);
end;
 
function Get_Arr_data_beli_detail (MstSeq : integer) : AR_data_beli_detail;
var buffer : _Recordset;
    i : integer;
begin
  sql := 'SELECT seq, master_seq, spesifikasi, harga, ppn, qty, keterangan, card_seq, satuan, origin, clust, destination, bpid, site_name, seq_detail_jual, serial FROM data_beli_detail '+
         'WHERE master_seq = '+FormatSQLNumber(MstSeq)+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].master_seq := BufferToInteger(buffer.Fields[1].Value);
    Result[i].spesifikasi := BufferToString(buffer.Fields[2].Value);
    Result[i].harga := BufferToFloat(buffer.Fields[3].Value);
    Result[i].ppn := BufferToFloat(buffer.Fields[4].Value);
    Result[i].qty := BufferToFloat(buffer.Fields[5].Value);
    Result[i].keterangan := BufferToString(buffer.Fields[6].Value);
    Result[i].card_seq := BufferToInteger(buffer.Fields[7].Value);
    Result[i].satuan := BufferToString(buffer.Fields[8].Value);
    Result[i].origin := BufferToString(buffer.Fields[9].Value);
    Result[i].clust := BufferToString(buffer.Fields[10].Value);
    Result[i].destination := BufferToString(buffer.Fields[11].Value);
    Result[i].bpid := BufferToString(buffer.Fields[12].Value);
    Result[i].site_name := BufferToString(buffer.Fields[13].Value);
    Result[i].seq_detail_jual := BufferToInteger(buffer.Fields[14].Value);
    Result[i].serial := BufferToString(buffer.Fields[15].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;
{data_beli_master}
procedure Reset_Record_data_beli_master (var Data : TR_data_beli_master);
begin
  Data.seq := 0;
  Data.tanggal := 0;
  Data.jual_seq := 0;
  Data.card_seq := 0;
  Data.job_seq := 0;
  Data.subtotal := 0;
  Data.ppn := 0;
  Data.total := 0;
  Data.keterangan := '';
  Data.user_id := '';
  Data.tgl_input := 0;
  Data.user_edit := '';
  Data.tgl_edit := 0;
  Data.Nomor_po := '';
  Data.tempo_byr := 0;
  Data.tgl_byr := 0;
end;

function Get_data_beli_master (VKey : integer) : TR_data_beli_master;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, tanggal, jual_seq, card_seq, subtotal, ppn, total, keterangan, user_id, ' +
    'tgl_input, user_edit, tgl_edit, job_seq, Nomor_po, tempo_byr, tgl_byr FROM data_beli_master '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.tanggal := BufferToDateTime(buffer.Fields[1].Value);
    Result.jual_seq := BufferToInteger(buffer.Fields[2].Value);
    Result.card_seq := BufferToInteger(buffer.Fields[3].Value);
    Result.subtotal := BufferToFloat(buffer.Fields[4].Value);
    Result.ppn := BufferToFloat(buffer.Fields[5].Value);
    Result.total := BufferToFloat(buffer.Fields[6].Value);
    Result.keterangan := BufferToString(buffer.Fields[7].Value);
    Result.user_id := BufferToString(buffer.Fields[8].Value);
    Result.tgl_input := BufferToDateTime(buffer.Fields[9].Value);
    Result.user_edit := BufferToString(buffer.Fields[10].Value);
    Result.tgl_edit := BufferToDateTime(buffer.Fields[11].Value);
    Result.job_seq := BufferTointeger(buffer.Fields[12].Value);
    Result.Nomor_po := BufferToString(buffer.Fields[13].Value);
    Result.tempo_byr := BufferTointeger(buffer.Fields[14].Value);
    Result.tgl_byr := BufferTointeger(buffer.Fields[15].Value);

  end else Reset_Record_data_beli_master(Result);
  buffer.Close;
end;

function Save_data_beli_master (var R_data_beli_master : TR_data_beli_master) : integer;
begin
  myConnection.Tunggu_Transaksi_Lain_Beres('data_beli_master');
  try
    myConnection.BeginSQL;
    myConnection.Set_Kunci_Table_Transaksi('data_beli_master');
//    R_data_beli_master.nomor := GetNextNumber(R_data_beli_master.tanggal, TRANS_DATA_beli);
    R_data_beli_master.seq := CreateNewSeqMaster('data_beli_seq');
    sql := 'INSERT INTO data_beli_master (seq, tanggal, jual_seq, job_seq, card_seq, subtotal, ppn, ' +
      'total, keterangan, user_id, tgl_input, user_edit, tgl_edit, Nomor_po, tempo_byr)  '+
         'VALUES ('+FormatSQLNumber(R_data_beli_master.seq)+', '+
                    FormatSQLDate(R_data_beli_master.tanggal)+', '+
                    FormatSQLNumber(R_data_beli_master.jual_seq)+', '+
                    FormatSQLNumber(R_data_beli_master.job_seq)+', '+
                    FormatSQLNumber(R_data_beli_master.card_seq)+', '+
                    FormatSQLNumber(R_data_beli_master.subtotal)+', '+
                    FormatSQLNumber(R_data_beli_master.ppn)+', '+
                    FormatSQLNumber(R_data_beli_master.total)+', '+
                    FormatSQLString(R_data_beli_master.keterangan)+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLDateTimeNow +', '+
                    FormatSQLString('')+', '+
                    FormatSQLNull2+', '+
                    FormatSQLString(R_data_beli_master.Nomor_po)+', '+
                    FormatSQLNumber(R_data_beli_master.tempo_byr)+')';
    ExecTransaction3(sql);
    Result := R_data_beli_master.seq;
    myConnection.EndSQL;
  except
    myConnection.UndoSQL;
    Result := 0;
  end;
end;

procedure Delete_data_beli_master (VKey : integer);
begin
  sql := 'DELETE FROM data_beli_master '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;

procedure Update_data_beli_master (R_data_beli_master : TR_data_beli_master; istglbeli : boolean=false);
begin
  if not istglbeli then begin
    sql := 'UPDATE data_beli_master SET '+
                  'tanggal = '+FormatSQLDate(R_data_beli_master.tanggal)+', '+
                  'job_seq = '+FormatSQLNumber(R_data_beli_master.job_seq)+', '+
                  'jual_seq = '+FormatSQLNumber(R_data_beli_master.jual_seq)+', '+
                  'card_seq = '+FormatSQLNumber(R_data_beli_master.card_seq)+', '+
                  'subtotal = '+FormatSQLNumber(R_data_beli_master.subtotal)+', '+
                  'tempo_byr = '+FormatSQLNumber(R_data_beli_master.tempo_byr)+', '+
                  'ppn = '+FormatSQLNumber(R_data_beli_master.ppn)+', '+
                  'total = '+FormatSQLNumber(R_data_beli_master.total)+', '+
                  'nomor_po = '+FormatSQLString(R_data_beli_master.nomor_po)+', '+
                  'keterangan = '+FormatSQLString(R_data_beli_master.keterangan)+', '+
                  'user_edit = '+FormatSQLString(GlobalSystemUser.UserId)+', '+
                  'tgl_edit = '+FormatSQLDateTimeNow+'  '+
           'WHERE seq = '+FormatSQLNumber(R_data_beli_master.seq);
  end else begin
    sql := 'UPDATE data_beli_master SET '+
                  'tgl_byr = '+ifthen(R_data_beli_master.tgl_byr=0,FormatSQLNULL2,FormatSQLDate(R_data_beli_master.tgl_byr))+' '+
           'WHERE seq = '+FormatSQLNumber(R_data_beli_master.seq);
  end;
 ExecTransaction3(sql);
end;

procedure Load_data_beli_master (var Buffer : _Recordset; Periode : TR_FilterPeriode; job_seq :
  integer = 0; card_seq : integer = 0; AtipePek : string = ''; addpar : string = '' );
var Filter : string;
begin
  filter := '';
  if Periode.FPeriodeAwal <> 0 then filter := filter + ' and trunc(bm.tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAwal)+FormatSQLDate(Periode.FPeriodeAwal);
  if Periode.FPeriodeAkhir <> 0 then filter := filter + ' and trunc(bm.tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAkhir)+FormatSQLDate(Periode.FPeriodeAkhir);
  if job_seq <> 0 then filter := filter + ' and bm.job_seq = ' + FormatSqlNumber(job_seq);
  if card_seq <> 0 then filter := filter + ' and bm.card_seq = ' + FormatSqlNumber(card_seq);
  filter := filter + ifthen(AtipePek<>'',' and bm.job_seq in (select seq from master_job where ' +
    'unit_bisnis = '+FormatSQLString(AtipePek)+')');
  filter := filter + addpar;

//  (seq = 0)  (tanggal = 1)  (jual_seq = 2)  (card_seq = 3)  (subtotal = 4)  (ppn = 5)  (total =
//6)  (keterangan = 7)  (user_id = 8)  (tgl_input = 9)  (user_edit = 10)  (tgl_edit = 11)
  sql := 'SELECT bm.seq, bm.tanggal, bm.jual_seq, bm.card_seq, bm.subtotal, bm.ppn, bm.total, ' +
          ' bm.keterangan, bm.user_id, bm.tgl_input, bm.user_edit, bm.tgl_edit, bm.job_seq, jm.nomor_po, ' +
          'jm.tempo_byr, jm.card_seq  '+
          ' FROM data_beli_master bm, data_jual_master jm '+
          ' WHERE jm.seq= bm.jual_seq ' + filter +
         ' union all '+
         ' SELECT bm.seq, bm.tanggal, bm.jual_seq, bm.card_seq, bm.subtotal, bm.ppn, bm.total, ' +
          ' bm.keterangan, bm.user_id, bm.tgl_input, bm.user_edit, bm.tgl_edit, bm.job_seq, ' +
          'bm.nomor_po, bm.tempo_byr, bm.card_seq  '+
          ' FROM data_beli_master bm '+
          ' WHERE bm.jual_seq = 0 ' + filter +
    ' order by tanggal';
  buffer := myConnection.OpenSQL(sql);
end;



{data_jual_detail}
procedure Reset_Record_data_jual_detail (var Data : TR_data_jual_detail);
begin
  Data.seq := 0;
  Data.master_seq := 0;
  Data.spesifikasi := '';
  Data.origin := '';
  Data.clust := '';
  Data.destination := '';
  Data.bpid := '';
  Data.satuan := '';
  Data.harga := 0;
  Data.ppn := 0;
  Data.qty := 0;
  Data.keterangan := '';
  Data.site_name := '';
  Data.serial:= '';
end;

function Get_data_jual_detail (VKey : integer) : TR_data_jual_detail;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, master_seq, spesifikasi, origin, clust, destination, bpid, satuan, harga, ppn, qty, keterangan, site_name, serial FROM data_jual_detail '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.master_seq := BufferToInteger(buffer.Fields[1].Value);
    Result.spesifikasi := BufferToString(buffer.Fields[2].Value);
    Result.origin := BufferToString(buffer.Fields[3].Value);
    Result.clust := BufferToString(buffer.Fields[4].Value);
    Result.destination := BufferToString(buffer.Fields[5].Value);
    Result.bpid := BufferToString(buffer.Fields[6].Value);
    Result.satuan := BufferToString(buffer.Fields[7].Value);
    Result.harga := BufferToFloat(buffer.Fields[8].Value);
    Result.ppn := BufferToFloat(buffer.Fields[9].Value);
    Result.qty := BufferToFloat(buffer.Fields[10].Value);
    Result.keterangan := BufferToString(buffer.Fields[11].Value);
    Result.site_name := BufferToString(buffer.Fields[12].Value);
    Result.serial := BufferToString(buffer.Fields[13].Value);

  end else Reset_Record_data_jual_detail(Result);
  buffer.Close;
end;

procedure Save_data_jual_detail (R_data_jual_detail : TR_data_jual_detail);
begin
    sql := 'INSERT INTO data_jual_detail (seq, master_seq, spesifikasi, origin, clust, destination, bpid, satuan, harga, ppn, qty, keterangan, site_name, serial)  '+
         'VALUES ('+FormatSQLNumber(R_data_jual_detail.seq)+', '+
                    FormatSQLNumber(R_data_jual_detail.master_seq)+', '+
                    FormatSQLString(R_data_jual_detail.spesifikasi)+', '+
                    FormatSQLString(R_data_jual_detail.origin)+', '+
                    FormatSQLString(R_data_jual_detail.clust)+', '+
                    FormatSQLString(R_data_jual_detail.destination)+', '+
                    FormatSQLString(R_data_jual_detail.bpid)+', '+
                    FormatSQLString(R_data_jual_detail.satuan)+', '+
                    FormatSQLNumber(R_data_jual_detail.harga)+', '+
                    FormatSQLNumber(R_data_jual_detail.ppn)+', '+
                    FormatSQLNumber(R_data_jual_detail.qty)+', '+
                    FormatSQLString(R_data_jual_detail.keterangan)+', '+
                    FormatSQLString(R_data_jual_detail.site_name)+', '+
                    FormatSQLString(R_data_jual_detail.serial)+')';
  ExecTransaction3(sql);
end;

procedure Delete_data_jual_detail (MstSeq : integer);
begin
  sql := 'DELETE FROM data_jual_detail '+
         'WHERE master_seq = '+FormatSQLNumber(MstSeq);
  ExecTransaction3(sql);
end;

procedure Update_data_jual_detail (R_data_jual_detail : TR_data_jual_detail);
begin
  sql := 'UPDATE data_jual_detail SET '+
                'master_seq = '+FormatSQLNumber(R_data_jual_detail.master_seq)+', '+
                'spesifikasi = '+FormatSQLString(R_data_jual_detail.spesifikasi)+', '+
                'origin = '+FormatSQLString(R_data_jual_detail.origin)+', '+
                'clust = '+FormatSQLString(R_data_jual_detail.clust)+', '+
                'destination = '+FormatSQLString(R_data_jual_detail.destination)+', '+
                'bpid = '+FormatSQLString(R_data_jual_detail.bpid)+', '+
                'satuan = '+FormatSQLString(R_data_jual_detail.satuan)+', '+
                'harga = '+FormatSQLNumber(R_data_jual_detail.harga)+', '+
                'ppn = '+FormatSQLNumber(R_data_jual_detail.ppn)+', '+
                'qty = '+FormatSQLNumber(R_data_jual_detail.qty)+', '+
                'keterangan = '+FormatSQLString(R_data_jual_detail.keterangan)+', '+
                'serial = '+FormatSQLString(R_data_jual_detail.serial)+', '+
                'site_name = '+FormatSQLString(R_data_jual_detail.site_name)+'  '+
         'WHERE seq = '+FormatSQLNumber(R_data_jual_detail.seq);
 ExecTransaction3(sql);
end;

function Get_Arr_data_jual_detail (MstSeq : integer; seqIn : string = '') : AR_data_jual_detail;
var buffer : _Recordset;
    i : integer;
begin
  sql := 'SELECT seq, master_seq, spesifikasi, origin, clust, destination, bpid, satuan, harga, ppn, qty, keterangan, site_name, serial FROM data_jual_detail '+
         'WHERE seq <> 0 '+
         ifthen(MstSeq<>0,' and master_seq = '+FormatSQLNumber(MstSeq))+
         ifthen(seqIn<>'',' and seq = '+FormatSQLNumber(MstSeq))+

         ' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].master_seq := BufferToInteger(buffer.Fields[1].Value);
    Result[i].spesifikasi := BufferToString(buffer.Fields[2].Value);
    Result[i].origin := BufferToString(buffer.Fields[3].Value);
    Result[i].clust := BufferToString(buffer.Fields[4].Value);
    Result[i].destination := BufferToString(buffer.Fields[5].Value);
    Result[i].bpid := BufferToString(buffer.Fields[6].Value);
    Result[i].satuan := BufferToString(buffer.Fields[7].Value);
    Result[i].harga := BufferToFloat(buffer.Fields[8].Value);
    Result[i].ppn := BufferToFloat(buffer.Fields[9].Value);
    Result[i].qty := BufferToFloat(buffer.Fields[10].Value);
    Result[i].keterangan := BufferToString(buffer.Fields[11].Value);
    Result[i].site_name := BufferToString(buffer.Fields[12].Value);
    Result[i].serial := BufferToString(buffer.Fields[13].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;





{invoice_master}
procedure Reset_Record_invoice_master (var Data : TR_invoice_master);
begin
  Data.seq := 0;
  Data.nomor := '';
  Data.tanggal := 0;
  Data.card_seq := 0;
  Data.total := 0;
  Data.keterangan := '';
  Data.user_id := '';
  Data.tgl_input := 0;
  Data.user_edit := '';
  Data.tgl_edit := 0;
  Data.nomor_bast := '';
  Data.nomor_baut := '';
  Data.nomor_kwt := '';
  Data.deskripsi := '';
  Data.bank_seq := 0;
end;

function Get_invoice_master (VKey : integer) : TR_invoice_master;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, nomor, tanggal, card_seq, total, keterangan, user_id, tgl_input, user_edit, tgl_edit, nomor_bast, nomor_baut, nomor_kwt, deskripsi, bank_seq FROM invoice_master '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin 
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.nomor := BufferToString(buffer.Fields[1].Value);
    Result.tanggal := BufferToDateTime(buffer.Fields[2].Value);
    Result.card_seq := BufferToInteger(buffer.Fields[3].Value);
    Result.total := BufferToFloat(buffer.Fields[4].Value);
    Result.keterangan := BufferToString(buffer.Fields[5].Value);
    Result.user_id := BufferToString(buffer.Fields[6].Value);
    Result.tgl_input := BufferToDateTime(buffer.Fields[7].Value);
    Result.user_edit := BufferToString(buffer.Fields[8].Value);
    Result.tgl_edit := BufferToDateTime(buffer.Fields[9].Value);
    Result.nomor_bast := BufferToString(buffer.Fields[10].Value);
    Result.nomor_baut := BufferToString(buffer.Fields[11].Value);
    Result.nomor_kwt := BufferToString(buffer.Fields[12].Value);
    Result.deskripsi := BufferToString(buffer.Fields[13].Value);
    Result.bank_seq := BufferToInteger(buffer.Fields[14].Value);
  end else Reset_Record_invoice_master(Result); 
  buffer.Close;
end;
 
function Save_invoice_master (var R_invoice_master : TR_invoice_master) : integer;
begin 
  myConnection.Tunggu_Transaksi_Lain_Beres('invoice_master');
//  try
//    myConnection.BeginSQL;
    myConnection.Set_Kunci_Table_Transaksi('invoice_master');
    R_invoice_master.nomor := GetNextNumber(R_invoice_master.tanggal, TRANS_INVOICE, R_invoice_master.card_seq);  
    R_invoice_master.seq := CreateNewSeqMaster('invoice_seq');
    sql := 'INSERT INTO invoice_master (seq, nomor, tanggal, card_seq, total, keterangan, user_id, tgl_input, user_edit, tgl_edit, nomor_bast, nomor_baut, nomor_kwt, deskripsi, bank_seq)  '+
         'VALUES ('+FormatSQLNumber(R_invoice_master.seq)+', '+
                    FormatSQLString(R_invoice_master.nomor)+', '+
                    FormatSQLDate(R_invoice_master.tanggal)+', '+
                    FormatSQLNumber(R_invoice_master.card_seq)+', '+
                    FormatSQLNumber(R_invoice_master.total)+', '+
                    FormatSQLString(R_invoice_master.keterangan)+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLDateTimeNow +', '+
                    FormatSQLString('')+', '+
                    FormatSQLNull2 +', '+
                    FormatSQLString(R_invoice_master.nomor_bast)+', '+
                    FormatSQLString(R_invoice_master.nomor_baut)+', '+
                    FormatSQLString(R_invoice_master.nomor_kwt)+', '+
                    FormatSQLString(R_invoice_master.deskripsi)+', '+
                    FormatSQLNumber(R_invoice_master.bank_seq)+')';
    ExecTransaction3(sql);
    Result := R_invoice_master.seq;
//    myConnection.EndSQL;
//  except
//    myConnection.UndoSQL;
//    Result := 0;
//  end;
end;

procedure Delete_invoice_master (VKey : integer);
begin
  sql := 'DELETE FROM invoice_master '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;
 
procedure Update_invoice_master (R_invoice_master : TR_invoice_master);
begin
  sql := 'UPDATE invoice_master SET '+
                'nomor = '+FormatSQLString(R_invoice_master.nomor)+', '+
                'tanggal = '+FormatSQLDate(R_invoice_master.tanggal)+', '+
                'card_seq = '+FormatSQLNumber(R_invoice_master.card_seq)+', '+
                'total = '+FormatSQLNumber(R_invoice_master.total)+', '+
                'bank_seq = '+FormatSQLNumber(R_invoice_master.bank_seq)+', '+
                'keterangan = '+FormatSQLString(R_invoice_master.keterangan)+', '+
                'user_edit = '+FormatSQLString(GlobalSystemUser.UserId)+', '+
                'tgl_edit = '+FormatSQLDateTimeNow+', '+
                'nomor_bast = '+FormatSQLString(R_invoice_master.nomor_bast)+', '+
                'nomor_baut = '+FormatSQLString(R_invoice_master.nomor_baut)+', '+
                'nomor_kwt = '+FormatSQLString(R_invoice_master.nomor_kwt)+', '+
                'deskripsi = '+FormatSQLString(R_invoice_master.deskripsi)+'  '+
         'WHERE seq = '+FormatSQLNumber(R_invoice_master.seq);
 ExecTransaction3(sql);
end;
 

procedure Load_invoice_master (var Buffer : _Recordset; Periode : TR_FilterPeriode; card_seq : integer = 0; addpar : string = '' );
var Filter : string;
begin
  filter := '';
  if Periode.FPeriodeAwal <> 0 then filter := filter + ' and trunc(tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAwal)+FormatSQLDate(Periode.FPeriodeAwal); 
  if Periode.FPeriodeAkhir <> 0 then filter := filter + ' and trunc(tanggal) '+FormatSQLOperator(Periode.FOprPeriodeAkhir)+FormatSQLDate(Periode.FPeriodeAkhir);
  if card_seq <> 0 then filter := filter + ' and card_seq = ' + FormatSqlNumber(card_seq);
    filter := filter + addpar; 

//  (seq = 0)  (nomor = 1)  (tanggal = 2)  (card_seq = 3)  (total = 4)  (keterangan = 5)  (user_id = 6)
//(tgl_input = 7)  (user_edit = 8)  (tgl_edit = 9)  (nomor_bast = 10)  (nomor_baut = 11)  (nomor_kwt = 12)  (deskripsi = 13) 
  sql := 'SELECT seq, nomor, tanggal, card_seq, total, keterangan, user_id, tgl_input, user_edit, tgl_edit, nomor_bast, '+
       ' nomor_baut, nomor_kwt, deskripsi, bank_seq FROM invoice_master '+
         ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter))) + ' order by tanggal, nomor ';
  buffer := myConnection.OpenSQL(sql);
end;
 
{invoice_detail}
procedure Reset_Record_invoice_detail (var Data : TR_invoice_detail);
begin
  Data.seq := 0;
  Data.master_seq := 0;
  Data.jual_seq := 0;
  Data.nomor := '';
  Data.dibayar := 0;
  Data.keterangan := '';
end;
 
function Get_invoice_detail (VKey : integer) : TR_invoice_detail;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, master_seq, jual_seq, nomor, dibayar, keterangan FROM invoice_detail '+
         'WHERE master_seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin 
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.master_seq := BufferToInteger(buffer.Fields[1].Value);
    Result.jual_seq := BufferToInteger(buffer.Fields[2].Value);
    Result.nomor := BufferToString(buffer.Fields[3].Value);
    Result.dibayar := BufferToFloat(buffer.Fields[4].Value);
    Result.keterangan := BufferToString(buffer.Fields[5].Value);
  end else Reset_Record_invoice_detail(Result); 
  buffer.Close;
end;
 
procedure Save_invoice_detail (R_invoice_detail : TR_invoice_detail);
begin 
    sql := 'INSERT INTO invoice_detail (seq, master_seq, jual_seq, nomor, dibayar, keterangan)  '+
         'VALUES ('+FormatSQLNumber(R_invoice_detail.seq)+', '+
                    FormatSQLNumber(R_invoice_detail.master_seq)+', '+
                    FormatSQLNumber(R_invoice_detail.jual_seq)+', '+
                    FormatSQLString(R_invoice_detail.nomor)+', '+
                    FormatSQLNumber(R_invoice_detail.dibayar)+', '+
                    FormatSQLString(R_invoice_detail.keterangan)+')';
  ExecTransaction3(sql);
end;
 
procedure Delete_invoice_detail (MstSeq : integer);
begin
  sql := 'DELETE FROM invoice_detail '+
         'WHERE master_seq = '+FormatSQLNumber(MstSeq);
  ExecTransaction3(sql);
end;
 
procedure Update_invoice_detail (R_invoice_detail : TR_invoice_detail);
begin
  sql := 'UPDATE invoice_detail SET '+
                'master_seq = '+FormatSQLNumber(R_invoice_detail.master_seq)+', '+
                'jual_seq = '+FormatSQLNumber(R_invoice_detail.jual_seq)+', '+
                'nomor = '+FormatSQLString(R_invoice_detail.nomor)+', '+
                'dibayar = '+FormatSQLNumber(R_invoice_detail.dibayar)+', '+
                'keterangan = '+FormatSQLString(R_invoice_detail.keterangan)+'  '+
         'WHERE seq = '+FormatSQLNumber(R_invoice_detail.seq);
 ExecTransaction3(sql);
end;
 
function Get_Arr_invoice_detail (MstSeq : integer) : AR_invoice_detail;
var buffer : _Recordset;
    i : integer;
begin
  sql := 'SELECT seq, master_seq, jual_seq, nomor, dibayar, keterangan FROM invoice_detail '+
         'WHERE master_seq = '+FormatSQLNumber(MstSeq)+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin 
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].master_seq := BufferToInteger(buffer.Fields[1].Value);
    Result[i].jual_seq := BufferToInteger(buffer.Fields[2].Value);
    Result[i].nomor := BufferToString(buffer.Fields[3].Value);
    Result[i].dibayar := BufferToFloat(buffer.Fields[4].Value);
    Result[i].keterangan := BufferToString(buffer.Fields[5].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;



 


end.









