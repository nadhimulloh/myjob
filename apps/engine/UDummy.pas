unit UDummy;

interface
uses ADOInt, controls, OracleConnection, Urecord, UGeneral, DateUtils,classes, StrUtils, USystemMenu, UConst, SysUtils, UEngine;


function Save_Dummy_data_jual_master (R_data_jual_master : TR_data_jual_master) : integer;

procedure Save_Dummy_data_jual_detail (SeqDummy,SeqMst : Integer);

function Save_Dummy_data_beli_master (R_data_beli_master : TR_data_beli_master) : integer;

procedure Save_Dummy_data_beli_detail (SeqDummy,SeqMst : Integer);

function Save_Dummy_invoice_master (R_invoice_master : TR_invoice_master) : integer;

procedure Save_Dummy_invoice_detail (SeqDummy,SeqMst : Integer);


implementation
var sql : string;

function Save_Dummy_data_jual_master (R_data_jual_master : TR_data_jual_master) : integer;
begin
  R_data_jual_master.seq := CreateNewSeqMaster('dummy_data_jual_seq');
    sql := 'INSERT INTO Dummy_data_jual_master (seq, tanggal, nomor_po, job_seq, card_seq, subtotal,' +
      ' ppn, total, keterangan, tempo_byr, user_id, tgl_input, user_edit, tgl_edit, terbayar, sisa)  '+
         '  VALUES ('+FormatSQLNumber(R_data_jual_master.seq)+', '+
                    FormatSQLDate(R_data_jual_master.tanggal)+', '+
                    FormatSQLString(R_data_jual_master.nomor_po)+', '+
                    FormatSQLNumber(R_data_jual_master.job_seq)+', '+
                    FormatSQLNumber(R_data_jual_master.card_seq)+', '+
                    FormatSQLNumber(R_data_jual_master.subtotal)+', '+
                    FormatSQLNumber(R_data_jual_master.ppn)+', '+
                    FormatSQLNumber(R_data_jual_master.total)+', '+
                    FormatSQLString(R_data_jual_master.keterangan)+', '+
                    FormatSQLNumber(R_data_jual_master.tempo_byr)+', '+
                    FormatSQLString(R_data_jual_master.user_id)+', '+
                    FormatSQLDateTime(R_data_jual_master.tgl_input) +', '+
                    FormatSQLString(R_data_jual_master.user_edit)+', '+
                    ifthen(trunc(R_data_jual_master.tgl_edit) = 0,FormatSQLNull2,FormatSQLDateTime(
                      R_data_jual_master.tgl_edit))+', '+
                    FormatSQLNumber(R_data_jual_master.terbayar)+', '+
                    FormatSQLNumber(R_data_jual_master.sisa)+')';
  Result := R_data_jual_master.seq;
  ExecTransaction3(sql);
end;


procedure Save_Dummy_data_jual_detail (SeqDummy,SeqMst : Integer);
begin
    sql := 'INSERT INTO Dummy_data_jual_detail (seq, master_seq, spesifikasi, origin, clust, destination, bpid, satuan, harga, ppn, qty, keterangan, site_name)  '+
           '(select seq, '+FormatSQLNumber(SeqDummy)+',spesifikasi, origin, clust, destination, ' +
           'bpid, satuan, harga, ppn, qty, keterangan, site_name FROM data_jual_detail WHERE ' +
           'Master_Seq = '+FormatSqlNumber(SeqMst)+')';
  ExecTransaction3(sql);
end;



function Save_Dummy_data_beli_master (R_data_beli_master : TR_data_beli_master) : integer;
begin
  R_data_beli_master.seq := CreateNewSeqMaster('dummy_data_beli_seq');
    sql := 'INSERT INTO Dummy_data_beli_master (seq, tanggal, jual_seq, card_seq, subtotal, ppn, total, keterangan, user_id, tgl_input, user_edit, tgl_edit,tgl_byr)  '+
         '  VALUES ('+FormatSQLNumber(R_data_beli_master.seq)+', '+
                    FormatSQLDate(R_data_beli_master.tanggal)+', '+
                    FormatSQLNumber(R_data_beli_master.jual_seq)+', '+
                    FormatSQLNumber(R_data_beli_master.card_seq)+', '+
                    FormatSQLNumber(R_data_beli_master.subtotal)+', '+
                    FormatSQLNumber(R_data_beli_master.ppn)+', '+
                    FormatSQLNumber(R_data_beli_master.total)+', '+
                    FormatSQLString(R_data_beli_master.keterangan)+', '+
                    FormatSQLString(R_data_beli_master.user_id)+', '+
                    FormatSQLDateTime(R_data_beli_master.tgl_input) +', '+
                    FormatSQLString(R_data_beli_master.user_edit)+', '+
                    ifthen(trunc(R_data_beli_master.tgl_edit) = 0, FormatSQLNull2, FormatSQLDateTime(R_data_beli_master.tgl_edit)) +', '+
                    ifthen(trunc(R_data_beli_master.tgl_byr) = 0, FormatSQLNull2, FormatSQLDateTime(R_data_beli_master.tgl_byr)) +')';
  Result := R_data_beli_master.seq;
  ExecTransaction3(sql);
end;


procedure Save_Dummy_data_beli_detail (SeqDummy,SeqMst : Integer);
begin 
    sql := 'INSERT INTO Dummy_data_beli_detail (seq, master_seq, spesifikasi, harga, ppn, qty, keterangan, card_seq, satuan, origin, clust, destination, bpid, site_name, seq_detail_jual)  '+
           '(select seq, '+FormatSQLNumber(SeqDummy)+',spesifikasi, harga, ppn, qty, keterangan, card_seq, satuan, origin, clust, destination, bpid, site_name, seq_detail_jual FROM data_beli_detail WHERE Master_Seq = '+FormatSqlNumber(SeqMst)+')';
  ExecTransaction3(sql);
end;


 
function Save_Dummy_invoice_master (R_invoice_master : TR_invoice_master) : integer;
begin 
  R_invoice_master.seq := CreateNewSeqMaster('dummy_invoice_seq');
    sql := 'INSERT INTO Dummy_invoice_master (seq, nomor, tanggal, card_seq, total, keterangan, user_id, tgl_input, user_edit, tgl_edit, nomor_bast, nomor_baut, nomor_kwt, deskripsi,bank_seq)  '+
         '  VALUES ('+FormatSQLNumber(R_invoice_master.seq)+', '+
                    FormatSQLString(R_invoice_master.nomor)+', '+
                    FormatSQLDate(R_invoice_master.tanggal)+', '+
                    FormatSQLNumber(R_invoice_master.card_seq)+', '+
                    FormatSQLNumber(R_invoice_master.total)+', '+
                    FormatSQLString(R_invoice_master.keterangan)+', '+
                    FormatSQLString(R_invoice_master.user_id)+', '+
                    FormatSQLDateTime(R_invoice_master.tgl_input) +', '+
                    FormatSQLString(R_invoice_master.user_edit)+', '+
                    ifthen(trunc(R_invoice_master.tgl_edit) = 0,FormatSQLNull2,FormatSQLDateTime(R_invoice_master.tgl_edit)) +', '+
                    FormatSQLString(R_invoice_master.nomor_bast)+', '+
                    FormatSQLString(R_invoice_master.nomor_baut)+', '+
                    FormatSQLString(R_invoice_master.nomor_kwt)+', '+
                    FormatSQLString(R_invoice_master.deskripsi)+', '+
                    FormatSQLNumber(R_invoice_master.bank_seq)+')';
  Result := R_invoice_master.seq;
  ExecTransaction3(sql);
end;


procedure Save_Dummy_invoice_detail (SeqDummy,SeqMst : Integer);
begin 
    sql := 'INSERT INTO Dummy_invoice_detail (seq, master_seq, jual_seq, nomor, dibayar, keterangan)  '+
           '(select seq, '+FormatSQLNumber(SeqDummy)+',jual_seq, nomor, dibayar, keterangan FROM invoice_detail WHERE Master_Seq = '+FormatSqlNumber(SeqMst)+')';
  ExecTransaction3(sql);
end;


end.


