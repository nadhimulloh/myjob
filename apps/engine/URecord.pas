unit URecord;

interface
uses ADOInt, controls, oracleConnection;

type

TR_FilterPeriode = Record
  FPeriodeAwal, FPeriodeAkhir,
  FPeriodeAwal2, FPeriodeAkhir2 : TDate;
  FOprPeriodeAwal, FOprPeriodeAkhir,
  FOprPeriodeAwal2, FOprPeriodeAkhir2 : TSQLOperator;
End;


TR_master_card = Record
  seq : integer;
  kode : string[50];
  nama : string[50];
  kode_nomor : string[5];
  unit_bisnis : string[10];
  tipe : string[5];
  jenis : string[5];
  alamat : string[255];
  kota : string[255];
  provinsi : string[255];
  negara : string[255];
  kodepos : string[20];
  no_telp1 : string[20];
  no_telp2 : string[20];
  no_fax : string[20];
  email : string[255];
  website : string[255];
  kontak : string[255];
  png_jwb1 : string[70];
  jabatan1 : string[70];
  png_jwb2 : string[70];
  jabatan2 : string[70];
  user_id : string[50];
  tgl_input : TDateTime;
  keterangan : string[255];
  tgl_hapus : TDateTime;
  isBautBast : string[5];
end;

TR_master_job = Record
  seq : integer;
  kode : string[50];
  nama : string[50];
  unit_bisnis : string[50];
  lokasi : string[255];
  card_seq : integer;
  deskripsi : string[255];
  contact : string[50];
  tgl_mulai : TDateTime;
  tgl_target : TDateTime;
  user_id : string[50];
  tgl_input : TDateTime;
  tgl_hapus : TDateTime;
  no_po : string[50];
  tgl_Bayar :TDateTime;
end;
       
TR_master_bank = Record
  seq : integer;
  nama : string[50];
  cabang : string[255]; 
  norek : string[255];
  an : string[255];
  user_id : string[50];
  tgl_input : TDateTime;
  tgl_hapus : TDateTime;
end;

TR_company_info = Record
  nama : string[50];
  alamat : string[255];
  no_telp : string[20];
  no_fax : string[20];
  email : string[255];
  no_npwp : string[50 ];
  png_jwb1 : string[70];
  jabatan1 : string[70];
  png_jwb2 : string[70];
  jabatan2 : string[70];
  logo : string[255];
end;

TR_LOG_FILE = Record
  Seq : integer;
  Seq_Dummy : integer;
  Seq_Edit : integer;
  Nama_Tabel : string[50];
  Tanggal : TDateTime;
  User_ID : string[30];
  Keterangan : string[250];
  Checked : string[10];
  catatan_hapus : string[250];
end;

TR_KetEditHapus = Record
  IsEdit    : Boolean;
  NamaTrans : String;
  IsMasuk   : TBooleanOperator;
  NamaVC    : String;
  NoTrans   : String;
  TglTrans  : TDate;
end;

TR_data_jual_master = Record
  seq : integer;
  tanggal : TDateTime;
  nomor_po : string[50];
  job_seq : integer;
  card_seq : integer;
  subtotal : real;
  ppn : real;
  total : real;
  keterangan : string[255];
  tempo_byr : integer;
  terbayar : real;
  sisa : real;
  user_id : string[50];
  tgl_input : TDateTime;
  user_edit : string[50];
  tgl_edit : TDateTime;
end;

TR_data_jual_detail = Record
  seq : integer;
  master_seq : integer;
  spesifikasi : string[255];
  origin : string[255];
  clust : string[255];
  destination : string[255];
  bpid : string[255];
  site_name: string[255];
  satuan : string[255];
  harga : real;
  ppn : real;
  qty : real;
  keterangan : string[255];
  serial : string[50];
end;

TR_data_beli_master = Record
  seq : integer;
  tanggal : TDateTime;
  jual_seq : integer;
  job_seq : integer;
  card_seq : integer;
  subtotal : real;
  ppn : real;
  total : real;
  keterangan : string[255];
  user_id : string[50];
  tgl_input : TDateTime;
  user_edit : string[50];
  tgl_edit : TDateTime;
  Nomor_po : string[50];
  tempo_byr : integer;
  tgl_byr : TDateTime;
end;

TR_data_beli_detail = Record
  seq : integer;
  master_seq : integer;
  spesifikasi : string[255];
  harga : real;
  ppn : real;
  qty : real;
  keterangan : string[255];
  card_seq : integer;
  satuan : string[50];
  origin : string[255];
  clust : string[255];
  destination : string[255];
  bpid : string[255];
  site_name: string[255];
  seq_detail_jual: integer;
  serial : string[50];
end;

TR_invoice_master = Record
  seq : integer;
  nomor : string[50];
  tanggal : TDateTime;
  card_seq : integer;
  total : real;
  keterangan : string[255];
  user_id : string[50];
  tgl_input : TDateTime;
  user_edit : string[50];
  tgl_edit : TDateTime;
  nomor_bast : string[50];
  nomor_baut : string[50];
  nomor_kwt  : string[50];
  deskripsi  : string[255];
  bank_seq  : integer;
end;

TR_invoice_detail = Record
  seq : integer;
  master_seq : integer;
  jual_seq : integer;
  nomor : string[50];
  dibayar : real;
  keterangan : string[255];
end;

TR_master_ppn = Record 
  seq : integer; 
  tgl_berlaku : TDateTime; 
  ppn : real; 
  tgl_nonaktif : TDateTime; 
end;



AR_invoice_master = array of TR_invoice_master;
AR_invoice_detail = array of TR_invoice_detail;
AR_data_jual_master = array of TR_data_jual_master;
AR_data_jual_detail = array of TR_data_jual_detail;
AR_data_beli_master = array of TR_data_beli_master;
AR_data_beli_detail = array of TR_data_beli_detail;

AR_master_bank = array of TR_master_bank;
AR_master_card = array of TR_master_card;
AR_master_job = array of TR_master_job;
AR_company_info = array of TR_company_info;
AR_LOG_FILE = array of TR_LOG_FILE;
AR_master_ppn = array of TR_master_ppn;




implementation

end.



