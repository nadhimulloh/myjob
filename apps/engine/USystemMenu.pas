unit USystemMenu;

interface

uses
  UConst, OracleConnection, 
  SysUtils, Controls, ADODB, Forms, ComCtrls, StdCtrls, Dialogs, Classes, Graphics,Messages,StrUtils,
  Variants, URecord,
  //Series,
  //Chart,
   AdvGrid, AdvEdit, DateUtils, Math, IniFiles, RTLConsts, Types;

type
  _SystemUser_Arr = class;
  _SystemUser = class;
  TSystemUser_Arr = class;
  TSystemUser = class;
  //Table Name: System_User
  //Fields:
  //  0. User_Id* (string[20])
  //  1. User_Name (string[50])
  //  2. Passwd (string[20])
  //  3. Access_Level (integer)
  //  4. Disabled_Date (TDate)
  //Properties:
  //  0. UserId* (string)
  //  1. UserName (string)
  //  2. Passwd (string)
  //  3. AccessLevel (integer)
  //  4. DisableDate (TDate)

  _SystemMenu_Arr = class;
  _SystemMenu = class;
  TSystemMenu_Arr = class;
  TSystemMenu = class;
  //Table Name: System_Menu
  //Fields:
  //  0. Menu_Id* (integer)
  //  1. Menu_Group (string[20])
  //  2. Menu_Name (string[40])
  //  3. Image_Index (integer)
  //  4. Form_Name (string[40])
  //  5. Form_Type (string[1])
  //Properties:
  //  0. MenuId* (integer)
  //  1. MenuGroup (string)
  //  2. MenuName (string)
  //  3. ImageIndex (integer)
  //  4. FormName (string)
  //  5. FormType (string)

  _SystemAccess_Arr = class;
  _SystemAccess = class;
  TSystemAccess_Arr = class;
  TSystemAccess = class;
  //Table Name: System_Access
  //Fields:
  //  0. User_Id* (string[20]) Reference to: System_User\User_Id
  //  1. Menu_Id* (integer) Reference to: System_Menu\Menu_Id
  //  2. Access_List (integer)
  //  3. Shortcut (integer)
  //  4. Outlook_Panel (string[20])
  //  5. Outlook_Button (string[40])
  //  6. Usage_Count (integer)                                                                      h
  //Properties:
  //  0. SystemUser* (_SystemUser)
  //  1. SystemMenu* (_SystemMenu)
  //  2. AccessList (integer)
  //  3. Shortcut (integer)
  //  4. OutlookPanel (string)
  //  5. OutlookButton (string)
  //  6. UsageCount (integer)

  _SystemUser_Arr = class(TObject) {Strong-Container SystemUser}
  protected
    FSystemUser_Arr: array of _SystemUser;
    function Get(Index: integer): _SystemUser; virtual;
    function New: _SystemUser; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear; virtual;
    function Count: integer;
    property SystemUser_Arr[Index: integer]: _SystemUser read Get; default;

    function Add(AUserId: string; AUserName: string; APasswd: string; AAccessLevel: integer; ADisableDate: TDate; ADeptId: STring): integer; virtual;
    function IndexOf(AUserId: string): integer; virtual;
    procedure Delete(Index: integer); virtual;
  end;

  _SystemUser = class(TObject) {Strong-Basic SystemUser}
  protected
    FUserId: string; //[20]
    FUserName: string; //[50]
    FPasswd: string; //[20]
    FAccessLevel, FPegawaiSeq: integer; //+puji, FPegawaiSeq
    FDisableDate: TDate; //
    FDeptId : String;                                       
  public
    constructor Create;
    destructor Destroy;override;
    procedure Reset; virtual;
    procedure InsertOnDB; virtual;
    procedure UpdateOnDB; virtual;
    procedure DeleteOnDB; virtual;
    property UserId: string  read FUserId write FUserId;
    property UserName: string  read FUserName write FUserName;
    property Passwd: string  read FPasswd write FPasswd;
    property AccessLevel: integer  read FAccessLevel write FAccessLevel;
    property DisableDate: TDate  read FDisableDate write FDisableDate;
    property DeptId: String  read FDeptId write FDeptId;
    property PegawaiSeq: integer  read FPegawaiSeq write FPegawaiSeq; //+puji
    function SelectInDB(AUserId, ADeptId: string): boolean; virtual;
    class function ExistInDB(AUserId: string): integer; virtual;
  end;

  _SystemMenu_Arr = class(TObject) {Strong-Container SystemMenu}
  protected
    FSystemMenu_Arr: array of _SystemMenu;
    function Get(Index: integer): _SystemMenu; virtual;
    function New: _SystemMenu; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear; virtual;
    function Count: integer;
    property SystemMenu_Arr[Index: integer]: _SystemMenu read Get; default;

    function FindOnDB(AMenuId: integer = 0; AMenuGroup: string = ''; AMenuName: string = ''; AImageIndex: integer = 0; AFormName: string = ''; AFormType: integer = 0; AShortcut: integer = 0;aDeptID:string=''): integer; virtual;
    function Add(AMenuId: integer; AMenuGroup: string; AMenuName: string; AImageIndex: integer; AFormName: string; AFormType: integer; AShortcut: integer;ASubmenuName:string;aDeptID:string): integer; virtual;
    function IndexOf(AMenuId: integer): integer; virtual;
    procedure Delete(Index: integer); virtual;
  end;

  _SystemMenu = class(TObject) {Strong-Basic SystemMenu}
  protected
    FMenuId: integer; //
    FMenuGroup: string; //[20]
    FMenuName: string; //[40]
    FImageIndex: integer; //
    FFormName: string; //[40]
    FFormType: integer; //[1]
    FShortcut: integer; //
//    FUrutan : integer;//juli tambah untuk urutan menu
    FSubMenuName : string;
    FDeptID : string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Reset; virtual;
    procedure InsertOnDB; virtual;
    procedure UpdateOnDB; virtual;
    procedure DeleteOnDB; virtual;
    property MenuId: integer  read FMenuId write FMenuId;
    property MenuGroup: string  read FMenuGroup write FMenuGroup;
    property MenuName: string  read FMenuName write FMenuName;
    property ImageIndex: integer  read FImageIndex write FImageIndex;
    property FormName: string  read FFormName write FFormName;
    property FormType: integer  read FFormType write FFormType;
    property Shortcut: integer  read FShortcut write FShortcut;
    property SubMenuName : string read FSubMenuName write FSubMenuName;
    property DeptID : string read FDeptID write FDeptID;
    function SelectInDB(AMenuId: integer): boolean; virtual;
    class function ExistInDB(AMenuId: integer): integer; virtual;
  end;

  _SystemAccess_Arr = class(TObject) {Weak-Container SystemAccess}
  protected
    FSystemAccess_Arr: array of _SystemAccess;
    function Get(Index: integer): _SystemAccess; virtual;
    function New: _SystemAccess; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear; virtual;
    function Count: integer;virtual;
    property SystemAccess_Arr[Index: integer]: _SystemAccess read Get; default;

    function Add(AUserId: string; AMenuId: integer; AAccessList: integer; AShortcut: integer; AOutlookPanel: string; AOutlookButton: string; AUsageCount: integer): integer; virtual;
    function IndexOf(AUserId: string; AMenuId: integer): integer; virtual;
    procedure Delete(Index: integer); virtual;
  end;

  _SystemAccess = class(TObject) {Weak-Basic SystemAccess}
  protected
    FSystemUser: _SystemUser; //UserId: string
    FSystemMenu: _SystemMenu; //MenuId: integer
    FAccessList: integer; //
    FOutlookPanel: string; //[20]
    FOutlookButton: string; //[40]
    FUsageCount: integer; //
  public
    constructor Create;
    destructor Destroy; override;
    procedure Reset; virtual;
    procedure InsertOnDB; virtual;
    procedure UpdateOnDB; virtual;
    procedure DeleteOnDB; virtual;
    property SystemUser: _SystemUser  read FSystemUser;
    property SystemMenu: _SystemMenu  read FSystemMenu;
    property AccessList: integer  read FAccessList write FAccessList;
    property OutlookPanel: string  read FOutlookPanel write FOutlookPanel;
    property OutlookButton: string  read FOutlookButton write FOutlookButton;
    property UsageCount: integer  read FUsageCount write FUsageCount;
  end;


  TSystemUser_Arr = class(_SystemUser_Arr)
  protected
    function Get(Index: integer): TSystemUser; reintroduce;overload;
    function New: _SystemUser; override;
  public
    procedure FindOnDB(LoginID:string='';LoginName:string='';AccessLevel:integer=-1;status:boolean=true;Dept:string='');
    property SystemUser_Arr[Index: integer]: TSystemUser read Get; default;
  end;

  TSystemUser = class(_SystemUser)
  protected
    FSystemAccess_Arr: TSystemAccess_Arr;
    function getDisable: boolean;
    procedure setDisable(const Value: boolean);
  public
    property SystemAccess_Arr: TSystemAccess_Arr read FSystemAccess_Arr write FSystemAccess_Arr;
    procedure UpdatePasswordOnDB;
    property is_disabled:boolean read getDisable write setDisable;
    procedure InsertOnDB; override;
    procedure UpdateOnDB; override;
    procedure Aktivasi_User (USER_ID : string; status : boolean);
    class procedure AccessLevelList(var ATarget:TStringList);
    class function IsExistInDB(userID:string): boolean;
    class function UserIDAlreadyRegistered(UserID: String): boolean;
  end;

  //TSystemReversal = class(_SystemReversal);
  //TSystemReversal_Arr = class(_SystemReversal_Arr);

  TFormType = (ftDetail,ftLookup,ftReview,ftBrowse,ftReport,ftSetting);
  TReportType = (rtMaster,rtDetail,rtMasterDetail,rtGlobal);
  TSystemMenu = class(_SystemMenu)
  private
    function GetFormTypeNew: Integer;
  protected                               
    function GetFormType: TFormType;
    procedure SetFormType(const Value: TFormType);
    function GetShortcut: TShortcut;
    procedure SetShortcut(const Value: TShortcut);
  published
  public
    function getMenuPrefix: string;
    property FormType: TFormType read GetFormType write SetFormType;
    property Shortcut: TShortcut read GetShortcut write SetShortcut;
    property FormTypeNew : Integer read GetFormTypeNew;
  end;

  TSystemMenu_Arr = class(_SystemMenu_Arr)
  protected
    function Get(Index: integer): TSystemMenu; reintroduce;
    function New: _SystemMenu; override;
  public
    function GetByMenuID(AMenuID: integer): TSystemMenu;
    property SystemMenu_Arr[Index: integer]: TSystemMenu read Get; default;
    procedure LoadFromDB(ADeptId: String=''; AccessLevel: integer=0; AUserId: string = '');
    procedure LoadFromDB_User(isfromKelolaUser : boolean = false);

    procedure getMenuGroup(var menuArr:TSystemMenu_Arr;menu_group:string);
    procedure getMenuGroupList(var aList:TStringList);
    function FindInDB(menu_id:integer=0;menu_group:string='';menu_name:string='';image_index:integer=-1;form_name:string='';form_type:integer=0;shortcut:integer=0):boolean;

  end;
{  //constanta untuk jenis form
  JENIS_FORM_AKSI_INPUT   = 1;
  JENIS_FORM_AKSI_EDIT    = 2;
  JENIS_FORM_AKSI_HAPUS   = 3;
  JENIS_FORM_AKSI_REKAP   = 4;
  JENIS_FORM_AKSI_REPORT  = 5;
  JENIS_FORM_AKSI_CETAK   = 6;
  JENIS_FORM_AKSI_EKSPORT = 7;}
  //TAccessList = (alInsert, alUpdate, alView,alPrint,alRevers);

  TAccessList = (alAksInput, alAksEdit, alAksHapus, alAksRekap{24}, alAksReport, alAksCetak, alAksExport,alAksTutup, alAksPosting );
  TAccessListSet = set of TAccessList;
  TSystemAccess = class(_SystemAccess)
  protected
    FOwner: TObject; //pointer;
    function GetAccessList: TAccessListSet;
    procedure SetAccessList(const Value: TAccessListSet);
    function GetSystemMenu: TSystemMenu;
    function GetSystemUser: TSystemUser;
  public
    constructor Create(AOwner: TObject);
    destructor Destroy; override;
    procedure Reset; override;
    function SelectInDB(AUserID: string; AMenuID: integer): boolean; overload;
    function SelectInDB: boolean; overload;

    procedure IncUsageCount;
    procedure UpdateUsageCountOnDB;
    procedure UpdateOutlookOnDB;
    property SystemUser: TSystemUser read GetSystemUser;
    property SystemMenu: TSystemMenu read GetSystemMenu;
    property AccessList: TAccessListSet read GetAccessList write SetAccessList;
    property UsageCount: integer read FUsageCount;
  end;

  TSystemAccess_Arr = class(_SystemAccess_Arr)
  protected
    FOwner: TObject; //pointer
    FIndexUsage_Arr  : array of integer;
    function Get(Index: integer): TSystemAccess; reintroduce;
    function New: _SystemAccess; override;
    function GetTopUsage(Position: integer): TSystemAccess;
  public
    constructor Create(AOwner: TObject);
    function LoadFromDB(AUserID: string; AMenuID: integer; ADeptId: String = ''): integer; overload;
    function LoadFromDB_All(AUserId: String; ADeptId: String = ''): integer; overload;
    function LoadFromDB: integer; overload;
    procedure SaveUsageCountOnDB;
    procedure SaveOutlookOnDB;

    function GetByFormName(AFormName: string): TSystemAccess;
    function GetByMenuID(AMenuID: integer): TSystemAccess;
    property SystemAccess_Arr[Index: integer]: TSystemAccess read Get; default;
    function Add(AUserId: string; AMenuId: integer; AAccessList: TAccessListSet; AShortcut: TShortcut; AOutlookPanel: string; AOutlookButton: string; AUsageCount: integer): integer; reintroduce; overload;
    function Add(AUserId: string; AAccessList: TAccessListSet; AShortcut: TShortcut; AOutlookPanel: string; AOutlookButton: string; AUsageCount: integer): integer; reintroduce; overload;
    function Add(AMenuId: integer; AAccessList: TAccessListSet; AShortcut: TShortcut; AOutlookPanel: string; AOutlookButton: string; AUsageCount: integer): integer; reintroduce; overload;
    function IndexOf(AUserId: string; AMenuId: integer): integer; reintroduce; overload;
    function IndexOf(AUserId: string): integer; reintroduce; overload;
    function IndexOf(AMenuId: integer): integer; reintroduce; overload;

    procedure InsertOnDB;
    procedure DeleteUpdateOnDB;//delete and then re-insert
    procedure RecalculateUsage;
    property TopUsage[Position: integer]: TSystemAccess read GetTopUsage;
    procedure LoadFromDBNew(var buffer : _Recordset; AUserID: string; AMenuID: integer;
                            FORMTYPE : integer = 0; MenuGroup : string = ''; SubMenu : string = '');
  end;

  TClosing_Trans = class
    User_id : String;
    MaxHari : Integer;
    constructor Create;
    function SetClosing: Boolean;
    //procedure StartDateTransaksi;
    class function SetTanggalTransaksi(ATgl: TDate): TDate;
    class function GetMaxHari: Integer;
  end;

  arString = array of string;
//  arInteger = array of integer;
  

var
  GlobalSystemUser: TSystemUser;
  GlobalSystemMenu_Arr: TSystemMenu_Arr;
  Global_Date: TDate;
  Global_Prog_Pkg_Number: String;
  GlobalListDocType: TStringList;
  PeriodeTransaksi : TClosing_Trans;

implementation

uses UGeneral;
{ _SystemUser_Arr }

constructor _SystemUser_Arr.Create;
begin
  inherited Create;
  setLength(FSystemUser_Arr,0);
end;

destructor _SystemUser_Arr.Destroy;
begin
  Self.Clear;
  inherited;
end;

procedure _SystemUser_Arr.Clear;
var i: integer;
begin
  for i:=0 to high(FSystemUser_Arr) do FSystemUser_Arr[i].Destroy;
  setLength(FSystemUser_Arr,0);
end;

function _SystemUser_Arr.New: _SystemUser;
begin
  Result := _SystemUser.Create;
end;

function _SystemUser_Arr.Add(AUserId: string; AUserName: string; APasswd: string; AAccessLevel: integer; ADisableDate: TDate; ADeptId: STring): integer;
begin
  setLength(FSystemUser_Arr, length(FSystemUser_Arr)+1);
  Result := high(FSystemUser_Arr);
  FSystemUser_Arr[Result] := New;
  FSystemUser_Arr[Result].UserId := AUserId;
  FSystemUser_Arr[Result].UserName := AUserName;
  FSystemUser_Arr[Result].Passwd := APasswd;
  FSystemUser_Arr[Result].AccessLevel := AAccessLevel;
  FSystemUser_Arr[Result].DisableDate := ADisableDate;
  FSystemUser_Arr[Result].FDeptId := ADeptId;
end;

function _SystemUser_Arr.IndexOf(AUserId: string): integer;
var i: integer; found: boolean;
begin
  i := 0; found := false;
  while (i<=high(FSystemUser_Arr)) and (not found) do
    if (FSystemUser_Arr[i].FUserId=AUserId) then
      found := true
    else inc(i);
  if (found) then
    Result := i
  else
    Result := -1;
end;

procedure _SystemUser_Arr.Delete(Index: integer);
var i: integer;
begin
  if (Index<0) or (Index>high(FSystemUser_Arr)) then Exit;
  FSystemUser_Arr[Index].Destroy;
  for i:=Index to high(FSystemUser_Arr)-1 do
    FSystemUser_Arr[Index] := FSystemUser_Arr[Index-1];
  setLength(FSystemUser_Arr,length(FSystemUser_Arr)-1);
end;

function _SystemUser_Arr.Count: integer;
begin
  Result := length(FSystemUser_Arr);
end;

function _SystemUser_Arr.Get(Index: integer): _SystemUser;
begin
  if (Index<0) or (Index>high(FSystemUser_Arr)) then Result := nil
  else Result := FSystemUser_Arr[Index];
end;


{ _SystemUser }

constructor _SystemUser.Create;
begin
  inherited Create;
end;

destructor _SystemUser.Destroy;
begin
  inherited;
end;

procedure _SystemUser.Reset;
begin
  FUserId := '';
  FUserName := '';
  FPasswd := '';
  FAccessLevel := 0;
  FDisableDate := 0;
  FDeptId := '';
end;

procedure _SystemUser.InsertOnDB;
begin
  try myConnection.ExecSQL(
    'INSERT INTO System_User (User_Id,User_Name,Passwd,Access_Level,Disabled_Date, dept_id)'+
    ' VALUES ('+
      FormatSQLString(FUserId)+','+
      FormatSQLString(FUserName)+','+
      FormatSQLString(FPasswd)+','+
      FormatSQLNumber(FAccessLevel)+','+
      FormatSQLDate(FDisableDate)+', '+
      FormatSQLString(FDeptId)+')'
  ); except raise; end;
end;

procedure _SystemUser.UpdateOnDB;
begin
  try myConnection.ExecSQL(
    'UPDATE System_User SET'+
          ' User_Name='+FormatSQLString(FUserName)+','+
          ' Passwd='+FormatSQLString(FPasswd)+','+
          ' Access_Level='+FormatSQLNumber(FAccessLevel)+','+
          ' Disabled_Date='+FormatSQLDate(FDisableDate)+','+
          ' dept_id='+FormatSQLString(FDeptId)+' '+
    ' WHERE User_Id='+FormatSQLString(FUserId)
  ); except raise; end;
end;

procedure _SystemUser.DeleteOnDB;
begin
  try myConnection.ExecSQL(
    'DELETE FROM System_User'+
    ' WHERE User_Id='+FormatSQLString(FUserId)
  ); except raise; end;
end;

function _SystemUser.SelectInDB(AUserId, ADeptId: string): boolean;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT SU.User_Id, SU.User_Name, SU.Passwd, SU.Access_Level, SU.Disabled_Date, dept_id '+
    'FROM System_User SU '+
    'WHERE SU.User_Id='+FormatSQLString(AUserId)//+' AND SU.dept_id='+FormatSQLString(ADeptId)
  );
  if (buffer.RecordCount<>0) then begin
    FUserId := BufferToString(buffer.Fields[0].Value);
    FUserName := BufferToString(buffer.Fields[1].Value);
    FPasswd := BufferToString(buffer.Fields[2].Value);
    FAccessLevel := BufferToInteger(buffer.Fields[3].Value);
    FDisableDate := BufferToDateTime(buffer.Fields[4].Value);
    FDeptId := BufferToString(buffer.Fields[5].Value);
  end;
  Result := (buffer.RecordCount<>0);
  buffer.Close;
end;

class function _SystemUser.ExistInDB(AUserId: string): integer;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT COUNT(*) FROM System_User'+
    ' WHERE User_Id='+FormatSQLString(AUserId)
  );
  Result := BufferToInteger(buffer.Fields[0].Value);
  buffer.Close;
end;

{ _SystemMenu_Arr }

constructor _SystemMenu_Arr.Create;
begin
  inherited Create;
  setLength(FSystemMenu_Arr,0);
end;

destructor _SystemMenu_Arr.Destroy;
begin
  Self.Clear;
  inherited;
end;

procedure _SystemMenu_Arr.Clear;
var i: integer;
begin
  for i:=0 to high(FSystemMenu_Arr) do FSystemMenu_Arr[i].Destroy;
  setLength(FSystemMenu_Arr,0);
end;

function _SystemMenu_Arr.New: _SystemMenu;
begin
  Result := _SystemMenu.Create;
end;

function _SystemMenu_Arr.Add(AMenuId: integer; AMenuGroup: string; AMenuName: string;
  AImageIndex: integer; AFormName: string;
  AFormType: integer; AShortcut: integer; ASubmenuName:string; aDeptID:string): integer;
begin
  setLength(FSystemMenu_Arr, length(FSystemMenu_Arr)+1);
  Result := high(FSystemMenu_Arr);
  FSystemMenu_Arr[Result] := New;
  FSystemMenu_Arr[Result].MenuId := AMenuId;
  FSystemMenu_Arr[Result].MenuGroup := AMenuGroup;
  FSystemMenu_Arr[Result].MenuName := AMenuName;
  FSystemMenu_Arr[Result].ImageIndex := AImageIndex;
  FSystemMenu_Arr[Result].FormName := AFormName;
  FSystemMenu_Arr[Result].FormType := AFormType;
  FSystemMenu_Arr[Result].Shortcut := AShortcut;
  FSystemMenu_Arr[Result].SubMenuName := ASubmenuName;
end;

function _SystemMenu_Arr.IndexOf(AMenuId: integer): integer;
var i: integer; found: boolean;
begin
  i := 0; found := false;
  while (i<=high(FSystemMenu_Arr)) and (not found) do
    if (FSystemMenu_Arr[i].FMenuId=AMenuId)
    then
      found := true
    else inc(i);
  if (found) then
    Result := i
  else
    Result := -1;
end;

procedure _SystemMenu_Arr.Delete(Index: integer);
var i: integer;
begin
  if (Index<0) or (Index>high(FSystemMenu_Arr)) then Exit;
  FSystemMenu_Arr[Index].Destroy;
  for i:=Index to high(FSystemMenu_Arr)-1 do
    FSystemMenu_Arr[Index] := FSystemMenu_Arr[Index-1];
  setLength(FSystemMenu_Arr,length(FSystemMenu_Arr)-1);
end;

function _SystemMenu_Arr.Count: integer;
begin
  Result := length(FSystemMenu_Arr);
end;

function _SystemMenu_Arr.Get(Index: integer): _SystemMenu;
begin
  if (Index<0) or (Index>high(FSystemMenu_Arr)) then Result := nil
  else Result := FSystemMenu_Arr[Index];
end;

function _SystemMenu_Arr.FindOnDB(AMenuId: integer = 0; AMenuGroup: string = ''; AMenuName: string = ''; AImageIndex: integer = 0; AFormName: string = ''; AFormType: integer = 0; AShortcut: integer = 0;aDeptID:string=''): integer;
var sql, filter: string; buffer: _RecordSet; i: integer;
begin
  Self.Clear;
  sql := 'SELECT ... FROM ... ';
  filter := '';
  if (AMenuId<>0) then filter := filter + ' AND SM.MenuId='+FormatSQLNumber(AMenuId);
  if (AMenuGroup<>'') then filter := filter + ' AND SM.MenuGroup='+FormatSQLString(AMenuGroup);
  if (AMenuName<>'') then filter := filter + ' AND SM.MenuName='+FormatSQLString(AMenuName);
  if (AImageIndex<>0) then filter := filter + ' AND SM.ImageIndex='+FormatSQLNumber(AImageIndex);
  if (AFormName<>'') then filter := filter + ' AND SM.FormName='+FormatSQLString(AFormName);
  if (AFormType<>0) then filter := filter + ' AND SM.FormType='+FormatSQLNumber(AFormType);
  if (AShortcut<>0) then filter := filter + ' AND SM.Shortcut='+FormatSQLNumber(AShortcut);
  if (filter<>'') then filter := copy(filter,6,length(filter)-5) + ' WHERE ';
  buffer := myConnection.OpenSQL(sql+filter);
  setLength(FSystemMenu_Arr, buffer.RecordCount);
  for i:=0 to buffer.RecordCount-1 do begin
    FSystemMenu_Arr[i] := _SystemMenu.Create;
    Self.Add(BufferToInteger(buffer.Fields[0].Value),BufferToString(buffer.Fields[1].Value),
        BufferToString(buffer.Fields[2].Value),BufferToInteger(buffer.Fields[3].Value),
        BufferToString(buffer.Fields[4].Value),BufferToInteger(buffer.Fields[5].Value),
        BufferToInteger(buffer.Fields[6].Value),BufferToString(buffer.Fields[7].Value),
        BufferToString(buffer.Fields[8].Value));
    buffer.MoveNext;
  end;
  Result := buffer.RecordCount;
  buffer.Close;
end;


{ _SystemMenu }

constructor _SystemMenu.Create;
begin
  inherited Create;
end;

destructor _SystemMenu.Destroy;
begin
  inherited;
end;

procedure _SystemMenu.Reset;
begin
  FMenuId := 0;
  FMenuGroup := '';
  FMenuName := '';
  FImageIndex := 0;
  FFormName := '';
  FFormType := 0;
  FShortcut := 0;
  FSubMenuName := '';
  FDeptID := '';
end;

procedure _SystemMenu.InsertOnDB;
begin
  try myConnection.ExecSQL(
    'INSERT INTO System_Menu (Menu_Id,Menu_Group,Menu_Name,Image_Index,Form_Name,Form_Type,Shortcut)'+
    ' VALUES ('+
      FormatSQLNumber(FMenuId)+','+
      FormatSQLString(FMenuGroup)+','+
      FormatSQLString(FMenuName)+','+
      FormatSQLNumber(FImageIndex)+','+
      FormatSQLString(FFormName)+','+
      FormatSQLNumber(FFormType)+','+
      FormatSQLNumber(FShortcut)+')'
      //FormatSQLNumber(FUrutan)+')'
  ); except raise; end;
end;

procedure _SystemMenu.UpdateOnDB;
begin
  try myConnection.ExecSQL(
    'UPDATE System_Menu SET'+
          ' Menu_Group='+FormatSQLString(FMenuGroup)+','+
          ' Menu_Name='+FormatSQLString(FMenuName)+','+
          ' Image_Index='+FormatSQLNumber(FImageIndex)+','+
          ' Form_Name='+FormatSQLString(FFormName)+','+
          ' Form_Type='+FormatSQLNumber(FFormType)+','+
          ' Shortcut='+FormatSQLNumber(FShortcut)+
//          ' Urutan='+FormatSQLNumber(FUrutan)+
    ' WHERE Menu_Id='+FormatSQLNumber(FMenuId)
  ); except raise; end;
end;

procedure _SystemMenu.DeleteOnDB;
begin
  try myConnection.ExecSQL(
    'DELETE FROM System_Menu'+
    ' WHERE Menu_Id='+FormatSQLNumber(FMenuId)
  ); except raise; end;
end;

function _SystemMenu.SelectInDB(AMenuId: integer): boolean;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT SM.Menu_Id, SM.Menu_Group, SM.Menu_Name, SM.Image_Index, SM.Form_Name, SM.Form_Type, SM.Shortcut,SM.Submenu_Name,SM.Dept_ID '+
     ' FROM System_Menu SM'+
    ' WHERE SM.Menu_Id='+FormatSQLNumber(AMenuId)
  );
  if (buffer.RecordCount<>0) then begin
    FMenuId := BufferToInteger(buffer.Fields[0].Value);
    FMenuGroup := BufferToString(buffer.Fields[1].Value);
    FMenuName := BufferToString(buffer.Fields[2].Value);
    FImageIndex := BufferToInteger(buffer.Fields[3].Value);
    FFormName := BufferToString(buffer.Fields[4].Value);
    FFormType := BufferToInteger(buffer.Fields[5].Value);
    FShortcut := BufferToInteger(buffer.Fields[6].Value);
    FSubMenuName := BufferToString(buffer.Fields[7].Value);
    FDeptID     := BufferToString(buffer.Fields[8].Value);
  end;
  Result := (buffer.RecordCount<>0);
  buffer.Close;
end;

class function _SystemMenu.ExistInDB(AMenuId: integer): integer;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT COUNT(*) FROM System_Menu'+
    ' WHERE Menu_Id='+FormatSQLNumber(AMenuId)
  );
  Result := BufferToInteger(buffer.Fields[0].Value);
  buffer.Close;
end;

{ _SystemAccess_Arr }

constructor _SystemAccess_Arr.Create;
begin
  inherited;
  SetLength(FSystemAccess_Arr,0);
end;

destructor _SystemAccess_Arr.Destroy;
begin
  Self.Clear;
  inherited;
end;

procedure _SystemAccess_Arr.Clear;
var i: integer;
begin
  for i:=0 to high(FSystemAccess_Arr) do FSystemAccess_Arr[i].Destroy;
  setLength(FSystemAccess_Arr,0);
end;

function _SystemAccess_Arr.New: _SystemAccess;
begin
  Result := _SystemAccess.Create;
end;

function _SystemAccess_Arr.Add(AUserId: string; AMenuId, AAccessList, AShortcut: integer; AOutlookPanel, AOutlookButton: string; AUsageCount: integer): integer;
begin
  setLength(FSystemAccess_Arr, length(FSystemAccess_Arr)+1);
  Result := high(FSystemAccess_Arr);
  FSystemAccess_Arr[Result] := New;
  FSystemAccess_Arr[Result].SystemUser.UserId := AUserId;
  FSystemAccess_Arr[Result].SystemMenu.MenuId := AMenuId;
  FSystemAccess_Arr[Result].AccessList := AAccessList;
  FSystemAccess_Arr[Result].OutlookPanel := AOutlookPanel;
  FSystemAccess_Arr[Result].OutlookButton := AOutlookButton;
  FSystemAccess_Arr[Result].UsageCount := AUsageCount;
end;

function _SystemAccess_Arr.IndexOf(AUserId: string; AMenuId: integer): integer;
var i: integer;
begin
  i := 0; Result := -1;
  while (i<=high(FSystemAccess_Arr)) and (Result=-1) do
    if (FSystemAccess_Arr[i].SystemUser.UserId=AUserId) and
      (FSystemAccess_Arr[i].SystemMenu.MenuId=AMenuId) then
      Result := i  //04.02.05
    else inc(i);
end;

procedure _SystemAccess_Arr.Delete(Index: integer);
var i: integer;
begin
  if (Index<0) or (Index>high(FSystemAccess_Arr)) then Exit;
  FSystemAccess_Arr[Index].Destroy;
  for i:=Index to high(FSystemAccess_Arr)-1 do
    FSystemAccess_Arr[Index] := FSystemAccess_Arr[Index-1];
  setLength(FSystemAccess_Arr,length(FSystemAccess_Arr)-1);
end;

function _SystemAccess_Arr.Count: integer;
begin
  Result := length(FSystemAccess_Arr);
end;

function _SystemAccess_Arr.Get(Index: integer): _SystemAccess;
begin
  if (Index<0) or (Index>high(FSystemAccess_Arr)) then Result := nil
  else Result := FSystemAccess_Arr[Index];
end;


{ _SystemAccess }

constructor _SystemAccess.Create;
begin
  inherited Create;
  FSystemUser := _SystemUser.Create;
  FSystemMenu := _SystemMenu.Create;
end;

destructor _SystemAccess.Destroy;
begin
  FSystemUser.Destroy;
  FSystemMenu.Destroy;
  inherited;
end;

procedure _SystemAccess.Reset;
begin
  FSystemUser.Reset;
  FSystemMenu.Reset;
  FAccessList := 0;
  FOutlookPanel := '';
  FOutlookButton := '';
  FUsageCount := 0;
end;

procedure _SystemAccess.InsertOnDB;
begin
  try myConnection.ExecSQL(
    'INSERT INTO System_Access (User_Id,Menu_Id,Access_List,Outlook_Panel,Outlook_Button,Usage_Count)'+
    ' VALUES ('+
      FormatSQLString(FSystemUser.UserId)+','+
      FormatSQLNumber(FSystemMenu.MenuId)+','+
      FormatSQLNumber(FAccessList)+','+
      FormatSQLString(FOutlookPanel)+','+
      FormatSQLString(FOutlookButton)+','+
      FormatSQLNumber(FUsageCount)+')'
  ); except raise; end;
end;

procedure _SystemAccess.UpdateOnDB;
begin
  try myConnection.ExecSQL(
    'UPDATE System_Access SET'+
          ' Access_List='+FormatSQLNumber(FAccessList)+','+
          ' Outlook_Panel='+FormatSQLString(FOutlookPanel)+','+
          ' Outlook_Button='+FormatSQLString(FOutlookButton)+','+
          ' Usage_Count='+FormatSQLNumber(FUsageCount)+
    ' WHERE User_Id='+FormatSQLString(FSystemUser.UserId)+
      ' AND Menu_Id='+FormatSQLNumber(FSystemMenu.MenuId)
  ); except raise; end;
end;

procedure _SystemAccess.DeleteOnDB;
begin
  try myConnection.ExecSQL(
    'DELETE FROM System_Access'+
    ' WHERE User_Id='+FormatSQLString(FSystemUser.UserId)+
      ' AND Menu_Id='+FormatSQLNumber(FSystemMenu.MenuId)
  ); except raise; end;
end;


{ TSystemConstant }
type stringArr = array of string;

function GetPrivateProfileStringFromDB(const Section, Name, Default: PChar; var BufOut: stringArr; TableName: string): integer;
var buffer: _RecordSet; i: integer;
begin
  if (Section<>nil) then begin
    if (Name<>nil) then begin
      if (Default<>nil) then begin
        //Read String
        buffer := myConnection.OpenSQL(
          'SELECT Nilai FROM '+TableName+
          ' WHERE Seksi='+FormatSQLString(Section)+' AND Nama='+FormatSQLString(Name)
        );
        setLength(BufOut,1);
        if (buffer.RecordCount=0) then BufOut[0] := Default
        else BufOut[0] := BufferToString(buffer.Fields[0].Value);
        Result := Length(BufOut[0]);
        buffer.Close;
      end else begin
        //Read Section
        buffer := myConnection.OpenSQL(
          'SELECT Nama, Nilai FROM '+TableName+
          ' WHERE Seksi='+FormatSQLString(string(Section))
        );
        setLength(BufOut,buffer.RecordCount);
        for i:=1 to buffer.RecordCount do begin
          BufOut[i-1] := BufferToString(buffer.Fields[0].Value)+'='+BufferToString(buffer.Fields[1].Value);
          buffer.MoveNext;
        end;
        Result := buffer.RecordCount;
        buffer.Close;
      end;
    end else begin
      //Read Section
      buffer := myConnection.OpenSQL(
        'SELECT Nama FROM '+TableName+
        ' WHERE Seksi='+FormatSQLString(string(Section))
      );
      setLength(BufOut,buffer.RecordCount);
      for i:=1 to buffer.RecordCount do begin
        BufOut[i-1] := BufferToString(buffer.Fields[0].Value);
        buffer.MoveNext;
      end;
      Result := buffer.RecordCount;
      buffer.Close;
    end;
  end else begin
    //Read Sections
    buffer := myConnection.OpenSQL(
      'SELECT DISTINCT(Seksi) FROM '+TableName
    );
    setLength(BufOut,buffer.RecordCount);
    for i:=1 to buffer.RecordCount do begin
      BufOut[i-1] := BufferToString(buffer.Fields[0].Value);
      buffer.MoveNext;
    end;
    Result := buffer.RecordCount;
    buffer.Close;
  end;
end;

{ TSystemUser }

class procedure TSystemUser.AccessLevelList(var ATarget: TStringList);
begin
  ATarget.Clear;
  ATarget.Add(IntToStr(LEVEL_OPERATOR) + '=Operator' );
//  ATarget.Add(IntToStr(LEVEL_SUPERVISOR) + '=Supervisor' );
  ATarget.Add(IntToStr(LEVEL_MANAGER) + '=Manager');
//  ATarget.Add(IntToStr(LEVEL_ADMIN) + '=Admin');
//  ATarget.Add(IntToStr(LEVEL_OWNER) + '=Direksi');
  ATarget.Add(IntToStr(LEVEL_DEVELOPER) + '=Developer');
end;

class function TSystemUser.IsExistInDB(userID: string): boolean;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT COUNT(*) FROM System_User'+
    ' WHERE User_ID = ' + FormatSQLString(userID)
  );
  Result := (BufferToInteger(buffer.Fields[0].Value)<>0);
  buffer.Close;
end;

class function TSystemUser.UserIDAlreadyRegistered(UserID: String): boolean;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT COUNT(*) FROM System_User WHERE User_ID = ' + FormatSQLString(UserID)
  );
  Result := (BufferToInteger(buffer.Fields[0].Value)<>0);
  buffer.Close;
end;

procedure TSystemUser.UpdatePasswordOnDB;
begin
  try myConnection.ExecSQL(
    'UPDATE System_User'+
      ' SET Passwd='+FormatSQLString(FPasswd)+
    ' WHERE User_Id='+FormatSQLString(FUserId)
  ); except raise; end;
end;

function TSystemUser.getDisable: boolean;
begin
  result := DisableDate <> 0;
end;

procedure TSystemUser.setDisable(const Value: boolean);
var sql: string;
begin
  sql:= 'UPDATE System_User Set Disabled_Date = '+IfThen(value,FormatSQLNULL2,FormatSQLDate(ServerNow))+
                       ' WHERE User_ID='+FormatSQLString(FUserId);
  myConnection.ExecSQL(sql);
end;

procedure TSystemUser.InsertOnDB;
begin
  try myConnection.ExecSQL(
    'INSERT INTO System_User (User_Id,User_Name,Passwd,Access_Level, dept_id )'+
    ' VALUES ('+
      FormatSQLString(FUserId)+','+
      FormatSQLString(FUserName)+','+
      FormatSQLString(FPasswd)+','+
      FormatSQLNumber(FAccessLevel)+', '+
      FormatSQLString(FDeptId)+')'
   ); except raise; end;
end;

procedure TSystemUser.UpdateOnDB;
var sql : string;
begin
  sql := 'UPDATE System_User SET'+
          ' User_Name='+FormatSQLString(FUserName)+','+
          ' Passwd='+FormatSQLString(FPasswd)+','+
          ' Access_Level='+FormatSQLNumber(FAccessLevel)+
    ' WHERE User_Id='+FormatSQLString(FUserId);
  try myConnection.ExecSQL(sql);
  except raise;
  end;
end;

procedure TSystemUser.Aktivasi_User(USER_ID: string; status: boolean);
var TglNonAktif : TDATE; 
begin
  tglNonAktif := 0;
  if status = True then  TglNonAktif := 0
  else if status = false then TglNonAktif := ServerNow;
  try myConnection.ExecSQL(
    'UPDATE System_User'+
      ' SET DISABLED_DATE ='+FormatSQLDate(TglNonAktif)+
    ' WHERE User_Id='+FormatSQLString(USER_ID)
  ); except raise;
  end;
end;


{ TSystemUser_Arr }

procedure TSystemUser_Arr.FindOnDB(LoginID, LoginName: string;
  AccessLevel: integer;status:boolean;Dept:string);
var sqlstring,filter:string;buffer:_RecordSet;i:integer;
begin
  sqlString := 'SELECT User_ID,User_Name,Passwd,Access_Level, Disabled_Date, DEPT_ID ' +
                ' FROM System_User ';
              // ' WHERE  '+
  filter := '';
  filter := IfThen(status,' AND Disabled_Date IS NULL ','');
  if LoginID <> '' then
    filter := filter + ' AND User_ID = ' + FormatSQLString(LoginID);
  if Dept <> '' then
    filter := filter + ' AND DEPT_ID = ' + FormatSQLString(Dept);


  if LoginName <> '' then
    filter := filter + ' AND User_Name LIKE ' + FormatSQLString('%' + LoginName + '%');
  if AccessLevel >= 0 then
    filter := filter + ' AND Access_Level < ' + FormatSQLNumber(AccessLevel);
  if filter <> '' then
    sqlstring := sqlstring +' WHERE '+ copy(filter,5,length(filter));
    sqlstring := sqlstring +' order by user_id ';

  buffer := myConnection.OpenSQL(sqlString);
  if buffer.RecordCount > 0 then begin
    for i := 0 to buffer.RecordCount-1 do begin
      add(bufferToString(buffer.Fields[0].value),bufferToString(buffer.Fields[1].value),
                        bufferToString(buffer.Fields[2].value),
                        bufferToInteger(buffer.Fields[3].value),
                        BufferToDateTime(buffer.Fields[4].value),
                        bufferToString(buffer.Fields[5].value));
      buffer.MoveNext;
    end;
  end
end;

function TSystemUser_Arr.Get(Index: integer): TSystemUser;
begin
  result := TSystemUser(inherited get(Index));
end;

function TSystemUser_Arr.New: _SystemUser;
begin
  result := TSystemUser.Create;
end;

{ TSystemMenu }

function TSystemMenu.GetFormType: TFormType;
begin
  Result := TFormType(FFormType);
end;

procedure TSystemMenu.SetFormType(const Value: TFormType);
begin
  FFormType := integer(Value);
end;

function TSystemMenu.GetShortcut: TShortcut;
begin
  Result := TShortcut(FShortcut);
end;

procedure TSystemMenu.SetShortcut(const Value: TShortcut);
begin
  FShortcut := Integer(Value);
end;

function TSystemMenu.GetFormTypeNew: Integer;
begin
  Result := FFormType;
end;

function TSystemMenu.getMenuPrefix: string;
begin
    result := '';
  case FormType of
    ftDetail:  Result := 'New ';//Form Type 0
    ftLookup:  Result := 'View ';//Form Type 1
    ftReview:  Result := 'New Trans ';//Form Type 2
    ftBrowse: Result := 'View Trans ';//Form Type 3
    ftReport: Result := 'Report ';
  end;
end;

{ TSystemMenu_Arr }

function TSystemMenu_Arr.New: _SystemMenu;
begin
  Result := TSystemMenu.Create;
end;

function TSystemMenu_Arr.Get(Index: integer): TSystemMenu;
begin
  Result := TSystemMenu(inherited Get(Index));
end;

procedure TSystemMenu_Arr.LoadFromDB(ADeptId: String; AccessLevel: integer; AUserId: string);
var buffer: _RecordSet; i: integer;
   sql : string;
begin
  Self.Clear;
  sql:=
    'SELECT Menu_Id, Menu_Group, Menu_Name, Image_Index, Form_Name, Form_Type, Shortcut, SubMenu_Name, Dept_Id '+
    'FROM System_Menu WHERE menu_id not in(436, 523) ';
    // ganti dzikri 221217
//    'FROM System_Menu ';//  WHERE menu_id not in(5212, 5409, 5412, 2102, 2104, 2011, 2012, 2013, 2108, 5406, 5410, 5411, 2017, 2018, 2019, 5508, 203 ,'+
//        '1001, 1002,1003,1004,1005,1006,1007) ';
    //di buka sama henra untuk load menu berdasarkan DEPT_ID
    if ADeptId <> '' then
        sql := sql + ' and (Dept_ID like '+FormatSQLString(aDeptID)+' or Dept_id IS NULL)';
//    if AUserId <> '' then
//        sql := sql + ' AND menu_id IN(SELECT menu_id FROM system_access WHERE user_id = '+formatsqlstring(AUserId)+')';

  buffer := myConnection.OpenSQL(sql+ ' ORDER BY Form_type,Menu_Group, SubMenu_Name, Menu_Id');
  for i:=0 to buffer.RecordCount-1 do begin
    Self.Add(
      BufferToInteger(buffer.Fields[0].Value),
      BufferToString(buffer.Fields[1].Value),
      BufferToString(buffer.Fields[2].Value),
      BufferToInteger(buffer.Fields[3].Value),
      BufferToString(buffer.Fields[4].Value),
      BufferToInteger(buffer.Fields[5].Value),
      BufferToInteger(buffer.Fields[6].Value),
      BufferToString(buffer.Fields[7].Value),
      BufferToString(buffer.Fields[8].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function TSystemMenu_Arr.GetByMenuID(AMenuID: integer): TSystemMenu;
var i: integer;
begin
  Result := nil;  i:=0;
  while (i<=high(FSystemMenu_Arr)) and (Result=nil) do begin
    if (FSystemMenu_Arr[i].MenuId=AMenuID) then
      Result := TSystemMenu(FSystemMenu_Arr[i]);
    inc(i);
  end;
end;

function TSystemMenu_Arr.FindInDB(menu_id: integer; menu_group,
  menu_name: string; image_index: integer; form_name: string;
  form_type, shortcut: integer): boolean;
var sql,filter:string;buffer:_RecordSet;i:integer;
begin
  result := false;
  sql := 'SELECT menu_id,menu_group,menu_name,image_index,form_name,form_type,shortcut,Submenu_name,Dept_ID '+
          ' FROM System_Menu where  menu_id not in(436, 523) ';
  filter := '';
  if menu_id > 0 then
    filter := filter + ' AND menu_id = '+FormatSQLNumber(menu_id);
  if menu_group <> '' then
    filter := filter + ' AND menu_group LIKE ' + FormatSQLString('%'+menu_group+'%');
  if menu_name <> '' then
    filter := filter + ' AND menu_name LIKE ' + FormatSQLString('%'+menu_name+'%');
  if image_index <> -1 then
    filter := filter + ' AND image_index = ' + FormatSQLNumber(image_index);
  if form_name <> '' then
    filter := filter + ' AND form_name LIKE ' + FormatSQLString('%'+form_name+'%');
  if form_type > 0 then
    filter := filter + ' AND form_type = ' + FormatSQLNumber(form_type);
  if shortcut > 0 then
    filter := filter + ' AND shortcut = ' + FormatSQLNumber(shortcut);
//  if filter <> '' then
//    filter := ' WHERE ' + copy(filter,5,length(filter));
  buffer := myConnection.OpenSQL(sql + filter);
  if buffer.RecordCount > 0 then begin
    for i:=0 to buffer.RecordCount-1 do begin
      Add(BufferToInteger(buffer.fields[0].value),
          BufferToString(buffer.fields[1].value),
          BufferToString(buffer.fields[2].value),
          BufferToInteger(buffer.fields[3].value),
          BufferToString(buffer.fields[4].value),
          BufferToInteger(buffer.fields[5].value),
          BufferToInteger(buffer.fields[6].value),
          BufferToString(buffer.Fields[7].Value),
          BufferToString(buffer.Fields[8].Value));
      buffer.MoveNext;
    end;
  end;
  buffer.close;
end;

procedure TSystemMenu_Arr.LoadFromDB_User(isfromKelolaUser : boolean = false);
var buffer: _RecordSet; i: integer;
   sql : string;
begin
  Self.Clear;
  sql:=
      'SELECT sm.Menu_Id, sm.Menu_Group, sm.Menu_Name, sm.Image_Index, sm.Form_Name, sm.Form_Type, sm.Shortcut, sm.SubMenu_Name, sm.Dept_Id '+
      'FROM System_Menu sm '+ ifthen(isfromKelolaUser=false,' where menu_id not in(436, 523) ');
  buffer := myConnection.OpenSQL(sql+ ' ORDER BY sm.form_type, sm.Menu_Id');
  for i:=0 to buffer.RecordCount-1 do begin
    Self.Add(
      BufferToInteger(buffer.Fields[0].Value),
      BufferToString(buffer.Fields[1].Value),
      BufferToString(buffer.Fields[2].Value),
      BufferToInteger(buffer.Fields[3].Value),
      BufferToString(buffer.Fields[4].Value),
      BufferToInteger(buffer.Fields[5].Value),
      BufferToInteger(buffer.Fields[6].Value),
      BufferToString(buffer.Fields[7].Value),
      BufferToString(buffer.Fields[8].Value));
    buffer.MoveNext;
  end;
  buffer.Close;

end;

procedure TSystemMenu_Arr.getMenuGroup(var menuArr: TSystemMenu_Arr;
  menu_group: string);
var i:integer;
begin
  menuArr.clear;
  for i:=0 to count-1 do begin
    if FSystemMenu_Arr[i].MenuGroup = menu_group then
      menuArr.Add(FSystemMenu_Arr[i].FMenuId,
                  FSystemMenu_Arr[i].FMenuGroup,
                  FSystemMenu_Arr[i].FMenuName,
                  FSystemMenu_Arr[i].FImageIndex,
                  FSystemMenu_Arr[i].FFormName,
                  FSystemMenu_Arr[i].FFormType,
                  FSystemMenu_Arr[i].FShortcut,
                  FSystemMenu_Arr[i].FSubMenuName,
                  FSystemMenu_arr[i].FDeptID);
  end;
end;

procedure TSystemMenu_Arr.getMenuGroupList(var aList: TStringList);
var buffer:_RecordSet;i:integer; X:TSystemMenu_Arr;
begin
  aList.Clear;
  buffer := myConnection.OpenSQL('SELECT Distinct Menu_Group FROM System_Menu');
  if buffer.RecordCount > 0 then begin
    for i:=0 to buffer.RecordCount-1 do begin
      X := TSystemMenu_Arr.Create;
      aList.Objects[aList.Add(BufferToString(Buffer.Fields[0].Value))] := X;
      Buffer.MoveNext;
    end;
  end;
  buffer.Close;
end;

{ TSystemAccess }

constructor TSystemAccess.Create(AOwner: TObject);
begin
  inherited Create;
  FSystemUser.Destroy;
  FSystemUser := TSystemUser.Create;
  FSystemMenu.Destroy;
  FSystemMenu := TSystemMenu.Create;

  FOwner := AOwner;
  if (FOwner is TSystemUser) then begin
    FSystemUser.Destroy;
    FSystemUser := (FOwner as TSystemUser);
  end else if (FOwner is TSystemMenu) then begin
    FSystemMenu.Destroy;
    FSystemMenu := (FOwner as TSystemMenu);
  end;
end;

destructor TSystemAccess.Destroy;
begin
  if (FOwner is TSystemUser) then
    FSystemUser := TSystemUser.Create
  else if (FOwner is TSystemMenu) then
    FSystemMenu := TSystemMenu.Create;
  inherited;
end;

procedure TSystemAccess.Reset;
begin
  if (FOwner is TSystemUser) then
    FSystemUser := TSystemUser.Create
  else if (FOwner is TSystemMenu) then
    FSystemMenu := TSystemMenu.Create;

  inherited;

  if (FOwner is TSystemUser) then begin
    FSystemUser.Destroy;
    FSystemUser := (FOwner as TSystemUser);
  end else if (FOwner is TSystemMenu) then begin
    FSystemMenu.Destroy;
    FSystemMenu := (FOwner as TSystemMenu);
  end;
end;

procedure TSystemAccess.IncUsageCount;
begin
  inc(FUsageCount);
end;

procedure TSystemAccess.UpdateUsageCountOnDB;
begin
  try myConnection.ExecSQL(
    'UPDATE System_Access'+
      ' SET Usage_Count='+FormatSQLNumber(FUsageCount)+
    ' WHERE User_ID='+FormatSQLString(SystemUser.UserID)+
      ' AND Menu_ID='+FormatSQLNumber(SystemMenu.MenuID)
  ); except raise; end;
end;

procedure TSystemAccess.UpdateOutlookOnDB;
begin
  try myConnection.ExecSQL(
    'UPDATE System_Access'+
      ' SET Outlook_Panel='+FormatSQLString(FOutlookPanel)+
         ', Outlook_Button='+FormatSQLString(FOutlookButton)+
    ' WHERE User_ID='+FormatSQLString(SystemUser.UserID)+
      ' AND Menu_ID='+FormatSQLNumber(SystemMenu.MenuID)
  ); except raise; end;
end;

function TSystemAccess.SelectInDB(AUserID: string; AMenuID: integer): boolean;
var buffer: _RecordSet;
begin
  buffer := myConnection.OpenSQL(
    'SELECT User_Id,Menu_Id,Access_List,Outlook_Panel,Outlook_Button,Usage_Count'+
     ' FROM System_Access'+
    ' WHERE User_Id='+FormatSQLString(AUserID)+
      ' AND Menu_Id='+FormatSQLNumber(AMenuID));
  Result := (buffer.RecordCount>0);
  if (Result) then begin
    if not (FOwner is TSystemUser) then
      FSystemUser.FUserId := BufferToString(buffer.Fields[0].Value);
    if not (FOwner is TSystemMenu) then
      FSystemMenu.FMenuId := BufferToInteger(buffer.Fields[1].Value);
    FAccessList := BufferToInteger(buffer.Fields[2].Value);
    FOutlookPanel := BufferToString(buffer.Fields[3].Value);
    FOutlookButton := BufferToString(buffer.Fields[4].Value);
    FUsageCount := BufferToInteger(buffer.Fields[5].Value);
  end;
  buffer.Close;
end;

function TSystemAccess.SelectInDB: boolean;
begin
  Result := Self.SelectInDB(FSystemUser.UserId,FSystemMenu.MenuId)
end;

function TSystemAccess.GetAccessList: TAccessListSet;
begin
  Result := [];
  if (FAccessList and  1 =  1) then Include(Result, alAksInput);
  if (FAccessList and  2 =  2) then Include(Result, alAksEdit);
  if (FAccessList and  4 =  4) then Include(Result,alAksHapus);
  if (FAccessList and  8 =  8) then Include(Result,alAksRekap);
  if (FAccessList and  16 =  16) then Include(Result,alAksReport);
  if (FAccessList and  32 =  32) then Include(Result,alAksCetak);
  if (FAccessList and  64 =  64) then Include(Result, alAksExport);
  if (FAccessList and  128 =  128) then Include(Result, alAksPosting);
//  if (FAccessList and  128 =  128) then Include(Result, alAksPosting);
  // + fahmi 300614, + acces realisasi untuk di pengeluaran biaya
//  if (FAccessList and  256 =  256) then Include(Result, alAksRealisasi);
//  Result := TAccessListSet(FAccessList);
end;

procedure TSystemAccess.SetAccessList(const Value: TAccessListSet);
begin
  FAccessList := 0;
  if (alAksInput in Value) then FAccessList := FAccessList or  1;
  if (alAksEdit in Value) then FAccessList := FAccessList or  2;
  if (alAksHapus in Value) then FAccessList := FAccessList or  4;
  if (alAksRekap in Value) then FAccessList := FAccessList or  8;
  if (alAksReport in Value) then FAccessList := FAccessList or  16;
  if (alAksCetak in Value) then FAccessList := FAccessList or 32;
  if (alAksExport in Value) then FAccessList := FAccessList or 64;
  if (alAksPosting in Value) then FAccessList := FAccessList or 128;
//  if (alAksPosting in Value) then FAccessList := FAccessList or 256;
  // + fahmi 300614, + acces realisasi untuk di pengeluaran biaya
//  if (alAksRealisasi in Value) then FAccessList := FAccessList or 256;
end;

function TSystemAccess.GetSystemMenu: TSystemMenu;
begin
  Result := TSystemMenu(FSystemMenu);
end;

function TSystemAccess.GetSystemUser: TSystemUser;
begin
  Result := TSystemUser(FSystemUser);
end;

{ TSystemAccess_Arr }

constructor TSystemAccess_Arr.Create(AOwner: TObject);
begin
  inherited Create;
  FOwner := AOwner;
  setlength(FIndexUsage_Arr,0);
end;

function TSystemAccess_Arr.New: _SystemAccess;
begin
  Result := TSystemAccess.Create(FOwner);
end;

function TSystemAccess_Arr.Get(Index: integer): TSystemAccess;
begin
  Result := TSystemAccess(inherited Get(Index));
end;

function TSystemAccess_Arr.Add(AUserId: string; AAccessList: TAccessListSet;
  AShortcut: TShortcut; AOutlookPanel, AOutlookButton: string; AUsageCount: integer): integer;
begin
  if not (FOwner is TSystemMenu) then
    raise EAbort.Create('Owner is not _SystemMenu')
  else
    Result := Self.Add(AUserId,(FOwner as _SystemMenu).FMenuId,AAccessList,AShortcut,AOutlookPanel,AOutlookButton,AUsageCount);
end;

function TSystemAccess_Arr.Add(AMenuId: integer; AAccessList: TAccessListSet;
  AShortcut: TShortcut; AOutlookPanel, AOutlookButton: string; AUsageCount: integer): integer;
begin
  if not (FOwner is TSystemUser) then
    raise EAbort.Create('Owner is not _SystemUser')
  else
    Result := Self.Add((FOwner as _SystemUser).FUserId,AMenuId,AAccessList,AShortcut,AOutlookPanel,AOutlookButton,AUsageCount);
end;

function TSystemAccess_Arr.Add(AUserId: string; AMenuId: integer; AAccessList: TAccessListSet;
  AShortcut: TShortcut; AOutlookPanel, AOutlookButton: string; AUsageCount: integer): integer;
begin
  setLength(FSystemAccess_Arr, length(FSystemAccess_Arr)+1);
  Result := high(FSystemAccess_Arr);
  FSystemAccess_Arr[Result] := TSystemAccess(Self.New);
  SystemAccess_Arr[Result].SystemUser.UserId := AUserId;
  SystemAccess_Arr[Result].SystemMenu.MenuId := AMenuId;
  SystemAccess_Arr[Result].AccessList := AAccessList;
  SystemAccess_Arr[Result].OutlookPanel := AOutlookPanel;
  SystemAccess_Arr[Result].OutlookButton := AOutlookButton;
  SystemAccess_Arr[Result].FUsageCount := AUsageCount;
end;

function TSystemAccess_Arr.IndexOf(AUserId: string; AMenuId: integer): integer;
begin
  if (FOwner is TSystemUser) then
    Result := Self.IndexOf(AMenuId)
  else if (FOwner is TSystemMenu) then
    Result := Self.IndexOf(AUserId)
  else
    Result := inherited IndexOf(AUserId,(FOwner as _SystemMenu).FMenuId);
end;

function TSystemAccess_Arr.IndexOf(AMenuId: integer): integer;
begin
  if not (FOwner is TSystemUser) then
    raise EAbort.Create('Owner is not _SystemUser')
  else
    Result := inherited IndexOf((FOwner as _SystemUser).FUserId,AMenuId);
end;

function TSystemAccess_Arr.IndexOf(AUserId: string): integer;
begin
  if not (FOwner is TSystemMenu) then
    raise EAbort.Create('Owner is not _SystemMenu')
  else
    Result := inherited IndexOf(AUserId,(FOwner as _SystemMenu).FMenuId);
end;

function TSystemAccess_Arr.LoadFromDB(AUserID: string; AMenuID: integer; ADeptId: String): integer;
var buffer: _RecordSet; i, idx: integer; sql,cond: string;
begin
  Self.Clear;

  cond := '';
  if ADeptId <> '' then
    cond := cond + ' AND ((SM.Dept_Id='+FormatSQLString(ADeptId)+') OR (SM.Dept_Id IS NULL))';
  if (AUserID<>'') then
    cond := cond + ' AND SA.User_Id='+FormatSQLString(AUserID);
//  if (AMenuID<>0) then
//    cond := cond + ' WHERE SA.User_Id=SM.User_Id AND SA.Menu_Id='+FormatSQLNumber(AMenuID);
  if (AUserID<>'') then
    cond := cond + ' AND SA.User_Id='+FormatSQLString(AUserID);
  if (AMenuID<>0) then
    cond := cond + ' AND SA.Menu_Id='+FormatSQLNumber(AMenuID);
                                                                           //Sm.Menu_Id not in (436) and
  sql := 'SELECT SA.User_Id, SA.Menu_Id, Access_List, Shortcut, Outlook_Panel, Outlook_Button, Usage_Count'+
         ', Menu_Group, Menu_Name, Image_Index, Form_Name, Form_Type '+
     ' FROM System_Access SA, System_Menu SM WHERE SA.Menu_Id=SM.Menu_Id '+ cond;
  buffer := myConnection.OpenSQL(sql+' order by SM.menu_id');
  Result := buffer.RecordCount;
  for i:=0 to buffer.RecordCount-1 do begin
    idx := Self.Add(
      BufferToString(buffer.Fields[0].Value),
      BufferToInteger(buffer.Fields[1].Value),
      BufferToInteger(buffer.Fields[2].Value),
      BufferToInteger(buffer.Fields[3].Value),
      BufferToString(buffer.Fields[4].Value),
      BufferToString(buffer.Fields[5].Value),
      BufferToInteger(buffer.Fields[6].Value));

    if (FOwner is TSystemUser) and Assigned(SystemAccess_Arr[idx].SystemMenu) then begin
      SystemAccess_Arr[idx].SystemMenu.FMenuGroup := BufferToString(buffer.Fields[7].Value);
      SystemAccess_Arr[idx].SystemMenu.FMenuName := BufferToString(buffer.Fields[8].Value);
      SystemAccess_Arr[idx].SystemMenu.FImageIndex := BufferToInteger(buffer.Fields[9].Value);
      SystemAccess_Arr[idx].SystemMenu.FFormName := BufferToString(buffer.Fields[10].Value);
      SystemAccess_Arr[idx].SystemMenu.FFormType := BufferToInteger(buffer.Fields[11].Value);
//      SystemAccess_Arr[idx].SystemMenu.FUrutan := BufferToInteger(buffer.Fields[12].Value);
    end;

    buffer.MoveNext;
  end;
  buffer.Close;
end;

function TSystemAccess_Arr.LoadFromDB: integer;
begin
  if (FOwner is TSystemUser) then
    Result := Self.LoadFromDB((FOwner as TSystemUser).FUserID,0)
  else if (FOwner is TSystemMenu) then
    Result := Self.LoadFromDB('',(FOwner as TSystemMenu).FMenuId)
  else
    raise EAbort.Create('Container has no parent. Use FindOnDB instead of LoadFromDB');
end;

procedure TSystemAccess_Arr.LoadFromDBNew(var buffer: _Recordset; AUserID: string; AMenuID: integer;
                                          FORMTYPE: integer; MenuGroup, SubMenu: string);
var sql,cond, cond2: string;
begin
  cond := '';
  cond2 := '';
  if (AUserID<>'') then
    cond := cond + ' AND SA.User_Id='+FormatSQLString(AUserID);
//  if (AMenuID<>0) then
//    cond := cond + ' WHERE SA.User_Id=SM.User_Id AND SA.Menu_Id='+FormatSQLNumber(AMenuID);
  if (AUserID<>'') then
    cond := cond + ' AND SA.User_Id='+FormatSQLString(AUserID);
  if (AMenuID<>0) then
    cond := cond + ' AND SA.Menu_Id='+FormatSQLNumber(AMenuID);
  if (FORMTYPE<>0) then
    cond2 := cond2 + 'and SM.form_type = '+FormatSQLNumber(FORMTYPE);
  if (MenuGroup<>'') then
    cond2 := cond2 + ' AND upper(SM.Menu_Group) ='+FormatSQLString(UpperCase(MenuGroup));
  if (SubMenu<>'') then
    cond2 := cond2 + ' AND upper(SM.SUBMENU_NAME) ='+FormatSQLString(UpperCase(SubMenu));
  cond := cond + cond2;//+' menu_id not in(436) ';
  if (UpperCase(AUserID) = 'ORION') or (UpperCase(AUserID) = 'SYSTEM') then begin
                      //       0                     1    2        3          4              5         6      7            8
    sql := 'SELECT '+FormatSQLString(AUserID)+', Menu_Id, 1, Menu_Group, Menu_Name, Image_Index, Form_Name, Form_Type '+
       ' FROM System_Menu SM '+ ifthen(cond2 <> '', ' where '+copy(cond2, 5, length(cond2)));
//         'where sm.menu_id not in(303, 304, 305, 306, 307, 308, 309)';
    buffer := myConnection.OpenSQL(sql+' order by Form_Type, menu_id desc');
  end else begin
            //       0             1            2           3          4              5      6            7          8
    sql := 'SELECT SA.User_Id, SA.Menu_Id, Access_List, Menu_Group, Menu_Name, Image_Index, Form_Name, Form_Type '+
       ' FROM System_Access SA, System_Menu SM WHERE SA.Menu_Id=SM.Menu_Id AND Access_List <> 0 '+ cond;
//         'and sm.menu_id not in(303, 304, 305, 306, 307, 308, 309)';;
    buffer := myConnection.OpenSQL(sql+' order by Form_Type, SM.menu_id desc');
  end;

end;


function TSystemAccess_Arr.LoadFromDB_All(AUserId: String; ADeptId: String): integer;
var buffer: _RecordSet; i, idx: integer; sql: string;
begin
  Self.Clear;

//  cond := '';
//  if ADeptId <> '' then
//    cond := cond + ' AND (SM.Dept_Id='+FormatSQLString(ADeptId)+') OR (SM.Dept_Id IS NULL))';
//MENU_ID                                                                                                                  NOT NULL                         NUMBER(5)
//MENU_GROUP                                                                                                                                                VARCHAR2(50)
//MENU_NAME                                                                                                                NOT NULL                         VARCHAR2(100)
//IMAGE_INDEX                                                                                                                                               NUMBER(5)
//FORM_NAME                                                                                                                NOT NULL                         VARCHAR2(100)
//FORM_TYPE                                                                                                                NOT NULL                         NUMBER(3)
//SHORTCUT                                                                                                                                                  NUMBER(5)
//DEPT_ID                                                                                                                                                   VARCHAR2(10)
//SUBMENU_NAME

  sql := 'SELECT '+FormatSQLString(AUserId)+', SM.Menu_Id, NULL, Shortcut, NULL, NULL, NULL, '+
         'Menu_Group, Menu_Name, Image_Index, Form_Name, Form_Type '+
     ' FROM System_Menu SM WHERE SM.Menu_Id NOT IN (436, 523) and  ((SM.Dept_Id='+FormatSQLString(ADeptId)+') OR (SM.Dept_Id IS NULL)) ';
  buffer := myConnection.OpenSQL(sql+' order by SM.menu_id');
  Result := buffer.RecordCount;
  for i:=0 to buffer.RecordCount-1 do begin
    idx := Self.Add(
      BufferToString(buffer.Fields[0].Value),
      BufferToInteger(buffer.Fields[1].Value),
      BufferToInteger(buffer.Fields[2].Value),
      BufferToInteger(buffer.Fields[3].Value),
      BufferToString(buffer.Fields[4].Value),
      BufferToString(buffer.Fields[5].Value),
      BufferToInteger(buffer.Fields[6].Value));

    if (FOwner is TSystemUser) and Assigned(SystemAccess_Arr[idx].SystemMenu) then begin
      SystemAccess_Arr[idx].SystemMenu.FMenuGroup := BufferToString(buffer.Fields[7].Value);
      SystemAccess_Arr[idx].SystemMenu.FMenuName := BufferToString(buffer.Fields[8].Value);
      SystemAccess_Arr[idx].SystemMenu.FImageIndex := BufferToInteger(buffer.Fields[9].Value);
      SystemAccess_Arr[idx].SystemMenu.FFormName := BufferToString(buffer.Fields[10].Value);
      SystemAccess_Arr[idx].SystemMenu.FFormType := BufferToInteger(buffer.Fields[11].Value);
//      SystemAccess_Arr[idx].SystemMenu.FUrutan := BufferToInteger(buffer.Fields[12].Value);
    end;

    buffer.MoveNext;
  end;
  buffer.Close;
end;

procedure TSystemAccess_Arr.RecalculateUsage;
var i, j, IndexMax, temp : integer;
begin
  setlength(FIndexUsage_Arr,Self.Count);
  for i := 0 to high(FIndexUsage_Arr) do FIndexUsage_Arr[i] := i;

  for i := 0 to high(FIndexUsage_Arr)-1 do begin
    IndexMax := i;
    for j := i + 1 to high(FIndexUsage_Arr) do begin
      if (SystemAccess_Arr[FIndexUsage_Arr[j]].UsageCount > SystemAccess_Arr[FIndexUsage_Arr[IndexMax]].UsageCount) then
        IndexMax := j;
    end;
    if not (IndexMax = i) then begin
      temp                       := FIndexUsage_Arr[i];
      FIndexUsage_Arr[i]         := FIndexUsage_Arr[IndexMax];
      FIndexUsage_Arr[IndexMax]  := temp;
    end;
  end;
end;

function TSystemAccess_Arr.GetTopUsage(Position: integer): TSystemAccess;
begin
  if (Position<0) or (Position>high(FIndexUsage_Arr)) then Result := nil
  else Result := SystemAccess_Arr[FIndexUsage_Arr[Position]];
end;

procedure TSystemAccess_Arr.SaveUsageCountOnDB;
var i: integer;
begin
  for i:=0 to high(FSystemAccess_Arr) do SystemAccess_Arr[i].UpdateUsageCountOnDB;
end;

procedure TSystemAccess_Arr.SaveOutlookOnDB;
var i: integer;
begin
  for i:=0 to high(FSystemAccess_Arr) do SystemAccess_Arr[i].UpdateOutlookOnDB;
end;

function TSystemAccess_Arr.GetByMenuID(AMenuID: integer): TSystemAccess;
var i: integer;
begin
  Result := nil;
  for i:=0 to Self.Count-1 do begin
    if (SystemAccess_Arr[i].SystemMenu.MenuId=AMenuId) then
      Result := SystemAccess_Arr[i];
  end;
end;

function TSystemAccess_Arr.GetByFormName(AFormName: string): TSystemAccess;
var i: integer;
begin
  Result := nil;
  for i:=0 to Self.Count-1 do begin
    if (UpperCase(SystemAccess_Arr[i].SystemMenu.FormName)=UpperCase(AFormName)) then
      Result := SystemAccess_Arr[i];
  end;
end;

procedure TSystemAccess_Arr.InsertOnDB;
var i:integer;
begin
  for i:=0 to Count-1 do FSystemAccess_Arr[i].InsertOnDB;
end;

procedure TSystemAccess_Arr.DeleteUpdateOnDB;
begin
  if (FOwner is TSystemUser) then begin
    myConnection.ExecSQL(
      'DELETE FROM System_access'+
      ' where user_id='+FormatSQLString(TSystemUser(FOwner).UserId));
    InsertOnDB;
  end;
end;

{ TClosing_Trans }

constructor TClosing_Trans.Create;
begin
  inherited Create;
end;

class function TClosing_Trans.GetMaxHari: Integer;
begin
  Result := getIntegerFromSQL('SELECT max_hari FROM Closing_transaksi');
end;

function TClosing_Trans.SetClosing: Boolean;
var
  vSql: String;
begin
  vSql := 'UPDATE Closing_transaksi SET '+
              'userid = '+FormatSQLString(GlobalSystemUser.UserId)+
              ', max_hari = '+FormatSQLNumber(MaxHari);
  try
    myConnection.BeginSQL;
    myConnection.ExecSQL(vSql);
    Result := True;
    myConnection.EndSQL;
    Inform('Tanggal transaksi boleh mundur '+IntToStrFmt(MaxHari));
  except
    myConnection.UndoSQL;
    Result := False;
    Inform(MSG_UNSUCCESS_UPDATE);
  end;

end;

class function TClosing_Trans.SetTanggalTransaksi(ATgl: TDate): TDate;
var sql:string; buffer:_Recordset;
    TanggalTrans : TDate;
    Nilai : Integer;
begin
  sql := 'SELECT max_hari FROM Closing_transaksi';
  buffer := myConnection.OpenSQL(sql);

  Nilai        := BufferToInteger(buffer.Fields[0].Value);
  TanggalTrans := ServerNow-Nilai;
  if ATgl <= TanggalTrans then begin
    Result := TanggalTrans;
    if ATgl < TanggalTrans then
      Inform('Tanggal transaksi idak boleh mundur lebih dari '+IntToStr(Nilai)+' hari');
  end else begin
    Result := ATgl;
  end;
  buffer.Close;
end;

//procedure TClosing_Trans.StartDateTransaksi;
//begin
//  MaxHari := getIntegerFromSQL('SELECT max_hari FROM Closing_transaksi');
//end;

initialization
  GlobalSystemMenu_Arr := TSystemMenu_Arr.Create;
  GlobalSystemUser := TSystemUser.Create;
  GlobalSystemUser.SystemAccess_Arr := TSystemAccess_Arr.Create(GlobalSystemUser);
  GlobalListDocType:= TStringList.Create;
  //AdvMoneyTipe := TEditLink.Create(AdvMoneyTipe);
  PeriodeTransaksi := TClosing_Trans.Create;
finalization
  GlobalSystemUser.SystemAccess_Arr.Destroy;
  GlobalSystemUser.Destroy;
  GlobalSystemMenu_Arr.Destroy;
  GlobalListDocType.Free;
  //AdvMoneyTipe.Destroy;
  PeriodeTransaksi.Destroy;
end.

