unit UEngine;

interface
uses OracleConnection, URecord, ADOInt, UGeneral, strutils, controls, Classes, UConst, sysUtils, DateUtils, Math, Gauges,
     UsystemMenu, AdvGrid;

function Is_Kode_Master_Exist(vKode, tab_name : string; vKey : integer = 0; tipe : string = '') : boolean;
function Is_Kode_Field_Exist(tab_name : string; Field : string; IsiField : string; vKey : integer = 0) : boolean;
function Aktivasi_Master(vKey : Integer; DisDate : TDate; tab_name : string;
  IsInform : Boolean = true; field : string = 'disable_date') : boolean;
function Get_Seq_By_KodeNama(KodeNama :string; isAktif : TBooleanOperator = boNone; TabName : string = ''; addPar : string = '') : integer;
function GetNextNumber(Tgl : TDate; TipeRef : String; seqCard : integer =0) : String; //yang baru
function GetNextNumberMaster(TipeMst : String; tipeCust: string = '') : String; //yang baru
function GetKetEditHapusTrans (Data : TR_KetEditHapus): String;
function Get_Seq_By_SparePartNama(KodeNama :string; isAktif : TBooleanOperator = boNone; TabName : string = ''; addPar : string = '') : integer;
procedure Get_list_field(Alist : TStringList; Table,Names,Value : String; AddPar : String = '');
function Get_Count(field, table, input : string; AddPar : string; tipeInput : string; IsDefaultWhere : Boolean = True) : integer;
function is_Field_exist(Field, Kode, TipeMstr : string; VKey : Integer; AddPar : string = '') : boolean;
procedure update_field_table(table, field, input, tipe_data : string;
  addpar:string='');
function get_next_rit_for_sj_armada(tanggal : TDate) : integer;
function is_no_os_exist(no_os : string; VKey : Integer; AddPar : string = '') :
boolean;



{master_card}
procedure Reset_Record_master_card (var Data : TR_master_card);
function Get_master_card (VKey : integer) : TR_master_card;
function Save_master_card (var R_master_card : TR_master_card) : integer;
procedure Delete_master_card (VKey : integer);
procedure Update_master_card (R_master_card : TR_master_card);
function Get_Arr_master_card (Cari : string = ''; addpar : string = '') : AR_master_card;
procedure Get_List_master_card (var Alist : TStringlist; ASeq : integer = 0);

{master_job}
procedure Reset_Record_master_job (var Data : TR_master_job);
function Get_master_job (VKey : integer) : TR_master_job;
function Save_master_job (var R_master_job : TR_master_job) : integer;
procedure Delete_master_job (VKey : integer);
procedure Update_master_job (R_master_job : TR_master_job; istglByr :boolean = false);
function Get_Arr_master_job (Cari : string = ''; card_seq : integer = 0; addpar : string = '') : AR_master_job;
procedure Get_List_master_job (var Alist : TStringlist; ASeq : integer = 0);

{master_bank}
procedure Reset_Record_master_bank (var Data : TR_master_bank);
function Get_master_bank (VKey : integer) : TR_master_bank;
function Save_master_bank (var R_master_bank : TR_master_bank) : integer;
procedure Delete_master_bank (VKey : integer);
procedure Update_master_bank (R_master_bank : TR_master_bank);
function Get_Arr_master_bank (Cari : string = ''; addpar : string = '') : AR_master_bank;
procedure Get_List_master_bank (var Alist : TStringlist; ASeq : integer = 0);

{company_info}
procedure Reset_Record_company_info (var Data : TR_company_info);
function Get_company_info : TR_company_info;
procedure Save_company_info (R_company_info : TR_company_info);
procedure Delete_company_info;
procedure Update_company_info (R_company_info : TR_company_info);
function Get_Arr_company_info ( addpar : string = '') : AR_company_info;
procedure Get_List_company_info (var Alist : TStringlist; ASeq : integer = 0);


{LOG_FILE}
function Get_LOG_FILE (VKey : integer) : TR_LOG_FILE;
function Save_LOG_FILE (R_LOG_FILE : TR_LOG_FILE) : integer;
procedure Delete_LOG_FILE (VKey : integer);
procedure Update_LOG_FILE (R_LOG_FILE : TR_LOG_FILE);
procedure UpdateSeqEditLogFile(SeqEdit, VKey : Integer);
function GetLastSeqLogFile : Integer;
function LoadLogFile(PerAwal, PerAkhir : TDate; UserID : string; IsEdit : TBooleanOperator =
  boNone) : AR_Log_File;


 
{master_ppn}
procedure Reset_Record_master_ppn (var Data : TR_master_ppn);
function Get_master_ppn (VKey : integer) : TR_master_ppn;
function Save_master_ppn (var R_master_ppn : TR_master_ppn) : integer;
procedure Delete_master_ppn (VKey : integer);
procedure Update_master_ppn (R_master_ppn : TR_master_ppn);
function Get_Arr_master_ppn (Cari : string = ''; addpar : string = '') : AR_master_ppn;
procedure Get_List_master_ppn (var Alist : TStringlist; ASeq : integer = 0);
function Get_nilai_ppn (tgl: tdate) : real;




implementation

uses
  EASendMailObjLib_TLB, EAGetMailObjLib_TLB,
  BaseGrid, Graphics, UFinance, oleacc,  Variants,
  UTransaksi, Loading, UConst_Temp;
var sqL : string;

function Is_Kode_Master_Exist(vKode, tab_name : string; vKey : integer = 0; tipe : string = '') : boolean;
begin
  sql := 'SELECT COUNT(*) FROM '+tab_name+' WHERE kode = '+FormatSQLString(vKode)+
         IfThen(vKey <> 0, ' AND seq <> '+FormatSQLNumber(vKey))+
         IfThen(tipe <> '',' AND tipe = '+FormatSQLString(tipe));
  Result := getIntegerFromSQL(sqL) > 0;
end;

function Is_Kode_Field_Exist(tab_name : string; Field : string; IsiField : string; vKey : integer = 0) : boolean;
begin
  sql := 'SELECT COUNT(*) FROM '+tab_name+' WHERE '+ Field +' = '+FormatSQLString(IsiField)+
         IfThen(vKey <> 0, ' AND seq <> '+FormatSQLNumber(vKey));
  Result := getIntegerFromSQL(sqL) > 0;
end;

function Aktivasi_Master(vKey : Integer; DisDate : TDate; tab_name : string; IsInform : Boolean = 
  true; field : string = 'disable_date') : boolean;
begin
  if DisDate = 0 then
    sql := 'UPDATE '+tab_name+' SET '+field+' = ' + FormatSQLDate(ServerNow) +
           ' WHERE Seq = ' + FormatSQLNumber(VKey)
  else sql := 'UPDATE '+tab_name+' SET '+field+' = ' + FormatSQLNULL2 +
              ' WHERE Seq = ' + FormatSQLNumber(VKey);
  Result := ExecAktivasi(sql, DisDate <> 0, IsInform);
end;

function Get_Seq_By_KodeNama(KodeNama :string; isAktif : TBooleanOperator = boNone; TabName : string = ''; addPar : string = '') : integer;
begin
  KodeNama := TrimAllString(UpperCase(KodeNama));
  if KodeNama <> '' then begin                          //
    sql := 'SELECT seq FROM '+TabName+' WHERE (upper(nama) = ' +FormatSQLString(UpperCase(
      KodeNama))+')';
    if isAktif <> boNone then begin
      if isAktif = boTrue then sql := sql + ' and TGL_HAPUS is null '
      else sql := sql + ' and TGL_HAPUS is not null ';
    end;
    if addPar <> '' then sql := sql + addPar;
    Result := getIntegerFromSQL(sqL);
  end else Result := 0;
end;

function GetNextNumber(Tgl : TDate; TipeRef : String; seqCard : integer=0) : String; //yang baru
  function GetNextNumberTrans(Tgl : TDate; TipeRef : String) : String;
  var fieldno, prefix, field, Head, kodeCust : String;
      no : Integer;
  begin
    fieldno := 'Nomor';
    if TipeRef = UConst.TRANS_PO then begin
      field := 'po_Master'; Head := UConst.TRANS_PO
    end else if TipeRef = UConst.TRANS_KWITANSI then begin
      field := 'invoice_master'; Head := 'KWT-JMG';//UConst.TRANS_INVOICE;
      fieldno := 'nomor_kwt';
    end else if TipeRef = UConst.TRANS_INVOICE then begin
      field := 'invoice_master'; Head := 'INV-JMG';//UConst.TRANS_INVOICE;
    end else if TipeRef = UConst.TRANS_BAUT then begin
      field := 'invoice_master'; Head := 'BAUT-JMG';//UConst.TRANS_INVOICE;
      fieldno:= 'nomor_baut';
    end else if TipeRef = UConst.TRANS_BAST then begin
      field := 'invoice_master'; Head := 'BAST-JMG';//UConst.TRANS_INVOICE;
      fieldno:= 'nomor_bast';
    end;

    if seqCard <> 0 then begin
      kodeCust := get_master_card(seqCard).kode_nomor;
    end;
//    if (TipeRef = TRANS_ORDER_ROMBONGAN) or (TipeRef = TRANS_WAITING_LIST) then begin
//      prefix := Head + FormatDateTime('MMYY',Tgl);
//      sql := 'SELECT MAX(NVL(TO_NUMBER((SUBSTR(Nomor,length(nomor)-3,4))),0)) FROM order_rombongan_master '+
//             ' WHERE (SUBSTR(Nomor,0,6)) = '+FormatSQLString(prefix);
//    end else begin
      prefix := Head +'-'+ FormatDateTime('MMYY',Tgl);
      sql := 'SELECT MAX(NVL(TO_NUMBER((SUBSTR('+fieldno+',0,3))),0)) FROM '+ field +
             ' WHERE tanggal between '+FormatSQLDate(StartOfTheMonth(Tgl))+' and '+FormatSQLDate(endOfTheMonth(Tgl));
//    end;
    no := getIntegerFromSQL(sql);
    no := no + 1;
    Result := NumberToString(no,3)+'/'+head+'/'+kodeCust+getPeriode(tgl, 1);
  end;
begin
  Result := GetNextNumberTrans(Tgl, TipeRef);
end;

function GetNextNumberMaster(TipeMst : String; tipeCust: string = '') : String; //yang baru
VAR Head, sql, field : string;
    no : integer;
begin
  if TipeMst = UConst.MASTER_CARD then begin
    field := 'MASTER_CARD';
  end else if TipeMst = UConst.MASTER_job then begin
    field := 'MASTER_job';
  end;

  sql := ' select count(seq) from '+field;
  sql := sql + ifthen(tipeCust<>'', ' where tipe = '+FormatSQLString(tipeCust));
  no := getIntegerFromSQL(sql);
  no := no + 1;
  Result := tipemst+tipeCust+NumberToString(no,4);
end;


function GetKetEditHapusTrans (Data : TR_KetEditHapus): String;
var Hasil : String;
begin
  Hasil := '';
  if Data.IsEdit then Hasil := Hasil + 'Edit trans '
  else Hasil := Hasil + 'Hapus trans ';
  if Data.NamaTrans <> '' then
    Hasil := Hasil + Data.NamaTrans + ' ';
  if Data.IsMasuk <> boNone then begin
    if Data.IsMasuk = boTrue then Hasil := Hasil + 'dari '
    else Hasil := Hasil + 'ke ';
  end;
  if Data.NamaVC <> '' then Hasil := Hasil + Data.NamaVC + ' ';
  if Data.NoTrans <> '' then Hasil := Hasil + 'dengan No. trans ' + Data.NoTrans + ' ';
  if Data.TglTrans <> 0 then Hasil := Hasil + 'tgl ' + DateToStr(Data.TglTrans) + ' ';
  Result := Hasil;
end;


function Get_Seq_By_SparePartNama(KodeNama :string; isAktif : TBooleanOperator = boNone; TabName : string = ''; addPar : string = '') : integer;
begin
  KodeNama := TrimAllString(UpperCase(KodeNama));
  if KodeNama <> '' then begin
    if UpperCase(TabName) = 'MASTER_SPARE_PART' then
      sql := 'SELECT seq FROM '+TabName+' WHERE ((upper(part_number) = '+FormatSQLString(UpperCase(KodeNama))+') OR '+
             '(upper(nama) = ' +FormatSQLString(UpperCase(KodeNama))+'))'
    else
      sql := 'SELECT seq FROM '+TabName+' WHERE ((upper(part_number) = '+FormatSQLString(UpperCase(KodeNama))+') OR '+
             '(upper(nama) = ' +FormatSQLString(UpperCase(KodeNama))+'))';
    if isAktif <> boNone then begin
      if isAktif = boTrue then sql := sql + ' and disable_date is null '
      else sql := sql + ' and disable_date is not null ';
    end;
    if addPar <> '' then sql := sql + addPar;
    Result := getIntegerFromSQL(sqL);
  end else Result := 0;
end;

procedure Get_list_field(Alist : TStringList; Table,Names,Value : String; AddPar : String = '');
begin
  sql := 'SELECT '+names+','+value+' FROM '+Table+AddPar;
  SQLToNameValueList(AList, sql);
end;

function Get_Count(field, table, input : string; AddPar : string; tipeInput : string; IsDefaultWhere : Boolean = True) : integer;
begin
  sql := 'select count(*) from '+table+ifthen(IsDefaultWhere = True,' where '+field+' = ');  //kalau isdefaulwhere = true, berarti otomatis dikasih where
  if tipeInput = 'date' then sql := sql + FormatSQLDate(StrToDate(input));
  if tipeInput = 'integer' then sql := sql + FormatSQLNumber(StrToInt(input));
  if tipeInput = 'string' then sql := sql + FormatSQLString(input);
  if tipeInput = 'float' then sql := sql + FormatSQLNumber(StrFmtToFloat(input));
  if AddPar <> '' then sql := sql + addpar;

  Result := GetIntegerFromSql(Sql);
end;

function is_Field_exist(Field, Kode, TipeMstr : string; VKey : Integer; AddPar : string = '') : boolean;
var TabName : String;
begin
  if TipeMstr = UConst.MASTER_CARD then TabName := 'MASTER_card'
  else if TipeMstr = UConst.MASTER_JOB then TabName := 'MASTER_job'
//  else if TipeMstr = UConst.TRANS_ABSENSI_PEGAWAI then TabName := 'ABSEN_MASTER';
  else  TabName := TipeMstr;
  sql := 'select count(*) from '+TabName+' where '+Field+' = '+FormatSQLString(Kode)+' and seq <> '+FormatSQLNumber(VKey) + AddPar;
  Result := GetIntegerFromSql(Sql) > 0;
end;


function is_no_os_exist(no_os : string; VKey : Integer; AddPar : string = '') : boolean;
begin
  sql := 'select count(*) from  order_rombongan_detail where nomor_os = '+FormatSQLString(no_os)+AddPar+
  ifthen(VKey <> 0,' and master_seq <> '+FormatSQLNumber(VKey));
  Result := GetIntegerFromSql(Sql) > 0;
end;


procedure update_field_table(table, field, input, tipe_data : string; addpar:string='');
begin
  sql := 'update '+table+' set '+field+' = ';
  if tipe_data = 'date' then sql := sql + FormatSQLDate(StrToDate(input));
  if tipe_data = 'integer' then sql := sql + FormatSQLNumber(StrToInt(input));
  if tipe_data = 'string' then sql := sql + FormatSQLString(input);
  if tipe_data = 'float' then sql := sql + FormatSQLNumber(StrFmtToFloat(input));
  if AddPar <> '' then sql := sql + addpar;

  ExecTransaction3(sql);
end;

function get_next_rit_for_sj_armada(tanggal : TDate) : integer;
begin
  sql := 'select max(rit) from sj_armada_master where tanggal = '+FormatSQLDate(tanggal);
  Result := getIntegerFromSQL(sql)+1;
end;


{master_card}
procedure Reset_Record_master_card (var Data : TR_master_card);
begin
  Data.seq := 0;
  Data.kode := '';
  Data.nama := '';
  Data.kode_nomor := '';
  Data.unit_bisnis := '';
  Data.tipe := '';
  Data.jenis := '';
  Data.alamat := '';
  Data.kota := '';
  Data.provinsi := '';
  Data.negara := '';
  Data.kodepos := '';
  Data.no_telp1 := '';
  Data.no_telp2 := '';
  Data.no_fax := '';
  Data.email := '';
  Data.website := '';
  Data.kontak := '';
  Data.png_jwb1 := '';
  Data.jabatan1 := '';
  Data.png_jwb2 := '';
  Data.jabatan2 := '';
  Data.user_id := '';
  Data.tgl_input := 0;
  Data.keterangan := '';
  Data.tgl_hapus := 0;
  Data.isBautBast := '';
end;
 
function Get_master_card (VKey : integer) : TR_master_card;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, kode, nama, kode_nomor, unit_bisnis, tipe, jenis, alamat, kota, provinsi, negara, kodepos, no_telp1, no_telp2, no_fax, email, website, kontak, png_jwb1, jabatan1, png_jwb2, jabatan2,'+
          ' user_id, tgl_input, keterangan, tgl_hapus, isBautBast FROM master_card '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin 
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.kode := BufferToString(buffer.Fields[1].Value);
    Result.nama := BufferToString(buffer.Fields[2].Value);
    Result.kode_nomor := BufferToString(buffer.Fields[3].Value);
    Result.unit_bisnis := BufferToString(buffer.Fields[4].Value);
    Result.tipe := BufferToString(buffer.Fields[5].Value);
    Result.jenis := BufferToString(buffer.Fields[6].Value);
    Result.alamat := BufferToString(buffer.Fields[7].Value);
    Result.kota := BufferToString(buffer.Fields[8].Value);
    Result.provinsi := BufferToString(buffer.Fields[9].Value);
    Result.negara := BufferToString(buffer.Fields[10].Value);
    Result.kodepos := BufferToString(buffer.Fields[11].Value);
    Result.no_telp1 := BufferToString(buffer.Fields[12].Value);
    Result.no_telp2 := BufferToString(buffer.Fields[13].Value);
    Result.no_fax := BufferToString(buffer.Fields[14].Value);
    Result.email := BufferToString(buffer.Fields[15].Value);
    Result.website := BufferToString(buffer.Fields[16].Value);
    Result.kontak := BufferToString(buffer.Fields[17].Value);
    Result.png_jwb1 := BufferToString(buffer.Fields[18].Value);
    Result.jabatan1 := BufferToString(buffer.Fields[19].Value);
    Result.png_jwb2 := BufferToString(buffer.Fields[20].Value);
    Result.jabatan2 := BufferToString(buffer.Fields[21].Value);
    Result.user_id := BufferToString(buffer.Fields[22].Value);
    Result.tgl_input := BufferToDateTime(buffer.Fields[23].Value);
    Result.keterangan := BufferToString(buffer.Fields[24].Value);
    Result.tgl_hapus := BufferToDateTime(buffer.Fields[25].Value); 
    Result.isBautBast := BufferToString(buffer.Fields[26].Value);
  end;
  buffer.Close;
end;
 
function Save_master_card (var R_master_card : TR_master_card) : integer;
begin 
    R_master_card.seq := CreateNewSeq('seq','master_card');
    sql := 'INSERT INTO master_card (seq, kode, nama, kode_nomor, unit_bisnis, tipe, jenis, alamat, kota, provinsi, negara, kodepos, no_telp1, no_telp2, no_fax, email, website, kontak, png_jwb1, jabatan1,'+
           ' png_jwb2, jabatan2, user_id, tgl_input, keterangan, tgl_hapus, isBautBast)  '+
         'VALUES ('+FormatSQLNumber(R_master_card.seq)+', '+
                    FormatSQLString(R_master_card.kode)+', '+
                    FormatSQLString(R_master_card.nama)+', '+
                    FormatSQLString(R_master_card.kode_nomor)+', '+
                    FormatSQLString(R_master_card.unit_bisnis)+', '+
                    FormatSQLString(R_master_card.tipe)+', '+
                    FormatSQLString(R_master_card.jenis)+', '+
                    FormatSQLString(R_master_card.alamat)+', '+
                    FormatSQLString(R_master_card.kota)+', '+
                    FormatSQLString(R_master_card.provinsi)+', '+
                    FormatSQLString(R_master_card.negara)+', '+
                    FormatSQLString(R_master_card.kodepos)+', '+
                    FormatSQLString(R_master_card.no_telp1)+', '+
                    FormatSQLString(R_master_card.no_telp2)+', '+
                    FormatSQLString(R_master_card.no_fax)+', '+
                    FormatSQLString(R_master_card.email)+', '+
                    FormatSQLString(R_master_card.website)+', '+
                    FormatSQLString(R_master_card.kontak)+', '+
                    FormatSQLString(R_master_card.png_jwb1)+', '+
                    FormatSQLString(R_master_card.jabatan1)+', '+
                    FormatSQLString(R_master_card.png_jwb2)+', '+
                    FormatSQLString(R_master_card.jabatan2)+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLDateTimeNow +', '+
                    FormatSQLString(R_master_card.keterangan)+', '+
                    FormatSQLNull2+', '+
                    FormatSQLString(R_master_card.isBautBast)+')';
  ExecTransaction3(sql);
    Result := R_master_card.seq; 
end;
 
procedure Delete_master_card (VKey : integer);
begin
  sql := 'DELETE FROM master_card '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;
 
procedure Update_master_card (R_master_card : TR_master_card);
begin
  sql := 'UPDATE master_card SET '+
                'kode = '+FormatSQLString(R_master_card.kode)+', '+
                'nama = '+FormatSQLString(R_master_card.nama)+', '+
                'kode_nomor = '+FormatSQLString(R_master_card.kode_nomor)+', '+
                'unit_bisnis = '+FormatSQLString(R_master_card.unit_bisnis)+', '+
                'tipe = '+FormatSQLString(R_master_card.tipe)+', '+
                'jenis = '+FormatSQLString(R_master_card.jenis)+', '+
                'alamat = '+FormatSQLString(R_master_card.alamat)+', '+
                'kota = '+FormatSQLString(R_master_card.kota)+', '+
                'provinsi = '+FormatSQLString(R_master_card.provinsi)+', '+
                'negara = '+FormatSQLString(R_master_card.negara)+', '+
                'kodepos = '+FormatSQLString(R_master_card.kodepos)+', '+
                'no_telp1 = '+FormatSQLString(R_master_card.no_telp1)+', '+
                'no_telp2 = '+FormatSQLString(R_master_card.no_telp2)+', '+
                'no_fax = '+FormatSQLString(R_master_card.no_fax)+', '+
                'email = '+FormatSQLString(R_master_card.email)+', '+
                'website = '+FormatSQLString(R_master_card.website)+', '+
                'kontak = '+FormatSQLString(R_master_card.kontak)+', '+
                'png_jwb1 = '+FormatSQLString(R_master_card.png_jwb1)+', '+
                'jabatan1 = '+FormatSQLString(R_master_card.jabatan1)+', '+
                'png_jwb2 = '+FormatSQLString(R_master_card.png_jwb2)+', '+
                'jabatan2 = '+FormatSQLString(R_master_card.jabatan2)+', '+
                'isBautBast = '+FormatSQLString(R_master_card.isBautBast)+', '+
                'keterangan = '+FormatSQLString(R_master_card.keterangan)+' '+
         'WHERE seq = '+FormatSQLNumber(R_master_card.seq);
 ExecTransaction3(sql);
end;
 
function Get_Arr_master_card (Cari : string = ''; addpar : string = '') : AR_master_card;
var buffer : _Recordset;
    i : integer;
    filter : string;
begin
  filter := '';
  if cari <> '' then 
    Filter := Filter + ' AND ((upper(kode) LIKE '+FormatSQLString('%'+UpperCase(cari)+'%')+') OR '+ 
    ' (upper(nama) LIKE ' +FormatSQLString('%'+UpperCase(cari)+'%')+'))';
  filter := filter + addpar;
  sql := 'SELECT seq, kode, nama, kode_nomor, unit_bisnis, tipe, jenis, alamat, kota, provinsi, negara, kodepos, no_telp1, no_telp2, no_fax, email, website, kontak, png_jwb1, jabatan1, png_jwb2, jabatan2,'+
          ' user_id, tgl_input, keterangan, tgl_hapus, isBautBast FROM master_card '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin 
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].kode := BufferToString(buffer.Fields[1].Value);
    Result[i].nama := BufferToString(buffer.Fields[2].Value);
    Result[i].kode_nomor := BufferToString(buffer.Fields[3].Value);
    Result[i].unit_bisnis := BufferToString(buffer.Fields[4].Value);
    Result[i].tipe := BufferToString(buffer.Fields[5].Value);
    Result[i].jenis := BufferToString(buffer.Fields[6].Value);
    Result[i].alamat := BufferToString(buffer.Fields[7].Value);
    Result[i].kota := BufferToString(buffer.Fields[8].Value);
    Result[i].provinsi := BufferToString(buffer.Fields[9].Value);
    Result[i].negara := BufferToString(buffer.Fields[10].Value);
    Result[i].kodepos := BufferToString(buffer.Fields[11].Value);
    Result[i].no_telp1 := BufferToString(buffer.Fields[12].Value);
    Result[i].no_telp2 := BufferToString(buffer.Fields[13].Value);
    Result[i].no_fax := BufferToString(buffer.Fields[14].Value);
    Result[i].email := BufferToString(buffer.Fields[15].Value);
    Result[i].website := BufferToString(buffer.Fields[16].Value);
    Result[i].kontak := BufferToString(buffer.Fields[17].Value);
    Result[i].png_jwb1 := BufferToString(buffer.Fields[18].Value);
    Result[i].jabatan1 := BufferToString(buffer.Fields[19].Value);
    Result[i].png_jwb2 := BufferToString(buffer.Fields[20].Value);
    Result[i].jabatan2 := BufferToString(buffer.Fields[21].Value);
    Result[i].user_id := BufferToString(buffer.Fields[22].Value);
    Result[i].tgl_input := BufferToDateTime(buffer.Fields[23].Value);
    Result[i].keterangan := BufferToString(buffer.Fields[24].Value);
    Result[i].tgl_hapus := BufferToDateTime(buffer.Fields[25].Value);
    Result[i].isBautBast := BufferToString(buffer.Fields[26].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;
procedure Get_List_master_card (var Alist : TStringlist; ASeq : integer = 0);
var buffer : _Recordset;
    filter : string;
    i : integer;
begin
  filter := '';
  if ASeq <> 0 then begin 
    if Alist.Values[IntToStr(ASeq)] <> '' then begin
      Alist.Delete(Alist.IndexOfName(IntToStr(ASeq))); 
    end; 
    filter := filter + ' and seq = ' + FormatSQLNumber(ASeq) 
  end else Alist.Clear;
  sql := 'SELECT seq, kode, nama, kode_nomor, unit_bisnis, tipe, jenis, alamat, kota, provinsi, negara, kodepos, no_telp1, no_telp2, no_fax, email, website, kontak, png_jwb1, jabatan1, png_jwb2, jabatan2,'+
          ' user_id, tgl_input, keterangan, tgl_hapus, isBautBast FROM master_card '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount -1 do begin 
    AList.add(BufferToString(buffer.Fields[0].Value)+'='+// seq
              BufferToString(buffer.Fields[1].Value)+'#'+// kode
              BufferToString(buffer.Fields[2].Value)+'#'+// nama
              BufferToString(buffer.Fields[3].Value)+'#'+// kode_nomor
              BufferToString(buffer.Fields[4].Value)+'#'+// unit_bisnis
              BufferToString(buffer.Fields[5].Value)+'#'+// tipe
              BufferToString(buffer.Fields[6].Value)+'#'+// jenis
              BufferToString(buffer.Fields[7].Value)+'#'+// alamat
              BufferToString(buffer.Fields[8].Value)+'#'+// kota
              BufferToString(buffer.Fields[9].Value)+'#'+// provinsi
              BufferToString(buffer.Fields[10].Value)+'#'+// negara
              BufferToString(buffer.Fields[11].Value)+'#'+// kodepos
              BufferToString(buffer.Fields[12].Value)+'#'+// no_telp1
              BufferToString(buffer.Fields[13].Value)+'#'+// no_telp2
              BufferToString(buffer.Fields[14].Value)+'#'+// no_fax
              BufferToString(buffer.Fields[15].Value)+'#'+// email
              BufferToString(buffer.Fields[16].Value)+'#'+// website
              BufferToString(buffer.Fields[17].Value)+'#'+// kontak
              BufferToString(buffer.Fields[18].Value)+'#'+// png_jwb1
              BufferToString(buffer.Fields[19].Value)+'#'+// jabatan1
              BufferToString(buffer.Fields[20].Value)+'#'+// png_jwb2
              BufferToString(buffer.Fields[21].Value)+'#'+// jabatan2
              BufferToString(buffer.Fields[22].Value)+'#'+// user_id
              BufferToString(buffer.Fields[23].Value)+'#'+// tgl_input
              BufferToString(buffer.Fields[24].Value)+'#'+// keterangan
              BufferToString(buffer.Fields[25].Value)+'#'+// tgl_hapus
              BufferToString(buffer.Fields[26].Value));// isBautBast

    buffer.MoveNext;
  end;
  buffer.Close;
end;
 
 
 
{master_job}
procedure Reset_Record_master_job (var Data : TR_master_job);
begin
  Data.seq := 0;
  Data.kode := '';
  Data.nama := '';
  Data.unit_bisnis := '';
  Data.lokasi := '';
  Data.card_seq := 0;
  Data.deskripsi := '';
  Data.contact := '';
  Data.tgl_mulai := 0;
  Data.tgl_target := 0;
  Data.user_id := '';
  Data.tgl_input := 0;
  Data.tgl_hapus := 0;
  Data.no_po    := '';
  data.tgl_Bayar := 0;
end;

function Get_master_job (VKey : integer) : TR_master_job;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, kode, nama, unit_bisnis, lokasi, card_seq, deskripsi, contact, tgl_mulai, ' +
    'tgl_target, user_id, tgl_input, tgl_hapus, no_po, tgl_Bayar FROM master_job '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.kode := BufferToString(buffer.Fields[1].Value);
    Result.nama := BufferToString(buffer.Fields[2].Value);
    Result.unit_bisnis := BufferToString(buffer.Fields[3].Value);
    Result.lokasi := BufferToString(buffer.Fields[4].Value);
    Result.card_seq := BufferToInteger(buffer.Fields[5].Value);
    Result.deskripsi := BufferToString(buffer.Fields[6].Value);
    Result.contact := BufferToString(buffer.Fields[7].Value);
    Result.tgl_mulai := BufferToDateTime(buffer.Fields[8].Value);
    Result.tgl_target := BufferToDateTime(buffer.Fields[9].Value);
    Result.user_id := BufferToString(buffer.Fields[10].Value);
    Result.tgl_input := BufferToDateTime(buffer.Fields[11].Value);
    Result.tgl_hapus := BufferToDateTime(buffer.Fields[12].Value);
    Result.no_po := BufferToString(buffer.Fields[13].Value);
    Result.tgl_Bayar := BufferToDateTime(buffer.Fields[14].Value);
  end;
  buffer.Close;
end;

function Save_master_job (var R_master_job : TR_master_job) : integer;
begin
    R_master_job.seq := CreateNewSeq('seq','master_job');
    sql := 'INSERT INTO master_job (seq, kode, nama, unit_bisnis, lokasi, card_seq, deskripsi, ' +
      'contact, tgl_mulai, tgl_target, user_id, tgl_input, tgl_hapus, no_po)  '+
         'VALUES ('+FormatSQLNumber(R_master_job.seq)+', '+
                    FormatSQLString(R_master_job.kode)+', '+
                    FormatSQLString(R_master_job.nama)+', '+
                    FormatSQLString(R_master_job.unit_bisnis)+', '+
                    FormatSQLString(R_master_job.lokasi)+', '+
                    FormatSQLNumber(R_master_job.card_seq)+', '+
                    FormatSQLString(R_master_job.deskripsi)+', '+
                    FormatSQLString(R_master_job.contact)+', '+
                    FormatSQLDate(R_master_job.tgl_mulai)+', '+
                    FormatSQLDate(R_master_job.tgl_target)+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLDateTimeNow +', '+
                    FormatSQLnull2+', '+
                    FormatSQLstring(R_master_job.no_po)+')';
  ExecTransaction3(sql);
  Result := R_master_job.seq;
end;

procedure Delete_master_job (VKey : integer);
begin
  sql := 'DELETE FROM master_job '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;
 
procedure Update_master_job (R_master_job : TR_master_job; istglByr :boolean = false);
begin
  if not istglByr then begin
    sql := 'UPDATE master_job SET '+
                  'kode = '+FormatSQLString(R_master_job.kode)+', '+
                  'nama = '+FormatSQLString(R_master_job.nama)+', '+
                  'no_po = '+FormatSQLString(R_master_job.no_po)+', '+
                  'unit_bisnis = '+FormatSQLString(R_master_job.unit_bisnis)+', '+
                  'lokasi = '+FormatSQLString(R_master_job.lokasi)+', '+
                  'card_seq = '+FormatSQLNumber(R_master_job.card_seq)+', '+
                  'deskripsi = '+FormatSQLString(R_master_job.deskripsi)+', '+
                  'contact = '+FormatSQLString(R_master_job.contact)+', '+
                  'tgl_mulai = '+FormatSQLDate(R_master_job.tgl_mulai)+', '+
                  'tgl_target = '+FormatSQLDate(R_master_job.tgl_target)+' '+
           'WHERE seq = '+FormatSQLNumber(R_master_job.seq);
  end else begin
    sql := 'UPDATE master_job SET '+
                  'tgl_bayar = '+ifthen(R_master_job.tgl_bayar=0,FormatSQLNULL2,FormatSQLDate(R_master_job.tgl_bayar))+' '+
           'WHERE seq = '+FormatSQLNumber(R_master_job.seq);         
  end;
  ExecTransaction3(sql);
end;
 
function Get_Arr_master_job (Cari : string = ''; card_seq : integer = 0; addpar : string = '') : AR_master_job;
var buffer : _Recordset;
    i : integer;
    filter : string;
begin
  filter := '';
  if cari <> '' then 
    Filter := Filter + ' AND ((upper(kode) LIKE '+FormatSQLString('%'+UpperCase(cari)+'%')+') OR '+ 
    ' (upper(nama) LIKE ' +FormatSQLString('%'+UpperCase(cari)+'%')+'))';
  if card_seq <> 0 then filter := filter + ' and card_seq = ' + FormatSqlNumber(card_seq);
  filter := filter + addpar;
  sql := 'SELECT seq, kode, nama, unit_bisnis, lokasi, card_seq, deskripsi, contact, tgl_mulai, '+
  'tgl_target, user_id, tgl_input, tgl_hapus, no_po, tgl_Bayar FROM master_job '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].kode := BufferToString(buffer.Fields[1].Value);
    Result[i].nama := BufferToString(buffer.Fields[2].Value);
    Result[i].unit_bisnis := BufferToString(buffer.Fields[3].Value);
    Result[i].lokasi := BufferToString(buffer.Fields[4].Value);
    Result[i].card_seq := BufferToInteger(buffer.Fields[5].Value);
    Result[i].deskripsi := BufferToString(buffer.Fields[6].Value);
    Result[i].contact := BufferToString(buffer.Fields[7].Value);
    Result[i].tgl_mulai := BufferToDateTime(buffer.Fields[8].Value);
    Result[i].tgl_target := BufferToDateTime(buffer.Fields[9].Value);
    Result[i].user_id := BufferToString(buffer.Fields[10].Value);
    Result[i].tgl_input := BufferToDateTime(buffer.Fields[11].Value);
    Result[i].tgl_hapus := BufferToDateTime(buffer.Fields[12].Value);
    Result[i].no_po := BufferToString(buffer.Fields[13].Value);
    Result[i].tgl_bayar := BufferToDateTime(buffer.Fields[14].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;
procedure Get_List_master_job (var Alist : TStringlist; ASeq : integer = 0);
var buffer : _Recordset;
    filter : string;
    i : integer;
begin
  filter := '';
  if ASeq <> 0 then begin 
    if Alist.Values[IntToStr(ASeq)] <> '' then begin
      Alist.Delete(Alist.IndexOfName(IntToStr(ASeq))); 
    end; 
    filter := filter + ' and seq = ' + FormatSQLNumber(ASeq) 
  end else Alist.Clear; 
  sql := 'SELECT seq, kode, nama, unit_bisnis, lokasi, card_seq, deskripsi, contact, tgl_mulai, ' +
    'tgl_target, user_id, tgl_input, tgl_hapus, no_po, tgl_bayar FROM master_job '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount -1 do begin
    AList.add(BufferToString(buffer.Fields[0].Value)+'='+// seq
              BufferToString(buffer.Fields[1].Value)+'#'+// kode
              BufferToString(buffer.Fields[2].Value)+'#'+// nama
              BufferToString(buffer.Fields[3].Value)+'#'+// unit_bisnis
              BufferToString(buffer.Fields[4].Value)+'#'+// lokasi
              BufferToString(buffer.Fields[5].Value)+'#'+// card_seq
              BufferToString(buffer.Fields[6].Value)+'#'+// deskripsi
              BufferToString(buffer.Fields[7].Value)+'#'+// contact
              BufferToString(buffer.Fields[8].Value)+'#'+// tgl_mulai
              BufferToString(buffer.Fields[9].Value)+'#'+// tgl_target
              BufferToString(buffer.Fields[10].Value)+'#'+// user_id
              BufferToString(buffer.Fields[11].Value)+'#'+// tgl_input
              BufferToString(buffer.Fields[12].Value)+'#'+// tgl_hapus
              BufferToString(buffer.Fields[13].Value)+'#'+// no_po
              BufferToString(buffer.Fields[14].Value));// //tgl_bayar
    buffer.MoveNext;
  end;
  buffer.Close;
end;

 
 
{master_bank}
procedure Reset_Record_master_bank (var Data : TR_master_bank);
begin
  Data.seq := 0;
  Data.nama := '';
  Data.cabang:= '';
  Data.norek := '';
  Data.an := '';
  Data.user_id := '';
  Data.tgl_input := 0;
  Data.tgl_hapus := 0;
end;

function Get_master_bank (VKey : integer) : TR_master_bank;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, nama, cabang, norek, an, user_id, tgl_input, tgl_hapus FROM master_bank '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.nama := BufferToString(buffer.Fields[1].Value);
    Result.cabang := BufferToString(buffer.Fields[2].Value);
    Result.norek := BufferToString(buffer.Fields[3].Value);
    Result.an := BufferToString(buffer.Fields[4].Value);
    Result.user_id := BufferToString(buffer.Fields[5].Value);
    Result.tgl_input := BufferToDateTime(buffer.Fields[6].Value);
    Result.tgl_hapus := BufferToDateTime(buffer.Fields[7].Value);
  end;
  buffer.Close;
end;

function Save_master_bank (var R_master_bank : TR_master_bank) : integer;
begin
    R_master_bank.seq := CreateNewSeq('seq','master_bank');
    sql := 'INSERT INTO master_bank (seq, nama, cabang, norek, an, user_id, tgl_input, tgl_hapus)  '+
         'VALUES ('+FormatSQLNumber(R_master_bank.seq)+', '+
                    FormatSQLString(R_master_bank.nama)+', '+
                    FormatSQLString(R_master_bank.cabang)+', '+
                    FormatSQLString(R_master_bank.norek)+', '+
                    FormatSQLString(R_master_bank.an)+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLDateTimeNow +', '+
                    FormatSQLnull2+')';
  ExecTransaction3(sql);
  Result := R_master_bank.seq;
end;

procedure Delete_master_bank (VKey : integer);
begin
  sql := 'DELETE FROM master_bank '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;
 
procedure Update_master_bank (R_master_bank : TR_master_bank);
begin
  sql := 'UPDATE master_bank SET '+
                'nama = '+FormatSQLString(R_master_bank.nama)+', '+
                'cabang = '+FormatSQLString(R_master_bank.cabang)+', '+
                'norek = '+FormatSQLString(R_master_bank.norek)+', '+
                'an = '+FormatSQLString(R_master_bank.an)+' '+
         'WHERE seq = '+FormatSQLNumber(R_master_bank.seq);
  ExecTransaction3(sql);
end;
 
function Get_Arr_master_bank (Cari : string = ''; addpar : string = '') : AR_master_bank;
var buffer : _Recordset;
    i : integer;
    filter : string;
begin
  filter := '';
  if cari <> '' then 
    Filter := Filter + ' (upper(nama) LIKE ' +FormatSQLString('%'+UpperCase(cari)+'%')+'))';

  filter := filter + addpar;
  sql := 'SELECT seq, nama, cabang, norek, an, user_id, tgl_input, tgl_hapus FROM master_bank '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].nama := BufferToString(buffer.Fields[1].Value);
    Result[i].cabang := BufferToString(buffer.Fields[2].Value);
    Result[i].norek := BufferToString(buffer.Fields[3].Value);
    Result[i].an := BufferToString(buffer.Fields[4].Value);
    Result[i].user_id := BufferToString(buffer.Fields[5].Value);
    Result[i].tgl_input := BufferToDateTime(buffer.Fields[6].Value);
    Result[i].tgl_hapus := BufferToDateTime(buffer.Fields[7].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

procedure Get_List_master_bank (var Alist : TStringlist; ASeq : integer = 0);
var buffer : _Recordset;
    filter : string;
    i : integer;
begin
  filter := '';
  if ASeq <> 0 then begin 
    if Alist.Values[IntToStr(ASeq)] <> '' then begin
      Alist.Delete(Alist.IndexOfName(IntToStr(ASeq))); 
    end; 
    filter := filter + ' and seq = ' + FormatSQLNumber(ASeq) 
  end else Alist.Clear; 
  sql := 'SELECT seq, nama, cabang, norek, an, user_id, tgl_input, tgl_hapus FROM master_bank '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount -1 do begin
    AList.add(BufferToString(buffer.Fields[0].Value)+'='+// 
              BufferToString(buffer.Fields[1].Value)+'#'+// 
              BufferToString(buffer.Fields[2].Value)+'#'+// 
              BufferToString(buffer.Fields[3].Value)+'#'+// 
              BufferToString(buffer.Fields[4].Value)+'#'+// 
              BufferToString(buffer.Fields[5].Value)+'#'+// 
              BufferToString(buffer.Fields[6].Value)+'#'+// 
              BufferToString(buffer.Fields[7].Value));// 
    buffer.MoveNext;
  end;
  buffer.Close;
end;

{company_info}
procedure Reset_Record_company_info (var Data : TR_company_info);
begin
  Data.nama := '';
  Data.alamat := '';
  Data.no_telp := '';
  Data.no_fax := '';
  Data.email := '';
  Data.no_npwp := '';
  Data.png_jwb1 := '';
  Data.jabatan1 := '';
  Data.png_jwb2 := '';
  Data.jabatan2 := '';
  Data.logo := '';
end;

function Get_company_info : TR_company_info;
var buffer : _Recordset;
begin
  sql := 'SELECT nama, alamat, no_telp, no_fax, email, no_npwp, png_jwb1, jabatan1, png_jwb2, ' +
    'jabatan2, logo FROM company_info ';
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.nama := BufferToString(buffer.Fields[0].Value);
    Result.alamat := BufferToString(buffer.Fields[1].Value);
    Result.no_telp := BufferToString(buffer.Fields[2].Value);
    Result.no_fax := BufferToString(buffer.Fields[3].Value);
    Result.email := BufferToString(buffer.Fields[4].Value);
    Result.no_npwp := BufferToString(buffer.Fields[5].Value);
    Result.png_jwb1 := BufferToString(buffer.Fields[6].Value);
    Result.jabatan1 := BufferToString(buffer.Fields[7].Value);
    Result.png_jwb2 := BufferToString(buffer.Fields[8].Value);
    Result.jabatan2 := BufferToString(buffer.Fields[9].Value);
    Result.logo := BufferToString(buffer.Fields[10].Value);
  end;
  buffer.Close;
end;

procedure Save_company_info (R_company_info : TR_company_info);
begin
    sql := 'INSERT INTO company_info (nama, alamat, no_telp, no_fax, email, no_npwp, png_jwb1, '+
    'jabatan1, png_jwb2, jabatan2, logo)  '+
         'VALUES ('+FormatSQLString(R_company_info.nama)+', '+
                    FormatSQLString(R_company_info.alamat)+', '+
                    FormatSQLString(R_company_info.no_telp)+', '+
                    FormatSQLString(R_company_info.no_fax)+', '+
                    FormatSQLString(R_company_info.email)+', '+
                    FormatSQLString(R_company_info.no_npwp)+', '+
                    FormatSQLString(R_company_info.png_jwb1)+', '+
                    FormatSQLString(R_company_info.jabatan1)+', '+
                    FormatSQLString(R_company_info.png_jwb2)+', '+
                    FormatSQLString(R_company_info.jabatan2)+', '+
                    FormatSQLString(R_company_info.logo)+')';
  ExecTransaction3(sql);
end;

procedure Delete_company_info;
begin
  sql := 'DELETE FROM company_info ';
  ExecTransaction3(sql);
end;

procedure Update_company_info (R_company_info : TR_company_info);
begin
  sql := 'UPDATE company_info SET '+
                'alamat = '+FormatSQLString(R_company_info.alamat)+', '+
                'no_telp = '+FormatSQLString(R_company_info.no_telp)+', '+
                'no_fax = '+FormatSQLString(R_company_info.no_fax)+', '+
                'email = '+FormatSQLString(R_company_info.email)+', '+
                'no_npwp = '+FormatSQLString(R_company_info.no_npwp)+', '+
                'png_jwb1 = '+FormatSQLString(R_company_info.png_jwb1)+', '+
                'jabatan1 = '+FormatSQLString(R_company_info.jabatan1)+', '+
                'png_jwb2 = '+FormatSQLString(R_company_info.png_jwb2)+', '+
                'jabatan2 = '+FormatSQLString(R_company_info.jabatan2)+', '+
                'logo = '+FormatSQLString(R_company_info.logo)+'  '+
         'WHERE nama = '+FormatSQLString(R_company_info.nama);
 ExecTransaction3(sql);
end;

function Get_Arr_company_info ( addpar : string = '') : AR_company_info;
var buffer : _Recordset;
    i : integer;
    filter : string;
begin
  filter := '';
  filter := filter + addpar;
  sql := 'SELECT nama, alamat, no_telp, no_fax, email, no_npwp, png_jwb1, jabatan1, png_jwb2, ' +
    'jabatan2, logo FROM company_info '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)));
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin
    Result[i].nama := BufferToString(buffer.Fields[0].Value);
    Result[i].alamat := BufferToString(buffer.Fields[1].Value);
    Result[i].no_telp := BufferToString(buffer.Fields[2].Value);
    Result[i].no_fax := BufferToString(buffer.Fields[3].Value);
    Result[i].email := BufferToString(buffer.Fields[4].Value);
    Result[i].no_npwp := BufferToString(buffer.Fields[5].Value);
    Result[i].png_jwb1 := BufferToString(buffer.Fields[6].Value);
    Result[i].jabatan1 := BufferToString(buffer.Fields[7].Value);
    Result[i].png_jwb2 := BufferToString(buffer.Fields[8].Value);
    Result[i].jabatan2 := BufferToString(buffer.Fields[9].Value);
    Result[i].logo := BufferToString(buffer.Fields[10].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;
procedure Get_List_company_info (var Alist : TStringlist; ASeq : integer = 0);
var buffer : _Recordset;
    filter : string;
    i : integer;
begin
  filter := '';
  if ASeq <> 0 then begin
    if Alist.Values[IntToStr(ASeq)] <> '' then begin
      Alist.Delete(Alist.IndexOfName(IntToStr(ASeq)));
    end;
    filter := filter + ' and seq = ' + FormatSQLNumber(ASeq)
  end else Alist.Clear;
  sql := 'SELECT nama, alamat, no_telp, no_fax, email, no_npwp, png_jwb1, jabatan1, png_jwb2, ' +
    'jabatan2, logo FROM company_info '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)));
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount -1 do begin
    AList.add(BufferToString(buffer.Fields[0].Value)+'='+// nama
              BufferToString(buffer.Fields[1].Value)+'#'+// alamat
              BufferToString(buffer.Fields[2].Value)+'#'+// no_telp
              BufferToString(buffer.Fields[3].Value)+'#'+// no_fax
              BufferToString(buffer.Fields[4].Value)+'#'+// email
              BufferToString(buffer.Fields[5].Value)+'#'+// no_npwp
              BufferToString(buffer.Fields[6].Value)+'#'+// png_jwb1
              BufferToString(buffer.Fields[7].Value)+'#'+// jabatan1
              BufferToString(buffer.Fields[8].Value)+'#'+// png_jwb2
              BufferToString(buffer.Fields[9].Value)+'#'+// jabatan2
              BufferToString(buffer.Fields[10].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

{LOG_FILE}
function Get_LOG_FILE (VKey : integer) : TR_LOG_FILE;
var buffer : _Recordset;
begin
  sql := 'SELECT Seq, Seq_Dummy, Seq_Edit, Nama_Tabel, Tanggal, User_ID, Keterangan, Checked FROM LOG_FILE '+
         'WHERE Seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.Seq := BufferToInteger(buffer.Fields[0].Value);
    Result.Seq_Dummy := BufferToInteger(buffer.Fields[1].Value);
    Result.Seq_Edit := BufferToInteger(buffer.Fields[2].Value);
    Result.Nama_Tabel := BufferToString(buffer.Fields[3].Value);
    Result.Tanggal := BufferToDateTime(buffer.Fields[4].Value);
    Result.User_ID := BufferToString(buffer.Fields[5].Value);
    Result.Keterangan := BufferToString(buffer.Fields[6].Value);
    Result.Checked := BufferToString(buffer.Fields[7].Value);
  end;
  buffer.Close;
end;
 
function Save_LOG_FILE (R_LOG_FILE : TR_LOG_FILE) : integer;
begin
  R_LOG_FILE.Seq := CreateNewSeq('Seq', 'LOG_FILE');
  sql := 'INSERT INTO LOG_FILE (Seq, Seq_Dummy, Seq_Edit, Nama_Tabel, Tanggal, User_ID, Keterangan, Checked, catatan_hapus )  '+
         'VALUES ('+FormatSQLNumber(R_LOG_FILE.Seq)+', '+
                    FormatSQLNumber(R_LOG_FILE.Seq_Dummy)+', '+
                    FormatSQLNumber(R_LOG_FILE.Seq_Edit)+', '+
                    FormatSQLString(R_LOG_FILE.Nama_Tabel)+', '+
                    FormatSQLDateNow+', '+
                    FormatSQLString(GlobalSystemUser.UserId)+', '+
                    FormatSQLString(R_LOG_FILE.Keterangan)+', '+
                    FormatSQLString(UConst.STATUS_FALSE)+','+
                    FormatSQLString(R_LOG_FILE.catatan_hapus)+')';
  if ExecTransaction3(sql) then Result := R_LOG_FILE.Seq else Result := 0;
end;
 
procedure Delete_LOG_FILE (VKey : integer);
begin
  sql := 'DELETE FROM LOG_FILE WHERE Seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;

procedure Update_LOG_FILE (R_LOG_FILE : TR_LOG_FILE);
begin
  sql := 'UPDATE LOG_FILE SET '+
                'Seq_Dummy = '+FormatSQLNumber(R_LOG_FILE.Seq_Dummy)+', '+
                'Seq_Edit = '+FormatSQLNumber(R_LOG_FILE.Seq_Edit)+', '+
                'Nama_Tabel = '+FormatSQLString(R_LOG_FILE.Nama_Tabel)+', '+
                'Tanggal = '+FormatSQLDate(R_LOG_FILE.Tanggal)+', '+
                'User_ID = '+FormatSQLString(R_LOG_FILE.User_ID)+', '+
                'Keterangan = '+FormatSQLString(R_LOG_FILE.Keterangan)+', '+
                'Checked = '+FormatSQLString(R_LOG_FILE.Checked)+' '+
         'WHERE Seq = '+FormatSQLNumber(R_LOG_FILE.Seq);
 ExecTransaction3(sql);
end;

procedure UpdateSeqEditLogFile(SeqEdit, VKey : Integer);
begin
   sql := 'UPDATE Log_File SET Seq_Edit = ' + FormatSQLNumber(SeqEdit) + ' WHERE Seq = ' + FormatSQLNumber(VKey);
  myConnection.ExecSQL(sql)
end;

function GetLastSeqLogFile : Integer;
begin
   Result := getIntegerFromSQL('SELECT MAX(Seq) FROM log_file');
end;

function LoadLogFile(PerAwal, PerAkhir : TDate; UserID : string; IsEdit : TBooleanOperator = boNone) : AR_Log_File;
var Filter : String;
    buffer : _Recordset;
    i : Integer;
begin
  Filter := '';
  if (PerAwal <> 0) and (PerAkhir <> 0) then
    Filter := Filter+' AND Tanggal BETWEEN '+FormatSQLDate(PerAwal)+' AND '+FormatSQLDate(PerAkhir);
  if UserID <> '' then
    Filter := Filter + ' AND User_ID = '+FormaTSQLSTring(UserID);
  if IsEdit <> boNone then begin
    if IsEdit = boTrue then
      Filter := Filter + ' AND Seq_Edit <> 0'
    else Filter := Filter + ' AND Seq_Edit = 0';
  end;
  sql := 'SELECT Seq, Seq_Dummy, Seq_Edit, Nama_Tabel, Tanggal, '+
            'Keterangan, Checked, User_Id FROM Log_File';
  if Filter <> '' then
    sql := sql + ' WHERE ' + copy(Filter, 5, length(Filter));
  sql := sql + ' ORDER BY Tanggal, Keterangan';
  buffer := myConnection.OpenSQL(sql);
  setlength(Result, buffer.RecordCount);
  if buffer.RecordCount > 0 then begin
    for i:=0 to buffer.RecordCount-1 do begin
      Result[i].Seq        := BufferToInteger(buffer.Fields[0].Value);
      Result[i].Seq_Dummy  := BufferToInteger(buffer.Fields[1].Value);
      Result[i].Seq_Edit   := BufferToInteger(buffer.Fields[2].Value);
      Result[i].Nama_Tabel := BufferToString(buffer.Fields[3].Value);
      Result[i].Tanggal    := BufferToDateTime(buffer.Fields[4].Value);
      Result[i].Keterangan := BufferToString(buffer.Fields[5].Value);
      Result[i].Checked    := BufferToString(buffer.Fields[6].Value);
      Result[i].User_id    := BufferToString(buffer.Fields[7].Value);
      buffer.MoveNext;
    end;
  end;
  buffer.Close;
end;



 
{master_ppn}
procedure Reset_Record_master_ppn (var Data : TR_master_ppn);
begin
  Data.seq := 0;
  Data.tgl_berlaku := 0;
  Data.ppn := 0;
  Data.tgl_nonaktif := 0;
end;
 
function Get_master_ppn (VKey : integer) : TR_master_ppn;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, tgl_berlaku, ppn, tgl_nonaktif FROM master_ppn '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin 
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.tgl_berlaku := BufferToDateTime(buffer.Fields[1].Value);
    Result.ppn := BufferToFloat(buffer.Fields[2].Value);
    Result.tgl_nonaktif := BufferToDateTime(buffer.Fields[3].Value);
  end;
  buffer.Close;
end;
 
function Save_master_ppn (var R_master_ppn : TR_master_ppn) : integer;
begin 
    R_master_ppn.seq := CreateNewSeq('seq','master_ppn');
    sql := 'INSERT INTO master_ppn (seq, tgl_berlaku, ppn, tgl_nonaktif)  '+
         'VALUES ('+FormatSQLNumber(R_master_ppn.seq)+', '+
                    FormatSQLDate(R_master_ppn.tgl_berlaku)+', '+
                    FormatSQLNumber(R_master_ppn.ppn)+', '+
                    FormatSQLDate(R_master_ppn.tgl_nonaktif)+')';
  ExecTransaction3(sql);
    Result := R_master_ppn.seq; 
end;
 
procedure Delete_master_ppn (VKey : integer);
begin
  sql := 'DELETE FROM master_ppn '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;
 
procedure Update_master_ppn (R_master_ppn : TR_master_ppn);
begin
  sql := 'UPDATE master_ppn SET '+
                'tgl_berlaku = '+FormatSQLDate(R_master_ppn.tgl_berlaku)+', '+
                'ppn = '+FormatSQLNumber(R_master_ppn.ppn)+', '+
                'tgl_nonaktif = '+FormatSQLDate(R_master_ppn.tgl_nonaktif)+'  '+
         'WHERE seq = '+FormatSQLNumber(R_master_ppn.seq);
 ExecTransaction3(sql);
end;
 
function Get_Arr_master_ppn (Cari : string = ''; addpar : string = '') : AR_master_ppn;
var buffer : _Recordset;
    i : integer;
    filter : string;
begin
  filter := '';
  if cari <> '' then 
    Filter := Filter + ' AND ((upper(kode) LIKE '+FormatSQLString('%'+UpperCase(cari)+'%')+') OR '+ 
    ' (upper(nama) LIKE ' +FormatSQLString('%'+UpperCase(cari)+'%')+'))';
  filter := filter + addpar;
  sql := 'SELECT seq, tgl_berlaku, ppn, tgl_nonaktif FROM master_ppn '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  SetLength(result, buffer.RecordCount);
  for i := 0 to buffer.RecordCount -1 do begin 
    Result[i].seq := BufferToInteger(buffer.Fields[0].Value);
    Result[i].tgl_berlaku := BufferToDateTime(buffer.Fields[1].Value);
    Result[i].ppn := BufferToFloat(buffer.Fields[2].Value);
    Result[i].tgl_nonaktif := BufferToDateTime(buffer.Fields[3].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;
procedure Get_List_master_ppn (var Alist : TStringlist; ASeq : integer = 0);
var buffer : _Recordset;
    filter : string;
    i : integer;
begin
  filter := '';
  if ASeq <> 0 then begin
    if Alist.Values[IntToStr(ASeq)] <> '' then begin
      Alist.Delete(Alist.IndexOfName(IntToStr(ASeq)));
    end;
    filter := filter + ' and seq = ' + FormatSQLNumber(ASeq)
  end else Alist.Clear;
  sql := 'SELECT seq, tgl_berlaku, ppn, tgl_nonaktif FROM master_ppn '+
          ifthen(filter <> '', ' WHERE ' + Copy(filter, 5, length(filter)))+' order by seq';
  buffer := myConnection.OpenSQL(sql);
  for i := 0 to buffer.RecordCount -1 do begin
    AList.add(BufferToString(buffer.Fields[0].Value)+'='+// seq
              BufferToString(buffer.Fields[1].Value)+'#'+// tgl_berlaku
              BufferToString(buffer.Fields[2].Value)+'#'+// ppn
              BufferToString(buffer.Fields[3].Value));// tgl_nonaktif
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Get_nilai_ppn (tgl: tdate) : real;
var ppn : real;
begin
  sql := 'SELECT max(ppn) FROM master_ppn WHERE tgl_berlaku <=  '+FormatSQLDate(tgl);
  Result := getFloatFromSQL(sql)
end;



end.




