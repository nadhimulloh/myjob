unit KelolaUser;

interface

uses
  USystemMenu, StdCtrls, Classes, Controls, Grids, BaseGrid, AdvGrid, AddUser, Login,
  Windows, Messages, SysUtils, Variants, Graphics, Forms,
  Dialogs, AdvGlowButton, AdvObj;

type
  TfrmKelolaUser = class(TForm)
    GroupBox1: TGroupBox;
    asgSystUsers: TAdvStringGrid;
    GroupBox2: TGroupBox;
    rbtAktif: TRadioButton;
    rbtNonAktif: TRadioButton;
    btnCancel: TAdvGlowButton;
    btnRemove: TAdvGlowButton;
    btnAdd: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure asgSystUsersButtonClick(Sender: TObject; ACol,
      ARow: Integer);
    procedure asgSystUsersGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure asgSystUsersCheckBoxClick(Sender: TObject; ACol,
      ARow: Integer; State: Boolean);
    procedure asgSystUsersCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure FormShow(Sender: TObject);
    procedure asgSystUsersMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgSystUsersMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgSystUsersDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgSystUsersSelectionChanged(Sender: TObject; ALeft, ATop, ARight,
      ABottom: Integer);
    procedure asgSystUsersClickCell(Sender: TObject; ARow, ACol: Integer);
  private
    systUsers : TSystemUser_Arr;
    accessList : TStringList;
    AIdMenu: INteger;
    procedure loadData;
    procedure ArrangeColsize;
    { Private declarations }
  public
    procedure execute(AMenuId: Integer);
    { Public declarations }
  end;

var
  frmKelolaUser: TfrmKelolaUser;

implementation

uses ChangePassword, MainMenu, UConst, UGeneral;

{$R *.dfm}

{ TkelolaSystUserFrm }

procedure TfrmKelolaUser.execute(AMenuId: Integer);
begin
  if (not BisaNambah(AMenuId)) and (not BisaEdit(AMenuId)) and (not BisaHapus(AMenuId))
  and (not BisaLihatRekap(AMenuId)) and (not BisaPrint(AMenuId)) and (not BisaEkspor(AMenuId)) then
  begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  AIdMenu := AMenuId;
//  btnAdd.Enabled := MainMenu.DEPT_ID <> 'ALL';
//  btnEdit.Enabled := MainMenu.DEPT_ID <> 'ALL';
//  btnRemove.Enabled := MainMenu.DEPT_ID <> 'ALL';
  loadData;
  Run(Self);
end;

procedure TfrmKelolaUser.loadData;
var i:integer;
begin
  TSystemUser.AccessLevelList(accessList);
  systUsers.Clear;
  systUsers.FindOnDB('','',GlobalSystemUser.AccessLevel,False);
  asgSystUsers.RowCount := systUsers.Count + 1;
  asgSystUsers.Cells[0,0] := 'No.';
  asgSystUsers.Cells[1,0] := 'User Id';
  asgSystUsers.Cells[2,0] := 'Name';
  asgSystUsers.Cells[3,0] := 'Access Level';
  asgSystUsers.Cells[4,0] := 'State';
  asgSystUsers.Cells[5,0] := 'Action';
  asgSystUsers.Cells[6,0] := 'IsDisable';  //buat pengecekan penghapusan

  for i := 0 to systUsers.Count-1 do begin
    asgSystUsers.Cells[1,i+1] := systUsers[i].UserId;
    asgSystUsers.Cells[2,i+1] := systUsers[i].UserName;
    asgSystUsers.Cells[3,i+1] := accessList.Values[IntToStr(systUsers[i].AccessLevel)];
    asgSystUsers.AddCheckBox(4,i+1,not systUsers[i].is_disabled,false);
    asgSystUsers.AddButton(5,i+1,60,19,'Set Password',haCenter,vaCenter);
    if systUsers[i].DisableDate <> 0 then
      asgSystUsers.Dates[6,i+1] := systUsers[i].DisableDate;
  end;
  asgSystUsers.AutoNumberCol(0);
//  asgSystUsers.AutoSizeColumns(True,2);
  ArrangeColsize;
end;

procedure TfrmKelolaUser.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmKelolaUser.FormCreate(Sender: TObject);
begin
  accessList := TStringList.Create;
  systUsers := TSystemUser_Arr.Create;
end;

procedure TfrmKelolaUser.FormDestroy(Sender: TObject);
begin
  SystUsers.Destroy;
  accessList.Destroy;
end;

procedure TfrmKelolaUser.FormShow(Sender: TObject);
begin
  ArrangeColsize;
end;

procedure TfrmKelolaUser.ArrangeColsize;
begin
  asgSystUsers.AutoSizeColumns(True,2);
  if GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER then Exit;
  asgSystUsers.ColWidths[6] := 0;
end;

procedure TfrmKelolaUser.asgSystUsersButtonClick(Sender: TObject; ACol, ARow: Integer);
begin
  //if MainMenu.DEPT_ID <> 'ALL' then
    Application.CreateForm(TfrmChangePassword, frmChangePassword);
    frmChangePassword.OnClose := frmMainMenu.ChildFormSingle;
    frmChangePassword.Execute(asgSystUsers.Cells[1, ARow])
  //else Alert('Dengan login sekarang anda tidak dapat mengakses fitur ini.');
  {if (MessageDlg('Password di-reset kosong',mtConfirmation,[mbYes,mbNo],0)) = mrYes then begin
    systUsers[ARow-1].LoginPwd := '';
    systUsers[ARow-1].UpdatePasswordOnDB;
    ShowMessage('Sukses');
  end;}
end;

procedure TfrmKelolaUser.asgSystUsersGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [0] then HAlign := taRightJustify;
  if ACol in [4,5] then
    HAlign := taCenter;
end;

procedure TfrmKelolaUser.asgSystUsersMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColsize;
end;

procedure TfrmKelolaUser.asgSystUsersMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColsize;
end;

procedure TfrmKelolaUser.asgSystUsersSelectionChanged(Sender: TObject; ALeft,
  ATop, ARight, ABottom: Integer);
begin
//  if (asgSystUsers.Row > 0) and (asgSystUsers.RowCount > 1) then begin
//    btnEdit.Enabled := (VarToDateTime(asgSystUsers.Cells[6,asgSystUsers.Row]) = 0) or (TrimAllString(asgSystUsers.cells[6,asgSystUsers.Row]) = '') ;
//  end;
end;

procedure TfrmKelolaUser.btnRemoveClick(Sender: TObject);
var aUser :TSystemUser; i:integer; state:boolean;
begin
  if (not BisaHapus(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  if (asgSystUsers.Row > 0) and (asgSystUsers.Cells[1, asgSystUsers.Row] <> '') then begin
    if (asgSystUsers.Cells[6,asgSystUsers.Row] = '') or (VarToDateTime(asgSystUsers.Cells[6,asgSystUsers.Row]) = 0) then begin
//      Inform('User Id "'+ asgSystUsers.Cells[1, asgSystUsers.Row] +'" status aktif, tidak bisa ' +
//        'dihapus. '+#13+' Non aktifkan dahulu User Id tersebut.');
      Inform('User Id "'+ asgSystUsers.Cells[1, asgSystUsers.Row] +'" cannot be ' +
        'deleted.'+#13+' Please set deactivated first.');
      Exit;
    end;
    if MessageDlg('User Id "'+ asgSystUsers.Cells[1, asgSystUsers.Row] +'" will be ' +
      'deleted. Are you ' +
      'sure ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      aUser := TSystemUser.Create;
      aUser.UserId := asgSystUsers.Cells[1, asgSystUsers.row];
      aUser.DeleteOnDB;
      aUser.Destroy;
      Alert(MSG_SUCCESS_DELETING);
      if asgSystUsers.Row <> asgSystUsers.RowCount - 1 then begin
        for i := asgSystUsers.Row +1 to asgSystUsers.RowCount -1 do begin
          asgSystUsers.Cells[1,i-1] := asgSystUsers.Cells[1,i];
          asgSystUsers.Cells[2,i-1] := asgSystUsers.Cells[2,i];
          asgSystUsers.Cells[3,i-1] := asgSystUsers.Cells[3,i];
          asgSystUsers.GetCheckBoxState(4,i,state);
          asgSystUsers.SetCheckBoxState(4,i-i,state);
        end;
      end;
      asgSystUsers.RowCount := asgSystUsers.RowCount - 1;
      ArrangeColsize;
    end;
  end;
end;

procedure TfrmKelolaUser.btnAddClick(Sender: TObject);
begin
  if (not BisaNambah(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  Application.CreateForm(TfrmAddUser, frmAddUser);
  frmAddUser.OnClose := frmMainMenu.ChildFormSingle;
  frmAddUser.execute;
  loadData;
end;

procedure TfrmKelolaUser.btnEditClick(Sender: TObject);
begin
  if (not BisaEdit(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  if (asgSystUsers.Row > 0) and (asgSystUsers.Cells[1, asgSystUsers.Row] <> '') and (asgSystUsers.Cells[6, asgSystUsers.Row] = '') then begin
    Application.CreateForm(TfrmAddUser, frmAddUser);
    frmAddUser.OnClose := frmMainMenu.ChildFormSingle;
    frmAddUser.Execute(asgSystUsers.Cells[1,asgSystUsers.Row]);
    LoadData;
  end;
end;

procedure TfrmKelolaUser.asgSystUsersCheckBoxClick(Sender: TObject;
  ACol, ARow: Integer; State: Boolean);
var User : TSystemUser;
begin
  User := TSystemUser.Create;
//  systUsers[ARow-1].is_disabled := state;
  //systUsers[ARow-1].UpdateOnDB;
  asgSystUsers.GetCheckBoxState(4,ARow,State);

  User.Aktivasi_User(TrimAllString(asgSystUsers.Cells[1,Arow]),State);
  if state = false then begin
    Alert('User ' + asgSystUsers.Cells[1,Arow] + ' has been unactivated');
    loadData;
  end
  else begin
    Alert('User ' + asgSystUsers.Cells[1,Arow] + ' has been activated');
    loadData;
  end;
  User.Destroy;
end;

procedure TfrmKelolaUser.asgSystUsersClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
//  if (asgSystUsers.Row > 0) and (asgSystUsers.RowCount > 1) then begin
//    btnEdit.Enabled := (VarToDateTime(asgSystUsers.Cells[6,asgSystUsers.Row]) = 0) or (TrimAllString(asgSystUsers.cells[6,asgSystUsers.Row]) = '') ;
//  end;
end;

procedure TfrmKelolaUser.asgSystUsersDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
//  if ARow > 0 then begin
//    if btnEdit.Enabled then btnEditClick(nil);
//  end;
end;

procedure TfrmKelolaUser.asgSystUsersCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if Arow > 0 then
    CanEdit := (ACol in [4,5]) //and (MainMenu.DEPT_ID <> 'ALL');
end;

end.
