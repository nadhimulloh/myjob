object frmKelolaUser: TfrmKelolaUser
  Left = 302
  Top = 118
  Caption = 'Kelola Sistem User'
  ClientHeight = 407
  ClientWidth = 726
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    726
    407)
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 39
    Width = 710
    Height = 360
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 4
    DesignSize = (
      710
      360)
    object asgSystUsers: TAdvStringGrid
      Left = 7
      Top = 11
      Width = 695
      Height = 343
      Cursor = crDefault
      Anchors = [akLeft, akTop, akRight, akBottom]
      ColCount = 8
      DefaultRowHeight = 18
      RowCount = 5
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing]
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
      OnMouseWheelDown = asgSystUsersMouseWheelDown
      OnMouseWheelUp = asgSystUsersMouseWheelUp
      OnGetAlignment = asgSystUsersGetAlignment
      OnClickCell = asgSystUsersClickCell
      OnDblClickCell = asgSystUsersDblClickCell
      OnCanEditCell = asgSystUsersCanEditCell
      OnButtonClick = asgSystUsersButtonClick
      OnCheckBoxClick = asgSystUsersCheckBoxClick
      OnSelectionChanged = asgSystUsersSelectionChanged
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'MS Sans Serif'
      ActiveCellFont.Style = [fsBold]
      CellNode.ShowTree = False
      ColumnSize.Stretch = True
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.ControlStyle = csClassic
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      ControlLook.NoDisabledButtonLook = True
      EnhRowColMove = False
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDownClear = '(All)'
      FixedColWidth = 30
      FixedRowHeight = 19
      FixedRowAlways = True
      FixedColAlways = True
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = []
      FloatFormat = '%.5n'
      MouseActions.AllColumnSize = True
      MouseActions.RowSelect = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'MS Sans Serif'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'MS Sans Serif'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'MS Sans Serif'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'MS Sans Serif'
      PrintSettings.FooterFont.Style = []
      PrintSettings.Borders = pbNoborder
      PrintSettings.Centered = False
      PrintSettings.PageNumSep = '/'
      ScrollWidth = 16
      SearchFooter.FindNextCaption = 'Find &next'
      SearchFooter.FindPrevCaption = 'Find &previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'MS Sans Serif'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurence'
      SearchFooter.HintFindPrev = 'Find previous occurence'
      SearchFooter.HintHighlight = 'Highlight occurences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SelectionColor = clHighlight
      SelectionTextColor = clHighlightText
      ShowDesignHelper = False
      SortSettings.Show = True
      SortSettings.NormalCellsOnly = True
      Version = '6.0.4.2'
      WordWrap = False
      ExplicitWidth = 613
      ExplicitHeight = 504
      ColWidths = (
        30
        37
        83
        70
        53
        66
        104
        247)
      RowHeights = (
        19
        18
        18
        18
        18)
    end
  end
  object GroupBox2: TGroupBox
    Left = 332
    Top = 1
    Width = 149
    Height = 36
    Caption = 'Status'
    TabOrder = 5
    Visible = False
    object rbtAktif: TRadioButton
      Left = 15
      Top = 14
      Width = 44
      Height = 17
      Caption = 'Aktif'
      Checked = True
      TabOrder = 0
      TabStop = True
    end
    object rbtNonAktif: TRadioButton
      Left = 65
      Top = 14
      Width = 70
      Height = 17
      Caption = 'Non Aktif'
      TabOrder = 1
    end
  end
  object btnCancel: TAdvGlowButton
    Left = 251
    Top = 8
    Width = 75
    Height = 25
    Caption = '&Close'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    NotesFont.Charset = DEFAULT_CHARSET
    NotesFont.Color = clWindowText
    NotesFont.Height = -11
    NotesFont.Name = 'Tahoma'
    NotesFont.Style = []
    ParentFont = False
    Picture.Data = {
      89504E470D0A1A0A0000000D49484452000000140000001408060000008D891D
      0D0000024A49444154384FAD94C16BD3501CC7BF2F699BAD8975154477503CB8
      3127C8305B87C2C4D293FFC58E524174C80E0A7A109987E1D845D8C9B3FF839B
      456160BBA06C6363B307A5A03BE8DCEC9226B6C9F3BDCC84A48D8D03DF2590F7
      7E9FDFF7F7FBFEDE23F8CF8B74E351807C55D5DE1E20C9CF65812634ADC182D8
      56F48A047EBC715EEAB74ECC80D022634AE1506AC11116E4DAF76952AD5AEDD8
      0EE0FEB5910131915A050113D655BF456D3A7AACB4B21E3C1502BAB0646AFB28
      6D751C6728F37A65CB8BF181747838659C56F62841EF518084A299DE3950C8C6
      C62F1EE7037FE673CF04017743F2CF9C03AD7D0AF149C43F66D19CB2549EF281
      DC4DBD30D6081AC003D32F5EA2555E8675FF308FF4E829121379348A9370B637
      0389A8252F5664A6CE76157E51D574A64FD4DB4B951ECF22716502F6DA07C030
      208E5F3D4CF08089A1E1C9716CFB64A6A47D7381BBAA7A3CD527EE45F5AE67F6
      39C411D5DDB2B73661DE9A8C6CB1D0A467D36F2AB578E0933957990B5C7D0F73
      EA663C90B292F5A892FFF48C2B4343674A47D15A7E0BEBE1BD0EA8CC4A265EC9
      DC14A39033D8D71F66DF9477BC6771A6C09417CB8A6F0A4F77901F9F87406F07
      530B034370AA6C660306088317DA1C661194CE2B4B953BA1397407BB5FD90FAA
      FCC7016FC99F7715EF5E87AE5E3D3F769108247437E3A049625F925E696BDEB9
      8EC7C12CE4066D8A757605DD27ABCB6A31D8E5202C54723090975F3FA5CC8802
      8AED2D600A4C4AE982BCA34F7BF737181BF7C08AF5EB6AB6D5449A0765933050
      D27E7037FFA6BC2B30AE7F51FBBF01AD04E81511DF13380000000049454E44AE
      426082}
    WordWrap = False
    TabOrder = 3
    TabStop = True
    OnClick = btnCancelClick
    Appearance.ColorChecked = 16111818
    Appearance.ColorCheckedTo = 16367008
    Appearance.ColorDisabled = 15921906
    Appearance.ColorDisabledTo = 15921906
    Appearance.ColorDown = 16111818
    Appearance.ColorDownTo = 16367008
    Appearance.ColorHot = 16117985
    Appearance.ColorHotTo = 16372402
    Appearance.ColorMirrorHot = 16107693
    Appearance.ColorMirrorHotTo = 16775412
    Appearance.ColorMirrorDown = 16102556
    Appearance.ColorMirrorDownTo = 16768988
    Appearance.ColorMirrorChecked = 16102556
    Appearance.ColorMirrorCheckedTo = 16768988
    Appearance.ColorMirrorDisabled = 11974326
    Appearance.ColorMirrorDisabledTo = 15921906
  end
  object btnRemove: TAdvGlowButton
    Left = 170
    Top = 8
    Width = 75
    Height = 25
    Caption = '&Delete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    NotesFont.Charset = DEFAULT_CHARSET
    NotesFont.Color = clWindowText
    NotesFont.Height = -11
    NotesFont.Name = 'Tahoma'
    NotesFont.Style = []
    ParentFont = False
    Picture.Data = {
      89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
      F80000019949444154484B6364A03160A4B1F90C4459F0DF8241818185211ECD
      318B198F30DC23E4400C0BFE175A0A31FC654F036A64876B7EFD3481E1CF6F05
      14C398591E3188C9CC4312FBC9F0F5E72CC6B9C7DF21ABC3B420CFA102A8A09D
      90CBB0CAFF63A8649C72A003BF05B98EF60C1F5E3530FCF8EA4092251CDC0718
      04C41A1827EF3F88D70290E47F1B867A60EC349064C17F8606609C34A2EBC11A
      C9C3CF82FF1C5C0C8C3FBEA1F81E438CDC20FA2F2ECFF0A7773703D39A090CCC
      1BA6812DF9EB97CEF02FA28481A5D88D81F1F97D88C5E45AF0CF2D86E16FD95C
      B0194C930B8006FD63F8973709CC67EE4E6560DAB988320BC02EF64D63F8973F
      19258898A614C27D44910F60A6FEA959C2F0DF310CCC653CB48E81A531023545
      921B44F03087060BCC54AAF9E09F5B2C300EE6D02E0EFE8BC94252D1DA498854
      048A93F062482A7AF990F248A6693E20BA3C2231926B80855D33D18643325A0D
      B0B06B45D783BDB0B36630015A700C885989B4E437D0024BA0056789B200EC20
      4B06430666066F50862560C95FA0FC766C8683F30C912E245B19CD2D0000FF04
      C019FC3522A60000000049454E44AE426082}
    WordWrap = False
    TabOrder = 2
    TabStop = True
    OnClick = btnRemoveClick
    Appearance.ColorChecked = 16111818
    Appearance.ColorCheckedTo = 16367008
    Appearance.ColorDisabled = 15921906
    Appearance.ColorDisabledTo = 15921906
    Appearance.ColorDown = 16111818
    Appearance.ColorDownTo = 16367008
    Appearance.ColorHot = 16117985
    Appearance.ColorHotTo = 16372402
    Appearance.ColorMirrorHot = 16107693
    Appearance.ColorMirrorHotTo = 16775412
    Appearance.ColorMirrorDown = 16102556
    Appearance.ColorMirrorDownTo = 16768988
    Appearance.ColorMirrorChecked = 16102556
    Appearance.ColorMirrorCheckedTo = 16768988
    Appearance.ColorMirrorDisabled = 11974326
    Appearance.ColorMirrorDisabledTo = 15921906
  end
  object btnAdd: TAdvGlowButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = '&Add'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    NotesFont.Charset = DEFAULT_CHARSET
    NotesFont.Color = clWindowText
    NotesFont.Height = -11
    NotesFont.Name = 'Tahoma'
    NotesFont.Style = []
    ParentFont = False
    Picture.Data = {
      89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
      F8000001F349444154484BBD964D4EC25010C7E7D558F108468C8961E14A177A
      092C4A0B022A9E4023317A012FC006A327F01BA4AD14B8842E74C542A389E819
      F8086F9CA22095B64002BEA48BBE8FF9CDFC67DEB40C463CD888ED832BC05FF0
      4F8C553CEBC050418465C660CA7408013EE9E00320571BE57AA69828569D1C75
      04046E8331C22781B169B7280956A6E7B0A0683776FBBA014720488BF2312DEC
      0C241FE289F1A427E00878E7B92E80A4CA27031BFFB148919CE6156DD711B0A2
      CA5101E07A20CFFF6CE6D08815945C5BAE7604FE1425D42BBEB869BE391F6B9A
      BB2C39FB40C550E69EAAAFB8F29DF83620905D8B0313CEDCBCCFC96A73795553
      DC8344BE6D84EECEAD0055CED044783800CC18213D62014859F98DEA7C762800
      805743D1E6FE46606A2676024CCDB7E6375CE5B8285DD9E5A44A00CFFF02FE43
      A2A1251919A4F3B216B548B4AA05B71059B3B49C46BF65CA38C67361FDC20230
      2F9A3033F14C17C3EB04E8E7A2D1D9F7C9FAB82F1D4DD72C00F345CA2A11C6D0
      B62BBADFACDF55441ECD87EED2AD19BB6697A2C9BD7E0D76EE4386C779993A6A
      C7B06DD78185608A7A92A52BF6022290F1477DBF67BB6E1992B26B249790A4F7
      991EC6DF81E38111D6CD2AEC1AAE9FCCC84D44AC88B575E4A050444B74BAF575
      FBA052BC1700354F4DCCB4123A30A0972CFDAC8FFCAFE20B6E37C619BA920FF3
      0000000049454E44AE426082}
    WordWrap = False
    TabOrder = 0
    TabStop = True
    OnClick = btnAddClick
    Appearance.ColorChecked = 16111818
    Appearance.ColorCheckedTo = 16367008
    Appearance.ColorDisabled = 15921906
    Appearance.ColorDisabledTo = 15921906
    Appearance.ColorDown = 16111818
    Appearance.ColorDownTo = 16367008
    Appearance.ColorHot = 16117985
    Appearance.ColorHotTo = 16372402
    Appearance.ColorMirrorHot = 16107693
    Appearance.ColorMirrorHotTo = 16775412
    Appearance.ColorMirrorDown = 16102556
    Appearance.ColorMirrorDownTo = 16768988
    Appearance.ColorMirrorChecked = 16102556
    Appearance.ColorMirrorCheckedTo = 16768988
    Appearance.ColorMirrorDisabled = 11974326
    Appearance.ColorMirrorDisabledTo = 15921906
  end
  object btnEdit: TAdvGlowButton
    Left = 89
    Top = 8
    Width = 75
    Height = 25
    Caption = '&Edit'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    NotesFont.Charset = DEFAULT_CHARSET
    NotesFont.Color = clWindowText
    NotesFont.Height = -11
    NotesFont.Name = 'Tahoma'
    NotesFont.Style = []
    ParentFont = False
    Picture.Data = {
      89504E470D0A1A0A0000000D4948445200000018000000180806000000E0773D
      F80000019C49444154484B6364A03160A4B1F90C700BEC03B716FEFFCFC047AE
      85FF19183F1EDEE035015D3FB205FFC9351CA6EFE07A6F8C10A19F05BBEFFDA6
      D807AE4AACB87D40570BAE3FFB0A8F8E1BCFBEE18C1A0D292E064D296EB83CD1
      3E18B500394CE1B18E1CC9E404D17F602ED59E51BD8CE1DF3FE17FFFFE05CAF6
      F77F0759441D0BFEFF65F8F1E61E03F7831BCFD44E1D1567FCFF7F9F745F9F1B
      C516805290A6241B83CEA55886A7AC060C8FB91C19F85FBF7CAE72EAF801D9DE
      EE288A2C0079DD458B97C1F2762283F8CB35E060BF2856FCE0BE40D05901C65F
      118E8E8E7FC8B64046889DC14C910BE8F218A0E16B91E374CD01F5864847C706
      B0E1645980CDE520835E8A0532885F5ACFCAD8C000371CA705F84A55C6FF7FC0
      610E0B1698E157F49632B8A870515616E133FC3F132B03FEA2E2FEEF7A42F581
      E2FB0E0BA5AB8D968C5FFFF083D4FE6215DC79C4EED1F17F2C1C60ADAE8AAC8D
      E8669054655E5D2EB65A4BEA8307C383DFBF18BEFDDFCFF09221023DCC29B220
      36D6F165B9D7D543DAA21FD6321CF9B58690E128914C2878C895272988C8B104
      009E9BF519AA8F99290000000049454E44AE426082}
    WordWrap = False
    TabOrder = 1
    TabStop = True
    OnClick = btnEditClick
    Appearance.ColorChecked = 16111818
    Appearance.ColorCheckedTo = 16367008
    Appearance.ColorDisabled = 15921906
    Appearance.ColorDisabledTo = 15921906
    Appearance.ColorDown = 16111818
    Appearance.ColorDownTo = 16367008
    Appearance.ColorHot = 16117985
    Appearance.ColorHotTo = 16372402
    Appearance.ColorMirrorHot = 16107693
    Appearance.ColorMirrorHotTo = 16775412
    Appearance.ColorMirrorDown = 16102556
    Appearance.ColorMirrorDownTo = 16768988
    Appearance.ColorMirrorChecked = 16102556
    Appearance.ColorMirrorCheckedTo = 16768988
    Appearance.ColorMirrorDisabled = 11974326
    Appearance.ColorMirrorDisabledTo = 15921906
  end
end
