unit AddUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit,OracleConnection,UConst,StrUtils,Math,Menus,
  Login, Grids, BaseGrid, AdvGrid, USystemMenu, AdvGlowButton, AdvObj,
  AdvOfficeButtons;

type
  TfrmAddUser = class(TForm)
    GroupBox1: TGroupBox;
    asgPermission: TAdvStringGrid;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtLoginName: TAdvEdit;
    cbAccessLevel: TComboBox;
    txtLoginID: TAdvEdit;
    btnSave: TAdvGlowButton;
    cancelBtn: TAdvGlowButton;
    Label2: TLabel;
    btnLOVPeg: TAdvGlowButton;
    lblPegawai: TLabel;
    chbSFooter: TAdvOfficeCheckBox;
    procedure cancelBtnClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure asgPermissionCanEditCell(Sender: TObject; ARow,
      ACol: Integer; var CanEdit: Boolean);
    procedure asgPermissionGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure cbAccessLevelChange(Sender: TObject);
    procedure btnLOVPegClick(Sender: TObject);
    procedure asgPermissionCheckBoxClick(Sender: TObject; ACol, ARow: Integer;
      State: Boolean);
    procedure chbSFooterClick(Sender: TObject);
  private
    { Private declarations }
    SeqPegawai : integer;
    accessList : TStringList;
    permissionArr: TSystemMenu_Arr;
    menuArr : TSystemMenu_Arr;
    accessArr:TSystemAccess_Arr;
    aUser:TSystemUser;
    menuGroupList:TStringList;
    IsTambahLg : boolean;
    ListTransNonPosting : Tstringlist;
    procedure clearForm;
    function validateForm :boolean;
    procedure loadPermission;
    procedure loadDefaultPermission;
    procedure ArrangeColSize();
    procedure SetListTransNonPost;
    //procedure execute(Name: string; jabatan: TJabatan);
  public
    { Public declarations }
    procedure execute; overload;
    function execute(ALoginID: string): boolean; overload;
  end;

var
  frmAddUser: TfrmAddUser;

implementation

uses mainMenu, UGeneral, URecord, UEngine;
const
  ColDesk = 0;
  ColInsert = 1;
  ColEdit = 2;
  ColHapus = 3;
  ColRekap = 4;
  ColReport = 5;
  ColCetak = 6;
  ColExcel = 7;
  Colseq = 8;

{$R *.dfm}



procedure TfrmAddUser.clearForm;
var i,j,k:integer;
begin
  SeqPegawai := 0;
  
  txtLoginID.Text := '';
  txtLoginName.Text := '';
  cbAccessLevel.ItemIndex := 0;
  cbAccessLevel.Clear;
  lblPegawai.Caption := '-';
  accessList.Clear;
  TSystemUser.AccessLevelList(accessList);
  for i := 0 to accessList.Count-1 do begin
    if GlobalSystemUser.AccessLevel <= StrToInt(accessList.Names[i]) then
      break;
  end;
  j := accessList.Count-i;
  for k:=1 to j do accessList.Delete(i);

  asgPermission.ClearNormalCells;
  asgPermission.RowCount := 1;

  tslNameValueListToValueList(accessList,cbAccessLevel.Items);
  cbAccessLevel.ItemIndex := -1;
  cbAccessLevel.Enabled := true;
//  txtLoginID.Visible := true;
//  GroupBox2.SetFocus;
  chbSFooter.Checked := false;
  asgPermission.SearchFooter.Visible := chbSFooter.Checked;
  if IsTambahLg then
    txtLoginID.SetFocus;
end;

procedure TfrmAddUser.execute;
begin
  ListTransNonPosting := TStringList.Create;
  SetListTransNonPost;
  clearForm;
  txtLoginID.Enabled := true;
  btnSave.Caption := '&Save';
  aUser.Reset;
  loadPermission;
  ArrangeColSize;
  Run(self);
end;

function TfrmAddUser.execute(ALoginID: string): boolean; 
//var Pegawai : TR_master_pegawai;
begin
  ListTransNonPosting := TStringList.Create;
  SetListTransNonPost;
  clearForm;
  aUser.SelectInDB(ALoginID, '');
  txtLoginID.Text := aUser.UserId;
  txtLoginID.Enabled := false;
  txtLoginName.Text := aUser.UserName;
  SeqPegawai := aUser.PegawaiSeq;
//  if SeqPegawai <> 0 then begin
////    Pegawai := Get_master_pegawai(SeqPegawai);
//    lblPegawai.Caption := Pegawai.kode+' | '+Pegawai.nama;
//  end;
//  txtNIK.Text := aSystUser.NIK;
  if (aUser.AccessLevel = LEVEL_OWNER) then
    cbAccessLevel.ItemIndex := cbAccessLevel.Items.IndexOf('Direksi')
  else if (aUser.AccessLevel = LEVEL_MANAGER) then
    cbAccessLevel.ItemIndex := cbAccessLevel.Items.IndexOf('Manager')
  else if (aUser.AccessLevel = LEVEL_SUPERVISOR) then
    cbAccessLevel.ItemIndex := cbAccessLevel.Items.IndexOf('Supervisor')
  else if (aUser.AccessLevel = LEVEL_OPERATOR) then
   cbAccessLevel.ItemIndex := cbAccessLevel.Items.IndexOf('Operator');
//  else if (aUser.AccessLevel = LEVEL_ADMIN) then
//   cbAccessLevel.ItemIndex := cbAccessLevel.Items.IndexOf('Admin');

  txtLoginID.Enabled := false;
  btnSave.Caption := '&Edit';
  loadPermission;
  ArrangeColSize;
  Result := (Run(self)=mrOK);
end;

procedure TfrmAddUser.cancelBtnClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

function TfrmAddUser.validateForm: boolean;
var msgString:string;
begin
  result := true;
  if txtLoginID.Text = '' then begin
    msgstring := msgString + 'User Id is empty' +SLineBreak;
    result := false;
  end else begin
    if (txtLoginID.Enabled) AND TSystemUser.IsExistInDB(trim(txtLoginID.Text)) then begin
      msgstring := msgString + 'User Id is exists.' +SLineBreak;
      result := false;
    end;
  end;
  if txtLoginName.Text = '' then begin
    msgstring := msgString + 'Name is empty' +SLineBreak;
    result := false;
  end;
  if (btnSave.Caption = '&Save') AND (TSystemUser.UserIDAlreadyRegistered(txtLoginID.Text)) then begin
    MessageDlg('User Id already registered ',mtInformation,[mbok],0);
  end;
  if cbAccessLevel.Text = '' then begin
    msgstring := msgString + 'Access Level must be choosen' +SLineBreak;
    result := false;
  end;
//  if SeqPegawai = 0 then begin
//    msgstring := msgString + 'Pegawai Belum Dipilih' +SLineBreak;
//    result := false;
//  end;
  if msgString <> '' then
    MessageDlg(msgString,mtError,[mbOK],0);
end;

procedure TfrmAddUser.btnLOVPegClick(Sender: TObject);
//var Pegawai : TR_master_pegawai;
begin
  lblPegawai.Caption := '-';
  SeqPegawai := 0;
//  Application.CreateForm(TfrmLOV, frmLOV);
//  frmLOV.OnClose := frmMainMenu.ChildFormSingle;
//  SeqPegawai := frmLOV.ExecutePegawai('',boTrue,TrimAllString(txtLoginID.Text));
//  if SeqPegawai <> 0 then begin
//    Pegawai := Get_master_pegawai(SeqPegawai);
//    lblPegawai.Caption := Pegawai.kode+' | '+Pegawai.nama;
//  end;
end;

procedure TfrmAddUser.btnSaveClick(Sender: TObject);
var systemAccess:TSystemAccess_Arr;i:integer;
    accessListset:TAccessListSet;state:boolean;
//    menuGroupList:TStringList; tempPermission:TSystemMenu_Arr;
begin
  if validateForm = true then begin
    aUser.UserId := txtLoginID.Text;
    aUser.UserName := txtLoginName.Text;
    aUser.PegawaiSeq := SeqPegawai;
//    if cbAccessLevel.Text = 'Direksi' then
//      aUser.AccessLevel := LEVEL_OWNER
//    else
    if cbAccessLevel.Text = 'Manager' then
      aUser.AccessLevel := LEVEL_MANAGER
//    else if cbAccessLevel.Text = 'Supervisor' then
//      aUser.AccessLevel := LEVEL_SUPERVISOR
    else if cbAccessLevel.Text = 'Operator' then
      aUser.AccessLevel := LEVEL_OPERATOR;
//    else if cbAccessLevel.Text = 'Admin' then
//      aUser.AccessLevel := LEVEL_ADMIN;

    systemAccess:=TSystemAccess_Arr.Create(aUser);
    for i:=1 to asgPermission.RowCount-1 do begin
      if asgPermission.Colors[0,i] <> clGradientActiveCaption then begin
        state := False;
        asgPermission.GetCheckBoxState(1,i,state);
        if state then accessListset := accessListset + [alAksInput] else accessListset := accessListset - [alAksInput];
        state := False;
        asgPermission.GetCheckBoxState(2,i,state);
        if state then accessListset := accessListset + [alAksEdit] else accessListset := accessListset - [alAksEdit];
        state := False;
        asgPermission.GetCheckBoxState(3,i,state);
        if state then accessListset := accessListset + [alAksHapus] else accessListset := accessListset - [alAksHapus];
        state := False;
        asgPermission.GetCheckBoxState(4,i,state);
        if state then accessListset := accessListset + [alAksRekap] else accessListset := accessListset - [alAksRekap];
        state := False;
        asgPermission.GetCheckBoxState(5,i,state);
        if state then accessListset := accessListset + [alAksReport] else accessListset := accessListset - [alAksReport];
        state := False;
        asgPermission.GetCheckBoxState(6,i,state);
        if state then accessListset := accessListset + [alAksCetak] else accessListset := accessListset - [alAksCetak];
        state := False;
        asgPermission.GetCheckBoxState(7,i,state);
        if state then accessListset := accessListset + [alAksExport] else accessListset := accessListset - [alAksExport];
        state := False;
        asgPermission.GetCheckBoxState(8,i,state);
        if state then accessListset := accessListset + [alAksPosting] else accessListset := accessListset - [alAksPosting];

        systemAccess.Add(asgPermission.Ints[Colseq,i],accessListset,TShortcut(0),'','',0);
      end;
    end;
    if btnSave.Caption = '&Save' then begin
      try
        myConnection.ADOConnection.BeginTrans;
        aUser.InsertOnDB;
//        systemAccess.de
        systemAccess.InsertOnDB;
        myConnection.ADOConnection.CommitTrans;
      except
        myConnection.ADOConnection.RollbackTrans;
        raise;
      end;
      MessageDlg('Success saving' + SLineBreak + ' You can change the password after login',mtInformation,[mbOK],0);
      if MessageDlg('Add new user ?',mtInformation,[mbYes,mbNo],0) = mrYes then begin
        IsTambahLg := True;
        clearForm
      end else
        cancelBtnClick(nil);
    end else begin
      try
        myconnection.ADOConnection.BeginTrans;
        aUser.UpdateOnDB;
        systemAccess.DeleteUpdateOnDB;
        myConnection.ADOConnection.CommitTrans;
      except
        myConnection.ADOConnection.RollbackTrans;
        raise;
      end;
      Alert('Success');
      cancelBtnClick(nil);
    end;
    systemAccess.Destroy;
  end;
end;

procedure TfrmAddUser.FormCreate(Sender: TObject);
begin
  accessList := TStringList.Create;
  permissionArr:=TSystemMenu_Arr.create;
  menuArr := TSystemMenu_Arr.create;
  accessArr :=TSystemAccess_Arr.Create(self);
  aUser:=TSystemUser.Create;
  menuGroupList := TStringList.Create;

end;

procedure TfrmAddUser.FormDestroy(Sender: TObject);
begin
  accessList.Destroy;
  permissionArr.destroy;
  menuArr.Destroy;
  accessArr.Destroy;
  aUser.Destroy;
  menuGroupList.Destroy;
//  ListTransNonPosting.Destroy;
end;

procedure TfrmAddUser.FormShow(Sender: TObject);
begin
  if txtLoginID.Enabled then txtLoginId.SetFocus else txtLoginName.SetFocus;
  ArrangeColSize;
end;

procedure TfrmAddUser.loadPermission;
var i,j,row,index:integer;accessList:TAccessListSet;
tempPermission: TSystemMenu_Arr;
 
begin
  permissionArr.Clear; 
  permissionArr.LoadFromDB_User(true);

  //Group permission
  permissionArr.getMenuGroupList(menuGroupList); //, True);
  for i:=0 to menuGroupList.Count-1 do begin
    tempPermission := menuGroupList.Objects[i] as TSystemMenu_Arr;
    permissionArr.getMenuGroup(tempPermission,menuGroupList.Strings[i]);
  end;
  accessArr.Destroy;
  accessArr := TSystemAccess_Arr.Create(aUser);
//  asgPermission.RowCount := permissionArr.count+1;
  if aUser.UserId <> '' then begin
    accessArr.LoadFromDB;
    asgPermission.RowCount := 1;
    asgPermission.UnHideColumnsAll;
    for i:=0 to MenuGroupList.Count-1 do begin
      asgPermission.AddRow;
      asgPermission.Cells[0,asgPermission.RowCount-1] := MenuGroupList.Strings[i];
      asgPermission.FontStyles[0,asgPermission.RowCount-1] := [fsBold];
      //color the row
      for j:=0 to asgPermission.ColCount-1 do asgPermission.Colors[j,asgPermission.RowCount-1] := clGradientActiveCaption;
      tempPermission := TSystemMenu_Arr(MenuGroupList.Objects[i]);
      for j:=0 to tempPermission.Count-1 do begin
        asgPermission.AddRow;
        row := asgPermission.RowCount-1;

        asgPermission.Cells[0,row] := tempPermission[j].MenuName;
        asgPermission.Ints[Colseq,row] := tempPermission[j].MenuId;
        index := accessArr.IndexOf(tempPermission[j].MenuId);
        if (index >= 0) then begin
          accessList := accessArr[index].AccessList;
//alAksInput, alAksEdit, alAksHapus, alAksRekap, alAksReport, alAksCetak, alAksExport
          case tempPermission[j].FormTypeNew of
            1:  begin // setting
                  asgPermission.AddCheckBox(ColInsert,row,alAksInput in accessList,false);
                  // kecuali patch saldo
                  if tempPermission[j].MenuId <> 3 then asgPermission.AddCheckBox(ColEdit,row,alAksEdit in accessList,false);
                  // kalau kelola user atau patch saldo
                  if (tempPermission[j].MenuId = 1) or (tempPermission[j].MenuId = 3) or (tempPermission[j].MenuId = 7) then asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                end;
            2:  begin // master
                  asgPermission.AddCheckBox(ColInsert,row,alAksInput in accessList,false);
                  asgPermission.AddCheckBox(ColEdit,row,alAksEdit in accessList,false);
                  asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColRekap,row,alAksRekap in accessList,false);
                  asgPermission.AddCheckBox(ColCetak,row,alAksCetak in accessList,false);
                  asgPermission.AddCheckBox(ColExcel,row,alAksExport in accessList,false);
                end;
            3 : begin // transaksi
                  asgPermission.AddCheckBox(ColInsert,row,alAksInput in accessList,false);
                  //selain tutup buku & pencairan cek giro
                  if (tempPermission[j].MenuId <> 427) and (tempPermission[j].MenuId <> 418) then
                    asgPermission.AddCheckBox(ColEdit,row,alAksEdit in accessList,false);
                  // selain cek giro
                  if (tempPermission[j].MenuId <> 418) then
                    asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColRekap,row,alAksRekap in accessList,false);
                  asgPermission.AddCheckBox(ColCetak,row,alAksCetak in accessList,false);
                  asgPermission.AddCheckBox(ColExcel,row,alAksExport in accessList,false);
                end ;
            4 : begin // transaksi keuangan
                  asgPermission.AddCheckBox(ColInsert,row,alAksInput in accessList,false);
                  //selain tutup buku & pencairan cek giro
                  if (tempPermission[j].MenuId <> 427) and (tempPermission[j].MenuId <> 418) then
                    asgPermission.AddCheckBox(ColEdit,row,alAksEdit in accessList,false);
                  // selain cek giro
                  if (tempPermission[j].MenuId <> 418) then
                    asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColRekap,row,alAksRekap in accessList,false);
                  asgPermission.AddCheckBox(ColCetak,row,alAksCetak in accessList,false);
                  asgPermission.AddCheckBox(ColExcel,row,alAksExport in accessList,false);
                end ;
            5 : begin // Laporan
                  //asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColReport,row,alAksReport in accessList,false);
                  asgPermission.AddCheckBox(ColCetak,row,alAksCetak in accessList,false);
                  asgPermission.AddCheckBox(ColExcel,row,alAksExport in accessList,false);
                end;
            6 : begin // administratif
                  //asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColReport,row,alAksReport in accessList,false);
                  asgPermission.AddCheckBox(ColCetak,row,alAksCetak in accessList,false);
                  asgPermission.AddCheckBox(ColExcel,row,alAksExport in accessList,false);
                end;
          end;
        end else begin
          case tempPermission[j].FormTypeNew of
            1 : begin //Setting
                        asgPermission.AddCheckBox(ColInsert,row, True, false);
                        // kecuali patch saldo
                        if tempPermission[j].MenuId <> 3 then asgPermission.AddCheckBox(ColEdit,row, True, false);
                        // kalau kelola user atau patch saldo
                        if (tempPermission[j].MenuId = 1) or (tempPermission[j].MenuId = 3) then asgPermission.AddCheckBox(ColHapus,row, True, false);
                      end;
            2 :  begin //Master
                        asgPermission.AddCheckBox(ColInsert,row,True,false);
                        asgPermission.AddCheckBox(ColEdit,row,True,false);
                        asgPermission.AddCheckBox(ColHapus,row,True,false);
                        asgPermission.AddCheckBox(ColRekap,row,True,false);
                        asgPermission.AddCheckBox(ColCetak,row,True,false);
                        asgPermission.AddCheckBox(ColExcel,row,True,false);
                      end;
            3 : begin //view transaksi
                        asgPermission.AddCheckBox(ColInsert,row,True, false);
//                        selain tutup buku & pencairan cek giro
                        if (tempPermission[j].MenuId <> 431) and (tempPermission[j].MenuId <> 430) then
                          asgPermission.AddCheckBox(ColEdit,row,True,false);
                        // selain cek giro
                        if (tempPermission[j].MenuId <> 430) then
                          asgPermission.AddCheckBox(ColHapus,row,True,false);
                        asgPermission.AddCheckBox(ColRekap,row,True,false);
                        asgPermission.AddCheckBox(ColCetak,row,True,false);
                        asgPermission.AddCheckBox(ColExcel,row,True,false);   


                       end;
                   4 : begin // transaksi keuangan
                         asgPermission.AddCheckBox(ColInsert,row,alAksInput in accessList,false);
                        //selain tutup buku & pencairan cek giro
                         if (tempPermission[j].MenuId <> 427) and (tempPermission[j].MenuId <> 418) then
                           asgPermission.AddCheckBox(ColEdit,row,alAksEdit in accessList,false);
                        // selain cek giro
                         if (tempPermission[j].MenuId <> 418) then
                         asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                         asgPermission.AddCheckBox(ColRekap,row,alAksRekap in accessList,false);
                         asgPermission.AddCheckBox(ColCetak,row,alAksCetak in accessList,false);
                         asgPermission.AddCheckBox(ColExcel,row,alAksExport in accessList,false);
                       end ;
//            4 : begin   // laporan
//                        //asgPermission.AddCheckBox(ColHapus,row,True,false);
//                        asgPermission.AddCheckBox(ColReport,row,True,false);
//                        asgPermission.AddCheckBox(ColCetak,row,True,false);
//                        asgPermission.AddCheckBox(ColExcel,row,True,false);
//                       end;
            5 : begin // Laporan
                  //asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColReport,row,True,false);
                  asgPermission.AddCheckBox(ColCetak,row,True,false);
                  asgPermission.AddCheckBox(ColExcel,row,True,false);
                end;
            6 : begin // administratif
                  //asgPermission.AddCheckBox(ColHapus,row,alAksHapus in accessList,false);
                  asgPermission.AddCheckBox(ColReport,row,True,false);
                  asgPermission.AddCheckBox(ColCetak,row,True,false);
                  asgPermission.AddCheckBox(ColExcel,row,True,false);
                end;
          end;
        end;
      end;
    end;
    //asgPermission.HideColumn(Colseq);
  end else
    LoadDefaultPermission;
end;

procedure TfrmAddUser.SetListTransNonPost;
begin
  ListTransNonPosting.Clear;
//
//  ListTransNonPosting.add('301='+TRUE_STRING); // TfrmRekapTargetSellOut.Create(Self);
//  ListTransNonPosting.add('302='+TRUE_STRING); // TfrmRekapPO.Create(Self);
//  ListTransNonPosting.add('303='+TRUE_STRING); // TfrmRekapPenerimaanBarang.Create(Self);
//  ListTransNonPosting.add('305='+TRUE_STRING); // TfrmRekapPengeluaranBarangRetur.Create(Self);
//  ListTransNonPosting.add('307='+TRUE_STRING); // TfrmRekapSO.Create(Self);
//  ListTransNonPosting.add('308='+TRUE_STRING); // TfrmRekapPengeluaranBarang.Create(Self);
//  ListTransNonPosting.add('310='+TRUE_STRING); // TfrmRekapPenerimaanBrgRetur.Create(Self);
//  ListTransNonPosting.add('313='+TRUE_STRING); // TfrmRekapBlankoCSO
//  ListTransNonPosting.add('314='+TRUE_STRING); // TfrmRekapHasilCSO
//
//  ListTransNonPosting.add('430='+TRUE_STRING); //cair cek giro
//  ListTransNonPosting.add('429='+TRUE_STRING); //Jurnal Transaksi
//  ListTransNonPosting.add('428='+TRUE_STRING); //Jurnal Umum
//  ListTransNonPosting.add('432='+TRUE_STRING); //Penerimaan Credit Note
//  ListTransNonPosting.add('431='+TRUE_STRING); //Tutup Buku
end;

procedure TfrmAddUser.asgPermissionCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
//  if GlobalSystemUser.AccessLevel >= LEVEL_OWNER then
    CanEdit := acol > 0;
end;

procedure TfrmAddUser.asgPermissionCheckBoxClick(Sender: TObject; ACol,
  ARow: Integer; State: Boolean);
var i : integer;
begin
  if Arow > 0 then begin
    if (ACol in [ColRekap]) and (State = false) then begin
      for i := 1 to asgPermission.ColCount - 2 do begin
        if asgPermission.HasCheckBox(i, Arow) then begin
          asgPermission.SetCheckBoxState(i,Arow,State);
        end;
      end;
    end else begin
      if (asgPermission.HasCheckBox(ColRekap, Arow)) and (State) then begin
        asgPermission.SetCheckBoxState(ColRekap,Arow,True);
      end;
    end;
  end;
end;

procedure TfrmAddUser.asgPermissionGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ACol > 0 then HAlign := taCenter;
end;

procedure TfrmAddUser.loadDefaultPermission;
var state:boolean;i,j,row:integer;tempPermission:TSystemMenu_Arr;
begin
  state := false;
  asgPermission.RowCount := 1;
  //Group permission
  for i:=0 to menuGroupList.Count-1 do begin
    tempPermission := menuGroupList.Objects[i] as TSystemMenu_Arr;
    permissionArr.getMenuGroup(tempPermission,menuGroupList.Strings[i]);
  end;

  if (cbAccessLevel.ItemIndex >= 0) AND (permissionArr.Count > 0) then begin
    asgPermission.UnHideColumnsAll;
    for i:=0 to MenuGroupList.Count-1 do begin
      asgPermission.AddRow;
      asgPermission.Cells[0,asgPermission.RowCount-1] := MenuGroupList.Strings[i];
      asgPermission.FontStyles[0,asgPermission.RowCount-1] := [fsBold];
      //color the row
      for j:=0 to asgPermission.ColCount-1 do asgPermission.Colors[j,asgPermission.RowCount-1] := clGradientActiveCaption;

      tempPermission := TSystemMenu_Arr(MenuGroupList.Objects[i]);
      for j:=0 to tempPermission.Count-1 do begin
        //menentukan default access rights
       // if accessList.Names[cbAccessLevel.ItemIndex] = IntToStr(LEVEL_KASIR) then
        //  state:=false
        if accessList.Names[cbAccessLevel.ItemIndex] = IntToStr(LEVEL_SUPERVISOR) then begin
            if {$IFDEF ACCOUNTING}(tempPermission[j].MenuGroup = MENU_GROUP_ACCOUNTING) OR {$ENDIF}
               (tempPermission[j].MenuGroup = MENU_GROUP_SYSTEM) then state := false
            else state:=true;
        end else if accessList.Names[cbAccessLevel.ItemIndex] = IntToStr(LEVEL_MANAGER) then begin
            if {$IFDEF ACCOUNTING}(tempPermission[j].MenuGroup = MENU_GROUP_ACCOUNTING) OR {$ENDIF}
               (tempPermission[j].MenuGroup = MENU_GROUP_SYSTEM) then state := false
            else state:=true;
        end else if accessList.Names[cbAccessLevel.ItemIndex] = IntToStr(LEVEL_OWNER) then
          state := true
        else if accessList.Names[cbAccessLevel.ItemIndex] = IntToStr(LEVEL_DEVELOPER) then
          state:=true;

        //nama menu
        asgPermission.AddRow;
        row := asgPermission.RowCount-1;
        asgPermission.Cells[0,row] := tempPermission[j].MenuName;
        asgPermission.Ints[ColSeq,row] := tempPermission[j].MenuId;

        case tempPermission[j].FormTypeNew of
            1 : begin //Setting
                        asgPermission.AddCheckBox(ColInsert,row,State,false);

                        // kecuali patch saldo
                        if tempPermission[j].MenuId <> 3 then asgPermission.AddCheckBox(ColEdit,row,state,false);
                        // kalau kelola user atau patch saldo
                        if (tempPermission[j].MenuId = 1) or (tempPermission[j].MenuId = 3) or (tempPermission[j].MenuId = 7) then asgPermission.AddCheckBox(ColHapus,row, State, false);
                      end;
            2 :  begin //kelola master
                        asgPermission.AddCheckBox(ColInsert,row,State,false);
                        asgPermission.AddCheckBox(ColEdit,row,state,false);
                        asgPermission.AddCheckBox(ColHapus,row,State,false);
                        asgPermission.AddCheckBox(ColRekap,row,State,false);
                        asgPermission.AddCheckBox(ColCetak,row,State,false);
                        asgPermission.AddCheckBox(ColExcel,row,State,false);
                      end;
            3 : begin//view transaksi
                        asgPermission.AddCheckBox(ColInsert,row,State,false);
                        //selain tutup buku & pencairan cek giro
                        if (tempPermission[j].MenuId <> 427) and (tempPermission[j].MenuId <> 418) then
                          asgPermission.AddCheckBox(ColEdit,row,state,false);
                        // selain cek giro
                        if (tempPermission[j].MenuId <> 418) then
                          asgPermission.AddCheckBox(ColHapus,row,State,false);
                        asgPermission.AddCheckBox(ColRekap,row,State,false);
                        asgPermission.AddCheckBox(ColCetak,row,State,false);
                        asgPermission.AddCheckBox(ColExcel,row,State,false);
                       end;
            4 : begin//view transaksi
                        asgPermission.AddCheckBox(ColInsert,row,State,false);
                        //selain tutup buku & pencairan cek giro
                        if (tempPermission[j].MenuId <> 427) and (tempPermission[j].MenuId <> 418) then
                        asgPermission.AddCheckBox(ColEdit,row,state,false);
                        // selain cek giro
                        if (tempPermission[j].MenuId <> 418) then
                          asgPermission.AddCheckBox(ColHapus,row,State,false);
                        asgPermission.AddCheckBox(ColRekap,row,State,false);
                        asgPermission.AddCheckBox(ColCetak,row,State,false);
                        asgPermission.AddCheckBox(ColExcel,row,State,false);
                       end;
            5 : begin
                       //asgPermission.AddCheckBox(ColHapus,row,State,false);
                       asgPermission.AddCheckBox(ColReport,row,State,false);
                       asgPermission.AddCheckBox(ColCetak,row,State,false);
                      asgPermission.AddCheckBox(ColExcel,row,State,false);
                      end;
            6 : begin
                       //asgPermission.AddCheckBox(ColHapus,row,State,false);
                       asgPermission.AddCheckBox(ColReport,row,State,false);
                       asgPermission.AddCheckBox(ColCetak,row,State,false);
                      asgPermission.AddCheckBox(ColExcel,row,State,false);
                      end;
        end;
      end;
    end;
  end;
  //asgPermission.HideColumn(ColSeq);
end;

procedure TfrmAddUser.cbAccessLevelChange(Sender: TObject);
begin
  loadDefaultPermission;
  ArrangeColSize;
end;

procedure TfrmAddUser.chbSFooterClick(Sender: TObject);
begin
  asgPermission.SearchFooter.Visible := chbSFooter.Checked;
end;

procedure TfrmAddUser.ArrangeColSize;
begin
  asgPermission.AutoSizeColumns(True, 2);
  asgPermission.ColWidths[Colseq] := 0;
end;

end.
